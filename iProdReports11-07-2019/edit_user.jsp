<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Edit User";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
</head>
<body>	
<div class="container-fluid" >
	<div class="row-head">
		<div class="span12">
			<h3 class="page-title">Edit Users <small>Edit Users details</small></h3>
				<ul class="breadcrumb">
					<li>
						<i class="fa fa-link"></i>
						<a href="#">Quick links : </a>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
					</li>
					<li><a href="ManageUsersMenuAction.action">Manage Users</a></li>
				</ul>
				
				<div class="cl-mcont">
					<div class="content">
						<form class="form-horizontal group-border-dashed" name="edit_users_form" id="edit_users_form"data-validate="parsley" action="" autocomplete='off' enctype="multipart/form-data">
						   <s:iterator value="selectUserDetailsListG" status="rowstatus">
							<s:hidden name="userId" value="%{userId}"/>
							
							<div class="form-group">
							<label class="col-sm-3 control-label">User Name</label>
							<div class="col-sm-6">
							<input type="text" name="userName" id="userName" data-required="true"  data-trigger="change" class="form-control" value="<s:property value="%{userName}" />">
							</div>
							<span id="display"></span>
							</div>
							  
							<div class="form-group">
							<label class="col-sm-3 control-label">User Title</label>
							<div class="col-sm-6">
							<input type="text" name="userTitle" id="userTitle" data-required="true"  data-trigger="change" class="form-control" value="<s:property value="%{userTitle}" />">
							</div>
							<span id="display"></span>
							</div>
							  
							<div class="form-group"> 
							<label class="col-sm-3 control-label">First Name</label>
							<div class="col-sm-6">
							<input type="text" name="firstName" id="firstName" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{firstName}" />">
							</div>
							</div>
							  
							<div class="form-group"> 
							<label class="col-sm-3 control-label">Last Name</label>
							<div class="col-sm-6">
							<input type="text" name="lastName" id="lastName" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{lastName}" />">
							</div>
							</div>
								
							
							<div class="form-group"> 
							<label class="col-sm-3 control-label">Gender</label>
							<div class="col-sm-6">
							<input type="text" name="gender"  id="gender" maxlength="100" parsley-trigger="change" data-required="true" required placeholder="Enter Gender" parsley-error-message="Please Enter Gender" class="form-control" value="<s:property value='%{gender}'/>">
							</div>
							</div>
							
							<div class="form-group"> 
							<label class="col-sm-3 control-label">Email Id</label>
							<div class="col-sm-6">
							<input type="email"  parsley-type="email" name="emailId" id="emailId" data-required="true" required  data-trigger="change" class="form-control"  value="<s:property value="%{emailId}" />">
							</div>
							</div>
							  
							<div class="form-group"> 
							<label class="col-sm-3 control-label">Conact Number</label>
							<div class="col-sm-6">
							<input type="text"  name="contactNo" id="contactNo" data-required="true" required  data-trigger="change" class="form-control"  value="<s:property value="%{contactNo}" />">
							</div>
							</div>
							  

							<div class="form-group">
							<label class="col-sm-3 control-label">Role</label>
							<div class="col-sm-6">
							<s:select id="roleId" name="roleId" required="true" parsley-error-message="Please select Role." parsley-required="true" list="allRoleListG" listValue="roleName" listKey="roleId" headerKey="" headerValue="Select Role" parsley-trigger="change" cssClass="form-control"/>
							</div>
							</div>  
					 </s:iterator>
					 
							<div class="form-group"> 
							<div class="col-sm-offset-2 col-sm-10">
							<button class="btn btn-primary" type="submit" id="updateUser" onclick="javascript:if($('#edit_users_form').parsley('validate')==true)">Update</button>
							<button class="btn btn-default" onclick="javascript:var f =document.forms['edit_users_form'];f.action='ManageUsersMenuAction';f.submit()">Cancel</button>
							</div>
							</div>			   

							</form>
						</div><!-- End of content -->
				</div><!-- End of cl-mcont -->
		</div>	
	</div>
</div>



	
	
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->			
			
			
<script>//for confirmation
	$('#updateUser').click(function() {
	var c = confirm("Are you sure you wish to update this?");
	if(c){
	javascript:var f =document.forms['edit_users_form'];f.action='UpdateSelectUserListAction';f.submit();
	}
	else{
	javascript:var f =document.forms['edit_users_form'];f.action='UpdateUserListAction';f.submit();
	}//you can just return c because it will be true or false	
	});
</script>

</body>
</html>
