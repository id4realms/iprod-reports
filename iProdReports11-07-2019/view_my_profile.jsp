<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "USER INFORMATION";</script>
<%
	response.setHeader("Cache-control","no-store"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires", -1);
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="css/parsley-validations.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="css/template-style.css" type="text/css" media="screen, projection"/>
<link href="css/blitzer/jquery-ui-1.10.3.custom.css" rel="stylesheet">
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<script src="js/parsley.js"></script>
<link rel="stylesheet" href="css/dimming.css" type="text/css" media="screen, projection"/>
<script type="text/javascript" src="js/dimmingdiv.js"></script>
<script> 
	$(function() {
		$( "#listing-tabs" ).tabs();
	});
	$(function() {
			$( "input:submit" ).button();
			$( "a.submit" ).button();
	});
	</script>
</head>
<body>
<%if(session.getAttribute("IprodUserId") != null){%>
<div class="navheads">
    <ul>
      <li>User Information</li>    
    </ul>
  </div>
<center>
<s:form id="view_my_profile_form" name="view_my_profile_form" data-validate="parsley" theme="simple">
<s:hidden name="action"/>
<table class="normal" align="center">
						<s:iterator value="selectUserDetailsListG" status="rowstatus">				
                            <s:hidden name="userId" value="%{userId}"/>
                              <tr> 
                                <td height="28"></td>
                                <td align="left" width="303" height="40"><strong>User Name</strong></td>
                                <td align="center" ><strong>:</strong></td>
                                <td height="28" colspan="1"><div id="sp1" onclick="document.getElementById('sp1').style.display= 'none';document.getElementById('userName').style.display='' ;document.getElementById('userName').select();">&nbsp;<s:property value="%{userName}"/></div>
                                <input style="display: none;" type="text" data-trigger="change" data-required="true" name="userName" id="userName" value="<s:property value="%{userName}"/>"></td>
                              </tr>
                              <tr> 
                                <td height="28"></td>
                                <td align="left" width="303" height="40"><strong>User Tile</strong></td>
                                <td align="center" ><strong>:</strong></td>
                                <td height="28" colspan="1">
                                <s:radio label="Answer" id="userTitle" cssClass="radioInput" name="userTitle" list="{'Mr.','Mrs.','Miss'}"/>
                                </td>
                              </tr>                              
							  <tr> 
                                <td height="28"></td>
                                <td align="left" width="303" height="40"><strong>First Name</strong></td>
                                <td align="center" ><strong>:</strong></td>
                                <td height="28" colspan="1">
                                <div id="sp2" onclick="document.getElementById('sp2').style.display= 'none';document.getElementById('firstName').style.display='' ;document.getElementById('firstName').select();">&nbsp;<s:property value="%{firstName}"/></div>
                                <input style="display: none;" type="text" data-trigger="change" data-required="true" name="firstName" id="firstName" value="<s:property value="%{firstName}"/>">
                                </td>
                              </tr>
							  <tr> 
                                <td height="28"></td>
                                <td align="left" width="303" height="40"><strong>Last Name</strong></td>
                                <td align="center" ><strong>:</strong></td>
                                <td height="28" colspan="1">
                                <div id="sp3" onclick="document.getElementById('sp3').style.display= 'none';document.getElementById('lastName').style.display='' ;document.getElementById('lastName').select();">&nbsp;<s:property value="%{lastName}" /></div>
                                <input style="display: none;" type="text" data-trigger="change" data-required="true" name="lastName" id="lastName" value="<s:property value="%{lastName}"/>">
                                </td>
                              </tr>
                              <tr> 
                                <td height="28"></td>
                                <td align="left" width="303" height="40"><strong>Gender </strong></td>
                                <td align="center" ><strong>:</strong></td>
                                <td colspan="1" height="28">
                                <div id="sp4" onclick="document.getElementById('sp4').style.display= 'none';document.getElementById('gender').style.display='' ;document.getElementById('gender').select();">&nbsp;<s:property value="%{gender}"/></div>
                                <s:select cssStyle="display: none;" name="gender" id="gender" list="{'Male', 'Female'}"
              					headerKey="0" headerValue="----Select Gender----"/>
                                </td>                               
                              </tr>
							  <tr> 
                                <td height="28"></td>
                                <td align="left" width="303" height="40"><strong>Email</strong></td>
                                <td align="center" ><strong>:</strong></td>
                                <td height="28" colspan="1">
                                <div id="sp5" onclick="document.getElementById('sp5').style.display= 'none';document.getElementById('emailId').style.display='' ;document.getElementById('emailId').select();">&nbsp;<s:property value="%{emailId}"/></div>
                                <input style="display: none;" type="text" name="emailId" data-type="email" data-trigger="change" data-required="true" id="emailId" value="<s:property value="%{emailId}"/>">
                                </td>
                              </tr>
							  <tr> 
                                <td height="28"></td>
                                <td align="left" width="303" height="40"><strong>Contact Number</strong></td>
                                <td align="center" ><strong>:</strong></td>
                                <td height="28" colspan="1">
                                <div id="sp6" onclick="document.getElementById('sp6').style.display= 'none';document.getElementById('contactNo').style.display='' ;document.getElementById('contactNo').select();">&nbsp;<s:property value="%{contactNo}"/></div>
                                <input style="display: none;" type="text" name="contactNo" data-trigger="change" data-type="number" data-required="true" id="contactNo" value="<s:property value="%{contactNo}"/>">
                                </td>
                              </tr>
							  <tr height="28"></tr>
							  </s:iterator>
							  </table> 
							  <div>
							  	 <a href="#" class="submit" onclick="javascript:var f =document.forms['view_my_profile_form'];f.action='ChangeMyPasswordAction';f.submit()">Change Password</a>
						         <a href="#" class="submit" onclick="javascript:if($('#view_my_profile_form').parsley('validate')==true){var f =document.forms['view_my_profile_form'];f.action='UpdateMyProfileAction';f.submit();}">Update</a>
						         <a href="#" class="submit" onclick="javascript:var f =document.forms['view_my_profile_form'];f.action='ViewLoginPageAction';f.submit()">Cancel</a>
						      </div>
							                                                            

</s:form>
</center>
<hr class="space" />
 <hr class="space" />
 <hr class="space" />
<%}else{%>
<jsp:forward page="index.jsp"></jsp:forward>
<%}%>
</body>
</html>