
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Add Image";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
<script type="text/javascript">
function imageDupCheck(){
	$("#displayimageName").load('ajax_check_existing_image_name.jsp',{imageName:document.getElementById('imageName').value});
  }
</script>
</head>
<body>
<div class="container-fluid" id="pcont">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Add Image<small></small></h3>
			<ul class="breadcrumb">
				<li>
					<i class="fa fa-link"></i>
					<a href="#">Quick links : </a>
				</li>
				<li>
					<i class="fa fa-home"></i>
					<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				 <li>
					<a href="<s:url action="ManageImageMenuAction"/>">Manage Image</a>
				</li>
			</ul>
    
			<div class="cl-mcont">
   
				 <div class="content">
					 <form class="form-horizontal group-border-dashed" style="border-radius: 0px;" name="add_new_image" id="add_new_image" 
						data-validate="parsley" action="AddImageAction" 
						autocomplete='off' enctype="multipart/form-data" method="post">

							 <div class="form-group">
									<label class="col-sm-3 control-label">Image Name</label>
									<div class="col-sm-6">
									<input type="text" name="imageName" id="imageName" maxlength="30" parsley-trigger="change" data-required="true" required placeholder="Enter Image name" onkeyup="imageDupCheck();" class="form-control" >
									</input>
									<span id="displayimageName"></span>
									</div>
									</div> 
									
									<div class="form-group">
										<label class="col-sm-3 control-label">Select header left picture (e.g. logo) : (Maximum Dimensions: 140*140) :</label>
										<div class="col-sm-6">
										<input id="rtemplateHeaderLeft" type="file" name="imagePath" required placeholder="Upload Your Image" accept="image/gif, image/jpeg, image/png" data-required="true"/>
										<span id="maxSizeForHeaderLeft" style="color:red;"></span>
										<br> <img src="" height="50" alt="Image preview..." id="previewLogo"></img>
										<!--<input type="file" onchange="previewFile()"><br> <img src="" height="50" alt="Image preview...">-->
										</div>
									</div>


									<div class="form-group"> 
										<div class="col-sm-offset-2 col-sm-10">         
										<button id="addImage" class="btn btn-primary" type="submit" onclick="javascript:if($('#add_new_image').parsley('validate')==true){var f = document.forms['add_new_image'];f.action='AddImageAction'; f.submit();}">Submit</button>
										<button class="btn btn-default" onclick="javascript:var f = document.forms['add_new_image'];f.action='ManageImageMenuAction';f.submit();">Cancel</button>
										</div>
								  </div>
						
									 
					</form>    
				</div><!-- End of content -->
			</div><!-- End of cl-mcont -->
		</div>
	</div>
</div>

<!-------------------CLEAN ZONE JS FILES ------------------------------->
	<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
	<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
	<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/fuelux/js/fuelux.js"></script>
	<script type="text/javascript" src="js/pick-a-color-1.2.3/js/tinycolor-0.9.15.min.js"></script>
	<script type="text/javascript" src="js/pick-a-color-1.2.3/js/pick-a-color-1.2.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
	<script type="text/javascript" src="js/imageSize.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->


</body>

</html>
