<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Create Report Template";</script>
<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
<link href="js/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="js/fuelux/css/fuelux.css"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
<link href="css/override_defaults.css" rel="stylesheet" />
<!-- Custom template-style for this template -->
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/screen.css" media="screen, projection"/>
<link rel="stylesheet" type="text/css" href="css/popup_style.css"></link>
<link rel="stylesheet" type="text/css" href="css/screen.css"  media="screen, projection"/>
<link rel="stylesheet" type="text/css" href="css/dimming.css" media="screen, projection"/>
<link rel="stylesheet" type="text/css" href="js/pick-a-color-1.2.3/css/pick-a-color-1.2.3.min.css"/>
<link rel="stylesheet" type="text/css" href="js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
<link rel="shortcut icon" href="images/favicon.png">
<script>
		function addNewDataSourcePopUp(){
		loadDataSourcePopup();
		$('#viewDataSourcePopUp').html('');
		var responseHTML="";	
		var url = 'ViewDataSourcePopUpAction';
		$.ajax({
			type: "POST",
			url: url,
			cache: false,
			async: false,
			dataType: 'html',
			success: function( html ) {
				responseHTML = html;	
			}	
		});
		$('#viewDataSourcePopUp').html(responseHTML);
	}
	</script>
<script type="text/javascript">
function rTemplateDupCheck(){
	$("#displayTemplateName").load('ajax_check_existing_template_name.jsp',{rtemplateName:document.getElementById('rtemplateName').value});
	
  }
</script>
<title>iProd</title>
</head>
<body class="fuelux">
<div class="container-fluid" id="pcont">
	<div class="row-fluid">
	<div class="span12">
	<h3 class="page-title">Create Report Template <small>datasets, styles and more</small></h3>
	<ol class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="<s:url action='LoginAction'/>">Home iPROD Reports</a>
		</li>
		<li><a href="#">Create Template</a></li>

	</ol>
	<div class="eventmessages">
		<s:if test="hasActionMessages()">
		<div class="alert alert-success alert-white rounded">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<div class="icon"><i class="fa fa-check"></i></div>
			<strong>Success!</strong> 
			<s:iterator value="actionMessages">
			<s:property /><br>
			</s:iterator>
		</div>					
		</s:if>
		<s:if test="hasActionErrors()">
			<div class="alert alert-danger alert-white rounded" >
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="fa fa-times-circle"></i><strong>Error!</strong>
				<s:iterator value="actionErrors">
				<s:property />
				</s:iterator>
			</div>
		</s:if>
	</div>
	</div>
	</div>

	<div class="cl-mcont">
	<div class="row">
	<div class="col-md-12">
	<div class="wizard" data-initialize="wizard" id="createReportTemplateWizard">
		<div class="steps-container">
			<ul class="steps">
				<li data-step="1" class="active"><span class="badge">1</span>Template Name<span class="chevron"></span></li>
				<li data-step="2"><span class="badge">2</span>Select Datasource<span class="chevron"></span></li>
				<li data-step="3"><span class="badge">3</span>Report Query Builder<span class="chevron"></span></li>
				<li data-step="4"><span class="badge">4</span>Template Style<span class="chevron"></span></li>
				<li data-step="5"><span class="badge">5</span>Report Parameters<span class="chevron"></span></li>
				<!-- <li data-step="6"><span class="badge">6</span>Commit<span class="chevron"></span></li> -->
			</ul>
		</div>
		<div class="actions">
			<button type="button" class="btn btn-default btn-prev">Prev
				<span class="glyphicon glyphicon-arrow-left"></span></button>
			
			<button type="button" class="btn btn-primary btn-next" id="validateTabFields">Next
				<span class="glyphicon glyphicon-arrow-right"></span>
			</button>

		</div>
	

	<form class="form-horizontal" role="form" name="createReportTemplateForm" id="createReportTemplateForm" action="SaveExcelReportTemplateAction" method="post" enctype="multipart/form-data" theme="simple">
	<div class="step-content">					
	<div class="step-pane sample-pane active" id="step1" data-step="1">
			<div class="alert alert-danger" id="templateNameEntryDivError" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			</div>
			<div class="alert alert-warning" id="templateNameEntryDivWarning" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>	
			 </div>
			<div class="form-group no-padding">
				<div class="col-sm-7">
					<h4 class="hthin">Start Report Template Creation</h3>
					<p>The process of generating a report template blueprint is your first key step
					to generating dynamic reports. You kick-start the template creation with a unique name
					following which the process includes selection of data from a dataset using SQL, design & styling, column selection & computation as per your needs</p>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label class="control-label">Enter Template Name</label> 
					<input placeholder="Enter a unique template name for report blueprint" name="rtemplateName"  data-parsley-required class="form-control" id="rtemplateName" onkeyup="rTemplateDupCheck();"/>
					<span id="displayTemplateName"></span>
					<div id="rtemplateNameOutput">
						<label class="jsonresult" id="templateNameCheckResult"></label>
					</div>
				</div>
			</div>
	</div>
	<div class="step-pane" id="step2" data-step="2">
		<div class="alert alert-danger" id="selectDatasourceResult">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>									  
		<h4><div id="checkTbodyResult" class="datasourcetableresult"></div></h4>
		<!--  <div class="button-panel">	
			<div class="button"><a href="<s:url action="AddDatasourcesAction" encode="true"/>"><input id="add" type="button" class="inputButton" value="+ ADD NEW DATASOURCE"/></a></div>
		</div>	-->
		<div class="block-flat">
			<div class="header">
			<h3>List of Datasources</h3>
			</div>
			<div class="content">
			<div class="table-responsive" id="tableid">
			<table class="table table-bordered" id="datatable">
			<thead>
			<tr>
				<th>ID</th>
				<th>PROFILE</th>
				<th>DATABASE</th>
				<th>SERVER</th>
				<th>PORT</th>
				<th>JDBC URL</th>
				<th>CPANEL</th>
			</tr>
			</thead>
			<tbody>
			<s:iterator value="allDatasourceInstanceListG" status="rowstatus">
			
			<tr id="rowid" data-toggle="tooltip">
				<td><s:property value="#rowstatus.count"/></td>
				<td><s:property value="datasourceName"/></td>
				<td><s:property value="rdbmsDatabaseName"/></td>
				<td><s:property value="rdbmsServerMachineName"/></td>
				<td><s:property value="rdbmsServerPort"/></td>
				<td><s:property value="rdbmsJdbcDriverUrl"/></td>
				<td>
					<div id="datasourceCBArray">
					<input type="checkbox" name="selectedDatasourceInstanceId" id="myCheckbox" fieldValue="%{datasourceId}"/>
					</div>

					<input type="hidden" id="datasourceName<s:property value='datasourceId'/>"
					value="<s:property value='datasourceName' />" />
					<input type="hidden" id="dbServerUsernameHidden<s:property value='datasourceId'/>"
					value="<s:property value='rdbmsServerUsername'/>" />
					<input type="hidden" id="dbServerPasswordHidden<s:property value='datasourceId'/>"
					value="<s:property value='rdbmsServerPassword'/>" />
					<input type="hidden" id="jdbcDriverUrlHidden<s:property value='datasourceId'/>"
					value="<s:property value='rdbmsJdbcDriverUrl'/>" /> 

				</td>
			</tr>
			</s:iterator>
			</tbody>
			</table>
			<div class="dataSource" id="viewDataSourcePopUp"></div>
						<div id="backgroundPopup"></div>
			</div>	
			</div>
			<input type="hidden" name="datasourceId" id="datasourceId" value="<s:property value='datasourceId'/>">
			<input type="hidden" id="selectedDatasourceInstanceName" />
			<input type="hidden" id="selectedDatabaseInstanceName" /> 
			<input type="hidden" id="selectedDatabaseServerUsername" />
			<input type="hidden" id="selectedDatabaseServerPassword" />
			<input type="hidden" id="selectedJdbcDriverUrl" />
			<input type="hidden" id="selectedJdbcDriverClassname" />
		</div>

	</div>
	<div class="step-pane" id="step3" data-step="3">
		<div class="row">
		<div class="col-md-12">		
			    <div class="col-sm-6">
				  <span class="col-sm-6"><strong>BASE SQL QUERY</strong></span>
				  <textarea id="rtemplateSqlquery" name="rtemplateSqlquery" value="" class="form-control" style="height:200px; width: 500px;"></textarea>
				  <input type="button" class="btn btn-primary" value="Clear" id="clearSqlQuery"/> 
				  <input type="button" class="btn btn-primary"value="Validate Query" id="validateSqlQuery"/>
				</div>
				<div class="col-sm-6">
					 <div class="alert alert-success" id="sqlQueryValidateDivSuccess" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					</div>
					<div class="alert alert-danger" id="sqlQueryValidateDivError" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					</div>
					<div class="alert alert-warning" id="sqlQueryValidateDivWarning" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					</div>
					 <h3><strong><label class="jsonresult" id="reportTemplateSqlQueryValidated" style="display:none"></label></strong></h3>
					<div id="reportTemplateSqlQueryResultDiv"></div>
				</div>
		</div>
		</div>

	<div class="row">
	<div class="col-md-12">	
		<div id="reportExecuteSqlQueryOutputDiv"></div>
	</div>
	</div>
	
	</div><!--DIV WITH CLASS step-pane-active AND ID step3 ENDS-->		
	<div class="step-pane" id="step4" data-step="4"><!-- DIV class for step-pane step4 STARTS -->
		<div class="page-head">
					<h2>Choose Styles</h2>
		</div>
		<div class="tab-container"><!-- DIV tab-container STARTS-->
		   <ul class="nav nav-tabs">
			<li class="active"><a href="#titlestyle" data-toggle="tab">Title Style</a></li>
			<li><a href="#headerstyle" data-toggle="tab">Header Style</a></li>
			<li><a href="#detailstyle" data-toggle="tab">Detail Style</a></li>
			<li><a href="#footerstyle" data-toggle="tab">Footer Style</a></li>
			<!--<li><a href="#reportstyle" data-toggle="tab">Report Style</a></li>	-->
			</ul> 
			<div class="tab-content"><!-- DIV tab-content STARTS-->
			    <div class="tab-pane active cont" id="titlestyle"><!-- DIV tab-pane active cont STARTS-->
					<div class="content">
						<label>Title Content Style</label>

						<div class="form-group">
						<label class="col-sm-3 control-label">Enter report title description</label>
						<div class="col-sm-6">
						<input  type="text" class="form-control"  name="rtemplateTitle" id="rtemplateTitle"/>
						<div id="rtemplateTitleOutput"><label id="titleResultPrint"></label></div>
						</div>
						</div> 

						<div class="form-group">
						<label class="col-sm-3 control-label">Enter report subtitle description</label>
						<div class="col-sm-6">
						<input  type="text" class="form-control"  name="rtemplateSubTitle" id="rtemplateSubTitle"/>
						</div>
						</div> 

						<div class="form-group">
							<label class="col-sm-3 control-label">Text Color</label>
							<div class="col-sm-6">
							<input class="pick-a-color form-control" id="titleStyleTextColor" name="titleStyleTextColor" type="text" value="000000"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Background color</label>
							<div class="col-sm-6">
							<input class="pick-a-color form-control" id="titleStyleBgColor" name="titleStyleBgColor" type="text" value="EAEAEA"/>
							</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label">Horizontal Alignment</label>
						<div class="col-sm-6">
						<s:radio theme="simple" name="titleStyleHAlign" list="#{'1':'Left','2':'Center','3':'Right','4':'Justified'}" value="2" />
						</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label">Vertical Alignment</label>
						<div class="col-sm-6">
						<s:radio theme="simple" name="titleStyleVAlign" list="#{'1':'Top','2':'Middle','3':'Bottom','4':'Justified'}" value="2" />		
						</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label">Detail Border Style</label>
						<div class="col-sm-6">
						<s:select theme="simple" label="Select Border Type" headerKey="no-border" headerValue="NO BORDER" list="#{'thin-border':'________________________', 'dashed-border':'------------------------------------', 'dotted-border':'.......................................................................','double-border':'====================='}" name="titleStyleBorderType" id="titleStyleBorderType" cssClass='select'/>
						</div>
						</div>

					</div>	
					<div class="content">
						<label>Sub Content Style</label>
						<div class="form-group">
							<label class="col-sm-3 control-label">Text color</label>
							<div class="col-sm-6">
							<input class="pick-a-color form-control" id="subTitleStyleTextColor" name="subTitleStyleTextColor" type="text" value="000000"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Background color</label>
							<div class="col-sm-6">
							<input class="pick-a-color form-control" type="text" id="subTitleStyleBgColor" name="subTitleStyleBgColor" value="EAEAEA"/>
							</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label">Horizontal Alignment</label>
						<div class="col-sm-6">
						<s:radio theme="simple" name="subTitleStyleHAlign" list="#{'1':'Left','2':'Center','3':'Right','4':'Justified'}" value="2" />
						</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label">Vertical Alignment</label>
						<div class="col-sm-6">
						<s:radio theme="simple" name="subTitleStyleVAlign" list="#{'1':'Top','2':'Middle','3':'Bottom','4':'Justified'}" value="2" />
						</div>
						</div>


						<div class="form-group">
						<label class="col-sm-3 control-label">Detail Border Style</label>
						<div class="col-sm-6">
						<s:select theme="simple" label="Select Border Type" headerKey="no-border" headerValue="NO BORDER" list="#{'thin-border':'________________________', 'dashed-border':'------------------------------------', 'dotted-border':'.......................................................................',                        'double-border':'====================='}" name="subTitleStyleBorderType" id="subTitleStyleBorderType" cssClass='select'/>
						</div>
						</div>
				    </div>
				</div><!-- DIV tab-pane active cont ENDS-->
			<div class="tab-pane cont" id="headerstyle"><!-- DIV FOR class="tab-pane cont" STARTS-->
				<div class="content">
					<label>Header Style</label>
					<div class="form-group">
				 	<label class="col-sm-3 control-label">Select header left picture (e.g. logo) : (Maximum Dimensions: 150*150) :</label>
					<div class="col-sm-6">
						<s:select id="imageId" name="rtemplateHeaderLeft" parsley-error-message="Please select Logo." list="IprodImageMasterListG" listValue="imageName" listKey="imageName" headerKey="" headerValue="Select Logo" parsley-trigger="change" cssClass="form-control"/>
					</div>
					</div>
					
					<div class="form-group">
					<label class="col-sm-3 control-label">Select header right picture (e.g. logo) : (Maximum Dimensions: 150*150) :</label>
					<div class="col-sm-6">
						<s:select id="imageId" name="rtemplateHeaderRight" parsley-error-message="Please select Logo." list="IprodImageMasterListG" listValue="imageName" listKey="imageName" headerKey="" headerValue="Select Logo" parsley-trigger="change" cssClass="form-control"/>
					</div>
					</div>
			
					<div class="form-group">
						<label class="col-sm-3 control-label">Header Text color</label>
						<div class="col-sm-6">
						<input class="pick-a-color form-control" id="headerStyleTextColor" name="headerStyleTextColor" value="000000" type="text"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Header Background color</label>
						<div class="col-sm-6">
						<input class="pick-a-color form-control" id="headerStyleBgColor" name="headerStyleBgColor" value ="FAD764" type="text"/>
						</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label">Horizontal Alignment</label>
					<div class="col-sm-6">
					<s:radio theme="simple" name="headerStyleHAlign" list="#{'1':'Left','2':'Center','3':'Right','4':'Justified'}" value="2" />
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label">Vertical Alignment</label>
					<div class="col-sm-6">
					<s:radio theme="simple"  name="headerStyleVAlign" list="#{'1':'Top','2':'Middle','3':'Bottom','4':'Justified'}" value="2" />
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label">Header Border Style</label>
					<div class="col-sm-6">
					<s:select  theme="simple" label="Select Border Type" headerKey="no-border" headerValue="NO BORDER" list="#{'thin-border':'________________________', 'dashed-border':'------------------------------------', 'dotted-border':'.......................................................................',                        'double-border':'====================='}" name="headerStyleBorderType" id="headerStyleBorderType" cssClass='select'/>
					</div>
					</div>
				</div>
			</div><!-- DIV FOR class="tab-pane cont" Header Style ENDS-->
			<div class="tab-pane cont" id="detailstyle"><!-- DIV FOR class="tab-content" STARTS-->
				<div class="content">
					<label>Report Details Style</label>
					<div class="form-group">
						<label class="col-sm-3 control-label">Text color</label>
						<div class="col-sm-6">
						<input class="pick-a-color form-control" id="detailStyleTextColor" name="detailStyleTextColor" type="text" value="000000"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Background Text color</label>
						<div class="col-sm-6">
						<input class="pick-a-color form-control" id="detailStyleBgColor" name="detailStyleBgColor" type="text" value="333333"/>
						</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label">Horizontal Alignment</label>
					<div class="col-sm-6">
					<s:radio theme="simple" name="detailStyleHAlign" list="#{'1':'Left','2':'Center','3':'Right','4':'Justified'}" value="2" />
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label">Vertical Alignment</label>
					<div class="col-sm-6">
					<s:radio theme="simple" name="detailStyleVAlign" list="#{'1':'Top','2':'Middle','3':'Bottom','4':'Justified'}" value="2" />
					</div>
					</div>


					<div class="form-group">
					<label class="col-sm-3 control-label">Detail Border Style</label>
					<div class="col-sm-6">
					<s:select theme="simple" label="Select Border Type" headerKey="no-border" headerValue="NO BORDER" list="#{'thin-border':'________________________', 'dashed-border':'------------------------------------', 'dotted-border':'.......................................................................',                        'double-border':'====================='}" name="detailStyleBorderType" id="detailStyleBorderType" cssClass='select'/>
					</div>
					</div>
				</div>
			</div><!-- DIV FOR class="tab-pane cont" ENDS-->
			<div class="tab-pane cont" id="footerstyle"><!-- DIV FOR class="tab-content" STARTS-->
				<div class="content">
					<label>Report Footer Style</label>
					<div class="form-group">
					<label class="col-sm-3 control-label">Select footer left picture (e.g. logo) : (Maximum Dimensions: 150*150)</label>
					<div class="col-sm-6">
						<s:select id="imageId" name="rtemplateFooterLeft" parsley-error-message="Please select Logo." list="IprodImageMasterListG" listValue="imageName" listKey="imageName" headerKey="" headerValue="Select Logo" parsley-trigger="change" cssClass="form-control"/>
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label">Select footer right picture (e.g. logo) : (Maximum Dimensions: 150*150) </label>
					<div class="col-sm-6">
						<s:select id="imageId" name="rtemplateFooterRight" parsley-error-message="Please select Logo." list="IprodImageMasterListG" listValue="imageName" listKey="imageName" headerKey="" headerValue="Select Logo" parsley-trigger="change" cssClass="form-control"/>
					</div>
					</div>
				</div>
				</div><!-----DIV FOR class="tab-pane cont" id="footerstyle" ENDS------->
				<div class="tab-pane" id="reportstyle"> <!--DIV FOR class="tab-pane" STARTS-->
						<div class="content">
							
						<div class="form-group">
						<label class="col-sm-3 control-label">Page Size</label>
						<div class="col-sm-6">
						<s:radio  theme="simple" name="pageSize" list="#{'1':'A4','2':'US LETTER'}" value="1" />
						</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label">Page Orientation</label>
						<div class="col-sm-6">
						<s:radio theme="simple" name="pageOrientation" list="#{'1':'Portrait','2':'Landscape'}" value="1" />
						</div>
						</div>

						
						</div><!--------DIV FOR class="tab-pane" ENDS-------->
				</div><!---------DIV FOR id="reportstyle"------->
			</div><!-- DIV tab-content ENDS-->
		</div><!-- DIV FOR class="tab-container" ENDS-->
	</div><!-- DIV class for step-pane step4 ENDS -->
	
	<div class="step-pane" id="step5" data-step="5"><!--DIV WITH CLASS step-pane-active AND ID step4 STARTS-->
			<div class="form-group">	
				<s:hidden name="templateRepo" id="templateRepo" value="D:\REPORTS" />
			</div>	

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" id="createTemplate" class="btn btn-primary wizard-next" style="float:right;" >Create Template<i class="fa fa-caret-right"></i></button>
			</div>
		</div>

		<div class="button-panel">		
			<a href="#"><input id="add" type="button" class="inputButton addColumnSingleton" value="+ ADD COLUMN SINGLETON"/></a>
			<a href="#"><input id="add" type="button" class="inputButton addColumnGroup" value="+ ADD COLUMN GROUP"/></a>
			<a href="#"><input id="add" type="button" class="inputButton removeSingletonAndColumnGroup" value="- REMOVE SINGLETON/ COLUMN GROUP"/></a>
		</div>	
		<div class="block-flat">
			<div class="header">
			<h3>Add Template Parameters</h3>
			</div>
		<div class="table-responsive">
			<table class="table table-bordered reportTemplateParmMasterTableClass" id="reportTemplateParmMasterTable">
			
				<thead>
					<tr>
						<th class="span1">SEQ</th>
						<th class="span4-1">SOURCE COLUMN NAME</th>
						<th class="span4-1">SOURCE COLUMN TYPE</th>
						<th class="span4-1">REPORT COLUMN ALIAS </th>
						<th class="span4-1" id="cpnl">CONTROL PANEL</th>
					</tr>
				</thead>
				<tbody>   
					
				</tbody>
			</table>					
		</div>	
		</div>
	</div>
<!--<div class="step-pane" id="step6" data-step="6">
		<div class="form-group no-padding">

		<div class="form-group">
		<label class="col-sm-3 control-label">To Create Template press the button below</label>	
		<div id ="viewTempDiv">													
		<div class="form-group">
		<s:radio name="rtemplateAllowOperatorAccess" list="#{'1':'Yes','0':'No'}" value="1" /><br>
		</div>
		</div>
		<s:hidden name="templateRepo" id="templateRepo" value="D:\REPORTS" />
		</div>
		</div>	

		<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" id="createTemplate" class="btn btn-primary wizard-next" >Create Template<i class="fa fa-caret-right"></i></button>
		</div>
		</div>		
	</div> -->	  										
</div>
</form>
<div id="loading" style="display:none; text-align: center"><img src="images/load.gif" alt="" /></div>
</div>
</div>								
</div>
</div>

</div>
						

<!-------------------CLEAN ZONE JS FILES ------------------------------->
	<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
	<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
	<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/fuelux/js/fuelux.js"></script>
	<script type="text/javascript" src="js/pick-a-color-1.2.3/js/tinycolor-0.9.15.min.js"></script>
	<script type="text/javascript" src="js/pick-a-color-1.2.3/js/pick-a-color-1.2.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
	<script type="text/javascript" src="js/imageSize.js"></script>
	<!-------------------CLEAN ZONE JS FILES ------------------------------->
	<!-------------------VALIDATION JS FILES ------------------------------->
	<!--<script type="text/javascript" src="js/jdbc_Mysql.js"/>-->
	<script type="text/javascript" src="js/report/createTemplateCustomValidation.js"></script>
	<script type="text/javascript" src="js/report/createTemplateFormTabValidation.js"></script>
	<script type="text/javascript" src="js/report/createTemplateFormElements.js"></script>
	<script type="text/javascript" src="js/report/popup_script.js"></script>
	<!-------------------VALIDATION JS FILES ------------------------------->
	
	<script>
	$(function(){
		$('#validateSqlQuery').on("click",function(){
			var resultDatasourceInstanceId=$('#datasourceId').val();
			//alert("resultDatasourceInstanceId"+resultDatasourceInstanceId);
		});
		
	});
	</script>
	
	<script type="text/javascript">
		$('#validateTabFields').on("click",function(){
			$('div.step-content').hide();
			$( '#loading' ).fadeIn( 100 );			
			$( '#loading' ).delay( 800 ).fadeOut( 100 );	
			setTimeout(function(){$('div.step-content').show();}, 1000);	
		});
	</script>
	
<script type="text/javascript">
	$(document).ready(function(){
	$('#datatable').DataTable();
	$('.dataTables_filter input').addClass('form-control').attr('placeholder','Search');
	$('.dataTables_length select').addClass('form-control'); 
	});
</script>

<!-- block steps -->
<script>
$('#createReportTemplateWizard').on('stepclicked.fu.wizard', function(e) {
    //console.log('change');
    if(data-step===5 && direction==='next') {
         return e.preventDefault();
    }
});
</script>
	

</body>
</html>
