<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page import="com.id4.iprod.*" %>
<%@ page import="org.joda.time.Interval" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>

<head>
<script>document.title = "Welcome to iProd - iD4's Production Supervision System";</script>
<%
	response.setHeader("Cache-control","no-store"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires", -1);
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="css/template-style.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" type="text/css" href="css/component.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<script src="js/parsley.js"></script>
<link rel="stylesheet" href="css/dimming.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="css/parsley-validations.css" type="text/css" media="screen, projection"/>
<script type="text/javascript" src="js/dimmingdiv.js"></script>
<script src="js/jquery-1.9.1.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/cbpHorizontalSlideOutMenu.min.js"></script>
<script>
function startNavMenu(){
	var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
}
</script>
<script type="text/javascript">
$(function () {
    $('.click-nav > ul').toggleClass('no-js js');
    $('.click-nav .js ul').hide();
    $('.click-nav .js').click(function(e) {
        $('.click-nav .js ul').slideToggle(200);
        $('.clicker').toggleClass('active');
        e.stopPropagation();
    });
    $(document).click(function() {
        if ($('.click-nav .js ul').is(':visible')) {
            $('.click-nav .js ul', this).slideUp();
            $('.clicker').removeClass('active');
        }
    });
});
</script> 
<script type="text/javascript">

function displayAboutUs(){
	window.scrollTo(0, 0);
	var w, h, l, t;
	w = 620;
	h = 290;
	l = (screen.width / 2) - (w / 2);
	t = (screen.height / 2) - (h * 0.6);
	displayFloatingDiv('aboutuscontent', '', w, h, l, t);
	return false;
}
</script>
</head>
<body onload="startNavMenu()">
<div id="loginHeader" class="nav-subpage clearfix border-radius-bottom-five box-shadow-trans">
<%if((Integer)session.getAttribute("IprodUserRoleId") == 1 || (Integer)session.getAttribute("IprodUserRoleId") == 2){%>
<nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
<div class="cbp-hsinner">
<a class="brand-subpage" href="/" target="_top"><span class="iprod_logo_holder"><span class="iprod_header_logo">
            </span></span><span class="divider-vertical-blue"></span></a>
<ul class="cbp-hsmenu">
						<li>
							<a href="#">LINE PROVISIONING</a>
							<ul class="cbp-hssubmenu">
								<li><a href="<s:url action="LoadWorkshiftJobsScreenAction"/>"><center><img src="images/job_alloc_icon.png"  width="40" height="40"/></center><span>Job Provisioning</span></a></li>
								<li><a href="<s:url action="LoadOperatorAllocationScreenAction"/>"><center><img src="images/operator_board_icon.png"  width="40" height="40"/></center><span>Operators</span></a></li>
								<li><a href="<s:url action="LoadWorkshiftKPIScreenAction"/>"><center><img src="images/kpi_icon.png"  width="34" height="45"/></center><span>KPI Config</span></a></li>
								<li><a href="<s:url action="ViewAllWorkShiftsProvisionedListAction"/>"><center><img src="images/workshift_icon.png"  width="40" height="40"/></center><span>Workshifts Provisioned</span></a></li>
								<li><a href="<s:url action="ViewAllOrderItemsProvisionedListAction"/>"><center><img src="images/jobs_icon.png"  width="40" height="40"/></center><span>Orderitems Provisioned</span></a></li>
								</ul>
						</li>
						<li>
							<a href="#">LINE HISTORY</a>
							<ul class="cbp-hssubmenu">
								<li><a href="<s:url action="ViewAllJobsActualsListAction"/>"><center><img src="images/jobs_actuals_icon.png"  width="45" height="45"/></center><span>Jobs History</span></a></li>
								<li><a href="<s:url action="ViewAllWorkshiftActualsListAction"/>"><center><img src="images/workshift_icon.png"  width="40" height="40"/></center><span>Workshift History</span></a></li>
								<li><a href="<s:url action="ViewAllOrderitemsActualsListAction"/>"><center><img src="images/orderitem_actuals.png"  width="40" height="40"/></center><span>Orderitem History</span></a></li>
								<li><a href="<s:url action="LoadJobDowntimeAction"/>"><center><img src="images/breakdown.png"  width="40" height="40"/></center><span>Line Breakdowns</span></a></li>
								</ul>
						</li>
					
						<li>
							<a href="#">LINE RUNTIMES</a>
							<ul class="cbp-hssubmenu">
								<li><a href="<s:url action="OrderItemsRuntimeListAction"/>"><center><img src="images/runtime.png"  width="40" height="40"/></center><span>Orderitems Runtime</span></a></li>
								</ul>
						</li>
						
						<li>
							<a href="#">REPORTING</a>
							<ul class="cbp-hssubmenu">
								<li><a href="<s:url action="CreateNewReportTemplateAction"/>"><center><img src="images/create_template.jpg"  width="40" height="40"/></center><span>Add Template</span></a></li>
								<li><a href="<s:url action="ManageReportsTemplatesAction"/>"><center><img src="images/manageTemplates.png"  width="40" height="40"/></center><span>Manage Templates</span></a></li>
								<li><a href="<s:url action="GenerateReportAction"/>"><center><img src="images/generate_reports_icon.png"  width="40" height="40"/></center><span>New Report</span></a></li>
								<li><a href="<s:url action="ManageReportsInstanceAction"/>"><center><img src="images/manage_report_instances.jpg"  width="40" height="40"/></center><span>Manage Reports</span></a></li>
								<li><a href="<s:url action="ManageRecipientsMenuAction"/>"><center><img src="images/email_images.png"  width="40" height="40"/></center><span>Manage Recipients</span></a></li>
								
							</ul>
						</li>
						<li>
							<a href="#">MASTER DATA</a>
							<ul class="cbp-hssubmenu">
								<li><a href="<s:url action="ManageUsersMenuAction"/>"><center><img src="images/job_alloc_icon.png"  width="40" height="40"/></center><span>Manage Users</span></a></li>
								<li><a href="<s:url action="ManageWorkstationAction"/>"><center><img src="images/workstation.png"  width="40" height="40"/></center><span>Manage Workstations </span></a></li>
								<li><a href="<s:url action="ManageShiftsAction"/>"><center><img src="images/workshift_icon.png"  width="40" height="40"/></center><span>Manage Shifts </span></a></li>
								<li><a href="<s:url action="ManageCustomersMenuAction"/>"><center><img src="images/manage_customer_icon.png"  width="40" height="40"/></center><span>Manage Customers</span></a></li>
								<li><a href="<s:url action="ManageSchedulersAction"/>"><center><img src="images/schedular.png"  width="40" height="40"/></center><span>Manage Schedulers</span></a></li>
								</ul>
						</li>	
						<li> 
						<div class="click-nav">
                        <ul class="no-js">
						<li>
							<a href="#" class="clicker"><%=(String)session.getAttribute("IprodFirstName")%>&nbsp;<img src="images/Exit.png"></a>
							<ul>
								<li><a href="<s:url action="LogoutAction"/>" class="log-out">Logout</a></li>
								<li><div href="#" onclick="displayAboutUs()" class="abt-us">About Us</div></li>
								
							</ul>
						<li>
						</ul>
					</div>
						</li>
						</ul>

</div>
</nav>    	       	    
<%}%>
</div>
<div id="aboutuscontent" style="display: none; background-color: #F4F4F4; overflow: auto;" align="center">
<div>
<img src="images/iprod-logo-200-40.png" align="left"/><br><br>
<p style="color: white; font-family: Myriad Pro, Calibri; font-size: 14px; text-align: justify; padding: 10px;">
iD4's iPROD - TAKT Time and Production Supervision System provides assembly line supervisors and operators with a real-time visual data of their line's progress in meeting the desired production. To remain competitive, companies must run their factories and production operations at the limit of their technical, logistical and organizational capabilities. Constant innovation and adaptability is the key to sustainable success.<br> 
<a href="about_us.jsp" style="margin-top: 100px;">Click here to see the license agreement</a>
</p></div>
</div>
<div class="container">
<div class="container-subpage">
<div class="page-title"><div class="firstWord">End User</div><div class="restWords">License Agreement</div></div>
</br>
<p>End-User License Agreement for iPROD consisting of iPROD (core), iProd ANDON and iProd REPORTS - suit of products of  iD4 REALMS Infotech Pvt. Ltd. hereon referred to in this EULA as "ID4 REALMS" </p>
</br>
IMPORTANT- READ CAREFULLY THE FOLLOWING TERMS & CONDITIONS : 
</br>
<p>
This  End-User License Agreement (EULA) is a legal agreement between you (either an individual or a single entity) and iD4 REALMS,  for the above mentioned products, which includes computer software, database structures and SOFTWARE PRODUCTs or SOFTWARE. BY DOWNLOADING, INSTALLING, COPYING, ACCESSING OR OTHERWISE USING THE SOFTWARE(s), YOU ARE AGREEING TO BE BOUND BY THIS AGREEMENT . If you do not agree to the terms of this agreement, you are not authorized to use these SOFTWARE PRODUCTS.  Terms of use are as stated below :  
</p></br>
<p>
1. GRANT OF LICENSE. You, the end user, are granted a non-exclusive license to use iPROD under the terms stated in this agreement.  This license allows you to run the Software on one LAN (Local Area Network) or on one computer.  You may not rent or lease the software. iD4 REALMS INFOTECH  PVT  LTD grants you the right to use the Software as a planning, scheduling & performance management tool  and for making copies of it for backup and archival purposes only. 
</p></br>

<p>
2. COPYRIGHT.  All title, intellectual property and proprietary rights, including trade secrets and presentation, in the software and any copies thereof and the accompanying electronic and written materials, are  owned by  iD4 REALMS INFOTECH  PVT  LTD and are protected by copyright laws and international treaty provisions. iD4 REALMS INFOTECH  PVT  LTD   retain all rights with respect to the software not expressly  granted herein. iD4 REALMS INFOTECH  PVT  LTD   also retain all rights to terminate the license without stating any reason. 
</p></br>

<p>
3. RESTRICTIONS.  You may not use, copy, merge, alter,  adapt, modify, rent, lease or sublicense the software or the  written materials, or any copy thereof, in  whole or  in part, except as expressly provided in this Agreement or under applicable statutes.  You may not reverse-engineer, de-compile or disassemble the software.  You may not modify, or create derivative works based upon the software, in whole or in part.  You may not copy the software, or any portion thereof, except as specifically permitted by the terms of this  agreement. The Software and its component parts may not be separated for use. 
</p></br>

<p>
4. TERMINATION.  The license is effective until terminated.  Without prejudice to other rights, iD4 REALMS may terminate this license automatically if you fail to comply with any of  the terms of this Agreement.  Upon any termination of this Agreement, you agree to either return the Software to iD4 REALMS INFOTECH  PVT  LTD   or destroy all copies of the Software, including, without limitation, any associated written materials. 
</p></br>

<p>
5. DISCLAIMER :  The software is not fault-tolerant. All decisions and actions made by you are your own sole responsibility. We make no guarantees or representations regarding the accuracy or significance. We cannot be held responsible for any interpretation, action or use that may be made. In no event shall we be liable for damages of any kind, including without limitation any compensatory, incidental, direct, indirect, special, punitive or consequential damages, loss of income or profit, damage to property, claims of third parties, or any other kind of losses. We cannot be held responsible for any claims regarding negative and adverse results as well. 
iD4 REALMS and its suppliers specifically disclaim any express or implied warranty of fitness. 
</p></br>

<p>
6. LIABILITY : NO LIABILITY FOR CONSEQUENTIAL DAMAGES. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL  iD4 REALMS   OR ITS SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION  OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE PRODUCTS, EVEN IF  iD4 REALMS   HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. NO LIABILITY WHATSOEVER. 
</p></br>

<p>
7. LIMITATION OF LIABILITY : BECAUSE SOME STATES AND JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE RESTRICTION MAY NOT APPLY TO YOU. IN ANY CASE, iD4 REALMS ENTIRE LIABILITY AND YOUR EXCLUSIVE REMEDY UNDER THIS EULA SHALL NOT EXCEED ONE INDIAN RUPEE (INR 1.00). 
</p></br>

<p>
8. LIMITATION OF REMEDIES : EXCEPT TO THE EXTENT EXPRESSLY REQUIRED BY APPLICABLE STATUTES, THE ENTIRE LIABILITY OF iD4 REALMS   AND YOUR REMEDY SHALL BE LIMITED TO PRODUCT REPLACEMENT ONLY.
</p></br>

<p>
9. INTERPRETATION.  If one or more provisions of this Agreement are held to be illegal or unenforceable under applicable law, such illegal or unenforceable portion(s) shall be limited or excluded from this Agreement to the minimum extent required so that this Agreement shall otherwise remain in full force and effect and enforceable in accordance with its terms. 
</p></br>

<p> 
10. GENERAL.  This Agreement shall be governed by and construed as per Indian Constitution.  You agree that this Agreement is the complete and exclusive statement of the mutual understanding between us and supersedes and cancels all previous written and oral agreements and communications relating to the subject matter of this Agreement.  If any provision of this Agreement is found void, invalid or unenforceable, it will not affect the validity of the balance of this Agreement, which shall remain valid and enforceable according to its terms. 
</p></br>

<p>
11. iD4 REALMS  RESERVES ALL RIGHTS NOT SPECIFICALLY GRANTED IN THIS STATEMENT. 
YOU ACKNOWLEDGE THAT YOU HAVE READ THE TERMS OF THIS AGREEMENT AND AGREE TO BE BOUND BY ITS TERMS.
SHOULD YOU HAVE ANY QUESTIONS CONCERNING THIS AGREEMENT, OR IF YOU DESIRE TO CONTACT iD4 REALMS FOR ANY REASON, CONTACT US AT  sales@id4-realms.com.
</p></br>
</div>
<div id="footerpush" class="footerpush"></div>
</div>
<div id="footercontainer">
        <div class="gradientback"></div>

        <div class="footer">
            <div class="footerinner">
                <div class="inliner">
                    Copyright &copy;2009-2013 iD4 Realms Infotech Pvt. Ltd. All rights reserved.
                    <a href="https://iprod.id4-realms.com/policies/termsandconditions.aspx">Legal</a>
                    <a href="https://iprod.id4-realms.com/policies/privacy.aspx">Privacy</a>
                    <a href="https://iprod.id4-realms.com/contactus/">Sales</a>
                    <span>Questions: +91.20.4078 8964</span>
                </div>
            </div>
        </div>  <!-- .footer -->
</div>

</body>
</html>


