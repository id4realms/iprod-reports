<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
  	<link rel="shortcut icon" href="images/favicon.png"/>
    <link href="js/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
	<link href="css/style.css" rel="stylesheet" />
</head>
<body>

<!-------------------CLEAN ZONE JS FILES -------------s------------------>
	<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
	<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
	<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/fuelux/js/fuelux.js"></script>
	<script type="text/javascript" src="js/wColorPicker.min.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dashBoard();
      });
    </script>

  </body>
</html>
