<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Add Recipient";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
<script type="text/javascript">
function userDupCheck(){
	$("#displayuserName").load('ajax_check_existing_recipient_name.jsp',{firstName:document.getElementById('firstName').value});
  }
</script>
</head>
<body>
<div class="container-fluid" id="pcont">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Add Recipient<small></small></h3>
			<ul class="breadcrumb">
			 <li>
			 <i class="fa fa-link"></i>
			 <a href="#">Quick links : </a>
			 </li>
			 <li>
			 <i class="fa fa-home"></i>
		<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
			 <i class="fa fa-angle-right"></i>
			 </li>
			 <li>
			 <a href="<s:url action="ManageRecipientsMenuAction"/>">Manage Recipient</a>
			 </li>
			</ul>
			
			<div class="cl-mcont">

				<div class="content">
					<form class="form-horizontal group-border-dashed" style="border-radius: 0px;" name="add_recipient_form" id="add_recipient_form" 
					data-validate="parsley" action="AddNewRecipientSaveAction" 
					autocomplete='off'>

					<div class="form-group">
					<label class="col-sm-3 control-label">First Name</label>
					<div class="col-sm-6">
					<input type="text" name="firstName" required parsley-type="alphanum" id="firstName" maxlength="30" parsley-trigger="change" data-required="true" required placeholder="Enter First name" onkeyup="userDupCheck();" class="form-control">
					<span id="displayuserName"></span>
					</div>
					</div> 

					<div class="form-group">
					<label class="col-sm-3 control-label">Last Name</label>
					<div class="col-sm-6">
					<input type="text" name="lastName" required parsley-type="alphanum" id="lastName" maxlength="30" parsley-trigger="change" data-required="true" required placeholder="Enter Last name" class="form-control">
					</div>
					</div> 

					<div class="form-group">
					<label class="col-sm-3 control-label">Email Id</label>
					<div class="col-sm-6">
					<input type="email" name="emailId" id="emailId" parsley-type="email" parsley-trigger="change" required placeholder="Email Id" class="form-control" data-error-message="Enter valid email Id">
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label">Contact No</label>
					<div class="col-sm-6">
					<input type="text" name="contactNo" id="contactNo"  maxlength="10" parsley-trigger="change" required parsley-type="phone"  placeholder="(XXX) XXXX XXX" class="form-control">
					</div>
					</div> 

					<div class="form-group"> 
					<div class="col-sm-offset-2 col-sm-10">         
					<button id="addRecipient" class="btn btn-primary" type="submit" onclick="javascript:if($('#add_recipient_form').parsley('validate')==true){var f = document.forms['add_recipient_form'];f.action='AddNewRecipientSaveAction'; f.submit()}">Submit</button>
					<button class="btn btn-default" onclick="javascript:var f = document.forms['add_recipient_form'];f.action='ManageRecipientsMenuAction';f.submit();">Cancel</button>
					</div>
					</div>
					</form>    
				</div><!-- End of content -->
			</div><!-- End of cl-mcont -->
		</div>
	</div>
</div>

<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
	

</body>

</html>
