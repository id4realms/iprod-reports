<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page import="com.id4.iprod.*" %>
<%@ page import="org.joda.time.Interval" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<head>
<%
	response.setHeader("Cache-control","no-store"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires", -1);
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome to iProd - iD4's Production Supervision System</title>
<!-- Framework CSS -->
<!--[if lt IE 8]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen, projection"><![endif]-->
<link rel="stylesheet" href="css/print.css" type="text/css" media="print"/>
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="css/template-style.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="css/blitzer/jquery-ui-1.10.3.custom.css"/>
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<script> 
	$(function() {
		$(function() {
			$( "input:submit" ).button();
			$('#userName').focus();
		});
	});
</script>
</head>
<body>
</body>
</html>
