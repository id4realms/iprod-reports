<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Change User Password";</script>
<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
<title>iProd</title>
</head>
<body>		
<div class="container-fluid" id="pcont">
<div class="row-fluid">
<div class="span12">
<h3 class="page-title">Change Password</h3>
<ul class="breadcrumb">
	<li>
		<i class="fa fa-home"></i>
		<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
	</li>
	<li><a href="#">Change Password</a></li>

</ul>
  
			<div class="eventmessages">
				<s:if test="hasActionMessages()">
				<div class="success-message">
				<img src="images/success.png" />
				<s:iterator value="actionMessages">
				<s:property />
				</s:iterator>
				</div>
				</s:if>
				<s:if test="hasActionErrors()">
				<div class="error-message">
				<img src="images/error.png" />
				<s:iterator value="actionErrors">
				<s:property />
				</s:iterator>
				</div>
				</s:if>
			</div>
<div class="cl-mcont">


    <div class="content">
		<form class="form-horizontal group-border-dashed" name="change_users_password_form" id="change_users_password_form" data-validate="parsley" action="ChangeUserPasswordSaveAction" autocomplete='off' method="POST"> 
  <s:hidden name="userId"/>         

			
	   <div class="form-group">
			


				   <div class="form-group">
				<label class="col-sm-3 control-label">Old Password</label>
				<div class="col-sm-6">
				<input type="password" name="oldPassword" id="oldPassword" maxlength="30" parsley-trigger="change" data-required="true"  required placeholder="Enter Old Password" class="form-control" />
				</div>
				</div> 

				   <div class="form-group">
				<label class="col-sm-3 control-label">New Password</label>
				<div class="col-sm-6">
				<input type="password" name="newPassword" id="newPassword" parsley-trigger="change"  parsley-minlength="6"  data-required="true" required placeholder="Enter New Password" class="form-control"  />
				
				
				</div>
				</div> 

				<div class="form-group">
				<label class="col-sm-3 control-label">Confirm Password</label>
				<div class="col-sm-6">	
				<input type="password" maxlength="8" data-trigger="change" parsley-required="true" parsley-equalto="#newPassword" data-required="true" parsley-error-message="Password does not match ! ";  name="userPassword" value="" class="form-control"  placeholder="Retype Password"/>
				</div>
				</div>


				 <div class="form-group"> 
				 <div class="col-sm-offset-2 col-sm-10">
					 
				 <button class="btn btn-primary" type="submit" onclick="javascript:if($('#change_users_password_form').parsley('validate')==true){var f =document.forms['change_users_password_form'];f.action='ChangeUserPasswordSaveAction';f.submit()}">Update</button>
				 <button class="btn btn-default" onclick="javascript:var f =document.forms['change_users_password_form'];f.action='ProfileAction';f.submit()">Cancel</button>
					  </div>
					  </div>

			</form>
            </div><!-- End of content -->
			</div>
		</div>
	</div>
</div>
</div>

<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->


</body>
</html>
