<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Edit Recipient";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
</head>
<body>	
<div class="container-fluid" >
	<div class="row-head">
		<div class="span12">
			<h3 class="page-title">Edit Recipient <small>edit recipient details</small></h3>
			<ul class="breadcrumb">
				<li>
					<i class="fa fa-link"></i>
					<a href="#">Quick links : </a>
				</li>
				<li>
					<i class="fa fa-home"></i>
					<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
				</li>
				<li><a href="ManageRecipientsMenuAction.action">Manage Recipient</a></li>

			</ul>
			<div class="cl-mcont">
				<div class="content">
					<form class="form-horizontal group-border-dashed" name="edit_recipient_form" id="edit_recipient_form" action="UpdateRecipientDetailsAction" data-validate="parsley" autocomplete='off' ">
					   <s:iterator value="selectRecipientDetailsListG" status="rowstatus">
						<s:hidden name="recipientId" value="%{recipientId}"/>
						
							<div class="form-group"> 
							<label class="col-sm-3 control-label">First Name</label>
							<div class="col-sm-6">
							<input type="text" required parsley-type="alphanum" name="firstName" id="firstName" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{firstName}" />">
							</div>
							<span id="display"></span>
							</div>
						  
							<div class="form-group"> 
							<label class="col-sm-3 control-label">Last Name</label>
							<div class="col-sm-6">
							<input type="text" required parsley-type="alphanum" name="lastName" id="lastName" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{lastName}" />">
							</div>
							<span id="display"></span>
							</div>
							
							<div class="form-group"> 
							<label class="col-sm-3 control-label">Email Id</label>
							<div class="col-sm-6">
							<input type="email" required parsley-type="email" name="emailId" id="emailId" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{emailId}" />">
							</div>
							<span id="display"></span>
							</div>
						  
							<div class="form-group"> 
							<label class="col-sm-3 control-label">Contact Number</label>
							<div class="col-sm-6">
							<input type="text"  name="contactNo" id="contactNo" data-required="true" parsley-type="phone" required data-trigger="change" class="form-control"  value="<s:property value="%{contactNo}" />">
							</div>
							<span id="display"></span>
							</div>
						  
				 </s:iterator>
				 
						<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
						<button class="btn btn-primary" type="submit" id="updateRecipient">Update</button>
						<button class="btn btn-default" onclick="javascript:var f =document.forms['edit_recipient_form'];f.action='ManageRecipientsMenuAction';f.submit()">Cancel</button>
						</div>
						</div>			   

						</form>
					</div><!-- End of content -->
				</div><!-- End of cl-mcont -->
		</div>
	</div>
</div>
	
	
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->				
			


</body>
</html>
