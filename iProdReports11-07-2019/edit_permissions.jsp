<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Edit Persmission";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
</head>
<body>	
<div class="container-fluid" >
	<div class="row-head">
		<div class="span12">
			<h3 class="page-title">Edit Role <small>edit role permissions</small></h3>
			<ul class="breadcrumb">
				<li>
					<i class="fa fa-link"></i>
					<a href="#">Quick links : </a>
				</li>
				<li>
					<i class="fa fa-home"></i>
					<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
				</li>
				<li><a href="ManageRoleAction.action">Manage Role</a></li>

			</ul>

			<div class="cl-mcont">
				<div class="content">
					<form class="form-horizontal group-border-dashed" name="edit_role_permission_form" id="edit_role_permission_form" 
						data-validate="parsley" action="EditRoleModuleOperationsAction" 
						autocomplete='off' method="post">
					Edit Persmission For :<h4><s:property value="roleName" /></h4>
					<s:hidden id="roleId" name="roleId" value="%{roleId}" />
					 <table class="table table-bordered">
						 <thead>
							<tr>
							<th>MODULE NAME</th>
							<s:iterator value="operationListG" var="operatorListGVar" status="operatorListGStatus">
							<th><s:property value="operationDescription"/></th>
							</s:iterator>
							</tr>
						</thead>
						<tbody class="assemblyqueueTbody">
									<s:iterator value="moduleListG" var="moduleListGVar" status="moduleListGStatus">
									<input type="hidden" name='roleModulePerms[<s:property value="%{#moduleListGStatus.index}" />].moduleId' value="<s:property value='#moduleListGVar.moduleId'/>"/>
									<% int operationCounter = 0; %>
										<tr>
										<td class="span4"><s:property value="moduleName"/></td>					
										<s:iterator value="moduleWiseOperationPermissionsListG" var="moduleWiseOperationPermissionsListGVar" status="moduleWiseOperationPermissionsListGStatus">
										<s:if test="#moduleWiseOperationPermissionsListGVar.moduleId == #moduleListGVar.moduleId">
										
											<s:if test="#moduleWiseOperationPermissionsListGVar.isAllowed == 1">
											<td class="span4">
												<input type="hidden" name='roleModulePerms[<s:property value="%{#moduleListGStatus.index}" />].operation[<%out.print(operationCounter);%>].operationId' value="<s:property value='#moduleWiseOperationPermissionsListGVar.operationId'/>"/>
												<input type="checkbox" name='roleModulePerms[<s:property value="%{#moduleListGStatus.index}" />].operation[<%out.print(operationCounter);%>].isAllowed' value="1" checked/>
												</td>
											</s:if>
											<s:else>
											<td class="span4">
												<input type="hidden" name='roleModulePerms[<s:property value="%{#moduleListGStatus.index}" />].operation[<%out.print(operationCounter);%>].operationId' value="<s:property value='#moduleWiseOperationPermissionsListGVar.operationId'/>"/>
												<input type="checkbox" name='roleModulePerms[<s:property value="%{#moduleListGStatus.index}" />].operation[<%out.print(operationCounter);%>].isAllowed' value="1"/>
												</td>
											</s:else>
											<% operationCounter++; %>
										
										</s:if>
										</s:iterator>
										</tr>
									</s:iterator>
						</tbody>
					  </table>

						<div class="form-group"> 
					<div class="col-sm-offset-2 col-sm-10">
					 
									 <!--	<s:submit type="button" value="Submit" action="%{editUrl}" theme="simple"/>   -->      
					  <button id="submitRole" class="btn btn-primary" type="submit" onclick="javascript:if($('#edit_role_permission_form').parsley('validate')==true){var f = document.forms['edit_role_permission_form'];f.action='EditRoleModuleOperationsAction'; f.submit()}">Submit</button>
					 <button class="btn btn-default" onclick="javascript:var f = document.forms['edit_role_permission_form'];f.action='ManageRoleAction';f.submit();">Cancel</button>
								  </div>
								  </div>
					 
					</form>
				</div><!-- End of content -->
			</div><!-- End of cl-mcont -->
		</div>
	<div>
</div>


		
 		

       
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->		
  

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
</body>

</html>
