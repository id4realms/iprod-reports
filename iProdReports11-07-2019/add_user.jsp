<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Add User";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
<script type="text/javascript">
function userDupCheck(){
	$("#displayuserName").load('ajax_check_existing_user_name.jsp',{userName:document.getElementById('userName').value});
  }
</script>
</head>
<body>
<div class="container-fluid" id="pcont">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Add User<small></small></h3>
			<ul class="breadcrumb">
				<li>
					<i class="fa fa-link"></i>
					<a href="#">Quick links : </a>
				</li>
				<li>
					<i class="fa fa-home"></i>
					<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				 <li>
					<a href="<s:url action="ManageUsersMenuAction"/>">Manage User</a>
				</li>

			</ul>
			<div class="cl-mcont">
   
				 <div class="content">
					<form class="form-horizontal group-border-dashed" style="border-radius: 0px;" name="add_new_user" id="add_new_user" 
						data-validate="parsley" action="AddNewUserSaveAction" 
						autocomplete='off'>

							 <div class="form-group">
									<label class="col-sm-3 control-label">User Name</label>
									<div class="col-sm-6">
									<input type="text" name="userName" id="userName" maxlength="30" parsley-trigger="change" data-required="true" required placeholder="Enter User name" onkeyup="userDupCheck();" class="form-control" >
									<span id="displayuserName"></span>
									</div>
									</div> 
													<div class="form-group">
									<label class="col-sm-3 control-label">User Title </label>
									<div class="col-sm-6">
									<s:radio label="Answer" id="userTitle" name="userTitle" list="#{'Mr':'Mr','Mrs':'Mrs','Miss':'Miss'}" value="1" cssClass="parsley-validated radioInput"/>
									</div>
									</div> 

									<div class="form-group">
									<label class="col-sm-3 control-label">First Name</label>
									<div class="col-sm-6">
									<input type="text" name="firstName" required parsley-type="alphanum" id="firstName" maxlength="30" parsley-trigger="change" data-required="true" required placeholder="Enter First name" class="form-control">
									</div>
									</div> 

											<div class="form-group">
									<label class="col-sm-3 control-label">Last Name</label>
									<div class="col-sm-6">
									<input type="text" name="lastName" required parsley-type="alphanum" id="lastName" maxlength="30" parsley-trigger="change" data-required="true" required placeholder="Enter Last name" class="form-control">
									</div>
									</div> 

									<div class="form-group">
									<label class="col-sm-3 control-label">Password</label>
									<div class="col-sm-6">
									<input type="password" name="userPassword" id="userPassword"  parsley-trigger="change" parsley-minlength="6" data-required="true" required placeholder="Password" class="form-control" data-equalto="#userPassword" data-required-message="password is must">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-sm-3 control-label">Confirm Password</label>
									<div class="col-sm-6">
									<input parsley-equalto="#userPassword" type="password" name="confirmPassword" parsley-trigger="change" data-required="true" required placeholder="Confirm Password" class="form-control" data-required-message="password is must" data-error-message="Password do not match">
									</div>
									</div> 

									<div class="form-group">
									<label class="col-sm-3 control-label">Gender</label>
									<div class="col-sm-6">
									<s:radio label="Answer" id="gender" name="gender" list="#{'Male':'Male','Female':'Female'}" value="1" cssClass="parsley-validated radioInput" />
									</div>
									</div>

									<div class="form-group">
									<label class="col-sm-3 control-label">Email Id</label>
									<div class="col-sm-6">
									<input type="text" name="emailId" id="emailId" parsley-type="email" parsley-trigger="change" required placeholder="Email Id" class="form-control" data-error-message="Enter valid email Id">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-sm-3 control-label">Contact No</label>
									<div class="col-sm-6">
									<input type="text" name="contactNo" id="contactNo" maxlength="30" parsley-trigger="change" required parsley-type="phone" required placeholder="(XXX) XXXX XXX" class="form-control">
									</div>
									</div> 

									<div class="form-group">
									<label class="col-sm-3 control-label">Role</label>
									<div class="col-sm-6">
									<s:select id="roleId" name="roleId" required="true" parsley-error-message="Please select Role." parsley-required="true" list="allRoleListG" listValue="roleName" listKey="roleId" headerKey="" headerValue="Select Role" parsley-trigger="change" cssClass="form-control"/>
									</div>
									</div>

									<div class="form-group"> 
										<div class="col-sm-offset-2 col-sm-10">         
										<button id="addUser" class="btn btn-primary" type="submit" onclick="javascript:if($('#add_new_user').parsley('validate')==true){var f = document.forms['add_new_user'];f.action='AddNewUserSaveAction'; f.submit()}">Submit</button>
										<button class="btn btn-default" onclick="javascript:var f = document.forms['add_new_user'];f.action='ManageUsersMenuAction';f.submit();">Cancel</button>
										</div>
								  </div>
						</form>    
				</div><!-- End of content -->
			</div><!-- End of cl-mcont -->
		</div>
	</div>
</div>

<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->

</body>

</html>
