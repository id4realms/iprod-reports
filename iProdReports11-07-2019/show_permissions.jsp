<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Show Permission";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
</head>
<body>		
<div class="container-fluid" id="pcont">
<div class="row-fluid">
<div class="span12">
<h3 class="page-title">Show Permissions <small>Permissions details</small></h3>
<ul class="breadcrumb">
	<li>
		<i class="fa fa-link"></i>
		<a href="#">Quick links : </a>
	</li>
	<li>
		<i class="fa fa-home"></i>
		<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
		<i class="fa fa-arrow-right"></i>
	</li>
	<a href="<s:url action="ManageRoleAction"/>">Manage Role</a>

</ul>
  <!--<div class="page-head">-->
<div class="eventmessages">
	<s:if test="hasActionMessages()">
	<div class="alert alert-success alert-white rounded">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<div class="icon"><i class="fa fa-check"></i></div>
					<strong>Success!</strong> <s:iterator value="actionMessages">
																		<s:property /><br>
																	  </s:iterator>
	</div>					
	</s:if>
	<s:if test="hasActionErrors()">
		<div class="alert alert-danger alert-white rounded" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="fa fa-times-circle"></i><strong>Error!</strong><s:iterator value="actionErrors">
																			<s:property />
																		  </s:iterator>
		</div>
	</s:if>
</div>
<div class="cl-mcont">		
						<h3>Permissions for <s:property value="roleName" /></h3>
						<div class="content">
						
							<div class="table-responsive">
			 <table class="table table-bordered">
	 <thead>
		<tr>
		<th>MODULE NAME</th>
		 <s:iterator value="operationListG" var="operatorListGVar" status="operatorListGStatus">
		<th><s:property value="operationDescription"/></th> 
		</s:iterator>
		</tr>
	</thead>
	<tbody class="assemblyqueueTbody">
				<s:iterator value="moduleListG" var="moduleListGVar" status="moduleListGStatus">
				<input type="hidden" name='roleModulePerms[<s:property value="%{#moduleListGStatus.index}" />].moduleId' value="<s:property value='#moduleListGVar.moduleId'/>"/>
					<tr>
					<td class="span4"><s:property value="moduleName"/></td>					
					<s:iterator value="moduleWiseOperationPermissionsListG" var="moduleWiseOperationPermissionsListGVar" status="moduleWiseOperationPermissionsListGStatus">
					 <s:if test="#moduleWiseOperationPermissionsListGVar.moduleId == #moduleListGVar.moduleId">
				
						<s:if test="#moduleWiseOperationPermissionsListGVar.isAllowed == 1">
						<td class="span4">
							<img src="images/yes.png" width="20" height="20" border="0" name="createPer" />
						</td>
						</s:if>
						<s:else>
						<td class="span4">
							<img src="images/no.png" width="20" height="20" border="0" name="createPer" />
							</td>
						</s:else>
					</s:if>
					</s:iterator>
					</tr>
				</s:iterator>
	</tbody>
  </table>						
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>

<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->


	
</body>
</html>
