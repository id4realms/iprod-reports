package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodModuleOperationMasterDao;
import com.id4.iprod.dto.IprodModuleOperationMaster;
import com.id4.iprod.dto.IprodModuleOperationMasterPk;
import com.id4.iprod.exceptions.IprodModuleOperationMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodModuleOperationMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodModuleOperationMasterPk
	 */
	public IprodModuleOperationMasterPk insert(IprodModuleOperationMaster dto);

	/** 
	 * Updates a single row in the iprod_module_operation_master table.
	 */
	public void update(IprodModuleOperationMasterPk pk, IprodModuleOperationMaster dto) throws IprodModuleOperationMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_module_operation_master table.
	 */
	public void delete(IprodModuleOperationMasterPk pk) throws IprodModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'MODULE_ID = :moduleId AND OPERATION_ID = :operationId'.
	 */
	public IprodModuleOperationMaster findByPrimaryKey(Integer moduleId, Integer operationId) throws IprodModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria ''.
	 */
	public List<IprodModuleOperationMaster> findAll() throws IprodModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	public List<IprodModuleOperationMaster> findWhereModuleIdEquals(Integer moduleId) throws IprodModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public List<IprodModuleOperationMaster> findWhereOperationIdEquals(Integer operationId) throws IprodModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'IS_APPLICABLE = :isApplicable'.
	 */
	public List<IprodModuleOperationMaster> findWhereIsApplicableEquals(Short isApplicable) throws IprodModuleOperationMasterDaoException;

	/** 
	 * Returns the rows from the iprod_module_operation_master table that matches the specified primary-key value.
	 */
	public IprodModuleOperationMaster findByPrimaryKey(IprodModuleOperationMasterPk pk) throws IprodModuleOperationMasterDaoException;

}
