package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodReportsLicenseDao;
import com.id4.iprod.dto.IprodReportsLicense;
import com.id4.iprod.dto.IprodReportsLicensePk;
import com.id4.iprod.exceptions.IprodReportsLicenseDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsLicenseDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsLicensePk
	 */
	public IprodReportsLicensePk insert(IprodReportsLicense dto);

	/** 
	 * Updates a single row in the iprod_reports_license table.
	 */
	public void update(IprodReportsLicensePk pk, IprodReportsLicense dto) throws IprodReportsLicenseDaoException;

	/** 
	 * Deletes a single row in the iprod_reports_license table.
	 */
	public void delete(IprodReportsLicensePk pk) throws IprodReportsLicenseDaoException;

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'ID = :id'.
	 */
	public IprodReportsLicense findByPrimaryKey(int id) throws IprodReportsLicenseDaoException;

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria ''.
	 */
	public List<IprodReportsLicense> findAll() throws IprodReportsLicenseDaoException;

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'ID = :id'.
	 */
	public List<IprodReportsLicense> findWhereIdEquals(int id) throws IprodReportsLicenseDaoException;

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'PRODUCT_KEY = :productKey'.
	 */
	public List<IprodReportsLicense> findWhereProductKeyEquals(String productKey) throws IprodReportsLicenseDaoException;

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'E_MAIL = :eMail'.
	 */
	public List<IprodReportsLicense> findWhereEMailEquals(String eMail) throws IprodReportsLicenseDaoException;

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'COMPANY_NAME = :companyName'.
	 */
	public List<IprodReportsLicense> findWhereCompanyNameEquals(String companyName) throws IprodReportsLicenseDaoException;

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'KEY_VALUE = :keyValue'.
	 */
	public List<IprodReportsLicense> findWhereKeyValueEquals(String keyValue) throws IprodReportsLicenseDaoException;

	/** 
	 * Returns the rows from the iprod_reports_license table that matches the specified primary-key value.
	 */
	public IprodReportsLicense findByPrimaryKey(IprodReportsLicensePk pk) throws IprodReportsLicenseDaoException;

}
