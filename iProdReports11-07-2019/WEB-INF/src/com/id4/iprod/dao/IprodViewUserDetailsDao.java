package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodViewUserDetailsDao;
import com.id4.iprod.dto.IprodViewUserDetails;
import com.id4.iprod.exceptions.IprodViewUserDetailsDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodViewUserDetailsDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodViewUserDetails dto);

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria ''.
	 */
	public List<IprodViewUserDetails> findAll() throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_ID = :userId'.
	 */
	public List<IprodViewUserDetails> findWhereUserIdEquals(int userId) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_NAME = :userName'.
	 */
	public List<IprodViewUserDetails> findWhereUserNameEquals(String userName) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_TITLE = :userTitle'.
	 */
	public List<IprodViewUserDetails> findWhereUserTitleEquals(String userTitle) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'FIRST_NAME = :firstName'.
	 */
	public List<IprodViewUserDetails> findWhereFirstNameEquals(String firstName) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'LAST_NAME = :lastName'.
	 */
	public List<IprodViewUserDetails> findWhereLastNameEquals(String lastName) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'GENDER = :gender'.
	 */
	public List<IprodViewUserDetails> findWhereGenderEquals(String gender) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'EMAIL_ID = :emailId'.
	 */
	public List<IprodViewUserDetails> findWhereEmailIdEquals(String emailId) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'CONTACT_NO = :contactNo'.
	 */
	public List<IprodViewUserDetails> findWhereContactNoEquals(String contactNo) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_PASSWORD = :userPassword'.
	 */
	public List<IprodViewUserDetails> findWhereUserPasswordEquals(String userPassword) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'LAST_LOGIN_DETAILS = :lastLoginDetails'.
	 */
	public List<IprodViewUserDetails> findWhereLastLoginDetailsEquals(String lastLoginDetails) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_IS_DELETED = :userIsDeleted'.
	 */
	public List<IprodViewUserDetails> findWhereUserIsDeletedEquals(short userIsDeleted) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'ROLE_ID = :roleId'.
	 */
	public List<IprodViewUserDetails> findWhereRoleIdEquals(int roleId) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'ROLE_NAME = :roleName'.
	 */
	public List<IprodViewUserDetails> findWhereRoleNameEquals(String roleName) throws IprodViewUserDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'ROLE_DESC = :roleDesc'.
	 */
	public List<IprodViewUserDetails> findWhereRoleDescEquals(String roleDesc) throws IprodViewUserDetailsDaoException;

		
	public List<IprodViewUserDetails> findWhereRoleIdNotEquals(int roleId) throws IprodViewUserDetailsDaoException;

	public List<IprodViewUserDetails> findWhereIsDeletedAndRoleExceptAdminAndPlantIdEquals(short isDeleted, int roleId) throws IprodViewUserDetailsDaoException;

	public List<IprodViewUserDetails> findWhereRoleIdAndIsDeletedEqualsAndPlantIdEquals(int roleId, short isDeleted)throws IprodViewUserDetailsDaoException;

	public List<IprodViewUserDetails> findWhereUserNameEqualsAndIsDeleted(String userName, short isDeleted) throws IprodViewUserDetailsDaoException;


}
