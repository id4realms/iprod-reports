package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodUserMasterDao;
import com.id4.iprod.dto.IprodUserMaster;
import com.id4.iprod.dto.IprodUserMasterPk;
import com.id4.iprod.exceptions.IprodUserMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodUserMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodUserMasterPk
	 */
	public IprodUserMasterPk insert(IprodUserMaster dto);

	/** 
	 * Updates a single row in the iprod_user_master table.
	 */
	public void update(IprodUserMasterPk pk, IprodUserMaster dto) throws IprodUserMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_user_master table.
	 */
	public void delete(IprodUserMasterPk pk) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_ID = :userId'.
	 */
	public IprodUserMaster findByPrimaryKey(int userId) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria ''.
	 */
	public List<IprodUserMaster> findAll() throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_ID = :userId'.
	 */
	public List<IprodUserMaster> findWhereUserIdEquals(int userId) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_NAME = :userName'.
	 */
	public List<IprodUserMaster> findWhereUserNameEquals(String userName) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_TITLE = :userTitle'.
	 */
	public List<IprodUserMaster> findWhereUserTitleEquals(String userTitle) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'FIRST_NAME = :firstName'.
	 */
	public List<IprodUserMaster> findWhereFirstNameEquals(String firstName) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'LAST_NAME = :lastName'.
	 */
	public List<IprodUserMaster> findWhereLastNameEquals(String lastName) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'GENDER = :gender'.
	 */
	public List<IprodUserMaster> findWhereGenderEquals(String gender) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'EMAIL_ID = :emailId'.
	 */
	public List<IprodUserMaster> findWhereEmailIdEquals(String emailId) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'CONTACT_NO = :contactNo'.
	 */
	public List<IprodUserMaster> findWhereContactNoEquals(String contactNo) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_PASSWORD = :userPassword'.
	 */
	public List<IprodUserMaster> findWhereUserPasswordEquals(String userPassword) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'LAST_LOGIN_DETAILS = :lastLoginDetails'.
	 */
	public List<IprodUserMaster> findWhereLastLoginDetailsEquals(String lastLoginDetails) throws IprodUserMasterDaoException;

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	public List<IprodUserMaster> findWhereIsDeletedEquals(short isDeleted) throws IprodUserMasterDaoException;

	/** 
	 * Returns the rows from the iprod_user_master table that matches the specified primary-key value.
	 */
	public IprodUserMaster findByPrimaryKey(IprodUserMasterPk pk) throws IprodUserMasterDaoException;
	
	public void updatePassword(IprodUserMasterPk pk, IprodUserMaster dto) throws IprodUserMasterDaoException;
	
	public List<IprodUserMaster> findWhereUserNameEqualsAndIsDeleted(String userName,short isDeleted) throws IprodUserMasterDaoException;
	
	public void updateWhereUserId(String userName,String userTitle,String firstName,String lastName,String gender,String emailId,String contact,int userId) throws IprodUserMasterDaoException;

}
