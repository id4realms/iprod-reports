package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodOperationMasterDao;
import com.id4.iprod.dto.IprodOperationMaster;
import com.id4.iprod.dto.IprodOperationMasterPk;
import com.id4.iprod.exceptions.IprodOperationMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodOperationMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodOperationMaster>, IprodOperationMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodOperationMasterPk
	 */
	public IprodOperationMasterPk insert(IprodOperationMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED ) VALUES ( ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getOperationName(),dto.getOperationDescription(),dto.getOperationCdatettime(),dto.getOperationMdatetime(),dto.getIsDeleted()} );
		IprodOperationMasterPk pk = new IprodOperationMasterPk();
		pk.setOperationId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_operation_master table.
	 */
	public void update(IprodOperationMasterPk pk, IprodOperationMaster dto) throws IprodOperationMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET OPERATION_NAME = ?, OPERATION_DESCRIPTION = ?, OPERATION_CDATETTIME = ?, OPERATION_MDATETIME = ?, IS_DELETED = ? WHERE OPERATION_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getOperationName(),dto.getOperationDescription(),dto.getOperationCdatettime(),dto.getOperationMdatetime(),dto.getIsDeleted(),pk.getOperationId() } );
	}

	/** 
	 * Deletes a single row in the iprod_operation_master table.
	 */
	@Transactional
	public void delete(IprodOperationMasterPk pk) throws IprodOperationMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE OPERATION_ID = ?",pk.getOperationId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodOperationMaster
	 */
	public IprodOperationMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodOperationMaster dto = new IprodOperationMaster();
		dto.setOperationId( rs.getInt( 1 ) );
		dto.setOperationName( rs.getString( 2 ) );
		dto.setOperationDescription( rs.getString( 3 ) );
		dto.setOperationCdatettime( rs.getString( 4 ) );
		dto.setOperationMdatetime( rs.getString( 5 ) );
		dto.setIsDeleted( rs.getShort( 6 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_operation_master";
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public IprodOperationMaster findByPrimaryKey(int operationId) throws IprodOperationMasterDaoException
	{
		try {
			List<IprodOperationMaster> list = jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " WHERE OPERATION_ID = ?", this,operationId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodOperationMaster> findAll() throws IprodOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " ORDER BY OPERATION_ID", this);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public List<IprodOperationMaster> findWhereOperationIdEquals(int operationId) throws IprodOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " WHERE OPERATION_ID = ? ORDER BY OPERATION_ID", this,operationId);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_NAME = :operationName'.
	 */
	@Transactional
	public List<IprodOperationMaster> findWhereOperationNameEquals(String operationName) throws IprodOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " WHERE OPERATION_NAME = ? ORDER BY OPERATION_NAME", this,operationName);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_DESCRIPTION = :operationDescription'.
	 */
	@Transactional
	public List<IprodOperationMaster> findWhereOperationDescriptionEquals(String operationDescription) throws IprodOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " WHERE OPERATION_DESCRIPTION = ? ORDER BY OPERATION_DESCRIPTION", this,operationDescription);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_CDATETTIME = :operationCdatettime'.
	 */
	@Transactional
	public List<IprodOperationMaster> findWhereOperationCdatettimeEquals(String operationCdatettime) throws IprodOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " WHERE OPERATION_CDATETTIME = ? ORDER BY OPERATION_CDATETTIME", this,operationCdatettime);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_MDATETIME = :operationMdatetime'.
	 */
	@Transactional
	public List<IprodOperationMaster> findWhereOperationMdatetimeEquals(String operationMdatetime) throws IprodOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " WHERE OPERATION_MDATETIME = ? ORDER BY OPERATION_MDATETIME", this,operationMdatetime);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	@Transactional
	public List<IprodOperationMaster> findWhereIsDeletedEquals(short isDeleted) throws IprodOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, OPERATION_CDATETTIME, OPERATION_MDATETIME, IS_DELETED FROM " + getTableName() + " WHERE IS_DELETED = ? ORDER BY IS_DELETED", this,isDeleted);
		}
		catch (Exception e) {
			throw new IprodOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_operation_master table that matches the specified primary-key value.
	 */
	public IprodOperationMaster findByPrimaryKey(IprodOperationMasterPk pk) throws IprodOperationMasterDaoException
	{
		return findByPrimaryKey( pk.getOperationId() );
	}

}
