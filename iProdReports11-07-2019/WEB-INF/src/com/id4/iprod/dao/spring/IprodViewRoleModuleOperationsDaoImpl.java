package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodViewRoleModuleOperationsDao;
import com.id4.iprod.dto.IprodViewRoleModuleOperations;
import com.id4.iprod.exceptions.IprodRoleModuleOperationsDaoException;
import com.id4.iprod.exceptions.IprodViewRoleModuleOperationsDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodViewRoleModuleOperationsDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodViewRoleModuleOperations>, IprodViewRoleModuleOperationsDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodViewRoleModuleOperations dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getRoleId(),dto.getRoleName(),dto.getModuleId(),dto.getModuleName(),dto.getOperationId(),dto.getOperationName(),dto.getOperationDescription(),dto.getIsAllowed(),dto.getIsApplicable()} );
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodViewRoleModuleOperations
	 */
	public IprodViewRoleModuleOperations mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodViewRoleModuleOperations dto = new IprodViewRoleModuleOperations();
		dto.setRoleId( rs.getInt( 1 ) );
		dto.setRoleName( rs.getString( 2 ) );
		dto.setModuleId( rs.getInt( 3 ) );
		dto.setModuleName( rs.getString( 4 ) );
		dto.setOperationId( rs.getInt( 5 ) );
		dto.setOperationName( rs.getString( 6 ) );
		dto.setOperationDescription( rs.getString( 7 ) );
		dto.setIsAllowed( rs.getShort( 8 ) );
		dto.setIsApplicable( rs.getShort( 9 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_view_role_module_operations";
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria ''.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findAll() throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + "", this);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'ROLE_ID = :roleId'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereRoleIdEquals(int roleId) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE ROLE_ID = ? ORDER BY ROLE_ID", this,roleId);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'ROLE_NAME = :roleName'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereRoleNameEquals(String roleName) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE ROLE_NAME = ? ORDER BY ROLE_NAME", this,roleName);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereModuleIdEquals(int moduleId) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE MODULE_ID = ? ORDER BY MODULE_ID", this,moduleId);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'MODULE_NAME = :moduleName'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereModuleNameEquals(String moduleName) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE MODULE_NAME = ? ORDER BY MODULE_NAME", this,moduleName);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereOperationIdEquals(int operationId) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE OPERATION_ID = ? ORDER BY OPERATION_ID", this,operationId);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'OPERATION_NAME = :operationName'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereOperationNameEquals(String operationName) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE OPERATION_NAME = ? ORDER BY OPERATION_NAME", this,operationName);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'OPERATION_DESCRIPTION = :operationDescription'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereOperationDescriptionEquals(String operationDescription) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE OPERATION_DESCRIPTION = ? ORDER BY OPERATION_DESCRIPTION", this,operationDescription);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'IS_ALLOWED = :isAllowed'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereIsAllowedEquals(short isAllowed) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE IS_ALLOWED = ? ORDER BY IS_ALLOWED", this,isAllowed);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'IS_APPLICABLE = :isApplicable'.
	 */
	@Transactional
	public List<IprodViewRoleModuleOperations> findWhereIsApplicableEquals(short isApplicable) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE IS_APPLICABLE = ? ORDER BY IS_APPLICABLE", this,isApplicable);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}
	
	
		public List<IprodViewRoleModuleOperations> findWhereRoleIdModuleNameAndOperationNameEquals(int roleId,String moduleName,String operationName) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE ROLE_ID = ? AND MODULE_NAME = ? AND OPERATION_NAME =? ORDER BY ROLE_ID", this,roleId,moduleName,operationName);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}
	
	public List<IprodViewRoleModuleOperations> findWhereRoleIdModuleNameEquals(int roleId,String moduleName) throws IprodViewRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_ALLOWED, IS_APPLICABLE FROM " + getTableName() + " WHERE ROLE_ID = ? AND MODULE_NAME = ? ORDER BY ROLE_ID", this,roleId,moduleName);
		}
		catch (Exception e) {
			throw new IprodViewRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}
	
	
	
	

}
