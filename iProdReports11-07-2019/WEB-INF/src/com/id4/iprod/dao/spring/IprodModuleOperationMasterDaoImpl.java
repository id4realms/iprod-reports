package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodModuleOperationMasterDao;
import com.id4.iprod.dto.IprodModuleOperationMaster;
import com.id4.iprod.dto.IprodModuleOperationMasterPk;
import com.id4.iprod.exceptions.IprodModuleOperationMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodModuleOperationMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodModuleOperationMaster>, IprodModuleOperationMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodModuleOperationMasterPk
	 */
	public IprodModuleOperationMasterPk insert(IprodModuleOperationMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( MODULE_ID, OPERATION_ID, IS_APPLICABLE ) VALUES ( ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getModuleId(),dto.getOperationId(),dto.getIsApplicable()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the iprod_module_operation_master table.
	 */
	public void update(IprodModuleOperationMasterPk pk, IprodModuleOperationMaster dto) throws IprodModuleOperationMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET MODULE_ID = ?, OPERATION_ID = ?, IS_APPLICABLE = ? WHERE MODULE_ID = ? AND OPERATION_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getModuleId(),dto.getOperationId(),dto.getIsApplicable(),pk.getModuleId(),pk.getOperationId() } );
	}

	/** 
	 * Deletes a single row in the iprod_module_operation_master table.
	 */
	@Transactional
	public void delete(IprodModuleOperationMasterPk pk) throws IprodModuleOperationMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE MODULE_ID = ? AND OPERATION_ID = ?",pk.getModuleId(),pk.getOperationId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodModuleOperationMaster
	 */
	public IprodModuleOperationMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodModuleOperationMaster dto = new IprodModuleOperationMaster();
		dto.setModuleId( new Integer( rs.getInt(1) ) );
		dto.setOperationId( new Integer( rs.getInt(2) ) );
		dto.setIsApplicable( new Short( rs.getShort(3) ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_module_operation_master";
	}

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'MODULE_ID = :moduleId AND OPERATION_ID = :operationId'.
	 */
	@Transactional
	public IprodModuleOperationMaster findByPrimaryKey(Integer moduleId, Integer operationId) throws IprodModuleOperationMasterDaoException
	{
		try {
			List<IprodModuleOperationMaster> list = jdbcTemplate.query("SELECT MODULE_ID, OPERATION_ID, IS_APPLICABLE FROM " + getTableName() + " WHERE MODULE_ID = ? AND OPERATION_ID = ?", this,moduleId,operationId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodModuleOperationMaster> findAll() throws IprodModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, OPERATION_ID, IS_APPLICABLE FROM " + getTableName() + " ORDER BY MODULE_ID, OPERATION_ID", this);
		}
		catch (Exception e) {
			throw new IprodModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	@Transactional
	public List<IprodModuleOperationMaster> findWhereModuleIdEquals(Integer moduleId) throws IprodModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, OPERATION_ID, IS_APPLICABLE FROM " + getTableName() + " WHERE MODULE_ID = ? ORDER BY MODULE_ID", this,moduleId);
		}
		catch (Exception e) {
			throw new IprodModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public List<IprodModuleOperationMaster> findWhereOperationIdEquals(Integer operationId) throws IprodModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, OPERATION_ID, IS_APPLICABLE FROM " + getTableName() + " WHERE OPERATION_ID = ? ORDER BY OPERATION_ID", this,operationId);
		}
		catch (Exception e) {
			throw new IprodModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_module_operation_master table that match the criteria 'IS_APPLICABLE = :isApplicable'.
	 */
	@Transactional
	public List<IprodModuleOperationMaster> findWhereIsApplicableEquals(Short isApplicable) throws IprodModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, OPERATION_ID, IS_APPLICABLE FROM " + getTableName() + " WHERE IS_APPLICABLE = ? ORDER BY IS_APPLICABLE", this,isApplicable);
		}
		catch (Exception e) {
			throw new IprodModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_module_operation_master table that matches the specified primary-key value.
	 */
	public IprodModuleOperationMaster findByPrimaryKey(IprodModuleOperationMasterPk pk) throws IprodModuleOperationMasterDaoException
	{
		return findByPrimaryKey( pk.getModuleId(), pk.getOperationId() );
	}

}
