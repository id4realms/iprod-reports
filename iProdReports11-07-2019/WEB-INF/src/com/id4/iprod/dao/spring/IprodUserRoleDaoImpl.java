package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodUserRoleDao;
import com.id4.iprod.dto.IprodUserRole;
import com.id4.iprod.dto.IprodUserRolePk;
import com.id4.iprod.exceptions.IprodUserRoleDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodUserRoleDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodUserRole>, IprodUserRoleDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodUserRolePk
	 */
	public IprodUserRolePk insert(IprodUserRole dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( USER_ID, ROLE_ID ) VALUES ( ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getUserId(),dto.getRoleId()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the iprod_user_role table.
	 */
	public void update(IprodUserRolePk pk, IprodUserRole dto) throws IprodUserRoleDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET USER_ID = ?, ROLE_ID = ? WHERE USER_ID = ? AND ROLE_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getUserId(),dto.getRoleId(),pk.getUserId(),pk.getRoleId() } );
	}

	/** 
	 * Deletes a single row in the iprod_user_role table.
	 */
	@Transactional
	public void delete(IprodUserRolePk pk) throws IprodUserRoleDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE USER_ID = ? AND ROLE_ID = ?",pk.getUserId(),pk.getRoleId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodUserRole
	 */
	public IprodUserRole mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodUserRole dto = new IprodUserRole();
		dto.setUserId( rs.getInt( 1 ) );
		dto.setRoleId( rs.getInt( 2 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_user_role";
	}

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria 'USER_ID = :userId AND ROLE_ID = :roleId'.
	 */
	@Transactional
	public IprodUserRole findByPrimaryKey(int userId, int roleId) throws IprodUserRoleDaoException
	{
		try {
			List<IprodUserRole> list = jdbcTemplate.query("SELECT USER_ID, ROLE_ID FROM " + getTableName() + " WHERE USER_ID = ? AND ROLE_ID = ?", this,userId,roleId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodUserRoleDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria ''.
	 */
	@Transactional
	public List<IprodUserRole> findAll() throws IprodUserRoleDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, ROLE_ID FROM " + getTableName() + " ORDER BY USER_ID, ROLE_ID", this);
		}
		catch (Exception e) {
			throw new IprodUserRoleDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria 'USER_ID = :userId'.
	 */
	@Transactional
	public List<IprodUserRole> findWhereUserIdEquals(int userId) throws IprodUserRoleDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, ROLE_ID FROM " + getTableName() + " WHERE USER_ID = ? ORDER BY USER_ID", this,userId);
		}
		catch (Exception e) {
			throw new IprodUserRoleDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria 'ROLE_ID = :roleId'.
	 */
	@Transactional
	public List<IprodUserRole> findWhereRoleIdEquals(int roleId) throws IprodUserRoleDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, ROLE_ID FROM " + getTableName() + " WHERE ROLE_ID = ? ORDER BY ROLE_ID", this,roleId);
		}
		catch (Exception e) {
			throw new IprodUserRoleDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_user_role table that matches the specified primary-key value.
	 */
	public IprodUserRole findByPrimaryKey(IprodUserRolePk pk) throws IprodUserRoleDaoException
	{
		return findByPrimaryKey( pk.getUserId(), pk.getRoleId() );
	}

	public void updateRoleIdWhereUserIdIs(int roleId,int userId) throws IprodUserRoleDaoException
	{
		jdbcTemplate.update("UPDATE " + getTableName() + " SET ROLE_ID = ? WHERE USER_ID = ?", roleId,userId);
	}
	

}
