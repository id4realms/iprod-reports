package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodReportsLicenseDao;
import com.id4.iprod.dto.IprodReportsLicense;
import com.id4.iprod.dto.IprodReportsLicensePk;
import com.id4.iprod.exceptions.IprodReportsLicenseDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsLicenseDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsLicense>, IprodReportsLicenseDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsLicensePk
	 */
	public IprodReportsLicensePk insert(IprodReportsLicense dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE ) VALUES ( ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getProductKey(),dto.getEMail(),dto.getCompanyName(),dto.getKeyValue()} );
		IprodReportsLicensePk pk = new IprodReportsLicensePk();
		pk.setId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_reports_license table.
	 */
	public void update(IprodReportsLicensePk pk, IprodReportsLicense dto) throws IprodReportsLicenseDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET PRODUCT_KEY = ?, E_MAIL = ?, COMPANY_NAME = ?, KEY_VALUE = ? WHERE ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getProductKey(),dto.getEMail(),dto.getCompanyName(),dto.getKeyValue(),pk.getId() } );
	}

	/** 
	 * Deletes a single row in the iprod_reports_license table.
	 */
	@Transactional
	public void delete(IprodReportsLicensePk pk) throws IprodReportsLicenseDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE ID = ?",pk.getId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsLicense
	 */
	public IprodReportsLicense mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsLicense dto = new IprodReportsLicense();
		dto.setId( rs.getInt( 1 ) );
		dto.setProductKey( rs.getString( 2 ) );
		dto.setEMail( rs.getString( 3 ) );
		dto.setCompanyName( rs.getString( 4 ) );
		dto.setKeyValue( rs.getString( 5 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_license";
	}

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'ID = :id'.
	 */
	@Transactional
	public IprodReportsLicense findByPrimaryKey(int id) throws IprodReportsLicenseDaoException
	{
		try {
			List<IprodReportsLicense> list = jdbcTemplate.query("SELECT ID, PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE FROM " + getTableName() + " WHERE ID = ?", this,id);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodReportsLicenseDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsLicense> findAll() throws IprodReportsLicenseDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ID, PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE FROM " + getTableName() + " ORDER BY ID", this);
		}
		catch (Exception e) {
			throw new IprodReportsLicenseDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'ID = :id'.
	 */
	@Transactional
	public List<IprodReportsLicense> findWhereIdEquals(int id) throws IprodReportsLicenseDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ID, PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE FROM " + getTableName() + " WHERE ID = ? ORDER BY ID", this,id);
		}
		catch (Exception e) {
			throw new IprodReportsLicenseDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'PRODUCT_KEY = :productKey'.
	 */
	@Transactional
	public List<IprodReportsLicense> findWhereProductKeyEquals(String productKey) throws IprodReportsLicenseDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ID, PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE FROM " + getTableName() + " WHERE PRODUCT_KEY = ? ORDER BY PRODUCT_KEY", this,productKey);
		}
		catch (Exception e) {
			throw new IprodReportsLicenseDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'E_MAIL = :eMail'.
	 */
	@Transactional
	public List<IprodReportsLicense> findWhereEMailEquals(String eMail) throws IprodReportsLicenseDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ID, PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE FROM " + getTableName() + " WHERE E_MAIL = ? ORDER BY E_MAIL", this,eMail);
		}
		catch (Exception e) {
			throw new IprodReportsLicenseDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'COMPANY_NAME = :companyName'.
	 */
	@Transactional
	public List<IprodReportsLicense> findWhereCompanyNameEquals(String companyName) throws IprodReportsLicenseDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ID, PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE FROM " + getTableName() + " WHERE COMPANY_NAME = ? ORDER BY COMPANY_NAME", this,companyName);
		}
		catch (Exception e) {
			throw new IprodReportsLicenseDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_license table that match the criteria 'KEY_VALUE = :keyValue'.
	 */
	@Transactional
	public List<IprodReportsLicense> findWhereKeyValueEquals(String keyValue) throws IprodReportsLicenseDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ID, PRODUCT_KEY, E_MAIL, COMPANY_NAME, KEY_VALUE FROM " + getTableName() + " WHERE KEY_VALUE = ? ORDER BY KEY_VALUE", this,keyValue);
		}
		catch (Exception e) {
			System.out.println("Connection " + e.getMessage());
			throw new IprodReportsLicenseDaoException("Query failed", e);
			
			
		}
		
	}

	/** 
	 * Returns the rows from the iprod_reports_license table that matches the specified primary-key value.
	 */
	public IprodReportsLicense findByPrimaryKey(IprodReportsLicensePk pk) throws IprodReportsLicenseDaoException
	{
		return findByPrimaryKey( pk.getId() );
	}

}
