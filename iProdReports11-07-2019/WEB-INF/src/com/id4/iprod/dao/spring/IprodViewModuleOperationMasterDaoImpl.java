package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodViewModuleOperationMasterDao;
import com.id4.iprod.dto.IprodViewModuleOperationMaster;
import com.id4.iprod.exceptions.IprodViewModuleOperationMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodViewModuleOperationMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodViewModuleOperationMaster>, IprodViewModuleOperationMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodViewModuleOperationMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE ) VALUES ( ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getModuleId(),dto.getModuleName(),dto.getOperationId(),dto.getOperationName(),dto.getOperationDescription(),dto.getIsApplicable()} );
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodViewModuleOperationMaster
	 */
	public IprodViewModuleOperationMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodViewModuleOperationMaster dto = new IprodViewModuleOperationMaster();
		dto.setModuleId( rs.getInt( 1 ) );
		dto.setModuleName( rs.getString( 2 ) );
		dto.setOperationId( rs.getInt( 3 ) );
		dto.setOperationName( rs.getString( 4 ) );
		dto.setOperationDescription( rs.getString( 5 ) );
		dto.setIsApplicable( rs.getShort( 6 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_view_module_operation_master";
	}

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodViewModuleOperationMaster> findAll() throws IprodViewModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE FROM " + getTableName() + "", this);
		}
		catch (Exception e) {
			throw new IprodViewModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	@Transactional
	public List<IprodViewModuleOperationMaster> findWhereModuleIdEquals(int moduleId) throws IprodViewModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE FROM " + getTableName() + " WHERE MODULE_ID = ? ORDER BY MODULE_ID", this,moduleId);
		}
		catch (Exception e) {
			throw new IprodViewModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'MODULE_NAME = :moduleName'.
	 */
	@Transactional
	public List<IprodViewModuleOperationMaster> findWhereModuleNameEquals(String moduleName) throws IprodViewModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE FROM " + getTableName() + " WHERE MODULE_NAME = ? ORDER BY MODULE_NAME", this,moduleName);
		}
		catch (Exception e) {
			throw new IprodViewModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public List<IprodViewModuleOperationMaster> findWhereOperationIdEquals(int operationId) throws IprodViewModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE FROM " + getTableName() + " WHERE OPERATION_ID = ? ORDER BY OPERATION_ID", this,operationId);
		}
		catch (Exception e) {
			throw new IprodViewModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'OPERATION_NAME = :operationName'.
	 */
	@Transactional
	public List<IprodViewModuleOperationMaster> findWhereOperationNameEquals(String operationName) throws IprodViewModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE FROM " + getTableName() + " WHERE OPERATION_NAME = ? ORDER BY OPERATION_NAME", this,operationName);
		}
		catch (Exception e) {
			throw new IprodViewModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'OPERATION_DESCRIPTION = :operationDescription'.
	 */
	@Transactional
	public List<IprodViewModuleOperationMaster> findWhereOperationDescriptionEquals(String operationDescription) throws IprodViewModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE FROM " + getTableName() + " WHERE OPERATION_DESCRIPTION = ? ORDER BY OPERATION_DESCRIPTION", this,operationDescription);
		}
		catch (Exception e) {
			throw new IprodViewModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'IS_APPLICABLE = :isApplicable'.
	 */
	@Transactional
	public List<IprodViewModuleOperationMaster> findWhereIsApplicableEquals(short isApplicable) throws IprodViewModuleOperationMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME, OPERATION_ID, OPERATION_NAME, OPERATION_DESCRIPTION, IS_APPLICABLE FROM " + getTableName() + " WHERE IS_APPLICABLE = ? ORDER BY IS_APPLICABLE", this,isApplicable);
		}
		catch (Exception e) {
			throw new IprodViewModuleOperationMasterDaoException("Query failed", e);
		}
		
	}

}
