package com.id4.iprod.dao.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.transaction.annotation.Transactional;

import com.id4.iprod.dto.IprodOperationLoggerHistory;
import com.id4.iprod.dao.IprodOperationLoggerHistoryDao;
import com.id4.iprod.dto.IprodOperationLoggerHistoryPk;
import com.id4.iprod.exceptions.IprodOperationLoggerHistoryDaoException;

public class IprodOperationLoggerHistoryDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodOperationLoggerHistory>, IprodOperationLoggerHistoryDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodOperationLoggerHistoryPk
	 */
	public IprodOperationLoggerHistoryPk insert(IprodOperationLoggerHistory dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME ) VALUES ( ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getOperationPageSource(),dto.getComments(),dto.getUserId(),dto.getUsername(),dto.getOperationLoggerCdatetime(),dto.getOperationLoggerMdatetime()} );
		IprodOperationLoggerHistoryPk pk = new IprodOperationLoggerHistoryPk();
		pk.setOperationId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_operation_logger_history table.
	 */
	public void update(IprodOperationLoggerHistoryPk pk, IprodOperationLoggerHistory dto) throws IprodOperationLoggerHistoryDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET OPERATION_PAGE_SOURCE = ?, COMMENTS = ?, USER_ID = ?, USERNAME = ?, OPERATION_LOGGER_CDATETIME = ?, OPERATION_LOGGER_MDATETIME = ? WHERE OPERATION_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getOperationPageSource(),dto.getComments(),dto.getUserId(),dto.getUsername(),dto.getOperationLoggerCdatetime(),dto.getOperationLoggerMdatetime(),pk.getOperationId() } );
	}

	/** 
	 * Deletes a single row in the iprod_operation_logger_history table.
	 */
	@Transactional
	public void delete(IprodOperationLoggerHistoryPk pk) throws IprodOperationLoggerHistoryDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE OPERATION_ID = ?",pk.getOperationId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodOperationLoggerHistory
	 */
	public IprodOperationLoggerHistory mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodOperationLoggerHistory dto = new IprodOperationLoggerHistory();
		dto.setOperationId( rs.getInt( 1 ) );
		dto.setOperationPageSource( rs.getString( 2 ) );
		dto.setComments( rs.getString( 3 ) );
		dto.setUserId( rs.getInt( 4 ) );
		if (rs.wasNull()) {
			dto.setUserIdNull( true );
		}
		
		dto.setUsername( rs.getString( 5 ) );
		dto.setOperationLoggerCdatetime( rs.getString( 6 ) );
		dto.setOperationLoggerMdatetime( rs.getString( 7 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_operation_logger_history";
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public IprodOperationLoggerHistory findByPrimaryKey(int operationId) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			List<IprodOperationLoggerHistory> list = jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE OPERATION_ID = ?", this,operationId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria ''.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findAll() throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " ORDER BY OPERATION_ID", this);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findWhereOperationIdEquals(int operationId) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE OPERATION_ID = ? ORDER BY OPERATION_ID", this,operationId);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_PAGE_SOURCE = :operationPageSource'.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findWhereOperationPageSourceEquals(String operationPageSource) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE OPERATION_PAGE_SOURCE = ? ORDER BY OPERATION_PAGE_SOURCE", this,operationPageSource);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'COMMENTS = :comments'.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findWhereCommentsEquals(String comments) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE COMMENTS = ? ORDER BY COMMENTS", this,comments);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'USER_ID = :userId'.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findWhereUserIdEquals(int userId) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE USER_ID = ? ORDER BY USER_ID", this,userId);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'USERNAME = :username'.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findWhereUsernameEquals(String username) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE USERNAME = ? ORDER BY USERNAME", this,username);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_LOGGER_CDATETIME = :operationLoggerCdatetime'.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findWhereOperationLoggerCdatetimeEquals(String operationLoggerCdatetime) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE OPERATION_LOGGER_CDATETIME = ? ORDER BY OPERATION_LOGGER_CDATETIME", this,operationLoggerCdatetime);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_LOGGER_MDATETIME = :operationLoggerMdatetime'.
	 */
	@Transactional
	public List<IprodOperationLoggerHistory> findWhereOperationLoggerMdatetimeEquals(String operationLoggerMdatetime) throws IprodOperationLoggerHistoryDaoException
	{
		try {
			return jdbcTemplate.query("SELECT OPERATION_ID, OPERATION_PAGE_SOURCE, COMMENTS, USER_ID, USERNAME, OPERATION_LOGGER_CDATETIME, OPERATION_LOGGER_MDATETIME FROM " + getTableName() + " WHERE OPERATION_LOGGER_MDATETIME = ? ORDER BY OPERATION_LOGGER_MDATETIME", this,operationLoggerMdatetime);
		}
		catch (Exception e) {
			throw new IprodOperationLoggerHistoryDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_operation_logger_history table that matches the specified primary-key value.
	 */
	public IprodOperationLoggerHistory findByPrimaryKey(IprodOperationLoggerHistoryPk pk) throws IprodOperationLoggerHistoryDaoException
	{
		return findByPrimaryKey( pk.getOperationId() );
	}

}
