package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodUserMasterDao;
import com.id4.iprod.dto.IprodUserMaster;
import com.id4.iprod.dto.IprodUserMasterPk;
import com.id4.iprod.exceptions.IprodUserMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodUserMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodUserMaster>, IprodUserMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodUserMasterPk
	 */
	public IprodUserMasterPk insert(IprodUserMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getUserName(),dto.getUserTitle(),dto.getFirstName(),dto.getLastName(),dto.getGender(),dto.getEmailId(),dto.getContactNo(),dto.getUserPassword(),dto.getLastLoginDetails(),dto.getIsDeleted()} );
		IprodUserMasterPk pk = new IprodUserMasterPk();
		pk.setUserId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_user_master table.
	 */
	public void update(IprodUserMasterPk pk, IprodUserMaster dto) throws IprodUserMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET USER_NAME = ?, USER_TITLE = ?, FIRST_NAME = ?, LAST_NAME = ?, GENDER = ?, EMAIL_ID = ?, CONTACT_NO = ?, USER_PASSWORD = ?, LAST_LOGIN_DETAILS = ?, IS_DELETED = ? WHERE USER_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getUserName(),dto.getUserTitle(),dto.getFirstName(),dto.getLastName(),dto.getGender(),dto.getEmailId(),dto.getContactNo(),dto.getUserPassword(),dto.getLastLoginDetails(),dto.getIsDeleted(),pk.getUserId() } );
	}

	/** 
	 * Deletes a single row in the iprod_user_master table.
	 */
	@Transactional
	public void delete(IprodUserMasterPk pk) throws IprodUserMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE USER_ID = ?",pk.getUserId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodUserMaster
	 */
	public IprodUserMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodUserMaster dto = new IprodUserMaster();
		dto.setUserId( rs.getInt( 1 ) );
		dto.setUserName( rs.getString( 2 ) );
		dto.setUserTitle( rs.getString( 3 ) );
		dto.setFirstName( rs.getString( 4 ) );
		dto.setLastName( rs.getString( 5 ) );
		dto.setGender( rs.getString( 6 ) );
		dto.setEmailId( rs.getString( 7 ) );
		dto.setContactNo( rs.getString( 8 ) );
		dto.setUserPassword( rs.getString( 9 ) );
		dto.setLastLoginDetails( rs.getString( 10 ) );
		dto.setIsDeleted( rs.getShort( 11 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_user_master";
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_ID = :userId'.
	 */
	@Transactional
	public IprodUserMaster findByPrimaryKey(int userId) throws IprodUserMasterDaoException
	{
		try {
			List<IprodUserMaster> list = jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE USER_ID = ?", this,userId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodUserMaster> findAll() throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " ORDER BY USER_ID", this);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_ID = :userId'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereUserIdEquals(int userId) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE USER_ID = ? ORDER BY USER_ID", this,userId);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_NAME = :userName'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereUserNameEquals(String userName) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE USER_NAME = ? ORDER BY USER_NAME", this,userName);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_TITLE = :userTitle'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereUserTitleEquals(String userTitle) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE USER_TITLE = ? ORDER BY USER_TITLE", this,userTitle);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'FIRST_NAME = :firstName'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereFirstNameEquals(String firstName) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE FIRST_NAME = ? ORDER BY FIRST_NAME", this,firstName);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'LAST_NAME = :lastName'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereLastNameEquals(String lastName) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE LAST_NAME = ? ORDER BY LAST_NAME", this,lastName);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'GENDER = :gender'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereGenderEquals(String gender) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE GENDER = ? ORDER BY GENDER", this,gender);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'EMAIL_ID = :emailId'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereEmailIdEquals(String emailId) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE EMAIL_ID = ? ORDER BY EMAIL_ID", this,emailId);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'CONTACT_NO = :contactNo'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereContactNoEquals(String contactNo) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE CONTACT_NO = ? ORDER BY CONTACT_NO", this,contactNo);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'USER_PASSWORD = :userPassword'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereUserPasswordEquals(String userPassword) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE USER_PASSWORD = ? ORDER BY USER_PASSWORD", this,userPassword);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'LAST_LOGIN_DETAILS = :lastLoginDetails'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereLastLoginDetailsEquals(String lastLoginDetails) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE LAST_LOGIN_DETAILS = ? ORDER BY LAST_LOGIN_DETAILS", this,lastLoginDetails);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_user_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	@Transactional
	public List<IprodUserMaster> findWhereIsDeletedEquals(short isDeleted) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE IS_DELETED = ? ORDER BY IS_DELETED", this,isDeleted);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_user_master table that matches the specified primary-key value.
	 */
	public IprodUserMaster findByPrimaryKey(IprodUserMasterPk pk) throws IprodUserMasterDaoException
	{
		return findByPrimaryKey( pk.getUserId() );
	}
	
	public void updatePassword(IprodUserMasterPk pk, IprodUserMaster dto) throws IprodUserMasterDaoException
	{
		jdbcTemplate.update("UPDATE " + getTableName() + " SET USER_PASSWORD = ? WHERE USER_ID = ?",dto.getUserPassword(),pk.getUserId());
	}

	public void updateWhereUserId(String userName,String userTitle,String firstName,String lastName,String gender,String emailId,String contact,int userId) throws IprodUserMasterDaoException
	{
		jdbcTemplate.update("UPDATE " + getTableName() + " SET USER_NAME = ?, USER_TITLE = ?, FIRST_NAME = ?, LAST_NAME = ?, GENDER = ?, EMAIL_ID = ?, CONTACT_NO = ? WHERE USER_ID = ?",userName,userTitle,firstName,lastName,gender,emailId,contact,userId);
		
	}
	
	public List<IprodUserMaster> findWhereUserNameEqualsAndIsDeleted(String userName,short isDeleted) throws IprodUserMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, IS_DELETED FROM " + getTableName() + " WHERE USER_NAME = ? AND IS_DELETED = ? ORDER BY USER_ID DESC", this,userName,isDeleted);
		}
		catch (Exception e) {
			throw new IprodUserMasterDaoException("Query failed", e);
		}
		
	}
}
