package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodSchedulerMasterDao;
import com.id4.iprod.dto.IprodSchedulerMaster;
import com.id4.iprod.dto.IprodSchedulerMasterPk;
import com.id4.iprod.exceptions.IprodSchedulerMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodSchedulerMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodSchedulerMaster>, IprodSchedulerMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodSchedulerMasterPk
	 */
	public IprodSchedulerMasterPk insert(IprodSchedulerMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHEDULAR_NAME, TRIGGER_NAME, SCHEDULAR_STATUS ) VALUES ( ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedularName(),dto.getTriggerName(),dto.getSchedularStatus()} );
		IprodSchedulerMasterPk pk = new IprodSchedulerMasterPk();
		pk.setSchedularId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_scheduler_master table.
	 */
	public void update(IprodSchedulerMasterPk pk, IprodSchedulerMaster dto) throws IprodSchedulerMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHEDULAR_NAME = ?, TRIGGER_NAME = ?, SCHEDULAR_STATUS = ? WHERE SCHEDULAR_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getSchedularName(),dto.getTriggerName(),dto.getSchedularStatus(),pk.getSchedularId() } );
	}

	/** 
	 * Deletes a single row in the iprod_scheduler_master table.
	 */
	@Transactional
	public void delete(IprodSchedulerMasterPk pk) throws IprodSchedulerMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHEDULAR_ID = ?",pk.getSchedularId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodSchedulerMaster
	 */
	public IprodSchedulerMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodSchedulerMaster dto = new IprodSchedulerMaster();
		dto.setSchedularId( rs.getInt( 1 ) );
		dto.setSchedularName( rs.getString( 2 ) );
		dto.setTriggerName( rs.getString( 3 ) );
		dto.setSchedularStatus( rs.getString( 4 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_scheduler_master";
	}

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_ID = :schedularId'.
	 */
	@Transactional
	public IprodSchedulerMaster findByPrimaryKey(int schedularId) throws IprodSchedulerMasterDaoException
	{
		try {
			List<IprodSchedulerMaster> list = jdbcTemplate.query("SELECT SCHEDULAR_ID, SCHEDULAR_NAME, TRIGGER_NAME, SCHEDULAR_STATUS FROM " + getTableName() + " WHERE SCHEDULAR_ID = ?", this,schedularId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodSchedulerMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodSchedulerMaster> findAll() throws IprodSchedulerMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHEDULAR_ID, SCHEDULAR_NAME, TRIGGER_NAME, SCHEDULAR_STATUS FROM " + getTableName() + " ORDER BY SCHEDULAR_ID", this);
		}
		catch (Exception e) {
			throw new IprodSchedulerMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_ID = :schedularId'.
	 */
	@Transactional
	public List<IprodSchedulerMaster> findWhereSchedularIdEquals(int schedularId) throws IprodSchedulerMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHEDULAR_ID, SCHEDULAR_NAME, TRIGGER_NAME, SCHEDULAR_STATUS FROM " + getTableName() + " WHERE SCHEDULAR_ID = ? ORDER BY SCHEDULAR_ID", this,schedularId);
		}
		catch (Exception e) {
			throw new IprodSchedulerMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_NAME = :schedularName'.
	 */
	@Transactional
	public List<IprodSchedulerMaster> findWhereSchedularNameEquals(String schedularName) throws IprodSchedulerMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHEDULAR_ID, SCHEDULAR_NAME, TRIGGER_NAME, SCHEDULAR_STATUS FROM " + getTableName() + " WHERE SCHEDULAR_NAME = ? ORDER BY SCHEDULAR_NAME", this,schedularName);
		}
		catch (Exception e) {
			throw new IprodSchedulerMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	@Transactional
	public List<IprodSchedulerMaster> findWhereTriggerNameEquals(String triggerName) throws IprodSchedulerMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHEDULAR_ID, SCHEDULAR_NAME, TRIGGER_NAME, SCHEDULAR_STATUS FROM " + getTableName() + " WHERE TRIGGER_NAME = ? ORDER BY TRIGGER_NAME", this,triggerName);
		}
		catch (Exception e) {
			throw new IprodSchedulerMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_STATUS = :schedularStatus'.
	 */
	@Transactional
	public List<IprodSchedulerMaster> findWhereSchedularStatusEquals(String schedularStatus) throws IprodSchedulerMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHEDULAR_ID, SCHEDULAR_NAME, TRIGGER_NAME, SCHEDULAR_STATUS FROM " + getTableName() + " WHERE SCHEDULAR_STATUS = ? ORDER BY SCHEDULAR_STATUS", this,schedularStatus);
		}
		catch (Exception e) {
			throw new IprodSchedulerMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_scheduler_master table that matches the specified primary-key value.
	 */
	public IprodSchedulerMaster findByPrimaryKey(IprodSchedulerMasterPk pk) throws IprodSchedulerMasterDaoException
	{
		return findByPrimaryKey( pk.getSchedularId() );
	}

}
