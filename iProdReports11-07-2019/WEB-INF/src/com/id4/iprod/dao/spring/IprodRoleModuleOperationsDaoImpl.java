package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodRoleModuleOperationsDao;
import com.id4.iprod.dto.IprodRoleModuleOperations;
import com.id4.iprod.dto.IprodRoleModuleOperationsPk;
import com.id4.iprod.exceptions.IprodRoleModuleOperationsDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodRoleModuleOperationsDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodRoleModuleOperations>, IprodRoleModuleOperationsDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodRoleModuleOperationsPk
	 */
	public IprodRoleModuleOperationsPk insert(IprodRoleModuleOperations dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( ROLE_ID, MODULE_ID, OPERATION_ID, IS_ALLOWED ) VALUES ( ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getRoleId(),dto.getModuleId(),dto.getOperationId(),dto.getIsAllowed()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the iprod_role_module_operations table.
	 */
	public void update(IprodRoleModuleOperationsPk pk, IprodRoleModuleOperations dto) throws IprodRoleModuleOperationsDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET ROLE_ID = ?, MODULE_ID = ?, OPERATION_ID = ?, IS_ALLOWED = ? WHERE ROLE_ID = ? AND MODULE_ID = ? AND OPERATION_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getRoleId(),dto.getModuleId(),dto.getOperationId(),dto.getIsAllowed(),pk.getRoleId(),pk.getModuleId(),pk.getOperationId() } );
	}

	/** 
	 * Deletes a single row in the iprod_role_module_operations table.
	 */
	@Transactional
	public void delete(IprodRoleModuleOperationsPk pk) throws IprodRoleModuleOperationsDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE ROLE_ID = ? AND MODULE_ID = ? AND OPERATION_ID = ?",pk.getRoleId(),pk.getModuleId(),pk.getOperationId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodRoleModuleOperations
	 */
	public IprodRoleModuleOperations mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodRoleModuleOperations dto = new IprodRoleModuleOperations();
		dto.setRoleId( rs.getInt( 1 ) );
		dto.setModuleId( rs.getInt( 2 ) );
		dto.setOperationId( rs.getInt( 3 ) );
		dto.setIsAllowed( rs.getShort( 4 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_role_module_operations";
	}

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'ROLE_ID = :roleId AND MODULE_ID = :moduleId AND OPERATION_ID = :operationId'.
	 */
	@Transactional
	public IprodRoleModuleOperations findByPrimaryKey(int roleId, int moduleId, int operationId) throws IprodRoleModuleOperationsDaoException
	{
		try {
			List<IprodRoleModuleOperations> list = jdbcTemplate.query("SELECT ROLE_ID, MODULE_ID, OPERATION_ID, IS_ALLOWED FROM " + getTableName() + " WHERE ROLE_ID = ? AND MODULE_ID = ? AND OPERATION_ID = ?", this,roleId,moduleId,operationId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria ''.
	 */
	@Transactional
	public List<IprodRoleModuleOperations> findAll() throws IprodRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, MODULE_ID, OPERATION_ID, IS_ALLOWED FROM " + getTableName() + " ORDER BY ROLE_ID, MODULE_ID, OPERATION_ID", this);
		}
		catch (Exception e) {
			throw new IprodRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'ROLE_ID = :roleId'.
	 */
	@Transactional
	public List<IprodRoleModuleOperations> findWhereRoleIdEquals(int roleId) throws IprodRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, MODULE_ID, OPERATION_ID, IS_ALLOWED FROM " + getTableName() + " WHERE ROLE_ID = ? ORDER BY ROLE_ID", this,roleId);
		}
		catch (Exception e) {
			throw new IprodRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	@Transactional
	public List<IprodRoleModuleOperations> findWhereModuleIdEquals(int moduleId) throws IprodRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, MODULE_ID, OPERATION_ID, IS_ALLOWED FROM " + getTableName() + " WHERE MODULE_ID = ? ORDER BY MODULE_ID", this,moduleId);
		}
		catch (Exception e) {
			throw new IprodRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	@Transactional
	public List<IprodRoleModuleOperations> findWhereOperationIdEquals(int operationId) throws IprodRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, MODULE_ID, OPERATION_ID, IS_ALLOWED FROM " + getTableName() + " WHERE OPERATION_ID = ? ORDER BY OPERATION_ID", this,operationId);
		}
		catch (Exception e) {
			throw new IprodRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'IS_ALLOWED = :isAllowed'.
	 */
	@Transactional
	public List<IprodRoleModuleOperations> findWhereIsAllowedEquals(short isAllowed) throws IprodRoleModuleOperationsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, MODULE_ID, OPERATION_ID, IS_ALLOWED FROM " + getTableName() + " WHERE IS_ALLOWED = ? ORDER BY IS_ALLOWED", this,isAllowed);
		}
		catch (Exception e) {
			throw new IprodRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_role_module_operations table that matches the specified primary-key value.
	 */
	public IprodRoleModuleOperations findByPrimaryKey(IprodRoleModuleOperationsPk pk) throws IprodRoleModuleOperationsDaoException
	{
		return findByPrimaryKey( pk.getRoleId(), pk.getModuleId(), pk.getOperationId() );
	}
	
	public void updateWhereRoleIdIsAndModuleIdAndOperationIdIs(short isAllowed,Integer moduleId,Integer operationId, Integer roleId) throws IprodRoleModuleOperationsDaoException {
		try {
		jdbcTemplate.update("UPDATE " + getTableName() + " SET IS_ALLOWED = ?  WHERE MODULE_ID= ? AND OPERATION_ID = ? AND ROLE_ID = ? ",isAllowed,moduleId,operationId,roleId);
		}
		catch (Exception e) {
			System.out.println("Exception is::"+e.getMessage());
			throw new IprodRoleModuleOperationsDaoException("Query failed", e);
		}
		
	}
	

}
