package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodModuleMasterDao;
import com.id4.iprod.dto.IprodModuleMaster;
import com.id4.iprod.dto.IprodModuleMasterPk;
import com.id4.iprod.exceptions.IprodModuleMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodModuleMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodModuleMaster>, IprodModuleMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodModuleMasterPk
	 */
	public IprodModuleMasterPk insert(IprodModuleMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( MODULE_NAME ) VALUES ( ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getModuleName()} );
		IprodModuleMasterPk pk = new IprodModuleMasterPk();
		pk.setModuleId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_module_master table.
	 */
	public void update(IprodModuleMasterPk pk, IprodModuleMaster dto) throws IprodModuleMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET MODULE_NAME = ? WHERE MODULE_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getModuleName(),pk.getModuleId() } );
	}

	/** 
	 * Deletes a single row in the iprod_module_master table.
	 */
	@Transactional
	public void delete(IprodModuleMasterPk pk) throws IprodModuleMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE MODULE_ID = ?",pk.getModuleId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodModuleMaster
	 */
	public IprodModuleMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodModuleMaster dto = new IprodModuleMaster();
		dto.setModuleId( rs.getInt( 1 ) );
		dto.setModuleName( rs.getString( 2 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_module_master";
	}

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	@Transactional
	public IprodModuleMaster findByPrimaryKey(int moduleId) throws IprodModuleMasterDaoException
	{
		try {
			List<IprodModuleMaster> list = jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME FROM " + getTableName() + " WHERE MODULE_ID = ?", this,moduleId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodModuleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodModuleMaster> findAll() throws IprodModuleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME FROM " + getTableName() + " ORDER BY MODULE_ID", this);
		}
		catch (Exception e) {
			throw new IprodModuleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	@Transactional
	public List<IprodModuleMaster> findWhereModuleIdEquals(int moduleId) throws IprodModuleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME FROM " + getTableName() + " WHERE MODULE_ID = ? ORDER BY MODULE_ID", this,moduleId);
		}
		catch (Exception e) {
			throw new IprodModuleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria 'MODULE_NAME = :moduleName'.
	 */
	@Transactional
	public List<IprodModuleMaster> findWhereModuleNameEquals(String moduleName) throws IprodModuleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT MODULE_ID, MODULE_NAME FROM " + getTableName() + " WHERE MODULE_NAME = ? ORDER BY MODULE_NAME", this,moduleName);
		}
		catch (Exception e) {
			throw new IprodModuleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_module_master table that matches the specified primary-key value.
	 */
	public IprodModuleMaster findByPrimaryKey(IprodModuleMasterPk pk) throws IprodModuleMasterDaoException
	{
		return findByPrimaryKey( pk.getModuleId() );
	}

}
