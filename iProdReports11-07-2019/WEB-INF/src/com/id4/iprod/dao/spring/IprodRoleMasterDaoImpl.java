package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodRoleMasterDao;
import com.id4.iprod.dto.IprodRoleMaster;
import com.id4.iprod.dto.IprodRoleMasterPk;
import com.id4.iprod.exceptions.IprodRoleMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodRoleMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodRoleMaster>, IprodRoleMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodRoleMasterPk
	 */
	public IprodRoleMasterPk insert(IprodRoleMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME ) VALUES ( ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getRoleName(),dto.getRoleDesc(),dto.getRoleIsDeleted(),dto.getRoleCdatettime(),dto.getRoleMdatettime()} );
		IprodRoleMasterPk pk = new IprodRoleMasterPk();
		pk.setRoleId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_role_master table.
	 */
	public void update(IprodRoleMasterPk pk, IprodRoleMaster dto) throws IprodRoleMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET ROLE_NAME = ?, ROLE_DESC = ?, ROLE_IS_DELETED = ?, ROLE_CDATETTIME = ?, ROLE_MDATETTIME = ? WHERE ROLE_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getRoleName(),dto.getRoleDesc(),dto.getRoleIsDeleted(),dto.getRoleCdatettime(),dto.getRoleMdatettime(),pk.getRoleId() } );
	}

	/** 
	 * Deletes a single row in the iprod_role_master table.
	 */
	@Transactional
	public void delete(IprodRoleMasterPk pk) throws IprodRoleMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE ROLE_ID = ?",pk.getRoleId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodRoleMaster
	 */
	public IprodRoleMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodRoleMaster dto = new IprodRoleMaster();
		dto.setRoleId( rs.getInt( 1 ) );
		dto.setRoleName( rs.getString( 2 ) );
		dto.setRoleDesc( rs.getString( 3 ) );
		dto.setRoleIsDeleted( rs.getShort( 4 ) );
		if (rs.wasNull()) {
			dto.setRoleIsDeletedNull( true );
		}
		
		dto.setRoleCdatettime( rs.getString( 5 ) );
		dto.setRoleMdatettime( rs.getString( 6 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_role_master";
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_ID = :roleId'.
	 */
	@Transactional
	public IprodRoleMaster findByPrimaryKey(int roleId) throws IprodRoleMasterDaoException
	{
		try {
			List<IprodRoleMaster> list = jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_ID = ?", this,roleId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodRoleMaster> findAll() throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " ORDER BY ROLE_ID", this);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_ID = :roleId'.
	 */
	@Transactional
	public List<IprodRoleMaster> findWhereRoleIdEquals(int roleId) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_ID = ? ORDER BY ROLE_ID", this,roleId);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_NAME = :roleName'.
	 */
	@Transactional
	public List<IprodRoleMaster> findWhereRoleNameEquals(String roleName) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_NAME = ? ORDER BY ROLE_NAME", this,roleName);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_DESC = :roleDesc'.
	 */
	@Transactional
	public List<IprodRoleMaster> findWhereRoleDescEquals(String roleDesc) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_DESC = ? ORDER BY ROLE_DESC", this,roleDesc);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_IS_DELETED = :roleIsDeleted'.
	 */
	@Transactional
	public List<IprodRoleMaster> findWhereRoleIsDeletedEquals(short roleIsDeleted) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_IS_DELETED = ? ORDER BY ROLE_IS_DELETED", this,roleIsDeleted);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_CDATETTIME = :roleCdatettime'.
	 */
	@Transactional
	public List<IprodRoleMaster> findWhereRoleCdatettimeEquals(String roleCdatettime) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_CDATETTIME = ? ORDER BY ROLE_CDATETTIME", this,roleCdatettime);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_MDATETTIME = :roleMdatettime'.
	 */
	@Transactional
	public List<IprodRoleMaster> findWhereRoleMdatettimeEquals(String roleMdatettime) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_MDATETTIME = ? ORDER BY ROLE_MDATETTIME", this,roleMdatettime);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_role_master table that matches the specified primary-key value.
	 */
	public IprodRoleMaster findByPrimaryKey(IprodRoleMasterPk pk) throws IprodRoleMasterDaoException
	{
		return findByPrimaryKey( pk.getRoleId() );
	}
	
	public List<IprodRoleMaster> findWhereRoleIdNotEquals(int roleId) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE NOT ROLE_ID = ? ORDER BY ROLE_ID", this,roleId);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}
	
	public List<IprodRoleMaster> findWhereRoleNameEqualsAndIsDeleted(String roleName,short roleIsDeleted) throws IprodRoleMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC, ROLE_IS_DELETED, ROLE_CDATETTIME, ROLE_MDATETTIME FROM " + getTableName() + " WHERE ROLE_NAME = ? AND ROLE_IS_DELETED = ? ORDER BY ROLE_NAME", this,roleName,roleIsDeleted);
		}
		catch (Exception e) {
			throw new IprodRoleMasterDaoException("Query failed", e);
		}
		
	}

}
