package com.id4.iprod.dao.spring;

import com.id4.iprod.dao.IprodViewUserDetailsDao;
import com.id4.iprod.dto.IprodViewUserDetails;
import com.id4.iprod.exceptions.IprodViewUserDetailsDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodViewUserDetailsDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodViewUserDetails>, IprodViewUserDetailsDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodViewUserDetails dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getUserId(),dto.getUserName(),dto.getUserTitle(),dto.getFirstName(),dto.getLastName(),dto.getGender(),dto.getEmailId(),dto.getContactNo(),dto.getUserPassword(),dto.getLastLoginDetails(),dto.getUserIsDeleted(),dto.getRoleId(),dto.getRoleName(),dto.getRoleDesc()} );
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodViewUserDetails
	 */
	public IprodViewUserDetails mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodViewUserDetails dto = new IprodViewUserDetails();
		dto.setUserId( rs.getInt( 1 ) );
		dto.setUserName( rs.getString( 2 ) );
		dto.setUserTitle( rs.getString( 3 ) );
		dto.setFirstName( rs.getString( 4 ) );
		dto.setLastName( rs.getString( 5 ) );
		dto.setGender( rs.getString( 6 ) );
		dto.setEmailId( rs.getString( 7 ) );
		dto.setContactNo( rs.getString( 8 ) );
		dto.setUserPassword( rs.getString( 9 ) );
		dto.setLastLoginDetails( rs.getString( 10 ) );
		dto.setUserIsDeleted( rs.getShort( 11 ) );
		dto.setRoleId( rs.getInt( 12 ) );
		if (rs.wasNull()) {
			dto.setRoleIdNull( true );
		}
		
		dto.setRoleName( rs.getString( 13 ) );
		dto.setRoleDesc( rs.getString( 14 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_view_user_details";
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria ''.
	 */
	@Transactional
	public List<IprodViewUserDetails> findAll() throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + "", this);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_ID = :userId'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereUserIdEquals(int userId) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE USER_ID = ? ORDER BY USER_ID", this,userId);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_NAME = :userName'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereUserNameEquals(String userName) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE USER_NAME = ? ORDER BY USER_NAME ", this,userName);
		}
		catch (Exception e) {
			
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_TITLE = :userTitle'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereUserTitleEquals(String userTitle) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE USER_TITLE = ? ORDER BY USER_TITLE", this,userTitle);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'FIRST_NAME = :firstName'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereFirstNameEquals(String firstName) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE FIRST_NAME = ? ORDER BY FIRST_NAME", this,firstName);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'LAST_NAME = :lastName'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereLastNameEquals(String lastName) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE LAST_NAME = ? ORDER BY LAST_NAME", this,lastName);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'GENDER = :gender'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereGenderEquals(String gender) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE GENDER = ? ORDER BY GENDER", this,gender);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'EMAIL_ID = :emailId'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereEmailIdEquals(String emailId) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE EMAIL_ID = ? ORDER BY EMAIL_ID", this,emailId);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'CONTACT_NO = :contactNo'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereContactNoEquals(String contactNo) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE CONTACT_NO = ? ORDER BY CONTACT_NO", this,contactNo);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_PASSWORD = :userPassword'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereUserPasswordEquals(String userPassword) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE USER_PASSWORD = ? ORDER BY USER_PASSWORD", this,userPassword);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'LAST_LOGIN_DETAILS = :lastLoginDetails'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereLastLoginDetailsEquals(String lastLoginDetails) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE LAST_LOGIN_DETAILS = ? ORDER BY LAST_LOGIN_DETAILS", this,lastLoginDetails);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'USER_IS_DELETED = :userIsDeleted'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereUserIsDeletedEquals(short userIsDeleted) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE USER_IS_DELETED = ? ORDER BY USER_IS_DELETED", this,userIsDeleted);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'ROLE_ID = :roleId'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereRoleIdEquals(int roleId) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE ROLE_ID = ? ORDER BY ROLE_ID", this,roleId);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'ROLE_NAME = :roleName'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereRoleNameEquals(String roleName) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE ROLE_NAME = ? ORDER BY ROLE_NAME", this,roleName);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'ROLE_DESC = :roleDesc'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereRoleDescEquals(String roleDesc) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE ROLE_DESC = ? ORDER BY ROLE_DESC", this,roleDesc);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	
	
	
	/***/
	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'IS_DELETE = :isDeleted AND
ROLE_ID = :roleId EXCEPT ADMIN'.
	 */
	@Transactional
	public List<IprodViewUserDetails> findWhereIsDeletedAndRoleExceptAdminAndPlantIdEquals(short isDeleted, int roleId) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE USER_IS_DELETED = ? AND NOT ROLE_ID = ? ORDER BY USER_ID", this,isDeleted,roleId);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_view_user_details table that match the criteria 'NOT IS_DELETED = :isDeleted AND
NOT ROLE_ID = :roleId AND ALL WORKERS EXCEPT CURRENT USER LOGGED IN'.
	 */
		
	@Transactional
	public List<IprodViewUserDetails> findWhereRoleIdAndIsDeletedEqualsAndPlantIdEquals(int roleId, short isDeleted) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE ROLE_ID = ? AND USER_IS_DELETED = ? ORDER BY USER_ID", this,roleId, isDeleted);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}
	
	public List<IprodViewUserDetails> findWhereRoleIdNotEquals(int roleId) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE NOT ROLE_ID = ? ORDER BY ROLE_ID", this,roleId);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}
	
	@Transactional
	public List<IprodViewUserDetails> findWhereUserNameEqualsAndIsDeleted(String userName,short isDeleted) throws IprodViewUserDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT USER_ID, USER_NAME, USER_TITLE, FIRST_NAME, LAST_NAME, GENDER, EMAIL_ID, CONTACT_NO, USER_PASSWORD, LAST_LOGIN_DETAILS, USER_IS_DELETED, ROLE_ID, ROLE_NAME, ROLE_DESC FROM " + getTableName() + " WHERE USER_NAME = ? AND USER_IS_DELETED = ? ORDER BY USER_ID DESC", this,userName,isDeleted);
		}
		catch (Exception e) {
			throw new IprodViewUserDetailsDaoException("Query failed", e);
		}
		
	}

}
