package com.id4.iprod.dao;

import java.util.List;

import com.id4.iprod.dto.IprodOperationLoggerHistory;
import com.id4.iprod.dto.IprodOperationLoggerHistoryPk;
import com.id4.iprod.exceptions.IprodOperationLoggerHistoryDaoException;

public interface IprodOperationLoggerHistoryDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodOperationLoggerHistoryPk
	 */
	public IprodOperationLoggerHistoryPk insert(IprodOperationLoggerHistory dto);

	/** 
	 * Updates a single row in the iprod_operation_logger_history table.
	 */
	public void update(IprodOperationLoggerHistoryPk pk, IprodOperationLoggerHistory dto) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Deletes a single row in the iprod_operation_logger_history table.
	 */
	public void delete(IprodOperationLoggerHistoryPk pk) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public IprodOperationLoggerHistory findByPrimaryKey(int operationId) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria ''.
	 */
	public List<IprodOperationLoggerHistory> findAll() throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public List<IprodOperationLoggerHistory> findWhereOperationIdEquals(int operationId) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_PAGE_SOURCE = :operationPageSource'.
	 */
	public List<IprodOperationLoggerHistory> findWhereOperationPageSourceEquals(String operationPageSource) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'COMMENTS = :comments'.
	 */
	public List<IprodOperationLoggerHistory> findWhereCommentsEquals(String comments) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'USER_ID = :userId'.
	 */
	public List<IprodOperationLoggerHistory> findWhereUserIdEquals(int userId) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'USERNAME = :username'.
	 */
	public List<IprodOperationLoggerHistory> findWhereUsernameEquals(String username) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_LOGGER_CDATETIME = :operationLoggerCdatetime'.
	 */
	public List<IprodOperationLoggerHistory> findWhereOperationLoggerCdatetimeEquals(String operationLoggerCdatetime) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns all rows from the iprod_operation_logger_history table that match the criteria 'OPERATION_LOGGER_MDATETIME = :operationLoggerMdatetime'.
	 */
	public List<IprodOperationLoggerHistory> findWhereOperationLoggerMdatetimeEquals(String operationLoggerMdatetime) throws IprodOperationLoggerHistoryDaoException;

	/** 
	 * Returns the rows from the iprod_operation_logger_history table that matches the specified primary-key value.
	 */
	public IprodOperationLoggerHistory findByPrimaryKey(IprodOperationLoggerHistoryPk pk) throws IprodOperationLoggerHistoryDaoException;

}
