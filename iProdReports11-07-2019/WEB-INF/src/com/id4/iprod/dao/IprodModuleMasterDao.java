package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodModuleMasterDao;
import com.id4.iprod.dto.IprodModuleMaster;
import com.id4.iprod.dto.IprodModuleMasterPk;
import com.id4.iprod.exceptions.IprodModuleMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodModuleMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodModuleMasterPk
	 */
	public IprodModuleMasterPk insert(IprodModuleMaster dto);

	/** 
	 * Updates a single row in the iprod_module_master table.
	 */
	public void update(IprodModuleMasterPk pk, IprodModuleMaster dto) throws IprodModuleMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_module_master table.
	 */
	public void delete(IprodModuleMasterPk pk) throws IprodModuleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	public IprodModuleMaster findByPrimaryKey(int moduleId) throws IprodModuleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria ''.
	 */
	public List<IprodModuleMaster> findAll() throws IprodModuleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	public List<IprodModuleMaster> findWhereModuleIdEquals(int moduleId) throws IprodModuleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_module_master table that match the criteria 'MODULE_NAME = :moduleName'.
	 */
	public List<IprodModuleMaster> findWhereModuleNameEquals(String moduleName) throws IprodModuleMasterDaoException;

	/** 
	 * Returns the rows from the iprod_module_master table that matches the specified primary-key value.
	 */
	public IprodModuleMaster findByPrimaryKey(IprodModuleMasterPk pk) throws IprodModuleMasterDaoException;

}
