package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodViewModuleOperationMasterDao;
import com.id4.iprod.dto.IprodViewModuleOperationMaster;
import com.id4.iprod.exceptions.IprodViewModuleOperationMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodViewModuleOperationMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodViewModuleOperationMaster dto);

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria ''.
	 */
	public List<IprodViewModuleOperationMaster> findAll() throws IprodViewModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	public List<IprodViewModuleOperationMaster> findWhereModuleIdEquals(int moduleId) throws IprodViewModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'MODULE_NAME = :moduleName'.
	 */
	public List<IprodViewModuleOperationMaster> findWhereModuleNameEquals(String moduleName) throws IprodViewModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public List<IprodViewModuleOperationMaster> findWhereOperationIdEquals(int operationId) throws IprodViewModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'OPERATION_NAME = :operationName'.
	 */
	public List<IprodViewModuleOperationMaster> findWhereOperationNameEquals(String operationName) throws IprodViewModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'OPERATION_DESCRIPTION = :operationDescription'.
	 */
	public List<IprodViewModuleOperationMaster> findWhereOperationDescriptionEquals(String operationDescription) throws IprodViewModuleOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_view_module_operation_master table that match the criteria 'IS_APPLICABLE = :isApplicable'.
	 */
	public List<IprodViewModuleOperationMaster> findWhereIsApplicableEquals(short isApplicable) throws IprodViewModuleOperationMasterDaoException;

}
