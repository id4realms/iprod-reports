package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodOperationMasterDao;
import com.id4.iprod.dto.IprodOperationMaster;
import com.id4.iprod.dto.IprodOperationMasterPk;
import com.id4.iprod.exceptions.IprodOperationMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodOperationMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodOperationMasterPk
	 */
	public IprodOperationMasterPk insert(IprodOperationMaster dto);

	/** 
	 * Updates a single row in the iprod_operation_master table.
	 */
	public void update(IprodOperationMasterPk pk, IprodOperationMaster dto) throws IprodOperationMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_operation_master table.
	 */
	public void delete(IprodOperationMasterPk pk) throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public IprodOperationMaster findByPrimaryKey(int operationId) throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria ''.
	 */
	public List<IprodOperationMaster> findAll() throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public List<IprodOperationMaster> findWhereOperationIdEquals(int operationId) throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_NAME = :operationName'.
	 */
	public List<IprodOperationMaster> findWhereOperationNameEquals(String operationName) throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_DESCRIPTION = :operationDescription'.
	 */
	public List<IprodOperationMaster> findWhereOperationDescriptionEquals(String operationDescription) throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_CDATETTIME = :operationCdatettime'.
	 */
	public List<IprodOperationMaster> findWhereOperationCdatettimeEquals(String operationCdatettime) throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'OPERATION_MDATETIME = :operationMdatetime'.
	 */
	public List<IprodOperationMaster> findWhereOperationMdatetimeEquals(String operationMdatetime) throws IprodOperationMasterDaoException;

	/** 
	 * Returns all rows from the iprod_operation_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	public List<IprodOperationMaster> findWhereIsDeletedEquals(short isDeleted) throws IprodOperationMasterDaoException;

	/** 
	 * Returns the rows from the iprod_operation_master table that matches the specified primary-key value.
	 */
	public IprodOperationMaster findByPrimaryKey(IprodOperationMasterPk pk) throws IprodOperationMasterDaoException;

}
