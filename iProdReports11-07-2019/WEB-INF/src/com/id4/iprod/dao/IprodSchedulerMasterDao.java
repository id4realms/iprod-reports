package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodSchedulerMasterDao;
import com.id4.iprod.dto.IprodSchedulerMaster;
import com.id4.iprod.dto.IprodSchedulerMasterPk;
import com.id4.iprod.exceptions.IprodSchedulerMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodSchedulerMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodSchedulerMasterPk
	 */
	public IprodSchedulerMasterPk insert(IprodSchedulerMaster dto);

	/** 
	 * Updates a single row in the iprod_scheduler_master table.
	 */
	public void update(IprodSchedulerMasterPk pk, IprodSchedulerMaster dto) throws IprodSchedulerMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_scheduler_master table.
	 */
	public void delete(IprodSchedulerMasterPk pk) throws IprodSchedulerMasterDaoException;

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_ID = :schedularId'.
	 */
	public IprodSchedulerMaster findByPrimaryKey(int schedularId) throws IprodSchedulerMasterDaoException;

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria ''.
	 */
	public List<IprodSchedulerMaster> findAll() throws IprodSchedulerMasterDaoException;

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_ID = :schedularId'.
	 */
	public List<IprodSchedulerMaster> findWhereSchedularIdEquals(int schedularId) throws IprodSchedulerMasterDaoException;

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_NAME = :schedularName'.
	 */
	public List<IprodSchedulerMaster> findWhereSchedularNameEquals(String schedularName) throws IprodSchedulerMasterDaoException;

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	public List<IprodSchedulerMaster> findWhereTriggerNameEquals(String triggerName) throws IprodSchedulerMasterDaoException;

	/** 
	 * Returns all rows from the iprod_scheduler_master table that match the criteria 'SCHEDULAR_STATUS = :schedularStatus'.
	 */
	public List<IprodSchedulerMaster> findWhereSchedularStatusEquals(String schedularStatus) throws IprodSchedulerMasterDaoException;

	/** 
	 * Returns the rows from the iprod_scheduler_master table that matches the specified primary-key value.
	 */
	public IprodSchedulerMaster findByPrimaryKey(IprodSchedulerMasterPk pk) throws IprodSchedulerMasterDaoException;

}
