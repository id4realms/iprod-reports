package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodRoleModuleOperationsDao;
import com.id4.iprod.dto.IprodRoleModuleOperations;
import com.id4.iprod.dto.IprodRoleModuleOperationsPk;
import com.id4.iprod.exceptions.IprodRoleModuleOperationsDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodRoleModuleOperationsDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodRoleModuleOperationsPk
	 */
	public IprodRoleModuleOperationsPk insert(IprodRoleModuleOperations dto);

	/** 
	 * Updates a single row in the iprod_role_module_operations table.
	 */
	public void update(IprodRoleModuleOperationsPk pk, IprodRoleModuleOperations dto) throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Deletes a single row in the iprod_role_module_operations table.
	 */
	public void delete(IprodRoleModuleOperationsPk pk) throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'ROLE_ID = :roleId AND MODULE_ID = :moduleId AND OPERATION_ID = :operationId'.
	 */
	public IprodRoleModuleOperations findByPrimaryKey(int roleId, int moduleId, int operationId) throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria ''.
	 */
	public List<IprodRoleModuleOperations> findAll() throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'ROLE_ID = :roleId'.
	 */
	public List<IprodRoleModuleOperations> findWhereRoleIdEquals(int roleId) throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	public List<IprodRoleModuleOperations> findWhereModuleIdEquals(int moduleId) throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public List<IprodRoleModuleOperations> findWhereOperationIdEquals(int operationId) throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_role_module_operations table that match the criteria 'IS_ALLOWED = :isAllowed'.
	 */
	public List<IprodRoleModuleOperations> findWhereIsAllowedEquals(short isAllowed) throws IprodRoleModuleOperationsDaoException;

	/** 
	 * Returns the rows from the iprod_role_module_operations table that matches the specified primary-key value.
	 */
	public IprodRoleModuleOperations findByPrimaryKey(IprodRoleModuleOperationsPk pk) throws IprodRoleModuleOperationsDaoException;

	public void updateWhereRoleIdIsAndModuleIdAndOperationIdIs(short isAllowed,Integer moduleId, Integer operationId, Integer roleId)throws IprodRoleModuleOperationsDaoException;

}
