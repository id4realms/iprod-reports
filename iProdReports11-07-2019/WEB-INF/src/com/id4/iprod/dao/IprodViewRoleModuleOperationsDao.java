package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodViewRoleModuleOperationsDao;
import com.id4.iprod.dto.IprodViewRoleModuleOperations;
import com.id4.iprod.exceptions.IprodViewRoleModuleOperationsDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodViewRoleModuleOperationsDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodViewRoleModuleOperations dto);

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria ''.
	 */
	public List<IprodViewRoleModuleOperations> findAll() throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'ROLE_ID = :roleId'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereRoleIdEquals(int roleId) throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'ROLE_NAME = :roleName'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereRoleNameEquals(String roleName) throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'MODULE_ID = :moduleId'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereModuleIdEquals(int moduleId) throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'MODULE_NAME = :moduleName'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereModuleNameEquals(String moduleName) throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'OPERATION_ID = :operationId'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereOperationIdEquals(int operationId) throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'OPERATION_NAME = :operationName'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereOperationNameEquals(String operationName) throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'OPERATION_DESCRIPTION = :operationDescription'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereOperationDescriptionEquals(String operationDescription) throws IprodViewRoleModuleOperationsDaoException;

	/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'IS_ALLOWED = :isAllowed'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereIsAllowedEquals(short isAllowed) throws IprodViewRoleModuleOperationsDaoException;

		/** 
	 * Returns all rows from the iprod_view_role_module_operations table that match the criteria 'IS_APPLICABLE = :isApplicable'.
	 */
	public List<IprodViewRoleModuleOperations> findWhereIsApplicableEquals(short isApplicable) throws IprodViewRoleModuleOperationsDaoException;


	public List<IprodViewRoleModuleOperations> findWhereRoleIdModuleNameAndOperationNameEquals(int roleId, String moduleName, String operationName) throws IprodViewRoleModuleOperationsDaoException;

	public List<IprodViewRoleModuleOperations> findWhereRoleIdModuleNameEquals(int roleId, String moduleName) throws IprodViewRoleModuleOperationsDaoException;

}
