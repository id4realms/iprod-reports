package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodUserRoleDao;
import com.id4.iprod.dto.IprodUserRole;
import com.id4.iprod.dto.IprodUserRolePk;
import com.id4.iprod.exceptions.IprodUserRoleDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodUserRoleDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodUserRolePk
	 */
	public IprodUserRolePk insert(IprodUserRole dto);

	/** 
	 * Updates a single row in the iprod_user_role table.
	 */
	public void update(IprodUserRolePk pk, IprodUserRole dto) throws IprodUserRoleDaoException;

	/** 
	 * Deletes a single row in the iprod_user_role table.
	 */
	public void delete(IprodUserRolePk pk) throws IprodUserRoleDaoException;

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria 'USER_ID = :userId AND ROLE_ID = :roleId'.
	 */
	public IprodUserRole findByPrimaryKey(int userId, int roleId) throws IprodUserRoleDaoException;

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria ''.
	 */
	public List<IprodUserRole> findAll() throws IprodUserRoleDaoException;

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria 'USER_ID = :userId'.
	 */
	public List<IprodUserRole> findWhereUserIdEquals(int userId) throws IprodUserRoleDaoException;

	/** 
	 * Returns all rows from the iprod_user_role table that match the criteria 'ROLE_ID = :roleId'.
	 */
	public List<IprodUserRole> findWhereRoleIdEquals(int roleId) throws IprodUserRoleDaoException;

	/** 
	 * Returns the rows from the iprod_user_role table that matches the specified primary-key value.
	 */
	public IprodUserRole findByPrimaryKey(IprodUserRolePk pk) throws IprodUserRoleDaoException;
	
	public void updateRoleIdWhereUserIdIs(int roleId,int userId) throws IprodUserRoleDaoException;

}
