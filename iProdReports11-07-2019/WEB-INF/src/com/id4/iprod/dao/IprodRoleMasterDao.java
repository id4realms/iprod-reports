package com.id4.iprod.dao;

import com.id4.iprod.dao.IprodRoleMasterDao;
import com.id4.iprod.dto.IprodRoleMaster;
import com.id4.iprod.dto.IprodRoleMasterPk;
import com.id4.iprod.exceptions.IprodRoleMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodRoleMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodRoleMasterPk
	 */
	public IprodRoleMasterPk insert(IprodRoleMaster dto);

	/** 
	 * Updates a single row in the iprod_role_master table.
	 */
	public void update(IprodRoleMasterPk pk, IprodRoleMaster dto) throws IprodRoleMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_role_master table.
	 */
	public void delete(IprodRoleMasterPk pk) throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_ID = :roleId'.
	 */
	public IprodRoleMaster findByPrimaryKey(int roleId) throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria ''.
	 */
	public List<IprodRoleMaster> findAll() throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_ID = :roleId'.
	 */
	public List<IprodRoleMaster> findWhereRoleIdEquals(int roleId) throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_NAME = :roleName'.
	 */
	public List<IprodRoleMaster> findWhereRoleNameEquals(String roleName) throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_DESC = :roleDesc'.
	 */
	public List<IprodRoleMaster> findWhereRoleDescEquals(String roleDesc) throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_IS_DELETED = :roleIsDeleted'.
	 */
	public List<IprodRoleMaster> findWhereRoleIsDeletedEquals(short roleIsDeleted) throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_CDATETTIME = :roleCdatettime'.
	 */
	public List<IprodRoleMaster> findWhereRoleCdatettimeEquals(String roleCdatettime) throws IprodRoleMasterDaoException;

	/** 
	 * Returns all rows from the iprod_role_master table that match the criteria 'ROLE_MDATETTIME = :roleMdatettime'.
	 */
	public List<IprodRoleMaster> findWhereRoleMdatettimeEquals(String roleMdatettime) throws IprodRoleMasterDaoException;

	/** 
	 * Returns the rows from the iprod_role_master table that matches the specified primary-key value.
	 */
	public IprodRoleMaster findByPrimaryKey(IprodRoleMasterPk pk) throws IprodRoleMasterDaoException;
	
	public List<IprodRoleMaster> findWhereRoleIdNotEquals(int roleId) throws IprodRoleMasterDaoException;

	public List<IprodRoleMaster> findWhereRoleNameEqualsAndIsDeleted(String roleName,short roleIsDeleted) throws IprodRoleMasterDaoException;
	
	

}
