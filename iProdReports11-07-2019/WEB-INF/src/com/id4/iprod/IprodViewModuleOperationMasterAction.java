package com.id4.iprod;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.dto.IprodModuleMaster;
import com.id4.iprod.dto.IprodOperationMaster;
import com.id4.iprod.dto.IprodViewModuleOperationMaster;
import com.id4.iprod.exceptions.IprodModuleMasterDaoException;
import com.id4.iprod.exceptions.IprodOperationMasterDaoException;
import com.id4.iprod.exceptions.IprodViewModuleOperationMasterDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodViewModuleOperationMasterAction extends ActionSupport{
	
	static Logger log = Logger.getLogger(IprodViewModuleOperationMasterAction.class);
	
	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_view_module_operation_master table.
	 */
	protected int moduleId;

	/** 
	 * This attribute maps to the column MODULE_NAME in the iprod_view_module_operation_master table.
	 */
	protected String moduleName;

	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_view_module_operation_master table.
	 */
	protected int operationId;

	/** 
	 * This attribute maps to the column OPERATION_NAME in the iprod_view_module_operation_master table.
	 */
	protected String operationName;
	
	/** 
	 * This attribute maps to the column OPERATION_DESCRIPTION in the iprod_operation_master table.
	 */
	protected String operationDescription;
	
	/** 
	 * This attribute maps to the column IS_APPLICABLE in the iprod_view_module_operation_master table.
	 */
	protected short isApplicable;

	public List<IprodModuleMaster> moduleListG = new ArrayList<IprodModuleMaster>(); 
	public List<IprodOperationMaster> operationListG = new ArrayList<IprodOperationMaster>();
	public List<IprodViewModuleOperationMaster> moduleWiseOperationListG = new ArrayList<IprodViewModuleOperationMaster>();



	
	public String showModuleWiseOperation(){
		log.info("IprodViewModuleOperationMasterAction::showModuleWiseOperation");
		try{
		Map<String, Object> session = ActionContext.getContext().getSession();
		if(session.get(Consts.IPRODUSERID)!= null)
	    {
			List<IprodModuleMaster> moduleListL = new ArrayList<IprodModuleMaster>();
			List<IprodOperationMaster> operationListL = new ArrayList<IprodOperationMaster>();
			List<IprodViewModuleOperationMaster> moduleWiseOperationListL = new ArrayList<IprodViewModuleOperationMaster>();
			try
			{
				moduleListL = DaoFactory.createIprodModuleMasterDao().findAll();
				if(moduleListL.size()>0)
				{
					moduleListG = moduleListL;
					log.info("moduleListG" +moduleListG);

				}
			}
			catch(IprodModuleMasterDaoException e)
			{
				log.error("Exception" +e.getMessage());
			}
			try
			{
				operationListL = DaoFactory.createIprodOperationMasterDao().findAll();
				if(operationListL.size()>0)
				{
					operationListG = operationListL;
					log.info("moduleListG" +moduleListG);

				}
			}
			catch(IprodOperationMasterDaoException e)
			{
				log.error("Exception" +e.getMessage());
			}
			try
			{
				moduleWiseOperationListL = DaoFactory.createIprodViewModuleOperationMasterDao().findAll();
				if(moduleWiseOperationListL.size()>0)
				{
					moduleWiseOperationListG = moduleWiseOperationListL;
					log.info("moduleWiseOperationListG" +moduleWiseOperationListG.size());
					log.info("moduleWiseOperationListG" +moduleWiseOperationListG);

				}
			}
			catch(IprodViewModuleOperationMasterDaoException e)
			{
				log.error("Exception" +e.getMessage());
			}

	    }
	 }
		catch (Exception e){
			log.error("getAllRoles :: Exception :: " + e.getMessage() +" occured in getAllRoles.");
		}
		
		return SUCCESS;
	}
	
	/**
	 * Method 'getModuleId'
	 * 
	 * @return int
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'setModuleId'
	 * 
	 * @param moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getModuleName'
	 * 
	 * @return String
	 */
	public String getModuleName()
	{
		return moduleName;
	}

	/**
	 * Method 'setModuleName'
	 * 
	 * @param moduleName
	 */
	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	/**
	 * Method 'getOperationId'
	 * 
	 * @return int
	 */
	public int getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'setOperationId'
	 * 
	 * @param operationId
	 */
	public void setOperationId(int operationId)
	{
		this.operationId = operationId;
	}

	/**
	 * Method 'getOperationName'
	 * 
	 * @return String
	 */
	public String getOperationName()
	{
		return operationName;
	}

	/**
	 * Method 'setOperationName'
	 * 
	 * @param operationName
	 */
	public void setOperationName(String operationName)
	{
		this.operationName = operationName;
	}

	public Short getIsApplicable() {
		return isApplicable;
	}

	public void setIsApplicable(Short isApplicable) {
		this.isApplicable = isApplicable;
	}

	public String getOperationDescription() {
		return operationDescription;
	}

	public void setOperationDescription(String operationDescription) {
		this.operationDescription = operationDescription;
	}

}
