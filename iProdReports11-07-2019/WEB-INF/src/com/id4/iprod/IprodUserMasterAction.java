package com.id4.iprod;

import java.io.IOException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.dao.IprodUserMasterDao;
import com.id4.iprod.dao.IprodUserRoleDao;
import com.id4.iprod.dto.IprodUserMaster;
import com.id4.iprod.dto.IprodUserMasterPk;
import com.id4.iprod.exceptions.IprodUserMasterDaoException;
import com.id4.iprod.exceptions.IprodUserRoleDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodUserMasterAction extends ActionSupport implements IprodModuleAuthorization {
	static Logger log = Logger.getLogger(IprodUserMasterAction.class);
	/**
	 * This attribute maps to the column USER_ID in the iprod_user_master table.
	 */
	protected int userId;

	/**
	 * This attribute maps to the column USER_NAME in the iprod_user_master
	 * table.
	 */
	protected String userName;

	/**
	 * This attribute maps to the column USER_TITLE in the iprod_user_master
	 * table.
	 */
	protected String userTitle;

	/**
	 * This attribute maps to the column FIRST_NAME in the iprod_user_master
	 * table.
	 */
	protected String firstName;

	/**
	 * This attribute maps to the column LAST_NAME in the iprod_user_master
	 * table.
	 */
	protected String lastName;

	/**
	 * This attribute maps to the column GENDER in the iprod_user_master table.
	 */
	protected String gender;

	/**
	 * This attribute maps to the column EMAIL_ID in the iprod_user_master
	 * table.
	 */
	protected String emailId;

	/**
	 * This attribute maps to the column CONTACT_NO in the iprod_user_master
	 * table.
	 */
	protected String contactNo;

	/**
	 * This attribute maps to the column USER_PASSWORD in the iprod_user_master
	 * table.
	 */
	protected String userPassword;

	/**
	 * This attribute maps to the column LAST_LOGIN_DETAILS in the
	 * iprod_user_master table.
	 */
	protected String lastLoginDetails;

	/**
	 * This attribute maps to the column IS_DELETED in the iprod_user_master
	 * table.
	 */
	protected short isDeleted;

	protected String oldPassword;
	protected String newPassword;
	private String iprodLicense;
	protected int roleId;	
	protected String prevUser;
	protected Boolean iProdDevMode = false;
	protected Boolean enableMacAddressLicense = false;
	protected Boolean enableMotherBoardLicense = false;
	protected Boolean enableHardDiskLicense = true;

	/* Public lists Here */
	public List<IprodUserMaster> selectMyProfileListG = new ArrayList<IprodUserMaster>();
	public List<IprodUserMaster> selectedUserListDetails = new ArrayList<IprodUserMaster>();
	 public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();
	/* FUNCTION TO CHECK THE WHETHER USER HAS LOGGED (i.e. WHETHER IT IS IN SESSION) */
	public String checkLogin(){	
		try{
			log.info("IprodUserMasterAction :: checkLogin");
	          Map<String, Object> session = ActionContext.getContext().getSession();                                                           
	                                                                
	          if(session.get(Consts.IPRODUSERROLEID) != null){
	            log.info("checkLogin :: Logged in users role in existing session is currently : "+session.get(Consts.IPRODUSERROLEID).toString());
	            
	            	  if(session.get(Consts.IPRODUSERROLEID).toString().equals("1") ){
	                        if(session.get(Consts.IPRODUSERID)!=null){
	                            log.info("checkLogin :: Normal User already logged in");
	                            return SUCCESS;                                                                                               
	                        }
	                        else{
	                            log.info("checkLogin :: Normal User not logged in!");
	                            addActionError("Normal User not logged in!");
	                            return ERROR;
	                        }
	                    }
    			    else if(session.get(Consts.IPRODUSERROLEID).toString().equals("2"))
    	            {
    	                log.info("checkLogin :: Supervisor logged in");
    	                return SUCCESS;       
    	            }
    			    else if(session.get(Consts.IPRODUSERID).toString().equals("3"))
    	            {
    	                log.info("checkLogin :: Trim1 User logged in");
    	                return "trim1success";       
    	            }
    			    else if(session.get(Consts.IPRODUSERID).toString().equals("10"))
	                    {
	                        log.info("checkLogin :: Trim2 User logged in");
	                        return "trim2success";         
	                    } 
	                    else if(session.get(Consts.IPRODUSERID).toString().equals("4"))
	                    {
	                        log.info("checkLogin :: PF1 User logged in");
	                        return "pf12success";         
	                    }                                                
	                    else if(session.get(Consts.IPRODUSERID).toString().equals("5"))
	                    {
                        log.info("checkLogin :: PF3 User logged in");
                        return "pf34success";    
	                                    
	                    }
	                    else if(session.get(Consts.IPRODUSERID).toString().equals("6"))
	                    {
                        log.info("checkLogin :: Final User logged in");
                        return "finalsuccess";     
	                                    
	                    }
	                    else if(session.get(Consts.IPRODUSERID).toString().equals("11"))
	                    {
                        log.info("checkLogin :: Final User logged in");
                        return "electricaltestingsuccess";     
	                                    
	                    }
	                    else{                                                                                                      
	                    	return SUCCESS;
	                    }
		     
			}
			else{                                                      
		      log.error("checkLogin :: Invalid License Key.Please contact sales@id4-realms.com to grab a key now!");
		      addActionError("Invalid License Key.Please contact sales@id4-realms.com to grab a key now!");
		      return "getLicense";
			}
		}catch (Exception e){
			log.error("checkLogin :: Exception :: " + e.getMessage() +" occured in checkLogin.");
		}
		return SUCCESS;
	}
	/* FUNCTION TO CHECK THE WHETHER USER HAS LOGGED in */
	public String loginUser() {
		log.info("IprodUserMasterAction: loginUser()");
		iProdDevMode = false;
		enableHardDiskLicense = true;
		String algo = "SHA";
		MessageDigest mdObject = null;
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.clear();
		String rId = "";
		String licenseEncryptedKey = "";

		if (enableMacAddressLicense == true) {
			MacAddress macAddress = new MacAddress();
			byte[] plainMacId = null;
			try {
				plainMacId = macAddress.getMacAddress().getBytes();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				mdObject = MessageDigest.getInstance(algo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			mdObject.reset();
			mdObject.update(plainMacId);
			byte[] encryptionMBSN = mdObject.digest();
			StringBuilder encryptedMACID = new StringBuilder();
			for (int j = 0; j < encryptionMBSN.length; j++) {
				if ((encryptionMBSN[j] & 0xff) < 0x10) {
					encryptedMACID.append("0");
				}
				encryptedMACID.append(Long.toString(encryptionMBSN[j] & 0xff,
						16));
			}
			licenseEncryptedKey = licenseEncryptedKey.concat(encryptedMACID
					.toString());
		}

		// log.info("License Encrpyted Key after mac address scan is : "+licenseEncryptedKey);

		if (enableMotherBoardLicense == true) {
			MotherboardSerialNumber motherboardSN = new MotherboardSerialNumber();
			String mSerialNo = motherboardSN.getMotherboardSN();
			byte[] plainMotherboardSN = mSerialNo.getBytes();
			try {
				mdObject = MessageDigest.getInstance(algo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			mdObject.reset();
			mdObject.update(plainMotherboardSN);
			byte[] encryptionMBSN = mdObject.digest();
			StringBuilder encryptedMotherboardSN = new StringBuilder();
			for (int j = 0; j < encryptionMBSN.length; j++) {
				if ((encryptionMBSN[j] & 0xff) < 0x10) {
					encryptedMotherboardSN.append("0");
				}
				encryptedMotherboardSN.append(Long.toString(
						encryptionMBSN[j] & 0xff, 16));
			}
			licenseEncryptedKey = licenseEncryptedKey
					.concat(encryptedMotherboardSN.toString());
		}

		// log.info("License Encrpyted Key after motherboard scan is : "+licenseEncryptedKey);

		if (enableHardDiskLicense == true) {
			HardDiskSerialNumber harddiskSN = new HardDiskSerialNumber();
			String hddSerialNo = harddiskSN.getHardDiskSerialNumber("C");
			// log.info("Plain hard disk serial number is : "+hddSerialNo);
			byte[] plainHardDiskSN = hddSerialNo.getBytes();
			try {
				mdObject = MessageDigest.getInstance(algo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			mdObject.reset();
			mdObject.update(plainHardDiskSN);
			byte[] encryptionHDDSN = mdObject.digest();
			StringBuilder encryptedHardDiskSN = new StringBuilder();
			for (int j = 0; j < encryptionHDDSN.length; j++) {
				if ((encryptionHDDSN[j] & 0xff) < 0x10) {
					encryptedHardDiskSN.append("0");
				}
				encryptedHardDiskSN.append(Long.toString(
						encryptionHDDSN[j] & 0xff, 16));
			}
			licenseEncryptedKey = licenseEncryptedKey
					.concat(encryptedHardDiskSN.toString());
		}

		// Hard coded for pluscon
		// licenseEncryptedKey = "2dad5972c98286a31332bc788d8afd05fca1a0cc";
		// log.info("Hard disk serial entered from user is : "+this.iprodLicense);

		// log.info("licenseEncryptedKey :: "+
		// licenseEncryptedKey.toString());
		// log.info("iprodLicense :: " + this.getIprodLicense());
		if ((licenseEncryptedKey.toString().compareTo(this.getIprodLicense()) == 0)
				|| (iProdDevMode == true)) {
			if (session.isEmpty()) {
				try {
					IprodUserMasterDao userDao = DaoFactory.createIprodUserMasterDao();
					List<IprodUserMaster> userListL = new ArrayList<IprodUserMaster>();
					try {
						userListL = userDao.findWhereUserNameEquals(this.userName.toLowerCase().trim());// .toLowerCase().trim()
						log.info("loginUser :: Found User : "+ userListL);
						String userPassword3 = userPassword;
						log.info(" Setting the value of userPassword3 with entered password  : "+ userPassword3+ " and the user password is :  "+ userPassword);
						log.info(" Setting the password value to that entered by the user ");
						// ******************************* encryption code
						// *************************************
						String password = userPassword3;
						// log.info(" userPassword3  :  "+userPassword3+" and the Password  :  "+password);
						String algorithm = "SHA";
						byte[] plainText = password.getBytes();
						MessageDigest md = null;
						try {
							md = MessageDigest.getInstance(algorithm);
						} catch (Exception e) {
							e.printStackTrace();
						}
						md.reset();
						md.update(plainText);
						byte[] encodedPassword = md.digest();
						StringBuilder sb = new StringBuilder();
						for (int i = 0; i < encodedPassword.length; i++) {
							if ((encodedPassword[i] & 0xff) < 0x10) {
								sb.append("0");
							}
							sb.append(Long.toString(encodedPassword[i] & 0xff,
									16));
						}
						log.info("Plain    : " + password);
						log.info("Encrypted: " + sb.toString());
						userPassword3 = sb.toString();
						log.info("the cipher text thus generated for  "+ userPassword + "  =  "+ userPassword3);

						// ******************************* encryption code
						// *************************************
						if (userListL.size() > 0) {
							log.info("Password entered by user : "
									+ userPassword);
							log.info("userPassword3 : "
									+ userPassword3);
							log.info("Password in the database : "
									+ userListL.get(0).getUserPassword());
							if (userPassword3.equals(userListL.get(0)
									.getUserPassword())) {
								if (userListL.get(0).getIsDeleted() == 1) {
									addActionError("User has been suspended. Please contact the iPROD administrator");
									return ERROR;
								}
								// Set required session variables on successful
								// user login to Iprod
								else {
									log.info("Setting required session variables after successful login");
									Date date = new Date();
									session.put("IprodLoggedIn", true);
									session.put("Context", date);
									session.put(Consts.IPRODUSERID, userListL
											.get(0).getUserId());
								    session.put("IprodUserPassword", userListL
											.get(0).getUserPassword());
									//session.put("IprodUserRoleId",userListL.get(0).getRoleId());
									//rId = userListL.get(0).getRoleId();
									//session.put("IprodUserRoleName",userListL.get(0).getRoleName());
								    rId = userListL.get(0).getUserName();
								    log.info("rid :: " + rId);
									session.put("IprodUserName",
											userListL.get(0).getUserName());
									session.put("IprodFirstName", userListL
											.get(0).getFirstName());
									session.put("IprodLastName",
											userListL.get(0).getLastName());
									IprodUserMasterDao userMasterDao = DaoFactory
											.createIprodUserMasterDao();
									IprodUserMaster userMasterDto = new IprodUserMaster();
									IprodUserMasterPk userMasterPk = new IprodUserMasterPk();
									userMasterDto.setUserId(userListL.get(0)
											.getUserId());
									userMasterDto.setUserName(userListL.get(0)
											.getUserName());
									userMasterDto.setUserTitle(userListL.get(0)
											.getUserTitle());
									userMasterDto.setFirstName(userListL.get(0)
											.getFirstName());
									userMasterDto.setLastName(userListL.get(0)
											.getLastName());
									userMasterDto.setGender(userListL.get(0)
											.getGender());
									userMasterDto.setEmailId(userListL.get(0)
											.getEmailId());
									userMasterDto.setContactNo(userListL.get(0)
											.getContactNo());
									userMasterDto.setUserPassword(userListL
											.get(0).getUserPassword());
									userMasterDto.setLastLoginDetails(date
											.toString());
									userMasterDto.setIsDeleted(userListL.get(0)
											.getIsDeleted());
									userMasterPk.setUserId(userListL.get(0)
											.getUserId());
									try {
										userMasterDao.update(userMasterPk,
												userMasterDto);
									} catch (IprodUserMasterDaoException e) {
										e.printStackTrace();
									}
									log.info("rid2 :: " +rId);
									if (rId.equals("andon")) {
										System.out
												.println("Logged in an authorized andon user!");
										return "andonsuccess";
									} else {
										return SUCCESS;
									}
								}
							} else {
								log.info("User password does not match");
								addActionError("User password does not match");
								return ERROR;
							}
						} else {
							log.info("User does not exist");
							addActionError("User does not exist");
							return ERROR;

						}
					} catch (IprodUserMasterDaoException e) {
						// TODO catch block
						addActionError("IprodViewUserDetailsDaoException"
								+ e.getMessage());
						return ERROR;
					}
				} catch (NullPointerException e) {
					addActionError("User name and password must!");
					e.getMessage();
					return ERROR;
				}
			}
		} else {
			System.out
					.println("The iPROD Reports License installed seems to be invalid. Please contact sales@id4-realms.com");
			return "getLicense";
		}
		return SUCCESS;
	}

	/* FUNCTION TO LOGOUT THE LOGIN USER */
	public String logoutUser(){
		try{
			log.info("IprodUserMasterAction :: logoutUser");	
			Map<String, Object> session = ActionContext.getContext().getSession();
			if (session.get(Consts.IPRODUSERID) != null) {
				session.remove(Consts.IPRODUSERID);
				session.remove("IprodLoggedIn");
				session.remove("Context");
				session.remove("IprodUserPassword");
				session.remove(Consts.IPRODUSERROLEID);
				session.remove(Consts.IPRODUSERNAME);
				session.remove(Consts.IPRODUSERROLENAME);
				session.remove(Consts.IPRODFNAME);
				session.remove(Consts.IPRODLNAME);	
				session.remove(Consts.LICENSEKEY);
				addActionMessage("You have successfully Logged Out.");
			} else {
				log.info("logoutUser():: Inside Else");
				addActionError("Sorry!! You have to Login first");
				return "login";
			}
		}catch (Exception e){
			log.error("logoutUser :: Exception :: " + e.getMessage() +" occured in logoutUser.");
		}
		return SUCCESS;
	}

	/* FUNCTION TO LOGOUT THE LOGIN OPERATOR */
	public String logoutOperator() {
		log.info("IprodUserMasterAction: logoutUser()");
		Map<String, Object> session = ActionContext.getContext().getSession();
		log.info("SESSON ID :"+ session.get(Consts.IPRODUSERID) );
		if (session.get(Consts.IPRODUSERID) != null) {
			session.remove("IprodLoggedIn");
			session.remove("Context");
			session.remove(Consts.IPRODUSERID);
			session.remove("IprodUserPassword");
			session.remove("IprodUserRoleId");
			session.remove("IprodUserName");
			session.remove("IprodUserRoleName");
			session.remove("IprodFirstName");
			session.remove("IprodLastName");
			log.info("You have successfully logged out");
			addActionMessage("You have successfully Logged Out.");
			return SUCCESS;
		} else {
			addActionError("Sorry!! You have to Login first");
			return "operatorLogin";
		}
	}
	
	public String viewDetails(){
		try{
			log.info("IprodUserMasterAction :: viewDetails");	
			Map<String, Object> session = ActionContext.getContext().getSession();
			Integer userId=(Integer)session.get(Consts.IPRODUSERID);	
			if (session.get(Consts.IPRODUSERID) != null) {
			try {		
				IprodUserMasterDao iprodUserMasterDao = DaoFactory.createIprodUserMasterDao();
				List<IprodUserMaster> lineListDetails = iprodUserMasterDao.findWhereUserIdEquals(userId);
				for(int i=0;i<lineListDetails.size();i++)
				{
			        //addActionMessage(lineListDetails.get(i).getFirstName());				
				}
				selectedUserListDetails = lineListDetails;	
			} catch (IprodUserMasterDaoException e) {
				e.printStackTrace();
			   }
			}
			else
			{
				addActionError("User has to login first");//alert
			}
		}catch (Exception e){
			log.error("viewDetails :: Exception :: " + e.getMessage() +" occured in viewDetails.");
		}
		return SUCCESS;
	}


	/*
	 * FUNCTION IS USED TO CREATE A ENCRYPTED PASSWORD VIA ALGORITHM "SHA" USING
	 * MESSAGE DIGEST
	 */
	public String addNewUser() {
		try{
			log.info("IprodUserMasterAction :: addNewUser");	
			Map<String, Object> session = ActionContext.getContext().getSession();
			if (session.get(Consts.IPRODUSERID) != null) {
				if (existsUserName(this.userName) == true) {
					log.error("User Name already exists. Please choose another");
					return ERROR;
				} else {
					// ******************* Encryption of userPassword2 Starts here
					// *******************
					String userPassword2 = userPassword;
					String password = userPassword2;
					String algorithm = "SHA";
					byte[] plainText = password.getBytes();
					MessageDigest md = null;
					try {
						md = MessageDigest.getInstance(algorithm);
					} catch (Exception e) {
						addActionError("Cannot Find User to Add");
					}
					md.reset();
					md.update(plainText);
					byte[] encodedPassword = md.digest();
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < encodedPassword.length; i++) {
						if ((encodedPassword[i] & 0xff) < 0x10) {
							sb.append("0");
						}
						sb.append(Long.toString(encodedPassword[i] & 0xff, 16));
					}
					userPassword2 = sb.toString();
					IprodUserMasterDao userDao = DaoFactory.createIprodUserMasterDao();
					IprodUserMaster IprodUserMaster = new IprodUserMaster();
					
					Date dNow = new Date();
					SimpleDateFormat ft = new SimpleDateFormat ("dd MMM yyyy HH:mm:ss:mm");
					String currentDateTime = ft.format(dNow);
					
					userName = userName.substring(0, 1).toUpperCase()+ userName.substring(1).toLowerCase();
					IprodUserMaster.setUserName(userName);
					IprodUserMaster.setUserTitle(this.userTitle);
					firstName = firstName.substring(0, 1).toUpperCase()+ firstName.substring(1).toLowerCase();
					IprodUserMaster.setFirstName(this.firstName);
					lastName = lastName.substring(0, 1).toUpperCase()+ lastName.substring(1).toLowerCase();
					IprodUserMaster.setLastName(this.lastName);
					IprodUserMaster.setGender(this.gender);
					IprodUserMaster.setEmailId(this.emailId);
					IprodUserMaster.setContactNo(this.contactNo);
					IprodUserMaster.setUserPassword(userPassword2);
					IprodUserMaster.setLastLoginDetails(currentDateTime);
					IprodUserMaster.setIsDeleted((short) 0);
					IprodUserMasterPk userPk = new IprodUserMasterPk();
					userPk = userDao.insert(IprodUserMaster);
					session.put("NewUserId", userPk.getUserId());
					session.put("IprodUserName", this.userName);
					String username2 = (String) session.get("IprodUserName");
					log.info("username2 from session:" +username2);
					// Is there anything to catch an insert fail exception.
					// This needs to be implemented.
					addActionMessage("User "+this.userName+" has been successfully added");
				}
			} else {
				addActionError("Sorry!! You have to Login first");
				return "login";
			}
		}catch (Exception e){
			log.error("addNewUser :: Exception :: " + e.getMessage() +" occured in addNewUser.");
		}
		//addUserLoggerHistory();
		return SUCCESS;
		}
	
	public String updateUserDetails(){
		try{
			log.info("IprodUserMasterAction :: updateUserDetails");	
			Map<String, Object> session = ActionContext.getContext().getSession();
			if (session.get(Consts.IPRODUSERID) != null) {
				IprodUserMasterDao userDetailsDao = DaoFactory.createIprodUserMasterDao();
				try {
					userDetailsDao.updateWhereUserId(this.userName,this.userTitle,this.firstName,this.lastName,this.gender,this.emailId,this.contactNo,this.userId);
				} catch (IprodUserMasterDaoException e) {
					e.printStackTrace();
				}
				session.put("NewUserId", this.getUserId());
				
				IprodUserRoleDao userRoleDao = DaoFactory.createIprodUserRoleDao();
				try {
					userRoleDao.updateRoleIdWhereUserIdIs(this.roleId,this.userId);
				} catch (IprodUserRoleDaoException e) {
					e.printStackTrace();
				}			
			
			}
		}catch (Exception e){
			log.error("updateUserDetails :: Exception :: " + e.getMessage() +" occured in updateUserDetails.");
		}
		return SUCCESS;
	}	
	/* FUNCTION TO CHECK WHETHER THE USER EXITS */
	public boolean existsUserName(String uName) {
		log.info("IprodUserMasterAction: existsUserName()");
		IprodUserMasterDao userDao = DaoFactory.createIprodUserMasterDao();
		List<IprodUserMaster> userListL = new ArrayList<IprodUserMaster>();
		try {
			userListL = userDao.findWhereUserNameEqualsAndIsDeleted(uName.toString().trim(),(short)0);
		} catch (IprodUserMasterDaoException e) {
			addActionError("IprodUserMasterDaoException" + e.getMessage());
			return true;
		}

		if (userListL.size() > 0) {
			log.info("User name " + uName + " already exists");
			return true;
		} else {
			log.info("User name " + uName + " does not exists");
			return false;
		}
	}

	/* FUNCTION TO DELETE THE USER DETAILS */
	public String deleteUser() {
		log.info("IprodUserMasterAction: deleteUser()");
		Map<String, Object> session = ActionContext.getContext().getSession();
		if (session.get(Consts.IPRODUSERID) != null) {
			IprodUserMasterDao userMasterDao = DaoFactory
					.createIprodUserMasterDao();
			IprodUserMaster userDto = new IprodUserMaster();
			List<IprodUserMaster> userMasterListL = new ArrayList<IprodUserMaster>();
			try {
				userMasterListL = userMasterDao.findWhereUserIdEquals(this.userId);
				IprodUserMasterPk IprodUserMasterpk = new IprodUserMasterPk();
				IprodUserMasterpk.setUserId(this.userId);
				userDto.setUserId(this.userId);
				userDto.setUserName(userMasterListL.get(0).getUserName());
				userDto.setUserTitle(userMasterListL.get(0).getUserTitle());
				userDto.setFirstName(userMasterListL.get(0).getFirstName());
				userDto.setLastName(userMasterListL.get(0).getLastName());
				userDto.setGender(userMasterListL.get(0).getGender());
				userDto.setEmailId(userMasterListL.get(0).getEmailId());
				userDto.setContactNo(userMasterListL.get(0).getContactNo());
				userDto.setUserPassword(userMasterListL.get(0).getUserPassword());
				userDto.setLastLoginDetails(userMasterListL.get(0).getLastLoginDetails());
				userDto.setIsDeleted((short) 1);
				userMasterDao.update(IprodUserMasterpk, userDto);
				log.info("User id " + this.userId
						+ " has been deleted");
			} catch (IprodUserMasterDaoException e) {
				// TODO catch block
				addActionError("IprodUserMasterDaoException" + e.getMessage());
			}
			addActionMessage("User id " + this.userId + " has been deleted");
		} else {
			addActionError("Sorry!! You have to Login first");
			return "login";
		}
		return SUCCESS;

	}
	
	/* FUNCTION TO UPDATE THE USER DETAILS */
	public String updateUser(){	
		try{
			log.info("IprodUserMasterAction :: updateUser");	
			Map<String, Object> session = ActionContext.getContext().getSession();
			if (session.get(Consts.IPRODUSERID) != null) {
				IprodUserMasterDao IprodUserMasterDao = DaoFactory.createIprodUserMasterDao();
				try {
					List<IprodUserMaster> LineListDetails = IprodUserMasterDao.findWhereUserIdEquals(this.userId);
					selectedUserListDetails = LineListDetails;	
					log.info("selectedUserListDetails :: +selectedUserListDetails");
				} catch (IprodUserMasterDaoException e) {
					addActionError("Cannot Find User to Update");
				}
				addActionMessage("User updated Successfully !");
			} else {
				addActionError("Sorry!! You have to Login first");
				return "login";
			}
		}catch (Exception e){
			log.error("updateUser :: Exception :: " + e.getMessage() +" occured in updateUser.");
		}
		return SUCCESS;
	}

	/* FUNCTION TO UPDATE THE USER DETAILS 
	public String updateUser() throws IprodUserMasterDaoException {
		log.info("IprodUserMasterAction: updateUser()");
		Map<String, Object> session = ActionContext.getContext().getSession();
		if (session.get(Consts.IPRODUSERID) != null) {
			IprodUserMasterDao userMasterDao = DaoFactory
					.createIprodUserMasterDao();
			IprodUserMaster userMasterDto = new IprodUserMaster();
			List<IprodUserMaster> userListL = new ArrayList<IprodUserMaster>();
			userListL = userMasterDao.findWhereUserIdEquals(this.userId);
			if (userListL.size() > 0) {
				userMasterDto.setUserId(this.userId);
				userMasterDto.setUserName(userListL.get(0).getUserName());
				userMasterDto.setUserTitle(this.userTitle);
				userMasterDto.setFirstName(this.firstName);
				userMasterDto.setLastName(this.lastName);
				userMasterDto.setGender(this.gender);
				userMasterDto.setEmailId(this.emailId);
				userMasterDto.setContactNo(this.contactNo);
				userMasterDto.setUserPassword(userListL.get(0)
						.getUserPassword());
				userMasterDto.setLastLoginDetails(userListL.get(0)
						.getLastLoginDetails());
				userMasterDto.setIsDeleted(userListL.get(0).getIsDeleted());
				IprodUserMasterPk userMasterpk = new IprodUserMasterPk();
				userMasterpk.setUserId(this.userId);
				// log.info(userMasterpk.getUserId());
				userMasterDao.update(userMasterpk, userMasterDto);
				addActionMessage("User " + userMasterDto.getUserName()
						+ " has been updated");
				return SUCCESS;
			} else {
				addActionError("Sorry!! User to be updated does not exist");
				log.info("User to be updated does not exist");
				return ERROR;
			}
			// log.info("User "+userMasterDto.getUserName()+" has been updated");
		} else {
			addActionError("Sorry!! You have to Login first");
			return "login";
		}
	}
*/
	/*
	 * FUNCTION TO CHANGE THE PASSWORD.wE USE MESSAGE DIGEST TO CHANGE THE
	 * PASSWORD
	 */
	public String changeMyPassword() throws IprodUserMasterDaoException {
		log.info("IprodUserMasterAction: changeMyPassword()");
		Map<String, Object> session = ActionContext.getContext().getSession();
		if (session.get(Consts.IPRODUSERID) != null) {
			IprodUserMasterDao IprodUserMasterDao = DaoFactory
					.createIprodUserMasterDao();
			IprodUserMaster IprodUserMaster = new IprodUserMaster();
			IprodUserMasterPk IprodUserMasterpk = new IprodUserMasterPk();
			List<IprodUserMaster> userMasterListL = new ArrayList<IprodUserMaster>();
			log.info(this.userId);
			userMasterListL = IprodUserMasterDao
					.findWhereUserIdEquals(this.userId);
			if (userMasterListL.size() > 0) {
				String userPassword2 = null;
				String updatedPassword = null;
				String algorithm = "SHA";
				byte[] plainText = this.oldPassword.getBytes();
				byte[] plainText1 = this.userPassword.getBytes();
				MessageDigest md = null;
				MessageDigest md1 = null;
				try {
					md = MessageDigest.getInstance(algorithm);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					md1 = MessageDigest.getInstance(algorithm);
				} catch (Exception e) {
					e.printStackTrace();
				}
				md.reset();
				md1.reset();
				md.update(plainText);
				md1.update(plainText1);
				byte[] encodedPassword = md.digest();
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < encodedPassword.length; i++) {
					if ((encodedPassword[i] & 0xff) < 0x10) {
						sb.append("0");
					}
					sb.append(Long.toString(encodedPassword[i] & 0xff, 16));
				}
				userPassword2 = sb.toString();
				byte[] encodedPassword1 = md1.digest();
				StringBuilder sb1 = new StringBuilder();
				for (int i = 0; i < encodedPassword1.length; i++) {
					if ((encodedPassword1[i] & 0xff) < 0x10) {
						sb1.append("0");
					}
					sb1.append(Long.toString(encodedPassword1[i] & 0xff, 16));
				}

				updatedPassword = sb1.toString();

				if (userPassword2.equals(userMasterListL.get(0)
						.getUserPassword())) {
					if (this.newPassword.equals(this.userPassword)) {
						IprodUserMaster.setUserPassword(updatedPassword);
						IprodUserMasterpk.setUserId(this.userId);
						try {
							IprodUserMasterDao.updatePassword(
									IprodUserMasterpk, IprodUserMaster);
						} catch (IprodUserMasterDaoException e) {
							e.printStackTrace();
						}
						log.info("User " + this.userId
								+ " Password has been Updated");
						addActionMessage("User " + this.userId
								+ " Password has been Updated");
						return SUCCESS;
					} else {
						System.out
								.println("new password and confirm password doesn't match");
						addActionError("new password and confirm password doesn't match");
						return ERROR;
					}
				} else {
					log.info("Old Password doesn't match");
					addActionError("Old Password doesn't match");
					return ERROR;
				}

			} else {
				addActionError("Sorry!! User to be updated with new password does not exist");
				System.out
						.println("User to be updated with new password does not exist");
				return ERROR;
			}

		} else {
			addActionError("Sorry!! You have to Login first");
			return "login";
		}
	}

	/* FUNCTION IS TO UPDATE THE PROFILE */
	public String updateMyProfile() throws IprodUserMasterDaoException {
		log.info("IprodUserMasterAction: updateMyProfile()");
		Map<String, Object> session = ActionContext.getContext().getSession();
		if (session.get(Consts.IPRODUSERID) != null) {
			IprodUserMasterDao userDao = DaoFactory.createIprodUserMasterDao();
			IprodUserMaster userDto = new IprodUserMaster();
			IprodUserMasterPk userMasterPk = new IprodUserMasterPk();
			List<IprodUserMaster> userListL = new ArrayList<IprodUserMaster>();
			userListL = userDao.findWhereUserIdEquals(this.userId);
			if (userListL.size() > 0) {
				userDto.setUserId(this.userId);
				userDto.setUserName(userListL.get(0).getUserName());
				userDto.setUserTitle(this.userTitle);
				userDto.setFirstName(this.firstName);
				userDto.setLastName(this.lastName);
				userDto.setGender(this.gender);
				userDto.setEmailId(this.emailId);
				userDto.setContactNo(this.contactNo);
				userDto.setUserPassword(userListL.get(0).getUserPassword());
				userDto.setLastLoginDetails(userListL.get(0)
						.getLastLoginDetails());
				userDto.setIsDeleted(userListL.get(0).getIsDeleted());
				userMasterPk.setUserId(this.userId);
				try {
					userDao.update(userMasterPk, userDto);
					log.info("Your Profile has been Updated");
					addActionMessage("Your Profile has been Updated");
				} catch (IprodUserMasterDaoException e) {
					e.printStackTrace();
					addActionError("Your Profile has not been Updated");
				}
				return SUCCESS;
			} else {
				addActionError("Sorry!! User whose profile is being updated does not exist");
				log.info("User whose profile is being updated does not exist");
				return ERROR;
			}
		} else {
			addActionError("Sorry!! You have to Login first");
			return "login";
		}
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Method 'getUserName'
	 * 
	 * @return String
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Method 'setUserName'
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Method 'getUserTitle'
	 * 
	 * @return String
	 */
	public String getUserTitle() {
		return userTitle;
	}

	/**
	 * Method 'setUserTitle'
	 * 
	 * @param userTitle
	 */
	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}

	/**
	 * Method 'getFirstName'
	 * 
	 * @return String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Method 'setFirstName'
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Method 'getLastName'
	 * 
	 * @return String
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Method 'setLastName'
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Method 'getGender'
	 * 
	 * @return String
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Method 'setGender'
	 * 
	 * @param gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Method 'getEmailId'
	 * 
	 * @return String
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * Method 'setEmailId'
	 * 
	 * @param emailId
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Method 'getContactNo'
	 * 
	 * @return String
	 */
	public String getContactNo() {
		return contactNo;
	}

	/**
	 * Method 'setContactNo'
	 * 
	 * @param contactNo
	 */
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	/**
	 * Method 'getUserPassword'
	 * 
	 * @return String
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Method 'setUserPassword'
	 * 
	 * @param userPassword
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * Method 'getLastLoginDetails'
	 * 
	 * @return String
	 */
	public String getLastLoginDetails() {
		return lastLoginDetails;
	}

	/**
	 * Method 'setLastLoginDetails'
	 * 
	 * @param lastLoginDetails
	 */
	public void setLastLoginDetails(String lastLoginDetails) {
		this.lastLoginDetails = lastLoginDetails;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted() {
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Method 'setUserPassword'
	 * 
	 * @param userPassword
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Method 'setUserPassword'
	 * 
	 * @param userPassword
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getIprodLicense() {
		return iprodLicense;
	}

	public void setIprodLicense(String iprodLicense) {
		this.iprodLicense = iprodLicense;
	}
	
	 public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
			return hashMapCheckRoleModuleOperationListG;
		}
	
		public void setHashMapCheckRoleModuleOperationListG(
				HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
			this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
		}

}
