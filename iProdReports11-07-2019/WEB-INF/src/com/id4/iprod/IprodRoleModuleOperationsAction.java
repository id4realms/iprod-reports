package com.id4.iprod;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.dao.IprodModuleOperationMasterDao;
import com.id4.iprod.dao.IprodRoleModuleOperationsDao;
import com.id4.iprod.dao.IprodViewRoleModuleOperationsDao;
import com.id4.iprod.dto.IprodModuleOperationMaster;
import com.id4.iprod.dto.IprodRoleModuleOperations;
import com.id4.iprod.dto.IprodViewRoleModuleOperations;
import com.id4.iprod.exceptions.IprodModuleOperationMasterDaoException;
import com.id4.iprod.exceptions.IprodViewRoleModuleOperationsDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodRoleModuleOperationsAction extends ActionSupport{
	
	
	static Logger log = Logger.getLogger(IprodRoleMasterAction.class);
	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_role_module_operations table.
	 */
	protected int roleId;

	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_role_module_operations table.
	 */
	protected int moduleId;

	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_role_module_operations table.
	 */
	protected int operationId;

	/** 
	 * This attribute maps to the column IS_ALLOWED in the iprod_role_module_operations table.
	 */
	
	protected short isAllowed;
	

	public ArrayList<IprodRoleModuleOperations> roleModulePerms =  new ArrayList<IprodRoleModuleOperations>(); 
		public List<IprodViewRoleModuleOperations> iprodViewRoleModuleOperationsListG = new ArrayList<IprodViewRoleModuleOperations>();

/*	public String addRoleModuleOperations(){
		try{
			log.info("IprodRoleMasterAction :: addRolePermission");
			log.info("roleModulePerms are::"+roleModulePerms);
			Map<String, Object> session = ActionContext.getContext().getSession();
			if(session.get(Consts.IPRODUSERID)!= null)
		    {
			//	log.info("NewRoleId" + (Integer)session.get("NewRoleId")) ;
	
			for(int i = 0 ; i < roleModulePerms.size(); i++){				
				if(roleModulePerms.get(i) != null)
				{
					IprodRoleModuleOperationsDao iprodRoleModuleOperationsDao  = DaoFactory.createIprodRoleModuleOperationsDao();
					IprodRoleModuleOperations iprodRoleModuleOperations = new IprodRoleModuleOperations();
					iprodRoleModuleOperations.setRoleId((Integer)session.get("NewRoleId"));
					iprodRoleModuleOperations.setModuleId(roleModulePerms.get(i).getModuleId());
					iprodRoleModuleOperations.setOperationId(roleModulePerms.get(i).getOperationId());
					iprodRoleModuleOperations.setIsAllowed(roleModulePerms.get(i).getIsAllowed());
					try {
						iprodRoleModuleOperationsDao.insert(iprodRoleModuleOperations);
					} catch (Exception e) {
						log.info("Exception while inserting permissions" +e.getMessage());
						e.printStackTrace();
					}
				}
			}
				
				
			}
		}catch (Exception e){
			log.info("addRolePermission :: Exception :: " + e.getMessage() +" occured in addRolePermission.");
		}
		return SUCCESS;	
	}*/
	
	public String addRoleModuleOperations(){
		try{
			log.info("IprodRoleMasterAction :: addRolePermission");
			log.info("roleModulePerms are::"+roleModulePerms);
			Map<String, Object> session = ActionContext.getContext().getSession();
			if(session.get(Consts.IPRODUSERID)!= null)
		    {
				IprodModuleOperationMasterDao 	iprodModuleOperationMasterDao =  DaoFactory.createIprodModuleOperationMasterDao();
				;
				try {
					List<IprodModuleOperationMaster> iprodModuleOperationMasterListL  = iprodModuleOperationMasterDao.findWhereIsApplicableEquals((short)1);
					if(iprodModuleOperationMasterListL.size() > 0)
					{
						for(int i = 0 ; i < iprodModuleOperationMasterListL.size(); i++){		
							IprodRoleModuleOperationsDao iprodRoleModuleOperationsDao  = DaoFactory.createIprodRoleModuleOperationsDao();
							IprodRoleModuleOperations iprodRoleModuleOperations = new IprodRoleModuleOperations();
							iprodRoleModuleOperations.setRoleId((Integer)session.get("NewRoleId"));
							iprodRoleModuleOperations.setModuleId(iprodModuleOperationMasterListL.get(i).getModuleId());
							log.info("Setting ModuleId::"+iprodModuleOperationMasterListL.get(i).getModuleId());
							iprodRoleModuleOperations.setOperationId(iprodModuleOperationMasterListL.get(i).getOperationId());
							log.info("Setting OperationId::"+iprodModuleOperationMasterListL.get(i).getOperationId());

							for(int j = 0 ; j < roleModulePerms.size(); j++){
								if(roleModulePerms.get(j) != null)
								{
									iprodRoleModuleOperations.setIsAllowed(roleModulePerms.get(j).getIsAllowed());
									log.info("Setting IsAllowed::"+ roleModulePerms.get(j).getIsAllowed());

								}
							}
							try {
								iprodRoleModuleOperationsDao.insert(iprodRoleModuleOperations);
							} catch (Exception e) {
								log.error("Exception while inserting permissions" +e.getMessage());
								e.printStackTrace();
							}
						}
					}
				} catch (IprodModuleOperationMasterDaoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			}
		}catch (Exception e){
			log.error("addRolePermission :: Exception :: " + e.getMessage() +" occured in addRolePermission.");
		}
		return SUCCESS;	
	}
	
	public String getAllRolePermissionsDetails(){
			log.info("IprodRoleMasterAction :: getAllRolePermissionsDetails");
			Map<String, Object> session = ActionContext.getContext().getSession();
			IprodViewRoleModuleOperationsDao iprodViewRoleModuleOperationsDao = DaoFactory.createIprodViewRoleModuleOperationsDao();
			List<IprodViewRoleModuleOperations> iprodViewRoleModuleOperationsListL = new ArrayList<IprodViewRoleModuleOperations>();
			if(this.roleId != 0)
			{
			  try {	
				  iprodViewRoleModuleOperationsListL = iprodViewRoleModuleOperationsDao.findWhereRoleIdEquals(this.roleId);
			  	  if(iprodViewRoleModuleOperationsListL.size() > 0){				  		  				  		  				  			  
			  		iprodViewRoleModuleOperationsListG = iprodViewRoleModuleOperationsListL;
				  }else{			  	
					  addActionError("");
				  	  return ERROR;
			  	  }
			  } catch (IprodViewRoleModuleOperationsDaoException e) {
				addActionError("IprodViewRoleModuleOperationsDaoException"+e.getMessage());
				return ERROR;
			  }
			}		
		return SUCCESS;	
	}


	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getOperationId() {
		return operationId;
	}

	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}

	public short getIsAllowed() {
		return isAllowed;
	}

	public void setIsAllowed(short isAllowed) {
		this.isAllowed = isAllowed;
	}


}
