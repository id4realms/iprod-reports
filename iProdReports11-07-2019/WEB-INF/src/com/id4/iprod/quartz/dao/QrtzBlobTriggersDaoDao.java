package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzBlobTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzBlobTriggersDao;
import com.id4.iprod.quartz.exceptions.QrtzBlobTriggersDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzBlobTriggersDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(QrtzBlobTriggersDao dto);

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria ''.
	 */
	public List<QrtzBlobTriggersDao> findAll() throws QrtzBlobTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzBlobTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzBlobTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	public List<QrtzBlobTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzBlobTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzBlobTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzBlobTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'BLOB_DATA = :blobData'.
	 */
	public List<QrtzBlobTriggersDao> findWhereBlobDataEquals(byte[] blobData) throws QrtzBlobTriggersDaoDaoException;

}
