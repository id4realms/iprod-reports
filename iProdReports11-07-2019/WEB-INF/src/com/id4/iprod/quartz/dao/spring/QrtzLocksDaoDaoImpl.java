package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzLocksDaoDao;
import com.id4.iprod.quartz.dto.QrtzLocksDao;
import com.id4.iprod.quartz.dto.QrtzLocksDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzLocksDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzLocksDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzLocksDao>, QrtzLocksDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzLocksDaoPk
	 */
	public QrtzLocksDaoPk insert(QrtzLocksDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, LOCK_NAME ) VALUES ( ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getLockName()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_LOCKS table.
	 */
	public void update(QrtzLocksDaoPk pk, QrtzLocksDao dto) throws QrtzLocksDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, LOCK_NAME = ? WHERE SCHED_NAME = ? AND LOCK_NAME = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getLockName(),pk.getSchedName(),pk.getLockName() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_LOCKS table.
	 */
	@Transactional
	public void delete(QrtzLocksDaoPk pk) throws QrtzLocksDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND LOCK_NAME = ?",pk.getSchedName(),pk.getLockName());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzLocksDao
	 */
	public QrtzLocksDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzLocksDao dto = new QrtzLocksDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setLockName( rs.getString( 2 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_LOCKS";
	}

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria 'SCHED_NAME = :schedName AND LOCK_NAME = :lockName'.
	 */
	@Transactional
	public QrtzLocksDao findByPrimaryKey(String schedName, String lockName) throws QrtzLocksDaoDaoException
	{
		try {
			List<QrtzLocksDao> list = jdbcTemplate.query("SELECT SCHED_NAME, LOCK_NAME FROM " + getTableName() + " WHERE SCHED_NAME = ? AND LOCK_NAME = ?", this,schedName,lockName);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzLocksDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzLocksDao> findAll() throws QrtzLocksDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, LOCK_NAME FROM " + getTableName() + " ORDER BY SCHED_NAME, LOCK_NAME", this);
		}
		catch (Exception e) {
			throw new QrtzLocksDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzLocksDao> findWhereSchedNameEquals(String schedName) throws QrtzLocksDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, LOCK_NAME FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzLocksDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria 'LOCK_NAME = :lockName'.
	 */
	@Transactional
	public List<QrtzLocksDao> findWhereLockNameEquals(String lockName) throws QrtzLocksDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, LOCK_NAME FROM " + getTableName() + " WHERE LOCK_NAME = ? ORDER BY LOCK_NAME", this,lockName);
		}
		catch (Exception e) {
			throw new QrtzLocksDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_LOCKS table that matches the specified primary-key value.
	 */
	public QrtzLocksDao findByPrimaryKey(QrtzLocksDaoPk pk) throws QrtzLocksDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getLockName() );
	}

}
