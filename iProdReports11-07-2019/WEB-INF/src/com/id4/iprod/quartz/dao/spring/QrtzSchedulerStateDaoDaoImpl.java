package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzSchedulerStateDaoDao;
import com.id4.iprod.quartz.dto.QrtzSchedulerStateDao;
import com.id4.iprod.quartz.dto.QrtzSchedulerStateDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzSchedulerStateDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzSchedulerStateDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzSchedulerStateDao>, QrtzSchedulerStateDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzSchedulerStateDaoPk
	 */
	public QrtzSchedulerStateDaoPk insert(QrtzSchedulerStateDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, INSTANCE_NAME, LAST_CHECKIN_TIME, CHECKIN_INTERVAL ) VALUES ( ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getInstanceName(),dto.getLastCheckinTime(),dto.getCheckinInterval()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_SCHEDULER_STATE table.
	 */
	public void update(QrtzSchedulerStateDaoPk pk, QrtzSchedulerStateDao dto) throws QrtzSchedulerStateDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, INSTANCE_NAME = ?, LAST_CHECKIN_TIME = ?, CHECKIN_INTERVAL = ? WHERE SCHED_NAME = ? AND INSTANCE_NAME = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getInstanceName(),dto.getLastCheckinTime(),dto.getCheckinInterval(),pk.getSchedName(),pk.getInstanceName() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_SCHEDULER_STATE table.
	 */
	@Transactional
	public void delete(QrtzSchedulerStateDaoPk pk) throws QrtzSchedulerStateDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND INSTANCE_NAME = ?",pk.getSchedName(),pk.getInstanceName());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzSchedulerStateDao
	 */
	public QrtzSchedulerStateDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzSchedulerStateDao dto = new QrtzSchedulerStateDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setInstanceName( rs.getString( 2 ) );
		dto.setLastCheckinTime( rs.getLong( 3 ) );
		dto.setCheckinInterval( rs.getLong( 4 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_SCHEDULER_STATE";
	}

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'SCHED_NAME = :schedName AND INSTANCE_NAME = :instanceName'.
	 */
	@Transactional
	public QrtzSchedulerStateDao findByPrimaryKey(String schedName, String instanceName) throws QrtzSchedulerStateDaoDaoException
	{
		try {
			List<QrtzSchedulerStateDao> list = jdbcTemplate.query("SELECT SCHED_NAME, INSTANCE_NAME, LAST_CHECKIN_TIME, CHECKIN_INTERVAL FROM " + getTableName() + " WHERE SCHED_NAME = ? AND INSTANCE_NAME = ?", this,schedName,instanceName);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzSchedulerStateDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzSchedulerStateDao> findAll() throws QrtzSchedulerStateDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, INSTANCE_NAME, LAST_CHECKIN_TIME, CHECKIN_INTERVAL FROM " + getTableName() + " ORDER BY SCHED_NAME, INSTANCE_NAME", this);
		}
		catch (Exception e) {
			throw new QrtzSchedulerStateDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzSchedulerStateDao> findWhereSchedNameEquals(String schedName) throws QrtzSchedulerStateDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, INSTANCE_NAME, LAST_CHECKIN_TIME, CHECKIN_INTERVAL FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzSchedulerStateDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'INSTANCE_NAME = :instanceName'.
	 */
	@Transactional
	public List<QrtzSchedulerStateDao> findWhereInstanceNameEquals(String instanceName) throws QrtzSchedulerStateDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, INSTANCE_NAME, LAST_CHECKIN_TIME, CHECKIN_INTERVAL FROM " + getTableName() + " WHERE INSTANCE_NAME = ? ORDER BY INSTANCE_NAME", this,instanceName);
		}
		catch (Exception e) {
			throw new QrtzSchedulerStateDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'LAST_CHECKIN_TIME = :lastCheckinTime'.
	 */
	@Transactional
	public List<QrtzSchedulerStateDao> findWhereLastCheckinTimeEquals(long lastCheckinTime) throws QrtzSchedulerStateDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, INSTANCE_NAME, LAST_CHECKIN_TIME, CHECKIN_INTERVAL FROM " + getTableName() + " WHERE LAST_CHECKIN_TIME = ? ORDER BY LAST_CHECKIN_TIME", this,lastCheckinTime);
		}
		catch (Exception e) {
			throw new QrtzSchedulerStateDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'CHECKIN_INTERVAL = :checkinInterval'.
	 */
	@Transactional
	public List<QrtzSchedulerStateDao> findWhereCheckinIntervalEquals(long checkinInterval) throws QrtzSchedulerStateDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, INSTANCE_NAME, LAST_CHECKIN_TIME, CHECKIN_INTERVAL FROM " + getTableName() + " WHERE CHECKIN_INTERVAL = ? ORDER BY CHECKIN_INTERVAL", this,checkinInterval);
		}
		catch (Exception e) {
			throw new QrtzSchedulerStateDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_SCHEDULER_STATE table that matches the specified primary-key value.
	 */
	public QrtzSchedulerStateDao findByPrimaryKey(QrtzSchedulerStateDaoPk pk) throws QrtzSchedulerStateDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getInstanceName() );
	}

}
