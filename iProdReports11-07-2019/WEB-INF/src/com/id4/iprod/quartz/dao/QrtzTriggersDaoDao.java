package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzTriggersDao;
import com.id4.iprod.quartz.dto.QrtzTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzTriggersDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzTriggersDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzTriggersDaoPk
	 */
	public QrtzTriggersDaoPk insert(QrtzTriggersDao dto);

	/** 
	 * Updates a single row in the QRTZ_TRIGGERS table.
	 */
	public void update(QrtzTriggersDaoPk pk, QrtzTriggersDao dto) throws QrtzTriggersDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_TRIGGERS table.
	 */
	public void delete(QrtzTriggersDaoPk pk) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public QrtzTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria ''.
	 */
	public List<QrtzTriggersDao> findAll() throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND JOB_NAME = :jobName AND JOB_GROUP = :jobGroup'.
	 */
	public List<QrtzTriggersDao> findByQrtzJobDetails(String schedName, String jobName, String jobGroup) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	public List<QrtzTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'JOB_NAME = :jobName'.
	 */
	public List<QrtzTriggersDao> findWhereJobNameEquals(String jobName) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'JOB_GROUP = :jobGroup'.
	 */
	public List<QrtzTriggersDao> findWhereJobGroupEquals(String jobGroup) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'DESCRIPTION = :description'.
	 */
	public List<QrtzTriggersDao> findWhereDescriptionEquals(String description) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'NEXT_FIRE_TIME = :nextFireTime'.
	 */
	public List<QrtzTriggersDao> findWhereNextFireTimeEquals(long nextFireTime) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'PREV_FIRE_TIME = :prevFireTime'.
	 */
	public List<QrtzTriggersDao> findWherePrevFireTimeEquals(long prevFireTime) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'PRIORITY = :priority'.
	 */
	public List<QrtzTriggersDao> findWherePriorityEquals(int priority) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_STATE = :triggerState'.
	 */
	public List<QrtzTriggersDao> findWhereTriggerStateEquals(String triggerState) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_TYPE = :triggerType'.
	 */
	public List<QrtzTriggersDao> findWhereTriggerTypeEquals(String triggerType) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'START_TIME = :startTime'.
	 */
	public List<QrtzTriggersDao> findWhereStartTimeEquals(long startTime) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'END_TIME = :endTime'.
	 */
	public List<QrtzTriggersDao> findWhereEndTimeEquals(long endTime) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'CALENDAR_NAME = :calendarName'.
	 */
	public List<QrtzTriggersDao> findWhereCalendarNameEquals(String calendarName) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'MISFIRE_INSTR = :misfireInstr'.
	 */
	public List<QrtzTriggersDao> findWhereMisfireInstrEquals(short misfireInstr) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'JOB_DATA = :jobData'.
	 */
	public List<QrtzTriggersDao> findWhereJobDataEquals(byte[] jobData) throws QrtzTriggersDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzTriggersDao findByPrimaryKey(QrtzTriggersDaoPk pk) throws QrtzTriggersDaoDaoException;

}
