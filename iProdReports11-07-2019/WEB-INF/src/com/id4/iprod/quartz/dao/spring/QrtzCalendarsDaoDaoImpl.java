package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzCalendarsDaoDao;
import com.id4.iprod.quartz.dto.QrtzCalendarsDao;
import com.id4.iprod.quartz.dto.QrtzCalendarsDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzCalendarsDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzCalendarsDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzCalendarsDao>, QrtzCalendarsDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzCalendarsDaoPk
	 */
	public QrtzCalendarsDaoPk insert(QrtzCalendarsDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, CALENDAR_NAME, CALENDAR ) VALUES ( ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getCalendarName(),dto.getCalendar()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_CALENDARS table.
	 */
	public void update(QrtzCalendarsDaoPk pk, QrtzCalendarsDao dto) throws QrtzCalendarsDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, CALENDAR_NAME = ?, CALENDAR = ? WHERE SCHED_NAME = ? AND CALENDAR_NAME = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getCalendarName(),dto.getCalendar(),pk.getSchedName(),pk.getCalendarName() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_CALENDARS table.
	 */
	@Transactional
	public void delete(QrtzCalendarsDaoPk pk) throws QrtzCalendarsDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND CALENDAR_NAME = ?",pk.getSchedName(),pk.getCalendarName());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzCalendarsDao
	 */
	public QrtzCalendarsDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzCalendarsDao dto = new QrtzCalendarsDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setCalendarName( rs.getString( 2 ) );
		dto.setCalendar( super.getBlobColumn(rs, 3 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_CALENDARS";
	}

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'SCHED_NAME = :schedName AND CALENDAR_NAME = :calendarName'.
	 */
	@Transactional
	public QrtzCalendarsDao findByPrimaryKey(String schedName, String calendarName) throws QrtzCalendarsDaoDaoException
	{
		try {
			List<QrtzCalendarsDao> list = jdbcTemplate.query("SELECT SCHED_NAME, CALENDAR_NAME, CALENDAR FROM " + getTableName() + " WHERE SCHED_NAME = ? AND CALENDAR_NAME = ?", this,schedName,calendarName);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzCalendarsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzCalendarsDao> findAll() throws QrtzCalendarsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, CALENDAR_NAME, CALENDAR FROM " + getTableName() + " ORDER BY SCHED_NAME, CALENDAR_NAME", this);
		}
		catch (Exception e) {
			throw new QrtzCalendarsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzCalendarsDao> findWhereSchedNameEquals(String schedName) throws QrtzCalendarsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, CALENDAR_NAME, CALENDAR FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzCalendarsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'CALENDAR_NAME = :calendarName'.
	 */
	@Transactional
	public List<QrtzCalendarsDao> findWhereCalendarNameEquals(String calendarName) throws QrtzCalendarsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, CALENDAR_NAME, CALENDAR FROM " + getTableName() + " WHERE CALENDAR_NAME = ? ORDER BY CALENDAR_NAME", this,calendarName);
		}
		catch (Exception e) {
			throw new QrtzCalendarsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'CALENDAR = :calendar'.
	 */
	@Transactional
	public List<QrtzCalendarsDao> findWhereCalendarEquals(byte[] calendar) throws QrtzCalendarsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, CALENDAR_NAME, CALENDAR FROM " + getTableName() + " WHERE CALENDAR = ? ORDER BY CALENDAR", this,calendar);
		}
		catch (Exception e) {
			throw new QrtzCalendarsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_CALENDARS table that matches the specified primary-key value.
	 */
	public QrtzCalendarsDao findByPrimaryKey(QrtzCalendarsDaoPk pk) throws QrtzCalendarsDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getCalendarName() );
	}

}
