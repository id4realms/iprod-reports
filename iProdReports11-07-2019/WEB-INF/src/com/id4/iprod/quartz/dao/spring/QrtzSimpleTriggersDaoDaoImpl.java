package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzSimpleTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzSimpleTriggersDao;
import com.id4.iprod.quartz.dto.QrtzSimpleTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzSimpleTriggersDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzSimpleTriggersDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzSimpleTriggersDao>, QrtzSimpleTriggersDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzSimpleTriggersDaoPk
	 */
	public QrtzSimpleTriggersDaoPk insert(QrtzSimpleTriggersDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED ) VALUES ( ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getRepeatCount(),dto.getRepeatInterval(),dto.getTimesTriggered()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	public void update(QrtzSimpleTriggersDaoPk pk, QrtzSimpleTriggersDao dto) throws QrtzSimpleTriggersDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, TRIGGER_NAME = ?, TRIGGER_GROUP = ?, REPEAT_COUNT = ?, REPEAT_INTERVAL = ?, TIMES_TRIGGERED = ? WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getRepeatCount(),dto.getRepeatInterval(),dto.getTimesTriggered(),pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	@Transactional
	public void delete(QrtzSimpleTriggersDaoPk pk) throws QrtzSimpleTriggersDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?",pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzSimpleTriggersDao
	 */
	public QrtzSimpleTriggersDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzSimpleTriggersDao dto = new QrtzSimpleTriggersDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setTriggerName( rs.getString( 2 ) );
		dto.setTriggerGroup( rs.getString( 3 ) );
		dto.setRepeatCount( rs.getLong( 4 ) );
		dto.setRepeatInterval( rs.getLong( 5 ) );
		dto.setTimesTriggered( rs.getLong( 6 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_SIMPLE_TRIGGERS";
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public QrtzSimpleTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			List<QrtzSimpleTriggersDao> list = jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerName,triggerGroup);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findAll() throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " ORDER BY SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP", this);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findByQrtzTriggers(String schedName, String triggerName, String triggerGroup) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerName,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE TRIGGER_NAME = ? ORDER BY TRIGGER_NAME", this,triggerName);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE TRIGGER_GROUP = ? ORDER BY TRIGGER_GROUP", this,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'REPEAT_COUNT = :repeatCount'.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findWhereRepeatCountEquals(long repeatCount) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE REPEAT_COUNT = ? ORDER BY REPEAT_COUNT", this,repeatCount);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'REPEAT_INTERVAL = :repeatInterval'.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findWhereRepeatIntervalEquals(long repeatInterval) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE REPEAT_INTERVAL = ? ORDER BY REPEAT_INTERVAL", this,repeatInterval);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'TIMES_TRIGGERED = :timesTriggered'.
	 */
	@Transactional
	public List<QrtzSimpleTriggersDao> findWhereTimesTriggeredEquals(long timesTriggered) throws QrtzSimpleTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, REPEAT_COUNT, REPEAT_INTERVAL, TIMES_TRIGGERED FROM " + getTableName() + " WHERE TIMES_TRIGGERED = ? ORDER BY TIMES_TRIGGERED", this,timesTriggered);
		}
		catch (Exception e) {
			throw new QrtzSimpleTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_SIMPLE_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzSimpleTriggersDao findByPrimaryKey(QrtzSimpleTriggersDaoPk pk) throws QrtzSimpleTriggersDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getTriggerName(), pk.getTriggerGroup() );
	}

}
