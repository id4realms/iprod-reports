package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzSimpropTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzSimpropTriggersDao;
import com.id4.iprod.quartz.dto.QrtzSimpropTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzSimpropTriggersDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzSimpropTriggersDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzSimpropTriggersDaoPk
	 */
	public QrtzSimpropTriggersDaoPk insert(QrtzSimpropTriggersDao dto);

	/** 
	 * Updates a single row in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	public void update(QrtzSimpropTriggersDaoPk pk, QrtzSimpropTriggersDao dto) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	public void delete(QrtzSimpropTriggersDaoPk pk) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public QrtzSimpropTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria ''.
	 */
	public List<QrtzSimpropTriggersDao> findAll() throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzSimpropTriggersDao> findByQrtzTriggers(String schedName, String triggerName, String triggerGroup) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'STR_PROP_1 = :strProp1'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereStrProp1Equals(String strProp1) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'STR_PROP_2 = :strProp2'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereStrProp2Equals(String strProp2) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'STR_PROP_3 = :strProp3'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereStrProp3Equals(String strProp3) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'INT_PROP_1 = :intProp1'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereIntProp1Equals(int intProp1) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'INT_PROP_2 = :intProp2'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereIntProp2Equals(int intProp2) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'LONG_PROP_1 = :longProp1'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereLongProp1Equals(long longProp1) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'LONG_PROP_2 = :longProp2'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereLongProp2Equals(long longProp2) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'DEC_PROP_1 = :decProp1'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereDecProp1Equals(float decProp1) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'DEC_PROP_2 = :decProp2'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereDecProp2Equals(float decProp2) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'BOOL_PROP_1 = :boolProp1'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereBoolProp1Equals(String boolProp1) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'BOOL_PROP_2 = :boolProp2'.
	 */
	public List<QrtzSimpropTriggersDao> findWhereBoolProp2Equals(String boolProp2) throws QrtzSimpropTriggersDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_SIMPROP_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzSimpropTriggersDao findByPrimaryKey(QrtzSimpropTriggersDaoPk pk) throws QrtzSimpropTriggersDaoDaoException;

}
