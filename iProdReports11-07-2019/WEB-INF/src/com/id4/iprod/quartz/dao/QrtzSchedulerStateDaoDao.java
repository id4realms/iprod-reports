package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzSchedulerStateDaoDao;
import com.id4.iprod.quartz.dto.QrtzSchedulerStateDao;
import com.id4.iprod.quartz.dto.QrtzSchedulerStateDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzSchedulerStateDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzSchedulerStateDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzSchedulerStateDaoPk
	 */
	public QrtzSchedulerStateDaoPk insert(QrtzSchedulerStateDao dto);

	/** 
	 * Updates a single row in the QRTZ_SCHEDULER_STATE table.
	 */
	public void update(QrtzSchedulerStateDaoPk pk, QrtzSchedulerStateDao dto) throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_SCHEDULER_STATE table.
	 */
	public void delete(QrtzSchedulerStateDaoPk pk) throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'SCHED_NAME = :schedName AND INSTANCE_NAME = :instanceName'.
	 */
	public QrtzSchedulerStateDao findByPrimaryKey(String schedName, String instanceName) throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria ''.
	 */
	public List<QrtzSchedulerStateDao> findAll() throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzSchedulerStateDao> findWhereSchedNameEquals(String schedName) throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'INSTANCE_NAME = :instanceName'.
	 */
	public List<QrtzSchedulerStateDao> findWhereInstanceNameEquals(String instanceName) throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'LAST_CHECKIN_TIME = :lastCheckinTime'.
	 */
	public List<QrtzSchedulerStateDao> findWhereLastCheckinTimeEquals(long lastCheckinTime) throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SCHEDULER_STATE table that match the criteria 'CHECKIN_INTERVAL = :checkinInterval'.
	 */
	public List<QrtzSchedulerStateDao> findWhereCheckinIntervalEquals(long checkinInterval) throws QrtzSchedulerStateDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_SCHEDULER_STATE table that matches the specified primary-key value.
	 */
	public QrtzSchedulerStateDao findByPrimaryKey(QrtzSchedulerStateDaoPk pk) throws QrtzSchedulerStateDaoDaoException;

}
