package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzFiredTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzFiredTriggersDao;
import com.id4.iprod.quartz.dto.QrtzFiredTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzFiredTriggersDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzFiredTriggersDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzFiredTriggersDao>, QrtzFiredTriggersDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzFiredTriggersDaoPk
	 */
	public QrtzFiredTriggersDaoPk insert(QrtzFiredTriggersDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getEntryId(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getInstanceName(),dto.getFiredTime(),dto.getSchedTime(),dto.getPriority(),dto.getState(),dto.getJobName(),dto.getJobGroup(),dto.getIsNonconcurrent(),dto.getRequestsRecovery()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_FIRED_TRIGGERS table.
	 */
	public void update(QrtzFiredTriggersDaoPk pk, QrtzFiredTriggersDao dto) throws QrtzFiredTriggersDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, ENTRY_ID = ?, TRIGGER_NAME = ?, TRIGGER_GROUP = ?, INSTANCE_NAME = ?, FIRED_TIME = ?, SCHED_TIME = ?, PRIORITY = ?, STATE = ?, JOB_NAME = ?, JOB_GROUP = ?, IS_NONCONCURRENT = ?, REQUESTS_RECOVERY = ? WHERE SCHED_NAME = ? AND ENTRY_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getEntryId(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getInstanceName(),dto.getFiredTime(),dto.getSchedTime(),dto.getPriority(),dto.getState(),dto.getJobName(),dto.getJobGroup(),dto.getIsNonconcurrent(),dto.getRequestsRecovery(),pk.getSchedName(),pk.getEntryId() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_FIRED_TRIGGERS table.
	 */
	@Transactional
	public void delete(QrtzFiredTriggersDaoPk pk) throws QrtzFiredTriggersDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND ENTRY_ID = ?",pk.getSchedName(),pk.getEntryId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzFiredTriggersDao
	 */
	public QrtzFiredTriggersDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzFiredTriggersDao dto = new QrtzFiredTriggersDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setEntryId( rs.getString( 2 ) );
		dto.setTriggerName( rs.getString( 3 ) );
		dto.setTriggerGroup( rs.getString( 4 ) );
		dto.setInstanceName( rs.getString( 5 ) );
		dto.setFiredTime( rs.getLong( 6 ) );
		dto.setSchedTime( rs.getLong( 7 ) );
		dto.setPriority( rs.getInt( 8 ) );
		dto.setState( rs.getString( 9 ) );
		dto.setJobName( rs.getString( 10 ) );
		dto.setJobGroup( rs.getString( 11 ) );
		dto.setIsNonconcurrent( rs.getString( 12 ) );
		dto.setRequestsRecovery( rs.getString( 13 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_FIRED_TRIGGERS";
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND ENTRY_ID = :entryId'.
	 */
	@Transactional
	public QrtzFiredTriggersDao findByPrimaryKey(String schedName, String entryId) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			List<QrtzFiredTriggersDao> list = jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE SCHED_NAME = ? AND ENTRY_ID = ?", this,schedName,entryId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findAll() throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " ORDER BY SCHED_NAME, ENTRY_ID", this);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'ENTRY_ID = :entryId'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereEntryIdEquals(String entryId) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE ENTRY_ID = ? ORDER BY ENTRY_ID", this,entryId);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE TRIGGER_NAME = ? ORDER BY TRIGGER_NAME", this,triggerName);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE TRIGGER_GROUP = ? ORDER BY TRIGGER_GROUP", this,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'INSTANCE_NAME = :instanceName'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereInstanceNameEquals(String instanceName) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE INSTANCE_NAME = ? ORDER BY INSTANCE_NAME", this,instanceName);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'FIRED_TIME = :firedTime'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereFiredTimeEquals(long firedTime) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE FIRED_TIME = ? ORDER BY FIRED_TIME", this,firedTime);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'SCHED_TIME = :schedTime'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereSchedTimeEquals(long schedTime) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE SCHED_TIME = ? ORDER BY SCHED_TIME", this,schedTime);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'PRIORITY = :priority'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWherePriorityEquals(int priority) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE PRIORITY = ? ORDER BY PRIORITY", this,priority);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'STATE = :state'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereStateEquals(String state) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE STATE = ? ORDER BY STATE", this,state);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'JOB_NAME = :jobName'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereJobNameEquals(String jobName) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE JOB_NAME = ? ORDER BY JOB_NAME", this,jobName);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'JOB_GROUP = :jobGroup'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereJobGroupEquals(String jobGroup) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE JOB_GROUP = ? ORDER BY JOB_GROUP", this,jobGroup);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'IS_NONCONCURRENT = :isNonconcurrent'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereIsNonconcurrentEquals(String isNonconcurrent) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE IS_NONCONCURRENT = ? ORDER BY IS_NONCONCURRENT", this,isNonconcurrent);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'REQUESTS_RECOVERY = :requestsRecovery'.
	 */
	@Transactional
	public List<QrtzFiredTriggersDao> findWhereRequestsRecoveryEquals(String requestsRecovery) throws QrtzFiredTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, ENTRY_ID, TRIGGER_NAME, TRIGGER_GROUP, INSTANCE_NAME, FIRED_TIME, SCHED_TIME, PRIORITY, STATE, JOB_NAME, JOB_GROUP, IS_NONCONCURRENT, REQUESTS_RECOVERY FROM " + getTableName() + " WHERE REQUESTS_RECOVERY = ? ORDER BY REQUESTS_RECOVERY", this,requestsRecovery);
		}
		catch (Exception e) {
			throw new QrtzFiredTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_FIRED_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzFiredTriggersDao findByPrimaryKey(QrtzFiredTriggersDaoPk pk) throws QrtzFiredTriggersDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getEntryId() );
	}

}
