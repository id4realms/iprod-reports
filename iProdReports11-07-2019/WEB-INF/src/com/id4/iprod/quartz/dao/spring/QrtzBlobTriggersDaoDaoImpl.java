package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzBlobTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzBlobTriggersDao;
import com.id4.iprod.quartz.exceptions.QrtzBlobTriggersDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzBlobTriggersDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzBlobTriggersDao>, QrtzBlobTriggersDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(QrtzBlobTriggersDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, BLOB_DATA ) VALUES ( ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getBlobData()} );
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzBlobTriggersDao
	 */
	public QrtzBlobTriggersDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzBlobTriggersDao dto = new QrtzBlobTriggersDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setTriggerName( rs.getString( 2 ) );
		dto.setTriggerGroup( rs.getString( 3 ) );
		dto.setBlobData( super.getBlobColumn(rs, 4 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_BLOB_TRIGGERS";
	}

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzBlobTriggersDao> findAll() throws QrtzBlobTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, BLOB_DATA FROM " + getTableName() + "", this);
		}
		catch (Exception e) {
			throw new QrtzBlobTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzBlobTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzBlobTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, BLOB_DATA FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzBlobTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	@Transactional
	public List<QrtzBlobTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzBlobTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, BLOB_DATA FROM " + getTableName() + " WHERE TRIGGER_NAME = ? ORDER BY TRIGGER_NAME", this,triggerName);
		}
		catch (Exception e) {
			throw new QrtzBlobTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzBlobTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzBlobTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, BLOB_DATA FROM " + getTableName() + " WHERE TRIGGER_GROUP = ? ORDER BY TRIGGER_GROUP", this,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzBlobTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_BLOB_TRIGGERS table that match the criteria 'BLOB_DATA = :blobData'.
	 */
	@Transactional
	public List<QrtzBlobTriggersDao> findWhereBlobDataEquals(byte[] blobData) throws QrtzBlobTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, BLOB_DATA FROM " + getTableName() + " WHERE BLOB_DATA = ? ORDER BY BLOB_DATA", this,blobData);
		}
		catch (Exception e) {
			throw new QrtzBlobTriggersDaoDaoException("Query failed", e);
		}
		
	}

}
