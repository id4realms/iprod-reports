package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzCalendarsDaoDao;
import com.id4.iprod.quartz.dto.QrtzCalendarsDao;
import com.id4.iprod.quartz.dto.QrtzCalendarsDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzCalendarsDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzCalendarsDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzCalendarsDaoPk
	 */
	public QrtzCalendarsDaoPk insert(QrtzCalendarsDao dto);

	/** 
	 * Updates a single row in the QRTZ_CALENDARS table.
	 */
	public void update(QrtzCalendarsDaoPk pk, QrtzCalendarsDao dto) throws QrtzCalendarsDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_CALENDARS table.
	 */
	public void delete(QrtzCalendarsDaoPk pk) throws QrtzCalendarsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'SCHED_NAME = :schedName AND CALENDAR_NAME = :calendarName'.
	 */
	public QrtzCalendarsDao findByPrimaryKey(String schedName, String calendarName) throws QrtzCalendarsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria ''.
	 */
	public List<QrtzCalendarsDao> findAll() throws QrtzCalendarsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzCalendarsDao> findWhereSchedNameEquals(String schedName) throws QrtzCalendarsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'CALENDAR_NAME = :calendarName'.
	 */
	public List<QrtzCalendarsDao> findWhereCalendarNameEquals(String calendarName) throws QrtzCalendarsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CALENDARS table that match the criteria 'CALENDAR = :calendar'.
	 */
	public List<QrtzCalendarsDao> findWhereCalendarEquals(byte[] calendar) throws QrtzCalendarsDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_CALENDARS table that matches the specified primary-key value.
	 */
	public QrtzCalendarsDao findByPrimaryKey(QrtzCalendarsDaoPk pk) throws QrtzCalendarsDaoDaoException;

}
