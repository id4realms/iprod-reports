package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzLocksDaoDao;
import com.id4.iprod.quartz.dto.QrtzLocksDao;
import com.id4.iprod.quartz.dto.QrtzLocksDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzLocksDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzLocksDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzLocksDaoPk
	 */
	public QrtzLocksDaoPk insert(QrtzLocksDao dto);

	/** 
	 * Updates a single row in the QRTZ_LOCKS table.
	 */
	public void update(QrtzLocksDaoPk pk, QrtzLocksDao dto) throws QrtzLocksDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_LOCKS table.
	 */
	public void delete(QrtzLocksDaoPk pk) throws QrtzLocksDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria 'SCHED_NAME = :schedName AND LOCK_NAME = :lockName'.
	 */
	public QrtzLocksDao findByPrimaryKey(String schedName, String lockName) throws QrtzLocksDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria ''.
	 */
	public List<QrtzLocksDao> findAll() throws QrtzLocksDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzLocksDao> findWhereSchedNameEquals(String schedName) throws QrtzLocksDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_LOCKS table that match the criteria 'LOCK_NAME = :lockName'.
	 */
	public List<QrtzLocksDao> findWhereLockNameEquals(String lockName) throws QrtzLocksDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_LOCKS table that matches the specified primary-key value.
	 */
	public QrtzLocksDao findByPrimaryKey(QrtzLocksDaoPk pk) throws QrtzLocksDaoDaoException;

}
