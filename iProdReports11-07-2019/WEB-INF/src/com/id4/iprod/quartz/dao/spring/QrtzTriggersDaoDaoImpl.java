package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzTriggersDao;
import com.id4.iprod.quartz.dto.QrtzTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzTriggersDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzTriggersDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzTriggersDao>, QrtzTriggersDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzTriggersDaoPk
	 */
	public QrtzTriggersDaoPk insert(QrtzTriggersDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.SMALLINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getJobName(),dto.getJobGroup(),dto.getDescription(),dto.getNextFireTime(),dto.getPrevFireTime(),dto.getPriority(),dto.getTriggerState(),dto.getTriggerType(),dto.getStartTime(),dto.getEndTime(),dto.getCalendarName(),dto.getMisfireInstr(),dto.getJobData()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_TRIGGERS table.
	 */
	public void update(QrtzTriggersDaoPk pk, QrtzTriggersDao dto) throws QrtzTriggersDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, TRIGGER_NAME = ?, TRIGGER_GROUP = ?, JOB_NAME = ?, JOB_GROUP = ?, DESCRIPTION = ?, NEXT_FIRE_TIME = ?, PREV_FIRE_TIME = ?, PRIORITY = ?, TRIGGER_STATE = ?, TRIGGER_TYPE = ?, START_TIME = ?, END_TIME = ?, CALENDAR_NAME = ?, MISFIRE_INSTR = ?, JOB_DATA = ? WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.SMALLINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getJobName(),dto.getJobGroup(),dto.getDescription(),dto.getNextFireTime(),dto.getPrevFireTime(),dto.getPriority(),dto.getTriggerState(),dto.getTriggerType(),dto.getStartTime(),dto.getEndTime(),dto.getCalendarName(),dto.getMisfireInstr(),dto.getJobData(),pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_TRIGGERS table.
	 */
	@Transactional
	public void delete(QrtzTriggersDaoPk pk) throws QrtzTriggersDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?",pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzTriggersDao
	 */
	public QrtzTriggersDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzTriggersDao dto = new QrtzTriggersDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setTriggerName( rs.getString( 2 ) );
		dto.setTriggerGroup( rs.getString( 3 ) );
		dto.setJobName( rs.getString( 4 ) );
		dto.setJobGroup( rs.getString( 5 ) );
		dto.setDescription( rs.getString( 6 ) );
		dto.setNextFireTime( rs.getLong( 7 ) );
		if (rs.wasNull()) {
			dto.setNextFireTimeNull( true );
		}
		
		dto.setPrevFireTime( rs.getLong( 8 ) );
		if (rs.wasNull()) {
			dto.setPrevFireTimeNull( true );
		}
		
		dto.setPriority( rs.getInt( 9 ) );
		if (rs.wasNull()) {
			dto.setPriorityNull( true );
		}
		
		dto.setTriggerState( rs.getString( 10 ) );
		dto.setTriggerType( rs.getString( 11 ) );
		dto.setStartTime( rs.getLong( 12 ) );
		dto.setEndTime( rs.getLong( 13 ) );
		if (rs.wasNull()) {
			dto.setEndTimeNull( true );
		}
		
		dto.setCalendarName( rs.getString( 14 ) );
		dto.setMisfireInstr( rs.getShort( 15 ) );
		if (rs.wasNull()) {
			dto.setMisfireInstrNull( true );
		}
		
		dto.setJobData( super.getBlobColumn(rs, 16 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_TRIGGERS";
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public QrtzTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzTriggersDaoDaoException
	{
		try {
			List<QrtzTriggersDao> list = jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerName,triggerGroup);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzTriggersDao> findAll() throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " ORDER BY SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP", this);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND JOB_NAME = :jobName AND JOB_GROUP = :jobGroup'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findByQrtzJobDetails(String schedName, String jobName, String jobGroup) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE SCHED_NAME = ? AND JOB_NAME = ? AND JOB_GROUP = ?", this,schedName,jobName,jobGroup);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE TRIGGER_NAME = ? ORDER BY TRIGGER_NAME", this,triggerName);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE TRIGGER_GROUP = ? ORDER BY TRIGGER_GROUP", this,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'JOB_NAME = :jobName'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereJobNameEquals(String jobName) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE JOB_NAME = ? ORDER BY JOB_NAME", this,jobName);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'JOB_GROUP = :jobGroup'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereJobGroupEquals(String jobGroup) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE JOB_GROUP = ? ORDER BY JOB_GROUP", this,jobGroup);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'DESCRIPTION = :description'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereDescriptionEquals(String description) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE DESCRIPTION = ? ORDER BY DESCRIPTION", this,description);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'NEXT_FIRE_TIME = :nextFireTime'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereNextFireTimeEquals(long nextFireTime) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE NEXT_FIRE_TIME = ? ORDER BY NEXT_FIRE_TIME", this,nextFireTime);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'PREV_FIRE_TIME = :prevFireTime'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWherePrevFireTimeEquals(long prevFireTime) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE PREV_FIRE_TIME = ? ORDER BY PREV_FIRE_TIME", this,prevFireTime);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'PRIORITY = :priority'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWherePriorityEquals(int priority) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE PRIORITY = ? ORDER BY PRIORITY", this,priority);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_STATE = :triggerState'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereTriggerStateEquals(String triggerState) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE TRIGGER_STATE = ? ORDER BY TRIGGER_STATE", this,triggerState);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'TRIGGER_TYPE = :triggerType'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereTriggerTypeEquals(String triggerType) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE TRIGGER_TYPE = ? ORDER BY TRIGGER_TYPE", this,triggerType);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'START_TIME = :startTime'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereStartTimeEquals(long startTime) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE START_TIME = ? ORDER BY START_TIME", this,startTime);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'END_TIME = :endTime'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereEndTimeEquals(long endTime) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE END_TIME = ? ORDER BY END_TIME", this,endTime);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'CALENDAR_NAME = :calendarName'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereCalendarNameEquals(String calendarName) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE CALENDAR_NAME = ? ORDER BY CALENDAR_NAME", this,calendarName);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'MISFIRE_INSTR = :misfireInstr'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereMisfireInstrEquals(short misfireInstr) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE MISFIRE_INSTR = ? ORDER BY MISFIRE_INSTR", this,misfireInstr);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_TRIGGERS table that match the criteria 'JOB_DATA = :jobData'.
	 */
	@Transactional
	public List<QrtzTriggersDao> findWhereJobDataEquals(byte[] jobData) throws QrtzTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, JOB_NAME, JOB_GROUP, DESCRIPTION, NEXT_FIRE_TIME, PREV_FIRE_TIME, PRIORITY, TRIGGER_STATE, TRIGGER_TYPE, START_TIME, END_TIME, CALENDAR_NAME, MISFIRE_INSTR, JOB_DATA FROM " + getTableName() + " WHERE JOB_DATA = ? ORDER BY JOB_DATA", this,jobData);
		}
		catch (Exception e) {
			throw new QrtzTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzTriggersDao findByPrimaryKey(QrtzTriggersDaoPk pk) throws QrtzTriggersDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getTriggerName(), pk.getTriggerGroup() );
	}

}
