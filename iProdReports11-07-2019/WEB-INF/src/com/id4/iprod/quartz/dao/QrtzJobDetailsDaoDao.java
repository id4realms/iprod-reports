package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzJobDetailsDaoDao;
import com.id4.iprod.quartz.dto.QrtzJobDetailsDao;
import com.id4.iprod.quartz.dto.QrtzJobDetailsDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzJobDetailsDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzJobDetailsDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzJobDetailsDaoPk
	 */
	public QrtzJobDetailsDaoPk insert(QrtzJobDetailsDao dto);

	/** 
	 * Updates a single row in the QRTZ_JOB_DETAILS table.
	 */
	public void update(QrtzJobDetailsDaoPk pk, QrtzJobDetailsDao dto) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_JOB_DETAILS table.
	 */
	public void delete(QrtzJobDetailsDaoPk pk) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'SCHED_NAME = :schedName AND JOB_NAME = :jobName AND JOB_GROUP = :jobGroup'.
	 */
	public QrtzJobDetailsDao findByPrimaryKey(String schedName, String jobName, String jobGroup) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria ''.
	 */
	public List<QrtzJobDetailsDao> findAll() throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzJobDetailsDao> findWhereSchedNameEquals(String schedName) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_NAME = :jobName'.
	 */
	public List<QrtzJobDetailsDao> findWhereJobNameEquals(String jobName) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_GROUP = :jobGroup'.
	 */
	public List<QrtzJobDetailsDao> findWhereJobGroupEquals(String jobGroup) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'DESCRIPTION = :description'.
	 */
	public List<QrtzJobDetailsDao> findWhereDescriptionEquals(String description) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_CLASS_NAME = :jobClassName'.
	 */
	public List<QrtzJobDetailsDao> findWhereJobClassNameEquals(String jobClassName) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'IS_DURABLE = :isDurable'.
	 */
	public List<QrtzJobDetailsDao> findWhereIsDurableEquals(String isDurable) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'IS_NONCONCURRENT = :isNonconcurrent'.
	 */
	public List<QrtzJobDetailsDao> findWhereIsNonconcurrentEquals(String isNonconcurrent) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'IS_UPDATE_DATA = :isUpdateData'.
	 */
	public List<QrtzJobDetailsDao> findWhereIsUpdateDataEquals(String isUpdateData) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'REQUESTS_RECOVERY = :requestsRecovery'.
	 */
	public List<QrtzJobDetailsDao> findWhereRequestsRecoveryEquals(String requestsRecovery) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_DATA = :jobData'.
	 */
	public List<QrtzJobDetailsDao> findWhereJobDataEquals(byte[] jobData) throws QrtzJobDetailsDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_JOB_DETAILS table that matches the specified primary-key value.
	 */
	public QrtzJobDetailsDao findByPrimaryKey(QrtzJobDetailsDaoPk pk) throws QrtzJobDetailsDaoDaoException;

}
