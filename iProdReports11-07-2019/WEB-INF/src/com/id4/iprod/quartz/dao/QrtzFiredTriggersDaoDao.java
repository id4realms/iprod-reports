package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzFiredTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzFiredTriggersDao;
import com.id4.iprod.quartz.dto.QrtzFiredTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzFiredTriggersDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzFiredTriggersDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzFiredTriggersDaoPk
	 */
	public QrtzFiredTriggersDaoPk insert(QrtzFiredTriggersDao dto);

	/** 
	 * Updates a single row in the QRTZ_FIRED_TRIGGERS table.
	 */
	public void update(QrtzFiredTriggersDaoPk pk, QrtzFiredTriggersDao dto) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_FIRED_TRIGGERS table.
	 */
	public void delete(QrtzFiredTriggersDaoPk pk) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND ENTRY_ID = :entryId'.
	 */
	public QrtzFiredTriggersDao findByPrimaryKey(String schedName, String entryId) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria ''.
	 */
	public List<QrtzFiredTriggersDao> findAll() throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzFiredTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'ENTRY_ID = :entryId'.
	 */
	public List<QrtzFiredTriggersDao> findWhereEntryIdEquals(String entryId) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	public List<QrtzFiredTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzFiredTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'INSTANCE_NAME = :instanceName'.
	 */
	public List<QrtzFiredTriggersDao> findWhereInstanceNameEquals(String instanceName) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'FIRED_TIME = :firedTime'.
	 */
	public List<QrtzFiredTriggersDao> findWhereFiredTimeEquals(long firedTime) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'SCHED_TIME = :schedTime'.
	 */
	public List<QrtzFiredTriggersDao> findWhereSchedTimeEquals(long schedTime) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'PRIORITY = :priority'.
	 */
	public List<QrtzFiredTriggersDao> findWherePriorityEquals(int priority) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'STATE = :state'.
	 */
	public List<QrtzFiredTriggersDao> findWhereStateEquals(String state) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'JOB_NAME = :jobName'.
	 */
	public List<QrtzFiredTriggersDao> findWhereJobNameEquals(String jobName) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'JOB_GROUP = :jobGroup'.
	 */
	public List<QrtzFiredTriggersDao> findWhereJobGroupEquals(String jobGroup) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'IS_NONCONCURRENT = :isNonconcurrent'.
	 */
	public List<QrtzFiredTriggersDao> findWhereIsNonconcurrentEquals(String isNonconcurrent) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_FIRED_TRIGGERS table that match the criteria 'REQUESTS_RECOVERY = :requestsRecovery'.
	 */
	public List<QrtzFiredTriggersDao> findWhereRequestsRecoveryEquals(String requestsRecovery) throws QrtzFiredTriggersDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_FIRED_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzFiredTriggersDao findByPrimaryKey(QrtzFiredTriggersDaoPk pk) throws QrtzFiredTriggersDaoDaoException;

}
