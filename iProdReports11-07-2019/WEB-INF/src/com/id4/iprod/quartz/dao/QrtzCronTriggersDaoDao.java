package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzCronTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzCronTriggersDao;
import com.id4.iprod.quartz.dto.QrtzCronTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzCronTriggersDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzCronTriggersDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzCronTriggersDaoPk
	 */
	public QrtzCronTriggersDaoPk insert(QrtzCronTriggersDao dto);

	/** 
	 * Updates a single row in the QRTZ_CRON_TRIGGERS table.
	 */
	public void update(QrtzCronTriggersDaoPk pk, QrtzCronTriggersDao dto) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_CRON_TRIGGERS table.
	 */
	public void delete(QrtzCronTriggersDaoPk pk) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public QrtzCronTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria ''.
	 */
	public List<QrtzCronTriggersDao> findAll() throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzCronTriggersDao> findByQrtzTriggers(String schedName, String triggerName, String triggerGroup) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzCronTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	public List<QrtzCronTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzCronTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'CRON_EXPRESSION = :cronExpression'.
	 */
	public List<QrtzCronTriggersDao> findWhereCronExpressionEquals(String cronExpression) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'TIME_ZONE_ID = :timeZoneId'.
	 */
	public List<QrtzCronTriggersDao> findWhereTimeZoneIdEquals(String timeZoneId) throws QrtzCronTriggersDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_CRON_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzCronTriggersDao findByPrimaryKey(QrtzCronTriggersDaoPk pk) throws QrtzCronTriggersDaoDaoException;

}
