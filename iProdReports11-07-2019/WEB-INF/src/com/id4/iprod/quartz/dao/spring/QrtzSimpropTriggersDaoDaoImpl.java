package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzSimpropTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzSimpropTriggersDao;
import com.id4.iprod.quartz.dto.QrtzSimpropTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzSimpropTriggersDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzSimpropTriggersDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzSimpropTriggersDao>, QrtzSimpropTriggersDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzSimpropTriggersDaoPk
	 */
	public QrtzSimpropTriggersDaoPk insert(QrtzSimpropTriggersDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getStrProp1(),dto.getStrProp2(),dto.getStrProp3(),dto.getIntProp1(),dto.getIntProp2(),dto.getLongProp1(),dto.getLongProp2(),dto.getDecProp1(),dto.getDecProp2(),dto.getBoolProp1(),dto.getBoolProp2()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	public void update(QrtzSimpropTriggersDaoPk pk, QrtzSimpropTriggersDao dto) throws QrtzSimpropTriggersDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, TRIGGER_NAME = ?, TRIGGER_GROUP = ?, STR_PROP_1 = ?, STR_PROP_2 = ?, STR_PROP_3 = ?, INT_PROP_1 = ?, INT_PROP_2 = ?, LONG_PROP_1 = ?, LONG_PROP_2 = ?, DEC_PROP_1 = ?, DEC_PROP_2 = ?, BOOL_PROP_1 = ?, BOOL_PROP_2 = ? WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.BIGINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getStrProp1(),dto.getStrProp2(),dto.getStrProp3(),dto.getIntProp1(),dto.getIntProp2(),dto.getLongProp1(),dto.getLongProp2(),dto.getDecProp1(),dto.getDecProp2(),dto.getBoolProp1(),dto.getBoolProp2(),pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	@Transactional
	public void delete(QrtzSimpropTriggersDaoPk pk) throws QrtzSimpropTriggersDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?",pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzSimpropTriggersDao
	 */
	public QrtzSimpropTriggersDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzSimpropTriggersDao dto = new QrtzSimpropTriggersDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setTriggerName( rs.getString( 2 ) );
		dto.setTriggerGroup( rs.getString( 3 ) );
		dto.setStrProp1( rs.getString( 4 ) );
		dto.setStrProp2( rs.getString( 5 ) );
		dto.setStrProp3( rs.getString( 6 ) );
		dto.setIntProp1( rs.getInt( 7 ) );
		if (rs.wasNull()) {
			dto.setIntProp1Null( true );
		}
		
		dto.setIntProp2( rs.getInt( 8 ) );
		if (rs.wasNull()) {
			dto.setIntProp2Null( true );
		}
		
		dto.setLongProp1( rs.getLong( 9 ) );
		if (rs.wasNull()) {
			dto.setLongProp1Null( true );
		}
		
		dto.setLongProp2( rs.getLong( 10 ) );
		if (rs.wasNull()) {
			dto.setLongProp2Null( true );
		}
		
		dto.setDecProp1( rs.getFloat( 11 ) );
		if (rs.wasNull()) {
			dto.setDecProp1Null( true );
		}
		
		dto.setDecProp2( rs.getFloat( 12 ) );
		if (rs.wasNull()) {
			dto.setDecProp2Null( true );
		}
		
		dto.setBoolProp1( rs.getString( 13 ) );
		dto.setBoolProp2( rs.getString( 14 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_SIMPROP_TRIGGERS";
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public QrtzSimpropTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			List<QrtzSimpropTriggersDao> list = jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerName,triggerGroup);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findAll() throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " ORDER BY SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP", this);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findByQrtzTriggers(String schedName, String triggerName, String triggerGroup) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerName,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE TRIGGER_NAME = ? ORDER BY TRIGGER_NAME", this,triggerName);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE TRIGGER_GROUP = ? ORDER BY TRIGGER_GROUP", this,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'STR_PROP_1 = :strProp1'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereStrProp1Equals(String strProp1) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE STR_PROP_1 = ? ORDER BY STR_PROP_1", this,strProp1);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'STR_PROP_2 = :strProp2'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereStrProp2Equals(String strProp2) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE STR_PROP_2 = ? ORDER BY STR_PROP_2", this,strProp2);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'STR_PROP_3 = :strProp3'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereStrProp3Equals(String strProp3) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE STR_PROP_3 = ? ORDER BY STR_PROP_3", this,strProp3);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'INT_PROP_1 = :intProp1'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereIntProp1Equals(int intProp1) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE INT_PROP_1 = ? ORDER BY INT_PROP_1", this,intProp1);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'INT_PROP_2 = :intProp2'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereIntProp2Equals(int intProp2) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE INT_PROP_2 = ? ORDER BY INT_PROP_2", this,intProp2);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'LONG_PROP_1 = :longProp1'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereLongProp1Equals(long longProp1) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE LONG_PROP_1 = ? ORDER BY LONG_PROP_1", this,longProp1);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'LONG_PROP_2 = :longProp2'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereLongProp2Equals(long longProp2) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE LONG_PROP_2 = ? ORDER BY LONG_PROP_2", this,longProp2);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'DEC_PROP_1 = :decProp1'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereDecProp1Equals(float decProp1) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE DEC_PROP_1 = ? ORDER BY DEC_PROP_1", this,decProp1);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'DEC_PROP_2 = :decProp2'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereDecProp2Equals(float decProp2) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE DEC_PROP_2 = ? ORDER BY DEC_PROP_2", this,decProp2);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'BOOL_PROP_1 = :boolProp1'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereBoolProp1Equals(String boolProp1) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE BOOL_PROP_1 = ? ORDER BY BOOL_PROP_1", this,boolProp1);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_SIMPROP_TRIGGERS table that match the criteria 'BOOL_PROP_2 = :boolProp2'.
	 */
	@Transactional
	public List<QrtzSimpropTriggersDao> findWhereBoolProp2Equals(String boolProp2) throws QrtzSimpropTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, STR_PROP_1, STR_PROP_2, STR_PROP_3, INT_PROP_1, INT_PROP_2, LONG_PROP_1, LONG_PROP_2, DEC_PROP_1, DEC_PROP_2, BOOL_PROP_1, BOOL_PROP_2 FROM " + getTableName() + " WHERE BOOL_PROP_2 = ? ORDER BY BOOL_PROP_2", this,boolProp2);
		}
		catch (Exception e) {
			throw new QrtzSimpropTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_SIMPROP_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzSimpropTriggersDao findByPrimaryKey(QrtzSimpropTriggersDaoPk pk) throws QrtzSimpropTriggersDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getTriggerName(), pk.getTriggerGroup() );
	}

}
