package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzPausedTriggerGrpsDaoDao;
import com.id4.iprod.quartz.dto.QrtzPausedTriggerGrpsDao;
import com.id4.iprod.quartz.dto.QrtzPausedTriggerGrpsDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzPausedTriggerGrpsDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzPausedTriggerGrpsDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzPausedTriggerGrpsDao>, QrtzPausedTriggerGrpsDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzPausedTriggerGrpsDaoPk
	 */
	public QrtzPausedTriggerGrpsDaoPk insert(QrtzPausedTriggerGrpsDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, TRIGGER_GROUP ) VALUES ( ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerGroup()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_PAUSED_TRIGGER_GRPS table.
	 */
	public void update(QrtzPausedTriggerGrpsDaoPk pk, QrtzPausedTriggerGrpsDao dto) throws QrtzPausedTriggerGrpsDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, TRIGGER_GROUP = ? WHERE SCHED_NAME = ? AND TRIGGER_GROUP = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerGroup(),pk.getSchedName(),pk.getTriggerGroup() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_PAUSED_TRIGGER_GRPS table.
	 */
	@Transactional
	public void delete(QrtzPausedTriggerGrpsDaoPk pk) throws QrtzPausedTriggerGrpsDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_GROUP = ?",pk.getSchedName(),pk.getTriggerGroup());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzPausedTriggerGrpsDao
	 */
	public QrtzPausedTriggerGrpsDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzPausedTriggerGrpsDao dto = new QrtzPausedTriggerGrpsDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setTriggerGroup( rs.getString( 2 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_PAUSED_TRIGGER_GRPS";
	}

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public QrtzPausedTriggerGrpsDao findByPrimaryKey(String schedName, String triggerGroup) throws QrtzPausedTriggerGrpsDaoDaoException
	{
		try {
			List<QrtzPausedTriggerGrpsDao> list = jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_GROUP FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerGroup);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzPausedTriggerGrpsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzPausedTriggerGrpsDao> findAll() throws QrtzPausedTriggerGrpsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_GROUP FROM " + getTableName() + " ORDER BY SCHED_NAME, TRIGGER_GROUP", this);
		}
		catch (Exception e) {
			throw new QrtzPausedTriggerGrpsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzPausedTriggerGrpsDao> findWhereSchedNameEquals(String schedName) throws QrtzPausedTriggerGrpsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_GROUP FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzPausedTriggerGrpsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzPausedTriggerGrpsDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzPausedTriggerGrpsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_GROUP FROM " + getTableName() + " WHERE TRIGGER_GROUP = ? ORDER BY TRIGGER_GROUP", this,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzPausedTriggerGrpsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_PAUSED_TRIGGER_GRPS table that matches the specified primary-key value.
	 */
	public QrtzPausedTriggerGrpsDao findByPrimaryKey(QrtzPausedTriggerGrpsDaoPk pk) throws QrtzPausedTriggerGrpsDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getTriggerGroup() );
	}

}
