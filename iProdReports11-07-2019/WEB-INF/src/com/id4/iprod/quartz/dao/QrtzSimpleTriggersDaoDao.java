package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzSimpleTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzSimpleTriggersDao;
import com.id4.iprod.quartz.dto.QrtzSimpleTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzSimpleTriggersDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzSimpleTriggersDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzSimpleTriggersDaoPk
	 */
	public QrtzSimpleTriggersDaoPk insert(QrtzSimpleTriggersDao dto);

	/** 
	 * Updates a single row in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	public void update(QrtzSimpleTriggersDaoPk pk, QrtzSimpleTriggersDao dto) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	public void delete(QrtzSimpleTriggersDaoPk pk) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public QrtzSimpleTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria ''.
	 */
	public List<QrtzSimpleTriggersDao> findAll() throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzSimpleTriggersDao> findByQrtzTriggers(String schedName, String triggerName, String triggerGroup) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzSimpleTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	public List<QrtzSimpleTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzSimpleTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'REPEAT_COUNT = :repeatCount'.
	 */
	public List<QrtzSimpleTriggersDao> findWhereRepeatCountEquals(long repeatCount) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'REPEAT_INTERVAL = :repeatInterval'.
	 */
	public List<QrtzSimpleTriggersDao> findWhereRepeatIntervalEquals(long repeatInterval) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_SIMPLE_TRIGGERS table that match the criteria 'TIMES_TRIGGERED = :timesTriggered'.
	 */
	public List<QrtzSimpleTriggersDao> findWhereTimesTriggeredEquals(long timesTriggered) throws QrtzSimpleTriggersDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_SIMPLE_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzSimpleTriggersDao findByPrimaryKey(QrtzSimpleTriggersDaoPk pk) throws QrtzSimpleTriggersDaoDaoException;

}
