package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzCronTriggersDaoDao;
import com.id4.iprod.quartz.dto.QrtzCronTriggersDao;
import com.id4.iprod.quartz.dto.QrtzCronTriggersDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzCronTriggersDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzCronTriggersDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzCronTriggersDao>, QrtzCronTriggersDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzCronTriggersDaoPk
	 */
	public QrtzCronTriggersDaoPk insert(QrtzCronTriggersDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID ) VALUES ( ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getCronExpression(),dto.getTimeZoneId()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_CRON_TRIGGERS table.
	 */
	public void update(QrtzCronTriggersDaoPk pk, QrtzCronTriggersDao dto) throws QrtzCronTriggersDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, TRIGGER_NAME = ?, TRIGGER_GROUP = ?, CRON_EXPRESSION = ?, TIME_ZONE_ID = ? WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getTriggerName(),dto.getTriggerGroup(),dto.getCronExpression(),dto.getTimeZoneId(),pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_CRON_TRIGGERS table.
	 */
	@Transactional
	public void delete(QrtzCronTriggersDaoPk pk) throws QrtzCronTriggersDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?",pk.getSchedName(),pk.getTriggerName(),pk.getTriggerGroup());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzCronTriggersDao
	 */
	public QrtzCronTriggersDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzCronTriggersDao dto = new QrtzCronTriggersDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setTriggerName( rs.getString( 2 ) );
		dto.setTriggerGroup( rs.getString( 3 ) );
		dto.setCronExpression( rs.getString( 4 ) );
		dto.setTimeZoneId( rs.getString( 5 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_CRON_TRIGGERS";
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public QrtzCronTriggersDao findByPrimaryKey(String schedName, String triggerName, String triggerGroup) throws QrtzCronTriggersDaoDaoException
	{
		try {
			List<QrtzCronTriggersDao> list = jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerName,triggerGroup);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzCronTriggersDao> findAll() throws QrtzCronTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " ORDER BY SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP", this);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_NAME = :triggerName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzCronTriggersDao> findByQrtzTriggers(String schedName, String triggerName, String triggerGroup) throws QrtzCronTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " WHERE SCHED_NAME = ? AND TRIGGER_NAME = ? AND TRIGGER_GROUP = ?", this,schedName,triggerName,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzCronTriggersDao> findWhereSchedNameEquals(String schedName) throws QrtzCronTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'TRIGGER_NAME = :triggerName'.
	 */
	@Transactional
	public List<QrtzCronTriggersDao> findWhereTriggerNameEquals(String triggerName) throws QrtzCronTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " WHERE TRIGGER_NAME = ? ORDER BY TRIGGER_NAME", this,triggerName);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	@Transactional
	public List<QrtzCronTriggersDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzCronTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " WHERE TRIGGER_GROUP = ? ORDER BY TRIGGER_GROUP", this,triggerGroup);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'CRON_EXPRESSION = :cronExpression'.
	 */
	@Transactional
	public List<QrtzCronTriggersDao> findWhereCronExpressionEquals(String cronExpression) throws QrtzCronTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " WHERE CRON_EXPRESSION = ? ORDER BY CRON_EXPRESSION", this,cronExpression);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_CRON_TRIGGERS table that match the criteria 'TIME_ZONE_ID = :timeZoneId'.
	 */
	@Transactional
	public List<QrtzCronTriggersDao> findWhereTimeZoneIdEquals(String timeZoneId) throws QrtzCronTriggersDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, CRON_EXPRESSION, TIME_ZONE_ID FROM " + getTableName() + " WHERE TIME_ZONE_ID = ? ORDER BY TIME_ZONE_ID", this,timeZoneId);
		}
		catch (Exception e) {
			throw new QrtzCronTriggersDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_CRON_TRIGGERS table that matches the specified primary-key value.
	 */
	public QrtzCronTriggersDao findByPrimaryKey(QrtzCronTriggersDaoPk pk) throws QrtzCronTriggersDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getTriggerName(), pk.getTriggerGroup() );
	}

}
