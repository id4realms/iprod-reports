package com.id4.iprod.quartz.dao.spring;

import com.id4.iprod.quartz.dao.QrtzJobDetailsDaoDao;
import com.id4.iprod.quartz.dto.QrtzJobDetailsDao;
import com.id4.iprod.quartz.dto.QrtzJobDetailsDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzJobDetailsDaoDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class QrtzJobDetailsDaoDaoImpl extends AbstractDAO implements ParameterizedRowMapper<QrtzJobDetailsDao>, QrtzJobDetailsDaoDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzJobDetailsDaoPk
	 */
	public QrtzJobDetailsDaoPk insert(QrtzJobDetailsDao dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getJobName(),dto.getJobGroup(),dto.getDescription(),dto.getJobClassName(),dto.getIsDurable(),dto.getIsNonconcurrent(),dto.getIsUpdateData(),dto.getRequestsRecovery(),dto.getJobData()} );
		return dto.createPk();
	}

	/** 
	 * Updates a single row in the QRTZ_JOB_DETAILS table.
	 */
	public void update(QrtzJobDetailsDaoPk pk, QrtzJobDetailsDao dto) throws QrtzJobDetailsDaoDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET SCHED_NAME = ?, JOB_NAME = ?, JOB_GROUP = ?, DESCRIPTION = ?, JOB_CLASS_NAME = ?, IS_DURABLE = ?, IS_NONCONCURRENT = ?, IS_UPDATE_DATA = ?, REQUESTS_RECOVERY = ?, JOB_DATA = ? WHERE SCHED_NAME = ? AND JOB_NAME = ? AND JOB_GROUP = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getSchedName(),dto.getJobName(),dto.getJobGroup(),dto.getDescription(),dto.getJobClassName(),dto.getIsDurable(),dto.getIsNonconcurrent(),dto.getIsUpdateData(),dto.getRequestsRecovery(),dto.getJobData(),pk.getSchedName(),pk.getJobName(),pk.getJobGroup() } );
	}

	/** 
	 * Deletes a single row in the QRTZ_JOB_DETAILS table.
	 */
	@Transactional
	public void delete(QrtzJobDetailsDaoPk pk) throws QrtzJobDetailsDaoDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE SCHED_NAME = ? AND JOB_NAME = ? AND JOB_GROUP = ?",pk.getSchedName(),pk.getJobName(),pk.getJobGroup());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return QrtzJobDetailsDao
	 */
	public QrtzJobDetailsDao mapRow(ResultSet rs, int row) throws SQLException
	{
		QrtzJobDetailsDao dto = new QrtzJobDetailsDao();
		dto.setSchedName( rs.getString( 1 ) );
		dto.setJobName( rs.getString( 2 ) );
		dto.setJobGroup( rs.getString( 3 ) );
		dto.setDescription( rs.getString( 4 ) );
		dto.setJobClassName( rs.getString( 5 ) );
		dto.setIsDurable( rs.getString( 6 ) );
		dto.setIsNonconcurrent( rs.getString( 7 ) );
		dto.setIsUpdateData( rs.getString( 8 ) );
		dto.setRequestsRecovery( rs.getString( 9 ) );
		dto.setJobData( super.getBlobColumn(rs, 10 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprodquartzdb..QRTZ_JOB_DETAILS";
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'SCHED_NAME = :schedName AND JOB_NAME = :jobName AND JOB_GROUP = :jobGroup'.
	 */
	@Transactional
	public QrtzJobDetailsDao findByPrimaryKey(String schedName, String jobName, String jobGroup) throws QrtzJobDetailsDaoDaoException
	{
		try {
			List<QrtzJobDetailsDao> list = jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE SCHED_NAME = ? AND JOB_NAME = ? AND JOB_GROUP = ?", this,schedName,jobName,jobGroup);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria ''.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findAll() throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " ORDER BY SCHED_NAME, JOB_NAME, JOB_GROUP", this);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereSchedNameEquals(String schedName) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE SCHED_NAME = ? ORDER BY SCHED_NAME", this,schedName);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_NAME = :jobName'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereJobNameEquals(String jobName) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE JOB_NAME = ? ORDER BY JOB_NAME", this,jobName);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_GROUP = :jobGroup'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereJobGroupEquals(String jobGroup) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE JOB_GROUP = ? ORDER BY JOB_GROUP", this,jobGroup);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'DESCRIPTION = :description'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereDescriptionEquals(String description) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE DESCRIPTION = ? ORDER BY DESCRIPTION", this,description);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_CLASS_NAME = :jobClassName'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereJobClassNameEquals(String jobClassName) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE JOB_CLASS_NAME = ? ORDER BY JOB_CLASS_NAME", this,jobClassName);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'IS_DURABLE = :isDurable'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereIsDurableEquals(String isDurable) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE IS_DURABLE = ? ORDER BY IS_DURABLE", this,isDurable);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'IS_NONCONCURRENT = :isNonconcurrent'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereIsNonconcurrentEquals(String isNonconcurrent) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE IS_NONCONCURRENT = ? ORDER BY IS_NONCONCURRENT", this,isNonconcurrent);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'IS_UPDATE_DATA = :isUpdateData'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereIsUpdateDataEquals(String isUpdateData) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE IS_UPDATE_DATA = ? ORDER BY IS_UPDATE_DATA", this,isUpdateData);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'REQUESTS_RECOVERY = :requestsRecovery'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereRequestsRecoveryEquals(String requestsRecovery) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE REQUESTS_RECOVERY = ? ORDER BY REQUESTS_RECOVERY", this,requestsRecovery);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the QRTZ_JOB_DETAILS table that match the criteria 'JOB_DATA = :jobData'.
	 */
	@Transactional
	public List<QrtzJobDetailsDao> findWhereJobDataEquals(byte[] jobData) throws QrtzJobDetailsDaoDaoException
	{
		try {
			return jdbcTemplate.query("SELECT SCHED_NAME, JOB_NAME, JOB_GROUP, DESCRIPTION, JOB_CLASS_NAME, IS_DURABLE, IS_NONCONCURRENT, IS_UPDATE_DATA, REQUESTS_RECOVERY, JOB_DATA FROM " + getTableName() + " WHERE JOB_DATA = ? ORDER BY JOB_DATA", this,jobData);
		}
		catch (Exception e) {
			throw new QrtzJobDetailsDaoDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the QRTZ_JOB_DETAILS table that matches the specified primary-key value.
	 */
	public QrtzJobDetailsDao findByPrimaryKey(QrtzJobDetailsDaoPk pk) throws QrtzJobDetailsDaoDaoException
	{
		return findByPrimaryKey( pk.getSchedName(), pk.getJobName(), pk.getJobGroup() );
	}

}
