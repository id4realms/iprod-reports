package com.id4.iprod.quartz.dao;

import com.id4.iprod.quartz.dao.QrtzPausedTriggerGrpsDaoDao;
import com.id4.iprod.quartz.dto.QrtzPausedTriggerGrpsDao;
import com.id4.iprod.quartz.dto.QrtzPausedTriggerGrpsDaoPk;
import com.id4.iprod.quartz.exceptions.QrtzPausedTriggerGrpsDaoDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface QrtzPausedTriggerGrpsDaoDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return QrtzPausedTriggerGrpsDaoPk
	 */
	public QrtzPausedTriggerGrpsDaoPk insert(QrtzPausedTriggerGrpsDao dto);

	/** 
	 * Updates a single row in the QRTZ_PAUSED_TRIGGER_GRPS table.
	 */
	public void update(QrtzPausedTriggerGrpsDaoPk pk, QrtzPausedTriggerGrpsDao dto) throws QrtzPausedTriggerGrpsDaoDaoException;

	/** 
	 * Deletes a single row in the QRTZ_PAUSED_TRIGGER_GRPS table.
	 */
	public void delete(QrtzPausedTriggerGrpsDaoPk pk) throws QrtzPausedTriggerGrpsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria 'SCHED_NAME = :schedName AND TRIGGER_GROUP = :triggerGroup'.
	 */
	public QrtzPausedTriggerGrpsDao findByPrimaryKey(String schedName, String triggerGroup) throws QrtzPausedTriggerGrpsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria ''.
	 */
	public List<QrtzPausedTriggerGrpsDao> findAll() throws QrtzPausedTriggerGrpsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria 'SCHED_NAME = :schedName'.
	 */
	public List<QrtzPausedTriggerGrpsDao> findWhereSchedNameEquals(String schedName) throws QrtzPausedTriggerGrpsDaoDaoException;

	/** 
	 * Returns all rows from the QRTZ_PAUSED_TRIGGER_GRPS table that match the criteria 'TRIGGER_GROUP = :triggerGroup'.
	 */
	public List<QrtzPausedTriggerGrpsDao> findWhereTriggerGroupEquals(String triggerGroup) throws QrtzPausedTriggerGrpsDaoDaoException;

	/** 
	 * Returns the rows from the QRTZ_PAUSED_TRIGGER_GRPS table that matches the specified primary-key value.
	 */
	public QrtzPausedTriggerGrpsDao findByPrimaryKey(QrtzPausedTriggerGrpsDaoPk pk) throws QrtzPausedTriggerGrpsDaoDaoException;

}
