package com.id4.iprod.quartz.exceptions;

public class QrtzPausedTriggerGrpsDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzPausedTriggerGrpsDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzPausedTriggerGrpsDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzPausedTriggerGrpsDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzPausedTriggerGrpsDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
