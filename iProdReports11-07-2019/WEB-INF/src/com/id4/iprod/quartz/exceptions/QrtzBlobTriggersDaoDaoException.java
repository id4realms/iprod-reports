package com.id4.iprod.quartz.exceptions;

public class QrtzBlobTriggersDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzBlobTriggersDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzBlobTriggersDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzBlobTriggersDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzBlobTriggersDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
