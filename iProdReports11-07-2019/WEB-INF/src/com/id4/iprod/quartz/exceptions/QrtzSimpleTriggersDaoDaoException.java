package com.id4.iprod.quartz.exceptions;

public class QrtzSimpleTriggersDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzSimpleTriggersDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzSimpleTriggersDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzSimpleTriggersDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzSimpleTriggersDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
