package com.id4.iprod.quartz.exceptions;

public class QrtzTriggersDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzTriggersDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzTriggersDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzTriggersDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzTriggersDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
