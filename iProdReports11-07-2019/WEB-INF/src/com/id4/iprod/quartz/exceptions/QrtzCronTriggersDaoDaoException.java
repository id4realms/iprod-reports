package com.id4.iprod.quartz.exceptions;

public class QrtzCronTriggersDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzCronTriggersDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzCronTriggersDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzCronTriggersDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzCronTriggersDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
