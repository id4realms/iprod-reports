package com.id4.iprod.quartz.exceptions;

public class QrtzSimpropTriggersDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzSimpropTriggersDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzSimpropTriggersDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzSimpropTriggersDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzSimpropTriggersDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
