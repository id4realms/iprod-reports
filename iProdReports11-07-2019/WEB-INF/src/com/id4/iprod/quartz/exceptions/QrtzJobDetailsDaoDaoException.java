package com.id4.iprod.quartz.exceptions;

public class QrtzJobDetailsDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzJobDetailsDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzJobDetailsDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzJobDetailsDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzJobDetailsDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
