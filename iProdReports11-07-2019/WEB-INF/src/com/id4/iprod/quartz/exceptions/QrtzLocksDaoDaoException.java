package com.id4.iprod.quartz.exceptions;

public class QrtzLocksDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzLocksDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzLocksDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzLocksDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzLocksDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
