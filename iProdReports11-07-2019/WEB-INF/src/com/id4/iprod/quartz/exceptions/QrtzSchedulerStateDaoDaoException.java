package com.id4.iprod.quartz.exceptions;

public class QrtzSchedulerStateDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzSchedulerStateDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzSchedulerStateDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzSchedulerStateDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzSchedulerStateDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
