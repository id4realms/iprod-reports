package com.id4.iprod.quartz.exceptions;

public class QrtzFiredTriggersDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzFiredTriggersDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzFiredTriggersDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzFiredTriggersDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzFiredTriggersDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
