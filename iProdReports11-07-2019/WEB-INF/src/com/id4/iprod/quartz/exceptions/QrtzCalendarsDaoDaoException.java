package com.id4.iprod.quartz.exceptions;

public class QrtzCalendarsDaoDaoException extends DaoException
{
	/**
	 * Method 'QrtzCalendarsDaoDaoException'
	 * 
	 * @param message
	 */
	public QrtzCalendarsDaoDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'QrtzCalendarsDaoDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public QrtzCalendarsDaoDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
