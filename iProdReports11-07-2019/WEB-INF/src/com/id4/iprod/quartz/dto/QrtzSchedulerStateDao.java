package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzSchedulerStateDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_SCHEDULER_STATE table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column INSTANCE_NAME in the QRTZ_SCHEDULER_STATE table.
	 */
	protected String instanceName;

	/** 
	 * This attribute maps to the column LAST_CHECKIN_TIME in the QRTZ_SCHEDULER_STATE table.
	 */
	protected long lastCheckinTime;

	/** 
	 * This attribute maps to the column CHECKIN_INTERVAL in the QRTZ_SCHEDULER_STATE table.
	 */
	protected long checkinInterval;

	/**
	 * Method 'QrtzSchedulerStateDao'
	 * 
	 */
	public QrtzSchedulerStateDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getInstanceName'
	 * 
	 * @return String
	 */
	public String getInstanceName()
	{
		return instanceName;
	}

	/**
	 * Method 'setInstanceName'
	 * 
	 * @param instanceName
	 */
	public void setInstanceName(String instanceName)
	{
		this.instanceName = instanceName;
	}

	/**
	 * Method 'getLastCheckinTime'
	 * 
	 * @return long
	 */
	public long getLastCheckinTime()
	{
		return lastCheckinTime;
	}

	/**
	 * Method 'setLastCheckinTime'
	 * 
	 * @param lastCheckinTime
	 */
	public void setLastCheckinTime(long lastCheckinTime)
	{
		this.lastCheckinTime = lastCheckinTime;
	}

	/**
	 * Method 'getCheckinInterval'
	 * 
	 * @return long
	 */
	public long getCheckinInterval()
	{
		return checkinInterval;
	}

	/**
	 * Method 'setCheckinInterval'
	 * 
	 * @param checkinInterval
	 */
	public void setCheckinInterval(long checkinInterval)
	{
		this.checkinInterval = checkinInterval;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzSchedulerStateDao)) {
			return false;
		}
		
		final QrtzSchedulerStateDao _cast = (QrtzSchedulerStateDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (instanceName == null ? _cast.instanceName != instanceName : !instanceName.equals( _cast.instanceName )) {
			return false;
		}
		
		if (lastCheckinTime != _cast.lastCheckinTime) {
			return false;
		}
		
		if (checkinInterval != _cast.checkinInterval) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (instanceName != null) {
			_hashCode = 29 * _hashCode + instanceName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) (lastCheckinTime ^ (lastCheckinTime >>> 32));
		_hashCode = 29 * _hashCode + (int) (checkinInterval ^ (checkinInterval >>> 32));
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzSchedulerStateDaoPk
	 */
	public QrtzSchedulerStateDaoPk createPk()
	{
		return new QrtzSchedulerStateDaoPk(schedName, instanceName);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzSchedulerStateDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", instanceName=" + instanceName );
		ret.append( ", lastCheckinTime=" + lastCheckinTime );
		ret.append( ", checkinInterval=" + checkinInterval );
		return ret.toString();
	}

}
