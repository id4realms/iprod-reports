package com.id4.iprod.quartz.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the QRTZ_TRIGGERS table.
 */
public class QrtzTriggersDaoPk implements Serializable
{
	protected String schedName;

	protected String triggerName;

	protected String triggerGroup;

	/** 
	 * Sets the value of schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/** 
	 * Gets the value of schedName
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/** 
	 * Sets the value of triggerName
	 */
	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	/** 
	 * Gets the value of triggerName
	 */
	public String getTriggerName()
	{
		return triggerName;
	}

	/** 
	 * Sets the value of triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/** 
	 * Gets the value of triggerGroup
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'QrtzTriggersDaoPk'
	 * 
	 */
	public QrtzTriggersDaoPk()
	{
	}

	/**
	 * Method 'QrtzTriggersDaoPk'
	 * 
	 * @param schedName
	 * @param triggerName
	 * @param triggerGroup
	 */
	public QrtzTriggersDaoPk(final String schedName, final String triggerName, final String triggerGroup)
	{
		this.schedName = schedName;
		this.triggerName = triggerName;
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzTriggersDaoPk)) {
			return false;
		}
		
		final QrtzTriggersDaoPk _cast = (QrtzTriggersDaoPk) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (triggerName == null ? _cast.triggerName != triggerName : !triggerName.equals( _cast.triggerName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (triggerName != null) {
			_hashCode = 29 * _hashCode + triggerName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzTriggersDaoPk: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", triggerName=" + triggerName );
		ret.append( ", triggerGroup=" + triggerGroup );
		return ret.toString();
	}

}
