package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzSimpleTriggersDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column TRIGGER_NAME in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	protected String triggerName;

	/** 
	 * This attribute maps to the column TRIGGER_GROUP in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	protected String triggerGroup;

	/** 
	 * This attribute maps to the column REPEAT_COUNT in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	protected long repeatCount;

	/** 
	 * This attribute maps to the column REPEAT_INTERVAL in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	protected long repeatInterval;

	/** 
	 * This attribute maps to the column TIMES_TRIGGERED in the QRTZ_SIMPLE_TRIGGERS table.
	 */
	protected long timesTriggered;

	/**
	 * Method 'QrtzSimpleTriggersDao'
	 * 
	 */
	public QrtzSimpleTriggersDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getTriggerName'
	 * 
	 * @return String
	 */
	public String getTriggerName()
	{
		return triggerName;
	}

	/**
	 * Method 'setTriggerName'
	 * 
	 * @param triggerName
	 */
	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	/**
	 * Method 'getTriggerGroup'
	 * 
	 * @return String
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'setTriggerGroup'
	 * 
	 * @param triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'getRepeatCount'
	 * 
	 * @return long
	 */
	public long getRepeatCount()
	{
		return repeatCount;
	}

	/**
	 * Method 'setRepeatCount'
	 * 
	 * @param repeatCount
	 */
	public void setRepeatCount(long repeatCount)
	{
		this.repeatCount = repeatCount;
	}

	/**
	 * Method 'getRepeatInterval'
	 * 
	 * @return long
	 */
	public long getRepeatInterval()
	{
		return repeatInterval;
	}

	/**
	 * Method 'setRepeatInterval'
	 * 
	 * @param repeatInterval
	 */
	public void setRepeatInterval(long repeatInterval)
	{
		this.repeatInterval = repeatInterval;
	}

	/**
	 * Method 'getTimesTriggered'
	 * 
	 * @return long
	 */
	public long getTimesTriggered()
	{
		return timesTriggered;
	}

	/**
	 * Method 'setTimesTriggered'
	 * 
	 * @param timesTriggered
	 */
	public void setTimesTriggered(long timesTriggered)
	{
		this.timesTriggered = timesTriggered;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzSimpleTriggersDao)) {
			return false;
		}
		
		final QrtzSimpleTriggersDao _cast = (QrtzSimpleTriggersDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (triggerName == null ? _cast.triggerName != triggerName : !triggerName.equals( _cast.triggerName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		if (repeatCount != _cast.repeatCount) {
			return false;
		}
		
		if (repeatInterval != _cast.repeatInterval) {
			return false;
		}
		
		if (timesTriggered != _cast.timesTriggered) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (triggerName != null) {
			_hashCode = 29 * _hashCode + triggerName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) (repeatCount ^ (repeatCount >>> 32));
		_hashCode = 29 * _hashCode + (int) (repeatInterval ^ (repeatInterval >>> 32));
		_hashCode = 29 * _hashCode + (int) (timesTriggered ^ (timesTriggered >>> 32));
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzSimpleTriggersDaoPk
	 */
	public QrtzSimpleTriggersDaoPk createPk()
	{
		return new QrtzSimpleTriggersDaoPk(schedName, triggerName, triggerGroup);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzSimpleTriggersDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", triggerName=" + triggerName );
		ret.append( ", triggerGroup=" + triggerGroup );
		ret.append( ", repeatCount=" + repeatCount );
		ret.append( ", repeatInterval=" + repeatInterval );
		ret.append( ", timesTriggered=" + timesTriggered );
		return ret.toString();
	}

}
