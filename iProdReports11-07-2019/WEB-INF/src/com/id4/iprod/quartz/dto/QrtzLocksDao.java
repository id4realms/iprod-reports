package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzLocksDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_LOCKS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column LOCK_NAME in the QRTZ_LOCKS table.
	 */
	protected String lockName;

	/**
	 * Method 'QrtzLocksDao'
	 * 
	 */
	public QrtzLocksDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getLockName'
	 * 
	 * @return String
	 */
	public String getLockName()
	{
		return lockName;
	}

	/**
	 * Method 'setLockName'
	 * 
	 * @param lockName
	 */
	public void setLockName(String lockName)
	{
		this.lockName = lockName;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzLocksDao)) {
			return false;
		}
		
		final QrtzLocksDao _cast = (QrtzLocksDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (lockName == null ? _cast.lockName != lockName : !lockName.equals( _cast.lockName )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (lockName != null) {
			_hashCode = 29 * _hashCode + lockName.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzLocksDaoPk
	 */
	public QrtzLocksDaoPk createPk()
	{
		return new QrtzLocksDaoPk(schedName, lockName);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzLocksDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", lockName=" + lockName );
		return ret.toString();
	}

}
