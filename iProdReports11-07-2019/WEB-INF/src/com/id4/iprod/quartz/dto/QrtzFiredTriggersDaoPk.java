package com.id4.iprod.quartz.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the QRTZ_FIRED_TRIGGERS table.
 */
public class QrtzFiredTriggersDaoPk implements Serializable
{
	protected String schedName;

	protected String entryId;

	/** 
	 * Sets the value of schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/** 
	 * Gets the value of schedName
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/** 
	 * Sets the value of entryId
	 */
	public void setEntryId(String entryId)
	{
		this.entryId = entryId;
	}

	/** 
	 * Gets the value of entryId
	 */
	public String getEntryId()
	{
		return entryId;
	}

	/**
	 * Method 'QrtzFiredTriggersDaoPk'
	 * 
	 */
	public QrtzFiredTriggersDaoPk()
	{
	}

	/**
	 * Method 'QrtzFiredTriggersDaoPk'
	 * 
	 * @param schedName
	 * @param entryId
	 */
	public QrtzFiredTriggersDaoPk(final String schedName, final String entryId)
	{
		this.schedName = schedName;
		this.entryId = entryId;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzFiredTriggersDaoPk)) {
			return false;
		}
		
		final QrtzFiredTriggersDaoPk _cast = (QrtzFiredTriggersDaoPk) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (entryId == null ? _cast.entryId != entryId : !entryId.equals( _cast.entryId )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (entryId != null) {
			_hashCode = 29 * _hashCode + entryId.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzFiredTriggersDaoPk: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", entryId=" + entryId );
		return ret.toString();
	}

}
