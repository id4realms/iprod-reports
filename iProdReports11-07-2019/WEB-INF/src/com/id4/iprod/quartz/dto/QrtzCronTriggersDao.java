package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzCronTriggersDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_CRON_TRIGGERS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column TRIGGER_NAME in the QRTZ_CRON_TRIGGERS table.
	 */
	protected String triggerName;

	/** 
	 * This attribute maps to the column TRIGGER_GROUP in the QRTZ_CRON_TRIGGERS table.
	 */
	protected String triggerGroup;

	/** 
	 * This attribute maps to the column CRON_EXPRESSION in the QRTZ_CRON_TRIGGERS table.
	 */
	protected String cronExpression;

	/** 
	 * This attribute maps to the column TIME_ZONE_ID in the QRTZ_CRON_TRIGGERS table.
	 */
	protected String timeZoneId;

	/**
	 * Method 'QrtzCronTriggersDao'
	 * 
	 */
	public QrtzCronTriggersDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getTriggerName'
	 * 
	 * @return String
	 */
	public String getTriggerName()
	{
		return triggerName;
	}

	/**
	 * Method 'setTriggerName'
	 * 
	 * @param triggerName
	 */
	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	/**
	 * Method 'getTriggerGroup'
	 * 
	 * @return String
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'setTriggerGroup'
	 * 
	 * @param triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'getCronExpression'
	 * 
	 * @return String
	 */
	public String getCronExpression()
	{
		return cronExpression;
	}

	/**
	 * Method 'setCronExpression'
	 * 
	 * @param cronExpression
	 */
	public void setCronExpression(String cronExpression)
	{
		this.cronExpression = cronExpression;
	}

	/**
	 * Method 'getTimeZoneId'
	 * 
	 * @return String
	 */
	public String getTimeZoneId()
	{
		return timeZoneId;
	}

	/**
	 * Method 'setTimeZoneId'
	 * 
	 * @param timeZoneId
	 */
	public void setTimeZoneId(String timeZoneId)
	{
		this.timeZoneId = timeZoneId;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzCronTriggersDao)) {
			return false;
		}
		
		final QrtzCronTriggersDao _cast = (QrtzCronTriggersDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (triggerName == null ? _cast.triggerName != triggerName : !triggerName.equals( _cast.triggerName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		if (cronExpression == null ? _cast.cronExpression != cronExpression : !cronExpression.equals( _cast.cronExpression )) {
			return false;
		}
		
		if (timeZoneId == null ? _cast.timeZoneId != timeZoneId : !timeZoneId.equals( _cast.timeZoneId )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (triggerName != null) {
			_hashCode = 29 * _hashCode + triggerName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		if (cronExpression != null) {
			_hashCode = 29 * _hashCode + cronExpression.hashCode();
		}
		
		if (timeZoneId != null) {
			_hashCode = 29 * _hashCode + timeZoneId.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzCronTriggersDaoPk
	 */
	public QrtzCronTriggersDaoPk createPk()
	{
		return new QrtzCronTriggersDaoPk(schedName, triggerName, triggerGroup);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzCronTriggersDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", triggerName=" + triggerName );
		ret.append( ", triggerGroup=" + triggerGroup );
		ret.append( ", cronExpression=" + cronExpression );
		ret.append( ", timeZoneId=" + timeZoneId );
		return ret.toString();
	}

}
