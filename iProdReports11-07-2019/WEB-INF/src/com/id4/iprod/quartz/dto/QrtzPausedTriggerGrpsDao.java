package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzPausedTriggerGrpsDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_PAUSED_TRIGGER_GRPS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column TRIGGER_GROUP in the QRTZ_PAUSED_TRIGGER_GRPS table.
	 */
	protected String triggerGroup;

	/**
	 * Method 'QrtzPausedTriggerGrpsDao'
	 * 
	 */
	public QrtzPausedTriggerGrpsDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getTriggerGroup'
	 * 
	 * @return String
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'setTriggerGroup'
	 * 
	 * @param triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzPausedTriggerGrpsDao)) {
			return false;
		}
		
		final QrtzPausedTriggerGrpsDao _cast = (QrtzPausedTriggerGrpsDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzPausedTriggerGrpsDaoPk
	 */
	public QrtzPausedTriggerGrpsDaoPk createPk()
	{
		return new QrtzPausedTriggerGrpsDaoPk(schedName, triggerGroup);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzPausedTriggerGrpsDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", triggerGroup=" + triggerGroup );
		return ret.toString();
	}

}
