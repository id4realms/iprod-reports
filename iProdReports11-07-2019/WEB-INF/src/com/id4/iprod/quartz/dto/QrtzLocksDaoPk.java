package com.id4.iprod.quartz.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the QRTZ_LOCKS table.
 */
public class QrtzLocksDaoPk implements Serializable
{
	protected String schedName;

	protected String lockName;

	/** 
	 * Sets the value of schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/** 
	 * Gets the value of schedName
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/** 
	 * Sets the value of lockName
	 */
	public void setLockName(String lockName)
	{
		this.lockName = lockName;
	}

	/** 
	 * Gets the value of lockName
	 */
	public String getLockName()
	{
		return lockName;
	}

	/**
	 * Method 'QrtzLocksDaoPk'
	 * 
	 */
	public QrtzLocksDaoPk()
	{
	}

	/**
	 * Method 'QrtzLocksDaoPk'
	 * 
	 * @param schedName
	 * @param lockName
	 */
	public QrtzLocksDaoPk(final String schedName, final String lockName)
	{
		this.schedName = schedName;
		this.lockName = lockName;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzLocksDaoPk)) {
			return false;
		}
		
		final QrtzLocksDaoPk _cast = (QrtzLocksDaoPk) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (lockName == null ? _cast.lockName != lockName : !lockName.equals( _cast.lockName )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (lockName != null) {
			_hashCode = 29 * _hashCode + lockName.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzLocksDaoPk: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", lockName=" + lockName );
		return ret.toString();
	}

}
