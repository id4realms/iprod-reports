package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzTriggersDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_TRIGGERS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column TRIGGER_NAME in the QRTZ_TRIGGERS table.
	 */
	protected String triggerName;

	/** 
	 * This attribute maps to the column TRIGGER_GROUP in the QRTZ_TRIGGERS table.
	 */
	protected String triggerGroup;

	/** 
	 * This attribute maps to the column JOB_NAME in the QRTZ_TRIGGERS table.
	 */
	protected String jobName;

	/** 
	 * This attribute maps to the column JOB_GROUP in the QRTZ_TRIGGERS table.
	 */
	protected String jobGroup;

	/** 
	 * This attribute maps to the column DESCRIPTION in the QRTZ_TRIGGERS table.
	 */
	protected String description;

	/** 
	 * This attribute maps to the column NEXT_FIRE_TIME in the QRTZ_TRIGGERS table.
	 */
	protected long nextFireTime;

	/** 
	 * This attribute represents whether the primitive attribute nextFireTime is null.
	 */
	protected boolean nextFireTimeNull = true;

	/** 
	 * This attribute maps to the column PREV_FIRE_TIME in the QRTZ_TRIGGERS table.
	 */
	protected long prevFireTime;

	/** 
	 * This attribute represents whether the primitive attribute prevFireTime is null.
	 */
	protected boolean prevFireTimeNull = true;

	/** 
	 * This attribute maps to the column PRIORITY in the QRTZ_TRIGGERS table.
	 */
	protected int priority;

	/** 
	 * This attribute represents whether the primitive attribute priority is null.
	 */
	protected boolean priorityNull = true;

	/** 
	 * This attribute maps to the column TRIGGER_STATE in the QRTZ_TRIGGERS table.
	 */
	protected String triggerState;

	/** 
	 * This attribute maps to the column TRIGGER_TYPE in the QRTZ_TRIGGERS table.
	 */
	protected String triggerType;

	/** 
	 * This attribute maps to the column START_TIME in the QRTZ_TRIGGERS table.
	 */
	protected long startTime;

	/** 
	 * This attribute maps to the column END_TIME in the QRTZ_TRIGGERS table.
	 */
	protected long endTime;

	/** 
	 * This attribute represents whether the primitive attribute endTime is null.
	 */
	protected boolean endTimeNull = true;

	/** 
	 * This attribute maps to the column CALENDAR_NAME in the QRTZ_TRIGGERS table.
	 */
	protected String calendarName;

	/** 
	 * This attribute maps to the column MISFIRE_INSTR in the QRTZ_TRIGGERS table.
	 */
	protected short misfireInstr;

	/** 
	 * This attribute represents whether the primitive attribute misfireInstr is null.
	 */
	protected boolean misfireInstrNull = true;

	/** 
	 * This attribute maps to the column JOB_DATA in the QRTZ_TRIGGERS table.
	 */
	protected byte[] jobData;

	/**
	 * Method 'QrtzTriggersDao'
	 * 
	 */
	public QrtzTriggersDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getTriggerName'
	 * 
	 * @return String
	 */
	public String getTriggerName()
	{
		return triggerName;
	}

	/**
	 * Method 'setTriggerName'
	 * 
	 * @param triggerName
	 */
	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	/**
	 * Method 'getTriggerGroup'
	 * 
	 * @return String
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'setTriggerGroup'
	 * 
	 * @param triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'getJobName'
	 * 
	 * @return String
	 */
	public String getJobName()
	{
		return jobName;
	}

	/**
	 * Method 'setJobName'
	 * 
	 * @param jobName
	 */
	public void setJobName(String jobName)
	{
		this.jobName = jobName;
	}

	/**
	 * Method 'getJobGroup'
	 * 
	 * @return String
	 */
	public String getJobGroup()
	{
		return jobGroup;
	}

	/**
	 * Method 'setJobGroup'
	 * 
	 * @param jobGroup
	 */
	public void setJobGroup(String jobGroup)
	{
		this.jobGroup = jobGroup;
	}

	/**
	 * Method 'getDescription'
	 * 
	 * @return String
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * Method 'setDescription'
	 * 
	 * @param description
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * Method 'getNextFireTime'
	 * 
	 * @return long
	 */
	public long getNextFireTime()
	{
		return nextFireTime;
	}

	/**
	 * Method 'setNextFireTime'
	 * 
	 * @param nextFireTime
	 */
	public void setNextFireTime(long nextFireTime)
	{
		this.nextFireTime = nextFireTime;
		this.nextFireTimeNull = false;
	}

	/**
	 * Method 'setNextFireTimeNull'
	 * 
	 * @param value
	 */
	public void setNextFireTimeNull(boolean value)
	{
		this.nextFireTimeNull = value;
	}

	/**
	 * Method 'isNextFireTimeNull'
	 * 
	 * @return boolean
	 */
	public boolean isNextFireTimeNull()
	{
		return nextFireTimeNull;
	}

	/**
	 * Method 'getPrevFireTime'
	 * 
	 * @return long
	 */
	public long getPrevFireTime()
	{
		return prevFireTime;
	}

	/**
	 * Method 'setPrevFireTime'
	 * 
	 * @param prevFireTime
	 */
	public void setPrevFireTime(long prevFireTime)
	{
		this.prevFireTime = prevFireTime;
		this.prevFireTimeNull = false;
	}

	/**
	 * Method 'setPrevFireTimeNull'
	 * 
	 * @param value
	 */
	public void setPrevFireTimeNull(boolean value)
	{
		this.prevFireTimeNull = value;
	}

	/**
	 * Method 'isPrevFireTimeNull'
	 * 
	 * @return boolean
	 */
	public boolean isPrevFireTimeNull()
	{
		return prevFireTimeNull;
	}

	/**
	 * Method 'getPriority'
	 * 
	 * @return int
	 */
	public int getPriority()
	{
		return priority;
	}

	/**
	 * Method 'setPriority'
	 * 
	 * @param priority
	 */
	public void setPriority(int priority)
	{
		this.priority = priority;
		this.priorityNull = false;
	}

	/**
	 * Method 'setPriorityNull'
	 * 
	 * @param value
	 */
	public void setPriorityNull(boolean value)
	{
		this.priorityNull = value;
	}

	/**
	 * Method 'isPriorityNull'
	 * 
	 * @return boolean
	 */
	public boolean isPriorityNull()
	{
		return priorityNull;
	}

	/**
	 * Method 'getTriggerState'
	 * 
	 * @return String
	 */
	public String getTriggerState()
	{
		return triggerState;
	}

	/**
	 * Method 'setTriggerState'
	 * 
	 * @param triggerState
	 */
	public void setTriggerState(String triggerState)
	{
		this.triggerState = triggerState;
	}

	/**
	 * Method 'getTriggerType'
	 * 
	 * @return String
	 */
	public String getTriggerType()
	{
		return triggerType;
	}

	/**
	 * Method 'setTriggerType'
	 * 
	 * @param triggerType
	 */
	public void setTriggerType(String triggerType)
	{
		this.triggerType = triggerType;
	}

	/**
	 * Method 'getStartTime'
	 * 
	 * @return long
	 */
	public long getStartTime()
	{
		return startTime;
	}

	/**
	 * Method 'setStartTime'
	 * 
	 * @param startTime
	 */
	public void setStartTime(long startTime)
	{
		this.startTime = startTime;
	}

	/**
	 * Method 'getEndTime'
	 * 
	 * @return long
	 */
	public long getEndTime()
	{
		return endTime;
	}

	/**
	 * Method 'setEndTime'
	 * 
	 * @param endTime
	 */
	public void setEndTime(long endTime)
	{
		this.endTime = endTime;
		this.endTimeNull = false;
	}

	/**
	 * Method 'setEndTimeNull'
	 * 
	 * @param value
	 */
	public void setEndTimeNull(boolean value)
	{
		this.endTimeNull = value;
	}

	/**
	 * Method 'isEndTimeNull'
	 * 
	 * @return boolean
	 */
	public boolean isEndTimeNull()
	{
		return endTimeNull;
	}

	/**
	 * Method 'getCalendarName'
	 * 
	 * @return String
	 */
	public String getCalendarName()
	{
		return calendarName;
	}

	/**
	 * Method 'setCalendarName'
	 * 
	 * @param calendarName
	 */
	public void setCalendarName(String calendarName)
	{
		this.calendarName = calendarName;
	}

	/**
	 * Method 'getMisfireInstr'
	 * 
	 * @return short
	 */
	public short getMisfireInstr()
	{
		return misfireInstr;
	}

	/**
	 * Method 'setMisfireInstr'
	 * 
	 * @param misfireInstr
	 */
	public void setMisfireInstr(short misfireInstr)
	{
		this.misfireInstr = misfireInstr;
		this.misfireInstrNull = false;
	}

	/**
	 * Method 'setMisfireInstrNull'
	 * 
	 * @param value
	 */
	public void setMisfireInstrNull(boolean value)
	{
		this.misfireInstrNull = value;
	}

	/**
	 * Method 'isMisfireInstrNull'
	 * 
	 * @return boolean
	 */
	public boolean isMisfireInstrNull()
	{
		return misfireInstrNull;
	}

	/**
	 * Method 'getJobData'
	 * 
	 * @return byte[]
	 */
	public byte[] getJobData()
	{
		return jobData;
	}

	/**
	 * Method 'setJobData'
	 * 
	 * @param jobData
	 */
	public void setJobData(byte[] jobData)
	{
		this.jobData = jobData;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzTriggersDao)) {
			return false;
		}
		
		final QrtzTriggersDao _cast = (QrtzTriggersDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (triggerName == null ? _cast.triggerName != triggerName : !triggerName.equals( _cast.triggerName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		if (jobName == null ? _cast.jobName != jobName : !jobName.equals( _cast.jobName )) {
			return false;
		}
		
		if (jobGroup == null ? _cast.jobGroup != jobGroup : !jobGroup.equals( _cast.jobGroup )) {
			return false;
		}
		
		if (description == null ? _cast.description != description : !description.equals( _cast.description )) {
			return false;
		}
		
		if (nextFireTime != _cast.nextFireTime) {
			return false;
		}
		
		if (nextFireTimeNull != _cast.nextFireTimeNull) {
			return false;
		}
		
		if (prevFireTime != _cast.prevFireTime) {
			return false;
		}
		
		if (prevFireTimeNull != _cast.prevFireTimeNull) {
			return false;
		}
		
		if (priority != _cast.priority) {
			return false;
		}
		
		if (priorityNull != _cast.priorityNull) {
			return false;
		}
		
		if (triggerState == null ? _cast.triggerState != triggerState : !triggerState.equals( _cast.triggerState )) {
			return false;
		}
		
		if (triggerType == null ? _cast.triggerType != triggerType : !triggerType.equals( _cast.triggerType )) {
			return false;
		}
		
		if (startTime != _cast.startTime) {
			return false;
		}
		
		if (endTime != _cast.endTime) {
			return false;
		}
		
		if (endTimeNull != _cast.endTimeNull) {
			return false;
		}
		
		if (calendarName == null ? _cast.calendarName != calendarName : !calendarName.equals( _cast.calendarName )) {
			return false;
		}
		
		if (misfireInstr != _cast.misfireInstr) {
			return false;
		}
		
		if (misfireInstrNull != _cast.misfireInstrNull) {
			return false;
		}
		
		if (jobData == null ? _cast.jobData != jobData : !jobData.equals( _cast.jobData )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (triggerName != null) {
			_hashCode = 29 * _hashCode + triggerName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		if (jobName != null) {
			_hashCode = 29 * _hashCode + jobName.hashCode();
		}
		
		if (jobGroup != null) {
			_hashCode = 29 * _hashCode + jobGroup.hashCode();
		}
		
		if (description != null) {
			_hashCode = 29 * _hashCode + description.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) (nextFireTime ^ (nextFireTime >>> 32));
		_hashCode = 29 * _hashCode + (nextFireTimeNull ? 1 : 0);
		_hashCode = 29 * _hashCode + (int) (prevFireTime ^ (prevFireTime >>> 32));
		_hashCode = 29 * _hashCode + (prevFireTimeNull ? 1 : 0);
		_hashCode = 29 * _hashCode + priority;
		_hashCode = 29 * _hashCode + (priorityNull ? 1 : 0);
		if (triggerState != null) {
			_hashCode = 29 * _hashCode + triggerState.hashCode();
		}
		
		if (triggerType != null) {
			_hashCode = 29 * _hashCode + triggerType.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) (startTime ^ (startTime >>> 32));
		_hashCode = 29 * _hashCode + (int) (endTime ^ (endTime >>> 32));
		_hashCode = 29 * _hashCode + (endTimeNull ? 1 : 0);
		if (calendarName != null) {
			_hashCode = 29 * _hashCode + calendarName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) misfireInstr;
		_hashCode = 29 * _hashCode + (misfireInstrNull ? 1 : 0);
		if (jobData != null) {
			_hashCode = 29 * _hashCode + jobData.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzTriggersDaoPk
	 */
	public QrtzTriggersDaoPk createPk()
	{
		return new QrtzTriggersDaoPk(schedName, triggerName, triggerGroup);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzTriggersDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", triggerName=" + triggerName );
		ret.append( ", triggerGroup=" + triggerGroup );
		ret.append( ", jobName=" + jobName );
		ret.append( ", jobGroup=" + jobGroup );
		ret.append( ", description=" + description );
		ret.append( ", nextFireTime=" + nextFireTime );
		ret.append( ", prevFireTime=" + prevFireTime );
		ret.append( ", priority=" + priority );
		ret.append( ", triggerState=" + triggerState );
		ret.append( ", triggerType=" + triggerType );
		ret.append( ", startTime=" + startTime );
		ret.append( ", endTime=" + endTime );
		ret.append( ", calendarName=" + calendarName );
		ret.append( ", misfireInstr=" + misfireInstr );
		ret.append( ", jobData=" + jobData );
		return ret.toString();
	}

}
