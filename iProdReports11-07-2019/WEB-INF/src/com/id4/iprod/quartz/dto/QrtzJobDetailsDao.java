package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzJobDetailsDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_JOB_DETAILS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column JOB_NAME in the QRTZ_JOB_DETAILS table.
	 */
	protected String jobName;

	/** 
	 * This attribute maps to the column JOB_GROUP in the QRTZ_JOB_DETAILS table.
	 */
	protected String jobGroup;

	/** 
	 * This attribute maps to the column DESCRIPTION in the QRTZ_JOB_DETAILS table.
	 */
	protected String description;

	/** 
	 * This attribute maps to the column JOB_CLASS_NAME in the QRTZ_JOB_DETAILS table.
	 */
	protected String jobClassName;

	/** 
	 * This attribute maps to the column IS_DURABLE in the QRTZ_JOB_DETAILS table.
	 */
	protected String isDurable;

	/** 
	 * This attribute maps to the column IS_NONCONCURRENT in the QRTZ_JOB_DETAILS table.
	 */
	protected String isNonconcurrent;

	/** 
	 * This attribute maps to the column IS_UPDATE_DATA in the QRTZ_JOB_DETAILS table.
	 */
	protected String isUpdateData;

	/** 
	 * This attribute maps to the column REQUESTS_RECOVERY in the QRTZ_JOB_DETAILS table.
	 */
	protected String requestsRecovery;

	/** 
	 * This attribute maps to the column JOB_DATA in the QRTZ_JOB_DETAILS table.
	 */
	protected byte[] jobData;

	/**
	 * Method 'QrtzJobDetailsDao'
	 * 
	 */
	public QrtzJobDetailsDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getJobName'
	 * 
	 * @return String
	 */
	public String getJobName()
	{
		return jobName;
	}

	/**
	 * Method 'setJobName'
	 * 
	 * @param jobName
	 */
	public void setJobName(String jobName)
	{
		this.jobName = jobName;
	}

	/**
	 * Method 'getJobGroup'
	 * 
	 * @return String
	 */
	public String getJobGroup()
	{
		return jobGroup;
	}

	/**
	 * Method 'setJobGroup'
	 * 
	 * @param jobGroup
	 */
	public void setJobGroup(String jobGroup)
	{
		this.jobGroup = jobGroup;
	}

	/**
	 * Method 'getDescription'
	 * 
	 * @return String
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * Method 'setDescription'
	 * 
	 * @param description
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * Method 'getJobClassName'
	 * 
	 * @return String
	 */
	public String getJobClassName()
	{
		return jobClassName;
	}

	/**
	 * Method 'setJobClassName'
	 * 
	 * @param jobClassName
	 */
	public void setJobClassName(String jobClassName)
	{
		this.jobClassName = jobClassName;
	}

	/**
	 * Method 'getIsDurable'
	 * 
	 * @return String
	 */
	public String getIsDurable()
	{
		return isDurable;
	}

	/**
	 * Method 'setIsDurable'
	 * 
	 * @param isDurable
	 */
	public void setIsDurable(String isDurable)
	{
		this.isDurable = isDurable;
	}

	/**
	 * Method 'getIsNonconcurrent'
	 * 
	 * @return String
	 */
	public String getIsNonconcurrent()
	{
		return isNonconcurrent;
	}

	/**
	 * Method 'setIsNonconcurrent'
	 * 
	 * @param isNonconcurrent
	 */
	public void setIsNonconcurrent(String isNonconcurrent)
	{
		this.isNonconcurrent = isNonconcurrent;
	}

	/**
	 * Method 'getIsUpdateData'
	 * 
	 * @return String
	 */
	public String getIsUpdateData()
	{
		return isUpdateData;
	}

	/**
	 * Method 'setIsUpdateData'
	 * 
	 * @param isUpdateData
	 */
	public void setIsUpdateData(String isUpdateData)
	{
		this.isUpdateData = isUpdateData;
	}

	/**
	 * Method 'getRequestsRecovery'
	 * 
	 * @return String
	 */
	public String getRequestsRecovery()
	{
		return requestsRecovery;
	}

	/**
	 * Method 'setRequestsRecovery'
	 * 
	 * @param requestsRecovery
	 */
	public void setRequestsRecovery(String requestsRecovery)
	{
		this.requestsRecovery = requestsRecovery;
	}

	/**
	 * Method 'getJobData'
	 * 
	 * @return byte[]
	 */
	public byte[] getJobData()
	{
		return jobData;
	}

	/**
	 * Method 'setJobData'
	 * 
	 * @param jobData
	 */
	public void setJobData(byte[] jobData)
	{
		this.jobData = jobData;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzJobDetailsDao)) {
			return false;
		}
		
		final QrtzJobDetailsDao _cast = (QrtzJobDetailsDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (jobName == null ? _cast.jobName != jobName : !jobName.equals( _cast.jobName )) {
			return false;
		}
		
		if (jobGroup == null ? _cast.jobGroup != jobGroup : !jobGroup.equals( _cast.jobGroup )) {
			return false;
		}
		
		if (description == null ? _cast.description != description : !description.equals( _cast.description )) {
			return false;
		}
		
		if (jobClassName == null ? _cast.jobClassName != jobClassName : !jobClassName.equals( _cast.jobClassName )) {
			return false;
		}
		
		if (isDurable == null ? _cast.isDurable != isDurable : !isDurable.equals( _cast.isDurable )) {
			return false;
		}
		
		if (isNonconcurrent == null ? _cast.isNonconcurrent != isNonconcurrent : !isNonconcurrent.equals( _cast.isNonconcurrent )) {
			return false;
		}
		
		if (isUpdateData == null ? _cast.isUpdateData != isUpdateData : !isUpdateData.equals( _cast.isUpdateData )) {
			return false;
		}
		
		if (requestsRecovery == null ? _cast.requestsRecovery != requestsRecovery : !requestsRecovery.equals( _cast.requestsRecovery )) {
			return false;
		}
		
		if (jobData == null ? _cast.jobData != jobData : !jobData.equals( _cast.jobData )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (jobName != null) {
			_hashCode = 29 * _hashCode + jobName.hashCode();
		}
		
		if (jobGroup != null) {
			_hashCode = 29 * _hashCode + jobGroup.hashCode();
		}
		
		if (description != null) {
			_hashCode = 29 * _hashCode + description.hashCode();
		}
		
		if (jobClassName != null) {
			_hashCode = 29 * _hashCode + jobClassName.hashCode();
		}
		
		if (isDurable != null) {
			_hashCode = 29 * _hashCode + isDurable.hashCode();
		}
		
		if (isNonconcurrent != null) {
			_hashCode = 29 * _hashCode + isNonconcurrent.hashCode();
		}
		
		if (isUpdateData != null) {
			_hashCode = 29 * _hashCode + isUpdateData.hashCode();
		}
		
		if (requestsRecovery != null) {
			_hashCode = 29 * _hashCode + requestsRecovery.hashCode();
		}
		
		if (jobData != null) {
			_hashCode = 29 * _hashCode + jobData.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzJobDetailsDaoPk
	 */
	public QrtzJobDetailsDaoPk createPk()
	{
		return new QrtzJobDetailsDaoPk(schedName, jobName, jobGroup);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzJobDetailsDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", jobName=" + jobName );
		ret.append( ", jobGroup=" + jobGroup );
		ret.append( ", description=" + description );
		ret.append( ", jobClassName=" + jobClassName );
		ret.append( ", isDurable=" + isDurable );
		ret.append( ", isNonconcurrent=" + isNonconcurrent );
		ret.append( ", isUpdateData=" + isUpdateData );
		ret.append( ", requestsRecovery=" + requestsRecovery );
		ret.append( ", jobData=" + jobData );
		return ret.toString();
	}

}
