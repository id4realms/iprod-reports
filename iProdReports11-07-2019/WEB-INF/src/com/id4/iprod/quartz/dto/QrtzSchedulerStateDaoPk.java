package com.id4.iprod.quartz.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the QRTZ_SCHEDULER_STATE table.
 */
public class QrtzSchedulerStateDaoPk implements Serializable
{
	protected String schedName;

	protected String instanceName;

	/** 
	 * Sets the value of schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/** 
	 * Gets the value of schedName
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/** 
	 * Sets the value of instanceName
	 */
	public void setInstanceName(String instanceName)
	{
		this.instanceName = instanceName;
	}

	/** 
	 * Gets the value of instanceName
	 */
	public String getInstanceName()
	{
		return instanceName;
	}

	/**
	 * Method 'QrtzSchedulerStateDaoPk'
	 * 
	 */
	public QrtzSchedulerStateDaoPk()
	{
	}

	/**
	 * Method 'QrtzSchedulerStateDaoPk'
	 * 
	 * @param schedName
	 * @param instanceName
	 */
	public QrtzSchedulerStateDaoPk(final String schedName, final String instanceName)
	{
		this.schedName = schedName;
		this.instanceName = instanceName;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzSchedulerStateDaoPk)) {
			return false;
		}
		
		final QrtzSchedulerStateDaoPk _cast = (QrtzSchedulerStateDaoPk) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (instanceName == null ? _cast.instanceName != instanceName : !instanceName.equals( _cast.instanceName )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (instanceName != null) {
			_hashCode = 29 * _hashCode + instanceName.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzSchedulerStateDaoPk: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", instanceName=" + instanceName );
		return ret.toString();
	}

}
