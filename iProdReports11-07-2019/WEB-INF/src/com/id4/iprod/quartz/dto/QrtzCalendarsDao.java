package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzCalendarsDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_CALENDARS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column CALENDAR_NAME in the QRTZ_CALENDARS table.
	 */
	protected String calendarName;

	/** 
	 * This attribute maps to the column CALENDAR in the QRTZ_CALENDARS table.
	 */
	protected byte[] calendar;

	/**
	 * Method 'QrtzCalendarsDao'
	 * 
	 */
	public QrtzCalendarsDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getCalendarName'
	 * 
	 * @return String
	 */
	public String getCalendarName()
	{
		return calendarName;
	}

	/**
	 * Method 'setCalendarName'
	 * 
	 * @param calendarName
	 */
	public void setCalendarName(String calendarName)
	{
		this.calendarName = calendarName;
	}

	/**
	 * Method 'getCalendar'
	 * 
	 * @return byte[]
	 */
	public byte[] getCalendar()
	{
		return calendar;
	}

	/**
	 * Method 'setCalendar'
	 * 
	 * @param calendar
	 */
	public void setCalendar(byte[] calendar)
	{
		this.calendar = calendar;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzCalendarsDao)) {
			return false;
		}
		
		final QrtzCalendarsDao _cast = (QrtzCalendarsDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (calendarName == null ? _cast.calendarName != calendarName : !calendarName.equals( _cast.calendarName )) {
			return false;
		}
		
		if (calendar == null ? _cast.calendar != calendar : !calendar.equals( _cast.calendar )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (calendarName != null) {
			_hashCode = 29 * _hashCode + calendarName.hashCode();
		}
		
		if (calendar != null) {
			_hashCode = 29 * _hashCode + calendar.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzCalendarsDaoPk
	 */
	public QrtzCalendarsDaoPk createPk()
	{
		return new QrtzCalendarsDaoPk(schedName, calendarName);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzCalendarsDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", calendarName=" + calendarName );
		ret.append( ", calendar=" + calendar );
		return ret.toString();
	}

}
