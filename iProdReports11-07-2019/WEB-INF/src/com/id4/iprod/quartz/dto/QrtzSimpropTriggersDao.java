package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzSimpropTriggersDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column TRIGGER_NAME in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String triggerName;

	/** 
	 * This attribute maps to the column TRIGGER_GROUP in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String triggerGroup;

	/** 
	 * This attribute maps to the column STR_PROP_1 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String strProp1;

	/** 
	 * This attribute maps to the column STR_PROP_2 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String strProp2;

	/** 
	 * This attribute maps to the column STR_PROP_3 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String strProp3;

	/** 
	 * This attribute maps to the column INT_PROP_1 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected int intProp1;

	/** 
	 * This attribute represents whether the primitive attribute intProp1 is null.
	 */
	protected boolean intProp1Null = true;

	/** 
	 * This attribute maps to the column INT_PROP_2 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected int intProp2;

	/** 
	 * This attribute represents whether the primitive attribute intProp2 is null.
	 */
	protected boolean intProp2Null = true;

	/** 
	 * This attribute maps to the column LONG_PROP_1 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected long longProp1;

	/** 
	 * This attribute represents whether the primitive attribute longProp1 is null.
	 */
	protected boolean longProp1Null = true;

	/** 
	 * This attribute maps to the column LONG_PROP_2 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected long longProp2;

	/** 
	 * This attribute represents whether the primitive attribute longProp2 is null.
	 */
	protected boolean longProp2Null = true;

	/** 
	 * This attribute maps to the column DEC_PROP_1 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected float decProp1;

	/** 
	 * This attribute represents whether the primitive attribute decProp1 is null.
	 */
	protected boolean decProp1Null = true;

	/** 
	 * This attribute maps to the column DEC_PROP_2 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected float decProp2;

	/** 
	 * This attribute represents whether the primitive attribute decProp2 is null.
	 */
	protected boolean decProp2Null = true;

	/** 
	 * This attribute maps to the column BOOL_PROP_1 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String boolProp1;

	/** 
	 * This attribute maps to the column BOOL_PROP_2 in the QRTZ_SIMPROP_TRIGGERS table.
	 */
	protected String boolProp2;

	/**
	 * Method 'QrtzSimpropTriggersDao'
	 * 
	 */
	public QrtzSimpropTriggersDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getTriggerName'
	 * 
	 * @return String
	 */
	public String getTriggerName()
	{
		return triggerName;
	}

	/**
	 * Method 'setTriggerName'
	 * 
	 * @param triggerName
	 */
	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	/**
	 * Method 'getTriggerGroup'
	 * 
	 * @return String
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'setTriggerGroup'
	 * 
	 * @param triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'getStrProp1'
	 * 
	 * @return String
	 */
	public String getStrProp1()
	{
		return strProp1;
	}

	/**
	 * Method 'setStrProp1'
	 * 
	 * @param strProp1
	 */
	public void setStrProp1(String strProp1)
	{
		this.strProp1 = strProp1;
	}

	/**
	 * Method 'getStrProp2'
	 * 
	 * @return String
	 */
	public String getStrProp2()
	{
		return strProp2;
	}

	/**
	 * Method 'setStrProp2'
	 * 
	 * @param strProp2
	 */
	public void setStrProp2(String strProp2)
	{
		this.strProp2 = strProp2;
	}

	/**
	 * Method 'getStrProp3'
	 * 
	 * @return String
	 */
	public String getStrProp3()
	{
		return strProp3;
	}

	/**
	 * Method 'setStrProp3'
	 * 
	 * @param strProp3
	 */
	public void setStrProp3(String strProp3)
	{
		this.strProp3 = strProp3;
	}

	/**
	 * Method 'getIntProp1'
	 * 
	 * @return int
	 */
	public int getIntProp1()
	{
		return intProp1;
	}

	/**
	 * Method 'setIntProp1'
	 * 
	 * @param intProp1
	 */
	public void setIntProp1(int intProp1)
	{
		this.intProp1 = intProp1;
		this.intProp1Null = false;
	}

	/**
	 * Method 'setIntProp1Null'
	 * 
	 * @param value
	 */
	public void setIntProp1Null(boolean value)
	{
		this.intProp1Null = value;
	}

	/**
	 * Method 'isIntProp1Null'
	 * 
	 * @return boolean
	 */
	public boolean isIntProp1Null()
	{
		return intProp1Null;
	}

	/**
	 * Method 'getIntProp2'
	 * 
	 * @return int
	 */
	public int getIntProp2()
	{
		return intProp2;
	}

	/**
	 * Method 'setIntProp2'
	 * 
	 * @param intProp2
	 */
	public void setIntProp2(int intProp2)
	{
		this.intProp2 = intProp2;
		this.intProp2Null = false;
	}

	/**
	 * Method 'setIntProp2Null'
	 * 
	 * @param value
	 */
	public void setIntProp2Null(boolean value)
	{
		this.intProp2Null = value;
	}

	/**
	 * Method 'isIntProp2Null'
	 * 
	 * @return boolean
	 */
	public boolean isIntProp2Null()
	{
		return intProp2Null;
	}

	/**
	 * Method 'getLongProp1'
	 * 
	 * @return long
	 */
	public long getLongProp1()
	{
		return longProp1;
	}

	/**
	 * Method 'setLongProp1'
	 * 
	 * @param longProp1
	 */
	public void setLongProp1(long longProp1)
	{
		this.longProp1 = longProp1;
		this.longProp1Null = false;
	}

	/**
	 * Method 'setLongProp1Null'
	 * 
	 * @param value
	 */
	public void setLongProp1Null(boolean value)
	{
		this.longProp1Null = value;
	}

	/**
	 * Method 'isLongProp1Null'
	 * 
	 * @return boolean
	 */
	public boolean isLongProp1Null()
	{
		return longProp1Null;
	}

	/**
	 * Method 'getLongProp2'
	 * 
	 * @return long
	 */
	public long getLongProp2()
	{
		return longProp2;
	}

	/**
	 * Method 'setLongProp2'
	 * 
	 * @param longProp2
	 */
	public void setLongProp2(long longProp2)
	{
		this.longProp2 = longProp2;
		this.longProp2Null = false;
	}

	/**
	 * Method 'setLongProp2Null'
	 * 
	 * @param value
	 */
	public void setLongProp2Null(boolean value)
	{
		this.longProp2Null = value;
	}

	/**
	 * Method 'isLongProp2Null'
	 * 
	 * @return boolean
	 */
	public boolean isLongProp2Null()
	{
		return longProp2Null;
	}

	/**
	 * Method 'getDecProp1'
	 * 
	 * @return float
	 */
	public float getDecProp1()
	{
		return decProp1;
	}

	/**
	 * Method 'setDecProp1'
	 * 
	 * @param decProp1
	 */
	public void setDecProp1(float decProp1)
	{
		this.decProp1 = decProp1;
		this.decProp1Null = false;
	}

	/**
	 * Method 'setDecProp1Null'
	 * 
	 * @param value
	 */
	public void setDecProp1Null(boolean value)
	{
		this.decProp1Null = value;
	}

	/**
	 * Method 'isDecProp1Null'
	 * 
	 * @return boolean
	 */
	public boolean isDecProp1Null()
	{
		return decProp1Null;
	}

	/**
	 * Method 'getDecProp2'
	 * 
	 * @return float
	 */
	public float getDecProp2()
	{
		return decProp2;
	}

	/**
	 * Method 'setDecProp2'
	 * 
	 * @param decProp2
	 */
	public void setDecProp2(float decProp2)
	{
		this.decProp2 = decProp2;
		this.decProp2Null = false;
	}

	/**
	 * Method 'setDecProp2Null'
	 * 
	 * @param value
	 */
	public void setDecProp2Null(boolean value)
	{
		this.decProp2Null = value;
	}

	/**
	 * Method 'isDecProp2Null'
	 * 
	 * @return boolean
	 */
	public boolean isDecProp2Null()
	{
		return decProp2Null;
	}

	/**
	 * Method 'getBoolProp1'
	 * 
	 * @return String
	 */
	public String getBoolProp1()
	{
		return boolProp1;
	}

	/**
	 * Method 'setBoolProp1'
	 * 
	 * @param boolProp1
	 */
	public void setBoolProp1(String boolProp1)
	{
		this.boolProp1 = boolProp1;
	}

	/**
	 * Method 'getBoolProp2'
	 * 
	 * @return String
	 */
	public String getBoolProp2()
	{
		return boolProp2;
	}

	/**
	 * Method 'setBoolProp2'
	 * 
	 * @param boolProp2
	 */
	public void setBoolProp2(String boolProp2)
	{
		this.boolProp2 = boolProp2;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzSimpropTriggersDao)) {
			return false;
		}
		
		final QrtzSimpropTriggersDao _cast = (QrtzSimpropTriggersDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (triggerName == null ? _cast.triggerName != triggerName : !triggerName.equals( _cast.triggerName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		if (strProp1 == null ? _cast.strProp1 != strProp1 : !strProp1.equals( _cast.strProp1 )) {
			return false;
		}
		
		if (strProp2 == null ? _cast.strProp2 != strProp2 : !strProp2.equals( _cast.strProp2 )) {
			return false;
		}
		
		if (strProp3 == null ? _cast.strProp3 != strProp3 : !strProp3.equals( _cast.strProp3 )) {
			return false;
		}
		
		if (intProp1 != _cast.intProp1) {
			return false;
		}
		
		if (intProp1Null != _cast.intProp1Null) {
			return false;
		}
		
		if (intProp2 != _cast.intProp2) {
			return false;
		}
		
		if (intProp2Null != _cast.intProp2Null) {
			return false;
		}
		
		if (longProp1 != _cast.longProp1) {
			return false;
		}
		
		if (longProp1Null != _cast.longProp1Null) {
			return false;
		}
		
		if (longProp2 != _cast.longProp2) {
			return false;
		}
		
		if (longProp2Null != _cast.longProp2Null) {
			return false;
		}
		
		if (decProp1 != _cast.decProp1) {
			return false;
		}
		
		if (decProp1Null != _cast.decProp1Null) {
			return false;
		}
		
		if (decProp2 != _cast.decProp2) {
			return false;
		}
		
		if (decProp2Null != _cast.decProp2Null) {
			return false;
		}
		
		if (boolProp1 == null ? _cast.boolProp1 != boolProp1 : !boolProp1.equals( _cast.boolProp1 )) {
			return false;
		}
		
		if (boolProp2 == null ? _cast.boolProp2 != boolProp2 : !boolProp2.equals( _cast.boolProp2 )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (triggerName != null) {
			_hashCode = 29 * _hashCode + triggerName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		if (strProp1 != null) {
			_hashCode = 29 * _hashCode + strProp1.hashCode();
		}
		
		if (strProp2 != null) {
			_hashCode = 29 * _hashCode + strProp2.hashCode();
		}
		
		if (strProp3 != null) {
			_hashCode = 29 * _hashCode + strProp3.hashCode();
		}
		
		_hashCode = 29 * _hashCode + intProp1;
		_hashCode = 29 * _hashCode + (intProp1Null ? 1 : 0);
		_hashCode = 29 * _hashCode + intProp2;
		_hashCode = 29 * _hashCode + (intProp2Null ? 1 : 0);
		_hashCode = 29 * _hashCode + (int) (longProp1 ^ (longProp1 >>> 32));
		_hashCode = 29 * _hashCode + (longProp1Null ? 1 : 0);
		_hashCode = 29 * _hashCode + (int) (longProp2 ^ (longProp2 >>> 32));
		_hashCode = 29 * _hashCode + (longProp2Null ? 1 : 0);
		_hashCode = 29 * _hashCode + Float.floatToIntBits(decProp1);
		_hashCode = 29 * _hashCode + (decProp1Null ? 1 : 0);
		_hashCode = 29 * _hashCode + Float.floatToIntBits(decProp2);
		_hashCode = 29 * _hashCode + (decProp2Null ? 1 : 0);
		if (boolProp1 != null) {
			_hashCode = 29 * _hashCode + boolProp1.hashCode();
		}
		
		if (boolProp2 != null) {
			_hashCode = 29 * _hashCode + boolProp2.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzSimpropTriggersDaoPk
	 */
	public QrtzSimpropTriggersDaoPk createPk()
	{
		return new QrtzSimpropTriggersDaoPk(schedName, triggerName, triggerGroup);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzSimpropTriggersDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", triggerName=" + triggerName );
		ret.append( ", triggerGroup=" + triggerGroup );
		ret.append( ", strProp1=" + strProp1 );
		ret.append( ", strProp2=" + strProp2 );
		ret.append( ", strProp3=" + strProp3 );
		ret.append( ", intProp1=" + intProp1 );
		ret.append( ", intProp2=" + intProp2 );
		ret.append( ", longProp1=" + longProp1 );
		ret.append( ", longProp2=" + longProp2 );
		ret.append( ", decProp1=" + decProp1 );
		ret.append( ", decProp2=" + decProp2 );
		ret.append( ", boolProp1=" + boolProp1 );
		ret.append( ", boolProp2=" + boolProp2 );
		return ret.toString();
	}

}
