package com.id4.iprod.quartz.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the QRTZ_PAUSED_TRIGGER_GRPS table.
 */
public class QrtzPausedTriggerGrpsDaoPk implements Serializable
{
	protected String schedName;

	protected String triggerGroup;

	/** 
	 * Sets the value of schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/** 
	 * Gets the value of schedName
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/** 
	 * Sets the value of triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/** 
	 * Gets the value of triggerGroup
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'QrtzPausedTriggerGrpsDaoPk'
	 * 
	 */
	public QrtzPausedTriggerGrpsDaoPk()
	{
	}

	/**
	 * Method 'QrtzPausedTriggerGrpsDaoPk'
	 * 
	 * @param schedName
	 * @param triggerGroup
	 */
	public QrtzPausedTriggerGrpsDaoPk(final String schedName, final String triggerGroup)
	{
		this.schedName = schedName;
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzPausedTriggerGrpsDaoPk)) {
			return false;
		}
		
		final QrtzPausedTriggerGrpsDaoPk _cast = (QrtzPausedTriggerGrpsDaoPk) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzPausedTriggerGrpsDaoPk: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", triggerGroup=" + triggerGroup );
		return ret.toString();
	}

}
