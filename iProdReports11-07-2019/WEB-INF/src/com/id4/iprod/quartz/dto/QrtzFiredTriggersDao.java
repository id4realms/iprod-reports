package com.id4.iprod.quartz.dto;

import com.id4.iprod.quartz.dao.*;
import com.id4.iprod.quartz.*;
import com.id4.iprod.quartz.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class QrtzFiredTriggersDao implements Serializable
{
	/** 
	 * This attribute maps to the column SCHED_NAME in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String schedName;

	/** 
	 * This attribute maps to the column ENTRY_ID in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String entryId;

	/** 
	 * This attribute maps to the column TRIGGER_NAME in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String triggerName;

	/** 
	 * This attribute maps to the column TRIGGER_GROUP in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String triggerGroup;

	/** 
	 * This attribute maps to the column INSTANCE_NAME in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String instanceName;

	/** 
	 * This attribute maps to the column FIRED_TIME in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected long firedTime;

	/** 
	 * This attribute maps to the column SCHED_TIME in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected long schedTime;

	/** 
	 * This attribute maps to the column PRIORITY in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected int priority;

	/** 
	 * This attribute maps to the column STATE in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String state;

	/** 
	 * This attribute maps to the column JOB_NAME in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String jobName;

	/** 
	 * This attribute maps to the column JOB_GROUP in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String jobGroup;

	/** 
	 * This attribute maps to the column IS_NONCONCURRENT in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String isNonconcurrent;

	/** 
	 * This attribute maps to the column REQUESTS_RECOVERY in the QRTZ_FIRED_TRIGGERS table.
	 */
	protected String requestsRecovery;

	/**
	 * Method 'QrtzFiredTriggersDao'
	 * 
	 */
	public QrtzFiredTriggersDao()
	{
	}

	/**
	 * Method 'getSchedName'
	 * 
	 * @return String
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/**
	 * Method 'setSchedName'
	 * 
	 * @param schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/**
	 * Method 'getEntryId'
	 * 
	 * @return String
	 */
	public String getEntryId()
	{
		return entryId;
	}

	/**
	 * Method 'setEntryId'
	 * 
	 * @param entryId
	 */
	public void setEntryId(String entryId)
	{
		this.entryId = entryId;
	}

	/**
	 * Method 'getTriggerName'
	 * 
	 * @return String
	 */
	public String getTriggerName()
	{
		return triggerName;
	}

	/**
	 * Method 'setTriggerName'
	 * 
	 * @param triggerName
	 */
	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	/**
	 * Method 'getTriggerGroup'
	 * 
	 * @return String
	 */
	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	/**
	 * Method 'setTriggerGroup'
	 * 
	 * @param triggerGroup
	 */
	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	/**
	 * Method 'getInstanceName'
	 * 
	 * @return String
	 */
	public String getInstanceName()
	{
		return instanceName;
	}

	/**
	 * Method 'setInstanceName'
	 * 
	 * @param instanceName
	 */
	public void setInstanceName(String instanceName)
	{
		this.instanceName = instanceName;
	}

	/**
	 * Method 'getFiredTime'
	 * 
	 * @return long
	 */
	public long getFiredTime()
	{
		return firedTime;
	}

	/**
	 * Method 'setFiredTime'
	 * 
	 * @param firedTime
	 */
	public void setFiredTime(long firedTime)
	{
		this.firedTime = firedTime;
	}

	/**
	 * Method 'getSchedTime'
	 * 
	 * @return long
	 */
	public long getSchedTime()
	{
		return schedTime;
	}

	/**
	 * Method 'setSchedTime'
	 * 
	 * @param schedTime
	 */
	public void setSchedTime(long schedTime)
	{
		this.schedTime = schedTime;
	}

	/**
	 * Method 'getPriority'
	 * 
	 * @return int
	 */
	public int getPriority()
	{
		return priority;
	}

	/**
	 * Method 'setPriority'
	 * 
	 * @param priority
	 */
	public void setPriority(int priority)
	{
		this.priority = priority;
	}

	/**
	 * Method 'getState'
	 * 
	 * @return String
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * Method 'setState'
	 * 
	 * @param state
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * Method 'getJobName'
	 * 
	 * @return String
	 */
	public String getJobName()
	{
		return jobName;
	}

	/**
	 * Method 'setJobName'
	 * 
	 * @param jobName
	 */
	public void setJobName(String jobName)
	{
		this.jobName = jobName;
	}

	/**
	 * Method 'getJobGroup'
	 * 
	 * @return String
	 */
	public String getJobGroup()
	{
		return jobGroup;
	}

	/**
	 * Method 'setJobGroup'
	 * 
	 * @param jobGroup
	 */
	public void setJobGroup(String jobGroup)
	{
		this.jobGroup = jobGroup;
	}

	/**
	 * Method 'getIsNonconcurrent'
	 * 
	 * @return String
	 */
	public String getIsNonconcurrent()
	{
		return isNonconcurrent;
	}

	/**
	 * Method 'setIsNonconcurrent'
	 * 
	 * @param isNonconcurrent
	 */
	public void setIsNonconcurrent(String isNonconcurrent)
	{
		this.isNonconcurrent = isNonconcurrent;
	}

	/**
	 * Method 'getRequestsRecovery'
	 * 
	 * @return String
	 */
	public String getRequestsRecovery()
	{
		return requestsRecovery;
	}

	/**
	 * Method 'setRequestsRecovery'
	 * 
	 * @param requestsRecovery
	 */
	public void setRequestsRecovery(String requestsRecovery)
	{
		this.requestsRecovery = requestsRecovery;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzFiredTriggersDao)) {
			return false;
		}
		
		final QrtzFiredTriggersDao _cast = (QrtzFiredTriggersDao) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (entryId == null ? _cast.entryId != entryId : !entryId.equals( _cast.entryId )) {
			return false;
		}
		
		if (triggerName == null ? _cast.triggerName != triggerName : !triggerName.equals( _cast.triggerName )) {
			return false;
		}
		
		if (triggerGroup == null ? _cast.triggerGroup != triggerGroup : !triggerGroup.equals( _cast.triggerGroup )) {
			return false;
		}
		
		if (instanceName == null ? _cast.instanceName != instanceName : !instanceName.equals( _cast.instanceName )) {
			return false;
		}
		
		if (firedTime != _cast.firedTime) {
			return false;
		}
		
		if (schedTime != _cast.schedTime) {
			return false;
		}
		
		if (priority != _cast.priority) {
			return false;
		}
		
		if (state == null ? _cast.state != state : !state.equals( _cast.state )) {
			return false;
		}
		
		if (jobName == null ? _cast.jobName != jobName : !jobName.equals( _cast.jobName )) {
			return false;
		}
		
		if (jobGroup == null ? _cast.jobGroup != jobGroup : !jobGroup.equals( _cast.jobGroup )) {
			return false;
		}
		
		if (isNonconcurrent == null ? _cast.isNonconcurrent != isNonconcurrent : !isNonconcurrent.equals( _cast.isNonconcurrent )) {
			return false;
		}
		
		if (requestsRecovery == null ? _cast.requestsRecovery != requestsRecovery : !requestsRecovery.equals( _cast.requestsRecovery )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (entryId != null) {
			_hashCode = 29 * _hashCode + entryId.hashCode();
		}
		
		if (triggerName != null) {
			_hashCode = 29 * _hashCode + triggerName.hashCode();
		}
		
		if (triggerGroup != null) {
			_hashCode = 29 * _hashCode + triggerGroup.hashCode();
		}
		
		if (instanceName != null) {
			_hashCode = 29 * _hashCode + instanceName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) (firedTime ^ (firedTime >>> 32));
		_hashCode = 29 * _hashCode + (int) (schedTime ^ (schedTime >>> 32));
		_hashCode = 29 * _hashCode + priority;
		if (state != null) {
			_hashCode = 29 * _hashCode + state.hashCode();
		}
		
		if (jobName != null) {
			_hashCode = 29 * _hashCode + jobName.hashCode();
		}
		
		if (jobGroup != null) {
			_hashCode = 29 * _hashCode + jobGroup.hashCode();
		}
		
		if (isNonconcurrent != null) {
			_hashCode = 29 * _hashCode + isNonconcurrent.hashCode();
		}
		
		if (requestsRecovery != null) {
			_hashCode = 29 * _hashCode + requestsRecovery.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return QrtzFiredTriggersDaoPk
	 */
	public QrtzFiredTriggersDaoPk createPk()
	{
		return new QrtzFiredTriggersDaoPk(schedName, entryId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzFiredTriggersDao: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", entryId=" + entryId );
		ret.append( ", triggerName=" + triggerName );
		ret.append( ", triggerGroup=" + triggerGroup );
		ret.append( ", instanceName=" + instanceName );
		ret.append( ", firedTime=" + firedTime );
		ret.append( ", schedTime=" + schedTime );
		ret.append( ", priority=" + priority );
		ret.append( ", state=" + state );
		ret.append( ", jobName=" + jobName );
		ret.append( ", jobGroup=" + jobGroup );
		ret.append( ", isNonconcurrent=" + isNonconcurrent );
		ret.append( ", requestsRecovery=" + requestsRecovery );
		return ret.toString();
	}

}
