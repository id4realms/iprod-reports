package com.id4.iprod.quartz.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the QRTZ_JOB_DETAILS table.
 */
public class QrtzJobDetailsDaoPk implements Serializable
{
	protected String schedName;

	protected String jobName;

	protected String jobGroup;

	/** 
	 * Sets the value of schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/** 
	 * Gets the value of schedName
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/** 
	 * Sets the value of jobName
	 */
	public void setJobName(String jobName)
	{
		this.jobName = jobName;
	}

	/** 
	 * Gets the value of jobName
	 */
	public String getJobName()
	{
		return jobName;
	}

	/** 
	 * Sets the value of jobGroup
	 */
	public void setJobGroup(String jobGroup)
	{
		this.jobGroup = jobGroup;
	}

	/** 
	 * Gets the value of jobGroup
	 */
	public String getJobGroup()
	{
		return jobGroup;
	}

	/**
	 * Method 'QrtzJobDetailsDaoPk'
	 * 
	 */
	public QrtzJobDetailsDaoPk()
	{
	}

	/**
	 * Method 'QrtzJobDetailsDaoPk'
	 * 
	 * @param schedName
	 * @param jobName
	 * @param jobGroup
	 */
	public QrtzJobDetailsDaoPk(final String schedName, final String jobName, final String jobGroup)
	{
		this.schedName = schedName;
		this.jobName = jobName;
		this.jobGroup = jobGroup;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzJobDetailsDaoPk)) {
			return false;
		}
		
		final QrtzJobDetailsDaoPk _cast = (QrtzJobDetailsDaoPk) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (jobName == null ? _cast.jobName != jobName : !jobName.equals( _cast.jobName )) {
			return false;
		}
		
		if (jobGroup == null ? _cast.jobGroup != jobGroup : !jobGroup.equals( _cast.jobGroup )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (jobName != null) {
			_hashCode = 29 * _hashCode + jobName.hashCode();
		}
		
		if (jobGroup != null) {
			_hashCode = 29 * _hashCode + jobGroup.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzJobDetailsDaoPk: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", jobName=" + jobName );
		ret.append( ", jobGroup=" + jobGroup );
		return ret.toString();
	}

}
