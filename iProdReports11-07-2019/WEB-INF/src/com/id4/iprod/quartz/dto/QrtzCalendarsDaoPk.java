package com.id4.iprod.quartz.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the QRTZ_CALENDARS table.
 */
public class QrtzCalendarsDaoPk implements Serializable
{
	protected String schedName;

	protected String calendarName;

	/** 
	 * Sets the value of schedName
	 */
	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	/** 
	 * Gets the value of schedName
	 */
	public String getSchedName()
	{
		return schedName;
	}

	/** 
	 * Sets the value of calendarName
	 */
	public void setCalendarName(String calendarName)
	{
		this.calendarName = calendarName;
	}

	/** 
	 * Gets the value of calendarName
	 */
	public String getCalendarName()
	{
		return calendarName;
	}

	/**
	 * Method 'QrtzCalendarsDaoPk'
	 * 
	 */
	public QrtzCalendarsDaoPk()
	{
	}

	/**
	 * Method 'QrtzCalendarsDaoPk'
	 * 
	 * @param schedName
	 * @param calendarName
	 */
	public QrtzCalendarsDaoPk(final String schedName, final String calendarName)
	{
		this.schedName = schedName;
		this.calendarName = calendarName;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof QrtzCalendarsDaoPk)) {
			return false;
		}
		
		final QrtzCalendarsDaoPk _cast = (QrtzCalendarsDaoPk) _other;
		if (schedName == null ? _cast.schedName != schedName : !schedName.equals( _cast.schedName )) {
			return false;
		}
		
		if (calendarName == null ? _cast.calendarName != calendarName : !calendarName.equals( _cast.calendarName )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (schedName != null) {
			_hashCode = 29 * _hashCode + schedName.hashCode();
		}
		
		if (calendarName != null) {
			_hashCode = 29 * _hashCode + calendarName.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.quartz.dto.QrtzCalendarsDaoPk: " );
		ret.append( "schedName=" + schedName );
		ret.append( ", calendarName=" + calendarName );
		return ret.toString();
	}

}
