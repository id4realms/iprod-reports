package com.id4.iprod;


public class IprodRoleWiseOperationPerms {

	public int operationId;

	public short isAllowed;
	
	public int getOperationId() {
		return operationId;
	}

	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}

	public short getIsAllowed()
	{
		return isAllowed;
	}

	/**
	 * Method 'setIsAllowed'
	 * 
	 * @param isAllowed
	 */
	public void setIsAllowed(short isAllowed)
	{
		this.isAllowed = isAllowed;
	}
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodRoleWiseOperationPerms: " );
		ret.append( "operationId=" + operationId );
		ret.append( ", isAllowed=" + isAllowed );
		
		
		return ret.toString();
	}
}
