package com.id4.iprod;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

	public class MacAddress{
		
	public static void main(String[] args)throws IOException{
			String address = new MacAddress().getMacAddress();
			//System.out.println(address);
	}

	public String getMacAddress() throws IOException{
	String macAddress = null;
	String osName = System.getProperty("os.name");
		if(osName.compareTo("Linux")==0){
		String command = "ifconfig -a";
		Process pid = Runtime.getRuntime().exec(command);
		BufferedReader in =	new BufferedReader(new InputStreamReader(pid.getInputStream()));
		while(true){
		String line = in.readLine();
		System.out.println("Line is : "+line);
		if (line == null)
		break;
		Pattern macPattern = Pattern.compile(".*((:?[0-9a-f]{2}[-:]){5}[0-9a-f]{2}).*",Pattern.CASE_INSENSITIVE);
		Matcher m = macPattern.matcher(line);
		if (m.matches()){
		macAddress = m.group(1).replaceAll("[-:]","");
		break;
		}
		}
		in.close();
		return macAddress;
		}else{
		String command = "ipconfig /all";
		Process pid = Runtime.getRuntime().exec(command);
		BufferedReader in = new BufferedReader(new InputStreamReader(pid.getInputStream()));
		while(true)
		{
		String line = in.readLine();
		//System.out.println("Line is : "+line);
		if(line == null)
		break;
		Pattern macPattern = Pattern.compile(".*((:?[0-9a-f]{2}[-:]){5}[0-9a-f]{2}).*",Pattern.CASE_INSENSITIVE);
		Matcher m = macPattern.matcher(line);
		if (m.matches()){
		macAddress = m.group(1).replaceAll("[-:]","");
		break;
		}
		}
		in.close();
		return macAddress;
		}
	}
}