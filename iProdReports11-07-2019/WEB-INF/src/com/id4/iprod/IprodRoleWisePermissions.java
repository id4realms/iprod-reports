package com.id4.iprod;
import java.util.List;

public class IprodRoleWisePermissions{
	
	
	public int moduleId;
	
	public List<IprodRoleWiseOperationPerms> operation;

	
	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	
	public List<IprodRoleWiseOperationPerms> getOperation() {
		return operation;
	}

	public void setOperation(List<IprodRoleWiseOperationPerms> operation) {
		this.operation = operation;
	}

	@Override
	public String toString() {
		return "IprodRoleWisePermissions [com.id4.iprod.dto.IprodRoleWiseOperationPerms: moduleId=" + moduleId + ", operation=" + operation + "]";
	}

	
}
