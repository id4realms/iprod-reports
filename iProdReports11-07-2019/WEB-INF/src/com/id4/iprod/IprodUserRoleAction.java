package com.id4.iprod;

import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.dao.IprodUserRoleDao;
import com.id4.iprod.dto.IprodUserRole;
import com.id4.iprod.dto.IprodUserRolePk;
import com.id4.iprod.exceptions.IprodUserRoleDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodUserRoleAction extends ActionSupport
{
	static Logger log = Logger.getLogger(IprodUserRoleAction.class);

	/** 
	 * This attribute maps to the column USER_ID in the iprod_user_role table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_user_role table.
	 */
	protected int roleId;	
	protected int plantId;

	public String addUserRole(){
		try{
			log.info("IprodUserRoleAction :: addUserRole");	
		    Map<String, Object> session = ActionContext.getContext().getSession();
			 if(session.get(Consts.IPRODUSERID)!= null){
				    IprodUserRoleDao userRoleDao = DaoFactory.createIprodUserRoleDao();
				    IprodUserRole userRoleDto = new IprodUserRole();
					IprodUserRolePk userRolePk = new IprodUserRolePk();
					userRoleDto.setUserId((Integer)session.get("NewUserId"));					
					userRoleDto.setRoleId(this.roleId);				
					userRolePk  =  userRoleDao.insert(userRoleDto);
					addActionMessage("User successfully added");
					return SUCCESS;
			  }else{
					 
					 addActionError("Sorry!! You have to Login first");
				     return "login";
			  } 
		}catch (Exception e){
			log.error("addUserRole :: Exception :: " + e.getMessage() +" occured in addUserRole.");
		}
		return SUCCESS;
	}

	public String updateUserRole(){	
		try{
			log.info("IprodUserRoleAction :: updateUserRole");	
		    Map<String, Object> session = ActionContext.getContext().getSession();
			 if(session.get(Consts.IPRODUSERID)!= null)
			    {
				 	IprodUserRoleDao userRoleDao = DaoFactory.createIprodUserRoleDao();
				    IprodUserRole userRoleDto = new IprodUserRole();
					IprodUserRolePk userRolePk = new IprodUserRolePk();
					userRoleDto.setUserId((Integer)session.get("IPRODUSERIDForUpdate"));				
					userRoleDto.setRoleId(this.roleId);				
					userRolePk.setUserId((Integer)session.get("IPRODUSERIDForUpdate"));				
					userRolePk.setRoleId(this.roleId);				
					try{
						userRoleDao.update(userRolePk,userRoleDto);
						addActionMessage("User ID : "+userRoleDto.getUserId()+" has been updated with Role ID: "+userRoleDto.getRoleId());
						session.remove("IPRODUSERIDForUpdate");
					 	return SUCCESS;
					}catch(IprodUserRoleDaoException e){
						addActionError("Cannot Update USer Role");
						addActionError("User ID : "+userRoleDto.getUserId()+" has not been updated with Role ID: "+userRoleDto.getRoleId());
						return ERROR;
					}
					
			    }else{				 
					 addActionError("Sorry!! You have to Login first");
				    	return "login";
				 }
		}catch (Exception e){
			log.error("updateUserRole :: Exception :: " + e.getMessage() +" occured in updateUserRole.");
		}
		return SUCCESS;
	}
	
	public String updateRole(){
		return SUCCESS; 
	}
	
	
	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}


	public int getPlantId() {
		return plantId;
	}


	public void setPlantId(int plantId) {
		this.plantId = plantId;
	}
}
