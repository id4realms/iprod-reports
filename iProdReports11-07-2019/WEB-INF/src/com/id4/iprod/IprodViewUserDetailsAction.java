package com.id4.iprod;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.id4.iprod.dao.IprodReportsLicenseDao;
import com.id4.iprod.dao.IprodRoleMasterDao;
import com.id4.iprod.dao.IprodViewRoleModuleOperationsDao;
import com.id4.iprod.dao.IprodViewUserDetailsDao;
import com.id4.iprod.dto.IprodReportsLicense;
import com.id4.iprod.dto.IprodRoleMaster;
import com.id4.iprod.dto.IprodViewUserDetails;
import com.id4.iprod.exceptions.IprodRoleMasterDaoException;
import com.id4.iprod.exceptions.IprodViewRoleModuleOperationsDaoException;
import com.id4.iprod.exceptions.IprodViewUserDetailsDaoException;
import com.id4.iprod.factory.DaoFactory;

import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodViewUserDetailsAction extends ActionSupport implements IprodModuleAuthorization
{
static Logger log = Logger.getLogger(IprodViewUserDetailsAction.class);

    /** 
     * This attribute maps to the column USER_ID in the iprod_view_user_details table.
     */
    protected int userId;

    /** 
     * This attribute maps to the column USER_NAME in the iprod_view_user_details table.
     */
    protected String userName;

    /** 
     * This attribute maps to the column USER_TITLE in the iprod_view_user_details table.
     */
    protected String userTitle;

    /** 
     * This attribute maps to the column FIRST_NAME in the iprod_view_user_details table.
     */
    protected String firstName;

    /** 
     * This attribute maps to the column LAST_NAME in the iprod_view_user_details table.
     */
    protected String lastName;

    /** 
     * This attribute maps to the column GENDER in the iprod_view_user_details table.
     */
    protected String gender;

    /** 
     * This attribute maps to the column EMAIL_ID in the iprod_view_user_details table.
     */
    protected String emailId;

    /** 
     * This attribute maps to the column CONTACT_NO in the iprod_view_user_details table.
     */
    protected String contactNo;

    /** 
     * This attribute maps to the column USER_PASSWORD in the iprod_view_user_details table.
     */
    protected String userPassword;

    /** 
     * This attribute maps to the column LAST_LOGIN_DETAILS in the iprod_view_user_details table.
     */
    protected String lastLoginDetails;

    /** 
     * This attribute maps to the column IS_DELETED in the iprod_view_user_details table.
     */
    protected short isDeleted;

    /** 
     * This attribute maps to the column ROLE_ID in the iprod_view_user_details table.
     */
    protected int roleId;

    /** 
     * This attribute represents whether the primitive attribute roleId is null.
     */
    protected boolean roleIdNull = true;

    /** 
     * This attribute maps to the column ROLE_NAME in the iprod_view_user_details table.
     */
    protected String roleName;
    
    protected int moduleId;
    /** 
     * This attribute maps to the column CREATE_PER in the iprod_view_role_module_permissions_details table.
     */
    protected short createPer;

    /** 
     * This attribute maps to the column RETRIEVE_PER in the iprod_view_role_module_permissions_details table.
     */
    protected short retrievePer;

    /** 
     * This attribute maps to the column UPDATE_PER in the iprod_view_role_module_permissions_details table.
     */
    protected short updatePer;

    /** 
     * This attribute maps to the column DELETE_PER in the iprod_view_role_module_permissions_details table.
     */
    protected short deletePer;

    /** 
     * This attribute maps to the column ALL_PER in the iprod_view_role_module_permissions_details table.
     */
    protected short allPer;
    
    public String currentUserId;

    /** 
     * This attribute maps to the column ROLE_DESC in the iprod_view_user_details table.
     */
    protected String roleDesc;
    protected String iprodLicense;
    protected String prevUser;
    protected Boolean iProdDevMode = false;
    protected Boolean enableMacAddressLicense = false;
    protected Boolean enableMotherBoardLicense = false;
    protected Boolean enableHardDiskLicense = true;
    public String ipaddress;
    

    /* Public List Here*/
    public List<IprodViewUserDetails> allUserListG = new ArrayList<IprodViewUserDetails>();
    public List<IprodViewUserDetails> selectUserDetailsListG = new ArrayList<IprodViewUserDetails>();
    public List<IprodViewUserDetails> allNotAllocUserListG = new ArrayList<IprodViewUserDetails>();
    public List<IprodViewUserDetails> allSupervisorForThisShiftListG = new ArrayList<IprodViewUserDetails>();
    public List<IprodViewUserDetails> allOperatorsForAllocationListG = new ArrayList<IprodViewUserDetails>();
    //public List<IprodViewRoleModuleOperationsDetails> accesspermissionsListL=new ArrayList<IprodViewRoleModulePermissionsDetails>();
    public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();
    
    public int getModuleId(){
		Map<String, Object> session = ActionContext.getContext().getSession();
    	session.put("ModuleId", 11);
		int mId=(Integer)session.get("ModuleId");
        return mId;
    }
    public String generatedLicKey(){
    	String licenseEncryptedKey = "";
    	
    	try{
            log.info("IprodViewUserDetailsAction :: generatedLicKey");                
            
            String algo = "SHA";
            MessageDigest mdObject = null;              
            if (enableMacAddressLicense == true){
                MacAddress macAddress = new MacAddress();
                byte[] plainMacId = null;
                try {
                                plainMacId = macAddress.getMacAddress().getBytes();
                } catch (IOException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                }
                try {
                                mdObject = MessageDigest.getInstance(algo);
                } catch (Exception e) {
                                e.printStackTrace();
                }
                mdObject.reset();
                mdObject.update(plainMacId);
                byte[] encryptionMBSN = mdObject.digest();
                StringBuilder encryptedMACID = new StringBuilder();
                for (int j = 0; j < encryptionMBSN.length; j++) {
                                if ((encryptionMBSN[j] & 0xff) < 0x10) {
                                                encryptedMACID.append("0");
                                }
                                encryptedMACID.append(Long.toString(encryptionMBSN[j] & 0xff,
                                                                16));
                }
                licenseEncryptedKey = licenseEncryptedKey.concat(encryptedMACID
                                                .toString());
            }

            // log.info("License Encrpyted Key after mac address scan is : "+licenseEncryptedKey);

            if (enableMotherBoardLicense == true){
                MotherboardSerialNumber motherboardSN = new MotherboardSerialNumber();
                String mSerialNo = motherboardSN.getMotherboardSN();
                byte[] plainMotherboardSN = mSerialNo.getBytes();
                try {
                                mdObject = MessageDigest.getInstance(algo);
                } catch (Exception e) {
                                e.printStackTrace();
                }
                mdObject.reset();
                mdObject.update(plainMotherboardSN);
                byte[] encryptionMBSN = mdObject.digest();
                StringBuilder encryptedMotherboardSN = new StringBuilder();
                for (int j = 0; j < encryptionMBSN.length; j++) {
                                if ((encryptionMBSN[j] & 0xff) < 0x10) {
                                                encryptedMotherboardSN.append("0");
                                }
                                encryptedMotherboardSN.append(Long.toString(
                                                                encryptionMBSN[j] & 0xff, 16));
                }
                licenseEncryptedKey = licenseEncryptedKey.concat(encryptedMotherboardSN.toString());
            }

            if (enableHardDiskLicense == true) {
	            HardDiskSerialNumber harddiskSN = new HardDiskSerialNumber();
	            String hddSerialNo = harddiskSN.getHardDiskSerialNumber("C");
	            // log.info("Plain hard disk serial number is : "+hddSerialNo);
	            byte[] plainHardDiskSN = hddSerialNo.getBytes();
	            try {
	                            mdObject = MessageDigest.getInstance(algo);
	            } catch (Exception e) {
	                            e.printStackTrace();
	            }
	            mdObject.reset();
	            mdObject.update(plainHardDiskSN);
	            byte[] encryptionHDDSN = mdObject.digest();
	            StringBuilder encryptedHardDiskSN = new StringBuilder();
	            for (int j = 0; j < encryptionHDDSN.length; j++) {
	                            if ((encryptionHDDSN[j] & 0xff) < 0x10) {
	                                            encryptedHardDiskSN.append("0");
	                            }
	                            encryptedHardDiskSN.append(Long.toString(
	                                                            encryptionHDDSN[j] & 0xff, 16));
	            }
	            licenseEncryptedKey = licenseEncryptedKey.concat(encryptedHardDiskSN.toString());
	            
	            log.info("generatedLicKey :: licenseEncryptedKey :: "+licenseEncryptedKey.toString());
                            
                            
                            
            }
    	}catch (Exception e){
			log.error("generatedLicKey :: Exception :: " + e.getMessage() +" occured in generatedLicKey.");
		}
            return licenseEncryptedKey;
    }
                                
	    public String encryptPassword(String password){
	    	StringBuilder sb = new StringBuilder();
	    	try{
	            log.info("IprodViewUserDetailsAction :: encryptPassword");
	            String algorithm = "SHA";
	            //String algo = "SHA";
	            byte[] plainText = password.getBytes();
	            MessageDigest md = null;
	            try {
	                            md = MessageDigest.getInstance(algorithm);
	            } catch (Exception e) {
	                            e.printStackTrace();
	            }
	            md.reset();
	            md.update(plainText);
	            byte[] encodedPassword = md.digest();
	           
	            for (int i = 0; i < encodedPassword.length; i++) {
	                            if ((encodedPassword[i] & 0xff) < 0x10) {
	                                            sb.append("0");
	                            }
	                            sb.append(Long.toString(encodedPassword[i] & 0xff,
	                                                            16));
	            }
	            //log.info("Plain    : " + password);
	           // log.info("encryptPassword :: Encrypted: " + sb.toString());
	    	}catch (Exception e){
				log.error("encryptPassword :: Exception :: " + e.getMessage() +" occured in encryptPassword.");
			}	    	
            return sb.toString();
	    }
                
	    public boolean validateUserAuthentication(String enteredUserPassword, String licenseKey){
	    	try{
		        log.info("IprodViewUserDetailsAction :: validateUserAuthentication");
		        Map<String, Object> session = ActionContext.getContext().getSession();
		        IprodViewUserDetailsDao userDao = DaoFactory.createIprodViewUserDetailsDao();     
		        List<IprodViewUserDetails> userListL = new ArrayList<IprodViewUserDetails>();
	            try{
	                userListL = userDao.findWhereUserNameEqualsAndIsDeleted(this.userName.toLowerCase().trim(),(short)0);// .toLowerCase().trim1()
	             if(userListL.size() > 0) {
	                	//log.info("validateUserAuthentication :: Found User : "+ userListL);
	                    String encryptedPassword = encryptPassword(enteredUserPassword);
	                	if(encryptedPassword.equals(userListL.get(0).getUserPassword())) {
	                      if(userListL.get(0).getUserIsDeleted() == 0) {
	                        log.info("validateUserAuthentication :: Setting required session variables after successful login");
	                        Date todaysDate = new Date();
	                        //if(session.isEmpty()){                                                                                                                     
	                            session.put("Context", todaysDate);
	                            session.put(Consts.IPRODUSERNAME,userListL.get(0).getUserName());
	                            session.put(Consts.IPRODFNAME, userListL.get(0).getFirstName());
	                            session.put(Consts.IPRODLNAME,userListL.get(0).getLastName());
	                            session.put(Consts.IPRODUSERROLENAME,userListL.get(0).getRoleName());
	                            session.put(Consts.IPRODUSERROLEID,userListL.get(0).getRoleId());
	                            //if(session.get(Consts.IPRODUSERROLEID)!= null){
		                            session.put(Consts.IPRODUSERID, userListL.get(0).getUserId());
		                            log.info("new ID:"+ Consts.IPRODUSERID);
	                            //}                                                                        
	                            session.put(Consts.LICENSEKEY, licenseKey); 
	                            log.info("registering licence key in session :: "+licenseKey);
	                         //}
	                         allUserListG = userListL;
	                         return true;
				        }
				        else{
		                    log.info("validateUserAuthentication :: User seems to be suspended or deleted. Please contact iPROD administrator");
		                    addActionError("User seems to be suspended or deleted. Please contact iPROD administrator");
		                    return false;
				        }
			        }else{
		                log.info("validateUserAuthentication :: Entered password does not match our database.");
		                addActionError("Entered password does not match our database.");
		                return false;           
			        }              
	            }            
	            else{
	                log.info("validateUserAuthentication :: User does not exists!");
	                addActionError("User does not exists!");
	                return false;
	            }
		        }catch(Exception ex){
	                log.error("validateUserAuthentication :: Exception in validating user login credentials. Contact iPROD administrator");
	                ex.printStackTrace();
	                addActionError("Exception in validating user login credentials. Contact iPROD administrator");
		            return false;
		        }   
	    	}catch (Exception e){
				log.error("validateUserAuthentication :: Exception :: " + e.getMessage() +" occured in validateUserAuthentication.");
			}	
	    	return false;
      }
	    
	  public String loginAttemptWithNewSession(String licenseKey){
		  try{
			  log.info("licenseKey while newSession:: "+licenseKey);
			  log.info("IprodViewUserDetailsAction :: loginAttemptWithNewSession");
			  Map<String, Object> session = ActionContext.getContext().getSession();
			  
	          if ((licenseKey.toString().compareTo(this.iprodLicense) == 0)) {
	        	  log.info("loginAttemptWithNewSession :: Checking license key for first time and if fine then registering in session");
	        	  //If licensekey matches check whether password is correct                                                         
	        	  if(validateUserAuthentication(this.userPassword,this.iprodLicense)==true){
	        		  log.info("loginAttemptWithNewSession :: User Authentication successful .. now proceeding");
	        		  if(session.get(Consts.IPRODUSERROLEID) != null){
	        			  log.info("loginAttemptWithNewSession :: Logged in users role in new session is currently : "+session.get(Consts.IPRODUSERROLEID).toString());
	        			  if(session.get(Consts.IPRODUSERROLEID).toString().equals("1") ){
		                        if(session.get(Consts.IPRODUSERID)!=null){
		                            log.info("loginAttemptWithNewSession :: Normal User already logged in");
		                            return SUCCESS;                                                                                               
		                        }
		                        else{
		                            log.info("loginAttemptWithNewSession :: Normal User not logged in!");
		                            addActionError("Normal User not logged in!");
		                            return ERROR;
		                        }
		                    }
	        			    else if(session.get(Consts.IPRODUSERROLEID).toString().equals("2"))
	        	            {
	        	                log.info("loginAttemptWithNewSession :: operator User logged in");
	        	                return "operatorsuccess";       
	        	            }
	        			    else{                                                                                                      
		                    	return SUCCESS;
		                     }
	                                                    
	                    }else{
	                        log.info("loginAttemptWithNewSession :: Logged user set ROLE does not permit you to use the system!");
	                        addActionError("Logged user set ROLE does not permit you to use the system. Please contact iPROD Administrator!");
	                        return ERROR;
	                    }
	                    
	                }
	                else{
	                      return ERROR;
	                }
				}else{                                                    
	            log.info("loginAttemptWithNewSession :: Invalid License Key.Please contact sales@id4-realms.com to grab a key now!");
	            addActionError("Invalid License Key.Please contact sales@id4-realms.com to grab a key now!");
	            return "getLicense";
	            }
		  }catch (Exception e){
				log.error("loginAttemptWithNewSession :: Exception :: " + e.getMessage() +" occured in loginAttemptWithNewSession.");
		  }	
		  return SUCCESS;
	  }
	  
	  public String loginAttemptWithAnActiveSession(String licenseKey){
		  try{
			  log.info("IprodViewUserDetailsAction :: loginAttemptWithAnActiveSession");
	          Map<String, Object> session = ActionContext.getContext().getSession();
	          if ((licenseKey.toString().compareTo(session.get(Consts.LICENSEKEY).toString()) == 0)) {                                                              
	          //If licensekey matches check whether password is correct                                                         
	          if(session.get(Consts.IPRODUSERROLEID) != null){
	            log.info("loginAttemptWithAnActiveSession :: Logged in users role in existing session is currently : "+session.get(Consts.IPRODUSERROLEID).toString());
	            if(session.get(Consts.IPRODUSERROLEID).toString().equals("1")){
	                  log.info("loginAttemptWithAnActiveSession :: Currently session user id is :: "+session.get(Consts.IPRODUSERID));
	            	  if(session.get(Consts.IPRODUSERROLEID).toString().equals("1") ){
	                        if(session.get(Consts.IPRODUSERID)!=null){
	                            log.info("loginAttemptWithAnActiveSession :: Normal User already logged in");
	                            return SUCCESS;                                                                                               
	                        }
	                        else{
	                            log.info("loginAttemptWithAnActiveSession :: Normal User not logged in!");
	                            addActionError("Normal User not logged in!");
	                            return ERROR;
	                        }
	                    }
	            	}
      			    else if(session.get(Consts.IPRODUSERROLEID).toString().equals("2"))
      	            {
      			    	 if(session.get(Consts.IPRODUSERID)!=null){
	                            log.info("loginAttemptWithAnActiveSession :: operatoruser already logged in");
	                            return "operatorsuccess";                                                                                               
	                        }
	                        else{
	                            log.info("loginAttemptWithAnActiveSession :: Operator User not logged in!");
	                            addActionError("Normal User not logged in!");
	                            return ERROR;
	                        }     
      	            }
      			   else{                                                                                                      
                	return SUCCESS;
                }
		      }else{
		          log.info("loginAttemptWithAnActiveSession :: Logged user set ROLE does not permit you to use the system!");
		          addActionError("Logged user set ROLE does not permit you to use the system. Please contact iPROD Administrator!");
		          return ERROR;
		      }
			}
			else{                                                      
		      log.info("loginAttemptWithAnActiveSession :: Invalid License Key.Please contact sales@id4-realms.com to grab a key now!");
		      addActionError("Invalid License Key.Please contact sales@id4-realms.com to grab a key now!");
		      return "getLicense";
			
	       }
		  }catch (Exception e){
				log.error("loginAttemptWithAnActiveSession :: Exception :: " + e.getMessage() +" occured in loginAttemptWithAnActiveSession.");
		  }	
		  return SUCCESS;
	 }
	  
	  
	  public String loginAttemptInGodMode(){
		  try{
		        //Devmode is set to ON
			  log.info("IprodViewUserDetailsAction :: loginAttemptInGodMode");
			  Map<String, Object> session = ActionContext.getContext().getSession();
				if (session.get(Consts.LICENSEKEY) != null || this.iprodLicense != "") {
					if(validateUserAuthentication(this.userPassword,this.iprodLicense)==true){
			            if(session.get(Consts.IPRODUSERROLEID) != null){
			                if(session.get(Consts.IPRODUSERROLEID).toString()!="4"){
			                    if(session.get(Consts.IPRODUSERID)!=null){
			                    	return SUCCESS;                                                                                               
			                    }
			                    else{
			                        log.info("loginAttemptInGodMode :: Normal User not logged in!");
			                        addActionError("Normal User not logged in!");
			                        return ERROR;
			                    }
			                }
			                else if(session.get(Consts.IPRODUSERROLEID).toString().equals("2"))
			                {
			                    log.info("loginAttemptInGodMode :: Supervisor already logged in");
			                    return SUCCESS;       
			                }
			                else if(session.get(Consts.IPRODUSERROLEID).toString().equals("5"))
			                {
			                    log.info("loginAttemptInGodMode :: PF1 User already logged in");
			                    return "pf1success";       
			                                
			                }
			                
			                else if(session.get(Consts.IPRODUSERROLEID).toString().equals("6"))
			                {
			                    log.info("loginAttemptInGodMode :: PF2 User already logged in");
			                    return "pf2success";       
			                                
			                }
			                else if(session.get(Consts.IPRODUSERROLEID).toString().equals("7"))
			                {
			                    log.info("loginAttemptInGodMode :: PF3 User already logged in");
			                    return "pf3success";       
			                                
			                }
			                else if(session.get(Consts.IPRODUSERROLEID).toString().equals("8"))
			                {
			                    log.info("loginAttemptInGodMode :: PF4 User already logged in");
			                    return "pf4success";       
			                                
			                }
			                else if(session.get(Consts.IPRODUSERROLEID).toString().equals("14"))
			                {
			                    log.info("loginAttemptInGodMode :: Final User already logged in");
			                    return "finalsuccess";     
			                                
			                }
			                else if(session.get(Consts.IPRODUSERROLEID).toString().equals("15"))
		                    {
	                            log.info("loginAttemptWithNewSession :: Electrical User User logged in");
	                            return "electricaltestingsuccess";     
		                                    
		                    }
			                else{                                                                                                      
			                    if(session.get(Consts.IPRODUSERID)!=null){
			                        return "andonsuccess";                                                                                                
			                    }
			                    else{
			                        log.info("loginAttemptInGodMode :: HMI User not logged in!");
			                        addActionError("HMI User not logged in!");
			                        return ERROR;
			                    }
			                }
			                                                
			        }else{
			            log.info("loginAttemptInGodMode :: Logged user set ROLE does not permit you to use the system!");
			            addActionError("Logged user set ROLE does not permit you to use the system. Please contact iPROD Administrator!");
			            return ERROR;
			        }
				}
				else{
				    return ERROR;
				}
			}else{
		        log.info("loginAttemptInGodMode :: Master key required in GOD MODE!");
		        addActionError("Master key required in GOD MODE!");
		        return ERROR;
			}
		  }catch (Exception e){
				log.error("loginAttemptInGodMode :: Exception :: " + e.getMessage() +" occured in loginAttemptInGodMode.");
		  }	
		  return SUCCESS;
	 }
                
      public String loginUser(){
    	  try{
			log.info("IprodViewUserDetailsAction :: loginUser");
			iProdDevMode = false;
			enableHardDiskLicense = true;
			
			Map<String, Object> session = ActionContext.getContext().getSession();
			log.info("loginUser :: User tring to login is : "+this.userName+" with password : "+this.userPassword);
			//log.info("loginUser :: License key claimed by User is : "+this.iprodLicense);
			log.info("loginUser :: IP address obtained from User is:"+ ipaddress);
			String licenseKey = "";
			String serviceName = "Tomcat8";  
		
				try {
					Process process = new ProcessBuilder("C:\\Windows\\System32\\sc.exe", "query" , serviceName ).start();
					InputStream is = process.getInputStream();
					InputStreamReader isr = new InputStreamReader(is);
					BufferedReader br = new BufferedReader(isr);
					
					String line;
					String scOutput = "";
					
						// Append the buffer lines into one string
						while ((line = br.readLine()) != null) {
						    scOutput +=  line + "\n" ;
						}
					
						if (scOutput.contains("STATE")) {
							if (scOutput.contains("RUNNING")) {
							log.info("Service running");
							}else{
								addActionError("iPROD Web App Server may be STOPPED. Please contact iPROD Administrator");
							    log.info("loginUser :: iPROD Web App Server may be STOPPED. Please contact iPROD Administrator");
							    return ERROR;
							}       
						}else{
							addActionError("Problem with iPROD Web App Server. Please contact iPROD Administrator");
						    log.info("loginUser :: Problem with iPROD Web App Server. Please contact iPROD Administrator");
						    return ERROR;
						}
				} catch (IOException e) {
					addActionError("Problem with iPROD Web App Server. Please contact iPROD Administrator");
					log.info("loginUser :: Problem with iPROD Web App Server. Please contact iPROD Administrator");
					return ERROR;
				}
            
                //First check whether dev mode is set ON
                if(iProdDevMode == false){                        
                    //generate license for system at runtime
                	try{
                    licenseKey = getHDDSerialNumber();
                   // log.info("loginUser :: getHDDSerialNumber() :: IPROD generated license key is :: "+licenseKey);
                                       
                  //got licence key from database 
        			  List<IprodReportsLicense> IprodReportsLicenseL=new  ArrayList<IprodReportsLicense>();        			  
        			  IprodReportsLicense iprodReportsLicenseDto=new IprodReportsLicense();
        			  IprodReportsLicenseDao iprodReportsLicenseDao=DaoFactory.createIprodReportsLicenseDao();
        			  IprodReportsLicenseL= iprodReportsLicenseDao.findWhereKeyValueEquals(licenseKey);
        			  if(IprodReportsLicenseL.size()>0){
        			  iprodLicense=IprodReportsLicenseL.get(0).getKeyValue();
        			 // log.info("Got key from database :: "+iprodLicense+" and leangth :: "+iprodLicense.length());
        			  }else{
        				  log.info("Licence Key Not Found in Database");
        				  iprodLicense="";
        			  }
                	}
                    catch(Exception ex){
        				log.error("loginUser :: Exception during generate lic key");
        				addActionError("Exception during generate lic key. Please contact iPROD Administrator : "+ex.toString());
        				return ERROR;
        		    }
                    //Check whether license is already set in session
                                
                    if(session.get(Consts.LICENSEKEY) != null) {
                    	try{                    		
                    	return loginAttemptWithAnActiveSession(licenseKey);
                    	}
                        catch(Exception ex){
            				log.error("loginUser :: Exception during loginAttemptWithAnActiveSession");
            				addActionError("Exception during loginAttemptWithAnActiveSession. Please contact iPROD Administrator : "+ex.toString());
            				return ERROR;
            		    }

		            }else{
			           //license key not set in session
		            	try{
		            	return loginAttemptWithNewSession(licenseKey);
			            }
	                    catch(Exception ex){
	        				log.error("loginUser :: Exception during loginAttemptWithNewSession");
	        				addActionError("Exception during loginAttemptWithNewSession. Please contact iPROD Administrator : "+ex.toString());
	        				return ERROR;
	        		    }
		            }
				}else{		
						try{
						return loginAttemptInGodMode();
						}
	                    catch(Exception ex){
	        				log.error("loginUser :: Exception during loginAttemptInGodMode");
	        				addActionError("Exception during loginAttemptInGodMode. Please contact iPROD Administrator : "+ex.toString());
	        				return ERROR;
	        		    }
				}     
    	  }catch (Exception e){
				log.error("loginUser :: Exception :: " + e.getMessage() +" occured in loginUser.");
		  }	
		  return SUCCESS;
      }
    String serial="";
      public String getHDDSerialNumber()
      {
     	 try{
     		  Process process = Runtime.getRuntime().exec(new String[] { "wmic", "bios", "get", "serialnumber" });
     	      process.getOutputStream().close();
     	      Scanner sc = new Scanner(process.getInputStream());
     	      String property = sc.next();
     	      serial = sc.next();
     	    //  System.out.println(property + ": " + serial);    
     		  }catch(Exception ex)
     		  {
     			 log.info("error :: "+ex.getMessage()); 
     		  }
     	return encryptPassword(serial.trim());     	 
      }
      
      public String getAllUsers() throws IprodViewRoleModuleOperationsDaoException{
    	  try{
			    log.info("IprodViewUserDetailsAction :: getAllUsers" +hashMapCheckRoleModuleOperationListG);
			    Map<String, Object> session = ActionContext.getContext().getSession();
			    //int roleId = (Integer) session.get(Consts.IPRODUSERROLEID);
			    if(session.get(Consts.IPRODUSERID)!= null){
			        IprodViewUserDetailsDao viewUserDetailsDao = DaoFactory.createIprodViewUserDetailsDao();
			        List<IprodViewUserDetails> allUserListL = new ArrayList<IprodViewUserDetails>();
			        IprodViewRoleModuleOperationsDao rolePermissionDao = DaoFactory.createIprodViewRoleModuleOperationsDao();
		            try{  
		            	
                            allUserListL = viewUserDetailsDao.findWhereIsDeletedAndRoleExceptAdminAndPlantIdEquals((short)0,(Integer)session.get(Consts.IPRODUSERROLEID));
                            log.info("allUserListG ::"+allUserListG);
		                
		                if(allUserListL.size()>0){
		                	allUserListG = allUserListL;
		                	log.info("allUserListG ::"+allUserListG);
		                }
		                else{
                            addActionError("Sorry!! Currently there seem to be no valid iProd registered users");
		                }
		               // accesspermissionsListL = rolePermissionDao.findWhereRoleIdAndModuleIdIs(roleId,this.moduleId);                                                                         
		                return SUCCESS;
		            }catch(IprodViewUserDetailsDaoException e){
                        addActionError("Cannot Find users list");
                        return ERROR;
		            }
			    }else{
			        addActionError("Sorry!! You have to Login first");
			        return "login";
			    }
    	  }catch (Exception e){
				log.error("getAllUsers :: Exception :: " + e.getMessage() +" occured in getAllUsers.");
		  }	
		  return SUCCESS;	                
      }
    
      public List<IprodRoleMaster> allRoleListG = new ArrayList<IprodRoleMaster>();

 	  public String findByUserId(){   
 		  try{
	         log.info("IprodViewUserDetailsAction :: findByUserId");
	         Map session = ActionContext.getContext().getSession();
	         if(session.get(Consts.IPRODUSERID)!= null){
	             IprodViewUserDetailsDao viewUserDetailsDao = DaoFactory.createIprodViewUserDetailsDao();                                                                  
	             List<IprodViewUserDetails> selectUserDetailsListL = new ArrayList<IprodViewUserDetails>();        
				//  List<IprodViewUserDetails> allRoleListL = new ArrayList<IprodViewUserDetails>();
				 try{
				     if(userId != 0)
				        selectUserDetailsListL = viewUserDetailsDao.findWhereUserIdEquals(userId);
				        if(selectUserDetailsListL.size() > 0){
				           selectUserDetailsListG = selectUserDetailsListL;
				        }else{                                                                                                   
				           addActionError("Failed to fetch user details");
				           return ERROR;
				         }
				        IprodRoleMasterDao roleDao = DaoFactory.createIprodRoleMasterDao();
				        List<IprodRoleMaster> allRoleListL = new ArrayList<IprodRoleMaster>();
				        try {                                                      
			               try {
			               	   	allRoleListL = roleDao.findWhereRoleIdNotEquals(1);
			               } catch (IprodRoleMasterDaoException e) {
								// TODO Auto-generated catch block
			            	   e.printStackTrace();
			               }                                                                    
				               if(allRoleListL.size() > 0){                                                                                                                                                                                                                                                                                                                 
				                  allRoleListG = allRoleListL;
							   }else{                                                  
							                  addActionError("Currently there are no roles to be displayed");
							                  return ERROR;
							   }
					      }
		                  finally
		                  {
		                                
		                  }
				                
				     }catch(IprodViewUserDetailsDaoException e) {
				    	 addActionError("Cannot Find User");
				         return ERROR;                                                  
			      }
		  }else{                   
		  addActionError("Sorry!! You have to Login first");
		  return "login";
		  }
	  }catch (Exception e){
				log.error("findByUserId :: Exception :: " + e.getMessage() +" occured in findByUserId.");
	  }	
	  return SUCCESS;
 	}
                 
 	/**
     * Method 'getUserId'
     * 
     * @return int
     */
    public int getUserId()
    {
                    return userId;
    }

    /**
     * Method 'setUserId'
     * 
     * @param userId
     */
    public void setUserId(int userId)
    {
                    this.userId = userId;
    }

    /**
     * Method 'getUserName'
     * 
     * @return String
     */
    public String getUserName()
    {
                    return userName;
    }

    /**
     * Method 'setUserName'
     * 
     * @param userName
     */
    public void setUserName(String userName)
    {
                    this.userName = userName;
    }

    /**
     * Method 'getUserTitle'
     * 
     * @return String
     */
    public String getUserTitle()
    {
                    return userTitle;
    }

    /**
     * Method 'setUserTitle'
     * 
     * @param userTitle
     */
    public void setUserTitle(String userTitle)
    {
                    this.userTitle = userTitle;
    }

    /**
     * Method 'getFirstName'
     * 
     * @return String
     */
    public String getFirstName()
    {
                    return firstName;
    }

    /**
     * Method 'setFirstName'
     * 
     * @param firstName
     */
    public void setFirstName(String firstName)
    {
                    this.firstName = firstName;
    }

    /**
     * Method 'getLastName'
     * 
     * @return String
     */
    public String getLastName()
    {
                    return lastName;
    }

    /**
     * Method 'setLastName'
     * 
     * @param lastName
     */
    public void setLastName(String lastName)
    {
                    this.lastName = lastName;
    }

    /**
     * Method 'getGender'
     * 
     * @return String
     */
    public String getGender()
    {
                    return gender;
    }

    /**
     * Method 'setGender'
     * 
     * @param gender
     */
    public void setGender(String gender)
    {
                    this.gender = gender;
    }

    /**
     * Method 'getEmailId'
     * 
     * @return String
     */
    public String getEmailId()
    {
                    return emailId;
    }

    /**
     * Method 'setEmailId'
     * 
     * @param emailId
     */
    public void setEmailId(String emailId)
    {
                    this.emailId = emailId;
    }

    /**
     * Method 'getContactNo'
     * 
     * @return String
     */
    public String getContactNo()
    {
                    return contactNo;
    }

    /**
     * Method 'setContactNo'
     * 
     * @param contactNo
     */
    public void setContactNo(String contactNo)
    {
                    this.contactNo = contactNo;
    }

    /**
     * Method 'getUserPassword'
     * 
     * @return String
     */
    public String getUserPassword()
    {
                    return userPassword;
    }

    /**
     * Method 'setUserPassword'
     * 
     * @param userPassword
     */
    public void setUserPassword(String userPassword)
    {
                    this.userPassword = userPassword;
    }

    /**
     * Method 'getLastLoginDetails'
     * 
     * @return String
     */
    public String getLastLoginDetails()
    {
                    return lastLoginDetails;
    }

    /**
     * Method 'setLastLoginDetails'
     * 
     * @param lastLoginDetails
     */
    public void setLastLoginDetails(String lastLoginDetails)
    {
                    this.lastLoginDetails = lastLoginDetails;
    }

    /**
     * Method 'getIsDeleted'
     * 
     * @return short
     */
    public short getIsDeleted()
    {
                    return isDeleted;
    }

    /**
     * Method 'setIsDeleted'
     * 
     * @param isDeleted
     */
    public void setIsDeleted(short isDeleted)
    {
                    this.isDeleted = isDeleted;
    }

    /**
     * Method 'getRoleId'
     * 
     * @return int
     */
    public int getRoleId()
    {
                    return roleId;
    }

    /**
     * Method 'setRoleId'
     * 
     * @param roleId
     */
    public void setRoleId(int roleId)
    {
                    this.roleId = roleId;
                    this.roleIdNull = false;
    }

    /**
     * Method 'setRoleIdNull'
     * 
     * @param value
     */
    public void setRoleIdNull(boolean value)
    {
                    this.roleIdNull = value;
    }

    /**
     * Method 'isRoleIdNull'
     * 
     * @return boolean
     */
    public boolean isRoleIdNull()
    {
                    return roleIdNull;
    }

    /**
     * Method 'getRoleName'
     * 
     * @return String
     */
    public String getRoleName()
    {
                    return roleName;
    }

    /**
     * Method 'setRoleName'
     * 
     * @param roleName
     */
    public void setRoleName(String roleName)
    {
                    this.roleName = roleName;
    }

    /**
     * Method 'getRoleDesc'
     * 
     * @return String
     */
    public String getRoleDesc()
    {
                    return roleDesc;
    }

	/**
	 * Method 'setRoleDesc'
	 * 
	 * @param roleDesc
	 */
	public void setRoleDesc(String roleDesc)
	{
	                this.roleDesc = roleDesc;
	}

	public String getPrevUser() {
	                return prevUser;
	}
	
	
	public void setPrevUser(String prevUser) {
	                this.prevUser = prevUser;
	}
	
	
	public String getIprodLicense() {
	                return iprodLicense;
	}
	
	
	public void setIprodLicense(String iprodLicense) {
	                this.iprodLicense = iprodLicense;
	}
	
	
	
	/**
	 * Method 'getCreatePer'
	 * 
	 * @return short
	 */
	public short getCreatePer()
	{
	                return createPer;
	}
	
	/**
	 * Method 'setCreatePer'
	 * 
	 * @param createPer
	 */
	public void setCreatePer(short createPer)
	{
	                this.createPer = createPer;
	}
	
	/**
	 * Method 'getRetrievePer'
	 * 
	 * @return short
	 */
	public short getRetrievePer()
	{
	                return retrievePer;
	}
	
	/**
	 * Method 'setRetrievePer'
	 * 
	 * @param retrievePer
	 */
	public void setRetrievePer(short retrievePer)
	{
	                this.retrievePer = retrievePer;
	}
	
	/**
	 * Method 'getUpdatePer'
	 * 
	 * @return short
	 */
	public short getUpdatePer()
	{
	                return updatePer;
	}
	
	/**
	 * Method 'setUpdatePer'
	 * 
	 * @param updatePer
	 */
	public void setUpdatePer(short updatePer)
	{
	                this.updatePer = updatePer;
	}
	
	/**
	 * Method 'getDeletePer'
	 * 
	 * @return short
	 */
	public short getDeletePer()
	{
	                return deletePer;
	}
	
	/**
	 * Method 'setDeletePer'
	 * 
	 * @param deletePer
	 */
	public void setDeletePer(short deletePer)
	{
	                this.deletePer = deletePer;
	}
	
	/**
	 * Method 'getAllPer'
	 * 
	 * @return short
	 */
	public short getAllPer()
	{
	                return allPer;
	}
	
	/**
	 * Method 'setAllPer'
	 * 
	 * @param allPer
	 */
	public void setAllPer(short allPer)
	{
	                this.allPer = allPer;
	}
	
	
	public String getIpaddress() {
	                return ipaddress;
	}
	
	
	public void setIpaddress(String ipaddress) {
	                this.ipaddress = ipaddress;
	}
		 public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
				return hashMapCheckRoleModuleOperationListG;
			}

			public void setHashMapCheckRoleModuleOperationListG(
					HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
				this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
			}
	

}
