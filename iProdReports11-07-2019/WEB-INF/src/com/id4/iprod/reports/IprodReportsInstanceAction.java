package com.id4.iprod.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.Consts;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.reports.dao.IprodReportsInstanceDao;
import com.id4.iprod.reports.dto.IprodReportsInstance;
import com.id4.iprod.reports.dto.IprodReportsInstancePk;
import com.id4.iprod.reports.exceptions.IprodReportsInstanceDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodReportsInstanceAction extends ActionSupport{
	
	static Logger log = Logger.getLogger(IprodReportsInstanceAction.class);

	
	/** 
	 * This attribute maps to the column REPORT_ID in the iprodreports_instance table.
	 */
	protected int reportId;

	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprodreports_instance table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column USER_ID in the iprodreports_instance table.
	 */
	protected int userId;
	
	/** 
	 * This attribute maps to the column USER_NAME in the iprodreports_instance table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column REPORT_QUERY in the iprodreports_instance table.
	 */
	protected String reportQuery;

	
	/** 
	 * This attribute maps to the column REPORT_EXCEL_LOCATION in the iprod_reports_instance table.
	 */
	protected String reportExcelLocation;
	/** 
	 * This attribute maps to the column REPORT_PDF_LOCATION in the iprodreports_instance table.
	 */
	protected String reportPdfLocation;

	/** 
	 * This attribute maps to the column REPORT_CDATETIME in the iprodreports_instance table.
	 */
	protected String reportCdatetime;

	/** 
	 * This attribute maps to the column REPORT_MDATETIME in the iprodreports_instance table.
	 */
	protected String reportMdatetime;

	/** 
	 * This attribute maps to the column IS_SCHEDULED in the iprodreports_instance table.
	 */
	protected short isScheduled;

	/** 
	 * This attribute represents whether the primitive attribute isScheduled is null.
	 */
	protected boolean isScheduledNull = true;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprodreports_instance table.
	 */
	protected short isDeleted;

	/** 
	 * This attribute represents whether the primitive attribute isDeleted is null.
	 */
	protected boolean isDeletedNull = true;
	
	public List<IprodReportsInstance> allInstanceListG = new ArrayList<IprodReportsInstance>();
	
	
	public String findAllInstances(){
		log.info("IprodReportsInstanceAction :: findAllInstances()");
		Map session = ActionContext.getContext().getSession();
		if(session.get(Consts.IPRODUSERID)!= null){
			IprodReportsInstanceDao instanceDao = DaoFactory.createIprodReportsInstanceDao();
			List<IprodReportsInstance> allInstanceListL = new ArrayList<IprodReportsInstance>();
	
		 try{
	  	  allInstanceListL =  instanceDao.findWhereIsDeletedEquals((short)0); 	
		  if(allInstanceListL.size()>0){
		  allInstanceListG = allInstanceListL;
		  }
		  else{
		   addActionError("Currently there are no templates to be displayed");
		  }
		  return SUCCESS;
		  }catch(IprodReportsInstanceDaoException e){
		   log.error(e.getMessage());
		   return ERROR;
		 }
    	}else{
    		addActionError("Sorry!! You have to Login first");
	    	return "login";
    	}    
		 
	 }

	
	/** 
	 * This function deletes the selected report instance from the selectedDatabase.
	 * Basically it marks IS_DELETED AS 1 and does not permanently delete the instance.
	 */
	public String deleteInstances() throws IprodReportsInstanceDaoException {
		log.info("IprodReportsInstanceAction : deleteInstances()");
	
		Map session = ActionContext.getContext().getSession();
		IprodReportsInstanceDao instanceDao = DaoFactory.createIprodReportsInstanceDao();
		IprodReportsInstance instanceDto = new IprodReportsInstance();
		List<IprodReportsInstance> instanceListL = new ArrayList<IprodReportsInstance>();
		
		try{
			instanceListL = instanceDao.findWhereReportIdEquals(this.reportId);
			IprodReportsInstancePk instancePk = new IprodReportsInstancePk();
			instancePk.setReportId(this.reportId);
			instanceDto.setReportId(this.reportId);
	 		instanceDto.setRtemplateId(instanceListL.get(0).getRtemplateId());
	 		instanceDto.setUserId(instanceListL.get(0).getUserId());
	 		instanceDto.setUserName(instanceListL.get(0).getUserName());
	 		instanceDto.setReportQuery(instanceListL.get(0).getReportQuery());
	 		instanceDto.setReportExcelLocation(instanceListL.get(0).getReportExcelLocation());
	 		instanceDto.setReportPdfLocation(instanceListL.get(0).getReportPdfLocation());
	 		instanceDto.setReportCdatetime(instanceListL.get(0).getReportCdatetime());
	 		instanceDto.setReportMdatetime(instanceListL.get(0).getReportMdatetime());
	 		instanceDto.setIsScheduled(instanceListL.get(0).getIsScheduled());
	 		instanceDto.setIsDeleted((short)1);
			instanceDao.update(instancePk, instanceDto);
			log.info("Report id " + this.reportId	+ " has been deleted");
    	 }catch (IprodReportsInstanceDaoException e) {
    		 addActionError("IprodReportsInstanceDaoException"+e.getMessage());
		 return ERROR;
		}
    	 addActionMessage("Template " + this.reportId	+ " has been deleted");	
			return SUCCESS;
	}
	

	/**
	 * Method 'getReportId'
	 * 
	 * @return int
	 */
	public int getReportId()
	{
		return reportId;
	}

	/**
	 * Method 'setReportId'
	 * 
	 * @param reportId
	 */
	public void setReportId(int reportId)
	{
		this.reportId = reportId;
	}

	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getReportQuery'
	 * 
	 * @return String
	 */
	public String getReportQuery()
	{
		return reportQuery;
	}

	/**
	 * Method 'setReportQuery'
	 * 
	 * @param reportQuery
	 */
	public void setReportQuery(String reportQuery)
	{
		this.reportQuery = reportQuery;
	}
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * Method 'getReportPdfLocation'
	 * 
	 * @return String
	 */
	public String getReportPdfLocation()
	{
		return reportPdfLocation;
	}

	/**
	 * Method 'setReportPdfLocation'
	 * 
	 * @param reportPdfLocation
	 */
	public void setReportPdfLocation(String reportPdfLocation)
	{
		this.reportPdfLocation = reportPdfLocation;
	}

	/**
	 * Method 'getReportCdatetime'
	 * 
	 * @return String
	 */
	public String getReportCdatetime()
	{
		return reportCdatetime;
	}

	/**
	 * Method 'setReportCdatetime'
	 * 
	 * @param reportCdatetime
	 */
	public void setReportCdatetime(String reportCdatetime)
	{
		this.reportCdatetime = reportCdatetime;
	}

	/**
	 * Method 'getReportMdatetime'
	 * 
	 * @return String
	 */
	public String getReportMdatetime()
	{
		return reportMdatetime;
	}

	/**
	 * Method 'setReportMdatetime'
	 * 
	 * @param reportMdatetime
	 */
	public void setReportMdatetime(String reportMdatetime)
	{
		this.reportMdatetime = reportMdatetime;
	}

	/**
	 * Method 'getIsScheduled'
	 * 
	 * @return short
	 */
	public short getIsScheduled()
	{
		return isScheduled;
	}

	/**
	 * Method 'setIsScheduled'
	 * 
	 * @param isScheduled
	 */
	public void setIsScheduled(short isScheduled)
	{
		this.isScheduled = isScheduled;
		this.isScheduledNull = false;
	}

	/**
	 * Method 'setIsScheduledNull'
	 * 
	 * @param value
	 */
	public void setIsScheduledNull(boolean value)
	{
		this.isScheduledNull = value;
	}

	/**
	 * Method 'isIsScheduledNull'
	 * 
	 * @return boolean
	 */
	public boolean isIsScheduledNull()
	{
		return isScheduledNull;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
		this.isDeletedNull = false;
	}

	/**
	 * Method 'setIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setIsDeletedNull(boolean value)
	{
		this.isDeletedNull = value;
	}

	/**
	 * Method 'isIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isIsDeletedNull()
	{
		return isDeletedNull;
	}
	
	/**
	 * Method 'createPk'
	 * 
	 * @return IprodReportsInstancePk
	 */
	public IprodReportsInstancePk createPk()
	{
		return new IprodReportsInstancePk(reportId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsInstance: " );
		ret.append( "reportId=" + reportId );
		ret.append( ", rtemplateId=" + rtemplateId );
		ret.append( ", userId=" + userId );
		ret.append( ", reportQuery=" + reportQuery );
		ret.append( ", reportExcelLocation=" + reportExcelLocation );
		ret.append( ", reportPdfLocation=" + reportPdfLocation );
		ret.append( ", reportCdatetime=" + reportCdatetime );
		ret.append( ", reportMdatetime=" + reportMdatetime );
		ret.append( ", isScheduled=" + isScheduled );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}


	public String getReportExcelLocation() {
		return reportExcelLocation;
	}


	public void setReportExcelLocation(String reportExcelLocation) {
		this.reportExcelLocation = reportExcelLocation;
	}


}
