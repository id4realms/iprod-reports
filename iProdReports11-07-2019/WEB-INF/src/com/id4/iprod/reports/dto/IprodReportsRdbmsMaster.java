package com.id4.iprod.reports.dto;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsRdbmsMaster implements Serializable
{
	/** 
	 * This attribute maps to the column RDBMS_ID in the iprod_reports_rdbms_master table.
	 */
	protected int rdbmsId;

	/** 
	 * This attribute maps to the column RDBMS_NAME in the iprod_reports_rdbms_master table.
	 */
	protected String rdbmsName;

	/** 
	 * This attribute maps to the column RDBMS_SUB_VERSION_NAME in the iprod_reports_rdbms_master table.
	 */
	protected String rdbmsSubVersionName;

	/** 
	 * This attribute maps to the column RDBMS_DESC in the iprod_reports_rdbms_master table.
	 */
	protected String rdbmsDesc;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_CLASS in the iprod_reports_rdbms_master table.
	 */
	protected String rdbmsJdbcDriverClass;

	/** 
	 * This attribute maps to the column RDBMS_DEFAULT_PORT in the iprod_reports_rdbms_master table.
	 */
	protected String rdbmsDefaultPort;

	/** 
	 * This attribute maps to the column RDBMS_CDATETIME in the iprod_reports_rdbms_master table.
	 */
	protected String rdbmsCdatetime;

	/** 
	 * This attribute maps to the column RDBMS_MDATETIME in the iprod_reports_rdbms_master table.
	 */
	protected String rdbmsMdatetime;

	/** 
	 * This attribute maps to the column RDBMS_IS_DELETED in the iprod_reports_rdbms_master table.
	 */
	protected short rdbmsIsDeleted;

	/**
	 * Method 'IprodReportsRdbmsMaster'
	 * 
	 */
	public IprodReportsRdbmsMaster()
	{
	}

	/**
	 * Method 'getRdbmsId'
	 * 
	 * @return int
	 */
	public int getRdbmsId()
	{
		return rdbmsId;
	}

	/**
	 * Method 'setRdbmsId'
	 * 
	 * @param rdbmsId
	 */
	public void setRdbmsId(int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
	}

	/**
	 * Method 'getRdbmsName'
	 * 
	 * @return String
	 */
	public String getRdbmsName()
	{
		return rdbmsName;
	}

	/**
	 * Method 'setRdbmsName'
	 * 
	 * @param rdbmsName
	 */
	public void setRdbmsName(String rdbmsName)
	{
		this.rdbmsName = rdbmsName;
	}

	/**
	 * Method 'getRdbmsSubVersionName'
	 * 
	 * @return String
	 */
	public String getRdbmsSubVersionName()
	{
		return rdbmsSubVersionName;
	}

	/**
	 * Method 'setRdbmsSubVersionName'
	 * 
	 * @param rdbmsSubVersionName
	 */
	public void setRdbmsSubVersionName(String rdbmsSubVersionName)
	{
		this.rdbmsSubVersionName = rdbmsSubVersionName;
	}

	/**
	 * Method 'getRdbmsDesc'
	 * 
	 * @return String
	 */
	public String getRdbmsDesc()
	{
		return rdbmsDesc;
	}

	/**
	 * Method 'setRdbmsDesc'
	 * 
	 * @param rdbmsDesc
	 */
	public void setRdbmsDesc(String rdbmsDesc)
	{
		this.rdbmsDesc = rdbmsDesc;
	}

	/**
	 * Method 'getRdbmsJdbcDriverClass'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverClass()
	{
		return rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'setRdbmsJdbcDriverClass'
	 * 
	 * @param rdbmsJdbcDriverClass
	 */
	public void setRdbmsJdbcDriverClass(String rdbmsJdbcDriverClass)
	{
		this.rdbmsJdbcDriverClass = rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'getRdbmsDefaultPort'
	 * 
	 * @return String
	 */
	public String getRdbmsDefaultPort()
	{
		return rdbmsDefaultPort;
	}

	/**
	 * Method 'setRdbmsDefaultPort'
	 * 
	 * @param rdbmsDefaultPort
	 */
	public void setRdbmsDefaultPort(String rdbmsDefaultPort)
	{
		this.rdbmsDefaultPort = rdbmsDefaultPort;
	}

	/**
	 * Method 'getRdbmsCdatetime'
	 * 
	 * @return String
	 */
	public String getRdbmsCdatetime()
	{
		return rdbmsCdatetime;
	}

	/**
	 * Method 'setRdbmsCdatetime'
	 * 
	 * @param rdbmsCdatetime
	 */
	public void setRdbmsCdatetime(String rdbmsCdatetime)
	{
		this.rdbmsCdatetime = rdbmsCdatetime;
	}

	/**
	 * Method 'getRdbmsMdatetime'
	 * 
	 * @return String
	 */
	public String getRdbmsMdatetime()
	{
		return rdbmsMdatetime;
	}

	/**
	 * Method 'setRdbmsMdatetime'
	 * 
	 * @param rdbmsMdatetime
	 */
	public void setRdbmsMdatetime(String rdbmsMdatetime)
	{
		this.rdbmsMdatetime = rdbmsMdatetime;
	}

	/**
	 * Method 'getRdbmsIsDeleted'
	 * 
	 * @return short
	 */
	public short getRdbmsIsDeleted()
	{
		return rdbmsIsDeleted;
	}

	/**
	 * Method 'setRdbmsIsDeleted'
	 * 
	 * @param rdbmsIsDeleted
	 */
	public void setRdbmsIsDeleted(short rdbmsIsDeleted)
	{
		this.rdbmsIsDeleted = rdbmsIsDeleted;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsRdbmsMaster)) {
			return false;
		}
		
		final IprodReportsRdbmsMaster _cast = (IprodReportsRdbmsMaster) _other;
		if (rdbmsId != _cast.rdbmsId) {
			return false;
		}
		
		if (rdbmsName == null ? _cast.rdbmsName != rdbmsName : !rdbmsName.equals( _cast.rdbmsName )) {
			return false;
		}
		
		if (rdbmsSubVersionName == null ? _cast.rdbmsSubVersionName != rdbmsSubVersionName : !rdbmsSubVersionName.equals( _cast.rdbmsSubVersionName )) {
			return false;
		}
		
		if (rdbmsDesc == null ? _cast.rdbmsDesc != rdbmsDesc : !rdbmsDesc.equals( _cast.rdbmsDesc )) {
			return false;
		}
		
		if (rdbmsJdbcDriverClass == null ? _cast.rdbmsJdbcDriverClass != rdbmsJdbcDriverClass : !rdbmsJdbcDriverClass.equals( _cast.rdbmsJdbcDriverClass )) {
			return false;
		}
		
		if (rdbmsDefaultPort == null ? _cast.rdbmsDefaultPort != rdbmsDefaultPort : !rdbmsDefaultPort.equals( _cast.rdbmsDefaultPort )) {
			return false;
		}
		
		if (rdbmsCdatetime == null ? _cast.rdbmsCdatetime != rdbmsCdatetime : !rdbmsCdatetime.equals( _cast.rdbmsCdatetime )) {
			return false;
		}
		
		if (rdbmsMdatetime == null ? _cast.rdbmsMdatetime != rdbmsMdatetime : !rdbmsMdatetime.equals( _cast.rdbmsMdatetime )) {
			return false;
		}
		
		if (rdbmsIsDeleted != _cast.rdbmsIsDeleted) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + rdbmsId;
		if (rdbmsName != null) {
			_hashCode = 29 * _hashCode + rdbmsName.hashCode();
		}
		
		if (rdbmsSubVersionName != null) {
			_hashCode = 29 * _hashCode + rdbmsSubVersionName.hashCode();
		}
		
		if (rdbmsDesc != null) {
			_hashCode = 29 * _hashCode + rdbmsDesc.hashCode();
		}
		
		if (rdbmsJdbcDriverClass != null) {
			_hashCode = 29 * _hashCode + rdbmsJdbcDriverClass.hashCode();
		}
		
		if (rdbmsDefaultPort != null) {
			_hashCode = 29 * _hashCode + rdbmsDefaultPort.hashCode();
		}
		
		if (rdbmsCdatetime != null) {
			_hashCode = 29 * _hashCode + rdbmsCdatetime.hashCode();
		}
		
		if (rdbmsMdatetime != null) {
			_hashCode = 29 * _hashCode + rdbmsMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) rdbmsIsDeleted;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodReportsRdbmsMasterPk
	 */
	public IprodReportsRdbmsMasterPk createPk()
	{
		return new IprodReportsRdbmsMasterPk(rdbmsId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsRdbmsMaster: " );
		ret.append( "rdbmsId=" + rdbmsId );
		ret.append( ", rdbmsName=" + rdbmsName );
		ret.append( ", rdbmsSubVersionName=" + rdbmsSubVersionName );
		ret.append( ", rdbmsDesc=" + rdbmsDesc );
		ret.append( ", rdbmsJdbcDriverClass=" + rdbmsJdbcDriverClass );
		ret.append( ", rdbmsDefaultPort=" + rdbmsDefaultPort );
		ret.append( ", rdbmsCdatetime=" + rdbmsCdatetime );
		ret.append( ", rdbmsMdatetime=" + rdbmsMdatetime );
		ret.append( ", rdbmsIsDeleted=" + rdbmsIsDeleted );
		return ret.toString();
	}

}
