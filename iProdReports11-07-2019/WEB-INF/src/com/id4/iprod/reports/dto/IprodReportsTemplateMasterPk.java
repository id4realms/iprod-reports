package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_template_master table.
 */
public class IprodReportsTemplateMasterPk implements Serializable
{
	protected int rtemplateId;

	/** 
	 * This attribute represents whether the primitive attribute rtemplateId is null.
	 */
	protected boolean rtemplateIdNull;

	/** 
	 * Sets the value of rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/** 
	 * Gets the value of rtemplateId
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'IprodReportsTemplateMasterPk'
	 * 
	 */
	public IprodReportsTemplateMasterPk()
	{
	}

	/**
	 * Method 'IprodReportsTemplateMasterPk'
	 * 
	 * @param rtemplateId
	 */
	public IprodReportsTemplateMasterPk(final int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/** 
	 * Sets the value of rtemplateIdNull
	 */
	public void setRtemplateIdNull(boolean rtemplateIdNull)
	{
		this.rtemplateIdNull = rtemplateIdNull;
	}

	/** 
	 * Gets the value of rtemplateIdNull
	 */
	public boolean isRtemplateIdNull()
	{
		return rtemplateIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsTemplateMasterPk)) {
			return false;
		}
		
		final IprodReportsTemplateMasterPk _cast = (IprodReportsTemplateMasterPk) _other;
		if (rtemplateId != _cast.rtemplateId) {
			return false;
		}
		
		if (rtemplateIdNull != _cast.rtemplateIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + rtemplateId;
		_hashCode = 29 * _hashCode + (rtemplateIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsTemplateMasterPk: " );
		ret.append( "rtemplateId=" + rtemplateId );
		return ret.toString();
	}

}
