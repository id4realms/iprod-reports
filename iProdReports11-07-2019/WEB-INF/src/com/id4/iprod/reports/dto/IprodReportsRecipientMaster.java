package com.id4.iprod.reports.dto;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsRecipientMaster implements Serializable
{
	/** 
	 * This attribute maps to the column RECIPIENT_ID in the iprod_reports_recipient_master table.
	 */
	protected int recipientId;

	/** 
	 * This attribute maps to the column FIRST_NAME in the iprod_reports_recipient_master table.
	 */
	protected String firstName;

	/** 
	 * This attribute maps to the column LAST_NAME in the iprod_reports_recipient_master table.
	 */
	protected String lastName;

	/** 
	 * This attribute maps to the column EMAIL_ID in the iprod_reports_recipient_master table.
	 */
	protected String emailId;

	/** 
	 * This attribute maps to the column CONTACT_NO in the iprod_reports_recipient_master table.
	 */
	protected String contactNo;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_reports_recipient_master table.
	 */
	protected int isDeleted;

	/** 
	 * This attribute maps to the column RECIPIENT_CDATETIME in the iprod_reports_recipient_master table.
	 */
	protected String recipientCdatetime;

	/** 
	 * This attribute maps to the column RECIPIENT_MDATETIME in the iprod_reports_recipient_master table.
	 */
	protected String recipientMdatetime;

	/**
	 * Method 'IprodReportsRecipientMaster'
	 * 
	 */
	public IprodReportsRecipientMaster()
	{
	}

	/**
	 * Method 'getRecipientId'
	 * 
	 * @return int
	 */
	public int getRecipientId()
	{
		return recipientId;
	}

	/**
	 * Method 'setRecipientId'
	 * 
	 * @param recipientId
	 */
	public void setRecipientId(int recipientId)
	{
		this.recipientId = recipientId;
	}

	/**
	 * Method 'getFirstName'
	 * 
	 * @return String
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Method 'setFirstName'
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Method 'getLastName'
	 * 
	 * @return String
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Method 'setLastName'
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Method 'getEmailId'
	 * 
	 * @return String
	 */
	public String getEmailId()
	{
		return emailId;
	}

	/**
	 * Method 'setEmailId'
	 * 
	 * @param emailId
	 */
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	/**
	 * Method 'getContactNo'
	 * 
	 * @return String
	 */
	public String getContactNo()
	{
		return contactNo;
	}

	/**
	 * Method 'setContactNo'
	 * 
	 * @param contactNo
	 */
	public void setContactNo(String contactNo)
	{
		this.contactNo = contactNo;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return int
	 */
	public int getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(int isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Method 'getRecipientCdatetime'
	 * 
	 * @return String
	 */
	public String getRecipientCdatetime()
	{
		return recipientCdatetime;
	}

	/**
	 * Method 'setRecipientCdatetime'
	 * 
	 * @param recipientCdatetime
	 */
	public void setRecipientCdatetime(String recipientCdatetime)
	{
		this.recipientCdatetime = recipientCdatetime;
	}

	/**
	 * Method 'getRecipientMdatetime'
	 * 
	 * @return String
	 */
	public String getRecipientMdatetime()
	{
		return recipientMdatetime;
	}

	/**
	 * Method 'setRecipientMdatetime'
	 * 
	 * @param recipientMdatetime
	 */
	public void setRecipientMdatetime(String recipientMdatetime)
	{
		this.recipientMdatetime = recipientMdatetime;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsRecipientMaster)) {
			return false;
		}
		
		final IprodReportsRecipientMaster _cast = (IprodReportsRecipientMaster) _other;
		if (recipientId != _cast.recipientId) {
			return false;
		}
		
		if (firstName == null ? _cast.firstName != firstName : !firstName.equals( _cast.firstName )) {
			return false;
		}
		
		if (lastName == null ? _cast.lastName != lastName : !lastName.equals( _cast.lastName )) {
			return false;
		}
		
		if (emailId == null ? _cast.emailId != emailId : !emailId.equals( _cast.emailId )) {
			return false;
		}
		
		if (contactNo == null ? _cast.contactNo != contactNo : !contactNo.equals( _cast.contactNo )) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		if (recipientCdatetime == null ? _cast.recipientCdatetime != recipientCdatetime : !recipientCdatetime.equals( _cast.recipientCdatetime )) {
			return false;
		}
		
		if (recipientMdatetime == null ? _cast.recipientMdatetime != recipientMdatetime : !recipientMdatetime.equals( _cast.recipientMdatetime )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + recipientId;
		if (firstName != null) {
			_hashCode = 29 * _hashCode + firstName.hashCode();
		}
		
		if (lastName != null) {
			_hashCode = 29 * _hashCode + lastName.hashCode();
		}
		
		if (emailId != null) {
			_hashCode = 29 * _hashCode + emailId.hashCode();
		}
		
		if (contactNo != null) {
			_hashCode = 29 * _hashCode + contactNo.hashCode();
		}
		
		_hashCode = 29 * _hashCode + isDeleted;
		if (recipientCdatetime != null) {
			_hashCode = 29 * _hashCode + recipientCdatetime.hashCode();
		}
		
		if (recipientMdatetime != null) {
			_hashCode = 29 * _hashCode + recipientMdatetime.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodReportsRecipientMasterPk
	 */
	public IprodReportsRecipientMasterPk createPk()
	{
		return new IprodReportsRecipientMasterPk(recipientId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsRecipientMaster: " );
		ret.append( "recipientId=" + recipientId );
		ret.append( ", firstName=" + firstName );
		ret.append( ", lastName=" + lastName );
		ret.append( ", emailId=" + emailId );
		ret.append( ", contactNo=" + contactNo );
		ret.append( ", isDeleted=" + isDeleted );
		ret.append( ", recipientCdatetime=" + recipientCdatetime );
		ret.append( ", recipientMdatetime=" + recipientMdatetime );
		return ret.toString();
	}

}
