package com.id4.iprod.reports.dto;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsViewReportTemplateDetails implements Serializable
{
	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprod_reports_view_report_template_details table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column RTEMPLATE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateName;

	/** 
	 * This attribute maps to the column RTEMPLATE_TITLE in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SUB_TITLE in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateSubTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateSqlquery;

	/** 
	 * This attribute maps to the column RTEMPLATE_EXCEL_DATA_STORAGE in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateExcelDataStorage;

	/** 
	 * This attribute maps to the column RTEMPLATE_PDF_DATA_STORAGE in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplatePdfDataStorage;

	/** 
	 * This attribute maps to the column RTEMPLATE_BIRT_RPT in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateBirtRpt;

	/** 
	 * This attribute maps to the column RTEMPLATE_CDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateCdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_MDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateMdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_IS_DELETED in the iprod_reports_view_report_template_details table.
	 */
	protected short rtemplateIsDeleted;

	/** 
	 * This attribute maps to the column DATASOURCE_ID in the iprod_reports_view_report_template_details table.
	 */
	protected int datasourceId;

	/** 
	 * This attribute maps to the column DATASOURCE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String datasourceName;

	/** 
	 * This attribute maps to the column RDBMS_ID in the iprod_reports_view_report_template_details table.
	 */
	protected int rdbmsId;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsId is null.
	 */
	protected boolean rdbmsIdNull = true;

	/** 
	 * This attribute maps to the column RDBMS_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsName;

	/** 
	 * This attribute maps to the column RDBMS_SUB_VERSION_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsSubVersionName;

	/** 
	 * This attribute maps to the column RDBMS_DESC in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsDesc;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_CLASS in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsJdbcDriverClass;

	/** 
	 * This attribute maps to the column RDBMS_DEFAULT_PORT in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsDefaultPort;

	/** 
	 * This attribute maps to the column RDBMS_CDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsCdatetime;

	/** 
	 * This attribute maps to the column RDBMS_MDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsMdatetime;

	/** 
	 * This attribute maps to the column RDBMS_IS_DELETED in the iprod_reports_view_report_template_details table.
	 */
	protected short rdbmsIsDeleted;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsIsDeleted is null.
	 */
	protected boolean rdbmsIsDeletedNull = true;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_URL in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsJdbcDriverUrl;

	/** 
	 * This attribute maps to the column RDBMS_IS_WINDOWS_AUTH in the iprod_reports_view_report_template_details table.
	 */
	protected int rdbmsIsWindowsAuth;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsIsWindowsAuth is null.
	 */
	protected boolean rdbmsIsWindowsAuthNull = true;

	/** 
	 * This attribute maps to the column RDBMS_DATABASE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsDatabaseName;

	/** 
	 * This attribute maps to the column RDBMS_INSTANCE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsInstanceName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_USERNAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerUsername;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PASSWORD in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerPassword;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_MACHINE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerMachineName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PORT in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerPort;

	/** 
	 * This attribute maps to the column DATASOURCE_CDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String datasourceCdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_MDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String datasourceMdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_IS_DELETED in the iprod_reports_view_report_template_details table.
	 */
	protected short datasourceIsDeleted;

	/** 
	 * This attribute represents whether the primitive attribute datasourceIsDeleted is null.
	 */
	protected boolean datasourceIsDeletedNull = true;

	/**
	 * Method 'IprodReportsViewReportTemplateDetails'
	 * 
	 */
	public IprodReportsViewReportTemplateDetails()
	{
	}

	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getRtemplateName'
	 * 
	 * @return String
	 */
	public String getRtemplateName()
	{
		return rtemplateName;
	}

	/**
	 * Method 'setRtemplateName'
	 * 
	 * @param rtemplateName
	 */
	public void setRtemplateName(String rtemplateName)
	{
		this.rtemplateName = rtemplateName;
	}

	/**
	 * Method 'getRtemplateTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateTitle()
	{
		return rtemplateTitle;
	}

	/**
	 * Method 'setRtemplateTitle'
	 * 
	 * @param rtemplateTitle
	 */
	public void setRtemplateTitle(String rtemplateTitle)
	{
		this.rtemplateTitle = rtemplateTitle;
	}

	/**
	 * Method 'getRtemplateSubTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateSubTitle()
	{
		return rtemplateSubTitle;
	}

	/**
	 * Method 'setRtemplateSubTitle'
	 * 
	 * @param rtemplateSubTitle
	 */
	public void setRtemplateSubTitle(String rtemplateSubTitle)
	{
		this.rtemplateSubTitle = rtemplateSubTitle;
	}

	/**
	 * Method 'getRtemplateSqlquery'
	 * 
	 * @return String
	 */
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

	/**
	 * Method 'setRtemplateSqlquery'
	 * 
	 * @param rtemplateSqlquery
	 */
	public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	/**
	 * Method 'getRtemplateExcelDataStorage'
	 * 
	 * @return String
	 */
	public String getRtemplateExcelDataStorage()
	{
		return rtemplateExcelDataStorage;
	}

	/**
	 * Method 'setRtemplateExcelDataStorage'
	 * 
	 * @param rtemplateExcelDataStorage
	 */
	public void setRtemplateExcelDataStorage(String rtemplateExcelDataStorage)
	{
		this.rtemplateExcelDataStorage = rtemplateExcelDataStorage;
	}

	/**
	 * Method 'getRtemplatePdfDataStorage'
	 * 
	 * @return String
	 */
	public String getRtemplatePdfDataStorage()
	{
		return rtemplatePdfDataStorage;
	}

	/**
	 * Method 'setRtemplatePdfDataStorage'
	 * 
	 * @param rtemplatePdfDataStorage
	 */
	public void setRtemplatePdfDataStorage(String rtemplatePdfDataStorage)
	{
		this.rtemplatePdfDataStorage = rtemplatePdfDataStorage;
	}

	/**
	 * Method 'getRtemplateBirtRpt'
	 * 
	 * @return String
	 */
	public String getRtemplateBirtRpt()
	{
		return rtemplateBirtRpt;
	}

	/**
	 * Method 'setRtemplateBirtRpt'
	 * 
	 * @param rtemplateBirtRpt
	 */
	public void setRtemplateBirtRpt(String rtemplateBirtRpt)
	{
		this.rtemplateBirtRpt = rtemplateBirtRpt;
	}

	/**
	 * Method 'getRtemplateCdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateCdatetime()
	{
		return rtemplateCdatetime;
	}

	/**
	 * Method 'setRtemplateCdatetime'
	 * 
	 * @param rtemplateCdatetime
	 */
	public void setRtemplateCdatetime(String rtemplateCdatetime)
	{
		this.rtemplateCdatetime = rtemplateCdatetime;
	}

	/**
	 * Method 'getRtemplateMdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateMdatetime()
	{
		return rtemplateMdatetime;
	}

	/**
	 * Method 'setRtemplateMdatetime'
	 * 
	 * @param rtemplateMdatetime
	 */
	public void setRtemplateMdatetime(String rtemplateMdatetime)
	{
		this.rtemplateMdatetime = rtemplateMdatetime;
	}

	/**
	 * Method 'getRtemplateIsDeleted'
	 * 
	 * @return short
	 */
	public short getRtemplateIsDeleted()
	{
		return rtemplateIsDeleted;
	}

	/**
	 * Method 'setRtemplateIsDeleted'
	 * 
	 * @param rtemplateIsDeleted
	 */
	public void setRtemplateIsDeleted(short rtemplateIsDeleted)
	{
		this.rtemplateIsDeleted = rtemplateIsDeleted;
	}

	/**
	 * Method 'getDatasourceId'
	 * 
	 * @return int
	 */
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'setDatasourceId'
	 * 
	 * @param datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
	}

	/**
	 * Method 'getDatasourceName'
	 * 
	 * @return String
	 */
	public String getDatasourceName()
	{
		return datasourceName;
	}

	/**
	 * Method 'setDatasourceName'
	 * 
	 * @param datasourceName
	 */
	public void setDatasourceName(String datasourceName)
	{
		this.datasourceName = datasourceName;
	}

	/**
	 * Method 'getRdbmsId'
	 * 
	 * @return int
	 */
	public int getRdbmsId()
	{
		return rdbmsId;
	}

	/**
	 * Method 'setRdbmsId'
	 * 
	 * @param rdbmsId
	 */
	public void setRdbmsId(int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
		this.rdbmsIdNull = false;
	}

	/**
	 * Method 'setRdbmsIdNull'
	 * 
	 * @param value
	 */
	public void setRdbmsIdNull(boolean value)
	{
		this.rdbmsIdNull = value;
	}

	/**
	 * Method 'isRdbmsIdNull'
	 * 
	 * @return boolean
	 */
	public boolean isRdbmsIdNull()
	{
		return rdbmsIdNull;
	}

	/**
	 * Method 'getRdbmsName'
	 * 
	 * @return String
	 */
	public String getRdbmsName()
	{
		return rdbmsName;
	}

	/**
	 * Method 'setRdbmsName'
	 * 
	 * @param rdbmsName
	 */
	public void setRdbmsName(String rdbmsName)
	{
		this.rdbmsName = rdbmsName;
	}

	/**
	 * Method 'getRdbmsSubVersionName'
	 * 
	 * @return String
	 */
	public String getRdbmsSubVersionName()
	{
		return rdbmsSubVersionName;
	}

	/**
	 * Method 'setRdbmsSubVersionName'
	 * 
	 * @param rdbmsSubVersionName
	 */
	public void setRdbmsSubVersionName(String rdbmsSubVersionName)
	{
		this.rdbmsSubVersionName = rdbmsSubVersionName;
	}

	/**
	 * Method 'getRdbmsDesc'
	 * 
	 * @return String
	 */
	public String getRdbmsDesc()
	{
		return rdbmsDesc;
	}

	/**
	 * Method 'setRdbmsDesc'
	 * 
	 * @param rdbmsDesc
	 */
	public void setRdbmsDesc(String rdbmsDesc)
	{
		this.rdbmsDesc = rdbmsDesc;
	}

	/**
	 * Method 'getRdbmsJdbcDriverClass'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverClass()
	{
		return rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'setRdbmsJdbcDriverClass'
	 * 
	 * @param rdbmsJdbcDriverClass
	 */
	public void setRdbmsJdbcDriverClass(String rdbmsJdbcDriverClass)
	{
		this.rdbmsJdbcDriverClass = rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'getRdbmsDefaultPort'
	 * 
	 * @return String
	 */
	public String getRdbmsDefaultPort()
	{
		return rdbmsDefaultPort;
	}

	/**
	 * Method 'setRdbmsDefaultPort'
	 * 
	 * @param rdbmsDefaultPort
	 */
	public void setRdbmsDefaultPort(String rdbmsDefaultPort)
	{
		this.rdbmsDefaultPort = rdbmsDefaultPort;
	}

	/**
	 * Method 'getRdbmsCdatetime'
	 * 
	 * @return String
	 */
	public String getRdbmsCdatetime()
	{
		return rdbmsCdatetime;
	}

	/**
	 * Method 'setRdbmsCdatetime'
	 * 
	 * @param rdbmsCdatetime
	 */
	public void setRdbmsCdatetime(String rdbmsCdatetime)
	{
		this.rdbmsCdatetime = rdbmsCdatetime;
	}

	/**
	 * Method 'getRdbmsMdatetime'
	 * 
	 * @return String
	 */
	public String getRdbmsMdatetime()
	{
		return rdbmsMdatetime;
	}

	/**
	 * Method 'setRdbmsMdatetime'
	 * 
	 * @param rdbmsMdatetime
	 */
	public void setRdbmsMdatetime(String rdbmsMdatetime)
	{
		this.rdbmsMdatetime = rdbmsMdatetime;
	}

	/**
	 * Method 'getRdbmsIsDeleted'
	 * 
	 * @return short
	 */
	public short getRdbmsIsDeleted()
	{
		return rdbmsIsDeleted;
	}

	/**
	 * Method 'setRdbmsIsDeleted'
	 * 
	 * @param rdbmsIsDeleted
	 */
	public void setRdbmsIsDeleted(short rdbmsIsDeleted)
	{
		this.rdbmsIsDeleted = rdbmsIsDeleted;
		this.rdbmsIsDeletedNull = false;
	}

	/**
	 * Method 'setRdbmsIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setRdbmsIsDeletedNull(boolean value)
	{
		this.rdbmsIsDeletedNull = value;
	}

	/**
	 * Method 'isRdbmsIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isRdbmsIsDeletedNull()
	{
		return rdbmsIsDeletedNull;
	}

	/**
	 * Method 'getRdbmsJdbcDriverUrl'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverUrl()
	{
		return rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'setRdbmsJdbcDriverUrl'
	 * 
	 * @param rdbmsJdbcDriverUrl
	 */
	public void setRdbmsJdbcDriverUrl(String rdbmsJdbcDriverUrl)
	{
		this.rdbmsJdbcDriverUrl = rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'getRdbmsIsWindowsAuth'
	 * 
	 * @return int
	 */
	public int getRdbmsIsWindowsAuth()
	{
		return rdbmsIsWindowsAuth;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuth'
	 * 
	 * @param rdbmsIsWindowsAuth
	 */
	public void setRdbmsIsWindowsAuth(int rdbmsIsWindowsAuth)
	{
		this.rdbmsIsWindowsAuth = rdbmsIsWindowsAuth;
		this.rdbmsIsWindowsAuthNull = false;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuthNull'
	 * 
	 * @param value
	 */
	public void setRdbmsIsWindowsAuthNull(boolean value)
	{
		this.rdbmsIsWindowsAuthNull = value;
	}

	/**
	 * Method 'isRdbmsIsWindowsAuthNull'
	 * 
	 * @return boolean
	 */
	public boolean isRdbmsIsWindowsAuthNull()
	{
		return rdbmsIsWindowsAuthNull;
	}

	/**
	 * Method 'getRdbmsDatabaseName'
	 * 
	 * @return String
	 */
	public String getRdbmsDatabaseName()
	{
		return rdbmsDatabaseName;
	}

	/**
	 * Method 'setRdbmsDatabaseName'
	 * 
	 * @param rdbmsDatabaseName
	 */
	public void setRdbmsDatabaseName(String rdbmsDatabaseName)
	{
		this.rdbmsDatabaseName = rdbmsDatabaseName;
	}

	/**
	 * Method 'getRdbmsInstanceName'
	 * 
	 * @return String
	 */
	public String getRdbmsInstanceName()
	{
		return rdbmsInstanceName;
	}

	/**
	 * Method 'setRdbmsInstanceName'
	 * 
	 * @param rdbmsInstanceName
	 */
	public void setRdbmsInstanceName(String rdbmsInstanceName)
	{
		this.rdbmsInstanceName = rdbmsInstanceName;
	}

	/**
	 * Method 'getRdbmsServerUsername'
	 * 
	 * @return String
	 */
	public String getRdbmsServerUsername()
	{
		return rdbmsServerUsername;
	}

	/**
	 * Method 'setRdbmsServerUsername'
	 * 
	 * @param rdbmsServerUsername
	 */
	public void setRdbmsServerUsername(String rdbmsServerUsername)
	{
		this.rdbmsServerUsername = rdbmsServerUsername;
	}

	/**
	 * Method 'getRdbmsServerPassword'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPassword()
	{
		return rdbmsServerPassword;
	}

	/**
	 * Method 'setRdbmsServerPassword'
	 * 
	 * @param rdbmsServerPassword
	 */
	public void setRdbmsServerPassword(String rdbmsServerPassword)
	{
		this.rdbmsServerPassword = rdbmsServerPassword;
	}

	/**
	 * Method 'getRdbmsServerMachineName'
	 * 
	 * @return String
	 */
	public String getRdbmsServerMachineName()
	{
		return rdbmsServerMachineName;
	}

	/**
	 * Method 'setRdbmsServerMachineName'
	 * 
	 * @param rdbmsServerMachineName
	 */
	public void setRdbmsServerMachineName(String rdbmsServerMachineName)
	{
		this.rdbmsServerMachineName = rdbmsServerMachineName;
	}

	/**
	 * Method 'getRdbmsServerPort'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPort()
	{
		return rdbmsServerPort;
	}

	/**
	 * Method 'setRdbmsServerPort'
	 * 
	 * @param rdbmsServerPort
	 */
	public void setRdbmsServerPort(String rdbmsServerPort)
	{
		this.rdbmsServerPort = rdbmsServerPort;
	}

	/**
	 * Method 'getDatasourceCdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceCdatetime()
	{
		return datasourceCdatetime;
	}

	/**
	 * Method 'setDatasourceCdatetime'
	 * 
	 * @param datasourceCdatetime
	 */
	public void setDatasourceCdatetime(String datasourceCdatetime)
	{
		this.datasourceCdatetime = datasourceCdatetime;
	}

	/**
	 * Method 'getDatasourceMdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceMdatetime()
	{
		return datasourceMdatetime;
	}

	/**
	 * Method 'setDatasourceMdatetime'
	 * 
	 * @param datasourceMdatetime
	 */
	public void setDatasourceMdatetime(String datasourceMdatetime)
	{
		this.datasourceMdatetime = datasourceMdatetime;
	}

	/**
	 * Method 'getDatasourceIsDeleted'
	 * 
	 * @return short
	 */
	public short getDatasourceIsDeleted()
	{
		return datasourceIsDeleted;
	}

	/**
	 * Method 'setDatasourceIsDeleted'
	 * 
	 * @param datasourceIsDeleted
	 */
	public void setDatasourceIsDeleted(short datasourceIsDeleted)
	{
		this.datasourceIsDeleted = datasourceIsDeleted;
		this.datasourceIsDeletedNull = false;
	}

	/**
	 * Method 'setDatasourceIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setDatasourceIsDeletedNull(boolean value)
	{
		this.datasourceIsDeletedNull = value;
	}

	/**
	 * Method 'isDatasourceIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isDatasourceIsDeletedNull()
	{
		return datasourceIsDeletedNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsViewReportTemplateDetails)) {
			return false;
		}
		
		final IprodReportsViewReportTemplateDetails _cast = (IprodReportsViewReportTemplateDetails) _other;
		if (rtemplateId != _cast.rtemplateId) {
			return false;
		}
		
		if (rtemplateName == null ? _cast.rtemplateName != rtemplateName : !rtemplateName.equals( _cast.rtemplateName )) {
			return false;
		}
		
		if (rtemplateTitle == null ? _cast.rtemplateTitle != rtemplateTitle : !rtemplateTitle.equals( _cast.rtemplateTitle )) {
			return false;
		}
		
		if (rtemplateSubTitle == null ? _cast.rtemplateSubTitle != rtemplateSubTitle : !rtemplateSubTitle.equals( _cast.rtemplateSubTitle )) {
			return false;
		}
		
		if (rtemplateSqlquery == null ? _cast.rtemplateSqlquery != rtemplateSqlquery : !rtemplateSqlquery.equals( _cast.rtemplateSqlquery )) {
			return false;
		}
		
		if (rtemplateExcelDataStorage == null ? _cast.rtemplateExcelDataStorage != rtemplateExcelDataStorage : !rtemplateExcelDataStorage.equals( _cast.rtemplateExcelDataStorage )) {
			return false;
		}
		
		if (rtemplatePdfDataStorage == null ? _cast.rtemplatePdfDataStorage != rtemplatePdfDataStorage : !rtemplatePdfDataStorage.equals( _cast.rtemplatePdfDataStorage )) {
			return false;
		}
		
		if (rtemplateBirtRpt == null ? _cast.rtemplateBirtRpt != rtemplateBirtRpt : !rtemplateBirtRpt.equals( _cast.rtemplateBirtRpt )) {
			return false;
		}
		
		if (rtemplateCdatetime == null ? _cast.rtemplateCdatetime != rtemplateCdatetime : !rtemplateCdatetime.equals( _cast.rtemplateCdatetime )) {
			return false;
		}
		
		if (rtemplateMdatetime == null ? _cast.rtemplateMdatetime != rtemplateMdatetime : !rtemplateMdatetime.equals( _cast.rtemplateMdatetime )) {
			return false;
		}
		
		if (rtemplateIsDeleted != _cast.rtemplateIsDeleted) {
			return false;
		}
		
		if (datasourceId != _cast.datasourceId) {
			return false;
		}
		
		if (datasourceName == null ? _cast.datasourceName != datasourceName : !datasourceName.equals( _cast.datasourceName )) {
			return false;
		}
		
		if (rdbmsId != _cast.rdbmsId) {
			return false;
		}
		
		if (rdbmsIdNull != _cast.rdbmsIdNull) {
			return false;
		}
		
		if (rdbmsName == null ? _cast.rdbmsName != rdbmsName : !rdbmsName.equals( _cast.rdbmsName )) {
			return false;
		}
		
		if (rdbmsSubVersionName == null ? _cast.rdbmsSubVersionName != rdbmsSubVersionName : !rdbmsSubVersionName.equals( _cast.rdbmsSubVersionName )) {
			return false;
		}
		
		if (rdbmsDesc == null ? _cast.rdbmsDesc != rdbmsDesc : !rdbmsDesc.equals( _cast.rdbmsDesc )) {
			return false;
		}
		
		if (rdbmsJdbcDriverClass == null ? _cast.rdbmsJdbcDriverClass != rdbmsJdbcDriverClass : !rdbmsJdbcDriverClass.equals( _cast.rdbmsJdbcDriverClass )) {
			return false;
		}
		
		if (rdbmsDefaultPort == null ? _cast.rdbmsDefaultPort != rdbmsDefaultPort : !rdbmsDefaultPort.equals( _cast.rdbmsDefaultPort )) {
			return false;
		}
		
		if (rdbmsCdatetime == null ? _cast.rdbmsCdatetime != rdbmsCdatetime : !rdbmsCdatetime.equals( _cast.rdbmsCdatetime )) {
			return false;
		}
		
		if (rdbmsMdatetime == null ? _cast.rdbmsMdatetime != rdbmsMdatetime : !rdbmsMdatetime.equals( _cast.rdbmsMdatetime )) {
			return false;
		}
		
		if (rdbmsIsDeleted != _cast.rdbmsIsDeleted) {
			return false;
		}
		
		if (rdbmsIsDeletedNull != _cast.rdbmsIsDeletedNull) {
			return false;
		}
		
		if (rdbmsJdbcDriverUrl == null ? _cast.rdbmsJdbcDriverUrl != rdbmsJdbcDriverUrl : !rdbmsJdbcDriverUrl.equals( _cast.rdbmsJdbcDriverUrl )) {
			return false;
		}
		
		if (rdbmsIsWindowsAuth != _cast.rdbmsIsWindowsAuth) {
			return false;
		}
		
		if (rdbmsIsWindowsAuthNull != _cast.rdbmsIsWindowsAuthNull) {
			return false;
		}
		
		if (rdbmsDatabaseName == null ? _cast.rdbmsDatabaseName != rdbmsDatabaseName : !rdbmsDatabaseName.equals( _cast.rdbmsDatabaseName )) {
			return false;
		}
		
		if (rdbmsInstanceName == null ? _cast.rdbmsInstanceName != rdbmsInstanceName : !rdbmsInstanceName.equals( _cast.rdbmsInstanceName )) {
			return false;
		}
		
		if (rdbmsServerUsername == null ? _cast.rdbmsServerUsername != rdbmsServerUsername : !rdbmsServerUsername.equals( _cast.rdbmsServerUsername )) {
			return false;
		}
		
		if (rdbmsServerPassword == null ? _cast.rdbmsServerPassword != rdbmsServerPassword : !rdbmsServerPassword.equals( _cast.rdbmsServerPassword )) {
			return false;
		}
		
		if (rdbmsServerMachineName == null ? _cast.rdbmsServerMachineName != rdbmsServerMachineName : !rdbmsServerMachineName.equals( _cast.rdbmsServerMachineName )) {
			return false;
		}
		
		if (rdbmsServerPort == null ? _cast.rdbmsServerPort != rdbmsServerPort : !rdbmsServerPort.equals( _cast.rdbmsServerPort )) {
			return false;
		}
		
		if (datasourceCdatetime == null ? _cast.datasourceCdatetime != datasourceCdatetime : !datasourceCdatetime.equals( _cast.datasourceCdatetime )) {
			return false;
		}
		
		if (datasourceMdatetime == null ? _cast.datasourceMdatetime != datasourceMdatetime : !datasourceMdatetime.equals( _cast.datasourceMdatetime )) {
			return false;
		}
		
		if (datasourceIsDeleted != _cast.datasourceIsDeleted) {
			return false;
		}
		
		if (datasourceIsDeletedNull != _cast.datasourceIsDeletedNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + rtemplateId;
		if (rtemplateName != null) {
			_hashCode = 29 * _hashCode + rtemplateName.hashCode();
		}
		
		if (rtemplateTitle != null) {
			_hashCode = 29 * _hashCode + rtemplateTitle.hashCode();
		}
		
		if (rtemplateSubTitle != null) {
			_hashCode = 29 * _hashCode + rtemplateSubTitle.hashCode();
		}
		
		if (rtemplateSqlquery != null) {
			_hashCode = 29 * _hashCode + rtemplateSqlquery.hashCode();
		}
		
		if (rtemplateExcelDataStorage != null) {
			_hashCode = 29 * _hashCode + rtemplateExcelDataStorage.hashCode();
		}
		
		if (rtemplatePdfDataStorage != null) {
			_hashCode = 29 * _hashCode + rtemplatePdfDataStorage.hashCode();
		}
		
		if (rtemplateBirtRpt != null) {
			_hashCode = 29 * _hashCode + rtemplateBirtRpt.hashCode();
		}
		
		if (rtemplateCdatetime != null) {
			_hashCode = 29 * _hashCode + rtemplateCdatetime.hashCode();
		}
		
		if (rtemplateMdatetime != null) {
			_hashCode = 29 * _hashCode + rtemplateMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) rtemplateIsDeleted;
		_hashCode = 29 * _hashCode + datasourceId;
		if (datasourceName != null) {
			_hashCode = 29 * _hashCode + datasourceName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + rdbmsId;
		_hashCode = 29 * _hashCode + (rdbmsIdNull ? 1 : 0);
		if (rdbmsName != null) {
			_hashCode = 29 * _hashCode + rdbmsName.hashCode();
		}
		
		if (rdbmsSubVersionName != null) {
			_hashCode = 29 * _hashCode + rdbmsSubVersionName.hashCode();
		}
		
		if (rdbmsDesc != null) {
			_hashCode = 29 * _hashCode + rdbmsDesc.hashCode();
		}
		
		if (rdbmsJdbcDriverClass != null) {
			_hashCode = 29 * _hashCode + rdbmsJdbcDriverClass.hashCode();
		}
		
		if (rdbmsDefaultPort != null) {
			_hashCode = 29 * _hashCode + rdbmsDefaultPort.hashCode();
		}
		
		if (rdbmsCdatetime != null) {
			_hashCode = 29 * _hashCode + rdbmsCdatetime.hashCode();
		}
		
		if (rdbmsMdatetime != null) {
			_hashCode = 29 * _hashCode + rdbmsMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) rdbmsIsDeleted;
		_hashCode = 29 * _hashCode + (rdbmsIsDeletedNull ? 1 : 0);
		if (rdbmsJdbcDriverUrl != null) {
			_hashCode = 29 * _hashCode + rdbmsJdbcDriverUrl.hashCode();
		}
		
		_hashCode = 29 * _hashCode + rdbmsIsWindowsAuth;
		_hashCode = 29 * _hashCode + (rdbmsIsWindowsAuthNull ? 1 : 0);
		if (rdbmsDatabaseName != null) {
			_hashCode = 29 * _hashCode + rdbmsDatabaseName.hashCode();
		}
		
		if (rdbmsInstanceName != null) {
			_hashCode = 29 * _hashCode + rdbmsInstanceName.hashCode();
		}
		
		if (rdbmsServerUsername != null) {
			_hashCode = 29 * _hashCode + rdbmsServerUsername.hashCode();
		}
		
		if (rdbmsServerPassword != null) {
			_hashCode = 29 * _hashCode + rdbmsServerPassword.hashCode();
		}
		
		if (rdbmsServerMachineName != null) {
			_hashCode = 29 * _hashCode + rdbmsServerMachineName.hashCode();
		}
		
		if (rdbmsServerPort != null) {
			_hashCode = 29 * _hashCode + rdbmsServerPort.hashCode();
		}
		
		if (datasourceCdatetime != null) {
			_hashCode = 29 * _hashCode + datasourceCdatetime.hashCode();
		}
		
		if (datasourceMdatetime != null) {
			_hashCode = 29 * _hashCode + datasourceMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) datasourceIsDeleted;
		_hashCode = 29 * _hashCode + (datasourceIsDeletedNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetails: " );
		ret.append( "rtemplateId=" + rtemplateId );
		ret.append( ", rtemplateName=" + rtemplateName );
		ret.append( ", rtemplateTitle=" + rtemplateTitle );
		ret.append( ", rtemplateSubTitle=" + rtemplateSubTitle );
		ret.append( ", rtemplateSqlquery=" + rtemplateSqlquery );
		ret.append( ", rtemplateExcelDataStorage=" + rtemplateExcelDataStorage );
		ret.append( ", rtemplatePdfDataStorage=" + rtemplatePdfDataStorage );
		ret.append( ", rtemplateBirtRpt=" + rtemplateBirtRpt );
		ret.append( ", rtemplateCdatetime=" + rtemplateCdatetime );
		ret.append( ", rtemplateMdatetime=" + rtemplateMdatetime );
		ret.append( ", rtemplateIsDeleted=" + rtemplateIsDeleted );
		ret.append( ", datasourceId=" + datasourceId );
		ret.append( ", datasourceName=" + datasourceName );
		ret.append( ", rdbmsId=" + rdbmsId );
		ret.append( ", rdbmsName=" + rdbmsName );
		ret.append( ", rdbmsSubVersionName=" + rdbmsSubVersionName );
		ret.append( ", rdbmsDesc=" + rdbmsDesc );
		ret.append( ", rdbmsJdbcDriverClass=" + rdbmsJdbcDriverClass );
		ret.append( ", rdbmsDefaultPort=" + rdbmsDefaultPort );
		ret.append( ", rdbmsCdatetime=" + rdbmsCdatetime );
		ret.append( ", rdbmsMdatetime=" + rdbmsMdatetime );
		ret.append( ", rdbmsIsDeleted=" + rdbmsIsDeleted );
		ret.append( ", rdbmsJdbcDriverUrl=" + rdbmsJdbcDriverUrl );
		ret.append( ", rdbmsIsWindowsAuth=" + rdbmsIsWindowsAuth );
		ret.append( ", rdbmsDatabaseName=" + rdbmsDatabaseName );
		ret.append( ", rdbmsInstanceName=" + rdbmsInstanceName );
		ret.append( ", rdbmsServerUsername=" + rdbmsServerUsername );
		ret.append( ", rdbmsServerPassword=" + rdbmsServerPassword );
		ret.append( ", rdbmsServerMachineName=" + rdbmsServerMachineName );
		ret.append( ", rdbmsServerPort=" + rdbmsServerPort );
		ret.append( ", datasourceCdatetime=" + datasourceCdatetime );
		ret.append( ", datasourceMdatetime=" + datasourceMdatetime );
		ret.append( ", datasourceIsDeleted=" + datasourceIsDeleted );
		return ret.toString();
	}

}
