package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_rdbms_master table.
 */
public class IprodReportsRdbmsMasterPk implements Serializable
{
	protected int rdbmsId;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsId is null.
	 */
	protected boolean rdbmsIdNull;

	/** 
	 * Sets the value of rdbmsId
	 */
	public void setRdbmsId(int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
	}

	/** 
	 * Gets the value of rdbmsId
	 */
	public int getRdbmsId()
	{
		return rdbmsId;
	}

	/**
	 * Method 'IprodReportsRdbmsMasterPk'
	 * 
	 */
	public IprodReportsRdbmsMasterPk()
	{
	}

	/**
	 * Method 'IprodReportsRdbmsMasterPk'
	 * 
	 * @param rdbmsId
	 */
	public IprodReportsRdbmsMasterPk(final int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
	}

	/** 
	 * Sets the value of rdbmsIdNull
	 */
	public void setRdbmsIdNull(boolean rdbmsIdNull)
	{
		this.rdbmsIdNull = rdbmsIdNull;
	}

	/** 
	 * Gets the value of rdbmsIdNull
	 */
	public boolean isRdbmsIdNull()
	{
		return rdbmsIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsRdbmsMasterPk)) {
			return false;
		}
		
		final IprodReportsRdbmsMasterPk _cast = (IprodReportsRdbmsMasterPk) _other;
		if (rdbmsId != _cast.rdbmsId) {
			return false;
		}
		
		if (rdbmsIdNull != _cast.rdbmsIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + rdbmsId;
		_hashCode = 29 * _hashCode + (rdbmsIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsRdbmsMasterPk: " );
		ret.append( "rdbmsId=" + rdbmsId );
		return ret.toString();
	}

}
