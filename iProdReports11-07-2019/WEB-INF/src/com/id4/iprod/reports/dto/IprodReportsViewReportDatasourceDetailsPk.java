package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_view_report_datasource_details table.
 */
public class IprodReportsViewReportDatasourceDetailsPk implements Serializable
{
	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsViewReportDatasourceDetailsPk)) {
			return false;
		}
		
		final IprodReportsViewReportDatasourceDetailsPk _cast = (IprodReportsViewReportDatasourceDetailsPk) _other;
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodReportsViewReportDatasourceDetailsPk: " );
		return ret.toString();
	}

}
