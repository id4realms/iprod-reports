package com.id4.iprod.reports.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsViewReportDatasourceDetails implements Serializable
{
	/** 
	 * This attribute maps to the column DATASOURCE_ID in the iprod_reports_view_report_datasource_details table.
	 */
	protected int datasourceId;

	/** 
	 * This attribute maps to the column DATASOURCE_NAME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String datasourceName;

	/** 
	 * This attribute maps to the column RDBMS_ID in the iprod_reports_view_report_datasource_details table.
	 */
	protected int rdbmsId;

	/** 
	 * This attribute maps to the column RDBMS_NAME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsName;

	/** 
	 * This attribute maps to the column RDBMS_SUB_VERSION_NAME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsSubVersionName;

	/** 
	 * This attribute maps to the column RDBMS_DESC in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsDesc;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_CLASS in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsJdbcDriverClass;

	/** 
	 * This attribute maps to the column RDBMS_DEFAULT_PORT in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsDefaultPort;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_URL in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsJdbcDriverUrl;

	/** 
	 * This attribute maps to the column RDBMS_IS_WINDOWS_AUTH in the iprod_reports_view_report_datasource_details table.
	 */
	protected int rdbmsIsWindowsAuth;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsIsWindowsAuth is null.
	 */
	protected boolean rdbmsIsWindowsAuthNull = true;

	/** 
	 * This attribute maps to the column RDBMS_DATABASE_NAME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsDatabaseName;

	/** 
	 * This attribute maps to the column RDBMS_INSTANCE_NAME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsInstanceName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_USERNAME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsServerUsername;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PASSWORD in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsServerPassword;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_MACHINE_NAME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsServerMachineName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PORT in the iprod_reports_view_report_datasource_details table.
	 */
	protected String rdbmsServerPort;

	/** 
	 * This attribute maps to the column DATASOURCE_CDATETIME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String datasourceCdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_MDATETIME in the iprod_reports_view_report_datasource_details table.
	 */
	protected String datasourceMdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_IS_DELETED in the iprod_reports_view_report_datasource_details table.
	 */
	protected short datasourceIsDeleted;

	/**
	 * Method 'IprodReportsViewReportDatasourceDetails'
	 * 
	 */
	public IprodReportsViewReportDatasourceDetails()
	{
	}

	/**
	 * Method 'getDatasourceId'
	 * 
	 * @return int
	 */
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'setDatasourceId'
	 * 
	 * @param datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
	}

	/**
	 * Method 'getDatasourceName'
	 * 
	 * @return String
	 */
	public String getDatasourceName()
	{
		return datasourceName;
	}

	/**
	 * Method 'setDatasourceName'
	 * 
	 * @param datasourceName
	 */
	public void setDatasourceName(String datasourceName)
	{
		this.datasourceName = datasourceName;
	}

	/**
	 * Method 'getRdbmsId'
	 * 
	 * @return int
	 */
	public int getRdbmsId()
	{
		return rdbmsId;
	}

	/**
	 * Method 'setRdbmsId'
	 * 
	 * @param rdbmsId
	 */
	public void setRdbmsId(int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
	}

	/**
	 * Method 'getRdbmsName'
	 * 
	 * @return String
	 */
	public String getRdbmsName()
	{
		return rdbmsName;
	}

	/**
	 * Method 'setRdbmsName'
	 * 
	 * @param rdbmsName
	 */
	public void setRdbmsName(String rdbmsName)
	{
		this.rdbmsName = rdbmsName;
	}

	/**
	 * Method 'getRdbmsSubVersionName'
	 * 
	 * @return String
	 */
	public String getRdbmsSubVersionName()
	{
		return rdbmsSubVersionName;
	}

	/**
	 * Method 'setRdbmsSubVersionName'
	 * 
	 * @param rdbmsSubVersionName
	 */
	public void setRdbmsSubVersionName(String rdbmsSubVersionName)
	{
		this.rdbmsSubVersionName = rdbmsSubVersionName;
	}

	/**
	 * Method 'getRdbmsDesc'
	 * 
	 * @return String
	 */
	public String getRdbmsDesc()
	{
		return rdbmsDesc;
	}

	/**
	 * Method 'setRdbmsDesc'
	 * 
	 * @param rdbmsDesc
	 */
	public void setRdbmsDesc(String rdbmsDesc)
	{
		this.rdbmsDesc = rdbmsDesc;
	}

	/**
	 * Method 'getRdbmsJdbcDriverClass'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverClass()
	{
		return rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'setRdbmsJdbcDriverClass'
	 * 
	 * @param rdbmsJdbcDriverClass
	 */
	public void setRdbmsJdbcDriverClass(String rdbmsJdbcDriverClass)
	{
		this.rdbmsJdbcDriverClass = rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'getRdbmsDefaultPort'
	 * 
	 * @return String
	 */
	public String getRdbmsDefaultPort()
	{
		return rdbmsDefaultPort;
	}

	/**
	 * Method 'setRdbmsDefaultPort'
	 * 
	 * @param rdbmsDefaultPort
	 */
	public void setRdbmsDefaultPort(String rdbmsDefaultPort)
	{
		this.rdbmsDefaultPort = rdbmsDefaultPort;
	}

	/**
	 * Method 'getRdbmsJdbcDriverUrl'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverUrl()
	{
		return rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'setRdbmsJdbcDriverUrl'
	 * 
	 * @param rdbmsJdbcDriverUrl
	 */
	public void setRdbmsJdbcDriverUrl(String rdbmsJdbcDriverUrl)
	{
		this.rdbmsJdbcDriverUrl = rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'getRdbmsIsWindowsAuth'
	 * 
	 * @return int
	 */
	public int getRdbmsIsWindowsAuth()
	{
		return rdbmsIsWindowsAuth;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuth'
	 * 
	 * @param rdbmsIsWindowsAuth
	 */
	public void setRdbmsIsWindowsAuth(int rdbmsIsWindowsAuth)
	{
		this.rdbmsIsWindowsAuth = rdbmsIsWindowsAuth;
		this.rdbmsIsWindowsAuthNull = false;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuthNull'
	 * 
	 * @param value
	 */
	public void setRdbmsIsWindowsAuthNull(boolean value)
	{
		this.rdbmsIsWindowsAuthNull = value;
	}

	/**
	 * Method 'isRdbmsIsWindowsAuthNull'
	 * 
	 * @return boolean
	 */
	public boolean isRdbmsIsWindowsAuthNull()
	{
		return rdbmsIsWindowsAuthNull;
	}

	/**
	 * Method 'getRdbmsDatabaseName'
	 * 
	 * @return String
	 */
	public String getRdbmsDatabaseName()
	{
		return rdbmsDatabaseName;
	}

	/**
	 * Method 'setRdbmsDatabaseName'
	 * 
	 * @param rdbmsDatabaseName
	 */
	public void setRdbmsDatabaseName(String rdbmsDatabaseName)
	{
		this.rdbmsDatabaseName = rdbmsDatabaseName;
	}

	/**
	 * Method 'getRdbmsInstanceName'
	 * 
	 * @return String
	 */
	public String getRdbmsInstanceName()
	{
		return rdbmsInstanceName;
	}

	/**
	 * Method 'setRdbmsInstanceName'
	 * 
	 * @param rdbmsInstanceName
	 */
	public void setRdbmsInstanceName(String rdbmsInstanceName)
	{
		this.rdbmsInstanceName = rdbmsInstanceName;
	}

	/**
	 * Method 'getRdbmsServerUsername'
	 * 
	 * @return String
	 */
	public String getRdbmsServerUsername()
	{
		return rdbmsServerUsername;
	}

	/**
	 * Method 'setRdbmsServerUsername'
	 * 
	 * @param rdbmsServerUsername
	 */
	public void setRdbmsServerUsername(String rdbmsServerUsername)
	{
		this.rdbmsServerUsername = rdbmsServerUsername;
	}

	/**
	 * Method 'getRdbmsServerPassword'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPassword()
	{
		return rdbmsServerPassword;
	}

	/**
	 * Method 'setRdbmsServerPassword'
	 * 
	 * @param rdbmsServerPassword
	 */
	public void setRdbmsServerPassword(String rdbmsServerPassword)
	{
		this.rdbmsServerPassword = rdbmsServerPassword;
	}

	/**
	 * Method 'getRdbmsServerMachineName'
	 * 
	 * @return String
	 */
	public String getRdbmsServerMachineName()
	{
		return rdbmsServerMachineName;
	}

	/**
	 * Method 'setRdbmsServerMachineName'
	 * 
	 * @param rdbmsServerMachineName
	 */
	public void setRdbmsServerMachineName(String rdbmsServerMachineName)
	{
		this.rdbmsServerMachineName = rdbmsServerMachineName;
	}

	/**
	 * Method 'getRdbmsServerPort'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPort()
	{
		return rdbmsServerPort;
	}

	/**
	 * Method 'setRdbmsServerPort'
	 * 
	 * @param rdbmsServerPort
	 */
	public void setRdbmsServerPort(String rdbmsServerPort)
	{
		this.rdbmsServerPort = rdbmsServerPort;
	}

	/**
	 * Method 'getDatasourceCdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceCdatetime()
	{
		return datasourceCdatetime;
	}

	/**
	 * Method 'setDatasourceCdatetime'
	 * 
	 * @param datasourceCdatetime
	 */
	public void setDatasourceCdatetime(String datasourceCdatetime)
	{
		this.datasourceCdatetime = datasourceCdatetime;
	}

	/**
	 * Method 'getDatasourceMdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceMdatetime()
	{
		return datasourceMdatetime;
	}

	/**
	 * Method 'setDatasourceMdatetime'
	 * 
	 * @param datasourceMdatetime
	 */
	public void setDatasourceMdatetime(String datasourceMdatetime)
	{
		this.datasourceMdatetime = datasourceMdatetime;
	}

	/**
	 * Method 'getDatasourceIsDeleted'
	 * 
	 * @return short
	 */
	public short getDatasourceIsDeleted()
	{
		return datasourceIsDeleted;
	}

	/**
	 * Method 'setDatasourceIsDeleted'
	 * 
	 * @param datasourceIsDeleted
	 */
	public void setDatasourceIsDeleted(short datasourceIsDeleted)
	{
		this.datasourceIsDeleted = datasourceIsDeleted;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsViewReportDatasourceDetails)) {
			return false;
		}
		
		final IprodReportsViewReportDatasourceDetails _cast = (IprodReportsViewReportDatasourceDetails) _other;
		if (datasourceId != _cast.datasourceId) {
			return false;
		}
		
		if (datasourceName == null ? _cast.datasourceName != datasourceName : !datasourceName.equals( _cast.datasourceName )) {
			return false;
		}
		
		if (rdbmsId != _cast.rdbmsId) {
			return false;
		}
		
		if (rdbmsName == null ? _cast.rdbmsName != rdbmsName : !rdbmsName.equals( _cast.rdbmsName )) {
			return false;
		}
		
		if (rdbmsSubVersionName == null ? _cast.rdbmsSubVersionName != rdbmsSubVersionName : !rdbmsSubVersionName.equals( _cast.rdbmsSubVersionName )) {
			return false;
		}
		
		if (rdbmsDesc == null ? _cast.rdbmsDesc != rdbmsDesc : !rdbmsDesc.equals( _cast.rdbmsDesc )) {
			return false;
		}
		
		if (rdbmsJdbcDriverClass == null ? _cast.rdbmsJdbcDriverClass != rdbmsJdbcDriverClass : !rdbmsJdbcDriverClass.equals( _cast.rdbmsJdbcDriverClass )) {
			return false;
		}
		
		if (rdbmsDefaultPort == null ? _cast.rdbmsDefaultPort != rdbmsDefaultPort : !rdbmsDefaultPort.equals( _cast.rdbmsDefaultPort )) {
			return false;
		}
		
		if (rdbmsJdbcDriverUrl == null ? _cast.rdbmsJdbcDriverUrl != rdbmsJdbcDriverUrl : !rdbmsJdbcDriverUrl.equals( _cast.rdbmsJdbcDriverUrl )) {
			return false;
		}
		
		if (rdbmsIsWindowsAuth != _cast.rdbmsIsWindowsAuth) {
			return false;
		}
		
		if (rdbmsIsWindowsAuthNull != _cast.rdbmsIsWindowsAuthNull) {
			return false;
		}
		
		if (rdbmsDatabaseName == null ? _cast.rdbmsDatabaseName != rdbmsDatabaseName : !rdbmsDatabaseName.equals( _cast.rdbmsDatabaseName )) {
			return false;
		}
		
		if (rdbmsInstanceName == null ? _cast.rdbmsInstanceName != rdbmsInstanceName : !rdbmsInstanceName.equals( _cast.rdbmsInstanceName )) {
			return false;
		}
		
		if (rdbmsServerUsername == null ? _cast.rdbmsServerUsername != rdbmsServerUsername : !rdbmsServerUsername.equals( _cast.rdbmsServerUsername )) {
			return false;
		}
		
		if (rdbmsServerPassword == null ? _cast.rdbmsServerPassword != rdbmsServerPassword : !rdbmsServerPassword.equals( _cast.rdbmsServerPassword )) {
			return false;
		}
		
		if (rdbmsServerMachineName == null ? _cast.rdbmsServerMachineName != rdbmsServerMachineName : !rdbmsServerMachineName.equals( _cast.rdbmsServerMachineName )) {
			return false;
		}
		
		if (rdbmsServerPort == null ? _cast.rdbmsServerPort != rdbmsServerPort : !rdbmsServerPort.equals( _cast.rdbmsServerPort )) {
			return false;
		}
		
		if (datasourceCdatetime == null ? _cast.datasourceCdatetime != datasourceCdatetime : !datasourceCdatetime.equals( _cast.datasourceCdatetime )) {
			return false;
		}
		
		if (datasourceMdatetime == null ? _cast.datasourceMdatetime != datasourceMdatetime : !datasourceMdatetime.equals( _cast.datasourceMdatetime )) {
			return false;
		}
		
		if (datasourceIsDeleted != _cast.datasourceIsDeleted) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + datasourceId;
		if (datasourceName != null) {
			_hashCode = 29 * _hashCode + datasourceName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + rdbmsId;
		if (rdbmsName != null) {
			_hashCode = 29 * _hashCode + rdbmsName.hashCode();
		}
		
		if (rdbmsSubVersionName != null) {
			_hashCode = 29 * _hashCode + rdbmsSubVersionName.hashCode();
		}
		
		if (rdbmsDesc != null) {
			_hashCode = 29 * _hashCode + rdbmsDesc.hashCode();
		}
		
		if (rdbmsJdbcDriverClass != null) {
			_hashCode = 29 * _hashCode + rdbmsJdbcDriverClass.hashCode();
		}
		
		if (rdbmsDefaultPort != null) {
			_hashCode = 29 * _hashCode + rdbmsDefaultPort.hashCode();
		}
		
		if (rdbmsJdbcDriverUrl != null) {
			_hashCode = 29 * _hashCode + rdbmsJdbcDriverUrl.hashCode();
		}
		
		_hashCode = 29 * _hashCode + rdbmsIsWindowsAuth;
		_hashCode = 29 * _hashCode + (rdbmsIsWindowsAuthNull ? 1 : 0);
		if (rdbmsDatabaseName != null) {
			_hashCode = 29 * _hashCode + rdbmsDatabaseName.hashCode();
		}
		
		if (rdbmsInstanceName != null) {
			_hashCode = 29 * _hashCode + rdbmsInstanceName.hashCode();
		}
		
		if (rdbmsServerUsername != null) {
			_hashCode = 29 * _hashCode + rdbmsServerUsername.hashCode();
		}
		
		if (rdbmsServerPassword != null) {
			_hashCode = 29 * _hashCode + rdbmsServerPassword.hashCode();
		}
		
		if (rdbmsServerMachineName != null) {
			_hashCode = 29 * _hashCode + rdbmsServerMachineName.hashCode();
		}
		
		if (rdbmsServerPort != null) {
			_hashCode = 29 * _hashCode + rdbmsServerPort.hashCode();
		}
		
		if (datasourceCdatetime != null) {
			_hashCode = 29 * _hashCode + datasourceCdatetime.hashCode();
		}
		
		if (datasourceMdatetime != null) {
			_hashCode = 29 * _hashCode + datasourceMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) datasourceIsDeleted;
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodReportsViewReportDatasourceDetails: " );
		ret.append( "datasourceId=" + datasourceId );
		ret.append( ", datasourceName=" + datasourceName );
		ret.append( ", rdbmsId=" + rdbmsId );
		ret.append( ", rdbmsName=" + rdbmsName );
		ret.append( ", rdbmsSubVersionName=" + rdbmsSubVersionName );
		ret.append( ", rdbmsDesc=" + rdbmsDesc );
		ret.append( ", rdbmsJdbcDriverClass=" + rdbmsJdbcDriverClass );
		ret.append( ", rdbmsDefaultPort=" + rdbmsDefaultPort );
		ret.append( ", rdbmsJdbcDriverUrl=" + rdbmsJdbcDriverUrl );
		ret.append( ", rdbmsIsWindowsAuth=" + rdbmsIsWindowsAuth );
		ret.append( ", rdbmsDatabaseName=" + rdbmsDatabaseName );
		ret.append( ", rdbmsInstanceName=" + rdbmsInstanceName );
		ret.append( ", rdbmsServerUsername=" + rdbmsServerUsername );
		ret.append( ", rdbmsServerPassword=" + rdbmsServerPassword );
		ret.append( ", rdbmsServerMachineName=" + rdbmsServerMachineName );
		ret.append( ", rdbmsServerPort=" + rdbmsServerPort );
		ret.append( ", datasourceCdatetime=" + datasourceCdatetime );
		ret.append( ", datasourceMdatetime=" + datasourceMdatetime );
		ret.append( ", datasourceIsDeleted=" + datasourceIsDeleted );
		return ret.toString();
	}

}
