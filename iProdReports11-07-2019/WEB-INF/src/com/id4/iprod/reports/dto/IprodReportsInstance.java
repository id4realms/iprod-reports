package com.id4.iprod.reports.dto;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsInstance implements Serializable
{
	/** 
	 * This attribute maps to the column REPORT_ID in the iprod_reports_instance table.
	 */
	protected int reportId;

	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprod_reports_instance table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column USER_ID in the iprod_reports_instance table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column USER_NAME in the iprod_reports_instance table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column REPORT_QUERY in the iprod_reports_instance table.
	 */
	protected String reportQuery;

	/** 
	 * This attribute maps to the column REPORT_EXCEL_LOCATION in the iprod_reports_instance table.
	 */
	protected String reportExcelLocation;

	/** 
	 * This attribute maps to the column REPORT_PDF_LOCATION in the iprod_reports_instance table.
	 */
	protected String reportPdfLocation;

	/** 
	 * This attribute maps to the column REPORT_CDATETIME in the iprod_reports_instance table.
	 */
	protected String reportCdatetime;

	/** 
	 * This attribute maps to the column REPORT_MDATETIME in the iprod_reports_instance table.
	 */
	protected String reportMdatetime;

	/** 
	 * This attribute maps to the column IS_SCHEDULED in the iprod_reports_instance table.
	 */
	protected int isScheduled;

	/** 
	 * This attribute represents whether the primitive attribute isScheduled is null.
	 */
	protected boolean isScheduledNull = true;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_reports_instance table.
	 */
	protected int isDeleted;

	/** 
	 * This attribute represents whether the primitive attribute isDeleted is null.
	 */
	protected boolean isDeletedNull = true;

	/**
	 * Method 'IprodReportsInstance'
	 * 
	 */
	public IprodReportsInstance()
	{
	}

	/**
	 * Method 'getReportId'
	 * 
	 * @return int
	 */
	public int getReportId()
	{
		return reportId;
	}

	/**
	 * Method 'setReportId'
	 * 
	 * @param reportId
	 */
	public void setReportId(int reportId)
	{
		this.reportId = reportId;
	}

	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getUserName'
	 * 
	 * @return String
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * Method 'setUserName'
	 * 
	 * @param userName
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * Method 'getReportQuery'
	 * 
	 * @return String
	 */
	public String getReportQuery()
	{
		return reportQuery;
	}

	/**
	 * Method 'setReportQuery'
	 * 
	 * @param reportQuery
	 */
	public void setReportQuery(String reportQuery)
	{
		this.reportQuery = reportQuery;
	}

	/**
	 * Method 'getReportExcelLocation'
	 * 
	 * @return String
	 */
	public String getReportExcelLocation()
	{
		return reportExcelLocation;
	}

	/**
	 * Method 'setReportExcelLocation'
	 * 
	 * @param reportExcelLocation
	 */
	public void setReportExcelLocation(String reportExcelLocation)
	{
		this.reportExcelLocation = reportExcelLocation;
	}

	/**
	 * Method 'getReportPdfLocation'
	 * 
	 * @return String
	 */
	public String getReportPdfLocation()
	{
		return reportPdfLocation;
	}

	/**
	 * Method 'setReportPdfLocation'
	 * 
	 * @param reportPdfLocation
	 */
	public void setReportPdfLocation(String reportPdfLocation)
	{
		this.reportPdfLocation = reportPdfLocation;
	}

	/**
	 * Method 'getReportCdatetime'
	 * 
	 * @return String
	 */
	public String getReportCdatetime()
	{
		return reportCdatetime;
	}

	/**
	 * Method 'setReportCdatetime'
	 * 
	 * @param reportCdatetime
	 */
	public void setReportCdatetime(String reportCdatetime)
	{
		this.reportCdatetime = reportCdatetime;
	}

	/**
	 * Method 'getReportMdatetime'
	 * 
	 * @return String
	 */
	public String getReportMdatetime()
	{
		return reportMdatetime;
	}

	/**
	 * Method 'setReportMdatetime'
	 * 
	 * @param reportMdatetime
	 */
	public void setReportMdatetime(String reportMdatetime)
	{
		this.reportMdatetime = reportMdatetime;
	}

	/**
	 * Method 'getIsScheduled'
	 * 
	 * @return int
	 */
	public int getIsScheduled()
	{
		return isScheduled;
	}

	/**
	 * Method 'setIsScheduled'
	 * 
	 * @param isScheduled
	 */
	public void setIsScheduled(int isScheduled)
	{
		this.isScheduled = isScheduled;
		this.isScheduledNull = false;
	}

	/**
	 * Method 'setIsScheduledNull'
	 * 
	 * @param value
	 */
	public void setIsScheduledNull(boolean value)
	{
		this.isScheduledNull = value;
	}

	/**
	 * Method 'isIsScheduledNull'
	 * 
	 * @return boolean
	 */
	public boolean isIsScheduledNull()
	{
		return isScheduledNull;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return int
	 */
	public int getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(int isDeleted)
	{
		this.isDeleted = isDeleted;
		this.isDeletedNull = false;
	}

	/**
	 * Method 'setIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setIsDeletedNull(boolean value)
	{
		this.isDeletedNull = value;
	}

	/**
	 * Method 'isIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isIsDeletedNull()
	{
		return isDeletedNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsInstance)) {
			return false;
		}
		
		final IprodReportsInstance _cast = (IprodReportsInstance) _other;
		if (reportId != _cast.reportId) {
			return false;
		}
		
		if (rtemplateId != _cast.rtemplateId) {
			return false;
		}
		
		if (userId != _cast.userId) {
			return false;
		}
		
		if (userName == null ? _cast.userName != userName : !userName.equals( _cast.userName )) {
			return false;
		}
		
		if (reportQuery == null ? _cast.reportQuery != reportQuery : !reportQuery.equals( _cast.reportQuery )) {
			return false;
		}
		
		if (reportExcelLocation == null ? _cast.reportExcelLocation != reportExcelLocation : !reportExcelLocation.equals( _cast.reportExcelLocation )) {
			return false;
		}
		
		if (reportPdfLocation == null ? _cast.reportPdfLocation != reportPdfLocation : !reportPdfLocation.equals( _cast.reportPdfLocation )) {
			return false;
		}
		
		if (reportCdatetime == null ? _cast.reportCdatetime != reportCdatetime : !reportCdatetime.equals( _cast.reportCdatetime )) {
			return false;
		}
		
		if (reportMdatetime == null ? _cast.reportMdatetime != reportMdatetime : !reportMdatetime.equals( _cast.reportMdatetime )) {
			return false;
		}
		
		if (isScheduled != _cast.isScheduled) {
			return false;
		}
		
		if (isScheduledNull != _cast.isScheduledNull) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		if (isDeletedNull != _cast.isDeletedNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + reportId;
		_hashCode = 29 * _hashCode + rtemplateId;
		_hashCode = 29 * _hashCode + userId;
		if (userName != null) {
			_hashCode = 29 * _hashCode + userName.hashCode();
		}
		
		if (reportQuery != null) {
			_hashCode = 29 * _hashCode + reportQuery.hashCode();
		}
		
		if (reportExcelLocation != null) {
			_hashCode = 29 * _hashCode + reportExcelLocation.hashCode();
		}
		
		if (reportPdfLocation != null) {
			_hashCode = 29 * _hashCode + reportPdfLocation.hashCode();
		}
		
		if (reportCdatetime != null) {
			_hashCode = 29 * _hashCode + reportCdatetime.hashCode();
		}
		
		if (reportMdatetime != null) {
			_hashCode = 29 * _hashCode + reportMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + isScheduled;
		_hashCode = 29 * _hashCode + (isScheduledNull ? 1 : 0);
		_hashCode = 29 * _hashCode + isDeleted;
		_hashCode = 29 * _hashCode + (isDeletedNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodReportsInstancePk
	 */
	public IprodReportsInstancePk createPk()
	{
		return new IprodReportsInstancePk(reportId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsInstance: " );
		ret.append( "reportId=" + reportId );
		ret.append( ", rtemplateId=" + rtemplateId );
		ret.append( ", userId=" + userId );
		ret.append( ", userName=" + userName );
		ret.append( ", reportQuery=" + reportQuery );
		ret.append( ", reportExcelLocation=" + reportExcelLocation );
		ret.append( ", reportPdfLocation=" + reportPdfLocation );
		ret.append( ", reportCdatetime=" + reportCdatetime );
		ret.append( ", reportMdatetime=" + reportMdatetime );
		ret.append( ", isScheduled=" + isScheduled );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

}
