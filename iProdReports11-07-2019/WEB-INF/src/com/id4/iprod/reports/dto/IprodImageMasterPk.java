package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_image_master table.
 */
public class IprodImageMasterPk implements Serializable
{
	protected int imageId;

	/** 
	 * This attribute represents whether the primitive attribute imageId is null.
	 */
	protected boolean imageIdNull;

	/** 
	 * Sets the value of imageId
	 */
	public void setImageId(int imageId)
	{
		this.imageId = imageId;
	}

	/** 
	 * Gets the value of imageId
	 */
	public int getImageId()
	{
		return imageId;
	}

	/**
	 * Method 'IprodImageMasterPk'
	 * 
	 */
	public IprodImageMasterPk()
	{
	}

	/**
	 * Method 'IprodImageMasterPk'
	 * 
	 * @param imageId
	 */
	public IprodImageMasterPk(final int imageId)
	{
		this.imageId = imageId;
	}

	/** 
	 * Sets the value of imageIdNull
	 */
	public void setImageIdNull(boolean imageIdNull)
	{
		this.imageIdNull = imageIdNull;
	}

	/** 
	 * Gets the value of imageIdNull
	 */
	public boolean isImageIdNull()
	{
		return imageIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodImageMasterPk)) {
			return false;
		}
		
		final IprodImageMasterPk _cast = (IprodImageMasterPk) _other;
		if (imageId != _cast.imageId) {
			return false;
		}
		
		if (imageIdNull != _cast.imageIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + imageId;
		_hashCode = 29 * _hashCode + (imageIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodImageMasterPk: " );
		ret.append( "imageId=" + imageId );
		return ret.toString();
	}

}
