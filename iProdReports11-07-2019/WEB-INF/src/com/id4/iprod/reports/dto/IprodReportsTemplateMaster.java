package com.id4.iprod.reports.dto;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsTemplateMaster implements Serializable
{
	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprod_reports_template_master table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column RTEMPLATE_NAME in the iprod_reports_template_master table.
	 */
	protected String rtemplateName;

	/** 
	 * This attribute maps to the column RTEMPLATE_TITLE in the iprod_reports_template_master table.
	 */
	protected String rtemplateTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SUB_TITLE in the iprod_reports_template_master table.
	 */
	protected String rtemplateSubTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprod_reports_template_master table.
	 */
	protected String rtemplateSqlquery;

	/** 
	 * This attribute maps to the column RTEMPLATE_EXCEL_DATA_STORAGE in the iprod_reports_template_master table.
	 */
	protected String rtemplateExcelDataStorage;

	/** 
	 * This attribute maps to the column RTEMPLATE_PDF_DATA_STORAGE in the iprod_reports_template_master table.
	 */
	protected String rtemplatePdfDataStorage;

	/** 
	 * This attribute maps to the column RTEMPLATE_BIRT_RPT in the iprod_reports_template_master table.
	 */
	protected String rtemplateBirtRpt;

	/** 
	 * This attribute maps to the column DATASOURCE_ID in the iprod_reports_template_master table.
	 */
	protected int datasourceId;

	/** 
	 * This attribute maps to the column RTEMPLATE_CDATETIME in the iprod_reports_template_master table.
	 */
	protected String rtemplateCdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_MDATETIME in the iprod_reports_template_master table.
	 */
	protected String rtemplateMdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_IS_DELETED in the iprod_reports_template_master table.
	 */
	protected short rtemplateIsDeleted;

	/**
	 * Method 'IprodReportsTemplateMaster'
	 * 
	 */
	public IprodReportsTemplateMaster()
	{
	}

	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getRtemplateName'
	 * 
	 * @return String
	 */
	public String getRtemplateName()
	{
		return rtemplateName;
	}

	/**
	 * Method 'setRtemplateName'
	 * 
	 * @param rtemplateName
	 */
	public void setRtemplateName(String rtemplateName)
	{
		this.rtemplateName = rtemplateName;
	}

	/**
	 * Method 'getRtemplateTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateTitle()
	{
		return rtemplateTitle;
	}

	/**
	 * Method 'setRtemplateTitle'
	 * 
	 * @param rtemplateTitle
	 */
	public void setRtemplateTitle(String rtemplateTitle)
	{
		this.rtemplateTitle = rtemplateTitle;
	}

	/**
	 * Method 'getRtemplateSubTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateSubTitle()
	{
		return rtemplateSubTitle;
	}

	/**
	 * Method 'setRtemplateSubTitle'
	 * 
	 * @param rtemplateSubTitle
	 */
	public void setRtemplateSubTitle(String rtemplateSubTitle)
	{
		this.rtemplateSubTitle = rtemplateSubTitle;
	}

	/**
	 * Method 'getRtemplateSqlquery'
	 * 
	 * @return String
	 */
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

	/**
	 * Method 'setRtemplateSqlquery'
	 * 
	 * @param rtemplateSqlquery
	 */
	public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	/**
	 * Method 'getRtemplateExcelDataStorage'
	 * 
	 * @return String
	 */
	public String getRtemplateExcelDataStorage()
	{
		return rtemplateExcelDataStorage;
	}

	/**
	 * Method 'setRtemplateExcelDataStorage'
	 * 
	 * @param rtemplateExcelDataStorage
	 */
	public void setRtemplateExcelDataStorage(String rtemplateExcelDataStorage)
	{
		this.rtemplateExcelDataStorage = rtemplateExcelDataStorage;
	}

	/**
	 * Method 'getRtemplatePdfDataStorage'
	 * 
	 * @return String
	 */
	public String getRtemplatePdfDataStorage()
	{
		return rtemplatePdfDataStorage;
	}

	/**
	 * Method 'setRtemplatePdfDataStorage'
	 * 
	 * @param rtemplatePdfDataStorage
	 */
	public void setRtemplatePdfDataStorage(String rtemplatePdfDataStorage)
	{
		this.rtemplatePdfDataStorage = rtemplatePdfDataStorage;
	}

	/**
	 * Method 'getRtemplateBirtRpt'
	 * 
	 * @return String
	 */
	public String getRtemplateBirtRpt()
	{
		return rtemplateBirtRpt;
	}

	/**
	 * Method 'setRtemplateBirtRpt'
	 * 
	 * @param rtemplateBirtRpt
	 */
	public void setRtemplateBirtRpt(String rtemplateBirtRpt)
	{
		this.rtemplateBirtRpt = rtemplateBirtRpt;
	}

	/**
	 * Method 'getDatasourceId'
	 * 
	 * @return int
	 */
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'setDatasourceId'
	 * 
	 * @param datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
	}

	/**
	 * Method 'getRtemplateCdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateCdatetime()
	{
		return rtemplateCdatetime;
	}

	/**
	 * Method 'setRtemplateCdatetime'
	 * 
	 * @param rtemplateCdatetime
	 */
	public void setRtemplateCdatetime(String rtemplateCdatetime)
	{
		this.rtemplateCdatetime = rtemplateCdatetime;
	}

	/**
	 * Method 'getRtemplateMdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateMdatetime()
	{
		return rtemplateMdatetime;
	}

	/**
	 * Method 'setRtemplateMdatetime'
	 * 
	 * @param rtemplateMdatetime
	 */
	public void setRtemplateMdatetime(String rtemplateMdatetime)
	{
		this.rtemplateMdatetime = rtemplateMdatetime;
	}

	/**
	 * Method 'getRtemplateIsDeleted'
	 * 
	 * @return short
	 */
	public short getRtemplateIsDeleted()
	{
		return rtemplateIsDeleted;
	}

	/**
	 * Method 'setRtemplateIsDeleted'
	 * 
	 * @param rtemplateIsDeleted
	 */
	public void setRtemplateIsDeleted(short rtemplateIsDeleted)
	{
		this.rtemplateIsDeleted = rtemplateIsDeleted;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsTemplateMaster)) {
			return false;
		}
		
		final IprodReportsTemplateMaster _cast = (IprodReportsTemplateMaster) _other;
		if (rtemplateId != _cast.rtemplateId) {
			return false;
		}
		
		if (rtemplateName == null ? _cast.rtemplateName != rtemplateName : !rtemplateName.equals( _cast.rtemplateName )) {
			return false;
		}
		
		if (rtemplateTitle == null ? _cast.rtemplateTitle != rtemplateTitle : !rtemplateTitle.equals( _cast.rtemplateTitle )) {
			return false;
		}
		
		if (rtemplateSubTitle == null ? _cast.rtemplateSubTitle != rtemplateSubTitle : !rtemplateSubTitle.equals( _cast.rtemplateSubTitle )) {
			return false;
		}
		
		if (rtemplateSqlquery == null ? _cast.rtemplateSqlquery != rtemplateSqlquery : !rtemplateSqlquery.equals( _cast.rtemplateSqlquery )) {
			return false;
		}
		
		if (rtemplateExcelDataStorage == null ? _cast.rtemplateExcelDataStorage != rtemplateExcelDataStorage : !rtemplateExcelDataStorage.equals( _cast.rtemplateExcelDataStorage )) {
			return false;
		}
		
		if (rtemplatePdfDataStorage == null ? _cast.rtemplatePdfDataStorage != rtemplatePdfDataStorage : !rtemplatePdfDataStorage.equals( _cast.rtemplatePdfDataStorage )) {
			return false;
		}
		
		if (rtemplateBirtRpt == null ? _cast.rtemplateBirtRpt != rtemplateBirtRpt : !rtemplateBirtRpt.equals( _cast.rtemplateBirtRpt )) {
			return false;
		}
		
		if (datasourceId != _cast.datasourceId) {
			return false;
		}
		
		if (rtemplateCdatetime == null ? _cast.rtemplateCdatetime != rtemplateCdatetime : !rtemplateCdatetime.equals( _cast.rtemplateCdatetime )) {
			return false;
		}
		
		if (rtemplateMdatetime == null ? _cast.rtemplateMdatetime != rtemplateMdatetime : !rtemplateMdatetime.equals( _cast.rtemplateMdatetime )) {
			return false;
		}
		
		if (rtemplateIsDeleted != _cast.rtemplateIsDeleted) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + rtemplateId;
		if (rtemplateName != null) {
			_hashCode = 29 * _hashCode + rtemplateName.hashCode();
		}
		
		if (rtemplateTitle != null) {
			_hashCode = 29 * _hashCode + rtemplateTitle.hashCode();
		}
		
		if (rtemplateSubTitle != null) {
			_hashCode = 29 * _hashCode + rtemplateSubTitle.hashCode();
		}
		
		if (rtemplateSqlquery != null) {
			_hashCode = 29 * _hashCode + rtemplateSqlquery.hashCode();
		}
		
		if (rtemplateExcelDataStorage != null) {
			_hashCode = 29 * _hashCode + rtemplateExcelDataStorage.hashCode();
		}
		
		if (rtemplatePdfDataStorage != null) {
			_hashCode = 29 * _hashCode + rtemplatePdfDataStorage.hashCode();
		}
		
		if (rtemplateBirtRpt != null) {
			_hashCode = 29 * _hashCode + rtemplateBirtRpt.hashCode();
		}
		
		_hashCode = 29 * _hashCode + datasourceId;
		if (rtemplateCdatetime != null) {
			_hashCode = 29 * _hashCode + rtemplateCdatetime.hashCode();
		}
		
		if (rtemplateMdatetime != null) {
			_hashCode = 29 * _hashCode + rtemplateMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) rtemplateIsDeleted;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodReportsTemplateMasterPk
	 */
	public IprodReportsTemplateMasterPk createPk()
	{
		return new IprodReportsTemplateMasterPk(rtemplateId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsTemplateMaster: " );
		ret.append( "rtemplateId=" + rtemplateId );
		ret.append( ", rtemplateName=" + rtemplateName );
		ret.append( ", rtemplateTitle=" + rtemplateTitle );
		ret.append( ", rtemplateSubTitle=" + rtemplateSubTitle );
		ret.append( ", rtemplateSqlquery=" + rtemplateSqlquery );
		ret.append( ", rtemplateExcelDataStorage=" + rtemplateExcelDataStorage );
		ret.append( ", rtemplatePdfDataStorage=" + rtemplatePdfDataStorage );
		ret.append( ", rtemplateBirtRpt=" + rtemplateBirtRpt );
		ret.append( ", datasourceId=" + datasourceId );
		ret.append( ", rtemplateCdatetime=" + rtemplateCdatetime );
		ret.append( ", rtemplateMdatetime=" + rtemplateMdatetime );
		ret.append( ", rtemplateIsDeleted=" + rtemplateIsDeleted );
		return ret.toString();
	}

}
