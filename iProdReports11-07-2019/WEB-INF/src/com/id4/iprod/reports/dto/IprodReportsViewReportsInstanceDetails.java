package com.id4.iprod.reports.dto;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsViewReportsInstanceDetails implements Serializable
{
	/** 
	 * This attribute maps to the column REPORT_ID in the iprod_reports_view_reports_instance_details table.
	 */
	protected int reportId;

	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprod_reports_view_reports_instance_details table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column RTEMPLATE_NAME in the iprod_reports_view_reports_instance_details table.
	 */
	protected String rtemplateName;

	/** 
	 * This attribute maps to the column DATASOURCE_ID in the iprod_reports_view_reports_instance_details table.
	 */
	protected int datasourceId;

	/** 
	 * This attribute represents whether the primitive attribute datasourceId is null.
	 */
	protected boolean datasourceIdNull = true;

	/** 
	 * This attribute maps to the column DATASOURCE_NAME in the iprod_reports_view_reports_instance_details table.
	 */
	protected String datasourceName;

	/** 
	 * This attribute maps to the column RDBMS_ID in the iprod_reports_view_reports_instance_details table.
	 */
	protected int rdbmsId;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsId is null.
	 */
	protected boolean rdbmsIdNull = true;

	/** 
	 * This attribute maps to the column RDBMS_NAME in the iprod_reports_view_reports_instance_details table.
	 */
	protected String rdbmsName;

	/** 
	 * This attribute maps to the column RDBMS_DATABASE_NAME in the iprod_reports_view_reports_instance_details table.
	 */
	protected String rdbmsDatabaseName;

	/** 
	 * This attribute maps to the column USER_ID in the iprod_reports_view_reports_instance_details table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column USER_NAME in the iprod_reports_view_reports_instance_details table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column REPORT_QUERY in the iprod_reports_view_reports_instance_details table.
	 */
	protected String reportQuery;

	/** 
	 * This attribute maps to the column REPORT_EXCEL_LOCATION in the iprod_reports_view_reports_instance_details table.
	 */
	protected String reportExcelLocation;

	/** 
	 * This attribute maps to the column REPORT_PDF_LOCATION in the iprod_reports_view_reports_instance_details table.
	 */
	protected String reportPdfLocation;

	/** 
	 * This attribute maps to the column REPORT_CDATETIME in the iprod_reports_view_reports_instance_details table.
	 */
	protected String reportCdatetime;

	/** 
	 * This attribute maps to the column REPORT_MDATETIME in the iprod_reports_view_reports_instance_details table.
	 */
	protected String reportMdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprod_reports_view_reports_instance_details table.
	 */
	protected String rtemplateSqlquery;

	/** 
	 * This attribute maps to the column RTEMPLATE_BIRT_RPT in the iprod_reports_view_reports_instance_details table.
	 */
	protected String rtemplateBirtRpt;

	/** 
	 * This attribute maps to the column RTEMPLATE_TITLE in the iprod_reports_view_reports_instance_details table.
	 */
	protected String rtemplateTitle;

	/** 
	 * This attribute maps to the column IS_SCHEDULED in the iprod_reports_view_reports_instance_details table.
	 */
	protected int isScheduled;

	/** 
	 * This attribute represents whether the primitive attribute isScheduled is null.
	 */
	protected boolean isScheduledNull = true;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_reports_view_reports_instance_details table.
	 */
	protected int isDeleted;

	/** 
	 * This attribute represents whether the primitive attribute isDeleted is null.
	 */
	protected boolean isDeletedNull = true;

	/**
	 * Method 'IprodReportsViewReportsInstanceDetails'
	 * 
	 */
	public IprodReportsViewReportsInstanceDetails()
	{
	}

	/**
	 * Method 'getReportId'
	 * 
	 * @return int
	 */
	public int getReportId()
	{
		return reportId;
	}

	/**
	 * Method 'setReportId'
	 * 
	 * @param reportId
	 */
	public void setReportId(int reportId)
	{
		this.reportId = reportId;
	}

	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getRtemplateName'
	 * 
	 * @return String
	 */
	public String getRtemplateName()
	{
		return rtemplateName;
	}

	/**
	 * Method 'setRtemplateName'
	 * 
	 * @param rtemplateName
	 */
	public void setRtemplateName(String rtemplateName)
	{
		this.rtemplateName = rtemplateName;
	}

	/**
	 * Method 'getDatasourceId'
	 * 
	 * @return int
	 */
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'setDatasourceId'
	 * 
	 * @param datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
		this.datasourceIdNull = false;
	}

	/**
	 * Method 'setDatasourceIdNull'
	 * 
	 * @param value
	 */
	public void setDatasourceIdNull(boolean value)
	{
		this.datasourceIdNull = value;
	}

	/**
	 * Method 'isDatasourceIdNull'
	 * 
	 * @return boolean
	 */
	public boolean isDatasourceIdNull()
	{
		return datasourceIdNull;
	}

	/**
	 * Method 'getDatasourceName'
	 * 
	 * @return String
	 */
	public String getDatasourceName()
	{
		return datasourceName;
	}

	/**
	 * Method 'setDatasourceName'
	 * 
	 * @param datasourceName
	 */
	public void setDatasourceName(String datasourceName)
	{
		this.datasourceName = datasourceName;
	}

	/**
	 * Method 'getRdbmsId'
	 * 
	 * @return int
	 */
	public int getRdbmsId()
	{
		return rdbmsId;
	}

	/**
	 * Method 'setRdbmsId'
	 * 
	 * @param rdbmsId
	 */
	public void setRdbmsId(int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
		this.rdbmsIdNull = false;
	}

	/**
	 * Method 'setRdbmsIdNull'
	 * 
	 * @param value
	 */
	public void setRdbmsIdNull(boolean value)
	{
		this.rdbmsIdNull = value;
	}

	/**
	 * Method 'isRdbmsIdNull'
	 * 
	 * @return boolean
	 */
	public boolean isRdbmsIdNull()
	{
		return rdbmsIdNull;
	}

	/**
	 * Method 'getRdbmsName'
	 * 
	 * @return String
	 */
	public String getRdbmsName()
	{
		return rdbmsName;
	}

	/**
	 * Method 'setRdbmsName'
	 * 
	 * @param rdbmsName
	 */
	public void setRdbmsName(String rdbmsName)
	{
		this.rdbmsName = rdbmsName;
	}

	/**
	 * Method 'getRdbmsDatabaseName'
	 * 
	 * @return String
	 */
	public String getRdbmsDatabaseName()
	{
		return rdbmsDatabaseName;
	}

	/**
	 * Method 'setRdbmsDatabaseName'
	 * 
	 * @param rdbmsDatabaseName
	 */
	public void setRdbmsDatabaseName(String rdbmsDatabaseName)
	{
		this.rdbmsDatabaseName = rdbmsDatabaseName;
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getUserName'
	 * 
	 * @return String
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * Method 'setUserName'
	 * 
	 * @param userName
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * Method 'getReportQuery'
	 * 
	 * @return String
	 */
	public String getReportQuery()
	{
		return reportQuery;
	}

	/**
	 * Method 'setReportQuery'
	 * 
	 * @param reportQuery
	 */
	public void setReportQuery(String reportQuery)
	{
		this.reportQuery = reportQuery;
	}

	/**
	 * Method 'getReportExcelLocation'
	 * 
	 * @return String
	 */
	public String getReportExcelLocation()
	{
		return reportExcelLocation;
	}

	/**
	 * Method 'setReportExcelLocation'
	 * 
	 * @param reportExcelLocation
	 */
	public void setReportExcelLocation(String reportExcelLocation)
	{
		this.reportExcelLocation = reportExcelLocation;
	}

	/**
	 * Method 'getReportPdfLocation'
	 * 
	 * @return String
	 */
	public String getReportPdfLocation()
	{
		return reportPdfLocation;
	}

	/**
	 * Method 'setReportPdfLocation'
	 * 
	 * @param reportPdfLocation
	 */
	public void setReportPdfLocation(String reportPdfLocation)
	{
		this.reportPdfLocation = reportPdfLocation;
	}

	/**
	 * Method 'getReportCdatetime'
	 * 
	 * @return String
	 */
	public String getReportCdatetime()
	{
		return reportCdatetime;
	}

	/**
	 * Method 'setReportCdatetime'
	 * 
	 * @param reportCdatetime
	 */
	public void setReportCdatetime(String reportCdatetime)
	{
		this.reportCdatetime = reportCdatetime;
	}

	/**
	 * Method 'getReportMdatetime'
	 * 
	 * @return String
	 */
	public String getReportMdatetime()
	{
		return reportMdatetime;
	}

	/**
	 * Method 'setReportMdatetime'
	 * 
	 * @param reportMdatetime
	 */
	public void setReportMdatetime(String reportMdatetime)
	{
		this.reportMdatetime = reportMdatetime;
	}

	/**
	 * Method 'getRtemplateSqlquery'
	 * 
	 * @return String
	 */
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

	/**
	 * Method 'setRtemplateSqlquery'
	 * 
	 * @param rtemplateSqlquery
	 */
	public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	/**
	 * Method 'getRtemplateBirtRpt'
	 * 
	 * @return String
	 */
	public String getRtemplateBirtRpt()
	{
		return rtemplateBirtRpt;
	}

	/**
	 * Method 'setRtemplateBirtRpt'
	 * 
	 * @param rtemplateBirtRpt
	 */
	public void setRtemplateBirtRpt(String rtemplateBirtRpt)
	{
		this.rtemplateBirtRpt = rtemplateBirtRpt;
	}

	/**
	 * Method 'getRtemplateTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateTitle()
	{
		return rtemplateTitle;
	}

	/**
	 * Method 'setRtemplateTitle'
	 * 
	 * @param rtemplateTitle
	 */
	public void setRtemplateTitle(String rtemplateTitle)
	{
		this.rtemplateTitle = rtemplateTitle;
	}

	/**
	 * Method 'getIsScheduled'
	 * 
	 * @return int
	 */
	public int getIsScheduled()
	{
		return isScheduled;
	}

	/**
	 * Method 'setIsScheduled'
	 * 
	 * @param isScheduled
	 */
	public void setIsScheduled(int isScheduled)
	{
		this.isScheduled = isScheduled;
		this.isScheduledNull = false;
	}

	/**
	 * Method 'setIsScheduledNull'
	 * 
	 * @param value
	 */
	public void setIsScheduledNull(boolean value)
	{
		this.isScheduledNull = value;
	}

	/**
	 * Method 'isIsScheduledNull'
	 * 
	 * @return boolean
	 */
	public boolean isIsScheduledNull()
	{
		return isScheduledNull;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return int
	 */
	public int getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(int isDeleted)
	{
		this.isDeleted = isDeleted;
		this.isDeletedNull = false;
	}

	/**
	 * Method 'setIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setIsDeletedNull(boolean value)
	{
		this.isDeletedNull = value;
	}

	/**
	 * Method 'isIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isIsDeletedNull()
	{
		return isDeletedNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsViewReportsInstanceDetails)) {
			return false;
		}
		
		final IprodReportsViewReportsInstanceDetails _cast = (IprodReportsViewReportsInstanceDetails) _other;
		if (reportId != _cast.reportId) {
			return false;
		}
		
		if (rtemplateId != _cast.rtemplateId) {
			return false;
		}
		
		if (rtemplateName == null ? _cast.rtemplateName != rtemplateName : !rtemplateName.equals( _cast.rtemplateName )) {
			return false;
		}
		
		if (datasourceId != _cast.datasourceId) {
			return false;
		}
		
		if (datasourceIdNull != _cast.datasourceIdNull) {
			return false;
		}
		
		if (datasourceName == null ? _cast.datasourceName != datasourceName : !datasourceName.equals( _cast.datasourceName )) {
			return false;
		}
		
		if (rdbmsId != _cast.rdbmsId) {
			return false;
		}
		
		if (rdbmsIdNull != _cast.rdbmsIdNull) {
			return false;
		}
		
		if (rdbmsName == null ? _cast.rdbmsName != rdbmsName : !rdbmsName.equals( _cast.rdbmsName )) {
			return false;
		}
		
		if (rdbmsDatabaseName == null ? _cast.rdbmsDatabaseName != rdbmsDatabaseName : !rdbmsDatabaseName.equals( _cast.rdbmsDatabaseName )) {
			return false;
		}
		
		if (userId != _cast.userId) {
			return false;
		}
		
		if (userName == null ? _cast.userName != userName : !userName.equals( _cast.userName )) {
			return false;
		}
		
		if (reportQuery == null ? _cast.reportQuery != reportQuery : !reportQuery.equals( _cast.reportQuery )) {
			return false;
		}
		
		if (reportExcelLocation == null ? _cast.reportExcelLocation != reportExcelLocation : !reportExcelLocation.equals( _cast.reportExcelLocation )) {
			return false;
		}
		
		if (reportPdfLocation == null ? _cast.reportPdfLocation != reportPdfLocation : !reportPdfLocation.equals( _cast.reportPdfLocation )) {
			return false;
		}
		
		if (reportCdatetime == null ? _cast.reportCdatetime != reportCdatetime : !reportCdatetime.equals( _cast.reportCdatetime )) {
			return false;
		}
		
		if (reportMdatetime == null ? _cast.reportMdatetime != reportMdatetime : !reportMdatetime.equals( _cast.reportMdatetime )) {
			return false;
		}
		
		if (rtemplateSqlquery == null ? _cast.rtemplateSqlquery != rtemplateSqlquery : !rtemplateSqlquery.equals( _cast.rtemplateSqlquery )) {
			return false;
		}
		
		if (rtemplateBirtRpt == null ? _cast.rtemplateBirtRpt != rtemplateBirtRpt : !rtemplateBirtRpt.equals( _cast.rtemplateBirtRpt )) {
			return false;
		}
		
		if (rtemplateTitle == null ? _cast.rtemplateTitle != rtemplateTitle : !rtemplateTitle.equals( _cast.rtemplateTitle )) {
			return false;
		}
		
		if (isScheduled != _cast.isScheduled) {
			return false;
		}
		
		if (isScheduledNull != _cast.isScheduledNull) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		if (isDeletedNull != _cast.isDeletedNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + reportId;
		_hashCode = 29 * _hashCode + rtemplateId;
		if (rtemplateName != null) {
			_hashCode = 29 * _hashCode + rtemplateName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + datasourceId;
		_hashCode = 29 * _hashCode + (datasourceIdNull ? 1 : 0);
		if (datasourceName != null) {
			_hashCode = 29 * _hashCode + datasourceName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + rdbmsId;
		_hashCode = 29 * _hashCode + (rdbmsIdNull ? 1 : 0);
		if (rdbmsName != null) {
			_hashCode = 29 * _hashCode + rdbmsName.hashCode();
		}
		
		if (rdbmsDatabaseName != null) {
			_hashCode = 29 * _hashCode + rdbmsDatabaseName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + userId;
		if (userName != null) {
			_hashCode = 29 * _hashCode + userName.hashCode();
		}
		
		if (reportQuery != null) {
			_hashCode = 29 * _hashCode + reportQuery.hashCode();
		}
		
		if (reportExcelLocation != null) {
			_hashCode = 29 * _hashCode + reportExcelLocation.hashCode();
		}
		
		if (reportPdfLocation != null) {
			_hashCode = 29 * _hashCode + reportPdfLocation.hashCode();
		}
		
		if (reportCdatetime != null) {
			_hashCode = 29 * _hashCode + reportCdatetime.hashCode();
		}
		
		if (reportMdatetime != null) {
			_hashCode = 29 * _hashCode + reportMdatetime.hashCode();
		}
		
		if (rtemplateSqlquery != null) {
			_hashCode = 29 * _hashCode + rtemplateSqlquery.hashCode();
		}
		
		if (rtemplateBirtRpt != null) {
			_hashCode = 29 * _hashCode + rtemplateBirtRpt.hashCode();
		}
		
		if (rtemplateTitle != null) {
			_hashCode = 29 * _hashCode + rtemplateTitle.hashCode();
		}
		
		_hashCode = 29 * _hashCode + isScheduled;
		_hashCode = 29 * _hashCode + (isScheduledNull ? 1 : 0);
		_hashCode = 29 * _hashCode + isDeleted;
		_hashCode = 29 * _hashCode + (isDeletedNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsViewReportsInstanceDetails: " );
		ret.append( "reportId=" + reportId );
		ret.append( ", rtemplateId=" + rtemplateId );
		ret.append( ", rtemplateName=" + rtemplateName );
		ret.append( ", datasourceId=" + datasourceId );
		ret.append( ", datasourceName=" + datasourceName );
		ret.append( ", rdbmsId=" + rdbmsId );
		ret.append( ", rdbmsName=" + rdbmsName );
		ret.append( ", rdbmsDatabaseName=" + rdbmsDatabaseName );
		ret.append( ", userId=" + userId );
		ret.append( ", userName=" + userName );
		ret.append( ", reportQuery=" + reportQuery );
		ret.append( ", reportExcelLocation=" + reportExcelLocation );
		ret.append( ", reportPdfLocation=" + reportPdfLocation );
		ret.append( ", reportCdatetime=" + reportCdatetime );
		ret.append( ", reportMdatetime=" + reportMdatetime );
		ret.append( ", rtemplateSqlquery=" + rtemplateSqlquery );
		ret.append( ", rtemplateBirtRpt=" + rtemplateBirtRpt );
		ret.append( ", rtemplateTitle=" + rtemplateTitle );
		ret.append( ", isScheduled=" + isScheduled );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

}
