package com.id4.iprod.reports.dto;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsJobData implements Serializable
{
	/** 
	 * This attribute maps to the column JOB_ID in the iprod_reports_job_data table.
	 */
	protected int jobId;

	/** 
	 * This attribute maps to the column JOB_NAME in the iprod_reports_job_data table.
	 */
	protected String jobName;

	/** 
	 * This attribute maps to the column JOB_DESCRIPTION in the iprod_reports_job_data table.
	 */
	protected String jobDescription;

	/** 
	 * This attribute maps to the column RECIPIENTS in the iprod_reports_job_data table.
	 */
	protected String recipients;

	/** 
	 * This attribute maps to the column REPEAT_INTERVAL in the iprod_reports_job_data table.
	 */
	protected String repeatInterval;

	/** 
	 * This attribute maps to the column START_DATE_TIME in the iprod_reports_job_data table.
	 */
	protected String startDateTime;

	/** 
	 * This attribute maps to the column END_DATE_TIME in the iprod_reports_job_data table.
	 */
	protected String endDateTime;

	/** 
	 * This attribute maps to the column ACTIVE in the iprod_reports_job_data table.
	 */
	protected int active;

	/** 
	 * This attribute represents whether the primitive attribute active is null.
	 */
	protected boolean activeNull = true;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_reports_job_data table.
	 */
	protected int isDeleted;

	/** 
	 * This attribute represents whether the primitive attribute isDeleted is null.
	 */
	protected boolean isDeletedNull = true;

	/**
	 * Method 'IprodReportsJobData'
	 * 
	 */
	public IprodReportsJobData()
	{
	}

	/**
	 * Method 'getJobId'
	 * 
	 * @return int
	 */
	public int getJobId()
	{
		return jobId;
	}

	/**
	 * Method 'setJobId'
	 * 
	 * @param jobId
	 */
	public void setJobId(int jobId)
	{
		this.jobId = jobId;
	}

	/**
	 * Method 'getJobName'
	 * 
	 * @return String
	 */
	public String getJobName()
	{
		return jobName;
	}

	/**
	 * Method 'setJobName'
	 * 
	 * @param jobName
	 */
	public void setJobName(String jobName)
	{
		this.jobName = jobName;
	}

	/**
	 * Method 'getJobDescription'
	 * 
	 * @return String
	 */
	public String getJobDescription()
	{
		return jobDescription;
	}

	/**
	 * Method 'setJobDescription'
	 * 
	 * @param jobDescription
	 */
	public void setJobDescription(String jobDescription)
	{
		this.jobDescription = jobDescription;
	}

	/**
	 * Method 'getRecipients'
	 * 
	 * @return String
	 */
	public String getRecipients()
	{
		return recipients;
	}

	/**
	 * Method 'setRecipients'
	 * 
	 * @param recipients
	 */
	public void setRecipients(String recipients)
	{
		this.recipients = recipients;
	}

	/**
	 * Method 'getRepeatInterval'
	 * 
	 * @return String
	 */
	public String getRepeatInterval()
	{
		return repeatInterval;
	}

	/**
	 * Method 'setRepeatInterval'
	 * 
	 * @param repeatInterval
	 */
	public void setRepeatInterval(String repeatInterval)
	{
		this.repeatInterval = repeatInterval;
	}

	/**
	 * Method 'getStartDateTime'
	 * 
	 * @return String
	 */
	public String getStartDateTime()
	{
		return startDateTime;
	}

	/**
	 * Method 'setStartDateTime'
	 * 
	 * @param startDateTime
	 */
	public void setStartDateTime(String startDateTime)
	{
		this.startDateTime = startDateTime;
	}

	/**
	 * Method 'getEndDateTime'
	 * 
	 * @return String
	 */
	public String getEndDateTime()
	{
		return endDateTime;
	}

	/**
	 * Method 'setEndDateTime'
	 * 
	 * @param endDateTime
	 */
	public void setEndDateTime(String endDateTime)
	{
		this.endDateTime = endDateTime;
	}

	/**
	 * Method 'getActive'
	 * 
	 * @return int
	 */
	public int getActive()
	{
		return active;
	}

	/**
	 * Method 'setActive'
	 * 
	 * @param active
	 */
	public void setActive(int active)
	{
		this.active = active;
		this.activeNull = false;
	}

	/**
	 * Method 'setActiveNull'
	 * 
	 * @param value
	 */
	public void setActiveNull(boolean value)
	{
		this.activeNull = value;
	}

	/**
	 * Method 'isActiveNull'
	 * 
	 * @return boolean
	 */
	public boolean isActiveNull()
	{
		return activeNull;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return int
	 */
	public int getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(int isDeleted)
	{
		this.isDeleted = isDeleted;
		this.isDeletedNull = false;
	}

	/**
	 * Method 'setIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setIsDeletedNull(boolean value)
	{
		this.isDeletedNull = value;
	}

	/**
	 * Method 'isIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isIsDeletedNull()
	{
		return isDeletedNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsJobData)) {
			return false;
		}
		
		final IprodReportsJobData _cast = (IprodReportsJobData) _other;
		if (jobId != _cast.jobId) {
			return false;
		}
		
		if (jobName == null ? _cast.jobName != jobName : !jobName.equals( _cast.jobName )) {
			return false;
		}
		
		if (jobDescription == null ? _cast.jobDescription != jobDescription : !jobDescription.equals( _cast.jobDescription )) {
			return false;
		}
		
		if (recipients == null ? _cast.recipients != recipients : !recipients.equals( _cast.recipients )) {
			return false;
		}
		
		if (repeatInterval == null ? _cast.repeatInterval != repeatInterval : !repeatInterval.equals( _cast.repeatInterval )) {
			return false;
		}
		
		if (startDateTime == null ? _cast.startDateTime != startDateTime : !startDateTime.equals( _cast.startDateTime )) {
			return false;
		}
		
		if (endDateTime == null ? _cast.endDateTime != endDateTime : !endDateTime.equals( _cast.endDateTime )) {
			return false;
		}
		
		if (active != _cast.active) {
			return false;
		}
		
		if (activeNull != _cast.activeNull) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		if (isDeletedNull != _cast.isDeletedNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + jobId;
		if (jobName != null) {
			_hashCode = 29 * _hashCode + jobName.hashCode();
		}
		
		if (jobDescription != null) {
			_hashCode = 29 * _hashCode + jobDescription.hashCode();
		}
		
		if (recipients != null) {
			_hashCode = 29 * _hashCode + recipients.hashCode();
		}
		
		if (repeatInterval != null) {
			_hashCode = 29 * _hashCode + repeatInterval.hashCode();
		}
		
		if (startDateTime != null) {
			_hashCode = 29 * _hashCode + startDateTime.hashCode();
		}
		
		if (endDateTime != null) {
			_hashCode = 29 * _hashCode + endDateTime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + active;
		_hashCode = 29 * _hashCode + (activeNull ? 1 : 0);
		_hashCode = 29 * _hashCode + isDeleted;
		_hashCode = 29 * _hashCode + (isDeletedNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodReportsJobDataPk
	 */
	public IprodReportsJobDataPk createPk()
	{
		return new IprodReportsJobDataPk(jobId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsJobData: " );
		ret.append( "jobId=" + jobId );
		ret.append( ", jobName=" + jobName );
		ret.append( ", jobDescription=" + jobDescription );
		ret.append( ", recipients=" + recipients );
		ret.append( ", repeatInterval=" + repeatInterval );
		ret.append( ", startDateTime=" + startDateTime );
		ret.append( ", endDateTime=" + endDateTime );
		ret.append( ", active=" + active );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

}
