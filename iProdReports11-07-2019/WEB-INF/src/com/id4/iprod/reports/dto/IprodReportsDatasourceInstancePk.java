package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_datasource_instance table.
 */
public class IprodReportsDatasourceInstancePk implements Serializable
{
	protected int datasourceId;

	/** 
	 * This attribute represents whether the primitive attribute datasourceId is null.
	 */
	protected boolean datasourceIdNull;

	/** 
	 * Sets the value of datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
	}

	/** 
	 * Gets the value of datasourceId
	 */
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'IprodReportsDatasourceInstancePk'
	 * 
	 */
	public IprodReportsDatasourceInstancePk()
	{
	}

	/**
	 * Method 'IprodReportsDatasourceInstancePk'
	 * 
	 * @param datasourceId
	 */
	public IprodReportsDatasourceInstancePk(final int datasourceId)
	{
		this.datasourceId = datasourceId;
	}

	/** 
	 * Sets the value of datasourceIdNull
	 */
	public void setDatasourceIdNull(boolean datasourceIdNull)
	{
		this.datasourceIdNull = datasourceIdNull;
	}

	/** 
	 * Gets the value of datasourceIdNull
	 */
	public boolean isDatasourceIdNull()
	{
		return datasourceIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsDatasourceInstancePk)) {
			return false;
		}
		
		final IprodReportsDatasourceInstancePk _cast = (IprodReportsDatasourceInstancePk) _other;
		if (datasourceId != _cast.datasourceId) {
			return false;
		}
		
		if (datasourceIdNull != _cast.datasourceIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + datasourceId;
		_hashCode = 29 * _hashCode + (datasourceIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsDatasourceInstancePk: " );
		ret.append( "datasourceId=" + datasourceId );
		return ret.toString();
	}

}
