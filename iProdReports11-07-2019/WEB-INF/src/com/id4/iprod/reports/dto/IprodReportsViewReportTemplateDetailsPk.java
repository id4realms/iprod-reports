package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_view_report_template_details table.
 */
public class IprodReportsViewReportTemplateDetailsPk implements Serializable
{
	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsViewReportTemplateDetailsPk)) {
			return false;
		}
		
		final IprodReportsViewReportTemplateDetailsPk _cast = (IprodReportsViewReportTemplateDetailsPk) _other;
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetailsPk: " );
		return ret.toString();
	}

}
