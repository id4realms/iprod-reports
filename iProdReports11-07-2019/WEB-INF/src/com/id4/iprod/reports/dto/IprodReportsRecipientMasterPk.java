package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_recipient_master table.
 */
public class IprodReportsRecipientMasterPk implements Serializable
{
	protected int recipientId;

	/** 
	 * This attribute represents whether the primitive attribute recipientId is null.
	 */
	protected boolean recipientIdNull;

	/** 
	 * Sets the value of recipientId
	 */
	public void setRecipientId(int recipientId)
	{
		this.recipientId = recipientId;
	}

	/** 
	 * Gets the value of recipientId
	 */
	public int getRecipientId()
	{
		return recipientId;
	}

	/**
	 * Method 'IprodReportsRecipientMasterPk'
	 * 
	 */
	public IprodReportsRecipientMasterPk()
	{
	}

	/**
	 * Method 'IprodReportsRecipientMasterPk'
	 * 
	 * @param recipientId
	 */
	public IprodReportsRecipientMasterPk(final int recipientId)
	{
		this.recipientId = recipientId;
	}

	/** 
	 * Sets the value of recipientIdNull
	 */
	public void setRecipientIdNull(boolean recipientIdNull)
	{
		this.recipientIdNull = recipientIdNull;
	}

	/** 
	 * Gets the value of recipientIdNull
	 */
	public boolean isRecipientIdNull()
	{
		return recipientIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsRecipientMasterPk)) {
			return false;
		}
		
		final IprodReportsRecipientMasterPk _cast = (IprodReportsRecipientMasterPk) _other;
		if (recipientId != _cast.recipientId) {
			return false;
		}
		
		if (recipientIdNull != _cast.recipientIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + recipientId;
		_hashCode = 29 * _hashCode + (recipientIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsRecipientMasterPk: " );
		ret.append( "recipientId=" + recipientId );
		return ret.toString();
	}

}
