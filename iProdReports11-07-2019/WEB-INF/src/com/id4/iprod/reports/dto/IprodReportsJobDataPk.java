package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_job_data table.
 */
public class IprodReportsJobDataPk implements Serializable
{
	protected int jobId;

	/** 
	 * This attribute represents whether the primitive attribute jobId is null.
	 */
	protected boolean jobIdNull;

	/** 
	 * Sets the value of jobId
	 */
	public void setJobId(int jobId)
	{
		this.jobId = jobId;
	}

	/** 
	 * Gets the value of jobId
	 */
	public int getJobId()
	{
		return jobId;
	}

	/**
	 * Method 'IprodReportsJobDataPk'
	 * 
	 */
	public IprodReportsJobDataPk()
	{
	}

	/**
	 * Method 'IprodReportsJobDataPk'
	 * 
	 * @param jobId
	 */
	public IprodReportsJobDataPk(final int jobId)
	{
		this.jobId = jobId;
	}

	/** 
	 * Sets the value of jobIdNull
	 */
	public void setJobIdNull(boolean jobIdNull)
	{
		this.jobIdNull = jobIdNull;
	}

	/** 
	 * Gets the value of jobIdNull
	 */
	public boolean isJobIdNull()
	{
		return jobIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsJobDataPk)) {
			return false;
		}
		
		final IprodReportsJobDataPk _cast = (IprodReportsJobDataPk) _other;
		if (jobId != _cast.jobId) {
			return false;
		}
		
		if (jobIdNull != _cast.jobIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + jobId;
		_hashCode = 29 * _hashCode + (jobIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsJobDataPk: " );
		ret.append( "jobId=" + jobId );
		return ret.toString();
	}

}
