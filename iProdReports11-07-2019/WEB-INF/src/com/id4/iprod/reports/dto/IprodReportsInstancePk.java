package com.id4.iprod.reports.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_reports_instance table.
 */
public class IprodReportsInstancePk implements Serializable
{
	protected int reportId;

	/** 
	 * This attribute represents whether the primitive attribute reportId is null.
	 */
	protected boolean reportIdNull;

	/** 
	 * Sets the value of reportId
	 */
	public void setReportId(int reportId)
	{
		this.reportId = reportId;
	}

	/** 
	 * Gets the value of reportId
	 */
	public int getReportId()
	{
		return reportId;
	}

	/**
	 * Method 'IprodReportsInstancePk'
	 * 
	 */
	public IprodReportsInstancePk()
	{
	}

	/**
	 * Method 'IprodReportsInstancePk'
	 * 
	 * @param reportId
	 */
	public IprodReportsInstancePk(final int reportId)
	{
		this.reportId = reportId;
	}

	/** 
	 * Sets the value of reportIdNull
	 */
	public void setReportIdNull(boolean reportIdNull)
	{
		this.reportIdNull = reportIdNull;
	}

	/** 
	 * Gets the value of reportIdNull
	 */
	public boolean isReportIdNull()
	{
		return reportIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsInstancePk)) {
			return false;
		}
		
		final IprodReportsInstancePk _cast = (IprodReportsInstancePk) _other;
		if (reportId != _cast.reportId) {
			return false;
		}
		
		if (reportIdNull != _cast.reportIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + reportId;
		_hashCode = 29 * _hashCode + (reportIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsInstancePk: " );
		ret.append( "reportId=" + reportId );
		return ret.toString();
	}

}
