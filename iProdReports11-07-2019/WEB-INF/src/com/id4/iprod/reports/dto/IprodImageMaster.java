package com.id4.iprod.reports.dto;

import java.io.Serializable;

public class IprodImageMaster implements Serializable
{
	/** 
	 * This attribute maps to the column IMAGE_ID in the iprod_image_master table.
	 */
	protected int imageId;

	/** 
	 * This attribute maps to the column IMAGE_NAME in the iprod_image_master table.
	 */
	protected String imageName;

	/** 
	 * This attribute maps to the column IMAGE_CONTENT in the iprod_image_master table.
	 */
	protected byte[] imageContent;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_image_master table.
	 */
	protected short isDeleted;

	/**
	 * Method 'IprodImageMaster'
	 * 
	 */
	public IprodImageMaster()
	{
	}

	/**
	 * Method 'getImageId'
	 * 
	 * @return int
	 */
	public int getImageId()
	{
		return imageId;
	}

	/**
	 * Method 'setImageId'
	 * 
	 * @param imageId
	 */
	public void setImageId(int imageId)
	{
		this.imageId = imageId;
	}

	/**
	 * Method 'getImageName'
	 * 
	 * @return String
	 */
	public String getImageName()
	{
		return imageName;
	}

	/**
	 * Method 'setImageName'
	 * 
	 * @param imageName
	 */
	public void setImageName(String imageName)
	{
		this.imageName = imageName;
	}

	/**
	 * Method 'getImageContent'
	 * 
	 * @return byte[]
	 */
	public byte[] getImageContent()
	{
		return imageContent;
	}

	/**
	 * Method 'setImageContent'
	 * 
	 * @param imageContent
	 */
	public void setImageContent(byte[] imageContent)
	{
		this.imageContent = imageContent;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodImageMaster)) {
			return false;
		}
		
		final IprodImageMaster _cast = (IprodImageMaster) _other;
		if (imageId != _cast.imageId) {
			return false;
		}
		
		if (imageName == null ? _cast.imageName != imageName : !imageName.equals( _cast.imageName )) {
			return false;
		}
		
		if (imageContent == null ? _cast.imageContent != imageContent : !imageContent.equals( _cast.imageContent )) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + imageId;
		if (imageName != null) {
			_hashCode = 29 * _hashCode + imageName.hashCode();
		}
		
		if (imageContent != null) {
			_hashCode = 29 * _hashCode + imageContent.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) isDeleted;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodImageMasterPk
	 */
	public IprodImageMasterPk createPk()
	{
		return new IprodImageMasterPk(imageId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodImageMaster: " );
		ret.append( "imageId=" + imageId );
		ret.append( ", imageName=" + imageName );
		ret.append( ", imageContent=" + imageContent );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

}
