package com.id4.iprod.reports;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DbConnection{
	protected static Connection connectionObject;

    protected Statement statementObject; 
    protected  String queryMessage; 
    
	public Statement getExecuteQuery(java.sql.Connection connectionObject, String query){
		if (connectionObject != null)
		{
			try {
				statementObject = connectionObject.createStatement();
				statementObject.execute(query);
			}// end of try//
			catch (SQLException se) {
			do{
				setQueryMessage(String.valueOf(se.getErrorCode())+se.getSQLState()+se.getMessage());
				se= se.getNextException();
				}while(se != null);
			}
		}// end of catch
		return statementObject;
	}

	public void closeConnection(Connection connectionObject){
		try{
			connectionObject.close();
		} 
		catch (SQLException e) {
	
			e.printStackTrace();
		}
	}

	
	public Connection getDbConnection(String jdbcDriverClassnameG,String jdbcDriverUrlG,String databaseUsernameG,String databasePasswordG){
		System.out.println("DbConnection::getDbConnection");
		try{
			Class.forName(jdbcDriverClassnameG);
			connectionObject = (Connection) DriverManager.getConnection(jdbcDriverUrlG,databaseUsernameG,databasePasswordG);
		}
		catch (NullPointerException ne){
			System.out.println(ne);
		}
		catch (Exception e) {
			System.out.println(e);
			System.out.println("ajax_validate_built_sql_query - DB connection closed due to error.");
		}
		return  connectionObject;
	}
	
	
	public  List<Map<String, String>> getQueryResultDetails(ResultSet rsObject){
	  System.out.println("In DbConnection.getQueryResultDetails()");
	  List<Map<String, String>> listQueryResultTable = new ArrayList<Map<String, String>>();
	  try{
			ResultSetMetaData rsMetaData = rsObject.getMetaData();
			System.out.println("rsMetaData.getColumnCount():"+rsMetaData.getColumnCount());
			while (rsObject.next()){
		         Map mapQueryTableDetails = new HashMap();
		         for (int i = 1; i <= rsMetaData.getColumnCount(); i++){
		             String key = rsMetaData.getColumnName(i);
		             //System.out.println("KEy is:"+key);
		             String value = rsObject.getString(key);
		             //System.out.println("value is:"+value);
		             mapQueryTableDetails.put(key, value);
		         }
		         for (Object key : mapQueryTableDetails.keySet()){
		        	 //System.out.println("Key : " + key.toString() + " Value : "+ mapQueryTableDetails.get(key));
		         }
		         listQueryResultTable.add(mapQueryTableDetails);
			}
	  }
	  catch (SQLException e){
			do{
				setQueryMessage(String.valueOf(e.getErrorCode()));
				setQueryMessage(e.getSQLState());
				setQueryMessage(e.getMessage());
				e= e.getNextException();
			}while(e != null);
			
	  }
	  return listQueryResultTable;
	}
	
	
	public String getSelectedColumnDataType(ResultSet rsObject,String dataFieldName){
	    // List listQueryColName= new ArrayList();
		String columnDataType = null;
		try
		{
				ResultSetMetaData rsMetaData = rsObject.getMetaData();
		        int numberOfColumns = rsMetaData.getColumnCount();
		        System.out.println("numberOfColumns"+numberOfColumns);

                if(rsObject.next()){
				    for(int i = 1; i <= numberOfColumns; i++){
				    	System.out.println("Comparing : "+dataFieldName+ " with : "+rsMetaData.getColumnLabel(i));
			        		if(rsMetaData.getColumnLabel(i).equals(dataFieldName)){
			        			// System.out.println("Matched for : "+rsMetaData.getColumnTypeName(i));
			        			columnDataType = rsMetaData.getColumnTypeName(i);
			        			break;
			        	    }
				    }
		
                }
		
		}
		 catch (SQLException e) {
				do{
					setQueryMessage(String.valueOf(e.getErrorCode()));
					setQueryMessage(e.getSQLState());
					setQueryMessage(e.getMessage());
					e= e.getNextException();
				}while(e != null);
				
			}
		return columnDataType;
	}
	
	
	
	public List getColumnDataType(ResultSet rsObject,String dataFieldName){
	    List listQueryColName= new ArrayList();
		try
		{
				ResultSetMetaData rsMetaData = rsObject.getMetaData();
		        int numberOfColumns = rsMetaData.getColumnCount();
		        //System.out.println("numberOfColumns"+numberOfColumns);
		        Map<String,String> mapQueryTableDetailsColName = new HashMap<String,String>();
                if(rsObject.next()){
               	
				    for(int i = 1; i <= numberOfColumns; i++){
			        		if(rsMetaData.getColumnLabel(i).equals(dataFieldName))
			        		{
			        			//System.out.println(rsMetaData.getColumnTypeName(i));
			        			rsMetaData.getColumnTypeName(i);
			        			//System.out.println("dataType====="+rsMetaData.getColumnTypeName(i).toUpperCase());
			        			listQueryColName.add( rsMetaData.getColumnTypeName(i));
			        	
			        	   }
				    }
		
                }
		
		}
		 catch (SQLException e) {
				do{
					setQueryMessage(String.valueOf(e.getErrorCode()));
					setQueryMessage(e.getSQLState());
					setQueryMessage(e.getMessage());
					e= e.getNextException();
				}while(e != null);
				
			}
		return listQueryColName;
	}
	
		
	public   String getQueryMessage() {
		return queryMessage;
	}

	public void setQueryMessage(String queryMessage) {
		this.queryMessage = queryMessage;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}