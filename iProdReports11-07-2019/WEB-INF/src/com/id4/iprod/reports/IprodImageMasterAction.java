package com.id4.iprod.reports;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.IprodImageCustomMaster;
import com.id4.iprod.Consts;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.id4.iprod.reports.dao.IprodImageMasterDao;
import com.id4.iprod.reports.dto.IprodImageMaster;
import com.id4.iprod.reports.dto.IprodImageMasterPk;
import com.id4.iprod.reports.exceptions.IprodImageMasterDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodImageMasterAction extends ActionSupport implements IprodModuleAuthorization
{
	static Logger log = Logger.getLogger(IprodImageMasterAction.class);

	/** 
	 * This attribute maps to the column IMAGE_ID in the iprod_image_master table.
	 */
	protected int imageId;

	/** 
	 * This attribute maps to the column IMAGE_NAME in the iprod_image_master table.
	 */
	protected String imageName;

	/** 
	 * This attribute maps to the column IMAGE_CONTENT in the iprod_image_master table.
	 */
	protected byte[] imageContent;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_image_master table.
	 */
	protected short isDeleted;

	protected String imagePath;
	
	
	
	public List<IprodImageMaster> allImageListG = new ArrayList<IprodImageMaster>();
	public List<IprodImageCustomMaster> allImageCustomListG = new ArrayList<IprodImageCustomMaster>();
	
	
	public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();

	
	public String addImage(){
		try{
			log.info("IprodImageMasterAction :: addImage");	
		    Map<String, Object> session = ActionContext.getContext().getSession();
			 if(session.get(Consts.IPRODUSERID)!= null){
			
				 log.info("Image path is :: " +this.imagePath);
					byte[] b = null;
					try {
						File file = new File(this.imagePath);
						FileInputStream input = new FileInputStream(file);
						b = new byte[(int) file.length()];
						 log.info("Image path is::"+b);
						input.read(b);
					} catch (Exception e) {
						log.error("addNewOperator :: Exception :: " + e.getMessage());
						
					}
					
					IprodImageMasterDao iprodImageMasterDao = DaoFactory.createIprodImageMasterDao();
					IprodImageMaster iprodImageMasterDto = new IprodImageMaster();
					iprodImageMasterDto.setImageName(this.imageName);
					iprodImageMasterDto.setImageContent(b);
					IprodImageMasterPk iprodImageMasterPk = new IprodImageMasterPk();
					iprodImageMasterPk = iprodImageMasterDao.insert(iprodImageMasterDto);
					addActionMessage("Image successfully added");
					return SUCCESS;
			  }else{
					 
					 addActionError("Sorry!! You have to Login first");
				     return "login";
			  } 
		}catch (Exception e){
			log.error("addImage :: Exception :: " + e.getMessage() +" occured in addImage.");
		}
		return SUCCESS;
	}
	
	public String execute() {
		return SUCCESS;
}	
	
	
	
	
	public String getAllImageDetails(){
		try{
			log.info("IprodImageMasterAction :: getAllImageDetails");
			Map<String, Object> session = ActionContext.getContext().getSession();
			int roleId = (Integer) session.get(Consts.IPRODUSERROLEID);
			log.info("hashMapCheckRoleModuleOperationListG :: "+hashMapCheckRoleModuleOperationListG);
			if(session.get(Consts.IPRODUSERID)!= null)
		    {
			  IprodImageMasterDao imageMasterDao = DaoFactory.createIprodImageMasterDao();
			  List<IprodImageMaster> allImageListL = new ArrayList<IprodImageMaster>();
			  try {	
					  //allImageListL = roleDao.findWhereRoleIdNotEquals(1);		
				  allImageListL = imageMasterDao.findWhereIsDeletedEquals((short)0);
				  log.info("allImageListL:" +allImageListL);
				  	  if(allImageListL.size() > 0){				
						  for(int i= 0; i<allImageListL.size(); i++)
						  {
							  allImageCustomListG.add(i, new IprodImageCustomMaster(allImageListL.get(i).getImageId(), allImageListL.get(i).getImageName(), Base64.getEncoder().encodeToString(allImageListL.get(i).getImageContent()) ));
						  }
						  log.info("allImageListG ::" +allImageCustomListG);
					  }else{			  	
						  addActionError("Currently there are no roles to be displayed");
				  	  }
				  	  return SUCCESS;
			  } catch (IprodImageMasterDaoException e) {
				addActionError("IprodImageMasterActionException"+e.getMessage());
				return ERROR;
			  }
		    }else{
		    	addActionError("Sorry!! You have to Login first");
		    	return "login";
		    }
		}catch (Exception e){
			log.info("getAllImages :: Exception :: " + e.getMessage() +" occured in getAllRoles.");
		}
		return SUCCESS;	
	}
	
	// FUNCTION TO DELETE THE VALUES OF THE SELECTED Image 
		public String deleteImage(){
			try{
			log.info("IprodImageMasterAction :: deleteImage");
			Map<String, Object> session = ActionContext.getContext().getSession();
			if (session.get(Consts.IPRODUSERID) != null) {
				IprodImageMasterDao imageDetailsDao = DaoFactory.createIprodImageMasterDao();
				try {
					imageDetailsDao.updateIsDeleted((short)1,this.imageId);
				} catch (IprodImageMasterDaoException e) {
					e.printStackTrace();
				}
				addActionMessage("Image Deleted Successfully !");
				return SUCCESS;					
			} else {
				addActionError("Sorry!! You have to Login first");
				return "login";
			}
			}catch (Exception e){
				log.error("deleteImage :: Exception :: " + e.getMessage() +" occured in deleteImage.");
			}
			return SUCCESS;
		}


		/**
		 * Method 'getImageId'
		 * 
		 * @return int
		 */
		public int getImageId()
		{
			return imageId;
		}

		/**
		 * Method 'setImageId'
		 * 
		 * @param imageId
		 */
		public void setImageId(int imageId)
		{
			this.imageId = imageId;
		}

		/**
		 * Method 'getImageName'
		 * 
		 * @return String
		 */
		public String getImageName()
		{
			return imageName;
		}

		/**
		 * Method 'setImageName'
		 * 
		 * @param imageName
		 */
		public void setImageName(String imageName)
		{
			this.imageName = imageName;
		}

		/**
		 * Method 'getImageContent'
		 * 
		 * @return byte[]
		 */
		public byte[] getImageContent()
		{
			return imageContent;
		}

		/**
		 * Method 'setImageContent'
		 * 
		 * @param imageContent
		 */
		public void setImageContent(byte[] imageContent)
		{
			this.imageContent = imageContent;
		}

		/**
		 * Method 'getIsDeleted'
		 * 
		 * @return short
		 */
		public short getIsDeleted()
		{
			return isDeleted;
		}

		/**
		 * Method 'setIsDeleted'
		 * 
		 * @param isDeleted
		 */
		public void setIsDeleted(short isDeleted)
		{
			this.isDeleted = isDeleted;
		}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	
	public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
		return hashMapCheckRoleModuleOperationListG;
	}


	public void setHashMapCheckRoleModuleOperationListG(HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
		this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
	}


}
