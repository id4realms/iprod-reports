
package com.id4.iprod.reports;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.id4.iprod.IprodViewModuleOperationMasterAction;
import com.id4.iprod.exceptions.IprodReportsViewReportDatasourceDetailsDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.reports.dao.IprodReportsDatasourceInstanceDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportDatasourceDetailsDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportTemplateDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsDatasourceInstance;
import com.id4.iprod.reports.dto.IprodReportsViewReportDatasourceDetails;
import com.id4.iprod.reports.exceptions.IprodReportsDatasourceInstanceDaoException;
import com.opensymphony.xwork2.ActionSupport;


public class IprodReportsSqlQueryMasterAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	static Logger log = Logger.getLogger(IprodReportsSqlQueryMasterAction.class);


	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprodreports_view_report_sql_query_details table.
	 */
	protected String rtemplateSqlquery;
	protected String queryColumnName;
	protected Integer datasourceId;
	protected String inputJSONData;
	protected String isSQLQueryValidCheckMessage;
	protected String dataFieldName;
	protected String outputJSONData = null;
	protected String jsonSQLColumnDataType = null;
	protected Integer rtemplateId;
	Connection con;
	ResultSet rsObject;
	Statement st;
	String jdbcDriverClassname,jdbcDriverUrl,databaseName,databaseServerUsername,databaseServerPassword;


	public void setJDBCConnectionParmsFromDatasource(int datasourceId){
		List<IprodReportsViewReportDatasourceDetails> iprodReportsViewReportDatasourceDetailsListL =new ArrayList<IprodReportsViewReportDatasourceDetails>();
		IprodReportsViewReportDatasourceDetailsDao iprodReportsViewReportDatasourceDetailsDao = DaoFactory.createIprodReportsViewReportDatasourceDetailsDao();
		try {
			iprodReportsViewReportDatasourceDetailsListL = iprodReportsViewReportDatasourceDetailsDao.findWhereDatasourceIdEquals(datasourceId);
		} catch (IprodReportsViewReportDatasourceDetailsDaoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		jdbcDriverClassname=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsJdbcDriverClass();
		jdbcDriverUrl=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsJdbcDriverUrl();
		databaseName=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsDatabaseName();
		databaseServerUsername=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsServerUsername();
		databaseServerPassword=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsServerPassword();
		System.out.println("The Details are:"+"\t"+jdbcDriverClassname+"\t"+jdbcDriverUrl+"\t"+jdbcDriverClassname+"\t"+jdbcDriverUrl+"\t"+databaseServerPassword);
	}

	
	public String validateSqlQuery() {
		log.info("IprodReportSqlQueryMasterAction:validateSqlQuery()");
		Gson gs=new GsonBuilder().serializeNulls().create();
		DbConnection dc=new DbConnection();
		//Convert input JSON data into POJO IprodReportSqlQueryDataSourceInstanceIdDetails object
		IprodReportsSQLDataset iprodReportsSQLDatasetPJObj = gs.fromJson(inputJSONData, IprodReportsSQLDataset.class);  
		rtemplateSqlquery=iprodReportsSQLDatasetPJObj.getRtemplateSqlquery();
		datasourceId=iprodReportsSQLDatasetPJObj.getDatasourceId();
		setJDBCConnectionParmsFromDatasource(datasourceId);
		try
		{
			con=dc.getDbConnection(jdbcDriverClassname, jdbcDriverUrl, databaseServerUsername, databaseServerPassword);
			List<Map<String, String>> listReportQueryResultTable = new ArrayList<Map<String, String>>();
			List<String> listColName=new ArrayList<String>();
			st=dc.getExecuteQuery(con, rtemplateSqlquery);
			rsObject = st.getResultSet();
			ResultSetMetaData rsMetaData = rsObject.getMetaData();
			// System.out.println(dc.getQueryMessage());
			String exceptionMessageOutput=dc.getQueryMessage();
			if(exceptionMessageOutput==null)
			{
				if (rsObject!=null)
				{
					for (int i = 1; i <= rsMetaData.getColumnCount(); i++)
					{ 	
						String key = rsMetaData.getColumnName(i);
						listColName.add(key.toString());
					} 	
					queryColumnName=gs.toJson(listColName);
					setIsSQLQueryValidCheckMessage("VALIDATED");            	   		
					outputJSONData = gs.toJson(dc.getQueryResultDetails(rsObject));
					//log.info("OUTPUT JSON Data Object key-values are :"+outputJSONData);
				}
			}
		}
		catch(NullPointerException e)
		{
			setIsSQLQueryValidCheckMessage("Validate Query NULL Pointer Exception : "+e.getMessage());

		}
		catch (SQLException e)
		{
			do{
				setIsSQLQueryValidCheckMessage("Validate Query SQL Exception : "+e.getMessage());
			}while(e != null);

		}
		catch(Exception e)
		{
			setIsSQLQueryValidCheckMessage("Validate Query General Exception : "+e.getMessage());

		}
		finally 
		{
			dc.closeConnection(con);               

		}	

		return SUCCESS;
	}

	/**
	 * fetchSelectedSQLColumnDataType  - to get data type of a particular column type within a database table
	 * @return
	 */
	public String fetchSelectedSQLColumnDataType(){   
		log.info("Init IprodReportsSqlQueryMasterAction.fetchSelectedSQLColumnDataType()");
		Gson gs=new GsonBuilder().serializeNulls().create();
		DbConnection dc=new DbConnection();
		System.out.println("Data received:"+ getInputJSONData());
		IprodReportsSQLDataset iprodReportsSQLDatasetPJObj = gs.fromJson(inputJSONData, IprodReportsSQLDataset.class);
		rtemplateSqlquery=iprodReportsSQLDatasetPJObj.getRtemplateSqlquery();
		System.out.println("rtemplateSqlquery:"+rtemplateSqlquery);
		datasourceId=iprodReportsSQLDatasetPJObj.getDatasourceId(); 	   
		System.out.println("datasourceId"+datasourceId);
		dataFieldName=iprodReportsSQLDatasetPJObj.getDataFieldName();
		System.out.println("dataFieldName:"+dataFieldName);
		setJDBCConnectionParmsFromDatasource(datasourceId);

		try{  
			con=dc.getDbConnection(jdbcDriverClassname, jdbcDriverUrl, databaseServerUsername, databaseServerPassword);
			System.out.println("Database connected");
			st=dc.getExecuteQuery(con, rtemplateSqlquery);
			System.out.println("query statement executed");
			rsObject = st.getResultSet();
			System.out.println("dc.getColumnDataType(rsObject,dataFieldName)"+dc.getColumnDataType(rsObject,dataFieldName));
			String dataType = "";
			switch(dc.getColumnDataType(rsObject,dataFieldName).toString()){
			case "[int]" : dataType = "INT";
						   break;
			case "[float]" : dataType = "FLOAT";
			   				  break;
			case "[varchar]" : dataType = "VARCHAR";
			   				   break;
			case "[boolean]" : dataType = "BOOLEAN";
			   				   break;
			default : break;
						
			}
			jsonSQLColumnDataType = gs.toJson(dataType);

		}			
		catch(IncorrectResultSizeDataAccessException rsde){

			System.out.println("IncorrectResultSizeDataAccessException");
			rsde.printStackTrace();
		}
		catch(NullPointerException ne){
			System.out.println("NullPointerException");
			ne.printStackTrace();

		}
		catch(SQLException e1){ 
			setIsSQLQueryValidCheckMessage("SQLException");
			System.out.println("DB connection closed due to error.");
		}
		finally{

			dc.closeConnection(con);               

		}	

		return SUCCESS;
	}

	/**
	 * fetchListOfSQLColumnsAndDataType  - to get data type of a particular column type within a database table
	 * @return
	 */
	public String fetchListOfSQLColumnsAndDataType(){   
		log.info("Init IprodReportsSqlQueryMasterAction.fetchListOfSQLColumnsAndDataType()");
		Gson gs=new GsonBuilder().serializeNulls().create();
		DbConnection dc=new DbConnection();
		IprodReportsSQLDataset iprodReportsSQLDatasetPJObj = gs.fromJson(inputJSONData, IprodReportsSQLDataset.class);
		rtemplateSqlquery=iprodReportsSQLDatasetPJObj.getRtemplateSqlquery();
		datasourceId=iprodReportsSQLDatasetPJObj.getDatasourceId(); 	   
		setJDBCConnectionParmsFromDatasource(this.datasourceId);

		try{  
			con=dc.getDbConnection(jdbcDriverClassname, jdbcDriverUrl, databaseServerUsername, databaseServerPassword);
			st=dc.getExecuteQuery(con, rtemplateSqlquery);
			rsObject = st.getResultSet();

			ResultSetMetaData rsMetaData = rsObject.getMetaData();
			// System.out.println(dc.getQueryMessage());
			String exceptionMessageOutput=dc.getQueryMessage();
			List<IprodReportsGenerateReportFilterColumn> iprodReportsGenerateReportFilterColumnList= new ArrayList<IprodReportsGenerateReportFilterColumn>();
			if(exceptionMessageOutput==null){
				if (rsObject != null){
					for (int i = 1; i <= rsMetaData.getColumnCount(); i++){ 	
						IprodReportsGenerateReportFilterColumn iprodReportsGenerateReportFilterColumn = new IprodReportsGenerateReportFilterColumn();
						iprodReportsGenerateReportFilterColumn.id = rsMetaData.getColumnName(i).trim();
						iprodReportsGenerateReportFilterColumn.label = rsMetaData.getColumnName(i).trim();
						List<String> operators = new ArrayList<String>();
						switch(rsMetaData.getColumnTypeName(i).trim().toUpperCase()){
						//Only here lies a cheat to support MS SQL Server date-time-datetime CASTs in VARCHAR switch
						//So if a COLUMN has SUB-STRINGS like DATE or TIME in the Column NAME and is
						//of DB TYPE - VARCHAR it should still be TREATED as NATIVE DB DATE, TIME & DATETIME types

						case "VARCHAR" :
							if (iprodReportsGenerateReportFilterColumn.id.contains("DATETIME")){
								iprodReportsGenerateReportFilterColumn.type = "datetime";
								iprodReportsGenerateReportFilterColumn.id = iprodReportsGenerateReportFilterColumn.label;
								iprodReportsGenerateReportFilterColumn.plugin = "datetimepicker";
								IprodReportsGenerateReportFilterDatePickerPluginConfig pluginConfig = new IprodReportsGenerateReportFilterDatePickerPluginConfig();
								pluginConfig.setTodayBtn("linked");
								pluginConfig.setTodayHighlight(true);
								pluginConfig.setFormat("yyyy/mm/dd hh:mm:ss");
								pluginConfig.setAutoclose(true);
								operators.add("equal");
								operators.add("not_equal");
								operators.add("between");
								iprodReportsGenerateReportFilterColumn.setOperators(operators);
								iprodReportsGenerateReportFilterColumn.setPlugin_config(pluginConfig);
							}
							
							else if (iprodReportsGenerateReportFilterColumn.id.contains("DATE")){
								System.out.println("3");
								iprodReportsGenerateReportFilterColumn.type = "date";
								iprodReportsGenerateReportFilterColumn.id = "CAST(CONVERT(DATETIME,"+iprodReportsGenerateReportFilterColumn.label+",120) AS DATETIME)";
								iprodReportsGenerateReportFilterColumn.plugin = "datepicker";
								IprodReportsGenerateReportFilterDatePickerPluginConfig pluginConfig = new IprodReportsGenerateReportFilterDatePickerPluginConfig();
								pluginConfig.setTodayBtn("linked");
								pluginConfig.setTodayHighlight(true);
								pluginConfig.setFormat("mm/dd/yyyy");
								pluginConfig.setAutoclose(true);
								operators.add("between");
								iprodReportsGenerateReportFilterColumn.setOperators(operators);
								iprodReportsGenerateReportFilterColumn.setPlugin_config(pluginConfig);
							}

							else if (iprodReportsGenerateReportFilterColumn.id.contains("TIME")){
								iprodReportsGenerateReportFilterColumn.type = "time";
								iprodReportsGenerateReportFilterColumn.id = "CAST("+iprodReportsGenerateReportFilterColumn.label+" AS DATETIME)";
								iprodReportsGenerateReportFilterColumn.plugin = "timepicker";
								IprodReportsGenerateReportFilterDatePickerPluginConfig pluginConfig = new IprodReportsGenerateReportFilterDatePickerPluginConfig();
								pluginConfig.setTodayBtn("linked");
								pluginConfig.setTodayHighlight(true);
								pluginConfig.setFormat("hh:mm:ss");
								pluginConfig.setAutoclose(true);
								operators.add("between");
								iprodReportsGenerateReportFilterColumn.setOperators(operators);
								iprodReportsGenerateReportFilterColumn.setPlugin_config(pluginConfig);
							}
							else{
								iprodReportsGenerateReportFilterColumn.type = "string";
								operators.add("equal");
								operators.add("not_equal");
								operators.add("begins_with");
								operators.add("not_begins_with");
								operators.add("contains");
								operators.add("not_contains");
								operators.add("ends_with");
								operators.add("not_ends_with");
							}

							break;
						case "INT"     : iprodReportsGenerateReportFilterColumn.type = "integer";
						operators.add("equal");
						operators.add("not_equal");
						operators.add("less");
						operators.add("less_or_equal");
						operators.add("greater");
						operators.add("greater_or_equal");
						operators.add("between");
						operators.add("not_between");
						break;
						case "TINYINT" : iprodReportsGenerateReportFilterColumn.type = "integer";
						operators.add("equal");
						operators.add("not_equal");
						operators.add("less");
						operators.add("less_or_equal");
						operators.add("greater");
						operators.add("greater_or_equal");
						operators.add("between");
						operators.add("not_between");
						break;
						case "SMALLINT" : iprodReportsGenerateReportFilterColumn.type = "integer";
						operators.add("equal");
						operators.add("not_equal");
						operators.add("less");
						operators.add("less_or_equal");
						operators.add("greater");
						operators.add("greater_or_equal");
						operators.add("between");
						operators.add("not_between");
						break;
						case "FLOAT"   : iprodReportsGenerateReportFilterColumn.type = "double";
						operators.add("equal");
						operators.add("not_equal");
						operators.add("less");
						operators.add("less_or_equal");
						operators.add("greater");
						operators.add("greater_or_equal");
						operators.add("between");
						operators.add("not_between");
						break;
						case "DATE"    : iprodReportsGenerateReportFilterColumn.type = "date";
						iprodReportsGenerateReportFilterColumn.plugin = "datepicker";
						operators.add("equal");
						operators.add("not_equal");
						operators.add("between");
						break;
						case "TIME"    : iprodReportsGenerateReportFilterColumn.type = "time";
						iprodReportsGenerateReportFilterColumn.plugin = "timepicker";
						IprodReportsGenerateReportFilterDatePickerPluginConfig pluginConfig1 = new IprodReportsGenerateReportFilterDatePickerPluginConfig();
						pluginConfig1.setTodayBtn("linked");
						pluginConfig1.setTodayHighlight(true);
						pluginConfig1.setFormat("hh:mm:ss");
						pluginConfig1.setAutoclose(true);
						operators.add("equal");
						operators.add("not_equal");
						operators.add("between");
						break;
						case "DATETIME": iprodReportsGenerateReportFilterColumn.type = "datetime";
						iprodReportsGenerateReportFilterColumn.plugin = "datetimepicker";
						IprodReportsGenerateReportFilterDatePickerPluginConfig pluginConfig = new IprodReportsGenerateReportFilterDatePickerPluginConfig();
						pluginConfig.setTodayBtn("linked");
						pluginConfig.setTodayHighlight(true);
						pluginConfig.setFormat("yyyy/mm/dd hh:mm:ss");
						pluginConfig.setAutoclose(true);
						operators.add("equal");
						operators.add("not_equal");
						operators.add("between");
						iprodReportsGenerateReportFilterColumn.setOperators(operators);
						iprodReportsGenerateReportFilterColumn.setPlugin_config(pluginConfig);
						break;
						case "BOOLEAN" : iprodReportsGenerateReportFilterColumn.type = "boolean";
						operators.add("equal");
						operators.add("not_equal");
						break;
						default : iprodReportsGenerateReportFilterColumn.type = "integer";
						operators.add("equal");
						operators.add("not_equal");
						operators.add("less");
						operators.add("less_or_equal");
						operators.add("greater");
						operators.add("greater_or_equal");
						operators.add("between");
						operators.add("not_between");
						break;
						}
						iprodReportsGenerateReportFilterColumnList.add(iprodReportsGenerateReportFilterColumn);
					} 	        	   		
					outputJSONData = gs.toJson(iprodReportsGenerateReportFilterColumnList);
				}
			}

		}			
		catch(IncorrectResultSizeDataAccessException rsde){
			log.error("IncorrectResultSizeDataAccessException : "+rsde.getMessage());
			rsde.printStackTrace();
		}
		catch(NullPointerException ne){
			log.error("NullPointerException");
			ne.printStackTrace();

		}
		catch(SQLException e1){ 
			setIsSQLQueryValidCheckMessage("SQLException : "+e1.getMessage());
			log.error("DB connection closed due to error.");
		}
		catch(Exception e2){ 
			setIsSQLQueryValidCheckMessage("General Exception : "+e2.getMessage());
		}
		finally{

			dc.closeConnection(con);               

		}	

		return SUCCESS;
	}

	public String validateSelectedReportTemplateDetails(){
		log.info("IprodReportsSqlQueryMasterAction.validateSelectedReportTemplateDetails()");
		List<Map<String,Object>> iprodReportsViewReportTemplateDetailsListL=new ArrayList<Map<String,Object>>();
		Gson gs=new GsonBuilder().serializeNulls().create();
		IprodReportsSQLDataset iprodReportsSQLDatasetPJObj =gs.fromJson(inputJSONData, IprodReportsSQLDataset.class);
		rtemplateId=iprodReportsSQLDatasetPJObj.getRtemplateId();

		try{
			IprodReportsViewReportTemplateDetailsDao iprodReportsViewReportTemplateDetailsDao = DaoFactory.createIprodReportsViewReportTemplateDetailsDao();
			iprodReportsViewReportTemplateDetailsListL=iprodReportsViewReportTemplateDetailsDao.findReportQueryTableColumnAndValues(this.rtemplateId);
			outputJSONData=gs.toJson(iprodReportsViewReportTemplateDetailsListL);
		}
		catch(IncorrectResultSizeDataAccessException rsde){
			log.error("IncorrectResultSizeDataAccessException "+rsde.getMessage());
			rsde.printStackTrace();
		}
		catch(NullPointerException ne){
			log.error("NullPointerException in getSelectedReportTemplateDetails method ");
			ne.printStackTrace();

		}
		catch(Exception e){
			log.error("Exception");
			e.printStackTrace();
		}
		return SUCCESS;

	}



	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

	public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	public String getInputJSONData() {
		return inputJSONData;
	}

	public void setInputJSONData(String inputJSONData) {
		this.inputJSONData = inputJSONData;
	}

	public String getOutputJSONData() {
		return outputJSONData;
	}

	public void setOutputJSONData(String outputJSONData) {
		this.outputJSONData = outputJSONData;
	}

	public String getDataFieldName() {
		return dataFieldName;
	}

	public void setDataFieldName(String dataFieldName) {
		this.dataFieldName = dataFieldName;
	}

	public String getIsSQLQueryValidCheckMessage() {
		return isSQLQueryValidCheckMessage;
	}

	public void setIsSQLQueryValidCheckMessage(String isSQLQueryValidCheckMessage) {
		this.isSQLQueryValidCheckMessage = isSQLQueryValidCheckMessage;
	}

	public Integer getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(Integer datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getJsonSQLColumnDataType() {
		return jsonSQLColumnDataType;
	}

	public void setJsonSQLColumnDataType(String jsonSQLColumnDataType) {
		this.jsonSQLColumnDataType = jsonSQLColumnDataType;
	}

	public Integer getRtemplateId() {
		return rtemplateId;
	}

	public void setRtemplateId(Integer rtemplateId) {
		this.rtemplateId = rtemplateId;
	}

	public String getQueryColumnName() {
		return queryColumnName;
	}

	public void setQueryColumnName(String queryColumnName) {
		this.queryColumnName = queryColumnName;
	}








}