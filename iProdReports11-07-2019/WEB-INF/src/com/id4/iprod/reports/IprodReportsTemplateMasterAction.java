package com.id4.iprod.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.id4.iprod.reports.dao.IprodReportsTemplateMasterDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportTemplateDetailsDao;
import com.id4.iprod.reports.dao.IprodReportsTemplateMasterDao;
import com.id4.iprod.reports.dto.IprodReportsTemplateMaster;
import com.id4.iprod.reports.dto.IprodReportsTemplateMasterPk;
import com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetails;
import com.id4.iprod.reports.dto.IprodReportsTemplateMaster;
import com.id4.iprod.reports.exceptions.IprodReportsTemplateMasterDaoException;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportTemplateDetailsDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodReportsTemplateMasterAction extends ActionSupport implements IprodModuleAuthorization{

	static Logger log = Logger.getLogger(IprodReportsTemplateMasterAction.class);
	/** 
	 * This attribute maps to the column DATASOURCE_ID in the iprod_reports_datasource_instance table.
	 */
	protected int datasourceId;

	/** 
	 * This attribute maps to the column RDBMS_ID in the iprod_reports_datasource_instance table.
	 */
	protected int rdbmsId;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_URL in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsJdbcDriverUrl;

	/** 
	 * This attribute maps to the column RDBMS_IS_WINDOWS_AUTH in the iprod_reports_datasource_instance table.
	 */
	protected int rdbmsIsWindowsAuth;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsIsWindowsAuth is null.
	 */
	protected boolean rdbmsIsWindowsAuthNull = true;

	/** 
	 * This attribute maps to the column RDBMS_DATABASE_NAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsDatabaseName;

	/** 
	 * This attribute maps to the column RDBMS_INSTANCE_NAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsInstanceName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_USERNAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerUsername;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PASSWORD in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerPassword;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_MACHINE_NAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerMachineName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PORT in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerPort;

	/** 
	 * This attribute maps to the column DATASOURCE_CDATETIME in the iprod_reports_datasource_instance table.
	 */
	protected String datasourceCdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_MDATETIME in the iprod_reports_datasource_instance table.
	 */
	protected String datasourceMdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_IS_DELETED in the iprod_reports_datasource_instance table.
	 */
	protected short datasourceIsDeleted;
	
	
	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprod_reports_template_master table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column RTEMPLATE_NAME in the iprod_reports_template_master table.
	 */
	protected String rtemplateName;

	/** 
	 * This attribute maps to the column RTEMPLATE_TITLE in the iprod_reports_template_master table.
	 */
	protected String rtemplateTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SUB_TITLE in the iprod_reports_template_master table.
	 */
	protected String rtemplateSubTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprod_reports_template_master table.
	 */
	protected String rtemplateSqlquery;

	/** 
	 * This attribute maps to the column RTEMPLATE_BIRT_RPT in the iprod_reports_template_master table.
	 */
	protected String rtemplateBirtRpt;

	/** 
	 * This attribute maps to the column RTEMPLATE_CDATETIME in the iprod_reports_template_master table.
	 */
	protected String rtemplateCdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_MDATETIME in the iprod_reports_template_master table.
	 */
	protected String rtemplateMdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_ALLOW_OPERATOR_ACCESS in the iprod_reports_template_master table.
	 */
	protected short rtemplateAllowOperatorAccess;

	
	public String streamExcelTemplatedata =null;
	
	public String streamPDFTemplatedata= null;
	
    public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();
	
	public List<IprodReportsViewReportTemplateDetails> iprodReportsViewReportTemplateDetailsListG = new ArrayList<IprodReportsViewReportTemplateDetails>();

	public String addReportTemplateDetails()
	{
	  log.info("IprodReportsTemplateMasterAction :: addReportTemplateDetails");
	 // log.info("streamExcelTemplatedata :" + streamExcelTemplatedata);
	  //log.info("streamPDFTemplatedata :" + streamPDFTemplatedata);
		try{
			Map<String, Object> session = ActionContext.getContext().getSession();
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat ("dd MMM yyyy HH:mm:ss:mm");
			String currentDateTime = ft.format(dNow);
			IprodReportsTemplateMasterDao iprodReportsTemplateMasterDao = DaoFactory.createIprodReportsTemplateMasterDao();
			IprodReportsTemplateMaster iprodReportsTemplateMasterDto = new IprodReportsTemplateMaster();
			iprodReportsTemplateMasterDto.setRtemplateName(this.rtemplateName);
			iprodReportsTemplateMasterDto.setRtemplateTitle(this.rtemplateTitle);
			iprodReportsTemplateMasterDto.setRtemplateSubTitle(this.rtemplateSubTitle);
			iprodReportsTemplateMasterDto.setRtemplateSqlquery(this.rtemplateSqlquery);
			iprodReportsTemplateMasterDto.setRtemplateBirtRpt(this.rtemplateName+".rptdesign");
			iprodReportsTemplateMasterDto.setRtemplateExcelDataStorage(streamExcelTemplatedata);
			iprodReportsTemplateMasterDto.setRtemplatePdfDataStorage(streamPDFTemplatedata);
			iprodReportsTemplateMasterDto.setRtemplateCdatetime(currentDateTime);
			iprodReportsTemplateMasterDto.setRtemplateMdatetime(currentDateTime);
			iprodReportsTemplateMasterDto.setDatasourceId(this.datasourceId);
			iprodReportsTemplateMasterDto.setRtemplateIsDeleted((short)0);

			IprodReportsTemplateMasterPk iprodReportsTemplateMasterPk = new IprodReportsTemplateMasterPk();
			iprodReportsTemplateMasterPk = iprodReportsTemplateMasterDao.insert(iprodReportsTemplateMasterDto);
			
			return SUCCESS;
			
		}catch (Exception e){
			log.error("Exception :: " + e.getMessage() +" occured in addReportTemplateDetails.");
		}
		return SUCCESS;
		
	}
	
	 public String findAllReportsTemplate(){
			log.info("IprodReportsTemplateMasterAction :: findAllReportsTemplate()");
			Map session = ActionContext.getContext().getSession();
			if(session.get("IprodUserId")!= null){
				IprodReportsViewReportTemplateDetailsDao iprodReportsViewReportTemplateDetailsDao = DaoFactory.createIprodReportsViewReportTemplateDetailsDao();
				List<IprodReportsViewReportTemplateDetails> iprodReportsViewReportTemplateDetailsListL = new ArrayList<IprodReportsViewReportTemplateDetails>();
		
			 try{
				 iprodReportsViewReportTemplateDetailsListL =  iprodReportsViewReportTemplateDetailsDao.findWhereRtemplateIsDeletedEquals((short)0); 	
			  if(iprodReportsViewReportTemplateDetailsListL.size()>0){
				  iprodReportsViewReportTemplateDetailsListG = iprodReportsViewReportTemplateDetailsListL;
				  //log.info("iprodReportsViewReportTemplateDetailsListG ::"+iprodReportsViewReportTemplateDetailsListG);
			  }
			  else{
			   log.info("Template List is Empty ");
			   addActionError("Currently there are no templates to be displayed");
			  }
			  return SUCCESS;
			  }catch(IprodReportsViewReportTemplateDetailsDaoException e){
			   log.error("findAllReportsTemplate :: Exception :: " + e.getMessage());
			   return ERROR;
			 }
	    	}else{
	    		log.error("findAllReportsTemplate :: Not logeed in.");
	    		addActionError("Sorry!! You have to Login first");
		    	return "login";
	    	}    
			 
		 }

		/** 
		 * This function deletes the selected report template from the selectedDatabase.
		 * Basically it marks IS_DELETED AS 1 and does not permanently delete the template.
		 */
		public String deleteTemplates(){
			log.info("IprodReportsTemplateMasterAction : deleteTemplate()");
			IprodReportsTemplateMasterDao templateDao = DaoFactory.createIprodReportsTemplateMasterDao();
			IprodReportsTemplateMaster iprodReportsTemplateL = new IprodReportsTemplateMaster();
			List<IprodReportsTemplateMaster> templateListL = new ArrayList<IprodReportsTemplateMaster>();
			 
			try{
				templateListL = templateDao.findWhereRtemplateIdEquals(this.rtemplateId);
				IprodReportsTemplateMasterPk templatePk = new IprodReportsTemplateMasterPk();
				templatePk.setRtemplateId(this.rtemplateId);
				iprodReportsTemplateL.setRtemplateId(this.rtemplateId);
				iprodReportsTemplateL.setRtemplateSqlquery(templateListL.get(0).getRtemplateSqlquery());
				iprodReportsTemplateL.setRtemplateTitle(templateListL.get(0).getRtemplateTitle());
				iprodReportsTemplateL.setRtemplateSubTitle(templateListL.get(0).getRtemplateSubTitle());
				iprodReportsTemplateL.setRtemplateName(templateListL.get(0).getRtemplateName());
				iprodReportsTemplateL.setRtemplateBirtRpt(templateListL.get(0).getRtemplateName());
				iprodReportsTemplateL.setRtemplateIsDeleted((short) 1);
				templateDao.update(templatePk, iprodReportsTemplateL);
				log.info("User id " + this.rtemplateId	+ " has been deleted");
	    	 }catch (IprodReportsTemplateMasterDaoException e) {
	    		 addActionError("IprodReportsTemplatesDaoException"+e.getMessage());
			 return ERROR;
			}
	    	 addActionMessage("Template " + this.rtemplateId	+ " has been deleted");	
				return SUCCESS;
		}

		/**
		 * Method 'getDatasourceId'
		 * 
		 * @return int
		 */
		public int getDatasourceId()
		{
			return datasourceId;
		}

		/**
		 * Method 'setDatasourceId'
		 * 
		 * @param datasourceId
		 */
		public void setDatasourceId(int datasourceId)
		{
			this.datasourceId = datasourceId;
		}

		/**
		 * Method 'getRdbmsId'
		 * 
		 * @return int
		 */
		public int getRdbmsId()
		{
			return rdbmsId;
		}

		/**
		 * Method 'setRdbmsId'
		 * 
		 * @param rdbmsId
		 */
		public void setRdbmsId(int rdbmsId)
		{
			this.rdbmsId = rdbmsId;
		}

		/**
		 * Method 'getRdbmsJdbcDriverUrl'
		 * 
		 * @return String
		 */
		public String getRdbmsJdbcDriverUrl()
		{
			return rdbmsJdbcDriverUrl;
		}

		/**
		 * Method 'setRdbmsJdbcDriverUrl'
		 * 
		 * @param rdbmsJdbcDriverUrl
		 */
		public void setRdbmsJdbcDriverUrl(String rdbmsJdbcDriverUrl)
		{
			this.rdbmsJdbcDriverUrl = rdbmsJdbcDriverUrl;
		}

		/**
		 * Method 'getRdbmsIsWindowsAuth'
		 * 
		 * @return int
		 */
		public int getRdbmsIsWindowsAuth()
		{
			return rdbmsIsWindowsAuth;
		}

		/**
		 * Method 'setRdbmsIsWindowsAuth'
		 * 
		 * @param rdbmsIsWindowsAuth
		 */
		public void setRdbmsIsWindowsAuth(int rdbmsIsWindowsAuth)
		{
			this.rdbmsIsWindowsAuth = rdbmsIsWindowsAuth;
			this.rdbmsIsWindowsAuthNull = false;
		}

		/**
		 * Method 'setRdbmsIsWindowsAuthNull'
		 * 
		 * @param value
		 */
		public void setRdbmsIsWindowsAuthNull(boolean value)
		{
			this.rdbmsIsWindowsAuthNull = value;
		}

		/**
		 * Method 'isRdbmsIsWindowsAuthNull'
		 * 
		 * @return boolean
		 */
		public boolean isRdbmsIsWindowsAuthNull()
		{
			return rdbmsIsWindowsAuthNull;
		}

		/**
		 * Method 'getRdbmsDatabaseName'
		 * 
		 * @return String
		 */
		public String getRdbmsDatabaseName()
		{
			return rdbmsDatabaseName;
		}

		/**
		 * Method 'setRdbmsDatabaseName'
		 * 
		 * @param rdbmsDatabaseName
		 */
		public void setRdbmsDatabaseName(String rdbmsDatabaseName)
		{
			this.rdbmsDatabaseName = rdbmsDatabaseName;
		}

		/**
		 * Method 'getRdbmsInstanceName'
		 * 
		 * @return String
		 */
		public String getRdbmsInstanceName()
		{
			return rdbmsInstanceName;
		}

		/**
		 * Method 'setRdbmsInstanceName'
		 * 
		 * @param rdbmsInstanceName
		 */
		public void setRdbmsInstanceName(String rdbmsInstanceName)
		{
			this.rdbmsInstanceName = rdbmsInstanceName;
		}

		/**
		 * Method 'getRdbmsServerUsername'
		 * 
		 * @return String
		 */
		public String getRdbmsServerUsername()
		{
			return rdbmsServerUsername;
		}

		/**
		 * Method 'setRdbmsServerUsername'
		 * 
		 * @param rdbmsServerUsername
		 */
		public void setRdbmsServerUsername(String rdbmsServerUsername)
		{
			this.rdbmsServerUsername = rdbmsServerUsername;
		}

		/**
		 * Method 'getRdbmsServerPassword'
		 * 
		 * @return String
		 */
		public String getRdbmsServerPassword()
		{
			return rdbmsServerPassword;
		}

		/**
		 * Method 'setRdbmsServerPassword'
		 * 
		 * @param rdbmsServerPassword
		 */
		public void setRdbmsServerPassword(String rdbmsServerPassword)
		{
			this.rdbmsServerPassword = rdbmsServerPassword;
		}

		/**
		 * Method 'getRdbmsServerMachineName'
		 * 
		 * @return String
		 */
		public String getRdbmsServerMachineName()
		{
			return rdbmsServerMachineName;
		}

		/**
		 * Method 'setRdbmsServerMachineName'
		 * 
		 * @param rdbmsServerMachineName
		 */
		public void setRdbmsServerMachineName(String rdbmsServerMachineName)
		{
			this.rdbmsServerMachineName = rdbmsServerMachineName;
		}

		/**
		 * Method 'getRdbmsServerPort'
		 * 
		 * @return String
		 */
		public String getRdbmsServerPort()
		{
			return rdbmsServerPort;
		}

		/**
		 * Method 'setRdbmsServerPort'
		 * 
		 * @param rdbmsServerPort
		 */
		public void setRdbmsServerPort(String rdbmsServerPort)
		{
			this.rdbmsServerPort = rdbmsServerPort;
		}

		/**
		 * Method 'getDatasourceCdatetime'
		 * 
		 * @return String
		 */
		public String getDatasourceCdatetime()
		{
			return datasourceCdatetime;
		}

		/**
		 * Method 'setDatasourceCdatetime'
		 * 
		 * @param datasourceCdatetime
		 */
		public void setDatasourceCdatetime(String datasourceCdatetime)
		{
			this.datasourceCdatetime = datasourceCdatetime;
		}

		/**
		 * Method 'getDatasourceMdatetime'
		 * 
		 * @return String
		 */
		public String getDatasourceMdatetime()
		{
			return datasourceMdatetime;
		}

		/**
		 * Method 'setDatasourceMdatetime'
		 * 
		 * @param datasourceMdatetime
		 */
		public void setDatasourceMdatetime(String datasourceMdatetime)
		{
			this.datasourceMdatetime = datasourceMdatetime;
		}

		/**
		 * Method 'getDatasourceIsDeleted'
		 * 
		 * @return short
		 */
		public short getDatasourceIsDeleted()
		{
			return datasourceIsDeleted;
		}

		/**
		 * Method 'setDatasourceIsDeleted'
		 * 
		 * @param datasourceIsDeleted
		 */
		public void setDatasourceIsDeleted(short datasourceIsDeleted)
		{
			this.datasourceIsDeleted = datasourceIsDeleted;
		}
		
		/**
		 * Method 'getRtemplateId'
		 * 
		 * @return int
		 */
		public int getRtemplateId()
		{
			return rtemplateId;
		}

		/**
		 * Method 'setRtemplateId'
		 * 
		 * @param rtemplateId
		 */
		public void setRtemplateId(int rtemplateId)
		{
			this.rtemplateId = rtemplateId;
		}

		/**
		 * Method 'getRtemplateName'
		 * 
		 * @return String
		 */
		public String getRtemplateName()
		{
			return rtemplateName;
		}

		/**
		 * Method 'setRtemplateName'
		 * 
		 * @param rtemplateName
		 */
		public void setRtemplateName(String rtemplateName)
		{
			this.rtemplateName = rtemplateName;
		}

		/**
		 * Method 'getRtemplateTitle'
		 * 
		 * @return String
		 */
		public String getRtemplateTitle()
		{
			return rtemplateTitle;
		}

		/**
		 * Method 'setRtemplateTitle'
		 * 
		 * @param rtemplateTitle
		 */
		public void setRtemplateTitle(String rtemplateTitle)
		{
			this.rtemplateTitle = rtemplateTitle;
		}

		/**
		 * Method 'getRtemplateSubTitle'
		 * 
		 * @return String
		 */
		public String getRtemplateSubTitle()
		{
			return rtemplateSubTitle;
		}

		/**
		 * Method 'setRtemplateSubTitle'
		 * 
		 * @param rtemplateSubTitle
		 */
		public void setRtemplateSubTitle(String rtemplateSubTitle)
		{
			this.rtemplateSubTitle = rtemplateSubTitle;
		}

		/**
		 * Method 'getRtemplateSqlquery'
		 * 
		 * @return String
		 */
		public String getRtemplateSqlquery()
		{
			return rtemplateSqlquery;
		}

		/**
		 * Method 'setRtemplateSqlquery'
		 * 
		 * @param rtemplateSqlquery
		 */
		public void setRtemplateSqlquery(String rtemplateSqlquery)
		{
			this.rtemplateSqlquery = rtemplateSqlquery;
		}

		/**
		 * Method 'getRtemplateBirtRpt'
		 * 
		 * @return String
		 */
		public String getRtemplateBirtRpt()
		{
			return rtemplateBirtRpt;
		}

		/**
		 * Method 'setRtemplateBirtRpt'
		 * 
		 * @param rtemplateBirtRpt
		 */
		public void setRtemplateBirtRpt(String rtemplateBirtRpt)
		{
			this.rtemplateBirtRpt = rtemplateBirtRpt;
		}

		/**
		 * Method 'getRtemplateCdatetime'
		 * 
		 * @return String
		 */
		public String getRtemplateCdatetime()
		{
			return rtemplateCdatetime;
		}

		/**
		 * Method 'setRtemplateCdatetime'
		 * 
		 * @param rtemplateCdatetime
		 */
		public void setRtemplateCdatetime(String rtemplateCdatetime)
		{
			this.rtemplateCdatetime = rtemplateCdatetime;
		}

		/**
		 * Method 'getRtemplateMdatetime'
		 * 
		 * @return String
		 */
		public String getRtemplateMdatetime()
		{
			return rtemplateMdatetime;
		}

		/**
		 * Method 'setRtemplateMdatetime'
		 * 
		 * @param rtemplateMdatetime
		 */
		public void setRtemplateMdatetime(String rtemplateMdatetime)
		{
			this.rtemplateMdatetime = rtemplateMdatetime;
		}

		public String getstreamExcelTemplatedata() {
			return streamExcelTemplatedata;
		}

		public void setstreamExcelTemplatedata(String streamExcelTemplatedata) {
			this.streamExcelTemplatedata = streamExcelTemplatedata;
		}

		public String getStreamPDFTemplatedata() {
			return streamPDFTemplatedata;
		}

		public void setStreamPDFTemplatedata(String streamPDFTemplatedata) {
			this.streamPDFTemplatedata = streamPDFTemplatedata;
		}

		/**
		 * Method 'getRtemplateAllowOperatorAccess'
		 * 
		 * @return short
		 */
		public short getRtemplateAllowOperatorAccess()
		{
			return rtemplateAllowOperatorAccess;
		}

		/**
		 * Method 'setRtemplateAllowOperatorAccess'
		 * 
		 * @param rtemplateAllowOperatorAccess
		 */
		public void setRtemplateAllowOperatorAccess(short rtemplateAllowOperatorAccess)
		{
			this.rtemplateAllowOperatorAccess = rtemplateAllowOperatorAccess;
		}

	
		public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
				return hashMapCheckRoleModuleOperationListG;
		}
	
		public void setHashMapCheckRoleModuleOperationListG(
				HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
			this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
		}


}
