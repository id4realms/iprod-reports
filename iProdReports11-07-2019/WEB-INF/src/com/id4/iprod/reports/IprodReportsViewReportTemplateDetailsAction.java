package com.id4.iprod.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.Consts;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.reports.dao.IprodReportsViewReportTemplateDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetails;
import com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetails;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportTemplateDetailsDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodReportsViewReportTemplateDetailsAction extends ActionSupport{
	
	static Logger log = Logger.getLogger(IprodReportsTemplateMasterAction.class);
	
	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprod_reports_view_report_template_details table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column RTEMPLATE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateName;

	/** 
	 * This attribute maps to the column RTEMPLATE_TITLE in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SUB_TITLE in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateSubTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateSqlquery;

	/** 
	 * This attribute maps to the column RTEMPLATE_BIRT_RPT in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateBirtRpt;

	/** 
	 * This attribute maps to the column RTEMPLATE_CDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateCdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_MDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rtemplateMdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_ALLOW_OPERATOR_ACCESS in the iprod_reports_view_report_template_details table.
	 */
	protected short rtemplateAllowOperatorAccess;

	/** 
	 * This attribute maps to the column RDBMS_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsName;

	/** 
	 * This attribute maps to the column RDBMS_SUB_VERSION_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsSubVersionName;

	/** 
	 * This attribute maps to the column RDBMS_DESC in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsDesc;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_CLASS in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsJdbcDriverClass;

	/** 
	 * This attribute maps to the column RDBMS_DEFAULT_PORT in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsDefaultPort;

	/** 
	 * This attribute maps to the column RDBMS_CDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsCdatetime;

	/** 
	 * This attribute maps to the column RDBMS_MDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsMdatetime;

	/** 
	 * This attribute maps to the column RDBMS_IS_DELETED in the iprod_reports_view_report_template_details table.
	 */
	protected short rdbmsIsDeleted;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_URL in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsJdbcDriverUrl;

	/** 
	 * This attribute maps to the column RDBMS_IS_WINDOWS_AUTH in the iprod_reports_view_report_template_details table.
	 */
	protected int rdbmsIsWindowsAuth;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsIsWindowsAuth is null.
	 */
	protected boolean rdbmsIsWindowsAuthNull = true;

	/** 
	 * This attribute maps to the column RDBMS_DATABASE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsDatabaseName;

	/** 
	 * This attribute maps to the column RDBMS_INSTANCE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsInstanceName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_USERNAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerUsername;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PASSWORD in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerPassword;
	
	/** 
	 * This attribute maps to the column RDBMS_SERVER_MACHINE_NAME in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerMachineName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PORT in the iprod_reports_view_report_template_details table.
	 */
	protected String rdbmsServerPort;

	/** 
	 * This attribute maps to the column DATASOURCE_ID in the iprod_reports_view_report_template_details table.
	 */
	protected int datasourceId;

	/** 
	 * This attribute maps to the column RDBMS_ID in the iprod_reports_view_report_template_details table.
	 */
	protected int rdbmsId;

	/** 
	 * This attribute maps to the column DATASOURCE_CDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String datasourceCdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_MDATETIME in the iprod_reports_view_report_template_details table.
	 */
	protected String datasourceMdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_IS_DELETED in the iprod_reports_view_report_template_details table.
	 */
	protected short datasourceIsDeleted;

	
	/* Public List Here*/
	public List<IprodReportsViewReportTemplateDetails> allTemplateDsListG = new ArrayList<IprodReportsViewReportTemplateDetails>();
	
	public String getAllTemplatesList(){
		log.info("IprodReportsViewReportTemplateDetailsAction : getAllTemplatesList()");
		Map<String, Object> session = ActionContext.getContext().getSession();
	    if(session.get(Consts.IPRODUSERID)!= null){
	    		IprodReportsViewReportTemplateDetailsDao viewTemplateDsInstanceDetailsDao = DaoFactory.createIprodReportsViewReportTemplateDetailsDao();
				List<IprodReportsViewReportTemplateDetails> allTemplateDsListL = new ArrayList<IprodReportsViewReportTemplateDetails>();
				try{
					allTemplateDsListL = viewTemplateDsInstanceDetailsDao.findAll();
					if(allTemplateDsListL.size()>0){
						allTemplateDsListG = allTemplateDsListL;
						log.info("Database JDBC List is : "+allTemplateDsListL);
					}
					else{
						log.info("Currently there are no Database JDBC connections");
					}
					
					return SUCCESS;
				}catch(IprodReportsViewReportTemplateDetailsDaoException e){
					log.info(e.getMessage());
					return ERROR;
				}
	    	}else{
			    	addActionError("Sorry!! You have to Login first");
			    	return "login";
			    }
		
	}


	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getRtemplateName'
	 * 
	 * @return String
	 */
	public String getRtemplateName()
	{
		return rtemplateName;
	}

	/**
	 * Method 'setRtemplateName'
	 * 
	 * @param rtemplateName
	 */
	public void setRtemplateName(String rtemplateName)
	{
		this.rtemplateName = rtemplateName;
	}

	/**
	 * Method 'getRtemplateTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateTitle()
	{
		return rtemplateTitle;
	}

	/**
	 * Method 'setRtemplateTitle'
	 * 
	 * @param rtemplateTitle
	 */
	public void setRtemplateTitle(String rtemplateTitle)
	{
		this.rtemplateTitle = rtemplateTitle;
	}

	/**
	 * Method 'getRtemplateSubTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateSubTitle()
	{
		return rtemplateSubTitle;
	}

	/**
	 * Method 'setRtemplateSubTitle'
	 * 
	 * @param rtemplateSubTitle
	 */
	public void setRtemplateSubTitle(String rtemplateSubTitle)
	{
		this.rtemplateSubTitle = rtemplateSubTitle;
	}

	/**
	 * Method 'getRtemplateSqlquery'
	 * 
	 * @return String
	 */
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

	/**
	 * Method 'setRtemplateSqlquery'
	 * 
	 * @param rtemplateSqlquery
	 */
	public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	/**
	 * Method 'getRtemplateBirtRpt'
	 * 
	 * @return String
	 */
	public String getRtemplateBirtRpt()
	{
		return rtemplateBirtRpt;
	}

	/**
	 * Method 'setRtemplateBirtRpt'
	 * 
	 * @param rtemplateBirtRpt
	 */
	public void setRtemplateBirtRpt(String rtemplateBirtRpt)
	{
		this.rtemplateBirtRpt = rtemplateBirtRpt;
	}

	/**
	 * Method 'getRtemplateCdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateCdatetime()
	{
		return rtemplateCdatetime;
	}

	/**
	 * Method 'setRtemplateCdatetime'
	 * 
	 * @param rtemplateCdatetime
	 */
	public void setRtemplateCdatetime(String rtemplateCdatetime)
	{
		this.rtemplateCdatetime = rtemplateCdatetime;
	}

	/**
	 * Method 'getRtemplateMdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateMdatetime()
	{
		return rtemplateMdatetime;
	}

	/**
	 * Method 'setRtemplateMdatetime'
	 * 
	 * @param rtemplateMdatetime
	 */
	public void setRtemplateMdatetime(String rtemplateMdatetime)
	{
		this.rtemplateMdatetime = rtemplateMdatetime;
	}

	/**
	 * Method 'getRtemplateAllowOperatorAccess'
	 * 
	 * @return short
	 */
	public short getRtemplateAllowOperatorAccess()
	{
		return rtemplateAllowOperatorAccess;
	}

	/**
	 * Method 'setRtemplateAllowOperatorAccess'
	 * 
	 * @param rtemplateAllowOperatorAccess
	 */
	public void setRtemplateAllowOperatorAccess(short rtemplateAllowOperatorAccess)
	{
		this.rtemplateAllowOperatorAccess = rtemplateAllowOperatorAccess;
	}


	/**
	 * Method 'getRdbmsName'
	 * 
	 * @return String
	 */
	public String getRdbmsName()
	{
		return rdbmsName;
	}

	/**
	 * Method 'setRdbmsName'
	 * 
	 * @param rdbmsName
	 */
	public void setRdbmsName(String rdbmsName)
	{
		this.rdbmsName = rdbmsName;
	}

	/**
	 * Method 'getRdbmsSubVersionName'
	 * 
	 * @return String
	 */
	public String getRdbmsSubVersionName()
	{
		return rdbmsSubVersionName;
	}

	/**
	 * Method 'setRdbmsSubVersionName'
	 * 
	 * @param rdbmsSubVersionName
	 */
	public void setRdbmsSubVersionName(String rdbmsSubVersionName)
	{
		this.rdbmsSubVersionName = rdbmsSubVersionName;
	}

	/**
	 * Method 'getRdbmsDesc'
	 * 
	 * @return String
	 */
	public String getRdbmsDesc()
	{
		return rdbmsDesc;
	}

	/**
	 * Method 'setRdbmsDesc'
	 * 
	 * @param rdbmsDesc
	 */
	public void setRdbmsDesc(String rdbmsDesc)
	{
		this.rdbmsDesc = rdbmsDesc;
	}

	/**
	 * Method 'getRdbmsJdbcDriverClass'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverClass()
	{
		return rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'setRdbmsJdbcDriverClass'
	 * 
	 * @param rdbmsJdbcDriverClass
	 */
	public void setRdbmsJdbcDriverClass(String rdbmsJdbcDriverClass)
	{
		this.rdbmsJdbcDriverClass = rdbmsJdbcDriverClass;
	}

	/**
	 * Method 'getRdbmsDefaultPort'
	 * 
	 * @return String
	 */
	public String getRdbmsDefaultPort()
	{
		return rdbmsDefaultPort;
	}

	/**
	 * Method 'setRdbmsDefaultPort'
	 * 
	 * @param rdbmsDefaultPort
	 */
	public void setRdbmsDefaultPort(String rdbmsDefaultPort)
	{
		this.rdbmsDefaultPort = rdbmsDefaultPort;
	}

	/**
	 * Method 'getRdbmsCdatetime'
	 * 
	 * @return String
	 */
	public String getRdbmsCdatetime()
	{
		return rdbmsCdatetime;
	}

	/**
	 * Method 'setRdbmsCdatetime'
	 * 
	 * @param rdbmsCdatetime
	 */
	public void setRdbmsCdatetime(String rdbmsCdatetime)
	{
		this.rdbmsCdatetime = rdbmsCdatetime;
	}

	/**
	 * Method 'getRdbmsMdatetime'
	 * 
	 * @return String
	 */
	public String getRdbmsMdatetime()
	{
		return rdbmsMdatetime;
	}

	/**
	 * Method 'setRdbmsMdatetime'
	 * 
	 * @param rdbmsMdatetime
	 */
	public void setRdbmsMdatetime(String rdbmsMdatetime)
	{
		this.rdbmsMdatetime = rdbmsMdatetime;
	}

	/**
	 * Method 'getRdbmsIsDeleted'
	 * 
	 * @return short
	 */
	public short getRdbmsIsDeleted()
	{
		return rdbmsIsDeleted;
	}

	/**
	 * Method 'setRdbmsIsDeleted'
	 * 
	 * @param rdbmsIsDeleted
	 */
	public void setRdbmsIsDeleted(short rdbmsIsDeleted)
	{
		this.rdbmsIsDeleted = rdbmsIsDeleted;
	}

	/**
	 * Method 'getRdbmsJdbcDriverUrl'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverUrl()
	{
		return rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'setRdbmsJdbcDriverUrl'
	 * 
	 * @param rdbmsJdbcDriverUrl
	 */
	public void setRdbmsJdbcDriverUrl(String rdbmsJdbcDriverUrl)
	{
		this.rdbmsJdbcDriverUrl = rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'getRdbmsIsWindowsAuth'
	 * 
	 * @return int
	 */
	public int getRdbmsIsWindowsAuth()
	{
		return rdbmsIsWindowsAuth;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuth'
	 * 
	 * @param rdbmsIsWindowsAuth
	 */
	public void setRdbmsIsWindowsAuth(int rdbmsIsWindowsAuth)
	{
		this.rdbmsIsWindowsAuth = rdbmsIsWindowsAuth;
		this.rdbmsIsWindowsAuthNull = false;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuthNull'
	 * 
	 * @param value
	 */
	public void setRdbmsIsWindowsAuthNull(boolean value)
	{
		this.rdbmsIsWindowsAuthNull = value;
	}

	/**
	 * Method 'isRdbmsIsWindowsAuthNull'
	 * 
	 * @return boolean
	 */
	public boolean isRdbmsIsWindowsAuthNull()
	{
		return rdbmsIsWindowsAuthNull;
	}

	/**
	 * Method 'getRdbmsDatabaseName'
	 * 
	 * @return String
	 */
	public String getRdbmsDatabaseName()
	{
		return rdbmsDatabaseName;
	}

	/**
	 * Method 'setRdbmsDatabaseName'
	 * 
	 * @param rdbmsDatabaseName
	 */
	public void setRdbmsDatabaseName(String rdbmsDatabaseName)
	{
		this.rdbmsDatabaseName = rdbmsDatabaseName;
	}

	/**
	 * Method 'getRdbmsInstanceName'
	 * 
	 * @return String
	 */
	public String getRdbmsInstanceName()
	{
		return rdbmsInstanceName;
	}

	/**
	 * Method 'setRdbmsInstanceName'
	 * 
	 * @param rdbmsInstanceName
	 */
	public void setRdbmsInstanceName(String rdbmsInstanceName)
	{
		this.rdbmsInstanceName = rdbmsInstanceName;
	}

	/**
	 * Method 'getRdbmsServerUsername'
	 * 
	 * @return String
	 */
	public String getRdbmsServerUsername()
	{
		return rdbmsServerUsername;
	}

	/**
	 * Method 'setRdbmsServerUsername'
	 * 
	 * @param rdbmsServerUsername
	 */
	public void setRdbmsServerUsername(String rdbmsServerUsername)
	{
		this.rdbmsServerUsername = rdbmsServerUsername;
	}

	/**
	 * Method 'getRdbmsServerPassword'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPassword()
	{
		return rdbmsServerPassword;
	}

	/**
	 * Method 'setRdbmsServerPassword'
	 * 
	 * @param rdbmsServerPassword
	 */
	public void setRdbmsServerPassword(String rdbmsServerPassword)
	{
		this.rdbmsServerPassword = rdbmsServerPassword;
	}

	/**
	 * Method 'getRdbmsServerMachineName'
	 * 
	 * @return String
	 */
	public String getRdbmsServerMachineName()
	{
		return rdbmsServerMachineName;
	}

	/**
	 * Method 'setRdbmsServerMachineName'
	 * 
	 * @param rdbmsServerMachineName
	 */
	public void setRdbmsServerMachineName(String rdbmsServerMachineName)
	{
		this.rdbmsServerMachineName = rdbmsServerMachineName;
	}

	/**
	 * Method 'getRdbmsServerPort'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPort()
	{
		return rdbmsServerPort;
	}

	/**
	 * Method 'setRdbmsServerPort'
	 * 
	 * @param rdbmsServerPort
	 */
	public void setRdbmsServerPort(String rdbmsServerPort)
	{
		this.rdbmsServerPort = rdbmsServerPort;
	}

	/**
	 * Method 'getDatasourceId'
	 * 
	 * @return int
	 */
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'setDatasourceId'
	 * 
	 * @param datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
	}

	/**
	 * Method 'getRdbmsId'
	 * 
	 * @return int
	 */
	public int getRdbmsId()
	{
		return rdbmsId;
	}

	/**
	 * Method 'setRdbmsId'
	 * 
	 * @param rdbmsId
	 */
	public void setRdbmsId(int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
	}

	/**
	 * Method 'getDatasourceCdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceCdatetime()
	{
		return datasourceCdatetime;
	}

	/**
	 * Method 'setDatasourceCdatetime'
	 * 
	 * @param datasourceCdatetime
	 */
	public void setDatasourceCdatetime(String datasourceCdatetime)
	{
		this.datasourceCdatetime = datasourceCdatetime;
	}

	/**
	 * Method 'getDatasourceMdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceMdatetime()
	{
		return datasourceMdatetime;
	}

	/**
	 * Method 'setDatasourceMdatetime'
	 * 
	 * @param datasourceMdatetime
	 */
	public void setDatasourceMdatetime(String datasourceMdatetime)
	{
		this.datasourceMdatetime = datasourceMdatetime;
	}

	/**
	 * Method 'getDatasourceIsDeleted'
	 * 
	 * @return short
	 */
	public short getDatasourceIsDeleted()
	{
		return datasourceIsDeleted;
	}

	/**
	 * Method 'setDatasourceIsDeleted'
	 * 
	 * @param datasourceIsDeleted
	 */
	public void setDatasourceIsDeleted(short datasourceIsDeleted)
	{
		this.datasourceIsDeleted = datasourceIsDeleted;
	}
	
	/** Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.IprodReportsViewReportTemplateDetailsAction: " );
		ret.append( "rtemplateId=" + rtemplateId );
		ret.append( ", rtemplateName=" + rtemplateName );
		ret.append( ", rtemplateTitle=" + rtemplateTitle );
		ret.append( ", rtemplateSubTitle=" + rtemplateSubTitle );
		ret.append( ", rtemplateSqlquery=" + rtemplateSqlquery );
		ret.append( ", rtemplateBirtRpt=" + rtemplateBirtRpt );
		ret.append( ", rtemplateCdatetime=" + rtemplateCdatetime );
		ret.append( ", rtemplateMdatetime=" + rtemplateMdatetime );
		ret.append( ", rtemplateAllowOperatorAccess=" + rtemplateAllowOperatorAccess );
		ret.append( ", rdbmsName=" + rdbmsName );
		ret.append( ", rdbmsSubVersionName=" + rdbmsSubVersionName );
		ret.append( ", rdbmsDesc=" + rdbmsDesc );
		ret.append( ", rdbmsJdbcDriverClass=" + rdbmsJdbcDriverClass );
		ret.append( ", rdbmsDefaultPort=" + rdbmsDefaultPort );
		ret.append( ", rdbmsCdatetime=" + rdbmsCdatetime );
		ret.append( ", rdbmsMdatetime=" + rdbmsMdatetime );
		ret.append( ", rdbmsIsDeleted=" + rdbmsIsDeleted );
		ret.append( ", rdbmsJdbcDriverUrl=" + rdbmsJdbcDriverUrl );
		ret.append( ", rdbmsIsWindowsAuth=" + rdbmsIsWindowsAuth );
		ret.append( ", rdbmsDatabaseName=" + rdbmsDatabaseName );
		ret.append( ", rdbmsInstanceName=" + rdbmsInstanceName );
		ret.append( ", rdbmsServerUsername=" + rdbmsServerUsername );
		ret.append( ", rdbmsServerPassword=" + rdbmsServerPassword );
		ret.append( ", rdbmsServerMachineName=" + rdbmsServerMachineName );
		ret.append( ", rdbmsServerPort=" + rdbmsServerPort );
		ret.append( ", datasourceId=" + datasourceId );
		ret.append( ", rdbmsId=" + rdbmsId );
		ret.append( ", datasourceCdatetime=" + datasourceCdatetime );
		ret.append( ", datasourceMdatetime=" + datasourceMdatetime );
		ret.append( ", datasourceIsDeleted=" + datasourceIsDeleted );
		return ret.toString();
	}


}
