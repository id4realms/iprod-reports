package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsViewReportTemplateDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetails;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportTemplateDetailsDaoException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsViewReportTemplateDetailsDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsViewReportTemplateDetails>, IprodReportsViewReportTemplateDetailsDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodReportsViewReportTemplateDetails dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getRtemplateId(),dto.getRtemplateName(),dto.getRtemplateTitle(),dto.getRtemplateSubTitle(),dto.getRtemplateSqlquery(),dto.getRtemplateExcelDataStorage(),dto.getRtemplatePdfDataStorage(),dto.getRtemplateBirtRpt(),dto.getRtemplateCdatetime(),dto.getRtemplateMdatetime(),dto.getRtemplateIsDeleted(),dto.getDatasourceId(),dto.getDatasourceName(),dto.getRdbmsId(),dto.getRdbmsName(),dto.getRdbmsSubVersionName(),dto.getRdbmsDesc(),dto.getRdbmsJdbcDriverClass(),dto.getRdbmsDefaultPort(),dto.getRdbmsCdatetime(),dto.getRdbmsMdatetime(),dto.getRdbmsIsDeleted(),dto.getRdbmsJdbcDriverUrl(),dto.getRdbmsIsWindowsAuth(),dto.getRdbmsDatabaseName(),dto.getRdbmsInstanceName(),dto.getRdbmsServerUsername(),dto.getRdbmsServerPassword(),dto.getRdbmsServerMachineName(),dto.getRdbmsServerPort(),dto.getDatasourceCdatetime(),dto.getDatasourceMdatetime(),dto.getDatasourceIsDeleted()} );
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsViewReportTemplateDetails
	 */
	public IprodReportsViewReportTemplateDetails mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsViewReportTemplateDetails dto = new IprodReportsViewReportTemplateDetails();
		dto.setRtemplateId( rs.getInt( 1 ) );
		dto.setRtemplateName( rs.getString( 2 ) );
		dto.setRtemplateTitle( rs.getString( 3 ) );
		dto.setRtemplateSubTitle( rs.getString( 4 ) );
		dto.setRtemplateSqlquery( rs.getString( 5 ) );
		dto.setRtemplateExcelDataStorage( rs.getString( 6 ) );
		dto.setRtemplatePdfDataStorage( rs.getString( 7 ) );
		dto.setRtemplateBirtRpt( rs.getString( 8 ) );
		dto.setRtemplateCdatetime( rs.getString( 9 ) );
		dto.setRtemplateMdatetime( rs.getString( 10 ) );
		dto.setRtemplateIsDeleted( rs.getShort( 11 ) );
		dto.setDatasourceId( rs.getInt( 12 ) );
		dto.setDatasourceName( rs.getString( 13 ) );
		dto.setRdbmsId( rs.getInt( 14 ) );
		if (rs.wasNull()) {
			dto.setRdbmsIdNull( true );
		}
		
		dto.setRdbmsName( rs.getString( 15 ) );
		dto.setRdbmsSubVersionName( rs.getString( 16 ) );
		dto.setRdbmsDesc( rs.getString( 17 ) );
		dto.setRdbmsJdbcDriverClass( rs.getString( 18 ) );
		dto.setRdbmsDefaultPort( rs.getString( 19 ) );
		dto.setRdbmsCdatetime( rs.getString( 20 ) );
		dto.setRdbmsMdatetime( rs.getString( 21 ) );
		dto.setRdbmsIsDeleted( rs.getShort( 22 ) );
		if (rs.wasNull()) {
			dto.setRdbmsIsDeletedNull( true );
		}
		
		dto.setRdbmsJdbcDriverUrl( rs.getString( 23 ) );
		dto.setRdbmsIsWindowsAuth( rs.getInt( 24 ) );
		if (rs.wasNull()) {
			dto.setRdbmsIsWindowsAuthNull( true );
		}
		
		dto.setRdbmsDatabaseName( rs.getString( 25 ) );
		dto.setRdbmsInstanceName( rs.getString( 26 ) );
		dto.setRdbmsServerUsername( rs.getString( 27 ) );
		dto.setRdbmsServerPassword( rs.getString( 28 ) );
		dto.setRdbmsServerMachineName( rs.getString( 29 ) );
		dto.setRdbmsServerPort( rs.getString( 30 ) );
		dto.setDatasourceCdatetime( rs.getString( 31 ) );
		dto.setDatasourceMdatetime( rs.getString( 32 ) );
		dto.setDatasourceIsDeleted( rs.getShort( 33 ) );
		if (rs.wasNull()) {
			dto.setDatasourceIsDeletedNull( true );
		}
		
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_view_report_template_details";
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findAll() throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + "", this);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_ID = ? ORDER BY RTEMPLATE_ID", this,rtemplateId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_NAME = :rtemplateName'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateNameEquals(String rtemplateName) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_NAME = ? ORDER BY RTEMPLATE_NAME", this,rtemplateName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_TITLE = :rtemplateTitle'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateTitleEquals(String rtemplateTitle) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_TITLE = ? ORDER BY RTEMPLATE_TITLE", this,rtemplateTitle);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_SUB_TITLE = :rtemplateSubTitle'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateSubTitleEquals(String rtemplateSubTitle) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_SUB_TITLE = ? ORDER BY RTEMPLATE_SUB_TITLE", this,rtemplateSubTitle);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_SQLQUERY = :rtemplateSqlquery'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateSqlqueryEquals(String rtemplateSqlquery) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_SQLQUERY = ? ORDER BY RTEMPLATE_SQLQUERY", this,rtemplateSqlquery);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_EXCEL_DATA_STORAGE = :rtemplateExcelDataStorage'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateExcelDataStorageEquals(String rtemplateExcelDataStorage) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_EXCEL_DATA_STORAGE = ? ORDER BY RTEMPLATE_EXCEL_DATA_STORAGE", this,rtemplateExcelDataStorage);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_PDF_DATA_STORAGE = :rtemplatePdfDataStorage'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplatePdfDataStorageEquals(String rtemplatePdfDataStorage) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_PDF_DATA_STORAGE = ? ORDER BY RTEMPLATE_PDF_DATA_STORAGE", this,rtemplatePdfDataStorage);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_BIRT_RPT = :rtemplateBirtRpt'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateBirtRptEquals(String rtemplateBirtRpt) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_BIRT_RPT = ? ORDER BY RTEMPLATE_BIRT_RPT", this,rtemplateBirtRpt);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_CDATETIME = :rtemplateCdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateCdatetimeEquals(String rtemplateCdatetime) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_CDATETIME = ? ORDER BY RTEMPLATE_CDATETIME", this,rtemplateCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_MDATETIME = :rtemplateMdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateMdatetimeEquals(String rtemplateMdatetime) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_MDATETIME = ? ORDER BY RTEMPLATE_MDATETIME", this,rtemplateMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_IS_DELETED = :rtemplateIsDeleted'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateIsDeletedEquals(short rtemplateIsDeleted) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_IS_DELETED = ? ORDER BY RTEMPLATE_IS_DELETED", this,rtemplateIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_ID = ? ORDER BY DATASOURCE_ID", this,datasourceId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_NAME = ? ORDER BY DATASOURCE_NAME", this,datasourceName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_ID = ? ORDER BY RDBMS_ID", this,rdbmsId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_NAME = ? ORDER BY RDBMS_NAME", this,rdbmsName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SUB_VERSION_NAME = :rdbmsSubVersionName'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsSubVersionNameEquals(String rdbmsSubVersionName) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SUB_VERSION_NAME = ? ORDER BY RDBMS_SUB_VERSION_NAME", this,rdbmsSubVersionName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_DESC = :rdbmsDesc'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsDescEquals(String rdbmsDesc) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DESC = ? ORDER BY RDBMS_DESC", this,rdbmsDesc);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_JDBC_DRIVER_CLASS = :rdbmsJdbcDriverClass'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsJdbcDriverClassEquals(String rdbmsJdbcDriverClass) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_JDBC_DRIVER_CLASS = ? ORDER BY RDBMS_JDBC_DRIVER_CLASS", this,rdbmsJdbcDriverClass);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_DEFAULT_PORT = :rdbmsDefaultPort'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsDefaultPortEquals(String rdbmsDefaultPort) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DEFAULT_PORT = ? ORDER BY RDBMS_DEFAULT_PORT", this,rdbmsDefaultPort);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_CDATETIME = :rdbmsCdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsCdatetimeEquals(String rdbmsCdatetime) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_CDATETIME = ? ORDER BY RDBMS_CDATETIME", this,rdbmsCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_MDATETIME = :rdbmsMdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsMdatetimeEquals(String rdbmsMdatetime) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_MDATETIME = ? ORDER BY RDBMS_MDATETIME", this,rdbmsMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_IS_DELETED = :rdbmsIsDeleted'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsIsDeletedEquals(short rdbmsIsDeleted) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_IS_DELETED = ? ORDER BY RDBMS_IS_DELETED", this,rdbmsIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_JDBC_DRIVER_URL = :rdbmsJdbcDriverUrl'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsJdbcDriverUrlEquals(String rdbmsJdbcDriverUrl) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_JDBC_DRIVER_URL = ? ORDER BY RDBMS_JDBC_DRIVER_URL", this,rdbmsJdbcDriverUrl);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_IS_WINDOWS_AUTH = :rdbmsIsWindowsAuth'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsIsWindowsAuthEquals(int rdbmsIsWindowsAuth) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_IS_WINDOWS_AUTH = ? ORDER BY RDBMS_IS_WINDOWS_AUTH", this,rdbmsIsWindowsAuth);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DATABASE_NAME = ? ORDER BY RDBMS_DATABASE_NAME", this,rdbmsDatabaseName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_INSTANCE_NAME = :rdbmsInstanceName'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsInstanceNameEquals(String rdbmsInstanceName) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_INSTANCE_NAME = ? ORDER BY RDBMS_INSTANCE_NAME", this,rdbmsInstanceName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_USERNAME = :rdbmsServerUsername'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerUsernameEquals(String rdbmsServerUsername) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_USERNAME = ? ORDER BY RDBMS_SERVER_USERNAME", this,rdbmsServerUsername);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_PASSWORD = :rdbmsServerPassword'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerPasswordEquals(String rdbmsServerPassword) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_PASSWORD = ? ORDER BY RDBMS_SERVER_PASSWORD", this,rdbmsServerPassword);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_MACHINE_NAME = :rdbmsServerMachineName'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerMachineNameEquals(String rdbmsServerMachineName) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_MACHINE_NAME = ? ORDER BY RDBMS_SERVER_MACHINE_NAME", this,rdbmsServerMachineName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_PORT = :rdbmsServerPort'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerPortEquals(String rdbmsServerPort) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_PORT = ? ORDER BY RDBMS_SERVER_PORT", this,rdbmsServerPort);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_CDATETIME = :datasourceCdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceCdatetimeEquals(String datasourceCdatetime) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_CDATETIME = ? ORDER BY DATASOURCE_CDATETIME", this,datasourceCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_MDATETIME = :datasourceMdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceMdatetimeEquals(String datasourceMdatetime) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_MDATETIME = ? ORDER BY DATASOURCE_MDATETIME", this,datasourceMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_IS_DELETED = :datasourceIsDeleted'.
	 */
	@Transactional
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceIsDeletedEquals(short datasourceIsDeleted) throws IprodReportsViewReportTemplateDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_IS_DELETED = ? ORDER BY DATASOURCE_IS_DELETED", this,datasourceIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportTemplateDetailsDaoException("Query failed", e);
		}
		
	}
	public List<Map<String, Object>> findReportQueryTableColumnAndValues(int rtemplateId) throws IncorrectResultSizeDataAccessException{	
		String sql = null;
		List<Map<String, Object>> listReportTableColumnsAndValues = new ArrayList<Map<String, Object>>();
		try{
			sql=" SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_ID="+rtemplateId+"";
		}catch(Exception e){
			System.out.println("Exception in query is::" +e.getMessage());
		}
		Map mapReportTableColumnsAndValues = new HashMap();
		mapReportTableColumnsAndValues = jdbcTemplate.queryForMap(sql);
		Set setObject = mapReportTableColumnsAndValues.keySet();
		Iterator iteratorObject = setObject.iterator();
		while(iteratorObject.hasNext())
		{
			String key=(String)iteratorObject.next();
			Object value=mapReportTableColumnsAndValues.get(key);
			mapReportTableColumnsAndValues.put(key, value);
		}
		listReportTableColumnsAndValues.add(mapReportTableColumnsAndValues);
		return listReportTableColumnsAndValues;
	}
}
