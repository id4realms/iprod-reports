package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodImageMasterDao;
import com.id4.iprod.reports.dto.IprodImageMaster;
import com.id4.iprod.reports.dto.IprodImageMasterPk;
import com.id4.iprod.reports.exceptions.IprodImageMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodImageMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodImageMasterPk
	 */
	public IprodImageMasterPk insert(IprodImageMaster dto);

	/** 
	 * Updates a single row in the iprod_image_master table.
	 */
	public void update(IprodImageMasterPk pk, IprodImageMaster dto) throws IprodImageMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_image_master table.
	 */
	public void delete(IprodImageMasterPk pk) throws IprodImageMasterDaoException;

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_ID = :imageId'.
	 */
	public IprodImageMaster findByPrimaryKey(int imageId) throws IprodImageMasterDaoException;

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria ''.
	 */
	public List<IprodImageMaster> findAll() throws IprodImageMasterDaoException;

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_ID = :imageId'.
	 */
	public List<IprodImageMaster> findWhereImageIdEquals(int imageId) throws IprodImageMasterDaoException;

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_NAME = :imageName'.
	 */
	public List<IprodImageMaster> findWhereImageNameEquals(String imageName) throws IprodImageMasterDaoException;

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_CONTENT = :imageContent'.
	 */
	public List<IprodImageMaster> findWhereImageContentEquals(byte[] imageContent) throws IprodImageMasterDaoException;

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	public List<IprodImageMaster> findWhereIsDeletedEquals(short isDeleted) throws IprodImageMasterDaoException;

	/** 
	 * Returns the rows from the iprod_image_master table that matches the specified primary-key value.
	 */
	public IprodImageMaster findByPrimaryKey(IprodImageMasterPk pk) throws IprodImageMasterDaoException;

	void updateIsDeleted(short isDeleted, int imageId) throws IprodImageMasterDaoException;

	public List<IprodImageMaster> findWhereImageNameAndIsDeletedEquals(String imageName, short isDeleted) throws IprodImageMasterDaoException;
	
	public List<IprodImageMaster> findWhereImageIdAndIsDeletedEquals(int imageId, short isDeleted) throws IprodImageMasterDaoException;

}
