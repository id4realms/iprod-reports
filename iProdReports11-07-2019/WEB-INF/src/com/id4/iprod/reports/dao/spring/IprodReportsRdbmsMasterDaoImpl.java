package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsRdbmsMasterDao;
import com.id4.iprod.reports.dto.IprodReportsRdbmsMaster;
import com.id4.iprod.reports.dto.IprodReportsRdbmsMasterPk;
import com.id4.iprod.reports.exceptions.IprodReportsRdbmsMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsRdbmsMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsRdbmsMaster>, IprodReportsRdbmsMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsRdbmsMasterPk
	 */
	public IprodReportsRdbmsMasterPk insert(IprodReportsRdbmsMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getRdbmsName(),dto.getRdbmsSubVersionName(),dto.getRdbmsDesc(),dto.getRdbmsJdbcDriverClass(),dto.getRdbmsDefaultPort(),dto.getRdbmsCdatetime(),dto.getRdbmsMdatetime(),dto.getRdbmsIsDeleted()} );
		IprodReportsRdbmsMasterPk pk = new IprodReportsRdbmsMasterPk();
		pk.setRdbmsId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_reports_rdbms_master table.
	 */
	public void update(IprodReportsRdbmsMasterPk pk, IprodReportsRdbmsMaster dto) throws IprodReportsRdbmsMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET RDBMS_NAME = ?, RDBMS_SUB_VERSION_NAME = ?, RDBMS_DESC = ?, RDBMS_JDBC_DRIVER_CLASS = ?, RDBMS_DEFAULT_PORT = ?, RDBMS_CDATETIME = ?, RDBMS_MDATETIME = ?, RDBMS_IS_DELETED = ? WHERE RDBMS_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getRdbmsName(),dto.getRdbmsSubVersionName(),dto.getRdbmsDesc(),dto.getRdbmsJdbcDriverClass(),dto.getRdbmsDefaultPort(),dto.getRdbmsCdatetime(),dto.getRdbmsMdatetime(),dto.getRdbmsIsDeleted(),pk.getRdbmsId() } );
	}

	/** 
	 * Deletes a single row in the iprod_reports_rdbms_master table.
	 */
	@Transactional
	public void delete(IprodReportsRdbmsMasterPk pk) throws IprodReportsRdbmsMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE RDBMS_ID = ?",pk.getRdbmsId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsRdbmsMaster
	 */
	public IprodReportsRdbmsMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsRdbmsMaster dto = new IprodReportsRdbmsMaster();
		dto.setRdbmsId( rs.getInt( 1 ) );
		dto.setRdbmsName( rs.getString( 2 ) );
		dto.setRdbmsSubVersionName( rs.getString( 3 ) );
		dto.setRdbmsDesc( rs.getString( 4 ) );
		dto.setRdbmsJdbcDriverClass( rs.getString( 5 ) );
		dto.setRdbmsDefaultPort( rs.getString( 6 ) );
		dto.setRdbmsCdatetime( rs.getString( 7 ) );
		dto.setRdbmsMdatetime( rs.getString( 8 ) );
		dto.setRdbmsIsDeleted( rs.getShort( 9 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_rdbms_master";
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	@Transactional
	public IprodReportsRdbmsMaster findByPrimaryKey(int rdbmsId) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			List<IprodReportsRdbmsMaster> list = jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_ID = ?", this,rdbmsId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findAll() throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " ORDER BY RDBMS_ID", this);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_ID = ? ORDER BY RDBMS_ID", this,rdbmsId);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_NAME = ? ORDER BY RDBMS_NAME", this,rdbmsName);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_SUB_VERSION_NAME = :rdbmsSubVersionName'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsSubVersionNameEquals(String rdbmsSubVersionName) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SUB_VERSION_NAME = ? ORDER BY RDBMS_SUB_VERSION_NAME", this,rdbmsSubVersionName);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_DESC = :rdbmsDesc'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsDescEquals(String rdbmsDesc) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DESC = ? ORDER BY RDBMS_DESC", this,rdbmsDesc);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_JDBC_DRIVER_CLASS = :rdbmsJdbcDriverClass'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsJdbcDriverClassEquals(String rdbmsJdbcDriverClass) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_JDBC_DRIVER_CLASS = ? ORDER BY RDBMS_JDBC_DRIVER_CLASS", this,rdbmsJdbcDriverClass);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_DEFAULT_PORT = :rdbmsDefaultPort'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsDefaultPortEquals(String rdbmsDefaultPort) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DEFAULT_PORT = ? ORDER BY RDBMS_DEFAULT_PORT", this,rdbmsDefaultPort);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_CDATETIME = :rdbmsCdatetime'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsCdatetimeEquals(String rdbmsCdatetime) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_CDATETIME = ? ORDER BY RDBMS_CDATETIME", this,rdbmsCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_MDATETIME = :rdbmsMdatetime'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsMdatetimeEquals(String rdbmsMdatetime) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_MDATETIME = ? ORDER BY RDBMS_MDATETIME", this,rdbmsMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_IS_DELETED = :rdbmsIsDeleted'.
	 */
	@Transactional
	public List<IprodReportsRdbmsMaster> findWhereRdbmsIsDeletedEquals(short rdbmsIsDeleted) throws IprodReportsRdbmsMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_CDATETIME, RDBMS_MDATETIME, RDBMS_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_IS_DELETED = ? ORDER BY RDBMS_IS_DELETED", this,rdbmsIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsRdbmsMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_reports_rdbms_master table that matches the specified primary-key value.
	 */
	public IprodReportsRdbmsMaster findByPrimaryKey(IprodReportsRdbmsMasterPk pk) throws IprodReportsRdbmsMasterDaoException
	{
		return findByPrimaryKey( pk.getRdbmsId() );
	}

}
