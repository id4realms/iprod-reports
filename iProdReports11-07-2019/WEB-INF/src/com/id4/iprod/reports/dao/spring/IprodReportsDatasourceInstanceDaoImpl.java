package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsDatasourceInstanceDao;
import com.id4.iprod.reports.dto.IprodReportsDatasourceInstance;
import com.id4.iprod.reports.dto.IprodReportsDatasourceInstancePk;
import com.id4.iprod.reports.exceptions.IprodReportsDatasourceInstanceDaoException;
import com.id4.iprod.reports.exceptions.IprodReportsTemplateMasterDaoException;

import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.jfree.util.Log;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsDatasourceInstanceDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsDatasourceInstance>, IprodReportsDatasourceInstanceDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsDatasourceInstancePk
	 */
	public IprodReportsDatasourceInstancePk insert(IprodReportsDatasourceInstance dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getDatasourceName(),dto.getRdbmsId(),dto.getRdbmsJdbcDriverUrl(),dto.getRdbmsIsWindowsAuth(),dto.getRdbmsDatabaseName(),dto.getRdbmsInstanceName(),dto.getRdbmsServerUsername(),dto.getRdbmsServerPassword(),dto.getRdbmsServerMachineName(),dto.getRdbmsServerPort(),dto.getDatasourceCdatetime(),dto.getDatasourceMdatetime(),dto.getDatasourceIsDeleted()} );
		IprodReportsDatasourceInstancePk pk = new IprodReportsDatasourceInstancePk();
		pk.setDatasourceId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_reports_datasource_instance table.
	 */
	public void update(IprodReportsDatasourceInstancePk pk, IprodReportsDatasourceInstance dto) throws IprodReportsDatasourceInstanceDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET DATASOURCE_NAME = ?, RDBMS_ID = ?, RDBMS_JDBC_DRIVER_URL = ?, RDBMS_IS_WINDOWS_AUTH = ?, RDBMS_DATABASE_NAME = ?, RDBMS_INSTANCE_NAME = ?, RDBMS_SERVER_USERNAME = ?, RDBMS_SERVER_PASSWORD = ?, RDBMS_SERVER_MACHINE_NAME = ?, RDBMS_SERVER_PORT = ?, DATASOURCE_CDATETIME = ?, DATASOURCE_MDATETIME = ?, DATASOURCE_IS_DELETED = ? WHERE DATASOURCE_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getDatasourceName(),dto.getRdbmsId(),dto.getRdbmsJdbcDriverUrl(),dto.getRdbmsIsWindowsAuth(),dto.getRdbmsDatabaseName(),dto.getRdbmsInstanceName(),dto.getRdbmsServerUsername(),dto.getRdbmsServerPassword(),dto.getRdbmsServerMachineName(),dto.getRdbmsServerPort(),dto.getDatasourceCdatetime(),dto.getDatasourceMdatetime(),dto.getDatasourceIsDeleted(),pk.getDatasourceId() } );
	}

	/** 
	 * Deletes a single row in the iprod_reports_datasource_instance table.
	 */
	@Transactional
	public void delete(IprodReportsDatasourceInstancePk pk) throws IprodReportsDatasourceInstanceDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE DATASOURCE_ID = ?",pk.getDatasourceId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsDatasourceInstance
	 */
	public IprodReportsDatasourceInstance mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsDatasourceInstance dto = new IprodReportsDatasourceInstance();
		dto.setDatasourceId( rs.getInt( 1 ) );
		dto.setDatasourceName( rs.getString( 2 ) );
		dto.setRdbmsId( rs.getInt( 3 ) );
		dto.setRdbmsJdbcDriverUrl( rs.getString( 4 ) );
		dto.setRdbmsIsWindowsAuth( rs.getInt( 5 ) );
		if (rs.wasNull()) {
			dto.setRdbmsIsWindowsAuthNull( true );
		}
		
		dto.setRdbmsDatabaseName( rs.getString( 6 ) );
		dto.setRdbmsInstanceName( rs.getString( 7 ) );
		dto.setRdbmsServerUsername( rs.getString( 8 ) );
		dto.setRdbmsServerPassword( rs.getString( 9 ) );
		dto.setRdbmsServerMachineName( rs.getString( 10 ) );
		dto.setRdbmsServerPort( rs.getString( 11 ) );
		dto.setDatasourceCdatetime( rs.getString( 12 ) );
		dto.setDatasourceMdatetime( rs.getString( 13 ) );
		dto.setDatasourceIsDeleted( rs.getShort( 14 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_datasource_instance";
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	@Transactional
	public IprodReportsDatasourceInstance findByPrimaryKey(int datasourceId) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			List<IprodReportsDatasourceInstance> list = jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_ID = ?", this,datasourceId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findAll() throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " ORDER BY DATASOURCE_ID", this);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_ID = ? ORDER BY DATASOURCE_ID", this,datasourceId);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_NAME = ? ORDER BY DATASOURCE_NAME", this,datasourceName);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_ID = ? ORDER BY RDBMS_ID", this,rdbmsId);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_JDBC_DRIVER_URL = :rdbmsJdbcDriverUrl'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsJdbcDriverUrlEquals(String rdbmsJdbcDriverUrl) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_JDBC_DRIVER_URL = ? ORDER BY RDBMS_JDBC_DRIVER_URL", this,rdbmsJdbcDriverUrl);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_IS_WINDOWS_AUTH = :rdbmsIsWindowsAuth'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsIsWindowsAuthEquals(int rdbmsIsWindowsAuth) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_IS_WINDOWS_AUTH = ? ORDER BY RDBMS_IS_WINDOWS_AUTH", this,rdbmsIsWindowsAuth);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DATABASE_NAME = ? ORDER BY RDBMS_DATABASE_NAME", this,rdbmsDatabaseName);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_INSTANCE_NAME = :rdbmsInstanceName'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsInstanceNameEquals(String rdbmsInstanceName) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_INSTANCE_NAME = ? ORDER BY RDBMS_INSTANCE_NAME", this,rdbmsInstanceName);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_USERNAME = :rdbmsServerUsername'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerUsernameEquals(String rdbmsServerUsername) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_USERNAME = ? ORDER BY RDBMS_SERVER_USERNAME", this,rdbmsServerUsername);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_PASSWORD = :rdbmsServerPassword'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerPasswordEquals(String rdbmsServerPassword) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_PASSWORD = ? ORDER BY RDBMS_SERVER_PASSWORD", this,rdbmsServerPassword);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_MACHINE_NAME = :rdbmsServerMachineName'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerMachineNameEquals(String rdbmsServerMachineName) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_MACHINE_NAME = ? ORDER BY RDBMS_SERVER_MACHINE_NAME", this,rdbmsServerMachineName);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_PORT = :rdbmsServerPort'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerPortEquals(String rdbmsServerPort) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_PORT = ? ORDER BY RDBMS_SERVER_PORT", this,rdbmsServerPort);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_CDATETIME = :datasourceCdatetime'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereDatasourceCdatetimeEquals(String datasourceCdatetime) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_CDATETIME = ? ORDER BY DATASOURCE_CDATETIME", this,datasourceCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_MDATETIME = :datasourceMdatetime'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereDatasourceMdatetimeEquals(String datasourceMdatetime) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_MDATETIME = ? ORDER BY DATASOURCE_MDATETIME", this,datasourceMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_IS_DELETED = :datasourceIsDeleted'.
	 */
	@Transactional
	public List<IprodReportsDatasourceInstance> findWhereDatasourceIsDeletedEquals(short datasourceIsDeleted) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_IS_DELETED = ? ORDER BY DATASOURCE_IS_DELETED", this,datasourceIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_reports_datasource_instance table that matches the specified primary-key value.
	 */
	public IprodReportsDatasourceInstance findByPrimaryKey(IprodReportsDatasourceInstancePk pk) throws IprodReportsDatasourceInstanceDaoException
	{
		return findByPrimaryKey( pk.getDatasourceId() );
	}

	public void updateIsDeleted(short datasourceIsDeleted, int datasourceId) throws IprodReportsDatasourceInstanceDaoException {		
		jdbcTemplate.update("UPDATE " + getTableName() + " SET DATASOURCE_IS_DELETED = ? WHERE DATASOURCE_ID=?",datasourceIsDeleted,datasourceId);		
	}
	
	public List<IprodReportsDatasourceInstance> findWhereDatasourceNameAndIsDeletedEquals(String datasourceName,short datasourceIsDeleted) throws IprodReportsDatasourceInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_NAME = ?  AND DATASOURCE_IS_DELETED = ?  ORDER BY DATASOURCE_IS_DELETED", this,datasourceName,datasourceIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
		
	}
	public void updateDatasourceDetailsWhereDatasourceIdEquals(String datasourceName,int rdbmsId,String rdmsDatabaseName,String rdbmsJdbcDriverUrl,int datasourceId) throws IprodReportsDatasourceInstanceDaoException
	{
		try 
		{
			jdbcTemplate.update("UPDATE " + getTableName() + " SET DATASOURCE_NAME = ?, RDBMS_ID = ?, RDBMS_DATABASE_NAME = ?, RDBMS_JDBC_DRIVER_URL = ? WHERE DATASOURCE_ID = ? ",datasourceName,rdbmsId,rdmsDatabaseName,rdbmsJdbcDriverUrl,datasourceId);
		} 
		catch (Exception e) 
		{
			throw new IprodReportsDatasourceInstanceDaoException("Query failed", e);
		}
	}
}
