package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsViewReportsInstanceDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportsInstanceDetails;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportsInstanceDetailsDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsViewReportsInstanceDetailsDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsViewReportsInstanceDetails>, IprodReportsViewReportsInstanceDetailsDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodReportsViewReportsInstanceDetails dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.compile();
		su.update( new Object[] { dto.getReportId(),dto.getRtemplateId(),dto.getRtemplateName(),dto.getDatasourceId(),dto.getDatasourceName(),dto.getRdbmsId(),dto.getRdbmsName(),dto.getRdbmsDatabaseName(),dto.getUserId(),dto.getUserName(),dto.getReportQuery(),dto.getReportExcelLocation(),dto.getReportPdfLocation(),dto.getReportCdatetime(),dto.getReportMdatetime(),dto.getRtemplateSqlquery(),dto.getRtemplateBirtRpt(),dto.getRtemplateTitle(),dto.getIsScheduled(),dto.getIsDeleted()} );
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsViewReportsInstanceDetails
	 */
	public IprodReportsViewReportsInstanceDetails mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsViewReportsInstanceDetails dto = new IprodReportsViewReportsInstanceDetails();
		dto.setReportId( rs.getInt( 1 ) );
		dto.setRtemplateId( rs.getInt( 2 ) );
		dto.setRtemplateName( rs.getString( 3 ) );
		dto.setDatasourceId( rs.getInt( 4 ) );
		if (rs.wasNull()) {
			dto.setDatasourceIdNull( true );
		}
		
		dto.setDatasourceName( rs.getString( 5 ) );
		dto.setRdbmsId( rs.getInt( 6 ) );
		if (rs.wasNull()) {
			dto.setRdbmsIdNull( true );
		}
		
		dto.setRdbmsName( rs.getString( 7 ) );
		dto.setRdbmsDatabaseName( rs.getString( 8 ) );
		dto.setUserId( rs.getInt( 9 ) );
		dto.setUserName( rs.getString( 10 ) );
		dto.setReportQuery( rs.getString( 11 ) );
		dto.setReportExcelLocation( rs.getString( 12 ) );
		dto.setReportPdfLocation( rs.getString( 13 ) );
		dto.setReportCdatetime( rs.getString( 14 ) );
		dto.setReportMdatetime( rs.getString( 15 ) );
		dto.setRtemplateSqlquery( rs.getString( 16 ) );
		dto.setRtemplateBirtRpt( rs.getString( 17 ) );
		dto.setRtemplateTitle( rs.getString( 18 ) );
		dto.setIsScheduled( rs.getInt( 19 ) );
		if (rs.wasNull()) {
			dto.setIsScheduledNull( true );
		}
		
		dto.setIsDeleted( rs.getInt( 20 ) );
		if (rs.wasNull()) {
			dto.setIsDeletedNull( true );
		}
		
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_view_reports_instance_details";
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findAll() throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + "", this);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_ID = :reportId'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportIdEquals(int reportId) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_ID = ? ORDER BY REPORT_ID", this,reportId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_ID = ? ORDER BY RTEMPLATE_ID", this,rtemplateId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_NAME = :rtemplateName'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateNameEquals(String rtemplateName) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_NAME = ? ORDER BY RTEMPLATE_NAME", this,rtemplateName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_ID = ? ORDER BY DATASOURCE_ID", this,datasourceId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_NAME = ? ORDER BY DATASOURCE_NAME", this,datasourceName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RDBMS_ID = ? ORDER BY RDBMS_ID", this,rdbmsId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RDBMS_NAME = ? ORDER BY RDBMS_NAME", this,rdbmsName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DATABASE_NAME = ? ORDER BY RDBMS_DATABASE_NAME", this,rdbmsDatabaseName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'USER_ID = :userId'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereUserIdEquals(int userId) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE USER_ID = ? ORDER BY USER_ID", this,userId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'USER_NAME = :userName'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereUserNameEquals(String userName) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE USER_NAME = ? ORDER BY USER_NAME", this,userName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_QUERY = :reportQuery'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportQueryEquals(String reportQuery) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_QUERY = ? ORDER BY REPORT_QUERY", this,reportQuery);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_EXCEL_LOCATION = :reportExcelLocation'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportExcelLocationEquals(String reportExcelLocation) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_EXCEL_LOCATION = ? ORDER BY REPORT_EXCEL_LOCATION", this,reportExcelLocation);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_PDF_LOCATION = :reportPdfLocation'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportPdfLocationEquals(String reportPdfLocation) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_PDF_LOCATION = ? ORDER BY REPORT_PDF_LOCATION", this,reportPdfLocation);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_CDATETIME = :reportCdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportCdatetimeEquals(String reportCdatetime) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_CDATETIME = ? ORDER BY REPORT_CDATETIME", this,reportCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_MDATETIME = :reportMdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportMdatetimeEquals(String reportMdatetime) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_MDATETIME = ? ORDER BY REPORT_MDATETIME", this,reportMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_SQLQUERY = :rtemplateSqlquery'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateSqlqueryEquals(String rtemplateSqlquery) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_SQLQUERY = ? ORDER BY RTEMPLATE_SQLQUERY", this,rtemplateSqlquery);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_BIRT_RPT = :rtemplateBirtRpt'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateBirtRptEquals(String rtemplateBirtRpt) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_BIRT_RPT = ? ORDER BY RTEMPLATE_BIRT_RPT", this,rtemplateBirtRpt);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_TITLE = :rtemplateTitle'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateTitleEquals(String rtemplateTitle) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_TITLE = ? ORDER BY RTEMPLATE_TITLE", this,rtemplateTitle);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'IS_SCHEDULED = :isScheduled'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereIsScheduledEquals(int isScheduled) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE IS_SCHEDULED = ? ORDER BY IS_SCHEDULED", this,isScheduled);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	@Transactional
	public List<IprodReportsViewReportsInstanceDetails> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsViewReportsInstanceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, RTEMPLATE_NAME, DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_DATABASE_NAME, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, RTEMPLATE_SQLQUERY, RTEMPLATE_BIRT_RPT, RTEMPLATE_TITLE, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE IS_DELETED = ? ORDER BY IS_DELETED", this,isDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportsInstanceDetailsDaoException("Query failed"+ e.getMessage());
		}
		
	}

}
