package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsJobDataDao;
import com.id4.iprod.reports.dto.IprodReportsJobData;
import com.id4.iprod.reports.dto.IprodReportsJobDataPk;
import com.id4.iprod.reports.exceptions.IprodReportsJobDataDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsJobDataDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsJobData>, IprodReportsJobDataDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsJobDataPk
	 */
	public IprodReportsJobDataPk insert(IprodReportsJobData dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.compile();
		su.update( new Object[] { dto.getJobName(),dto.getJobDescription(),dto.getRecipients(),dto.getRepeatInterval(),dto.getStartDateTime(),dto.getEndDateTime(),dto.getActive(),dto.getIsDeleted()} );
		IprodReportsJobDataPk pk = new IprodReportsJobDataPk();
		pk.setJobId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_reports_job_data table.
	 */
	public void update(IprodReportsJobDataPk pk, IprodReportsJobData dto) throws IprodReportsJobDataDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET JOB_NAME = ?, JOB_DESCRIPTION = ?, RECIPIENTS = ?, REPEAT_INTERVAL = ?, START_DATE_TIME = ?, END_DATE_TIME = ?, ACTIVE = ?, IS_DELETED = ? WHERE JOB_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getJobName(),dto.getJobDescription(),dto.getRecipients(),dto.getRepeatInterval(),dto.getStartDateTime(),dto.getEndDateTime(),dto.getActive(),dto.getIsDeleted(),pk.getJobId() } );
	}

	/** 
	 * Deletes a single row in the iprod_reports_job_data table.
	 */
	@Transactional
	public void delete(IprodReportsJobDataPk pk) throws IprodReportsJobDataDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE JOB_ID = ?",pk.getJobId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsJobData
	 */
	public IprodReportsJobData mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsJobData dto = new IprodReportsJobData();
		dto.setJobId( rs.getInt( 1 ) );
		dto.setJobName( rs.getString( 2 ) );
		dto.setJobDescription( rs.getString( 3 ) );
		dto.setRecipients( rs.getString( 4 ) );
		dto.setRepeatInterval( rs.getString( 5 ) );
		dto.setStartDateTime( rs.getString( 6 ) );
		dto.setEndDateTime( rs.getString( 7 ) );
		dto.setActive( rs.getInt( 8 ) );
		if (rs.wasNull()) {
			dto.setActiveNull( true );
		}
		
		dto.setIsDeleted( rs.getInt( 9 ) );
		if (rs.wasNull()) {
			dto.setIsDeletedNull( true );
		}
		
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_job_data";
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_ID = :jobId'.
	 */
	@Transactional
	public IprodReportsJobData findByPrimaryKey(int jobId) throws IprodReportsJobDataDaoException
	{
		try {
			List<IprodReportsJobData> list = jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE JOB_ID = ?", this,jobId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsJobData> findAll() throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " ORDER BY JOB_ID", this);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_ID = :jobId'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereJobIdEquals(int jobId) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE JOB_ID = ? ORDER BY JOB_ID", this,jobId);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_NAME = :jobName'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereJobNameEquals(String jobName) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE JOB_NAME = ? ORDER BY JOB_NAME", this,jobName);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_DESCRIPTION = :jobDescription'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereJobDescriptionEquals(String jobDescription) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE JOB_DESCRIPTION = ? ORDER BY JOB_DESCRIPTION", this,jobDescription);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'RECIPIENTS = :recipients'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereRecipientsEquals(String recipients) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE RECIPIENTS = ? ORDER BY RECIPIENTS", this,recipients);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'REPEAT_INTERVAL = :repeatInterval'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereRepeatIntervalEquals(String repeatInterval) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE REPEAT_INTERVAL = ? ORDER BY REPEAT_INTERVAL", this,repeatInterval);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'START_DATE_TIME = :startDateTime'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereStartDateTimeEquals(String startDateTime) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE START_DATE_TIME = ? ORDER BY START_DATE_TIME", this,startDateTime);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'END_DATE_TIME = :endDateTime'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereEndDateTimeEquals(String endDateTime) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE END_DATE_TIME = ? ORDER BY END_DATE_TIME", this,endDateTime);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'ACTIVE = :active'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereActiveEquals(int active) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE ACTIVE = ? ORDER BY ACTIVE", this,active);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	@Transactional
	public List<IprodReportsJobData> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsJobDataDaoException
	{
		try {
			return jdbcTemplate.query("SELECT JOB_ID, JOB_NAME, JOB_DESCRIPTION, RECIPIENTS, REPEAT_INTERVAL, START_DATE_TIME, END_DATE_TIME, ACTIVE, IS_DELETED FROM " + getTableName() + " WHERE IS_DELETED = ? ORDER BY IS_DELETED", this,isDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsJobDataDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_reports_job_data table that matches the specified primary-key value.
	 */
	public IprodReportsJobData findByPrimaryKey(IprodReportsJobDataPk pk) throws IprodReportsJobDataDaoException
	{
		return findByPrimaryKey( pk.getJobId() );
	}

}
