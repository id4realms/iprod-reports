package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsRdbmsMasterDao;
import com.id4.iprod.reports.dto.IprodReportsRdbmsMaster;
import com.id4.iprod.reports.dto.IprodReportsRdbmsMasterPk;
import com.id4.iprod.reports.exceptions.IprodReportsRdbmsMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsRdbmsMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsRdbmsMasterPk
	 */
	public IprodReportsRdbmsMasterPk insert(IprodReportsRdbmsMaster dto);

	/** 
	 * Updates a single row in the iprod_reports_rdbms_master table.
	 */
	public void update(IprodReportsRdbmsMasterPk pk, IprodReportsRdbmsMaster dto) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_reports_rdbms_master table.
	 */
	public void delete(IprodReportsRdbmsMasterPk pk) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	public IprodReportsRdbmsMaster findByPrimaryKey(int rdbmsId) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria ''.
	 */
	public List<IprodReportsRdbmsMaster> findAll() throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_SUB_VERSION_NAME = :rdbmsSubVersionName'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsSubVersionNameEquals(String rdbmsSubVersionName) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_DESC = :rdbmsDesc'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsDescEquals(String rdbmsDesc) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_JDBC_DRIVER_CLASS = :rdbmsJdbcDriverClass'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsJdbcDriverClassEquals(String rdbmsJdbcDriverClass) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_DEFAULT_PORT = :rdbmsDefaultPort'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsDefaultPortEquals(String rdbmsDefaultPort) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_CDATETIME = :rdbmsCdatetime'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsCdatetimeEquals(String rdbmsCdatetime) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_MDATETIME = :rdbmsMdatetime'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsMdatetimeEquals(String rdbmsMdatetime) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_rdbms_master table that match the criteria 'RDBMS_IS_DELETED = :rdbmsIsDeleted'.
	 */
	public List<IprodReportsRdbmsMaster> findWhereRdbmsIsDeletedEquals(short rdbmsIsDeleted) throws IprodReportsRdbmsMasterDaoException;

	/** 
	 * Returns the rows from the iprod_reports_rdbms_master table that matches the specified primary-key value.
	 */
	public IprodReportsRdbmsMaster findByPrimaryKey(IprodReportsRdbmsMasterPk pk) throws IprodReportsRdbmsMasterDaoException;

}
