package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsViewReportsInstanceDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportsInstanceDetails;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportsInstanceDetailsDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsViewReportsInstanceDetailsDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodReportsViewReportsInstanceDetails dto);

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria ''.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findAll() throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_ID = :reportId'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportIdEquals(int reportId) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_NAME = :rtemplateName'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateNameEquals(String rtemplateName) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'USER_ID = :userId'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereUserIdEquals(int userId) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'USER_NAME = :userName'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereUserNameEquals(String userName) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_QUERY = :reportQuery'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportQueryEquals(String reportQuery) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_EXCEL_LOCATION = :reportExcelLocation'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportExcelLocationEquals(String reportExcelLocation) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_PDF_LOCATION = :reportPdfLocation'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportPdfLocationEquals(String reportPdfLocation) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_CDATETIME = :reportCdatetime'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportCdatetimeEquals(String reportCdatetime) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'REPORT_MDATETIME = :reportMdatetime'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereReportMdatetimeEquals(String reportMdatetime) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_SQLQUERY = :rtemplateSqlquery'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateSqlqueryEquals(String rtemplateSqlquery) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_BIRT_RPT = :rtemplateBirtRpt'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateBirtRptEquals(String rtemplateBirtRpt) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'RTEMPLATE_TITLE = :rtemplateTitle'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereRtemplateTitleEquals(String rtemplateTitle) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'IS_SCHEDULED = :isScheduled'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereIsScheduledEquals(int isScheduled) throws IprodReportsViewReportsInstanceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_reports_instance_details table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	public List<IprodReportsViewReportsInstanceDetails> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsViewReportsInstanceDetailsDaoException;

}
