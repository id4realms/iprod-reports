package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsRecipientMasterDao;
import com.id4.iprod.reports.dto.IprodReportsRecipientMaster;
import com.id4.iprod.reports.dto.IprodReportsRecipientMasterPk;
import com.id4.iprod.reports.exceptions.IprodReportsRecipientMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsRecipientMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsRecipientMaster>, IprodReportsRecipientMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsRecipientMasterPk
	 */
	public IprodReportsRecipientMasterPk insert(IprodReportsRecipientMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME ) VALUES ( ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.compile();
		su.update( new Object[] { dto.getFirstName(),dto.getLastName(),dto.getEmailId(),dto.getContactNo(),dto.getIsDeleted(),dto.getRecipientCdatetime(),dto.getRecipientMdatetime()} );
		IprodReportsRecipientMasterPk pk = new IprodReportsRecipientMasterPk();
		pk.setRecipientId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_reports_recipient_master table.
	 */
	public void update(IprodReportsRecipientMasterPk pk, IprodReportsRecipientMaster dto) throws IprodReportsRecipientMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET FIRST_NAME = ?, LAST_NAME = ?, EMAIL_ID = ?, CONTACT_NO = ?, IS_DELETED = ?, RECIPIENT_CDATETIME = ?, RECIPIENT_MDATETIME = ? WHERE RECIPIENT_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getFirstName(),dto.getLastName(),dto.getEmailId(),dto.getContactNo(),dto.getIsDeleted(),dto.getRecipientCdatetime(),dto.getRecipientMdatetime(),pk.getRecipientId() } );
	}

	/** 
	 * Deletes a single row in the iprod_reports_recipient_master table.
	 */
	@Transactional
	public void delete(IprodReportsRecipientMasterPk pk) throws IprodReportsRecipientMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE RECIPIENT_ID = ?",pk.getRecipientId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsRecipientMaster
	 */
	public IprodReportsRecipientMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsRecipientMaster dto = new IprodReportsRecipientMaster();
		dto.setRecipientId( rs.getInt( 1 ) );
		dto.setFirstName( rs.getString( 2 ) );
		dto.setLastName( rs.getString( 3 ) );
		dto.setEmailId( rs.getString( 4 ) );
		dto.setContactNo( rs.getString( 5 ) );
		dto.setIsDeleted( rs.getInt( 6 ) );
		dto.setRecipientCdatetime( rs.getString( 7 ) );
		dto.setRecipientMdatetime( rs.getString( 8 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_recipient_master";
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_ID = :recipientId'.
	 */
	@Transactional
	public IprodReportsRecipientMaster findByPrimaryKey(int recipientId) throws IprodReportsRecipientMasterDaoException
	{
		try {
			List<IprodReportsRecipientMaster> list = jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE RECIPIENT_ID = ?", this,recipientId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findAll() throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " ORDER BY RECIPIENT_ID", this);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_ID = :recipientId'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereRecipientIdEquals(int recipientId) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE RECIPIENT_ID = ? ORDER BY RECIPIENT_ID", this,recipientId);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'FIRST_NAME = :firstName'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereFirstNameEquals(String firstName) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE FIRST_NAME = ? ORDER BY FIRST_NAME", this,firstName);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'LAST_NAME = :lastName'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereLastNameEquals(String lastName) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE LAST_NAME = ? ORDER BY LAST_NAME", this,lastName);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'EMAIL_ID = :emailId'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereEmailIdEquals(String emailId) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE EMAIL_ID = ? ORDER BY EMAIL_ID", this,emailId);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'CONTACT_NO = :contactNo'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereContactNoEquals(String contactNo) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE CONTACT_NO = ? ORDER BY CONTACT_NO", this,contactNo);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE IS_DELETED = ? ORDER BY IS_DELETED", this,isDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_CDATETIME = :recipientCdatetime'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereRecipientCdatetimeEquals(String recipientCdatetime) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE RECIPIENT_CDATETIME = ? ORDER BY RECIPIENT_CDATETIME", this,recipientCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_MDATETIME = :recipientMdatetime'.
	 */
	@Transactional
	public List<IprodReportsRecipientMaster> findWhereRecipientMdatetimeEquals(String recipientMdatetime) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE RECIPIENT_MDATETIME = ? ORDER BY RECIPIENT_MDATETIME", this,recipientMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_reports_recipient_master table that matches the specified primary-key value.
	 */
	public IprodReportsRecipientMaster findByPrimaryKey(IprodReportsRecipientMasterPk pk) throws IprodReportsRecipientMasterDaoException
	{
		return findByPrimaryKey( pk.getRecipientId() );
	}
	
	public List<IprodReportsRecipientMaster> findWhereFirstNameAndIsDeletedEquals(String firstName, int isDeleted) throws IprodReportsRecipientMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RECIPIENT_ID, FIRST_NAME, LAST_NAME, EMAIL_ID, CONTACT_NO, IS_DELETED, RECIPIENT_CDATETIME, RECIPIENT_MDATETIME FROM " + getTableName() + " WHERE FIRST_NAME = ? AND IS_DELETED = ? ORDER BY FIRST_NAME", this,firstName,isDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsRecipientMasterDaoException("Query failed", e);
		}
		
	}

}
