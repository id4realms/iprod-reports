package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsRecipientMasterDao;
import com.id4.iprod.reports.dto.IprodReportsRecipientMaster;
import com.id4.iprod.reports.dto.IprodReportsRecipientMasterPk;
import com.id4.iprod.reports.exceptions.IprodReportsRecipientMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsRecipientMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsRecipientMasterPk
	 */
	public IprodReportsRecipientMasterPk insert(IprodReportsRecipientMaster dto);

	/** 
	 * Updates a single row in the iprod_reports_recipient_master table.
	 */
	public void update(IprodReportsRecipientMasterPk pk, IprodReportsRecipientMaster dto) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_reports_recipient_master table.
	 */
	public void delete(IprodReportsRecipientMasterPk pk) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_ID = :recipientId'.
	 */
	public IprodReportsRecipientMaster findByPrimaryKey(int recipientId) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria ''.
	 */
	public List<IprodReportsRecipientMaster> findAll() throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_ID = :recipientId'.
	 */
	public List<IprodReportsRecipientMaster> findWhereRecipientIdEquals(int recipientId) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'FIRST_NAME = :firstName'.
	 */
	public List<IprodReportsRecipientMaster> findWhereFirstNameEquals(String firstName) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'LAST_NAME = :lastName'.
	 */
	public List<IprodReportsRecipientMaster> findWhereLastNameEquals(String lastName) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'EMAIL_ID = :emailId'.
	 */
	public List<IprodReportsRecipientMaster> findWhereEmailIdEquals(String emailId) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'CONTACT_NO = :contactNo'.
	 */
	public List<IprodReportsRecipientMaster> findWhereContactNoEquals(String contactNo) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	public List<IprodReportsRecipientMaster> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_CDATETIME = :recipientCdatetime'.
	 */
	public List<IprodReportsRecipientMaster> findWhereRecipientCdatetimeEquals(String recipientCdatetime) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_recipient_master table that match the criteria 'RECIPIENT_MDATETIME = :recipientMdatetime'.
	 */
	public List<IprodReportsRecipientMaster> findWhereRecipientMdatetimeEquals(String recipientMdatetime) throws IprodReportsRecipientMasterDaoException;

	/** 
	 * Returns the rows from the iprod_reports_recipient_master table that matches the specified primary-key value.
	 */
	public IprodReportsRecipientMaster findByPrimaryKey(IprodReportsRecipientMasterPk pk) throws IprodReportsRecipientMasterDaoException;

	public List<IprodReportsRecipientMaster> findWhereFirstNameAndIsDeletedEquals(String firstName, int isDeleted) throws IprodReportsRecipientMasterDaoException;
}
