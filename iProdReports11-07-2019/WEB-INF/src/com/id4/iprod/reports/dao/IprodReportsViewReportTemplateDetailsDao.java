package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsViewReportTemplateDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetails;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportTemplateDetailsDaoException;
import java.util.List;
import java.util.Map;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsViewReportTemplateDetailsDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodReportsViewReportTemplateDetails dto);

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria ''.
	 */
	public List<IprodReportsViewReportTemplateDetails> findAll() throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_NAME = :rtemplateName'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateNameEquals(String rtemplateName) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_TITLE = :rtemplateTitle'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateTitleEquals(String rtemplateTitle) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_SUB_TITLE = :rtemplateSubTitle'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateSubTitleEquals(String rtemplateSubTitle) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_SQLQUERY = :rtemplateSqlquery'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateSqlqueryEquals(String rtemplateSqlquery) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_EXCEL_DATA_STORAGE = :rtemplateExcelDataStorage'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateExcelDataStorageEquals(String rtemplateExcelDataStorage) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_PDF_DATA_STORAGE = :rtemplatePdfDataStorage'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplatePdfDataStorageEquals(String rtemplatePdfDataStorage) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_BIRT_RPT = :rtemplateBirtRpt'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateBirtRptEquals(String rtemplateBirtRpt) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_CDATETIME = :rtemplateCdatetime'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateCdatetimeEquals(String rtemplateCdatetime) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_MDATETIME = :rtemplateMdatetime'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateMdatetimeEquals(String rtemplateMdatetime) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RTEMPLATE_IS_DELETED = :rtemplateIsDeleted'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRtemplateIsDeletedEquals(short rtemplateIsDeleted) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SUB_VERSION_NAME = :rdbmsSubVersionName'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsSubVersionNameEquals(String rdbmsSubVersionName) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_DESC = :rdbmsDesc'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsDescEquals(String rdbmsDesc) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_JDBC_DRIVER_CLASS = :rdbmsJdbcDriverClass'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsJdbcDriverClassEquals(String rdbmsJdbcDriverClass) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_DEFAULT_PORT = :rdbmsDefaultPort'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsDefaultPortEquals(String rdbmsDefaultPort) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_CDATETIME = :rdbmsCdatetime'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsCdatetimeEquals(String rdbmsCdatetime) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_MDATETIME = :rdbmsMdatetime'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsMdatetimeEquals(String rdbmsMdatetime) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_IS_DELETED = :rdbmsIsDeleted'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsIsDeletedEquals(short rdbmsIsDeleted) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_JDBC_DRIVER_URL = :rdbmsJdbcDriverUrl'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsJdbcDriverUrlEquals(String rdbmsJdbcDriverUrl) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_IS_WINDOWS_AUTH = :rdbmsIsWindowsAuth'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsIsWindowsAuthEquals(int rdbmsIsWindowsAuth) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_INSTANCE_NAME = :rdbmsInstanceName'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsInstanceNameEquals(String rdbmsInstanceName) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_USERNAME = :rdbmsServerUsername'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerUsernameEquals(String rdbmsServerUsername) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_PASSWORD = :rdbmsServerPassword'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerPasswordEquals(String rdbmsServerPassword) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_MACHINE_NAME = :rdbmsServerMachineName'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerMachineNameEquals(String rdbmsServerMachineName) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'RDBMS_SERVER_PORT = :rdbmsServerPort'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereRdbmsServerPortEquals(String rdbmsServerPort) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_CDATETIME = :datasourceCdatetime'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceCdatetimeEquals(String datasourceCdatetime) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_MDATETIME = :datasourceMdatetime'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceMdatetimeEquals(String datasourceMdatetime) throws IprodReportsViewReportTemplateDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_template_details table that match the criteria 'DATASOURCE_IS_DELETED = :datasourceIsDeleted'.
	 */
	public List<IprodReportsViewReportTemplateDetails> findWhereDatasourceIsDeletedEquals(short datasourceIsDeleted) throws IprodReportsViewReportTemplateDetailsDaoException;

	public List<Map<String, Object>> findReportQueryTableColumnAndValues(int rtemplateId) throws IncorrectResultSizeDataAccessException;

}
