package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsInstanceDao;
import com.id4.iprod.reports.dto.IprodReportsInstance;
import com.id4.iprod.reports.dto.IprodReportsInstancePk;
import com.id4.iprod.reports.exceptions.IprodReportsInstanceDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsInstanceDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsInstancePk
	 */
	public IprodReportsInstancePk insert(IprodReportsInstance dto);

	/** 
	 * Updates a single row in the iprod_reports_instance table.
	 */
	public void update(IprodReportsInstancePk pk, IprodReportsInstance dto) throws IprodReportsInstanceDaoException;

	/** 
	 * Deletes a single row in the iprod_reports_instance table.
	 */
	public void delete(IprodReportsInstancePk pk) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_ID = :reportId'.
	 */
	public IprodReportsInstance findByPrimaryKey(int reportId) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria ''.
	 */
	public List<IprodReportsInstance> findAll() throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_ID = :reportId'.
	 */
	public List<IprodReportsInstance> findWhereReportIdEquals(int reportId) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	public List<IprodReportsInstance> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'USER_ID = :userId'.
	 */
	public List<IprodReportsInstance> findWhereUserIdEquals(int userId) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'USER_NAME = :userName'.
	 */
	public List<IprodReportsInstance> findWhereUserNameEquals(String userName) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_QUERY = :reportQuery'.
	 */
	public List<IprodReportsInstance> findWhereReportQueryEquals(String reportQuery) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_EXCEL_LOCATION = :reportExcelLocation'.
	 */
	public List<IprodReportsInstance> findWhereReportExcelLocationEquals(String reportExcelLocation) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_PDF_LOCATION = :reportPdfLocation'.
	 */
	public List<IprodReportsInstance> findWhereReportPdfLocationEquals(String reportPdfLocation) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_CDATETIME = :reportCdatetime'.
	 */
	public List<IprodReportsInstance> findWhereReportCdatetimeEquals(String reportCdatetime) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_MDATETIME = :reportMdatetime'.
	 */
	public List<IprodReportsInstance> findWhereReportMdatetimeEquals(String reportMdatetime) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'IS_SCHEDULED = :isScheduled'.
	 */
	public List<IprodReportsInstance> findWhereIsScheduledEquals(int isScheduled) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	public List<IprodReportsInstance> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsInstanceDaoException;

	/** 
	 * Returns the rows from the iprod_reports_instance table that matches the specified primary-key value.
	 */
	public IprodReportsInstance findByPrimaryKey(IprodReportsInstancePk pk) throws IprodReportsInstanceDaoException;

	public void updateIsScheduledWhereReportIdEquals(int isScheduled, int reportId) throws IprodReportsInstanceDaoException;

	public void updateReportPDFLocationWhereReportIdIs(int reportId, String reportPdfLocation) throws IprodReportsInstanceDaoException;

	public void updateReportExcelLocationWhereReportIdIs(int reportId, String reportExcelLocation) throws IprodReportsInstanceDaoException;

}
