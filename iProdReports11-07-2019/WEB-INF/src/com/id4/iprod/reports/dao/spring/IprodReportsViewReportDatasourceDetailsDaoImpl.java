package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsViewReportDatasourceDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportDatasourceDetails;
import com.id4.iprod.exceptions.IprodReportsViewReportDatasourceDetailsDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsViewReportDatasourceDetailsDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsViewReportDatasourceDetails>, IprodReportsViewReportDatasourceDetailsDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodReportsViewReportDatasourceDetails dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getDatasourceId(),dto.getDatasourceName(),dto.getRdbmsId(),dto.getRdbmsName(),dto.getRdbmsSubVersionName(),dto.getRdbmsDesc(),dto.getRdbmsJdbcDriverClass(),dto.getRdbmsDefaultPort(),dto.getRdbmsJdbcDriverUrl(),dto.getRdbmsIsWindowsAuth(),dto.getRdbmsDatabaseName(),dto.getRdbmsInstanceName(),dto.getRdbmsServerUsername(),dto.getRdbmsServerPassword(),dto.getRdbmsServerMachineName(),dto.getRdbmsServerPort(),dto.getDatasourceCdatetime(),dto.getDatasourceMdatetime(),dto.getDatasourceIsDeleted()} );
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsViewReportDatasourceDetails
	 */
	public IprodReportsViewReportDatasourceDetails mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsViewReportDatasourceDetails dto = new IprodReportsViewReportDatasourceDetails();
		dto.setDatasourceId( rs.getInt( 1 ) );
		dto.setDatasourceName( rs.getString( 2 ) );
		dto.setRdbmsId( rs.getInt( 3 ) );
		dto.setRdbmsName( rs.getString( 4 ) );
		dto.setRdbmsSubVersionName( rs.getString( 5 ) );
		dto.setRdbmsDesc( rs.getString( 6 ) );
		dto.setRdbmsJdbcDriverClass( rs.getString( 7 ) );
		dto.setRdbmsDefaultPort( rs.getString( 8 ) );
		dto.setRdbmsJdbcDriverUrl( rs.getString( 9 ) );
		dto.setRdbmsIsWindowsAuth( rs.getInt( 10 ) );
		if (rs.wasNull()) {
			dto.setRdbmsIsWindowsAuthNull( true );
		}
		
		dto.setRdbmsDatabaseName( rs.getString( 11 ) );
		dto.setRdbmsInstanceName( rs.getString( 12 ) );
		dto.setRdbmsServerUsername( rs.getString( 13 ) );
		dto.setRdbmsServerPassword( rs.getString( 14 ) );
		dto.setRdbmsServerMachineName( rs.getString( 15 ) );
		dto.setRdbmsServerPort( rs.getString( 16 ) );
		dto.setDatasourceCdatetime( rs.getString( 17 ) );
		dto.setDatasourceMdatetime( rs.getString( 18 ) );
		dto.setDatasourceIsDeleted( rs.getShort( 19 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_view_report_datasource_details";
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findAll() throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + "", this);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_ID = ? ORDER BY DATASOURCE_ID", this,datasourceId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_NAME = ? ORDER BY DATASOURCE_NAME", this,datasourceName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_ID = ? ORDER BY RDBMS_ID", this,rdbmsId);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_NAME = ? ORDER BY RDBMS_NAME", this,rdbmsName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SUB_VERSION_NAME = :rdbmsSubVersionName'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsSubVersionNameEquals(String rdbmsSubVersionName) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SUB_VERSION_NAME = ? ORDER BY RDBMS_SUB_VERSION_NAME", this,rdbmsSubVersionName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_DESC = :rdbmsDesc'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsDescEquals(String rdbmsDesc) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DESC = ? ORDER BY RDBMS_DESC", this,rdbmsDesc);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_JDBC_DRIVER_CLASS = :rdbmsJdbcDriverClass'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsJdbcDriverClassEquals(String rdbmsJdbcDriverClass) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_JDBC_DRIVER_CLASS = ? ORDER BY RDBMS_JDBC_DRIVER_CLASS", this,rdbmsJdbcDriverClass);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_DEFAULT_PORT = :rdbmsDefaultPort'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsDefaultPortEquals(String rdbmsDefaultPort) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DEFAULT_PORT = ? ORDER BY RDBMS_DEFAULT_PORT", this,rdbmsDefaultPort);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_JDBC_DRIVER_URL = :rdbmsJdbcDriverUrl'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsJdbcDriverUrlEquals(String rdbmsJdbcDriverUrl) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_JDBC_DRIVER_URL = ? ORDER BY RDBMS_JDBC_DRIVER_URL", this,rdbmsJdbcDriverUrl);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_IS_WINDOWS_AUTH = :rdbmsIsWindowsAuth'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsIsWindowsAuthEquals(int rdbmsIsWindowsAuth) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_IS_WINDOWS_AUTH = ? ORDER BY RDBMS_IS_WINDOWS_AUTH", this,rdbmsIsWindowsAuth);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_DATABASE_NAME = ? ORDER BY RDBMS_DATABASE_NAME", this,rdbmsDatabaseName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_INSTANCE_NAME = :rdbmsInstanceName'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsInstanceNameEquals(String rdbmsInstanceName) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_INSTANCE_NAME = ? ORDER BY RDBMS_INSTANCE_NAME", this,rdbmsInstanceName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_USERNAME = :rdbmsServerUsername'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerUsernameEquals(String rdbmsServerUsername) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_USERNAME = ? ORDER BY RDBMS_SERVER_USERNAME", this,rdbmsServerUsername);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_PASSWORD = :rdbmsServerPassword'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerPasswordEquals(String rdbmsServerPassword) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_PASSWORD = ? ORDER BY RDBMS_SERVER_PASSWORD", this,rdbmsServerPassword);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_MACHINE_NAME = :rdbmsServerMachineName'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerMachineNameEquals(String rdbmsServerMachineName) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_MACHINE_NAME = ? ORDER BY RDBMS_SERVER_MACHINE_NAME", this,rdbmsServerMachineName);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_PORT = :rdbmsServerPort'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerPortEquals(String rdbmsServerPort) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE RDBMS_SERVER_PORT = ? ORDER BY RDBMS_SERVER_PORT", this,rdbmsServerPort);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_CDATETIME = :datasourceCdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceCdatetimeEquals(String datasourceCdatetime) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_CDATETIME = ? ORDER BY DATASOURCE_CDATETIME", this,datasourceCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_MDATETIME = :datasourceMdatetime'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceMdatetimeEquals(String datasourceMdatetime) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_MDATETIME = ? ORDER BY DATASOURCE_MDATETIME", this,datasourceMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_IS_DELETED = :datasourceIsDeleted'.
	 */
	@Transactional
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceIsDeletedEquals(short datasourceIsDeleted) throws IprodReportsViewReportDatasourceDetailsDaoException
	{
		try {
			return jdbcTemplate.query("SELECT DATASOURCE_ID, DATASOURCE_NAME, RDBMS_ID, RDBMS_NAME, RDBMS_SUB_VERSION_NAME, RDBMS_DESC, RDBMS_JDBC_DRIVER_CLASS, RDBMS_DEFAULT_PORT, RDBMS_JDBC_DRIVER_URL, RDBMS_IS_WINDOWS_AUTH, RDBMS_DATABASE_NAME, RDBMS_INSTANCE_NAME, RDBMS_SERVER_USERNAME, RDBMS_SERVER_PASSWORD, RDBMS_SERVER_MACHINE_NAME, RDBMS_SERVER_PORT, DATASOURCE_CDATETIME, DATASOURCE_MDATETIME, DATASOURCE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_IS_DELETED = ? ORDER BY DATASOURCE_IS_DELETED", this,datasourceIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsViewReportDatasourceDetailsDaoException("Query failed", e);
		}
		
	}

}
