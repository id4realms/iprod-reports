package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodImageMasterDao;
import com.id4.iprod.reports.dto.IprodImageMaster;
import com.id4.iprod.reports.dto.IprodImageMasterPk;
import com.id4.iprod.reports.exceptions.IprodImageMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodImageMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodImageMaster>, IprodImageMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodImageMasterPk
	 */
	public IprodImageMasterPk insert(IprodImageMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( IMAGE_NAME, IMAGE_CONTENT, IS_DELETED ) VALUES ( ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getImageName(),dto.getImageContent(),dto.getIsDeleted()} );
		IprodImageMasterPk pk = new IprodImageMasterPk();
		pk.setImageId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_image_master table.
	 */
	public void update(IprodImageMasterPk pk, IprodImageMaster dto) throws IprodImageMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET IMAGE_NAME = ?, IMAGE_CONTENT = ?, IS_DELETED = ? WHERE IMAGE_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.LONGVARBINARY) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getImageName(),dto.getImageContent(),dto.getIsDeleted(),pk.getImageId() } );
	}

	/** 
	 * Deletes a single row in the iprod_image_master table.
	 */
	@Transactional
	public void delete(IprodImageMasterPk pk) throws IprodImageMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE IMAGE_ID = ?",pk.getImageId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodImageMaster
	 */
	public IprodImageMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodImageMaster dto = new IprodImageMaster();
		dto.setImageId( rs.getInt( 1 ) );
		dto.setImageName( rs.getString( 2 ) );
		dto.setImageContent( super.getBlobColumn(rs, 3 ) );
		dto.setIsDeleted( rs.getShort( 4 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_image_master";
	}

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_ID = :imageId'.
	 */
	@Transactional
	public IprodImageMaster findByPrimaryKey(int imageId) throws IprodImageMasterDaoException
	{
		try {
			List<IprodImageMaster> list = jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " WHERE IMAGE_ID = ?", this,imageId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodImageMaster> findAll() throws IprodImageMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " ORDER BY IMAGE_ID", this);
		}
		catch (Exception e) {
			System.out.println("Exception::"+ e.getMessage());
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_ID = :imageId'.
	 */
	@Transactional
	public List<IprodImageMaster> findWhereImageIdEquals(int imageId) throws IprodImageMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " WHERE IMAGE_ID = ? ORDER BY IMAGE_ID", this,imageId);
		}
		catch (Exception e) {
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_NAME = :imageName'.
	 */
	@Transactional
	public List<IprodImageMaster> findWhereImageNameEquals(String imageName) throws IprodImageMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " WHERE IMAGE_NAME = ? ORDER BY IMAGE_NAME", this,imageName);
		}
		catch (Exception e) {
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IMAGE_CONTENT = :imageContent'.
	 */
	@Transactional
	public List<IprodImageMaster> findWhereImageContentEquals(byte[] imageContent) throws IprodImageMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " WHERE IMAGE_CONTENT = ? ORDER BY IMAGE_CONTENT", this,imageContent);
		}
		catch (Exception e) {
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_image_master table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	@Transactional
	public List<IprodImageMaster> findWhereIsDeletedEquals(short isDeleted) throws IprodImageMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " WHERE IS_DELETED = ? ORDER BY IS_DELETED", this,isDeleted);
		}
		catch (Exception e) {
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_image_master table that matches the specified primary-key value.
	 */
	public IprodImageMaster findByPrimaryKey(IprodImageMasterPk pk) throws IprodImageMasterDaoException
	{
		return findByPrimaryKey( pk.getImageId() );
	}
	
	public void updateIsDeleted(short isDeleted, int imageId) throws IprodImageMasterDaoException {
		jdbcTemplate.update("UPDATE " + getTableName() + " SET IS_DELETED = ? WHERE Image_ID=?",isDeleted,imageId);	
	}

	@Transactional
	public List<IprodImageMaster> findWhereImageNameAndIsDeletedEquals(String imageName,short isDeleted) throws IprodImageMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " WHERE IMAGE_NAME = ? AND IS_DELETED = ? ORDER BY IMAGE_NAME", this,imageName,isDeleted);
		}
		catch (Exception e) {
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}
	
	@Transactional
	public List<IprodImageMaster> findWhereImageIdAndIsDeletedEquals(int imageId,short isDeleted) throws IprodImageMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT IMAGE_ID, IMAGE_NAME, IMAGE_CONTENT, IS_DELETED FROM " + getTableName() + " WHERE IMAGE_ID = ? AND IS_DELETED = ? ORDER BY IMAGE_NAME", this,imageId,isDeleted);
		}
		catch (Exception e) {
			throw new IprodImageMasterDaoException("Query failed", e);
		}
		
	}
}
