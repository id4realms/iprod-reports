package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsDatasourceInstanceDao;
import com.id4.iprod.reports.dto.IprodReportsDatasourceInstance;
import com.id4.iprod.reports.dto.IprodReportsDatasourceInstancePk;
import com.id4.iprod.reports.exceptions.IprodReportsDatasourceInstanceDaoException;
import com.id4.iprod.reports.exceptions.IprodReportsTemplateMasterDaoException;

import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsDatasourceInstanceDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsDatasourceInstancePk
	 */
	public IprodReportsDatasourceInstancePk insert(IprodReportsDatasourceInstance dto);

	/** 
	 * Updates a single row in the iprod_reports_datasource_instance table.
	 */
	public void update(IprodReportsDatasourceInstancePk pk, IprodReportsDatasourceInstance dto) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Deletes a single row in the iprod_reports_datasource_instance table.
	 */
	public void delete(IprodReportsDatasourceInstancePk pk) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	public IprodReportsDatasourceInstance findByPrimaryKey(int datasourceId) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria ''.
	 */
	public List<IprodReportsDatasourceInstance> findAll() throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_JDBC_DRIVER_URL = :rdbmsJdbcDriverUrl'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsJdbcDriverUrlEquals(String rdbmsJdbcDriverUrl) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_IS_WINDOWS_AUTH = :rdbmsIsWindowsAuth'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsIsWindowsAuthEquals(int rdbmsIsWindowsAuth) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_INSTANCE_NAME = :rdbmsInstanceName'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsInstanceNameEquals(String rdbmsInstanceName) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_USERNAME = :rdbmsServerUsername'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerUsernameEquals(String rdbmsServerUsername) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_PASSWORD = :rdbmsServerPassword'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerPasswordEquals(String rdbmsServerPassword) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_MACHINE_NAME = :rdbmsServerMachineName'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerMachineNameEquals(String rdbmsServerMachineName) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'RDBMS_SERVER_PORT = :rdbmsServerPort'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereRdbmsServerPortEquals(String rdbmsServerPort) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_CDATETIME = :datasourceCdatetime'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereDatasourceCdatetimeEquals(String datasourceCdatetime) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_MDATETIME = :datasourceMdatetime'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereDatasourceMdatetimeEquals(String datasourceMdatetime) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns all rows from the iprod_reports_datasource_instance table that match the criteria 'DATASOURCE_IS_DELETED = :datasourceIsDeleted'.
	 */
	public List<IprodReportsDatasourceInstance> findWhereDatasourceIsDeletedEquals(short datasourceIsDeleted) throws IprodReportsDatasourceInstanceDaoException;

	/** 
	 * Returns the rows from the iprod_reports_datasource_instance table that matches the specified primary-key value.
	 */
	public IprodReportsDatasourceInstance findByPrimaryKey(IprodReportsDatasourceInstancePk pk) throws IprodReportsDatasourceInstanceDaoException;
	
	public void updateIsDeleted(short datasourceIsDeleted, int datasourceId) throws IprodReportsDatasourceInstanceDaoException;
	
	public List<IprodReportsDatasourceInstance> findWhereDatasourceNameAndIsDeletedEquals(String datasourceName,short datasourceIsDeleted) throws IprodReportsDatasourceInstanceDaoException;

	public void updateDatasourceDetailsWhereDatasourceIdEquals(String datasourceName, int rdbmsId, String rdmsDatabaseName,String rdbmsJdbcDriverUrl, int datasourceId) throws IprodReportsDatasourceInstanceDaoException;

}
