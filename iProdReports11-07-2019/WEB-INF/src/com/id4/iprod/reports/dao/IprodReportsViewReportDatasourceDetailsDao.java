package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsViewReportDatasourceDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportDatasourceDetails;
import com.id4.iprod.exceptions.IprodReportsViewReportDatasourceDetailsDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsViewReportDatasourceDetailsDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 */
	public void insert(IprodReportsViewReportDatasourceDetails dto);

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria ''.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findAll() throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_NAME = :datasourceName'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceNameEquals(String datasourceName) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_ID = :rdbmsId'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsIdEquals(int rdbmsId) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_NAME = :rdbmsName'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsNameEquals(String rdbmsName) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SUB_VERSION_NAME = :rdbmsSubVersionName'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsSubVersionNameEquals(String rdbmsSubVersionName) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_DESC = :rdbmsDesc'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsDescEquals(String rdbmsDesc) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_JDBC_DRIVER_CLASS = :rdbmsJdbcDriverClass'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsJdbcDriverClassEquals(String rdbmsJdbcDriverClass) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_DEFAULT_PORT = :rdbmsDefaultPort'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsDefaultPortEquals(String rdbmsDefaultPort) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_JDBC_DRIVER_URL = :rdbmsJdbcDriverUrl'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsJdbcDriverUrlEquals(String rdbmsJdbcDriverUrl) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_IS_WINDOWS_AUTH = :rdbmsIsWindowsAuth'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsIsWindowsAuthEquals(int rdbmsIsWindowsAuth) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_DATABASE_NAME = :rdbmsDatabaseName'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsDatabaseNameEquals(String rdbmsDatabaseName) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_INSTANCE_NAME = :rdbmsInstanceName'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsInstanceNameEquals(String rdbmsInstanceName) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_USERNAME = :rdbmsServerUsername'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerUsernameEquals(String rdbmsServerUsername) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_PASSWORD = :rdbmsServerPassword'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerPasswordEquals(String rdbmsServerPassword) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_MACHINE_NAME = :rdbmsServerMachineName'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerMachineNameEquals(String rdbmsServerMachineName) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'RDBMS_SERVER_PORT = :rdbmsServerPort'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereRdbmsServerPortEquals(String rdbmsServerPort) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_CDATETIME = :datasourceCdatetime'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceCdatetimeEquals(String datasourceCdatetime) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_MDATETIME = :datasourceMdatetime'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceMdatetimeEquals(String datasourceMdatetime) throws IprodReportsViewReportDatasourceDetailsDaoException;

	/** 
	 * Returns all rows from the iprod_reports_view_report_datasource_details table that match the criteria 'DATASOURCE_IS_DELETED = :datasourceIsDeleted'.
	 */
	public List<IprodReportsViewReportDatasourceDetails> findWhereDatasourceIsDeletedEquals(short datasourceIsDeleted) throws IprodReportsViewReportDatasourceDetailsDaoException;

}
