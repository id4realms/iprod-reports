package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsInstanceDao;
import com.id4.iprod.reports.dto.IprodReportsInstance;
import com.id4.iprod.reports.dto.IprodReportsInstancePk;
import com.id4.iprod.reports.exceptions.IprodReportsInstanceDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsInstanceDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsInstance>, IprodReportsInstanceDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsInstancePk
	 */
	public IprodReportsInstancePk insert(IprodReportsInstance dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.compile();
		su.update( new Object[] { dto.getRtemplateId(),dto.getUserId(),dto.getUserName(),dto.getReportQuery(),dto.getReportExcelLocation(),dto.getReportPdfLocation(),dto.getReportCdatetime(),dto.getReportMdatetime(),dto.getIsScheduled(),dto.getIsDeleted()} );
		IprodReportsInstancePk pk = new IprodReportsInstancePk();
		pk.setReportId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_reports_instance table.
	 */
	public void update(IprodReportsInstancePk pk, IprodReportsInstance dto) throws IprodReportsInstanceDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET RTEMPLATE_ID = ?, USER_ID = ?, USER_NAME = ?, REPORT_QUERY = ?, REPORT_EXCEL_LOCATION = ?, REPORT_PDF_LOCATION = ?, REPORT_CDATETIME = ?, REPORT_MDATETIME = ?, IS_SCHEDULED = ?, IS_DELETED = ? WHERE REPORT_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.NUMERIC) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getRtemplateId(),dto.getUserId(),dto.getUserName(),dto.getReportQuery(),dto.getReportExcelLocation(),dto.getReportPdfLocation(),dto.getReportCdatetime(),dto.getReportMdatetime(),dto.getIsScheduled(),dto.getIsDeleted(),pk.getReportId() } );
	}

	/** 
	 * Deletes a single row in the iprod_reports_instance table.
	 */
	@Transactional
	public void delete(IprodReportsInstancePk pk) throws IprodReportsInstanceDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE REPORT_ID = ?",pk.getReportId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsInstance
	 */
	public IprodReportsInstance mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsInstance dto = new IprodReportsInstance();
		dto.setReportId( rs.getInt( 1 ) );
		dto.setRtemplateId( rs.getInt( 2 ) );
		dto.setUserId( rs.getInt( 3 ) );
		dto.setUserName( rs.getString( 4 ) );
		dto.setReportQuery( rs.getString( 5 ) );
		dto.setReportExcelLocation( rs.getString( 6 ) );
		dto.setReportPdfLocation( rs.getString( 7 ) );
		dto.setReportCdatetime( rs.getString( 8 ) );
		dto.setReportMdatetime( rs.getString( 9 ) );
		dto.setIsScheduled( rs.getInt( 10 ) );
		if (rs.wasNull()) {
			dto.setIsScheduledNull( true );
		}
		
		dto.setIsDeleted( rs.getInt( 11 ) );
		if (rs.wasNull()) {
			dto.setIsDeletedNull( true );
		}
		
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_instance";
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_ID = :reportId'.
	 */
	@Transactional
	public IprodReportsInstance findByPrimaryKey(int reportId) throws IprodReportsInstanceDaoException
	{
		try {
			List<IprodReportsInstance> list = jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_ID = ?", this,reportId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsInstance> findAll() throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " ORDER BY REPORT_ID", this);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_ID = :reportId'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereReportIdEquals(int reportId) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_ID = ? ORDER BY REPORT_ID", this,reportId);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_ID = ? ORDER BY RTEMPLATE_ID", this,rtemplateId);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'USER_ID = :userId'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereUserIdEquals(int userId) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE USER_ID = ? ORDER BY USER_ID", this,userId);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'USER_NAME = :userName'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereUserNameEquals(String userName) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE USER_NAME = ? ORDER BY USER_NAME", this,userName);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_QUERY = :reportQuery'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereReportQueryEquals(String reportQuery) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_QUERY = ? ORDER BY REPORT_QUERY", this,reportQuery);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_EXCEL_LOCATION = :reportExcelLocation'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereReportExcelLocationEquals(String reportExcelLocation) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_EXCEL_LOCATION = ? ORDER BY REPORT_EXCEL_LOCATION", this,reportExcelLocation);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_PDF_LOCATION = :reportPdfLocation'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereReportPdfLocationEquals(String reportPdfLocation) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_PDF_LOCATION = ? ORDER BY REPORT_PDF_LOCATION", this,reportPdfLocation);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_CDATETIME = :reportCdatetime'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereReportCdatetimeEquals(String reportCdatetime) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_CDATETIME = ? ORDER BY REPORT_CDATETIME", this,reportCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'REPORT_MDATETIME = :reportMdatetime'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereReportMdatetimeEquals(String reportMdatetime) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE REPORT_MDATETIME = ? ORDER BY REPORT_MDATETIME", this,reportMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'IS_SCHEDULED = :isScheduled'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereIsScheduledEquals(int isScheduled) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE IS_SCHEDULED = ? ORDER BY IS_SCHEDULED", this,isScheduled);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_instance table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	@Transactional
	public List<IprodReportsInstance> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsInstanceDaoException
	{
		try {
			return jdbcTemplate.query("SELECT REPORT_ID, RTEMPLATE_ID, USER_ID, USER_NAME, REPORT_QUERY, REPORT_EXCEL_LOCATION, REPORT_PDF_LOCATION, REPORT_CDATETIME, REPORT_MDATETIME, IS_SCHEDULED, IS_DELETED FROM " + getTableName() + " WHERE IS_DELETED = ? ORDER BY IS_DELETED", this,isDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsInstanceDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_reports_instance table that matches the specified primary-key value.
	 */
	public IprodReportsInstance findByPrimaryKey(IprodReportsInstancePk pk) throws IprodReportsInstanceDaoException
	{
		return findByPrimaryKey( pk.getReportId() );
	}
	
	public void updateIsScheduledWhereReportIdEquals(int isScheduled,int reportId) throws IprodReportsInstanceDaoException {
		jdbcTemplate.update("UPDATE " + getTableName() + " SET IS_SCHEDULED = ? WHERE REPORT_ID= ? ",isScheduled,reportId);
		
	}
	
	public void updateReportPDFLocationWhereReportIdIs(int reportId,String reportPdfLocation) throws IprodReportsInstanceDaoException {
		jdbcTemplate.update("UPDATE " + getTableName() + " SET REPORT_PDF_LOCATION = ? WHERE REPORT_ID= ? ",reportPdfLocation,reportId);
		
	}
	
	public void updateReportExcelLocationWhereReportIdIs(int reportId,String reportExcelLocation) throws IprodReportsInstanceDaoException {
		jdbcTemplate.update("UPDATE " + getTableName() + " SET REPORT_EXCEL_LOCATION = ? WHERE REPORT_ID= ? ",reportExcelLocation,reportId);
		
	}

}
