package com.id4.iprod.reports.dao.spring;

import com.id4.iprod.reports.dao.IprodReportsTemplateMasterDao;
import com.id4.iprod.reports.dto.IprodReportsTemplateMaster;
import com.id4.iprod.reports.dto.IprodReportsTemplateMasterPk;
import com.id4.iprod.reports.exceptions.IprodReportsTemplateMasterDaoException;
import java.util.List;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public class IprodReportsTemplateMasterDaoImpl extends AbstractDAO implements ParameterizedRowMapper<IprodReportsTemplateMaster>, IprodReportsTemplateMasterDao
{
	protected SimpleJdbcTemplate jdbcTemplate;

	protected DataSource dataSource;

	/**
	 * Method 'setDataSource'
	 * 
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}

	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsTemplateMasterPk
	 */
	public IprodReportsTemplateMasterPk insert(IprodReportsTemplateMaster dto)
	{
		SqlUpdate su = new SqlUpdate( dataSource, "INSERT INTO " + getTableName() + " ( RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.compile();
		su.update( new Object[] { dto.getRtemplateName(),dto.getRtemplateTitle(),dto.getRtemplateSubTitle(),dto.getRtemplateSqlquery(),dto.getRtemplateExcelDataStorage(),dto.getRtemplatePdfDataStorage(),dto.getRtemplateBirtRpt(),dto.getDatasourceId(),dto.getRtemplateCdatetime(),dto.getRtemplateMdatetime(),dto.getRtemplateIsDeleted()} );
		IprodReportsTemplateMasterPk pk = new IprodReportsTemplateMasterPk();
		pk.setRtemplateId( jdbcTemplate.queryForInt("select @@IDENTITY") );
		return pk;
	}

	/** 
	 * Updates a single row in the iprod_reports_template_master table.
	 */
	public void update(IprodReportsTemplateMasterPk pk, IprodReportsTemplateMaster dto) throws IprodReportsTemplateMasterDaoException
	{
		SqlUpdate su = new SqlUpdate( dataSource, "UPDATE " + getTableName() + " SET RTEMPLATE_NAME = ?, RTEMPLATE_TITLE = ?, RTEMPLATE_SUB_TITLE = ?, RTEMPLATE_SQLQUERY = ?, RTEMPLATE_EXCEL_DATA_STORAGE = ?, RTEMPLATE_PDF_DATA_STORAGE = ?, RTEMPLATE_BIRT_RPT = ?, DATASOURCE_ID = ?, RTEMPLATE_CDATETIME = ?, RTEMPLATE_MDATETIME = ?, RTEMPLATE_IS_DELETED = ? WHERE RTEMPLATE_ID = ?");
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.VARCHAR) );
		su.declareParameter( new SqlParameter( java.sql.Types.TINYINT) );
		su.declareParameter( new SqlParameter( java.sql.Types.INTEGER) );
		su.compile();
		su.update( new Object[] { dto.getRtemplateName(),dto.getRtemplateTitle(),dto.getRtemplateSubTitle(),dto.getRtemplateSqlquery(),dto.getRtemplateExcelDataStorage(),dto.getRtemplatePdfDataStorage(),dto.getRtemplateBirtRpt(),dto.getDatasourceId(),dto.getRtemplateCdatetime(),dto.getRtemplateMdatetime(),dto.getRtemplateIsDeleted(),pk.getRtemplateId() } );
	}

	/** 
	 * Deletes a single row in the iprod_reports_template_master table.
	 */
	@Transactional
	public void delete(IprodReportsTemplateMasterPk pk) throws IprodReportsTemplateMasterDaoException
	{
		jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE RTEMPLATE_ID = ?",pk.getRtemplateId());
	}

	/**
	 * Method 'mapRow'
	 * 
	 * @param rs
	 * @param row
	 * @throws SQLException
	 * @return IprodReportsTemplateMaster
	 */
	public IprodReportsTemplateMaster mapRow(ResultSet rs, int row) throws SQLException
	{
		IprodReportsTemplateMaster dto = new IprodReportsTemplateMaster();
		dto.setRtemplateId( rs.getInt( 1 ) );
		dto.setRtemplateName( rs.getString( 2 ) );
		dto.setRtemplateTitle( rs.getString( 3 ) );
		dto.setRtemplateSubTitle( rs.getString( 4 ) );
		dto.setRtemplateSqlquery( rs.getString( 5 ) );
		dto.setRtemplateExcelDataStorage( rs.getString( 6 ) );
		dto.setRtemplatePdfDataStorage( rs.getString( 7 ) );
		dto.setRtemplateBirtRpt( rs.getString( 8 ) );
		dto.setDatasourceId( rs.getInt( 9 ) );
		dto.setRtemplateCdatetime( rs.getString( 10 ) );
		dto.setRtemplateMdatetime( rs.getString( 11 ) );
		dto.setRtemplateIsDeleted( rs.getShort( 12 ) );
		return dto;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "iprod_reports_db..iprod_reports_template_master";
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	@Transactional
	public IprodReportsTemplateMaster findByPrimaryKey(int rtemplateId) throws IprodReportsTemplateMasterDaoException
	{
		try {
			List<IprodReportsTemplateMaster> list = jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_ID = ?", this,rtemplateId);
			return list.size() == 0 ? null : list.get(0);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria ''.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findAll() throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " ORDER BY RTEMPLATE_ID", this);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_ID = ? ORDER BY RTEMPLATE_ID", this,rtemplateId);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_NAME = :rtemplateName'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateNameEquals(String rtemplateName) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_NAME = ? ORDER BY RTEMPLATE_NAME", this,rtemplateName);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_TITLE = :rtemplateTitle'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateTitleEquals(String rtemplateTitle) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_TITLE = ? ORDER BY RTEMPLATE_TITLE", this,rtemplateTitle);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_SUB_TITLE = :rtemplateSubTitle'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateSubTitleEquals(String rtemplateSubTitle) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_SUB_TITLE = ? ORDER BY RTEMPLATE_SUB_TITLE", this,rtemplateSubTitle);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_SQLQUERY = :rtemplateSqlquery'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateSqlqueryEquals(String rtemplateSqlquery) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_SQLQUERY = ? ORDER BY RTEMPLATE_SQLQUERY", this,rtemplateSqlquery);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_EXCEL_DATA_STORAGE = :rtemplateExcelDataStorage'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateExcelDataStorageEquals(String rtemplateExcelDataStorage) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_EXCEL_DATA_STORAGE = ? ORDER BY RTEMPLATE_EXCEL_DATA_STORAGE", this,rtemplateExcelDataStorage);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_PDF_DATA_STORAGE = :rtemplatePdfDataStorage'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplatePdfDataStorageEquals(String rtemplatePdfDataStorage) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_PDF_DATA_STORAGE = ? ORDER BY RTEMPLATE_PDF_DATA_STORAGE", this,rtemplatePdfDataStorage);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_BIRT_RPT = :rtemplateBirtRpt'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateBirtRptEquals(String rtemplateBirtRpt) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_BIRT_RPT = ? ORDER BY RTEMPLATE_BIRT_RPT", this,rtemplateBirtRpt);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE DATASOURCE_ID = ? ORDER BY DATASOURCE_ID", this,datasourceId);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_CDATETIME = :rtemplateCdatetime'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateCdatetimeEquals(String rtemplateCdatetime) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_CDATETIME = ? ORDER BY RTEMPLATE_CDATETIME", this,rtemplateCdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_MDATETIME = :rtemplateMdatetime'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateMdatetimeEquals(String rtemplateMdatetime) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_MDATETIME = ? ORDER BY RTEMPLATE_MDATETIME", this,rtemplateMdatetime);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_IS_DELETED = :rtemplateIsDeleted'.
	 */
	@Transactional
	public List<IprodReportsTemplateMaster> findWhereRtemplateIsDeletedEquals(short rtemplateIsDeleted) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_IS_DELETED = ? ORDER BY RTEMPLATE_IS_DELETED", this,rtemplateIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}

	/** 
	 * Returns the rows from the iprod_reports_template_master table that matches the specified primary-key value.
	 */
	public IprodReportsTemplateMaster findByPrimaryKey(IprodReportsTemplateMasterPk pk) throws IprodReportsTemplateMasterDaoException
	{
		return findByPrimaryKey( pk.getRtemplateId() );
	}
	
	public List<IprodReportsTemplateMaster> findWhereRtemplateNameAndlsDeletedEquals(String rtemplateName,short rtemplateIsDeleted) throws IprodReportsTemplateMasterDaoException
	{
		try {
			return jdbcTemplate.query("SELECT RTEMPLATE_ID, RTEMPLATE_NAME, RTEMPLATE_TITLE, RTEMPLATE_SUB_TITLE, RTEMPLATE_SQLQUERY, RTEMPLATE_EXCEL_DATA_STORAGE, RTEMPLATE_PDF_DATA_STORAGE, RTEMPLATE_BIRT_RPT, DATASOURCE_ID, RTEMPLATE_CDATETIME, RTEMPLATE_MDATETIME, RTEMPLATE_IS_DELETED FROM " + getTableName() + " WHERE RTEMPLATE_NAME = ? AND RTEMPLATE_IS_DELETED = ? ORDER BY RTEMPLATE_NAME", this,rtemplateName,rtemplateIsDeleted);
		}
		catch (Exception e) {
			throw new IprodReportsTemplateMasterDaoException("Query failed", e);
		}
		
	}
	
	
	public void updateExcelDataStorageWhereReportTemplateNameIs(String rtemplateExcelDataStorage,String rtemplateName) throws IprodReportsTemplateMasterDaoException
	{
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update("UPDATE " + getTableName() + " SET RTEMPLATE_EXCEL_DATA_STORAGE = ? WHERE RTEMPLATE_NAME = ? ",rtemplateExcelDataStorage,rtemplateName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(" Exception while update updateDataStorage query :: " + e.getMessage());
		}

	}
	
		public void updatePdfDataStorageWhereReportTemplateNameIs(String rtemplatePdfDataStorage,String rtemplateName) throws IprodReportsTemplateMasterDaoException
	{
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update("UPDATE " + getTableName() + " SET RTEMPLATE_PDF_DATA_STORAGE = ? WHERE RTEMPLATE_NAME = ? ",rtemplatePdfDataStorage,rtemplateName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(" Exception while update updateDataStorage query :: " + e.getMessage());
		}

	}

}
