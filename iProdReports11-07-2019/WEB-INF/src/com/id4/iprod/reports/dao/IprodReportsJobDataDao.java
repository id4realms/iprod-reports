package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsJobDataDao;
import com.id4.iprod.reports.dto.IprodReportsJobData;
import com.id4.iprod.reports.dto.IprodReportsJobDataPk;
import com.id4.iprod.reports.exceptions.IprodReportsJobDataDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsJobDataDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsJobDataPk
	 */
	public IprodReportsJobDataPk insert(IprodReportsJobData dto);

	/** 
	 * Updates a single row in the iprod_reports_job_data table.
	 */
	public void update(IprodReportsJobDataPk pk, IprodReportsJobData dto) throws IprodReportsJobDataDaoException;

	/** 
	 * Deletes a single row in the iprod_reports_job_data table.
	 */
	public void delete(IprodReportsJobDataPk pk) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_ID = :jobId'.
	 */
	public IprodReportsJobData findByPrimaryKey(int jobId) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria ''.
	 */
	public List<IprodReportsJobData> findAll() throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_ID = :jobId'.
	 */
	public List<IprodReportsJobData> findWhereJobIdEquals(int jobId) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_NAME = :jobName'.
	 */
	public List<IprodReportsJobData> findWhereJobNameEquals(String jobName) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'JOB_DESCRIPTION = :jobDescription'.
	 */
	public List<IprodReportsJobData> findWhereJobDescriptionEquals(String jobDescription) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'RECIPIENTS = :recipients'.
	 */
	public List<IprodReportsJobData> findWhereRecipientsEquals(String recipients) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'REPEAT_INTERVAL = :repeatInterval'.
	 */
	public List<IprodReportsJobData> findWhereRepeatIntervalEquals(String repeatInterval) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'START_DATE_TIME = :startDateTime'.
	 */
	public List<IprodReportsJobData> findWhereStartDateTimeEquals(String startDateTime) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'END_DATE_TIME = :endDateTime'.
	 */
	public List<IprodReportsJobData> findWhereEndDateTimeEquals(String endDateTime) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'ACTIVE = :active'.
	 */
	public List<IprodReportsJobData> findWhereActiveEquals(int active) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns all rows from the iprod_reports_job_data table that match the criteria 'IS_DELETED = :isDeleted'.
	 */
	public List<IprodReportsJobData> findWhereIsDeletedEquals(int isDeleted) throws IprodReportsJobDataDaoException;

	/** 
	 * Returns the rows from the iprod_reports_job_data table that matches the specified primary-key value.
	 */
	public IprodReportsJobData findByPrimaryKey(IprodReportsJobDataPk pk) throws IprodReportsJobDataDaoException;

}
