package com.id4.iprod.reports.dao;

import com.id4.iprod.reports.dao.IprodReportsTemplateMasterDao;
import com.id4.iprod.reports.dto.IprodReportsTemplateMaster;
import com.id4.iprod.reports.dto.IprodReportsTemplateMasterPk;
import com.id4.iprod.reports.exceptions.IprodReportsTemplateMasterDaoException;
import java.util.List;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

public interface IprodReportsTemplateMasterDao
{
	/**
	 * Method 'insert'
	 * 
	 * @param dto
	 * @return IprodReportsTemplateMasterPk
	 */
	public IprodReportsTemplateMasterPk insert(IprodReportsTemplateMaster dto);

	/** 
	 * Updates a single row in the iprod_reports_template_master table.
	 */
	public void update(IprodReportsTemplateMasterPk pk, IprodReportsTemplateMaster dto) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Deletes a single row in the iprod_reports_template_master table.
	 */
	public void delete(IprodReportsTemplateMasterPk pk) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	public IprodReportsTemplateMaster findByPrimaryKey(int rtemplateId) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria ''.
	 */
	public List<IprodReportsTemplateMaster> findAll() throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_ID = :rtemplateId'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateIdEquals(int rtemplateId) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_NAME = :rtemplateName'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateNameEquals(String rtemplateName) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_TITLE = :rtemplateTitle'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateTitleEquals(String rtemplateTitle) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_SUB_TITLE = :rtemplateSubTitle'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateSubTitleEquals(String rtemplateSubTitle) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_SQLQUERY = :rtemplateSqlquery'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateSqlqueryEquals(String rtemplateSqlquery) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_EXCEL_DATA_STORAGE = :rtemplateExcelDataStorage'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateExcelDataStorageEquals(String rtemplateExcelDataStorage) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_PDF_DATA_STORAGE = :rtemplatePdfDataStorage'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplatePdfDataStorageEquals(String rtemplatePdfDataStorage) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_BIRT_RPT = :rtemplateBirtRpt'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateBirtRptEquals(String rtemplateBirtRpt) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'DATASOURCE_ID = :datasourceId'.
	 */
	public List<IprodReportsTemplateMaster> findWhereDatasourceIdEquals(int datasourceId) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_CDATETIME = :rtemplateCdatetime'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateCdatetimeEquals(String rtemplateCdatetime) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_MDATETIME = :rtemplateMdatetime'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateMdatetimeEquals(String rtemplateMdatetime) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns all rows from the iprod_reports_template_master table that match the criteria 'RTEMPLATE_IS_DELETED = :rtemplateIsDeleted'.
	 */
	public List<IprodReportsTemplateMaster> findWhereRtemplateIsDeletedEquals(short rtemplateIsDeleted) throws IprodReportsTemplateMasterDaoException;

	/** 
	 * Returns the rows from the iprod_reports_template_master table that matches the specified primary-key value.
	 */
	public IprodReportsTemplateMaster findByPrimaryKey(IprodReportsTemplateMasterPk pk) throws IprodReportsTemplateMasterDaoException;
	
	public List<IprodReportsTemplateMaster> findWhereRtemplateNameAndlsDeletedEquals(String rtemplateName,short rtemplateIsDeleted) throws IprodReportsTemplateMasterDaoException;

	public void updateExcelDataStorageWhereReportTemplateNameIs(String rtemplateExcelDataStorage,String rtemplateName) throws IprodReportsTemplateMasterDaoException;

	public void updatePdfDataStorageWhereReportTemplateNameIs(String rtemplatePdfDataStorage,String rtemplateName) throws IprodReportsTemplateMasterDaoException;

}
