package com.id4.iprod.reports;	

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.Consts;
import com.id4.iprod.IprodViewUserDetailsAction;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.id4.iprod.reports.dao.IprodReportsRecipientMasterDao;
import com.id4.iprod.reports.dto.IprodReportsRecipientMaster;
import com.id4.iprod.reports.dto.IprodReportsRecipientMasterPk;
import com.id4.iprod.reports.exceptions.IprodReportsRecipientMasterDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodReportsRecipientMasterAction extends ActionSupport implements IprodModuleAuthorization{
	static Logger log = Logger.getLogger(IprodReportsRecipientMasterAction.class);
	/** 
	* This attribute maps to the column RECIPIENT_ID in the IprodReports_recipient_master table.
	*/
	protected int recipientId;
	
	/** 
	* This attribute maps to the column FIRST_NAME in the IprodReports_recipient_master table.
	*/
	protected String firstName;

	/** 
	* This attribute maps to the column LAST_NAME in the IprodReports_recipient_master table.
	*/
	protected String lastName;

	/** 
	* This attribute maps to the column EMAIL_ID in the IprodReports_recipient_master table.
	*/
	protected String emailId;
				
	/** 
	* This attribute maps to the column CONTACT_NO in the IprodReports_recipient_master table.
	*/
	protected String contactNo;

	/** 
	* This attribute maps to the column IS_DELETED in the IprodReports_recipient_master table.
	*/
	protected int isDeleted;
				
	/** 
	* This attribute maps to the column ROLE_ID in the hms_recipient table.
	*/
	protected int roleId;
	/** 
	* This attribute maps to the column ROLE_NAME in the RoleMaster table.
	*/
	protected String roleName;
	protected String newRecipientEmailId;
	protected String recipientCdatetime;
	protected String recipientMdatetime;
	
	public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();
	public List<IprodReportsRecipientMaster> selectRecipientDetailsListG = new ArrayList<IprodReportsRecipientMaster>();
	public List<IprodReportsRecipientMaster> AllRecipientDetailsListG = new ArrayList<IprodReportsRecipientMaster>();


   /**
	* Method 'execute' - default execution
	* 
	*/
	public String execute(){
	return SUCCESS;
	}

	public String addNewRecipient(){
	 log.info("IprodReportsRecipientMasterAction: addRecipient()"); 
	 Map session = ActionContext.getContext().getSession();
	 if(session.get(Consts.IPRODUSERID)!= null){
		 
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat ("dd MMM yyyy HH:mm:ss:mm");
		String currentDateTime = ft.format(dNow);
		
		IprodReportsRecipientMasterDao recipientDao = DaoFactory.createIprodReportsRecipientMasterDao();
		IprodReportsRecipientMaster IprodReportsRecipient = new IprodReportsRecipientMaster();
		IprodReportsRecipient.setFirstName(this.firstName);
		log.info("firstName ::"+this.firstName);
		IprodReportsRecipient.setLastName(this.lastName);
		IprodReportsRecipient.setEmailId(this.emailId);
		IprodReportsRecipient.setContactNo(this.contactNo);		  
		IprodReportsRecipient.setIsDeleted((short)0);
		IprodReportsRecipient.setRecipientCdatetime(currentDateTime);
		IprodReportsRecipient.setRecipientMdatetime(currentDateTime);
		IprodReportsRecipientMasterPk RecipientPk = new IprodReportsRecipientMasterPk(); 
		RecipientPk = recipientDao.insert(IprodReportsRecipient);
		log.info("Recipient "+this.firstName+" has been successfully added");	
		return SUCCESS;
	 }else {
		addActionError("Sorry!! You have to Login first");
		return "login";
     } 
  }

  public String findAllRecipientDetails(){
  log.info("IprodReportsRecipientMasterAction : findAllRecipientDetails()");
  Map session = ActionContext.getContext().getSession();
  if(session.get(Consts.IPRODUSERID)!= null){
		IprodReportsRecipientMasterDao RecipientDao = DaoFactory.createIprodReportsRecipientMasterDao();
		List<IprodReportsRecipientMaster> AllRecipientDetailsListL = new ArrayList<IprodReportsRecipientMaster>();
		try {	
			AllRecipientDetailsListL = RecipientDao.findWhereIsDeletedEquals((short)0); 			  	 
			if(AllRecipientDetailsListL.size() > 0){
			 AllRecipientDetailsListG = AllRecipientDetailsListL;
			 session.put("AllRecipientDetailsListG1",AllRecipientDetailsListG);
			 return SUCCESS;
			}else{
			 System.out.println("Currently there are no recipient to be displayed");
			 return ERROR;
			}
		}catch (IprodReportsRecipientMasterDaoException e) {
			// TODO catch block
			//addActionError(""+this.complaintId);
			addActionError("IprodReportsdRecipientDaoException"+e.getMessage());
			return ERROR;
		}
	 }else{
		addActionError("Sorry!! You have to Login first");
		return "login";
	}
  }
  
   public String findByRecipientId(){
	log.info("IprodReportsRecipientMasterAction : findByRecipientId()");
	Map session = ActionContext.getContext().getSession();
	if(session.get(Consts.IPRODUSERID)!= null){
		// Integer recipientId = (Integer) session.get("IprodReportsUsertId");
		IprodReportsRecipientMasterDao RecipientDao = DaoFactory.createIprodReportsRecipientMasterDao();				  
		List<IprodReportsRecipientMaster> selectRecipientDetailsListL = new ArrayList<IprodReportsRecipientMaster>();			  
		try{
		 if(recipientId != 0)
		 selectRecipientDetailsListL = RecipientDao.findWhereRecipientIdEquals(this.recipientId);
		 if(selectRecipientDetailsListL.size() > 0){
		  selectRecipientDetailsListG = selectRecipientDetailsListL;
		  return SUCCESS;
		 }else{	
		  log.info("Failed to fetch selected recipient details");
		  addActionError("Failed to fetch recipient details");
		  return ERROR;
		 }			
	    }catch(IprodReportsRecipientMasterDaoException e) {
		 // TODO catch block				
		 addActionError("IprodReportsRecipientDaoException"+e.getMessage());
		 return ERROR;
		}
	}else{
		addActionError("Sorry!! You have to Login first");
	   return "login";
	}
				    	
   }

   public String deleteRecipient(){
	log.info("IprodReportsRecipientMasterAction: deleteRecipient()");
    Map session = ActionContext.getContext().getSession();
	if(session.get(Consts.IPRODUSERID)!= null){
		IprodReportsRecipientMasterDao recipientDao = DaoFactory.createIprodReportsRecipientMasterDao();
		IprodReportsRecipientMaster IprodReportsRecipient = new IprodReportsRecipientMaster();
		List<IprodReportsRecipientMaster> RecipientMasterList = new ArrayList<IprodReportsRecipientMaster>();
		try{
		 RecipientMasterList = recipientDao.findWhereRecipientIdEquals(this.recipientId);
		 IprodReportsRecipientMasterPk RecipientPk = new IprodReportsRecipientMasterPk(); 
		 RecipientPk.setRecipientId(this.recipientId);
		 IprodReportsRecipient.setRecipientId(this.recipientId);
		 IprodReportsRecipient.setFirstName(RecipientMasterList.get(0).getFirstName());
		 IprodReportsRecipient.setLastName(RecipientMasterList.get(0).getLastName());
		 IprodReportsRecipient.setEmailId(RecipientMasterList.get(0).getEmailId());
		 IprodReportsRecipient.setContactNo(RecipientMasterList.get(0).getContactNo());
		 IprodReportsRecipient.setIsDeleted((short) 1);
		 recipientDao.update(RecipientPk,IprodReportsRecipient);
		 log.info("Recipient id "+this.recipientId+" has been deleted");
	     }catch (IprodReportsRecipientMasterDaoException e) {
		 // TODO catch block
		 addActionError("IprodReportsRecipientDaoException"+e.getMessage());
		 return ERROR;
		}
	  }else{							 
		addActionError("Sorry!! You have to Login first");
		return "login";
	}
	addActionMessage("Recipient id "+this.recipientId+" has been deleted");
	 return SUCCESS;							    
   } 

   public String updateRecipient(){
	log.info("IprodReportsRecipientMasterAction: updateRecipient()");
	Map session = ActionContext.getContext().getSession();
	if(session.get(Consts.IPRODUSERID)!= null){
		IprodReportsRecipientMasterDao RecipientDao = DaoFactory.createIprodReportsRecipientMasterDao();
		IprodReportsRecipientMaster Recipientmaster = new IprodReportsRecipientMaster();
		Recipientmaster.setRecipientId(this.recipientId);
		Recipientmaster.setFirstName(this.firstName);
		Recipientmaster.setLastName(this.lastName);
		Recipientmaster.setEmailId(this.emailId);
		Recipientmaster.setContactNo(this.contactNo);
		IprodReportsRecipientMasterPk RecipientPk = new IprodReportsRecipientMasterPk(); 
		RecipientPk.setRecipientId(this.recipientId);		
		try {
		RecipientDao.update(RecipientPk, Recipientmaster);
		log.info("Recipient "+Recipientmaster.getRecipientId()+" has been updated");
	   } catch (IprodReportsRecipientMasterDaoException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return ERROR;
	  }
	}else{
	 addActionError("Sorry!! You have to Login first");
	 return "login";
    }   
	addActionMessage("Recipient "+this.firstName+" has been successfully updated");	
	return SUCCESS;
}

	/**
	 * Method 'getRecipientId'
	 * 
	 * @return int
	 */
	public int getRecipientId()
	{
		return recipientId;
	}

	/**
	 * Method 'setRecipientId'
	 * 
	 * @param recipientId
	 */
	public void setRecipientId(int recipientId)
	{
		this.recipientId = recipientId;
	}

	/**
	 * Method 'getFirstName'
	 * 
	 * @return String
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Method 'setFirstName'
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Method 'getLastName'
	 * 
	 * @return String
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Method 'setLastName'
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Method 'getEmailId'
	 * 
	 * @return String
	 */
	public String getEmailId()
	{
		return emailId;
	}

	/**
	 * Method 'setEmailId'
	 * 
	 * @param emailId
	 */
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	/**
	 * Method 'getContactNo'
	 * 
	 * @return String
	 */
	public String getContactNo()
	{
		return contactNo;
	}

	/**
	 * Method 'setContactNo'
	 * 
	 * @param contactNo
	 */
	public void setContactNo(String contactNo)
	{
		this.contactNo = contactNo;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public int getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Method 'getRecipientCdatetime'
	 * 
	 * @return String
	 */
	public String getRecipientCdatetime()
	{
		return recipientCdatetime;
	}

	/**
	 * Method 'setRecipientCdatetime'
	 * 
	 * @param recipientCdatetime
	 */
	public void setRecipientCdatetime(String recipientCdatetime)
	{
		this.recipientCdatetime = recipientCdatetime;
	}

	/**
	 * Method 'getRecipientMdatetime'
	 * 
	 * @return String
	 */
	public String getRecipientMdatetime()
	{
		return recipientMdatetime;
	}

	/**
	 * Method 'setRecipientMdatetime'
	 * 
	 * @param recipientMdatetime
	 */
	public void setRecipientMdatetime(String recipientMdatetime)
	{
		this.recipientMdatetime = recipientMdatetime;
	}
	
	public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
		return hashMapCheckRoleModuleOperationListG;
	}


	public void setHashMapCheckRoleModuleOperationListG(HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
		this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsRecipientMasterAction: " );
		ret.append( "recipientId=" + recipientId );
		ret.append( ", firstName=" + firstName );
		ret.append( ", lastName=" + lastName );
		ret.append( ", emailId=" + emailId );
		ret.append( ", contactNo=" + contactNo );
		ret.append( ", isDeleted=" + isDeleted );
		ret.append( ", recipientCdatetime=" + recipientCdatetime );
		ret.append( ", recipientMdatetime=" + recipientMdatetime );
		return ret.toString();
	}

}