package com.id4.iprod.reports;

import java.util.List;

public class IprodReportsGenerateReportFilterColumn {
	
	protected String id;
	
	protected String label;
	
	protected String type;
	
	protected String plugin;
	
	protected List<String> operators;
	
	protected IprodReportsGenerateReportFilterDatePickerPluginConfig plugin_config;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPlugin() {
		return plugin;
	}

	public void setPlugin(String plugin) {
		this.plugin = plugin;
	}

	public List<String> getOperators() {
		return operators;
	}

	public void setOperators(List<String> operators) {
		this.operators = operators;
	}

	public IprodReportsGenerateReportFilterDatePickerPluginConfig getPlugin_config() {
		return plugin_config;
	}

	public void setPlugin_config(IprodReportsGenerateReportFilterDatePickerPluginConfig plugin_config) {
		this.plugin_config = plugin_config;
	}
	
	

}
