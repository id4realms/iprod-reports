package com.id4.iprod.reports;

public class IprodReportsTableColumn {
	
	public String columnName;
	public String columnDataType;
	
	
	public String getColumnName() {
		return columnName;
	}
	
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnDataType() {
		return columnDataType;
	}
	public void setColumnDataType(String columnDataType) {
		this.columnDataType = columnDataType;
	}
	
}
