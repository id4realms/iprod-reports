package com.id4.iprod.reports;

public class IprodReportsSQLDataset {

	protected String rtemplateSqlquery;
    protected Integer datasourceId;
	protected String dataFieldName;
    protected Integer rtemplateId;
    
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

		public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	public Integer getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(Integer datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getDataFieldName() {
		return dataFieldName;
	}

	public void setDataFieldName(String dataFieldName) {
		this.dataFieldName = dataFieldName;
	}

	public Integer getRtemplateId() {
		return rtemplateId;
	}

	public void setRtemplateId(Integer rtemplateId) {
		this.rtemplateId = rtemplateId;
	}

	
}
