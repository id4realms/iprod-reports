package com.id4.iprod.reports;

public class IprodReportsGenerateReportFilterDatePickerPluginConfig {
    
    protected String format;

	protected String todayBtn;
	
	protected boolean todayHighlight;
	
	protected boolean autoclose;

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getTodayBtn() {
		return todayBtn;
	}

	public void setTodayBtn(String todayBtn) {
		this.todayBtn = todayBtn;
	}

	public boolean isTodayHighlight() {
		return todayHighlight;
	}

	public void setTodayHighlight(boolean todayHighlight) {
		this.todayHighlight = todayHighlight;
	}

	public boolean isAutoclose() {
		return autoclose;
	}

	public void setAutoclose(boolean autoclose) {
		this.autoclose = autoclose;
	}

}
