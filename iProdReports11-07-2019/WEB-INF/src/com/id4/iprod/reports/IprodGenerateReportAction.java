package com.id4.iprod.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.model.api.CellHandle;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.GridHandle;
import org.eclipse.birt.report.model.api.LabelHandle;
import org.eclipse.birt.report.model.api.OdaDataSetHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.SlotHandle;
import org.eclipse.birt.report.model.api.SlotIterator;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.command.ContentException;
import org.eclipse.birt.report.model.api.command.NameException;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.reports.dao.IprodReportsInstanceDao;
import com.id4.iprod.reports.dao.IprodReportsTemplateMasterDao;
import com.id4.iprod.reports.dto.IprodReportsInstance;
import com.id4.iprod.reports.dto.IprodReportsTemplateMaster;
import com.id4.iprod.reports.exceptions.IprodReportsInstanceDaoException;
import com.id4.iprod.reports.exceptions.IprodReportsTemplateMasterDaoException;
import com.opensymphony.xwork2.ActionSupport;

public class IprodGenerateReportAction extends ActionSupport implements Job {

	static Logger log = Logger.getLogger(IprodGenerateReportAction.class);

	public List<IprodReportsTemplateMaster> iprodReportTemplateNameDetailsListG = new ArrayList<IprodReportsTemplateMaster>();

	String reportQueryWithFilters;

	protected String[] exportOption;

	String reportTemplateName;

	protected String[] chooseRecipientEmails;

	/**
	 * This attribute maps to the column REPORT_ID in the iprodreports_instance
	 * table.
	 */
	protected int reportId;

	protected String dateTimeFilter;

	private String toDate = "--|--";
	private String fromDate = "--|--";
	private String toTime = "--|--";
	private String fromTime = "--|--";
	private String dateTimeNow= null;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("Scheduling Report" + new Date());
		JobDataMap dataMap = context.getMergedJobDataMap();
		int rTemplateId = dataMap.getInt("RtemplateId");
		reportQueryWithFilters = dataMap.getString("ReportQueryWithFilters");
		exportOption = (String[]) dataMap.get("ExportOption");
		chooseRecipientEmails = (String[]) dataMap.get("RecipientEmails");
		reportId = dataMap.getInt("ReportId");
		dateTimeFilter = dataMap.getString("DateTimeFilter");
		log.info("Report Generate" + dateTimeFilter);

		// generate report
		getReportTemplateDetails(rTemplateId);
		EngineConfig config1 = null;
		EngineConfig config2 = null;
		IReportRunnable design1 = null;
		IReportEngine engine1 = null;
		IReportRunnable design2 = null;
		IReportEngine engine2 = null;
		config1 = new EngineConfig();
		config2 = new EngineConfig();
		
		reportTemplateName = iprodReportTemplateNameDetailsListG.get(0).getRtemplateName();
		try {
			Platform.startup(config1);
		} catch (BirtException e) {
			e.printStackTrace();
		}
		
		try {
			Platform.startup(config2);
		} catch (BirtException e) {
			e.printStackTrace();
		}
		
		final IReportEngineFactory FACTORY = (IReportEngineFactory) Platform.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
		engine1 = FACTORY.createReportEngine(config1);
		
		final IReportEngineFactory FACTORY1 = (IReportEngineFactory) Platform.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
		engine2 = FACTORY1.createReportEngine(config2);
		// Open the report design
		// GET FILE FROM TEMP FOLDER

		IprodReportsTemplateMasterDao iprodReportsTemplateMasterDao = DaoFactory.createIprodReportsTemplateMasterDao();
		List<IprodReportsTemplateMaster> iprodReportsTemplateMasterListL = new ArrayList<IprodReportsTemplateMaster>();

		String ExcelstorageData = null;
		String PDFstorageData = null;
		File excelFile = null;
		File pdfFile = null;
		String extension = ".rptdesign";
		String excelTemplatePath = null;
		String pdfTemplatePath = null;
		String openExcelReportTemplateName = null;
		String openPdfReportTemplateName = null;

		try {
			iprodReportsTemplateMasterListL = iprodReportsTemplateMasterDao.findWhereRtemplateNameEquals(this.reportTemplateName);
			if (iprodReportsTemplateMasterListL.size() > 0) {
				//log.info("iprodReportsTemplateMasterListL ::" + iprodReportsTemplateMasterListL);
				ExcelstorageData = iprodReportsTemplateMasterListL.get(0).getRtemplateExcelDataStorage();
				PDFstorageData= iprodReportsTemplateMasterListL.get(0).getRtemplatePdfDataStorage();
			}
		} catch (IprodReportsTemplateMasterDaoException e) {
			log.info("Exception Occured while getting storageadta::" + e.getMessage());
		}

		//log.info("File data from database ::" + ExcelstorageData);

		File dir = new File("D:\\REPORTS");
		if (!dir.exists()) {
			if (dir.mkdir()) {
				log.info("Directory is created!");
			} else {
				log.info("Failed to create directory!");
			}
		}

		for (int i = 0; i < exportOption.length; i++) {
			if (exportOption[i].equals("PDF")) {	
				// FOR PDF GENERATE RPT FILE IN IPROD ROOT FOLDER  WITH REPORT TEMPLATE NAME , WRITE THE DAT IN FILE AND PROCEED TO GENERATE REPORT 
				
				pdfFile = new File("D:\\REPORTS" + this.reportTemplateName+ "PDF" + extension);
				try {
					if (pdfFile.createNewFile()) {

						//log.info("File is created!");
					} else {
						log.info("File already exists.");
					}
				} catch (IOException e1) {
					
					log.info("Exception while PDF create IOException ::" + e1.getMessage());
				}	
				
				fileWriter(PDFstorageData,pdfFile);
				
				pdfTemplatePath = "D:\\REPORTS" + this.reportTemplateName + "PDF"+ ".rptdesign";
				
				openPdfReportTemplateName= pdfTemplatePath;
				
				try {
					design2 = engine2.openReportDesign(openPdfReportTemplateName);
				} catch (EngineException e1) {
					
					log.error("Exception while assigning rpt file to design2 ::" + e1.getMessage());
				}
				
				design2.setDesignHandle(setFilterQueryOnIReportRunnablePDF(design2, reportQueryWithFilters));
				
				IprodGeneratePDFAction iprodGeneratePDFActionObj = new IprodGeneratePDFAction();
				iprodGeneratePDFActionObj.generatePDF(engine2, design2, reportId, reportTemplateName);
				
				pdfFile.delete();

			} else if (exportOption[i].equals("EXCEL")) {
				
				// FOR EXCEL GENERATE RPT FILE IN IPROD ROOT FOLDER  WITH REPORT TEMPLATE NAME , WRITE THE DAT IN FILE AND PROCEED TO GENERATE REPORT 				
				excelFile = new File("D:\\REPORTS" + this.reportTemplateName+ "Excel" + extension);
				try {
					if (excelFile.createNewFile()) {

						//log.info("File is created!");
					} else {
						log.info("File already exists.");
					}
				} catch (IOException e1) {
					
					log.info("Exception while Excel create IOException ::" + e1.getMessage());
				}				
				fileWriter(ExcelstorageData,excelFile);
				
				excelTemplatePath = "D:\\REPORTS" + this.reportTemplateName + "Excel"+ ".rptdesign";
				openExcelReportTemplateName = excelTemplatePath;
				try {
					design1 = engine1.openReportDesign(openExcelReportTemplateName);
				}  catch (EngineException e1) {					
					log.error("Exception while assigning rpt file to design1 ::" + e1.getMessage());
				}
				design1.setDesignHandle(setFilterQueryOnIReportRunnableExcel(design1, reportQueryWithFilters));
				
				IprodGenerateExcelAction iprodGenerateExcelActionObj = new IprodGenerateExcelAction();
				iprodGenerateExcelActionObj.generateEXCEL(engine1, design1, reportId, reportTemplateName);
				
				excelFile.delete();
				
			} else if (exportOption[i].equals("PDF") && exportOption[i].equals("EXCEL")) {
				log.info("PDF AND EXCEL");
				
				pdfFile = new File("D:\\REPORTS" + this.reportTemplateName+ "PDF" + extension);
				try {
					if (pdfFile.createNewFile()) {

						//log.info("File is created!");
					} else {
						log.info("File already exists.");
					}
				} catch (IOException e1) {
					
					log.info("Exception while PDF create IOException ::" + e1.getMessage());
				}	
				
				fileWriter(PDFstorageData,pdfFile);
				
				pdfTemplatePath = "D:\\REPORTS" + this.reportTemplateName + "PDF"+ ".rptdesign";
				
				openPdfReportTemplateName= pdfTemplatePath;
				
				try {
					design2 = engine2.openReportDesign(openPdfReportTemplateName);
				} catch (EngineException e1) {
					
					log.error("Exception while assigning rpt file to design2 ::" + e1.getMessage());
				}
				
				design2.setDesignHandle(setFilterQueryOnIReportRunnablePDF(design2, reportQueryWithFilters));
				
				IprodGeneratePDFAction iprodGeneratePDFActionObj = new IprodGeneratePDFAction();
				iprodGeneratePDFActionObj.generatePDF(engine2, design2, reportId, reportTemplateName);
				
				pdfFile.delete();
				
				// FOR EXCEL GENERATE RPT FILE IN IPROD ROOT FOLDER  WITH REPORT TEMPLATE NAME , WRITE THE DAT IN FILE AND PROCEED TO GENERATE REPORT 				
				excelFile = new File("D:\\REPORTS" + this.reportTemplateName+ "Excel" + extension);
				try {
					if (excelFile.createNewFile()) {

						//log.info("File is created!");
					} else {
						log.info("File already exists.");
					}
				} catch (IOException e1) {
					
					log.info("Exception while Excel create IOException ::" + e1.getMessage());
				}				
				fileWriter(ExcelstorageData,excelFile);
				
				excelTemplatePath = "D:\\REPORTS" + this.reportTemplateName + "Excel"+ ".rptdesign";
				openExcelReportTemplateName = excelTemplatePath;
				try {
					design1 = engine1.openReportDesign(openExcelReportTemplateName);
				}  catch (EngineException e1) {					
					log.error("Exception while assigning rpt file to design1 ::" + e1.getMessage());
				}
				design1.setDesignHandle(setFilterQueryOnIReportRunnableExcel(design1, reportQueryWithFilters));
				
				IprodGenerateExcelAction iprodGenerateExcelActionObj = new IprodGenerateExcelAction();
				iprodGenerateExcelActionObj.generateEXCEL(engine1, design1, reportId, reportTemplateName);
				
				excelFile.delete();
			}
		}

		if (!chooseRecipientEmails.equals(null)) {
			log.info("SEND MAIL TO RECEPIENT");
			try {
				sendEmail(exportOption);
			} catch (FileNotFoundException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				
				e.printStackTrace();
			} catch (MessagingException e) {
				
				e.printStackTrace();
			}
		}

	}

	public void fileWriter(String writedata, File filename)
	{
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {

			String content = writedata;

			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);
			bw.write(content);

			//log.info("Done");

		} catch (IOException e) {
			log.info("Exception While file write operation ::  " + e.getMessage());

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				log.info("Exception While final block operation ::  " + ex.getMessage());
			}
		}
		
	}
	
	public String getReportTemplateDetails(int rTemplateId) {
		try {
			log.info("Generate Report :getReportTemplateDetails" + rTemplateId);
			IprodReportsTemplateMasterDao iprodReportsTemplateMasterDao = DaoFactory
					.createIprodReportsTemplateMasterDao();
			try {
				List<IprodReportsTemplateMaster> iprodReportTemplateNameDetailsListL = new ArrayList<IprodReportsTemplateMaster>();
				iprodReportTemplateNameDetailsListL = iprodReportsTemplateMasterDao
						.findWhereRtemplateIdEquals(rTemplateId);
				if (iprodReportTemplateNameDetailsListL.size() > 0) {
					iprodReportTemplateNameDetailsListG = iprodReportTemplateNameDetailsListL;
				}
			} catch (IprodReportsTemplateMasterDaoException e) {
				this.addActionError("Sorry!!!.Firstly Add Shifts ");
			}
		} catch (Exception e) {
			log.error("reportTemplateName :: Exception :: " + e.getMessage() + " occured while getting Template Name");
		}
		return SUCCESS;
	}

	private ReportDesignHandle setFilterQueryOnIReportRunnableExcel(IReportRunnable design1, String updatedSQLQuery) {
		log.info("IprodGenerateReportAction:: setFilterQueryOnIReportRunnableExcel");
		try {
			log.info("DateTimeFilter :  " + dateTimeFilter);
			String[] dateAndTime = dateTimeFilter.split("-");
			if (dateAndTime != null) {
				// log.info("Length :: " + dateAndTime.length);
				int i = 0;
				while (i < dateAndTime.length) {
					// log.info("Index :: " + i);
					if (dateAndTime[i].contains("/")) {
						// log.info("Index :: " + i);
						log.info("From Date : " + dateAndTime[i]);
						fromDate = dateAndTime[i];
						if (dateAndTime[i + 1].contains("/")) {
							i++;
							log.info("To Date : " + dateAndTime[i]);
							toDate = dateAndTime[i];
						}
					}
					if (dateAndTime[i].contains(":")) {
						// log.info("Index :: " + i);
						log.info("From Time : " + dateAndTime[i]);
						fromTime = dateAndTime[i];
						if (dateAndTime[i + 1].contains(":")) {
							i++;
							log.info("To Time : " + dateAndTime[i]);
							toTime = dateAndTime[i];
						}
					}
					i++;
				}
			}
		} catch (Exception e1) {
			log.error("Exception in Date Time For Header " + e1.getStackTrace());
		}
		ReportDesignHandle reportDesignHandle = (ReportDesignHandle) design1.getDesignHandle();
		// ElementFactory elementFactory =
		// reportDesignHandle.getElementFactory();
		SlotHandle datasets = reportDesignHandle.getDataSets();
		SlotHandle datasources = reportDesignHandle.getDataSources();
		SlotIterator slotIterator = (SlotIterator) datasets.iterator();
		while (slotIterator.hasNext()) {
			DesignElementHandle designElementHandle = (DesignElementHandle) slotIterator.next();
			if (designElementHandle instanceof OdaDataSetHandle) {
				OdaDataSetHandle odaDataSetHandle = (OdaDataSetHandle) designElementHandle;
				try {
					//log.info("Original Query In Design : " + odaDataSetHandle.getQueryText());
					log.info("Removing extra spaces, characters from input sql query");
					updatedSQLQuery = updatedSQLQuery.replaceAll("\\s+", " ").replaceAll("\n", "").replaceAll("\r", "");
					odaDataSetHandle.setQueryText(updatedSQLQuery);
					//log.info("Updated Query In Design : " + odaDataSetHandle.getQueryText());
				} catch (SemanticException e) {
					log.error("Exception in setFilterQueryOnIReportRunnable :: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		// Put currentdateTime on report
		// Put Date And Time on report
		try {
			ElementFactory elementFactory = reportDesignHandle.getElementFactory();
			GridHandle grid = (GridHandle) reportDesignHandle.findElement("IprodReportHeaderMasterGrid");			
			
			// for report date time
			RowHandle currdatetimeRow = (RowHandle) grid.getRows().get(0);
			
			Date dtNow = new Date();
			SimpleDateFormat currDateT = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
			dateTimeNow = currDateT.format(dtNow);
			
			CellHandle blankDateCellOne = (CellHandle)currdatetimeRow.getCells().get(1);
			LabelHandle currDateTLabel = elementFactory.newLabel(null);
		    //blankDateCellOne.setColumnSpan(2);
		    currDateTLabel.setText(dateTimeNow);
		    //blankDateCellOne.setStyleName("ReportTemplateDTHeaderStyle");
		    blankDateCellOne.getContent().add(currDateTLabel);			
			
			
			// select From date and Time row
			RowHandle fromDateAndTimeRow = (RowHandle) grid.getRows().get(5);

			if (fromDate == "--|--" && fromTime == "--|--") {

				Date dNow = new Date();
				SimpleDateFormat fromDateT = new SimpleDateFormat("dd-MMM-yyyy");
				fromDate = fromDateT.format(dNow);

				SimpleDateFormat fromTimeT = new SimpleDateFormat("hh:mm:ss");
				fromTime = fromTimeT.format(dNow);

				CellHandle fromDateCell = (CellHandle) fromDateAndTimeRow.getCells().get(1);
				LabelHandle fromDateLabel = elementFactory.newLabel(null);
				fromDateLabel.setText(fromDate);
				fromDateCell.getContent().add(fromDateLabel);
				
				// select from Time cell
				CellHandle fromTimeCell = (CellHandle) fromDateAndTimeRow.getCells().get(2);
				LabelHandle fromTimeLabel = elementFactory.newLabel(null);
				fromTimeLabel.setText(fromTime);
				fromTimeCell.getContent().add(fromTimeLabel);
			} else {
				// select from Date cell
				CellHandle fromDateCell = (CellHandle) fromDateAndTimeRow.getCells().get(1);
				LabelHandle fromDateLabel = elementFactory.newLabel(null);
				fromDateLabel.setText(fromDate);
				fromDateCell.getContent().add(fromDateLabel);
				// select from Time cell
				CellHandle fromTimeCell = (CellHandle) fromDateAndTimeRow.getCells().get(2);
				LabelHandle fromTimeLabel = elementFactory.newLabel(null);
				fromTimeLabel.setText(fromTime);
				fromTimeCell.getContent().add(fromTimeLabel);
			}

			RowHandle toDateAndTimeRow = (RowHandle) grid.getRows().get(6);
			// select toDate Cell

			if (toDate == "--|--" && toTime == "--|--") {
				log.info("empty");
				toDateAndTimeRow.drop();
			} else {
				log.info("toDate:" +toDate);
				log.info("toTime:" +toTime);

				CellHandle toDateCell = (CellHandle) toDateAndTimeRow.getCells().get(1);
				LabelHandle ToDateLabel = elementFactory.newLabel(null);
				ToDateLabel.setText(toDate);
				toDateCell.getContent().add(ToDateLabel);

				CellHandle toTimeCell = (CellHandle) toDateAndTimeRow.getCells().get(2);
				LabelHandle toTimeLabel = elementFactory.newLabel(null);
				toTimeLabel.setText(toTime);
				toTimeCell.getContent().add(toTimeLabel);
			}
		} catch (ContentException e) {
			e.printStackTrace();
		} catch (NameException e) {
			e.printStackTrace();
		} catch (SemanticException e) {
			e.printStackTrace();
		}

		return reportDesignHandle;
	}
	
	
	// FOR PDF
	
	private ReportDesignHandle setFilterQueryOnIReportRunnablePDF(IReportRunnable design2, String updatedSQLQuery) {
		log.info("IprodGenerateReportAction:: setFilterQueryOnIReportRunnablePDF");
		try {
			log.info("DateTimeFilter :  " + dateTimeFilter);
			String[] dateAndTime = dateTimeFilter.split("-");
			if (dateAndTime != null) {
				// log.info("Length :: " + dateAndTime.length);
				int i = 0;
				while (i < dateAndTime.length) {
					// log.info("Index :: " + i);
					if (dateAndTime[i].contains("/")) {
						// log.info("Index :: " + i);
						log.info("From Date : " + dateAndTime[i]);
						fromDate = dateAndTime[i];
						if (dateAndTime[i + 1].contains("/")) {
							i++;
							log.info("To Date : " + dateAndTime[i]);
							toDate = dateAndTime[i];
						}
					}
					if (dateAndTime[i].contains(":")) {
						// log.info("Index :: " + i);
						log.info("From Time : " + dateAndTime[i]);
						fromTime = dateAndTime[i];
						if (dateAndTime[i + 1].contains(":")) {
							i++;
							log.info("To Time : " + dateAndTime[i]);
							toTime = dateAndTime[i];
						}
					}
					i++;
				}
			}
		} catch (Exception e1) {
			log.error("Exception in Date Time For Header " + e1.getStackTrace());
		}
		ReportDesignHandle reportDesignHandle = (ReportDesignHandle) design2.getDesignHandle();
		// ElementFactory elementFactory =
		// reportDesignHandle.getElementFactory();
		SlotHandle datasets = reportDesignHandle.getDataSets();
		SlotHandle datasources = reportDesignHandle.getDataSources();
		SlotIterator slotIterator = (SlotIterator) datasets.iterator();
		while (slotIterator.hasNext()) {
			DesignElementHandle designElementHandle = (DesignElementHandle) slotIterator.next();
			if (designElementHandle instanceof OdaDataSetHandle) {
				OdaDataSetHandle odaDataSetHandle = (OdaDataSetHandle) designElementHandle;
				try {
					//log.info("Original Query In Design : " + odaDataSetHandle.getQueryText());
					log.info("Removing extra spaces, characters from input sql query");
					updatedSQLQuery = updatedSQLQuery.replaceAll("\\s+", " ").replaceAll("\n", "").replaceAll("\r", "");
					odaDataSetHandle.setQueryText(updatedSQLQuery);
					//log.info("Updated Query In Design : " + odaDataSetHandle.getQueryText());
				} catch (SemanticException e) {
					log.error("Exception in setFilterQueryOnIReportRunnable :: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		// Put Date And Time on report
		try {
			ElementFactory elementFactory = reportDesignHandle.getElementFactory();
			GridHandle grid = (GridHandle) reportDesignHandle.findElement("IprodReportHeaderMasterGrid");
			
			// for report date time
			RowHandle currdatetimeRow = (RowHandle) grid.getRows().get(0);
			
			Date dtNow = new Date();
			SimpleDateFormat currDateT = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
			dateTimeNow = currDateT.format(dtNow);
			
			CellHandle blankDateCellOne = (CellHandle)currdatetimeRow.getCells().get(1);
			LabelHandle currDateTLabel = elementFactory.newLabel(null);
		    //blankDateCellOne.setColumnSpan(2);
		    currDateTLabel.setText(dateTimeNow);
		    //blankDateCellOne.setStyleName("ReportTemplateDTHeaderStyle");
		    blankDateCellOne.getContent().add(currDateTLabel);	
		    
			// select From date and Time row
			RowHandle fromDateAndTimeRow = (RowHandle) grid.getRows().get(5);

			if (fromDate == "--|--" && fromTime == "--|--") {

				Date dNow = new Date();
				SimpleDateFormat fromDateT = new SimpleDateFormat("dd-MMM-yyyy");
				fromDate = fromDateT.format(dNow);

				SimpleDateFormat fromTimeT = new SimpleDateFormat("hh:mm:ss");
				fromTime = fromTimeT.format(dNow);

				CellHandle fromDateCell = (CellHandle) fromDateAndTimeRow.getCells().get(1);
				LabelHandle fromDateLabel = elementFactory.newLabel(null);
				fromDateLabel.setText(fromDate);
				fromDateCell.getContent().add(fromDateLabel);
				// select from Time cell
				CellHandle fromTimeCell = (CellHandle) fromDateAndTimeRow.getCells().get(2);
				LabelHandle fromTimeLabel = elementFactory.newLabel(null);
				fromTimeLabel.setText(fromTime);
				fromTimeCell.getContent().add(fromTimeLabel);
			} else {
				// select from Date cell
				CellHandle fromDateCell = (CellHandle) fromDateAndTimeRow.getCells().get(1);
				LabelHandle fromDateLabel = elementFactory.newLabel(null);
				fromDateLabel.setText(fromDate);
				fromDateCell.getContent().add(fromDateLabel);
				// select from Time cell
				CellHandle fromTimeCell = (CellHandle) fromDateAndTimeRow.getCells().get(2);
				LabelHandle fromTimeLabel = elementFactory.newLabel(null);
				fromTimeLabel.setText(fromTime);
				fromTimeCell.getContent().add(fromTimeLabel);
			}

			RowHandle toDateAndTimeRow = (RowHandle) grid.getRows().get(6);
			// select toDate Cell

			if (toDate == "--|--" && toTime == "--|--") {
				log.info("empty");
				toDateAndTimeRow.drop();
			} else {
				log.info("toDate:" +toDate);
				log.info("toTime:" +toTime);

				CellHandle toDateCell = (CellHandle) toDateAndTimeRow.getCells().get(1);
				LabelHandle ToDateLabel = elementFactory.newLabel(null);
				ToDateLabel.setText(toDate);
				toDateCell.getContent().add(ToDateLabel);

				CellHandle toTimeCell = (CellHandle) toDateAndTimeRow.getCells().get(2);
				LabelHandle toTimeLabel = elementFactory.newLabel(null);
				toTimeLabel.setText(toTime);
				toTimeCell.getContent().add(toTimeLabel);
			}
		} catch (ContentException e) {
			e.printStackTrace();
		} catch (NameException e) {
			e.printStackTrace();
		} catch (SemanticException e) {
			e.printStackTrace();
		}

		return reportDesignHandle;
	}

	public void sendEmail(String[] exportOption) throws FileNotFoundException, IOException, MessagingException {
		log.info("IprodGenerateReportAction :: sendEmail" + chooseRecipientEmails);
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("dd-MMM-yyyy-HH-mm-ss");
		String currentTime = ft.format(dNow);
		String emails[] = chooseRecipientEmails;
		int noOfEmailIds = emails.length;
		// Load the mail.properties file
		Properties props = new Properties();
		props.put("mail.smtp.host", "mail.id4-realms.com");
		props.put("mail.transport.protocol", "TLS");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.user", "iprod.reports@id4-realms.com");
		props.put("mail.smtp.auth", "true");

		final String emailUserName = "iprod.reports@id4-realms.com";
		final String emailPassword = "R#p0rts@123";
		String transportProtocol = "TLS";
		String emailSentAs = "iprod.reports@id4-realms.com";

		String pdfName = null, excelName = null;

		String emailSubject = "PLUSCON REPORT EMAIL";
		// log.info("EMAILSUBJECT :"+emailSubject);

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailUserName, emailPassword);
			}
		};
		Session session = Session.getInstance(props, auth);

		MimeMessage message = new MimeMessage(session); // Define message
		message.setFrom(new InternetAddress(emailSentAs));
		message.setSentDate(new Date());
		InternetAddress[] toAddresses = new InternetAddress[noOfEmailIds];
		for (int i = 0; i < noOfEmailIds; i++) {
			toAddresses[i] = new InternetAddress(emails[i]);
		}
		message.setRecipients(Message.RecipientType.TO, toAddresses);
		message.setSubject(emailSubject); // create the subject
		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		// Now set the actual message
		messageBodyPart.setText("PFA");

		IprodReportsInstanceDao instanceDao = DaoFactory.createIprodReportsInstanceDao();
		List<IprodReportsInstance> allInstanceListL = new ArrayList<IprodReportsInstance>();
		try {
			allInstanceListL = instanceDao.findWhereReportIdEquals(reportId);
		} catch (IprodReportsInstanceDaoException e1) {
			
			e1.printStackTrace();
		}
		for (int i = 0; i < exportOption.length; i++) {
			if (exportOption[i].equals("EXCEL")) {

				// Create a multipar message
				Multipart multipart = new MimeMultipart();
				// Set text message part
				multipart.addBodyPart(messageBodyPart);
				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				String excelFilename = allInstanceListL.get(0).getReportExcelLocation();
				log.info("excelFilename::" + excelFilename);
				DataSource excelSource = new FileDataSource(excelFilename);
				messageBodyPart.setDataHandler(new DataHandler(excelSource));
				messageBodyPart.setFileName(excelFilename);
				multipart.addBodyPart(messageBodyPart);
				// Send the complete message parts
				message.setContent(multipart);
			} else if (exportOption[i].equals("PDF")) {

				// Create a multipar message
				Multipart multipart = new MimeMultipart();
				// Set text message part
				multipart.addBodyPart(messageBodyPart);
				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				String pdfFilename = allInstanceListL.get(0).getReportPdfLocation();
				DataSource pdfSource = new FileDataSource(pdfFilename);
				messageBodyPart.setDataHandler(new DataHandler(pdfSource));
				messageBodyPart.setFileName(pdfFilename);
				multipart.addBodyPart(messageBodyPart);
				// Send the complete message parts
				message.setContent(multipart);
			}

		}
		log.info("REPORT ATTACHED TO EMAIL!!");
		try {
			Transport.send(message);
		} catch (Exception e) {
			log.info("Exception occurs while sending message::" + e.getMessage());
			e.printStackTrace();
		}
		log.info("EMAIL MESSAGE SENT SUCCESSFULLY!!");
		log.info(" ==============================  REPORT GENERATION SUCCESSFULLY!! =============================");
		
	}
}
