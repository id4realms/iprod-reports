package com.id4.iprod.reports;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.batik.svggen.ImageCacher.Embedded;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.emitter.docx.writer.Footer;
import org.eclipse.birt.report.model.api.AutoTextHandle;
import org.eclipse.birt.report.model.api.CellHandle;
import org.eclipse.birt.report.model.api.DataItemHandle;
import org.eclipse.birt.report.model.api.DesignConfig;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.Expression;
import org.eclipse.birt.report.model.api.GridHandle;
import org.eclipse.birt.report.model.api.IDesignEngine;
import org.eclipse.birt.report.model.api.IDesignEngineFactory;
import org.eclipse.birt.report.model.api.ImageHandle;
import org.eclipse.birt.report.model.api.LabelHandle;
import org.eclipse.birt.report.model.api.OdaDataSetHandle;
import org.eclipse.birt.report.model.api.OdaDataSourceHandle;
import org.eclipse.birt.report.model.api.PropertyHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.RowOperationParameters;
import org.eclipse.birt.report.model.api.SessionHandle;
import org.eclipse.birt.report.model.api.SimpleMasterPageHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.StyleHandle;
import org.eclipse.birt.report.model.api.TableHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.command.ContentException;
import org.eclipse.birt.report.model.api.command.NameException;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.eclipse.birt.report.model.api.elements.structures.ComputedColumn;
import org.eclipse.birt.report.model.api.elements.structures.EmbeddedImage;
import org.eclipse.birt.report.model.elements.MasterPage;
import org.eclipse.birt.report.model.elements.SimpleMasterPage;
import org.eclipse.birt.report.data.oda.jdbc.ResultSet;
import org.eclipse.birt.report.model.api.elements.structures.FormatValue;
import org.eclipse.birt.report.model.api.metadata.PropertyValueException;
import org.eclipse.birt.report.model.api.util.DocumentUtil;

import com.ibm.icu.util.ULocale;
import com.id4.iprod.IprodViewModuleOperationMasterAction;
import com.id4.iprod.dto.IprodColumnGroup;
import com.id4.iprod.dto.IprodTemplateColumnMasterTable;
import com.id4.iprod.exceptions.IprodReportsViewReportDatasourceDetailsDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.id4.iprod.reports.dao.IprodImageMasterDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportDatasourceDetailsDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportTemplateDetailsDao;
import com.id4.iprod.reports.dto.IprodImageMaster;
import com.id4.iprod.reports.dto.IprodImageMasterPk;
import com.id4.iprod.reports.dto.IprodReportsViewReportDatasourceDetails;
import com.id4.iprod.reports.dto.IprodReportsViewReportTemplateDetails;
import com.id4.iprod.reports.exceptions.IprodImageMasterDaoException;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportTemplateDetailsDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.util.TablesNamesFinder;



public class IprodReportsTemplatesAction extends ActionSupport implements IprodModuleAuthorization{	
	
	static Logger log = Logger.getLogger(IprodReportsTemplatesAction.class);

		
	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprodreports_templates table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column RTEMPLATE_NAME in the iprodreports_templates table.
	 */
	protected String rtemplateName;

	/** 
	 * This attribute maps to the column RTEMPLATE_TITLE in the iprodreports_templates table.
	 */
	protected String rtemplateTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SUB_TITLE in the iprodreports_templates table.
	 */
	protected String rtemplateSubTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprodreports_templates table.
	 */
	protected String rtemplateSqlquery;
	
	/** 
	 * This attribute maps to the column REPORT_QUERY in the iprodreports_templates table.
	 */
	protected String reportQuery;
	
	/** 
	 * This attribute maps to the column DATASOURCE_INSTANCE_ID in the iprodreports_datasource_instance table.
	 */
	protected int datasourceId;
	
	protected int imageId;
	
	 public List<IprodImageMaster> IprodImageMasterListG = new ArrayList<IprodImageMaster>();
	 
	 public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();
	
	/**
	 * Header Style parameters
	*/
	protected String headerStyleTextColor;
	protected String headerStyleBgColor;
	protected String headerStyleHAlign;
	protected String headerStyleVAlign;
	protected String headerStyleBorderType;
	protected String headerStyleTransparent;
    
	/**
	 * Detail Style parameters
	*/
	
	protected String detailStyleTextColor;
	protected String detailStyleBgColor;
	protected String detailStyleHAlign;
	protected String detailStyleVAlign;
	protected String detailStyleBorderType;
	protected String detailStyleTransparent;
    
	/**
	 * Title Style parameters
	*/
	protected String titleStyleTextColor;
	protected String titleStyleBgColor;
	protected String titleStyleHAlign;
	protected String titleStyleVAlign;
	protected String titleStyleBorderType;
	protected String titleStyleTransparent;
    
	/**
	 * Sub Title Style parameters
	*/
	protected String subTitleStyleTextColor;
	protected String subTitleStyleBgColor;
	protected String subTitleStyleHAlign;
	protected String subTitleStyleVAlign;
	protected String subTitleStyleBorderType;
	protected String subTitleStyleTransparent;
    
	/**
	 * General Style parameters
	 */	
	protected String pageOrientation;
	protected String pageSize;	
	protected String reportLeftLogo;	
	protected String reportRightLogo;	
	protected String reportFooterLeftLogo;	
	protected String reportFooterTitle;	
	protected String reportFooterRightLogo;	
	protected String viewDropDown;	
	protected int templateId;
	
/*	private File rtemplateHeaderLeft;	
	private File rtemplateHeaderRight;	
	private File rtemplateFooterLeft;	
	private File rtemplateFooterRight;	*/
	
	protected String rtemplateHeaderLeft;	
	protected String rtemplateHeaderRight;	
	protected String rtemplateFooterLeft;	
	protected String rtemplateFooterRight;	
	
	
	protected String templateRepo;	
	Boolean isSplitReport = false;	
	protected String pathToReportsRepo = null;
	protected String pathToReportsImg = null;	
	
	public String streamTemplatedata = null;
	//BIRT Specific parameters
	ReportDesignHandle designHandle = null;
	ElementFactory designFactory = null;
	StructureFactory structFactory = null;
	TableHandle jdbcQueryResultDataTable = null;
	OdaDataSetHandle dataSetHandleToUse = null;
	
	protected String imageLogo = null;

	
	protected ArrayList<IprodTemplateColumnMasterTable> templateColumnMasterTable = new ArrayList<IprodTemplateColumnMasterTable>();
	
	protected static String jdbcDriverClassname,jdbcDriverUrl,databaseName,databaseServerUsername,databaseServerPassword;
	

	public String execute(){
		return SUCCESS;
	}
	
      public boolean setJDBCConnectionParmsFromDatasource(int datasourceId){
   	   log.info("IprodReportsTemplatesAction:setJDBCConnectionParmsFromDatasource");
   		List<IprodReportsViewReportDatasourceDetails> iprodReportsViewReportDatasourceDetailsListL =new ArrayList<IprodReportsViewReportDatasourceDetails>();
   		IprodReportsViewReportDatasourceDetailsDao iprodReportsViewReportDatasourceDetailsDao = DaoFactory.createIprodReportsViewReportDatasourceDetailsDao();
	     try {
	    	 iprodReportsViewReportDatasourceDetailsListL = iprodReportsViewReportDatasourceDetailsDao.findWhereDatasourceIdEquals(datasourceId);
		 } catch (IprodReportsViewReportDatasourceDetailsDaoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		 }
	     if(iprodReportsViewReportDatasourceDetailsListL.size() > 0){
		   	jdbcDriverClassname=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsJdbcDriverClass();
		   	jdbcDriverUrl=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsJdbcDriverUrl();
		   	databaseName=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsDatabaseName();
		   	databaseServerUsername=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsServerUsername();
		   	databaseServerPassword=iprodReportsViewReportDatasourceDetailsListL.get(0).getRdbmsServerPassword();
		   	return true;
	    }
	     else
	    	 return false;
       //log.info("The Details are:"+"\t"+jdbcDriverClassname+"\t"+jdbcDriverUrl+"\t"+databaseInstanceName+"\t"+databaseServerName+"\t"+databaseServerPassword);
   	}
	
	/**
	 * getMsSqlConnection() to create a sql connection
	 * @throws SemanticException
	 */
	public static Connection getMsSqlConnection(){
		log.info("IprodReportsTemplatesAction:getMsSqlConnection");
        try {
			Connection conn = DriverManager.getConnection(jdbcDriverUrl, databaseServerUsername, databaseServerPassword);
			return conn;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	 /**
	 * buildDataSource() to create a jdbc type of data source
	 * @throws SemanticException
	 */
	public boolean buildJDBCDataSource(){
	   log.info("IprodReportsTemplatesAction:buildJDBCDataSource");
	   try {
		OdaDataSourceHandle dsHandle = designFactory.newOdaDataSource("IprodReportTemplateDataSource", "org.eclipse.birt.report.data.oda.jdbc");
		   dsHandle.setProperty("odaDriverClass",jdbcDriverClassname);
		   dsHandle.setProperty("odaURL",jdbcDriverUrl);
		   dsHandle.setProperty("odaUser",databaseServerUsername);
		   dsHandle.setProperty("odaPassword",databaseServerPassword);
		   designHandle.getDataSources().add(dsHandle);
		   return true;
	} catch (Exception e) {
		return false;
	}
	}
	
	/**
	 * buildJDBCDataSet() to create a jdbc type of data source
	 * @throws SemanticException
	 */
	public boolean buildJDBCDataSet(String inputBaseSqlQuery) {
	   log.info("IprodReportsTemplatesAction:buildJDBCDataSet()" +inputBaseSqlQuery);
	   try{
	       OdaDataSetHandle dataSetHandleToUse = designFactory.newOdaDataSet( "IprodReportTemplateDataSet","org.eclipse.birt.report.data.oda.jdbc.JdbcSelectDataSet");
	       dataSetHandleToUse.setDataSource("IprodReportTemplateDataSource");
	       dataSetHandleToUse.setQueryText(inputBaseSqlQuery);
	       designHandle.getDataSets().add(dataSetHandleToUse);
	       return true;
	   }catch(SemanticException e){
		   return false;
	   }
	   
	}
	 
	/**
	 * buildJDBCReportTemplateDataTable() to add report table with selected singleton and column group span parameters
	 * @throws SemanticException
	 */
	public boolean buildJDBCReportTemplateWithStyleAndDataTable(ArrayList<IprodReportsTableColumn> reportsTableComputedColumnList) {
	log.info("IprodReportsTemplatesAction:buildJDBCReportTemplateWithStyleAndDataTable()" +reportsTableComputedColumnList);
    //RETREIVE IMAGE
	try{
    List<IprodImageMaster> headerLeftLogoListL = new ArrayList<IprodImageMaster>();
    List<IprodImageMaster> headerRightLogoListL = new ArrayList<IprodImageMaster>();
    List<IprodImageMaster> footerLeftLogoListL = new ArrayList<IprodImageMaster>();
    List<IprodImageMaster> footerRightLogoListL = new ArrayList<IprodImageMaster>();
    IprodImageMasterDao imageMasterDao = DaoFactory.createIprodImageMasterDao();
    
		try {
			headerLeftLogoListL = imageMasterDao.findWhereImageNameEquals(this.rtemplateHeaderLeft);
			headerRightLogoListL = imageMasterDao.findWhereImageNameEquals(this.rtemplateHeaderRight);
			footerLeftLogoListL = imageMasterDao.findWhereImageNameEquals(this.rtemplateFooterLeft);
			footerRightLogoListL = imageMasterDao.findWhereImageNameEquals(this.rtemplateFooterRight);

	} catch (IprodImageMasterDaoException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		CellHandle cell = null;
		RowHandle row  = null;
		LabelHandle label = null;	
    	// Create a simple master page that describes how the report will appear when printed.
    	//
    	// Note: The report will fail to load in the BIRT designer unless you create a master page.
    	// So we first add a master page, then add a grid to the body slot of the report template.
		buildReportTemplateStyles();
	    //DesignElementHandle element = designFactory.newSimpleMasterPage("IprodReportMasterPage"); 
		SimpleMasterPageHandle simpleMasterPage = designFactory.newSimpleMasterPage("IprodReportMasterPage");
		
		simpleMasterPage.setOrientation(DesignChoiceConstants.PAGE_ORIENTATION_LANDSCAPE);
		simpleMasterPage.setPageType(DesignChoiceConstants.PAGE_SIZE_US_LETTER);

		simpleMasterPage.setProperty(MasterPage.TOP_MARGIN_PROP, "1.9cm");
		simpleMasterPage.setProperty(MasterPage.BOTTOM_MARGIN_PROP, "1.9cm");
	
		simpleMasterPage.setProperty(MasterPage.LEFT_MARGIN_PROP, "2.0cm");
		simpleMasterPage.setProperty(MasterPage.RIGHT_MARGIN_PROP, "1.0cm");
		//simpleMasterPage.setProperty("headerHeight", "1.7in");
		//simpleMasterPage.setProperty("footerHeight", "0.7in");
		
		
		   //Simple master page Start..............................................................................................
		GridHandle masterPageHeaderGrid = designFactory.newGridItem("IprodReportHeaderMasterGrid", 3 /* cols */, 6 /* row */);  
		GridHandle masterPageFooterGrid = designFactory.newGridItem("IprodReportFooterMasterGrid", 5 /* cols */, 1 /* row */);
		   
		masterPageFooterGrid.setWidth("100%");
		masterPageHeaderGrid.setWidth("100%");
		   
		   //=====Simple master Header Start
		simpleMasterPage.getPageHeader().add(masterPageHeaderGrid);	 
		RowHandle masterPageHeaderRow = (RowHandle) masterPageHeaderGrid.getRows().get(0);

		//Simple master page header [row, col].
		
		//Simple master page header [0,0].
		if(headerLeftLogoListL.size() > 0)
		{
			CellHandle masterPageHeaderCellRight = (CellHandle)masterPageHeaderRow.getCells().get(0);
			masterPageHeaderCellRight.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_LEFT);
			masterPageHeaderCellRight.setStringProperty(StyleHandle.MARGIN_BOTTOM_PROP, "2pt");
			
			ImageHandle imageHeaderLeft = designFactory.newImage( null );
			imageHeaderLeft.setSource("embed");
			imageHeaderLeft.setImageName("Header Left Logo");
			masterPageHeaderCellRight.setColumnSpan(getTotalColumnsInMainTable()-1);
			masterPageHeaderCellRight.getContent().add(imageHeaderLeft);
		}
		else if(headerLeftLogoListL.size() == 0)
		{
			CellHandle masterPageHeaderCellRight = (CellHandle)masterPageHeaderRow.getCells().get(0);
			masterPageHeaderCellRight.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_LEFT);
			masterPageHeaderCellRight.setStringProperty(StyleHandle.MARGIN_BOTTOM_PROP, "2pt");
			
			ImageHandle imageHeaderLeft = designFactory.newImage( null );
			masterPageHeaderCellRight.setColumnSpan(getTotalColumnsInMainTable()-1);
			masterPageHeaderCellRight.getContent().add(imageHeaderLeft);
		}
			
		
		//Simple master page header [0,1].
		if(headerRightLogoListL.size() > 0)
		{
			CellHandle masterPageHeaderCellForImageRight = (CellHandle)masterPageHeaderRow.getCells().get(1);
			masterPageHeaderCellForImageRight.setColumnSpan(3);
			masterPageHeaderCellForImageRight.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_RIGHT);
			masterPageHeaderCellForImageRight.setStringProperty(StyleHandle.MARGIN_BOTTOM_PROP, "2pt");
			
			ImageHandle imageHeaderRight1 = designFactory.newImage( null );
			imageHeaderRight1.setSource("embed");
			imageHeaderRight1.setImageName("Header Right Logo");
			masterPageHeaderCellForImageRight.getContent().add(imageHeaderRight1);
		}
		
		removeExtraCellsOnHeader(masterPageHeaderRow); //remove [0,2]
		//Simple master page header [1,0].
/*		RowHandle masterPageHeaderMainTitle = (RowHandle) masterPageHeaderGrid.getRows().get(1);
		CellHandle mainTitleCell = (CellHandle) masterPageHeaderMainTitle.getCells().get(0);
		mainTitleCell.setColumnSpan(getTotalColumnsInMainTable());
		mainTitleCell.setStyleName("ReportTemplateMainTitleStyle");
		LabelHandle mainTitleLabel = designFactory.newLabel("PageHeaderMainTitle");
		mainTitleLabel.setText(this.rtemplateTitle);
		mainTitleCell.getContent().add(mainTitleLabel);
		removeExtraCellsOnHeader(masterPageHeaderMainTitle);  //remove [1,1] [1,2]
		*/
		
		//Simple master page header [2,0]
		RowHandle masterPageHeaderSubTitle = (RowHandle) masterPageHeaderGrid.getRows().get(2);
	    CellHandle subTitleCell = (CellHandle)masterPageHeaderSubTitle.getCells().get(0);
	    subTitleCell.setColumnSpan(getTotalColumnsInMainTable());
	    subTitleCell.setStyleName("ReportTemplateSubTitleStyle");
	    LabelHandle subTitleLabel = designFactory.newLabel("PageHeaderSubTitle");
	    subTitleLabel.setText(this.rtemplateSubTitle);
		subTitleCell.getContent().add(subTitleLabel);
		removeExtraCellsOnHeader(masterPageHeaderSubTitle); //remove [2,1] [2,2]
		
		
		//Simple master page header [3,0]
		row = (RowHandle) masterPageHeaderGrid.getRows().get(3);
		CellHandle blankHeadingCell = (CellHandle)row.getCells().get(0);
	    blankHeadingCell.setStyleName("ReportTemplateDTHeaderStyle");
	    //Simple master page header [3,1]
		CellHandle dateHeadingCell = (CellHandle)row.getCells().get(1);
		dateHeadingCell.setColumnSpan(2);
		dateHeadingCell.setStyleName("ReportTemplateDTHeaderStyle");
	    LabelHandle dateHadingLable = designFactory.newLabel(null);
	    dateHadingLable.setText("MM/DD/YYYY");
	    dateHeadingCell.getContent().add(dateHadingLable);
	    //Simple master page header [3,2]
	    CellHandle timeHeadingCell = (CellHandle)row.getCells().get(2);
	    timeHeadingCell.setStyleName("ReportTemplateDTHeaderStyle");
	    LabelHandle timeHadingLable = designFactory.newLabel(null);
	    timeHadingLable.setText("HH:MM:SS");
	    timeHeadingCell.getContent().add(timeHadingLable);
	    
	    row = (RowHandle) masterPageHeaderGrid.getRows().get(4);
	    //Simple master page header [4,0]
	    CellHandle fromDateCell = (CellHandle)row.getCells().get(0);
	    fromDateCell.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_LEFT);
	    fromDateCell.setStyleName("ReportTemplateDTHeaderStyle");
	    LabelHandle fromDateLabel = designFactory.newLabel(null);
	    fromDateLabel.setText("From : ");
	    fromDateCell.getContent().add(fromDateLabel);
	    //Simple master page header [4,1]
	    CellHandle blankDateCellOne = (CellHandle)row.getCells().get(1);
	    blankDateCellOne.setColumnSpan(2);
	    blankDateCellOne.setStyleName("ReportTemplateDTHeaderStyle");
	    //Simple master page header [4,2]
	    CellHandle blankDateCellTwo = (CellHandle)row.getCells().get(2);
	    blankDateCellTwo.setStyleName("ReportTemplateDTHeaderStyle");
	    
	    row = (RowHandle) masterPageHeaderGrid.getRows().get(5);
	    //Simple master page header [5,0]
	    CellHandle toDateCell = (CellHandle)row.getCells().get(0);
	    toDateCell.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_LEFT);
	    toDateCell.setStyleName("ReportTemplateDTHeaderStyle");
	    LabelHandle toDateLabel = designFactory.newLabel(null);
	    toDateLabel.setText("To : ");
	    toDateCell.getContent().add(toDateLabel);
	    //Simple master page header [5,1]
	    CellHandle blankTimeCellOne = (CellHandle)row.getCells().get(1);
	    blankTimeCellOne.setStyleName("ReportTemplateDTHeaderStyle");
	    blankTimeCellOne.setColumnSpan(2);
	    //Simple master page header [5,2]
	    CellHandle blankTimeCellTwo = (CellHandle)row.getCells().get(2);
	    blankTimeCellTwo.setStyleName("ReportTemplateDTHeaderStyle");
		//======Simple master Header End.........................................................................
		   
		//Simple master page Footer [row, col]. 
		simpleMasterPage.getPageFooter().add(masterPageFooterGrid);
		RowHandle masterPageFooterRow = (RowHandle) masterPageFooterGrid.getRows().get(0);
		
		int footerLeftImg = 0;
		int footerPageNum = 0;
		int footerRightImg = 0;
		int totalColumn = getTotalColumnsInMainTable();
		
		if(totalColumn == 1){
			footerLeftImg = 1;
			footerPageNum = 1;
			footerRightImg = 1;
		}else if(totalColumn == 2){
			footerLeftImg = 1;
			footerPageNum = 1;
			footerRightImg = 1;
		}else if(totalColumn == 3){
			footerLeftImg = 1;
			footerPageNum = 1;
			footerRightImg = 1;
		}else if(totalColumn == 4){
			footerLeftImg = 1;
			footerPageNum = 2;
			footerRightImg = 1;
		}else if(totalColumn == 5){
			footerLeftImg = 2;
			footerPageNum = 1;
			footerRightImg = 2;
			
		}else if(totalColumn > 5 && totalColumn <= 17){
			if(getTotalColumnsInMainTable() % 2 == 0){
				footerLeftImg = getTotalColumnsInMainTable() / 2;
				footerPageNum = (getTotalColumnsInMainTable() / 2) - 2;
				footerRightImg = 2;
			}else{
				footerLeftImg = getTotalColumnsInMainTable() / 2;
				footerPageNum = ((getTotalColumnsInMainTable() / 2) - 2) - 1;
				footerRightImg = 2;
			}
		}else if(totalColumn > 18){
			if(getTotalColumnsInMainTable() % 2 == 0){
				footerLeftImg = getTotalColumnsInMainTable() / 2;
				footerPageNum = (getTotalColumnsInMainTable() / 2) - 3;
				footerRightImg = 3;
			}else{
				footerLeftImg = getTotalColumnsInMainTable() / 2;
				footerPageNum = ((getTotalColumnsInMainTable() / 2) - 3) - 1;
				footerRightImg = 3;
			}
		}
		if(footerLeftLogoListL.size() > 0){
			CellHandle masterPageFooterCellForImage = (CellHandle)masterPageFooterRow.getCells().get(0);
			masterPageFooterCellForImage.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_LEFT);
			masterPageFooterCellForImage.setColumnSpan(footerLeftImg);
			
			ImageHandle imageFooterLeft = designFactory.newImage( null );
			imageFooterLeft.setSource("embed");
			imageFooterLeft.setImageName("Footer Left Logo");
			masterPageFooterCellForImage.getContent().add(imageFooterLeft);
	  }
		 
	 	// Page number code start 
		CellHandle pageNumberMain = (CellHandle)masterPageFooterRow.getCells().get(1);
		pageNumberMain.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_LEFT);
		pageNumberMain.setColumnSpan(footerPageNum);
		GridHandle masterPageFooterGridForPageNumber = designFactory.newGridItem("IprodReportFooterMasterGrid", 4 /* cols */, 1 /* row */);
		masterPageFooterGridForPageNumber.setWidth("42%");
		 
		 
		RowHandle PageNumberRow = (RowHandle) masterPageFooterGridForPageNumber.getRows().get(0);
		 
		CellHandle page = (CellHandle)PageNumberRow.getCells().get(0);
		page.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_RIGHT);
		LabelHandle lblPage = designFactory.newLabel("Page");
		lblPage.setText("Page ");
		page.getContent().add(lblPage);
		 
		CellHandle currentPageNum = (CellHandle)PageNumberRow.getCells().get(1);
		AutoTextHandle pageNumber = designFactory.newAutoText("PageNumberText");
		pageNumber.setProperty(AutoTextHandle.AUTOTEXT_TYPE_PROP, DesignChoiceConstants.AUTO_TEXT_PAGE_NUMBER);
		currentPageNum.getContent().add(pageNumber);
		 
		CellHandle PageOF = (CellHandle)PageNumberRow.getCells().get(2);
		LabelHandle of =designFactory.newLabel("OF");
		of.setText("of");
		PageOF.getContent().add(of);
		 
		CellHandle TotalPage = (CellHandle)PageNumberRow.getCells().get(3);
		AutoTextHandle totalPageNumber = designFactory.newAutoText("TotalPageNumberText");
		totalPageNumber.setProperty(AutoTextHandle.AUTOTEXT_TYPE_PROP, DesignChoiceConstants.AUTO_TEXT_TOTAL_PAGE);
		TotalPage.getContent().add(totalPageNumber);
		 
		 
		pageNumberMain.getContent().add(masterPageFooterGridForPageNumber);
		 //Page Number end
		
		if(footerRightLogoListL.size() > 0){
			CellHandle masterPageFooterCellForImageRight = (CellHandle)masterPageFooterRow.getCells().get(3);
			masterPageFooterCellForImageRight.setColumnSpan(footerRightImg);
			masterPageFooterCellForImageRight.setStringProperty(StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.BACKGROUND_POSITION_RIGHT);
			
			ImageHandle imageFooterRight = designFactory.newImage( null );
			imageFooterRight.setSource("embed");
			imageFooterRight.setImageName("Footer Right Logo");
			masterPageFooterCellForImageRight.getContent().add(imageFooterRight);
		}
		//Simple master Footer End
		
		designHandle.getMasterPages().add(simpleMasterPage); 
		//Simple master End..............................................................................................................
		
		//Start building report main Table
		
	   Integer totalColumnsInMainTable = 0;
	   for(int i=0;i<templateColumnMasterTable.size();i++){
		   for(int j=0;j<templateColumnMasterTable.get(i).templateColumnSubTable.size();j++){
		   if(!templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnName().equals(""))
			   totalColumnsInMainTable++;
		   }
	   }
	   log.info("TOTAL COLUMNS CONSIDERED FOR MASTER TABLE : " + getTotalColumnsInMainTable());
	   
	   
	   int in = 0;
	 // ArrayList<IprodTemplateColumnMasterTable> tempTemplateColumnMasterTable = templateColumnMasterTable;
	   while(in < templateColumnMasterTable.size()){
		   ArrayList<IprodColumnGroup> tempTemplateColumnSubTable = templateColumnMasterTable.get(in).getTemplateColumnSubTable();
		   int j = 0;
		   while(j < tempTemplateColumnSubTable.size()){
			   String tempColumnAlias = tempTemplateColumnSubTable.get(j).getColumnAlias();
			   tempColumnAlias = tempColumnAlias.replace("_", " ");
			   log.info("Temp  " + tempColumnAlias);
			   j++;
		   }
		   in++;
	   }
	 
	   jdbcQueryResultDataTable = designFactory.newTableItem("JdbcQueryResultDataTable",getTotalColumnsInMainTable());
	   jdbcQueryResultDataTable.setWidth("100%");
	   jdbcQueryResultDataTable.setDataSet(designHandle.findDataSet("IprodReportTemplateDataSet"));
		        
		 //create a new detail row and add to the report
		 
		 int currentColumnIndex = 0; //used to mark current column position
        
         //go through column list and create a new column binding, otherwise data will not be populated into the report
         //Then add a new column to our row
         
        PropertyHandle computedSet = jdbcQueryResultDataTable.getColumnBindings();
	             
            for (Iterator i = reportsTableComputedColumnList.iterator(); i.hasNext();){
            	 IprodReportsTableColumn iprodReportsTableColumn = (IprodReportsTableColumn) i.next();	 
                 ComputedColumn computedColumn = StructureFactory.createComputedColumn();
                 computedColumn.setName(iprodReportsTableColumn.getColumnName());
      
                 
                // computedColumn.setDataType(columnDataTypeName);
                 switch(iprodReportsTableColumn.getColumnDataType().toLowerCase()){
                 case "int"     : 	computedColumn.setDataType("integer"); 
                 					computedColumn.setExpression("dataSetRow[\"" + iprodReportsTableColumn.getColumnName() +"\"]"); 
                 					
                 					jdbcQueryResultDataTable.getColumnBindings().addItem(computedColumn);
                 					ComputedColumn computedColumnIntSUM = StructureFactory.createComputedColumn();
                 					computedColumnIntSUM.setName(iprodReportsTableColumn.getColumnName()+"_SUM");
                 					
                 					//computedColumn1.setAggregateFunction("SUM"); 
                 					computedColumnIntSUM.setExpression("Total.sum(row[\""+iprodReportsTableColumn.getColumnName()+"\"])");
                 					jdbcQueryResultDataTable.getColumnBindings().addItem(computedColumnIntSUM);
                 					break;
                 case "varchar" : 	computedColumn.setDataType("string"); 
                 					computedColumn.setExpression("dataSetRow[\"" + iprodReportsTableColumn.getColumnName() +"\"]"); 
                 					jdbcQueryResultDataTable.getColumnBindings().addItem(computedColumn);
                 					break;
                 case "float"   :  	computedColumn.setDataType("float"); 
                 					computedColumn.setExpression("dataSetRow[\"" + iprodReportsTableColumn.getColumnName() +"\"]");
                 					jdbcQueryResultDataTable.getColumnBindings().addItem(computedColumn);
                 					
                 					ComputedColumn computedColumnFloatSUM = StructureFactory.createComputedColumn();
                 					computedColumnFloatSUM.setName(iprodReportsTableColumn.getColumnName()+"_SUM");
                 					
                 					//computedColumn1.setAggregateFunction("SUM"); 
                 					computedColumnFloatSUM.setExpression("Total.sum(row[\""+iprodReportsTableColumn.getColumnName()+"\"])");
                 					jdbcQueryResultDataTable.getColumnBindings().addItem(computedColumnFloatSUM);
                 					break;
                 case "tinyint" : 	computedColumn.setDataType("boolean"); 
                 					computedColumn.setExpression("dataSetRow[\"" + iprodReportsTableColumn.getColumnName() +"\"]");
                 					jdbcQueryResultDataTable.getColumnBindings().addItem(computedColumn);
                 					break;
                 default		: 	break;
                 }

                 currentColumnIndex++; //advance position
             }
	             
        	currentColumnIndex = 0;
    		boolean columnSpanGroupExists = false;
    		for(int i=0;i<templateColumnMasterTable.size();i++){
    			log.info("templateColumnMasterTable.get(i).columnGroupAlias:" +templateColumnMasterTable.get(i).columnGroupAlias + "  " + (!templateColumnMasterTable.get(i).columnGroupAlias.equals("GRPALIASNONE") && templateColumnMasterTable.get(i).columnGroupAlias != null));
    			
    			if(!templateColumnMasterTable.get(i).columnGroupAlias.equals("GRPALIASNONE") && templateColumnMasterTable.get(i).columnGroupAlias != null){
    				log.info("Yes atleast 1 column span group exists in the report template");
    				columnSpanGroupExists = true;
    				break;
    			}
    		}
	    		
    		if(columnSpanGroupExists == true){
    			log.info("Adding additional row to Header Row since column span group exists");
    			jdbcQueryResultDataTable.insertRow(new RowOperationParameters(0, -1, 1));
    		}
    		
    		RowHandle headerRow = (RowHandle) jdbcQueryResultDataTable.getHeader().get(0);
    		RowHandle detailRow = (RowHandle) jdbcQueryResultDataTable.getDetail().get(0);
    		RowHandle footerRow = (RowHandle) jdbcQueryResultDataTable.getFooter().get(0);
    		
    		
    		for(int i=0;i<templateColumnMasterTable.size();i++){
			try{
					//IF SINGLETON						
					if(templateColumnMasterTable.get(i).columnGroupAlias.equals("GRPALIASNONE")){
						log.info("ADDING SINGLETON COLUMN");
						 for(int j=0;j<templateColumnMasterTable.get(i).templateColumnSubTable.size();j++){
							 log.info("Adding column singleton header with label with name: " +templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnAlias()+ " & text : " + templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnName() + " to Header Row 0 at index: "+currentColumnIndex);
			                 label = designFactory.newLabel(null);
							 label = designFactory.newLabel(templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnName());	
			                 label.setText(templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnAlias().replace("_", "\n"));
			  	             cell = (CellHandle)headerRow.getCells().get(currentColumnIndex);
			  	             if(columnSpanGroupExists == true)
			  	             cell.setRowSpan(2);
			  	             cell.getContent().add(label);
			  	             cell.setStyleName("ReportTemplateDTHeaderStyle");
			  	             currentColumnIndex++;
				         }
					}
					// IF SINGLETON AND SPAN GROUP
					if(columnSpanGroupExists == true){
						log.info("ADDING COL SPAN FOR COLUMN GROUP");
						if(!templateColumnMasterTable.get(i).columnGroupAlias.equals("GRPALIASNONE")  && templateColumnMasterTable.get(i).columnGroupAlias != null){
							 log.info("Adding group column header with label with name: " +templateColumnMasterTable.get(i).getColumnGroupAlias()+ "to Header Row 0 at index: "+currentColumnIndex);
			                 label = designFactory.newLabel(templateColumnMasterTable.get(i).getColumnGroupAlias());	
			                 label.setText(templateColumnMasterTable.get(i).getColumnGroupAlias().replace("_", "\n"));
			  	             cell = (CellHandle)headerRow.getCells().get(currentColumnIndex);
			  	             cell.getContent().add(label);
			  	             cell.setStyleName("ReportTemplateDTHeaderStyle");
			  	             log.info("Adding colspan - templateColumnMasterTable.get(i).templateColumnSubTable.size():" +templateColumnMasterTable.get(i).templateColumnSubTable.size());
			  	             cell.setColumnSpan(templateColumnMasterTable.get(i).templateColumnSubTable.size());
			  	             currentColumnIndex=currentColumnIndex+(templateColumnMasterTable.get(i).templateColumnSubTable.size()-1);
			  	             log.info("currentColumnIndex..after adding colspan:" +currentColumnIndex);
						}
					}
				
				}catch (NullPointerException e){
						log.info("Exception:" +e.getMessage());
					    e.printStackTrace();
				}
    		 }
	    		
	    	if(columnSpanGroupExists == true){
	    		currentColumnIndex = 0;
	    		headerRow = (RowHandle) jdbcQueryResultDataTable.getHeader().get(1);
	    		
	    		for(int i=0;i<templateColumnMasterTable.size();i++){
				try{
					// IF SPAN GROUP						
					if(!templateColumnMasterTable.get(i).columnGroupAlias.equals("GRPALIASNONE") && templateColumnMasterTable.get(i).columnGroupAlias != null){
						
						 for(int j=0;j<templateColumnMasterTable.get(i).templateColumnSubTable.size();j++){
							 	 log.info("Adding member of COLUMN GROUP header with label with name & text : " +templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnAlias()+ " to Header Row 1 at index: "+currentColumnIndex);
								 label = designFactory.newLabel(templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnName());	
				                 label.setText(templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnAlias().replace("_", "\n"));
				  	             cell = (CellHandle)headerRow.getCells().get(currentColumnIndex);
				  	             cell.getContent().add(label);
				  	             cell.setStyleName("ReportTemplateDTHeaderStyle");
				  	             currentColumnIndex++;
				  	             
				         }
			            
					}
					
					}catch (NullPointerException e){
						log.info("Exception:" +e.getMessage());
						e.printStackTrace();
						}
				}
	    		}
	    	
	    	int currentColumnPointer = 0;
	    	int mainColumn = 0;
	    	while(mainColumn < templateColumnMasterTable.size()){
	    		ArrayList<IprodColumnGroup> iprodColumnGroupArray = templateColumnMasterTable.get(mainColumn).getTemplateColumnSubTable();
	    		int subColumn = 0; 
	    		while(subColumn < iprodColumnGroupArray.size()){
	    			IprodColumnGroup iprodColumnGroup = iprodColumnGroupArray.get(subColumn);
	    			addDataToDetailRow(detailRow,iprodColumnGroup,currentColumnPointer);
	    			addAggregateToFooterRow(footerRow,iprodColumnGroup,currentColumnPointer);
	    			currentColumnPointer++;
	    			subColumn++;
	    		}
	    		mainColumn++;
	    	}
	    	if(columnSpanGroupExists == true){
	    		removeExtraCells();
	    	}
	            
	            currentColumnIndex = 0; //used to mark current column position
	            
	             //add the table to my report
	            try {
					designHandle.getBody().add(jdbcQueryResultDataTable);
				} catch (Exception e) {
					log.info("Exception while adding datatable::" +e.getMessage());
					e.printStackTrace();
				}
	            log.info("Added table to design");
	            
	     
	     		
	     		//HEADER LEFT LOGO
	     		if(headerLeftLogoListL.size() > 0)
	            {
		            EmbeddedImage headerLeftLogo = StructureFactory.createEmbeddedImage();
		            headerLeftLogo.setName("Header Left Logo");
		            headerLeftLogo.setData(headerLeftLogoListL.get(0).getImageContent());
		            designHandle.addImage(headerLeftLogo);
	            }
	            
	            //HEADER RIGHT LOGO
	     		if(headerRightLogoListL.size() > 0)
	            {
		            EmbeddedImage headerRightLogo = StructureFactory.createEmbeddedImage();
		            headerRightLogo.setName("Header Right Logo");
		            headerRightLogo.setData(headerRightLogoListL.get(0).getImageContent());
		            designHandle.addImage(headerRightLogo);
	            }
	            
	            //FOOTER LEFT LOGO
	     		if(footerLeftLogoListL.size() > 0)
	     		{
		            EmbeddedImage footerLeftLogo = StructureFactory.createEmbeddedImage();
		            footerLeftLogo.setName("Footer Left Logo");
		            footerLeftLogo.setData(footerLeftLogoListL.get(0).getImageContent());
		            designHandle.addImage(footerLeftLogo);
	     		}
	            
	            //FOOTER RIGHT LOGO
	     		if(footerRightLogoListL.size() > 0)
	     		{
		            EmbeddedImage footerRightLogo = StructureFactory.createEmbeddedImage();
		            footerRightLogo.setName("Footer Right Logo");
		            footerRightLogo.setData(footerRightLogoListL.get(0).getImageContent());
		            designHandle.addImage(footerRightLogo);
	     		}
             
	     } catch (ContentException e) {
	         e.printStackTrace();
	         return false;
	     } catch (NameException e) {
	         e.printStackTrace();
	         return false;
	     } catch (SemanticException e) {
	         e.printStackTrace();
	         return false;
	     }
	    return true;
		}
	  catch(Exception e){
		  log.error("Exception in styling");
		  return false;
	  }
	}
	
	/**
	 * addDataToDetailRow
	 * Add DataItems to the Detail Row
	 * @param detailRow
	 * @param columnInstance
	 * @param currentDetailColumnIndex
	 */
	public void addDataToDetailRow(RowHandle detailRow, IprodColumnGroup columnInstance, int currentDetailColumnIndex){
		log.info("IprodReportsTemplates::addDataToDetailColumn for Column : "+columnInstance.getColumnName()+ " at index : "+currentDetailColumnIndex);
		// buildReportTemplateStyles();
        try {
		CellHandle cell = (CellHandle)detailRow.getCells().get(currentDetailColumnIndex);
		//add new data item and cell
        DataItemHandle data = designHandle.getElementFactory().newDataItem(columnInstance.getColumnName()+"_DATA");
        try {
			data.setResultSetColumn(columnInstance.getColumnName());
			FormatValue formattedValue = StructureFactory.newFormatValue();
        	data.setProperty("numberFormat", formattedValue);
        	formattedValue.setCategory("Currency");
        	formattedValue.setPattern("#,##0.00{RoundingMode=HALF_UP}");
		} catch (Exception e) {
			log.error("Exception::" +e.getMessage());
			
		}
        cell = (CellHandle)detailRow.getCells().get(currentDetailColumnIndex);
		cell.getContent().add(data);
		cell.setStyleName("ReportTemplateDTDetailStyle");
		 } catch (SemanticException e) {
			// TODO Auto-generated catch block
			log.info("Exception in addDataToDetailRow::" +e.getMessage());
			e.printStackTrace();
		 }
       	
	}
	
	public void addAggregateToFooterRow(RowHandle footerRow, IprodColumnGroup columnInstance, int currentFooterColumnIndex){
		log.info("IprodReportsTemplates::addAggregateToFooterColumn for Column : "+columnInstance.getColumnName()+ " at index : "+currentFooterColumnIndex);
		//buildReportTemplateStyles();
		try {
		CellHandle cell = (CellHandle)footerRow.getCells().get(currentFooterColumnIndex);
       	 switch(columnInstance.getColumnType().toLowerCase()){
            case "int"     : 	DataItemHandle data = designHandle.getElementFactory().newDataItem(columnInstance.getColumnName()+"_SUM");
								data.setResultSetColumn(columnInstance.getColumnName()+"_SUM");  	            
								FormatValue formattedValue = StructureFactory.newFormatValue();
					        	data.setProperty("numberFormat", formattedValue);
					        	formattedValue.setCategory("Currency");
					        	formattedValue.setPattern("#,##0.00{RoundingMode=HALF_UP}");
								cell.getContent().add(data);	  	            
				  	            cell.setStyleName("ReportTemplateDTDetailStyle");
            					break;
            case "varchar" : 	break;
            case "float"   : 	DataItemHandle data1 = designHandle.getElementFactory().newDataItem(columnInstance.getColumnName()+"_SUM");
            					data1.setResultSetColumn(columnInstance.getColumnName()+"_SUM");
            					FormatValue formattedValue1 = StructureFactory.newFormatValue();
            					data1.setProperty("numberFormat", formattedValue1);
            					formattedValue1.setCategory("Currency");
            					formattedValue1.setPattern("#,##0.00{RoundingMode=HALF_UP}");
            					cell.getContent().add(data1);	  	            
					            cell.setStyleName("ReportTemplateDTDetailStyle");
					            break;
            case "tinyint" :    break;
            default        : 	break;
         }
       	} catch (SemanticException e) {
			// TODO Auto-generated catch block
       		log.info("Exception in addAggregateToFooterRow::" +e.getMessage());
			e.printStackTrace();
		}
	}


	public String initReportTemplateEngine(){
		log.info("IprodReportsTemplatesAction : initReportTemplateEngine()" +this.rtemplateFooterRight+ "" +this.rtemplateFooterLeft+ "" +this.rtemplateHeaderLeft+ "" +this.rtemplateHeaderRight);
		log.info("Original SQL Query : " +this.rtemplateSqlquery);
		log.info("Removing extra spaces, characters from input sql query");
		this.rtemplateSqlquery = this.rtemplateSqlquery.replaceAll("\\s+", " ").replaceAll("\n","").replaceAll("\r", "");
		this.setRtemplateSqlquery(this.rtemplateSqlquery);
		log.info("SQL Query after removing unnecessary spaces & line breaks : " +this.getRtemplateSqlquery());
		String tableName = "";
		TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
		try {
			Select selectStatement = (Select)CCJSqlParserUtil.parse(this.rtemplateSqlquery);
			List<String> tableList = tablesNamesFinder.getTableList(selectStatement);
			tableName = tableList.get(0);
		} catch (JSQLParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		boolean setJDBCConnectionParmsFromDatasourceResult = setJDBCConnectionParmsFromDatasource(this.datasourceId);
		if(setJDBCConnectionParmsFromDatasourceResult == true){
			ArrayList<IprodReportsTableColumn> reportsTableComputedColumnList = new ArrayList<IprodReportsTableColumn>();
			tableName = tableName.substring(tableName.lastIndexOf('[')+1,tableName.lastIndexOf(']')).trim();
			log.info("tableName::" +tableName);
	 	    Connection conn;
			Statement st;
			String dbName;
			conn = getMsSqlConnection();
			if(conn != null){
				//RETREIVE COLUMN NAMES AND DATATYPE
				try{
				  dbName = conn.getCatalog();
				  log.info("dbName::" +dbName);
				  st = conn.createStatement();
			  	  java.sql.ResultSet rs = st.executeQuery("SELECT DATA_TYPE,COLUMN_NAME FROM "+dbName+".INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"+tableName+"'");
			  	  while (rs.next()){
		  	    	IprodReportsTableColumn reportsTableComputedColumn = new IprodReportsTableColumn();
		  	    	reportsTableComputedColumn.setColumnName(rs.getString("COLUMN_NAME"));
		  	    	reportsTableComputedColumn.setColumnDataType(rs.getString("DATA_TYPE"));
		  	    	reportsTableComputedColumnList.add(reportsTableComputedColumn);
		  			log.info("reportsTableComputedColumnList..:" +reportsTableComputedColumnList);
			  	  }
				  }catch (Exception e2){
					log.error("Exception::" +e2.getMessage());
					e2.printStackTrace();
				  }
				   //Configure the Engine and start the Platform
			       DesignConfig config = new DesignConfig();
			       IDesignEngine engine = null;
			       try{
			           Platform.startup(config);
			           IDesignEngineFactory factory = (IDesignEngineFactory) Platform.createFactoryObject(IDesignEngineFactory.EXTENSION_DESIGN_ENGINE_FACTORY);
			           engine = factory.createDesignEngine(config);
			       }
			       catch(Exception ex){
			           ex.printStackTrace();
			           return ERROR;
			       }
			     //The first step is to create an instance of the design engine.
			       SessionHandle session = engine.newSessionHandle(ULocale.ENGLISH) ;
			       try
			       {
					//open a design or a template
					designHandle = session.createDesign();
					//Next, get the "element factory" that creates new elements within your design
					designFactory = designHandle.getElementFactory();
					if(buildJDBCDataSource() == true){
						boolean buildJDBCDataSetResult = buildJDBCDataSet(this.rtemplateSqlquery);
						if(buildJDBCDataSetResult == true){
							boolean buildJDBCReportTemplateWithStyleAndDataTableResult = buildJDBCReportTemplateWithStyleAndDataTable(reportsTableComputedColumnList);
							if(buildJDBCReportTemplateWithStyleAndDataTableResult == true){
								ByteArrayOutputStream out = new ByteArrayOutputStream();
								try {
									DocumentUtil.serialize(designHandle, out);
								}catch (Exception e) {
									// TODO Auto-generated catch block
									log.info("Exception while serialize ::" + e.getMessage());
								}
								// Convert to String.
								streamTemplatedata = out.toString();
								log.info("value is ::" + streamTemplatedata);
				                designHandle.close();
							}
							if(buildJDBCReportTemplateWithStyleAndDataTableResult == false){
								log.error("Error in styling");
								return ERROR;
							}
						}
						if(buildJDBCDataSetResult == false){
							log.error("Error in setting query");
							return ERROR;
						}
					}
					if(buildJDBCDataSource() == false){
						log.error("Error in buiding Jdbc Datasource..Try Again!!");
						return ERROR;
					}
			       }
			       catch (Exception e){
			           e.printStackTrace();
			           return ERROR;
			       }
			}
			if(conn == null){
				log.error("Could Not Connect To Database..Try Again!!");
				return ERROR;
			}
		}
		if(setJDBCConnectionParmsFromDatasourceResult == false){
			log.error("Could Not Find Datasource...Try Again!!");
			return ERROR;
		}
		return SUCCESS;
		
	}
	

	 public void buildReportTemplateStyles(){
		StyleHandle reportTemplateMainTitleStyle = designFactory.newStyle("ReportTemplateMainTitleStyle");
		try {
			reportTemplateMainTitleStyle.setProperty( StyleHandle.FONT_WEIGHT_PROP, DesignChoiceConstants.FONT_WEIGHT_BOLD );
			reportTemplateMainTitleStyle.setProperty( StyleHandle.FONT_FAMILY_PROP, "Arial" );
			reportTemplateMainTitleStyle.setProperty( StyleHandle.FONT_SIZE_PROP, "18pt" );
			reportTemplateMainTitleStyle.setProperty( StyleHandle.COLOR_PROP, "#"+this.titleStyleTextColor);
			reportTemplateMainTitleStyle.setProperty( StyleHandle.BACKGROUND_COLOR_PROP, "#"+this.titleStyleBgColor);
			
			if(this.titleStyleHAlign.equals("1"))
				reportTemplateMainTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_DATA_CONTENT_TYPE_AUTO);
			else if(this.titleStyleHAlign.equals("2"))
				reportTemplateMainTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			else if(this.titleStyleHAlign.equals("3"))
				reportTemplateMainTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_RIGHT);
			else if(this.titleStyleHAlign.equals("4"))
				reportTemplateMainTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_JUSTIFY);
			else
				reportTemplateMainTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			
			
			if(this.titleStyleVAlign.equals("1"))
				reportTemplateMainTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_TOP);
			else if(this.titleStyleVAlign.equals("2"))
				reportTemplateMainTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			else if(this.titleStyleVAlign.equals("3"))
				reportTemplateMainTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_BOTTOM);
			else
				reportTemplateMainTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			

		    if(this.titleStyleBorderType.equals("no-border"))
		     	reportTemplateMainTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
		    else if(this.titleStyleBorderType.equals("thin-border"))
		    	reportTemplateMainTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,"");
		    else if(this.titleStyleBorderType.equals("dashed-border"))
		    	reportTemplateMainTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DASHED);
		    else if(this.titleStyleBorderType.equals("dotted-border"))
		    	reportTemplateMainTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOTTED);
		    else if(this.titleStyleBorderType.equals("double-border"))
		    	reportTemplateMainTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOUBLE);
		    else
		    	reportTemplateMainTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
		

			
		} catch (SemanticException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StyleHandle reportTemplateSubTitleStyle = designFactory.newStyle("ReportTemplateSubTitleStyle");
		try {
			reportTemplateSubTitleStyle.setProperty( StyleHandle.FONT_WEIGHT_PROP, DesignChoiceConstants.FONT_WEIGHT_BOLD );
			reportTemplateSubTitleStyle.setProperty( StyleHandle.FONT_FAMILY_PROP, "Arial" );
			reportTemplateSubTitleStyle.setProperty( StyleHandle.FONT_SIZE_PROP, "12pt" );
			reportTemplateSubTitleStyle.setProperty( StyleHandle.COLOR_PROP, "#"+this.subTitleStyleTextColor);
			reportTemplateSubTitleStyle.setProperty( StyleHandle.BACKGROUND_COLOR_PROP, "#"+this.subTitleStyleBgColor);
			
			if(this.subTitleStyleVAlign.equals("1"))
				reportTemplateSubTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_LEFT);
			else if(this.subTitleStyleHAlign.equals("2"))
				reportTemplateSubTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			else if(this.subTitleStyleHAlign.equals("3"))
				reportTemplateSubTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_RIGHT);
			else if(this.subTitleStyleHAlign.equals("4"))
				reportTemplateSubTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_JUSTIFY);
			else
				reportTemplateSubTitleStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			
			if(this.subTitleStyleVAlign.equals("1"))
				reportTemplateSubTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_TOP);
			else if(this.subTitleStyleVAlign.equals("2"))
				reportTemplateSubTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			else if(this.subTitleStyleVAlign.equals("3"))
				reportTemplateSubTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_BOTTOM);
			else
				reportTemplateSubTitleStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);

		    if(this.subTitleStyleBorderType.equals("no-border"))
		    	reportTemplateSubTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
		    else if(this.subTitleStyleBorderType.equals("thin-border"))
		    	reportTemplateSubTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,"");
		    else if(this.subTitleStyleBorderType.equals("dashed-border"))
		    	reportTemplateSubTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DASHED);
		    else if(this.subTitleStyleBorderType.equals("dotted-border"))
		    	reportTemplateSubTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOTTED);
		    else if(this.subTitleStyleBorderType.equals("double-border"))
		    	reportTemplateSubTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOUBLE);
		    else
		    	reportTemplateSubTitleStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
		
			
			
		} catch (SemanticException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StyleHandle reportTemplateDTHeaderStyle = designFactory.newStyle("ReportTemplateDTHeaderStyle");
		try {
			reportTemplateDTHeaderStyle.setProperty(StyleHandle.FONT_WEIGHT_PROP, DesignChoiceConstants.FONT_WEIGHT_BOLD);
			reportTemplateDTHeaderStyle.setProperty(StyleHandle.FONT_FAMILY_PROP, "sans-serif" );
			reportTemplateDTHeaderStyle.setProperty(StyleHandle.FONT_SIZE_PROP, "8pt" );
			reportTemplateDTHeaderStyle.setProperty(StyleHandle.COLOR_PROP, "#"+this.headerStyleTextColor);
			reportTemplateDTHeaderStyle.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#"+this.headerStyleBgColor);	
			reportTemplateDTHeaderStyle.setProperty(StyleHandle.WIDTH_PROP,"2.5in");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_COLOR_PROP, "#000000");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP, "solid");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_WIDTH_PROP, "1px");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_LEFT_COLOR_PROP, "#000000");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_LEFT_STYLE_PROP, "solid");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_LEFT_WIDTH_PROP, "1px");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_TOP_COLOR_PROP, "#000000");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_TOP_STYLE_PROP, "solid");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_TOP_WIDTH_PROP, "1px");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_RIGHT_COLOR_PROP, "#000000");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_RIGHT_STYLE_PROP, "solid");
			reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_RIGHT_WIDTH_PROP, "1px");
			
			if(this.headerStyleHAlign.equals("1"))
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_LEFT);
			else if(this.headerStyleHAlign.equals("2"))
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			else if(this.headerStyleHAlign.equals("3"))
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_RIGHT);
			else if(this.headerStyleHAlign.equals("4"))
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_JUSTIFY);
			else
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			
			
			if(this.headerStyleVAlign.equals("1"))
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_TOP);
			else if(this.headerStyleVAlign.equals("2"))
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			else if(this.headerStyleVAlign.equals("3"))
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_BOTTOM);
			else
				reportTemplateDTHeaderStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			
			
		    if(this.headerStyleBorderType.equals("thin-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_SOLID);
		    else if(this.headerStyleBorderType.equals("dashed-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DASHED);
		    else if(this.headerStyleBorderType.equals("dotted-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOTTED);
		    else if(this.headerStyleBorderType.equals("double-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOUBLE);
		    else
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_SOLID);
		    
			} catch (SemanticException e) {
				e.printStackTrace();
		}
		
		StyleHandle reportTemplateDTDetailStyle = designFactory.newStyle("ReportTemplateDTDetailStyle");
		try {
			reportTemplateDTDetailStyle.setProperty(StyleHandle.FONT_WEIGHT_PROP, DesignChoiceConstants.FONT_WEIGHT_LIGHTER );
			reportTemplateDTDetailStyle.setProperty(StyleHandle.FONT_FAMILY_PROP, "sans-serif" );
			reportTemplateDTDetailStyle.setProperty(StyleHandle.FONT_SIZE_PROP, "8pt" );
			reportTemplateDTDetailStyle.setProperty(StyleHandle.COLOR_PROP, "#"+this.detailStyleTextColor);
			reportTemplateDTDetailStyle.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#"+this.detailStyleBgColor);
			reportTemplateDTDetailStyle.setProperty(StyleHandle.WIDTH_PROP,"2.5in");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_BOTTOM_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP, DesignChoiceConstants.LINE_STYLE_SOLID);
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_BOTTOM_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_LEFT_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_LEFT_STYLE_PROP, "solid");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_LEFT_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_TOP_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_TOP_STYLE_PROP, "solid");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_TOP_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_RIGHT_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_RIGHT_STYLE_PROP, "solid");
			reportTemplateDTDetailStyle.setProperty( StyleHandle.BORDER_RIGHT_WIDTH_PROP, "1px");
			

			
			if(this.detailStyleHAlign.equals("1"))
				reportTemplateDTDetailStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_LEFT);
			else if(this.detailStyleHAlign.equals("2"))
				reportTemplateDTDetailStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			else if(this.detailStyleHAlign.equals("3"))
				reportTemplateDTDetailStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_RIGHT);
			else if(this.detailStyleHAlign.equals("4"))
				reportTemplateDTDetailStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_JUSTIFY);
			else
				reportTemplateDTDetailStyle.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			
			
			if(this.detailStyleVAlign.equals("1"))
				reportTemplateDTDetailStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_TOP);
			else if(this.detailStyleVAlign.equals("2"))
				reportTemplateDTDetailStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			else if(this.detailStyleVAlign.equals("3"))
				reportTemplateDTDetailStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_BOTTOM);
			else
				reportTemplateDTDetailStyle.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			
		  if(this.detailStyleBorderType.equals("no-border"))
			  reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
		    else if(this.detailStyleBorderType.equals("thin-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,"");
		    else if(this.detailStyleBorderType.equals("dashed-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DASHED);
		    else if(this.detailStyleBorderType.equals("dotted-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOTTED);
		    else if(this.detailStyleBorderType.equals("double-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOUBLE);
		    else
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_SOLID);
				
			
			
		} catch (SemanticException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		//FOR PDF
		
		StyleHandle reportTemplateDTDetailStyleForPDF = designFactory.newStyle("reportTemplateDTDetailStyleForPDF");
		try {
			reportTemplateDTDetailStyleForPDF.setProperty(StyleHandle.FONT_WEIGHT_PROP, DesignChoiceConstants.FONT_WEIGHT_LIGHTER );
			reportTemplateDTDetailStyleForPDF.setProperty(StyleHandle.FONT_FAMILY_PROP, "sans-serif" );
			reportTemplateDTDetailStyleForPDF.setProperty(StyleHandle.FONT_SIZE_PROP, "5pt" );
			reportTemplateDTDetailStyleForPDF.setProperty(StyleHandle.COLOR_PROP, "#"+this.detailStyleTextColor);
			reportTemplateDTDetailStyleForPDF.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#"+this.detailStyleBgColor);
			reportTemplateDTDetailStyleForPDF.setProperty(StyleHandle.WIDTH_PROP,"2.5in");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_BOTTOM_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP, DesignChoiceConstants.LINE_STYLE_SOLID);
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_BOTTOM_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_LEFT_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_LEFT_STYLE_PROP, "solid");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_LEFT_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_TOP_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_TOP_STYLE_PROP, "solid");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_TOP_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_RIGHT_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_RIGHT_STYLE_PROP, "solid");
			reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.BORDER_RIGHT_WIDTH_PROP, "1px");

			
			if(this.detailStyleHAlign.equals("1"))
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_LEFT);
			else if(this.detailStyleHAlign.equals("2"))
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			else if(this.detailStyleHAlign.equals("3"))
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_RIGHT);
			else if(this.detailStyleHAlign.equals("4"))
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_JUSTIFY);
			else
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			
			
			if(this.detailStyleVAlign.equals("1"))
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_TOP);
			else if(this.detailStyleVAlign.equals("2"))
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			else if(this.detailStyleVAlign.equals("3"))
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_BOTTOM);
			else
				reportTemplateDTDetailStyleForPDF.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			
		  if(this.detailStyleBorderType.equals("no-border"))
			  	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
		    else if(this.detailStyleBorderType.equals("thin-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,"");
		    else if(this.detailStyleBorderType.equals("dashed-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DASHED);
		    else if(this.detailStyleBorderType.equals("dotted-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOTTED);
		    else if(this.detailStyleBorderType.equals("double-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOUBLE);
		    else
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
				
			
			
		} catch (SemanticException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		//FOR EXCEL
		
		StyleHandle reportTemplateDTDetailStyleForEXCEL = designFactory.newStyle("reportTemplateDTDetailStyleForEXCEL");
		try {
			reportTemplateDTDetailStyleForEXCEL.setProperty(StyleHandle.FONT_WEIGHT_PROP, DesignChoiceConstants.FONT_WEIGHT_LIGHTER );
			reportTemplateDTDetailStyleForEXCEL.setProperty(StyleHandle.FONT_FAMILY_PROP, "sans-serif" );
			reportTemplateDTDetailStyleForEXCEL.setProperty(StyleHandle.FONT_SIZE_PROP, "8pt" );
			reportTemplateDTDetailStyleForEXCEL.setProperty(StyleHandle.COLOR_PROP, "#"+this.detailStyleTextColor);
			reportTemplateDTDetailStyleForEXCEL.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#"+this.detailStyleBgColor);
			reportTemplateDTDetailStyleForEXCEL.setProperty(StyleHandle.WIDTH_PROP,"2.5in");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_BOTTOM_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP, DesignChoiceConstants.LINE_STYLE_SOLID);
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_BOTTOM_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_LEFT_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_LEFT_STYLE_PROP, "solid");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_LEFT_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_TOP_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_TOP_STYLE_PROP, "solid");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_TOP_WIDTH_PROP, "1px");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_RIGHT_COLOR_PROP, "#000000");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_RIGHT_STYLE_PROP, "solid");
			reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.BORDER_RIGHT_WIDTH_PROP, "1px");

			
			if(this.detailStyleHAlign.equals("1"))
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_LEFT);
			else if(this.detailStyleHAlign.equals("2"))
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			else if(this.detailStyleHAlign.equals("3"))
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_RIGHT);
			else if(this.detailStyleHAlign.equals("4"))
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_JUSTIFY);
			else
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.TEXT_ALIGN_PROP, DesignChoiceConstants.TEXT_ALIGN_CENTER);
			
			
			if(this.detailStyleVAlign.equals("1"))
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_TOP);
			else if(this.detailStyleVAlign.equals("2"))
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			else if(this.detailStyleVAlign.equals("3"))
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_BOTTOM);
			else
				reportTemplateDTDetailStyleForEXCEL.setProperty( StyleHandle.VERTICAL_ALIGN_PROP,DesignChoiceConstants.VERTICAL_ALIGN_MIDDLE);
			
		  if(this.detailStyleBorderType.equals("no-border"))
			  reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
		    else if(this.detailStyleBorderType.equals("thin-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,"");
		    else if(this.detailStyleBorderType.equals("dashed-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DASHED);
		    else if(this.detailStyleBorderType.equals("dotted-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOTTED);
		    else if(this.detailStyleBorderType.equals("double-border"))
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_DOUBLE);
		    else
		    	reportTemplateDTHeaderStyle.setProperty( StyleHandle.BORDER_BOTTOM_STYLE_PROP,DesignChoiceConstants.LINE_STYLE_NONE);
				
			
			
		} catch (SemanticException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		StyleHandle reportTemplateFooterStyle = designFactory.newStyle("ReportTemplateFooterStyle");
		try {
			reportTemplateFooterStyle.setProperty(StyleHandle.FONT_WEIGHT_PROP, DesignChoiceConstants.FONT_WEIGHT_BOLD );
			reportTemplateFooterStyle.setProperty(StyleHandle.FONT_FAMILY_PROP, "Arial Black" );
			reportTemplateFooterStyle.setProperty(StyleHandle.COLOR_PROP, "#000000" );
			reportTemplateFooterStyle.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#EAEAEA");
			reportTemplateFooterStyle.setProperty(StyleHandle.VERTICAL_ALIGN_PROP,"");
			reportTemplateFooterStyle.setProperty(StyleHandle.BORDER_BOTTOM_STYLE_PROP,"");
			} catch (SemanticException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		
		try {
			designHandle.getStyles().add(reportTemplateMainTitleStyle);
			designHandle.getStyles().add(reportTemplateSubTitleStyle);
			designHandle.getStyles().add(reportTemplateDTHeaderStyle);
			designHandle.getStyles().add(reportTemplateDTDetailStyle);
			designHandle.getStyles().add(reportTemplateFooterStyle);
			designHandle.getStyles().add(reportTemplateDTDetailStyleForPDF);
			designHandle.getStyles().add(reportTemplateDTDetailStyleForEXCEL);
			
		} catch (ContentException | NameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	 
		public List<IprodImageMaster> getAllImageList()
		{
			log.info("IprodReportsTemplatesAction :: getAllImageList");
			IprodImageMasterDao IprodImageMasterDao = DaoFactory.createIprodImageMasterDao();
			List<IprodImageMaster> IprodImageMasterListL = new ArrayList<IprodImageMaster>();
			try 
			{	
				IprodImageMasterListL = IprodImageMasterDao.findWhereImageIdAndIsDeletedEquals(this.imageId,(short)0);
				if(IprodImageMasterListL.size() > 0)
				{				  		  				  		  				  			  
					IprodImageMasterListG = IprodImageMasterListL;
					
				}
				else
				{			  	
					addActionError("Currently there are no roles to be displayed");
					//return ERROR;
				}
			} 
			catch (IprodImageMasterDaoException e)
			{
				addActionError("IprodImageMasterDaoException"+e.getMessage());
				//return ERROR;
			}
			return IprodImageMasterListG;	
		}
		
    
	public Boolean removeExtraCells(){
        try{
        	RowHandle rowHandleZero = (RowHandle) jdbcQueryResultDataTable.getHeader().get(0);
        	RowHandle rowHandleFirst = (RowHandle) jdbcQueryResultDataTable.getHeader().get(1);
            int next = rowHandleZero.getCells().getCount() - 1;
            while(next >=0 ){
            	CellHandle tempCellFirst = (CellHandle) rowHandleFirst.getCells().get(next);
            	if(tempCellFirst.getContent().getCount() == 0){
            		tempCellFirst.drop();
            	}
            	CellHandle tempCellZero= (CellHandle) rowHandleZero.getCells().get(next);
            	if(tempCellZero.getContent().getCount() == 0){
            		tempCellZero.drop();
            	}
            	next--;
            
            }
        }catch(BirtException ex){
//        	System.err.println("Remove Exception " + ex);
        	log.info("Remove Extra cell " + ex);
        }
		return false;
	}
	
	
	public Boolean removeExtraCellsOnHeader(RowHandle row){
        try{
        	int i = row.getCells().getCount() - 1;
        	while(i  >= 0){
        		CellHandle cellHandle = (CellHandle) row.getCells().get(i);
        		if(cellHandle.getContent().getCount() == 0){
        			cellHandle.drop();
        		}
        		i--;
        	}	
        }catch(BirtException ex){
        	log.error("Remove Extra cell " + ex);
        }
		return false;
	}
	
	
	public Integer getTotalColumnsInMainTable(){
		Integer totalColumnsInMainTable = 0;
		   for(int i=0;i<templateColumnMasterTable.size();i++){
			   for(int j=0;j<templateColumnMasterTable.get(i).templateColumnSubTable.size();j++){
			   if(!templateColumnMasterTable.get(i).templateColumnSubTable.get(j).getColumnName().equals(""))
				   totalColumnsInMainTable++;
			   }
		   }
		return totalColumnsInMainTable;
	}
	
	public String getAllImages()
	{
		log.info("IprodReportsTemplatesAction :: getAllImages");
		IprodImageMasterDao IprodImageMasterDao = DaoFactory.createIprodImageMasterDao();
		List<IprodImageMaster> IprodImageMasterListL = new ArrayList<IprodImageMaster>();
		try 
		{	
			IprodImageMasterListL = IprodImageMasterDao.findWhereIsDeletedEquals((short)0);
			if(IprodImageMasterListL.size() > 0)
			{				  		  				  		  				  			  
				IprodImageMasterListG = IprodImageMasterListL;
				
			}
			else
			{			  	
				addActionError("Currently there are no roles to be displayed");
				//return ERROR;
			}
		} 
		catch (IprodImageMasterDaoException e)
		{
			addActionError("IprodImageMasterDaoException"+e.getMessage());
			//return ERROR;
		}
		return SUCCESS;	
	}
	
	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getRtemplateName'
	 * 
	 * @return String
	 */
	public String getRtemplateName()
	{
		return rtemplateName;
	}

	/**
	 * Method 'setRtemplateName'
	 * 
	 * @param rtemplateName
	 */
	public void setRtemplateName(String rtemplateName)
	{
		this.rtemplateName = rtemplateName;
	}

	/**
	 * Method 'getRtemplateTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateTitle()
	{
		return rtemplateTitle;
	}

	/**
	 * Method 'setRtemplateTitle'
	 * 
	 * @param rtemplateTitle
	 */
	public void setRtemplateTitle(String rtemplateTitle)
	{
		this.rtemplateTitle = rtemplateTitle;
	}

	/**
	 * Method 'getRtemplateSubTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateSubTitle()
	{
		return rtemplateSubTitle;
	}

	/**
	 * Method 'setRtemplateSubTitle'
	 * 
	 * @param rtemplateSubTitle
	 */
	public void setRtemplateSubTitle(String rtemplateSubTitle)
	{
		this.rtemplateSubTitle = rtemplateSubTitle;
	}

	/**
	 * Method 'getRtemplateSqlquery'
	 * 
	 * @return String
	 */
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

	/**
	 * Method 'setRtemplateSqlquery'
	 * 
	 * @param rtemplateSqlquery
	 */
	public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	/**
	 * @return the reportQuery
	 */
	public String getReportQuery() {
		return reportQuery;
	}


	/**
	 * @param reportQuery the reportQuery to set
	 */
	public void setReportQuery(String reportQuery) {
		this.reportQuery = reportQuery;
	}

	/**
	 * Method 'getDatasourceId'
	 * 
	 * @return int
	 */
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'setDatasourceId'
	 * 
	 * @param datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
	}
	
	public String getReportLeftLogo() {
		return reportLeftLogo;
	}
	
	public void setReportLeftLogo(String reportLeftLogo) {
		this.reportLeftLogo = reportLeftLogo;
	}
	
	public String getReportRightLogo() {
		return reportRightLogo;
	}
	
	public void setReportRightLogo(String reportRightLogo) {
		this.reportRightLogo = reportRightLogo;
	}
	
	public String getReportFooterLeftLogo() {
		return reportFooterLeftLogo;
	}
	
	public void setReportFooterLeftLogo(String reportFooterLeftLogo) {
		this.reportFooterLeftLogo = reportFooterLeftLogo;
	}
	
	public String getReportFooterTitle() {
		return reportFooterTitle;
	}
	
	public void setReportFooterTitle(String reportFooterTitle) {
		this.reportFooterTitle = reportFooterTitle;
	}
	
	public String getReportFooterRightLogo() {
		return reportFooterRightLogo;
	}
	
	public void setReportFooterRightLogo(String reportFooterRightLogo) {
		this.reportFooterRightLogo = reportFooterRightLogo;
	}
	
	public String getViewDropDown() {
		return viewDropDown;
	}
	
	public void setViewDropDown(String viewDropDown) {
		this.viewDropDown = viewDropDown;
	}
	
	/*public File getRtemplateHeaderLeft() {
		return rtemplateHeaderLeft;
	}
	
	public void setRtemplateHeaderLeft(File rtemplateHeaderLeft) {
		this.rtemplateHeaderLeft = rtemplateHeaderLeft;
	}

	public File getRtemplateHeaderRight() {
		return rtemplateHeaderRight;
	}
	
	public void setRtemplateHeaderRight(File rtemplateHeaderRight) {
		this.rtemplateHeaderRight = rtemplateHeaderRight;
	}
	
  
	public File getRtemplateFooterLeft() {
		return rtemplateFooterLeft;
	}
	
	public void setRtemplateFooterLeft(File rtemplateFooterLeft) {
		this.rtemplateFooterLeft = rtemplateFooterLeft;
	}
	
	    
	public File getRtemplateFooterRight() {
		return rtemplateFooterRight;
	}
	
	public void setRtemplateFooterRight(File rtemplateFooterRight) {
		this.rtemplateFooterRight = rtemplateFooterRight;
	}*/
	
	public String getTemplateRepo() {
		return templateRepo;
	}
	
	public void setTemplateRepo(String templateRepo) {
		this.templateRepo = templateRepo;
	}
	
	public String getHeaderStyleBgColor() {
		return headerStyleBgColor;
	}

	public void setHeaderStyleBgColor(String headerStyleBgColor) {
		this.headerStyleBgColor = headerStyleBgColor;
	}

	public String getHeaderStyleHAlign() {
		return headerStyleHAlign;
	}

	public void setHeaderStyleHAlign(String headerStyleHAlign) {
		this.headerStyleHAlign = headerStyleHAlign;
	}

	public String getHeaderStyleVAlign() {
		return headerStyleVAlign;
	}

	public void setHeaderStyleVAlign(String headerStyleVAlign) {
		this.headerStyleVAlign = headerStyleVAlign;
	}

	public String getHeaderStyleBorderType() {
		return headerStyleBorderType;
	}

	public void setHeaderStyleBorderType(String headerStyleBorderType) {
		this.headerStyleBorderType = headerStyleBorderType;
	}

	public String getDetailStyleBgColor() {
		return detailStyleBgColor;
	}

	public void setDetailStyleBgColor(String detailStyleBgColor) {
		this.detailStyleBgColor = detailStyleBgColor;
	}

	public String getDetailStyleHAlign() {
		return detailStyleHAlign;
	}

	public void setDetailStyleHAlign(String detailStyleHAlign) {
		this.detailStyleHAlign = detailStyleHAlign;
	}

	public String getDetailStyleVAlign() {
		return detailStyleVAlign;
	}

	public void setDetailStyleVAlign(String detailStyleVAlign) {
		this.detailStyleVAlign = detailStyleVAlign;
	}

	public String getDetailStyleBorderType() {
		return detailStyleBorderType;
	}

	public void setDetailStyleBorderType(String detailStyleBorderType) {
		this.detailStyleBorderType = detailStyleBorderType;
	}


	public String getTitleStyleBgColor() {
		return titleStyleBgColor;
	}

	public void setTitleStyleBgColor(String titleStyleBgColor) {
		this.titleStyleBgColor = titleStyleBgColor;
	}

	public String getTitleStyleHAlign() {
		return titleStyleHAlign;
	}

	public void setTitleStyleHAlign(String titleStyleHAlign) {
		this.titleStyleHAlign = titleStyleHAlign;
	}

	public String getTitleStyleVAlign() {
		return titleStyleVAlign;
	}

	public void setTitleStyleVAlign(String titleStyleVAlign) {
		this.titleStyleVAlign = titleStyleVAlign;
	}

	public String getTitleStyleBorderType() {
		return titleStyleBorderType;
	}

	public void setTitleStyleBorderType(String titleStyleBorderType) {
		this.titleStyleBorderType = titleStyleBorderType;
	}

	public String getSubTitleStyleBgColor() {
		return subTitleStyleBgColor;
	}

	public void setSubTitleStyleBgColor(String subTitleStyleBgColor) {
		this.subTitleStyleBgColor = subTitleStyleBgColor;
	}

	public String getSubTitleStyleHAlign() {
		return subTitleStyleHAlign;
	}

	public void setSubTitleStyleHAlign(String subTitleStyleHAlign) {
		this.subTitleStyleHAlign = subTitleStyleHAlign;
	}

	public String getSubTitleStyleVAlign() {
		return subTitleStyleVAlign;
	}

	public void setSubTitleStyleVAlign(String subTitleStyleVAlign) {
		this.subTitleStyleVAlign = subTitleStyleVAlign;
	}

	public String getSubTitleStyleBorderType() {
		return subTitleStyleBorderType;
	}

	public void setSubTitleStyleBorderType(String subTitleStyleBorderType) {
		this.subTitleStyleBorderType = subTitleStyleBorderType;
	}


	public String getHeaderStyleTextColor() {
		return headerStyleTextColor;
	}


	public void setHeaderStyleTextColor(String headerStyleTextColor) {
		this.headerStyleTextColor = headerStyleTextColor;
	}


	public String getDetailStyleTextColor() {
		return detailStyleTextColor;
	}


	public void setDetailStyleTextColor(String detailStyleTextColor) {
		this.detailStyleTextColor = detailStyleTextColor;
	}


	public String getTitleStyleTextColor() {
		return titleStyleTextColor;
	}


	public void setTitleStyleTextColor(String titleStyleTextColor) {
		this.titleStyleTextColor = titleStyleTextColor;
	}


	public String getSubTitleStyleTextColor() {
		return subTitleStyleTextColor;
	}


	public void setSubTitleStyleTextColor(String subTitleStyleTextColor) {
		this.subTitleStyleTextColor = subTitleStyleTextColor;
	}


	public String getHeaderStyleTransparent() {
		return headerStyleTransparent;
	}


	public void setHeaderStyleTransparent(String headerStyleTransparent) {
		this.headerStyleTransparent = headerStyleTransparent;
	}


	public String getDetailStyleTransparent() {
		return detailStyleTransparent;
	}


	public void setDetailStyleTransparent(String detailStyleTransparent) {
		this.detailStyleTransparent = detailStyleTransparent;
	}


	public String getTitleStyleTransparent() {
		return titleStyleTransparent;
	}


	public void setTitleStyleTransparent(String titleStyleTransparent) {
		this.titleStyleTransparent = titleStyleTransparent;
	}


	public String getSubTitleStyleTransparent() {
		return subTitleStyleTransparent;
	}


	public void setSubTitleStyleTransparent(String subTitleStyleTransparent) {
		this.subTitleStyleTransparent = subTitleStyleTransparent;
	}
	
	
	public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
			return hashMapCheckRoleModuleOperationListG;
	}

	public void setHashMapCheckRoleModuleOperationListG(
			HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
		this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
	}

	public ArrayList<IprodTemplateColumnMasterTable> getTemplateColumnMasterTable() {
		return templateColumnMasterTable;
	}

	public void setTemplateColumnMasterTable(ArrayList<IprodTemplateColumnMasterTable> templateColumnMasterTable) {
		this.templateColumnMasterTable = templateColumnMasterTable;
	}

	public String getRtemplateHeaderLeft() {
		return rtemplateHeaderLeft;
	}

	public void setRtemplateHeaderLeft(String rtemplateHeaderLeft) {
		this.rtemplateHeaderLeft = rtemplateHeaderLeft;
	}

	public String getRtemplateHeaderRight() {
		return rtemplateHeaderRight;
	}

	public void setRtemplateHeaderRight(String rtemplateHeaderRight) {
		this.rtemplateHeaderRight = rtemplateHeaderRight;
	}

	public String getRtemplateFooterLeft() {
		return rtemplateFooterLeft;
	}

	public void setRtemplateFooterLeft(String rtemplateFooterLeft) {
		this.rtemplateFooterLeft = rtemplateFooterLeft;
	}

	public String getRtemplateFooterRight() {
		return rtemplateFooterRight;
	}

	public void setRtemplateFooterRight(String rtemplateFooterRight) {
		this.rtemplateFooterRight = rtemplateFooterRight;
	}

	public String getPageOrientation() {
		return pageOrientation;
	}

	public void setPageOrientation(String pageOrientation) {
		this.pageOrientation = pageOrientation;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getStreamTemplatedata() {
		return streamTemplatedata;
	}

	public void setStreamTemplatedata(String streamTemplatedata) {
		this.streamTemplatedata = streamTemplatedata;
	}
}