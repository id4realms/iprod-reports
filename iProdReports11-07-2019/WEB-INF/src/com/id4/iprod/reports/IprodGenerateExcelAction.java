package com.id4.iprod.reports;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.birt.report.engine.api.EXCELRenderOption;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.TableHandle;
import org.eclipse.birt.report.model.api.command.StyleException;

import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.reports.dao.IprodReportsInstanceDao;

import uk.co.spudsoft.birt.emitters.excel.ExcelEmitter;

public class IprodGenerateExcelAction {

	static Logger log = Logger.getLogger(IprodGenerateExcelAction.class);

	public void generateEXCEL(IReportEngine engine1, IReportRunnable design1, int reportId, String reportTemplateName) {
		log.info("IprodGenerateExcelAction:: generateEXCEL: " + reportTemplateName);

		// Style for Excel only...
		try {
			ReportDesignHandle reportDesignHandle = (ReportDesignHandle) design1.getDesignHandle();
			TableHandle table = (TableHandle) reportDesignHandle.findElement("JdbcQueryResultDataTable");
			
			RowHandle detailRow = (RowHandle) table.getDetail().get(0);
			log.info("IprodGenerateExcelAction:: DetailRow.getCells().getCount() : " + detailRow.getCells().getCount());
			int detailRowCounter = 0;
			while (detailRowCounter < detailRow.getCells().getCount()) {
				detailRow.getCells().get(detailRowCounter).setStyleName("reportTemplateDTDetailStyleForEXCEL");
				detailRowCounter++;
			}
		} catch (StyleException e) {
			e.printStackTrace();
			log.error("Exception  for Excel Styling :: " + e.getMessage());
		}

		File dir = new File("D:\\REPORTS");
		if (!dir.exists()) {
			if (dir.mkdir()) {
				log.info("Directory is created!");
			} else {
				log.info("Failed to create directory!");
			}
		}
		// Date and time use for give name for excel file
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("dd-MMM-yyyy-HH-mm-ss");
		String currentTime = ft.format(dNow);
		try {
			IRunAndRenderTask task = engine1.createRunAndRenderTask(design1);
			EXCELRenderOption options = new EXCELRenderOption();
			options.setEmitterID("uk.co.spudsoft.birt.emitters.excel.XlsEmitter");
			
			options.setOption(ExcelEmitter.FORCEAUTOCOLWIDTHS_PROP, true);
			options.setOption(ExcelEmitter.BLANK_ROW_AFTER_TOP_LEVEL_TABLE, true); 
			options.setOption(ExcelEmitter.DEBUG, true);	
			
			options.setOption(ExcelEmitter.REMOVE_BLANK_ROWS,true);				
			options.setOption(ExcelEmitter.STRUCTURED_HEADER, true);
			options.setOption(ExcelEmitter.PRINT_SCALE, 15);
			options.setOption(ExcelEmitter.PRINT_PAGES_WIDE, 1);
			options.setOption(ExcelEmitter.PRINT_PAGES_HIGH, 0);
			options.setOption(ExcelEmitter.DISPLAYROWCOLHEADINGS_PROP, true);
			options.setEnableMultipleSheet(true);
			options.setOutputFileName("D:/REPORTS/" + reportTemplateName + "-" + currentTime + ".xls");
			task.setRenderOption(options);
			log.info("IprodGenerateExcelAction :: options.isEnableMultipleSheet() : " + options.isEnableMultipleSheet());
			task.run();
			task.close();
			log.info("IprodGenerateExcelAction :: EXCEL FILE GENERATED SUCCESSFULLY");
		} catch (EngineException e) {
			log.error(" IprodGenerateExcelAction :: REPORT GENERATION IN EXCEL FORMAT FAILED DUE TO EngineException : " + e.toString());
		} catch (Exception e1) {
			log.error("IprodGenerateExcelAction :: REPORT GENERATION IN EXCEL FORMAT FAILED DUE TO General Exception : " + e1.toString());
		}
		// UPDATE THE PATH IN REPORT_INSTANCE TABLE
		IprodReportsInstanceDao instanceDao = DaoFactory.createIprodReportsInstanceDao();
		try {
			instanceDao.updateReportExcelLocationWhereReportIdIs(reportId, "D:/REPORTS/" + reportTemplateName + "-" + currentTime + ".xls");
		} catch (Exception e) {
			log.error("IprodGenerateExcelAction :: Exception while updating Report Excel location" + e.getMessage());
			e.printStackTrace();
		}

	}
}
