package com.id4.iprod.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.Consts;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.id4.iprod.reports.dao.IprodReportsViewReportsInstanceDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportsInstanceDetails;
import com.id4.iprod.reports.exceptions.IprodReportsViewReportsInstanceDetailsDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodReportsViewReportInstanceDetailsAction extends ActionSupport implements IprodModuleAuthorization{
	static Logger log = Logger.getLogger(IprodReportsViewReportInstanceDetailsAction.class);
	/** 
	 * This attribute maps to the column REPORT_ID in the iprodreports_view_report_instance_details table.
	 */
	protected int reportId;

	/** 
	 * This attribute maps to the column USER_ID in the iprodreports_view_report_instance_details table.
	 */
	protected int userId;
	
	/** 
	 * This attribute maps to the column USER_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column REPORT_QUERY in the iprodreports_view_report_instance_details table.
	 */
	protected String reportQuery;

	/** 
	 * This attribute maps to the column REPORT_EXCEL_LOCATION in the iprodreports_view_report_instance_details table.
	 */
	protected String reportExcelLocation;

	/** 
	 * This attribute maps to the column REPORT_PDF_LOCATION in the iprodreports_view_report_instance_details table.
	 */
	protected String reportPdfLocation;

	/** 
	 * This attribute maps to the column REPORT_CDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String reportCdatetime;

	/** 
	 * This attribute maps to the column REPORT_MDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String reportMdatetime;

	/** 
	 * This attribute maps to the column REPORT_IS_SCHEDULED in the iprodreports_view_report_instance_details table.
	 */
	protected short reportIsScheduled;

	/** 
	 * This attribute represents whether the primitive attribute reportIsScheduled is null.
	 */
	protected boolean reportIsScheduledNull = true;

	/** 
	 * This attribute maps to the column REPORT_IS_DELETED in the iprodreports_view_report_instance_details table.
	 */
	protected short reportIsDeleted;

	/** 
	 * This attribute represents whether the primitive attribute reportIsDeleted is null.
	 */
	protected boolean reportIsDeletedNull = true;

	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprodreports_view_report_instance_details table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column RTEMPLATE_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String rtemplateName;

	/** 
	 * This attribute maps to the column RTEMPLATE_TITLE in the iprodreports_view_report_instance_details table.
	 */
	protected String rtemplateTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SUB_TITLE in the iprodreports_view_report_instance_details table.
	 */
	protected String rtemplateSubTitle;

	/** 
	 * This attribute maps to the column RTEMPLATE_SQLQUERY in the iprodreports_view_report_instance_details table.
	 */
	protected String rtemplateSqlquery;

	/** 
	 * This attribute maps to the column RTEMPLATE_JRXML in the iprodreports_view_report_instance_details table.
	 */
	protected String rtemplateJrxml;

	/** 
	 * This attribute maps to the column RTEMPLATE_CDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String rtemplateCdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_MDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String rtemplateMdatetime;

	/** 
	 * This attribute maps to the column RTEMPLATE_IS_DELETED in the iprodreports_view_report_instance_details table.
	 */
	protected short rtemplateIsDeleted;

	/** 
	 * This attribute represents whether the primitive attribute rtemplateIsDeleted is null.
	 */
	protected boolean rtemplateIsDeletedNull = true;

	/** 
	 * This attribute maps to the column DATASOURCE_INSTANCE_ID in the iprodreports_view_report_instance_details table.
	 */
	protected int datasourceInstanceId;

	/** 
	 * This attribute maps to the column DATASOURCE_INSTANCE_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String datasourceInstanceName;

	/** 
	 * This attribute maps to the column DATASOURCE_TYPE_ID in the iprodreports_view_report_instance_details table.
	 */
	protected int datasourceTypeId;

	/** 
	 * This attribute maps to the column DATASOURCE_TYPE_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String datasourceTypeName;

	/** 
	 * This attribute maps to the column DATABASE_ID in the iprodreports_view_report_instance_details table.
	 */
	protected int databaseId;

	/** 
	 * This attribute maps to the column DATABASE_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseName;

	/** 
	 * This attribute maps to the column DATABASE_DEFAULT_PORT in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseDefaultPort;

	/** 
	 * This attribute maps to the column DATABASE_IS_DELETED in the iprodreports_view_report_instance_details table.
	 */
	protected short databaseIsDeleted;

	/** 
	 * This attribute maps to the column DATABASE_DESC in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseDesc;

	/** 
	 * This attribute maps to the column DATABASE_CDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseCdatetime;

	/** 
	 * This attribute maps to the column DATABASE_MDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseMdatetime;

	/** 
	 * This attribute maps to the column JDBC_DRIVER_ID in the iprodreports_view_report_instance_details table.
	 */
	protected int jdbcDriverId;

	/** 
	 * This attribute represents whether the primitive attribute jdbcDriverId is null.
	 */
	protected boolean jdbcDriverIdNull = true;

	/** 
	 * This attribute maps to the column JDBC_DRIVER_URL in the iprodreports_view_report_instance_details table.
	 */
	protected String jdbcDriverUrl;

	/** 
	 * This attribute maps to the column JDBC_DRIVER_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String jdbcDriverName;

	/** 
	 * This attribute maps to the column JDBC_DRIVER_CLASSNAME in the iprodreports_view_report_instance_details table.
	 */
	protected String jdbcDriverClassname;

	/** 
	 * This attribute maps to the column JDBC_DRIVER_DESC in the iprodreports_view_report_instance_details table.
	 */
	protected String jdbcDriverDesc;

	/** 
	 * This attribute maps to the column JDBC_DRIVER_CDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String jdbcDriverCdatetime;

	/** 
	 * This attribute maps to the column JDBC_DRIVER_MDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String jdbcDriverMdatetime;

	/** 
	 * This attribute maps to the column DATABASE_INSTANCE_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseInstanceName;

	/** 
	 * This attribute maps to the column DATABASE_SERVER_USERNAME in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseServerUsername;

	/** 
	 * This attribute maps to the column DATABASE_SERVER_PASSWORD in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseServerPassword;

	/** 
	 * This attribute maps to the column DATABASE_SERVER_NAME in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseServerName;

	/** 
	 * This attribute maps to the column DATABASE_SERVER_PORT in the iprodreports_view_report_instance_details table.
	 */
	protected String databaseServerPort;

	/** 
	 * This attribute maps to the column DATASOURCE_INSTANCE_CDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String datasourceInstanceCdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_INSTANCE_MDATETIME in the iprodreports_view_report_instance_details table.
	 */
	protected String datasourceInstanceMdatetime;

	/** 
	 * This attribute maps to the column IS_VALIDATED in the iprodreports_view_report_instance_details table.
	 */
	protected short isValidated;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprodreports_view_report_instance_details table.
	 */
	protected short isDeleted;
		
	
	/* Public List Here*/
	public List<IprodReportsViewReportsInstanceDetails> allReportInstanceListG = new ArrayList<IprodReportsViewReportsInstanceDetails>();
	
    public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();

	
	public String findAllReportInstanceList(){
		log.info("IprodReportsViewReportInstanceDetailsAction : findAllReportInstanceList()");
		Map<String, Object> session = ActionContext.getContext().getSession();
	    if(session.get(Consts.IPRODUSERID)!= null){
	    	IprodReportsViewReportsInstanceDetailsDao viewReportInstanceDetailsDao = DaoFactory.createIprodReportsViewReportsInstanceDetailsDao();
				List<IprodReportsViewReportsInstanceDetails> allReportInstanceListL = new ArrayList<IprodReportsViewReportsInstanceDetails>();
				try{
					allReportInstanceListL= viewReportInstanceDetailsDao.findWhereIsDeletedEquals((short)0);
					if(allReportInstanceListL.size()>0){
						log.info("=========allReportInstanceListL==========="+allReportInstanceListL.size());
						allReportInstanceListG = allReportInstanceListL;
						//log.info("Database JDBC List is : "+allReportInstanceListL);
					}
					else{
						addActionError("Currently there is no report to be displayed");
					}
					
					return SUCCESS;
				}catch(IprodReportsViewReportsInstanceDetailsDaoException e){
					log.error(e.getMessage());
					return ERROR;
				}
	    	}else{
			    	addActionError("Sorry!! You have to Login first");
			    	return "login";
			    }
		
	}
	
		
	/**
	 * Method 'getReportId'
	 * 
	 * @return int
	 */
	public int getReportId()
	{
		return reportId;
	}

	/**
	 * Method 'setReportId'
	 * 
	 * @param reportId
	 */
	public void setReportId(int reportId)
	{
		this.reportId = reportId;
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Method 'getReportQuery'
	 * 
	 * @return String
	 */
	public String getReportQuery()
	{
		return reportQuery;
	}

	/**
	 * Method 'setReportQuery'
	 * 
	 * @param reportQuery
	 */
	public void setReportQuery(String reportQuery)
	{
		this.reportQuery = reportQuery;
	}

	/**
	 * Method 'getReportPdfLocation'
	 * 
	 * @return String
	 */
	public String getReportPdfLocation()
	{
		return reportPdfLocation;
	}

	/**
	 * Method 'setReportPdfLocation'
	 * 
	 * @param reportPdfLocation
	 */
	public void setReportPdfLocation(String reportPdfLocation)
	{
		this.reportPdfLocation = reportPdfLocation;
	}

	/**
	 * Method 'getReportCdatetime'
	 * 
	 * @return String
	 */
	public String getReportCdatetime()
	{
		return reportCdatetime;
	}

	/**
	 * Method 'setReportCdatetime'
	 * 
	 * @param reportCdatetime
	 */
	public void setReportCdatetime(String reportCdatetime)
	{
		this.reportCdatetime = reportCdatetime;
	}

	/**
	 * Method 'getReportMdatetime'
	 * 
	 * @return String
	 */
	public String getReportMdatetime()
	{
		return reportMdatetime;
	}

	/**
	 * Method 'setReportMdatetime'
	 * 
	 * @param reportMdatetime
	 */
	public void setReportMdatetime(String reportMdatetime)
	{
		this.reportMdatetime = reportMdatetime;
	}

	/**
	 * Method 'getReportIsScheduled'
	 * 
	 * @return short
	 */
	public short getReportIsScheduled()
	{
		return reportIsScheduled;
	}

	/**
	 * Method 'setReportIsScheduled'
	 * 
	 * @param reportIsScheduled
	 */
	public void setReportIsScheduled(short reportIsScheduled)
	{
		this.reportIsScheduled = reportIsScheduled;
		this.reportIsScheduledNull = false;
	}

	/**
	 * Method 'setReportIsScheduledNull'
	 * 
	 * @param value
	 */
	public void setReportIsScheduledNull(boolean value)
	{
		this.reportIsScheduledNull = value;
	}

	/**
	 * Method 'isReportIsScheduledNull'
	 * 
	 * @return boolean
	 */
	public boolean isReportIsScheduledNull()
	{
		return reportIsScheduledNull;
	}

	/**
	 * Method 'getReportIsDeleted'
	 * 
	 * @return short
	 */
	public short getReportIsDeleted()
	{
		return reportIsDeleted;
	}

	/**
	 * Method 'setReportIsDeleted'
	 * 
	 * @param reportIsDeleted
	 */
	public void setReportIsDeleted(short reportIsDeleted)
	{
		this.reportIsDeleted = reportIsDeleted;
		this.reportIsDeletedNull = false;
	}

	/**
	 * Method 'setReportIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setReportIsDeletedNull(boolean value)
	{
		this.reportIsDeletedNull = value;
	}

	/**
	 * Method 'isReportIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isReportIsDeletedNull()
	{
		return reportIsDeletedNull;
	}

	/**
	 * Method 'getRtemplateId'
	 * 
	 * @return int
	 */
	public int getRtemplateId()
	{
		return rtemplateId;
	}

	/**
	 * Method 'setRtemplateId'
	 * 
	 * @param rtemplateId
	 */
	public void setRtemplateId(int rtemplateId)
	{
		this.rtemplateId = rtemplateId;
	}

	/**
	 * Method 'getRtemplateName'
	 * 
	 * @return String
	 */
	public String getRtemplateName()
	{
		return rtemplateName;
	}

	/**
	 * Method 'setRtemplateName'
	 * 
	 * @param rtemplateName
	 */
	public void setRtemplateName(String rtemplateName)
	{
		this.rtemplateName = rtemplateName;
	}

	/**
	 * Method 'getRtemplateTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateTitle()
	{
		return rtemplateTitle;
	}

	/**
	 * Method 'setRtemplateTitle'
	 * 
	 * @param rtemplateTitle
	 */
	public void setRtemplateTitle(String rtemplateTitle)
	{
		this.rtemplateTitle = rtemplateTitle;
	}

	/**
	 * Method 'getRtemplateSubTitle'
	 * 
	 * @return String
	 */
	public String getRtemplateSubTitle()
	{
		return rtemplateSubTitle;
	}

	/**
	 * Method 'setRtemplateSubTitle'
	 * 
	 * @param rtemplateSubTitle
	 */
	public void setRtemplateSubTitle(String rtemplateSubTitle)
	{
		this.rtemplateSubTitle = rtemplateSubTitle;
	}

	/**
	 * Method 'getRtemplateSqlquery'
	 * 
	 * @return String
	 */
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

	/**
	 * Method 'setRtemplateSqlquery'
	 * 
	 * @param rtemplateSqlquery
	 */
	public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	/**
	 * Method 'getRtemplateJrxml'
	 * 
	 * @return String
	 */
	public String getRtemplateJrxml()
	{
		return rtemplateJrxml;
	}

	/**
	 * Method 'setRtemplateJrxml'
	 * 
	 * @param rtemplateJrxml
	 */
	public void setRtemplateJrxml(String rtemplateJrxml)
	{
		this.rtemplateJrxml = rtemplateJrxml;
	}

	/**
	 * Method 'getRtemplateCdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateCdatetime()
	{
		return rtemplateCdatetime;
	}

	/**
	 * Method 'setRtemplateCdatetime'
	 * 
	 * @param rtemplateCdatetime
	 */
	public void setRtemplateCdatetime(String rtemplateCdatetime)
	{
		this.rtemplateCdatetime = rtemplateCdatetime;
	}

	/**
	 * Method 'getRtemplateMdatetime'
	 * 
	 * @return String
	 */
	public String getRtemplateMdatetime()
	{
		return rtemplateMdatetime;
	}

	/**
	 * Method 'setRtemplateMdatetime'
	 * 
	 * @param rtemplateMdatetime
	 */
	public void setRtemplateMdatetime(String rtemplateMdatetime)
	{
		this.rtemplateMdatetime = rtemplateMdatetime;
	}

	/**
	 * Method 'getRtemplateIsDeleted'
	 * 
	 * @return short
	 */
	public short getRtemplateIsDeleted()
	{
		return rtemplateIsDeleted;
	}

	/**
	 * Method 'setRtemplateIsDeleted'
	 * 
	 * @param rtemplateIsDeleted
	 */
	public void setRtemplateIsDeleted(short rtemplateIsDeleted)
	{
		this.rtemplateIsDeleted = rtemplateIsDeleted;
		this.rtemplateIsDeletedNull = false;
	}

	/**
	 * Method 'setRtemplateIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setRtemplateIsDeletedNull(boolean value)
	{
		this.rtemplateIsDeletedNull = value;
	}

	/**
	 * Method 'isRtemplateIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isRtemplateIsDeletedNull()
	{
		return rtemplateIsDeletedNull;
	}

	/**
	 * Method 'getDatasourceInstanceId'
	 * 
	 * @return int
	 */
	public int getDatasourceInstanceId()
	{
		return datasourceInstanceId;
	}

	/**
	 * Method 'setDatasourceInstanceId'
	 * 
	 * @param datasourceInstanceId
	 */
	public void setDatasourceInstanceId(int datasourceInstanceId)
	{
		this.datasourceInstanceId = datasourceInstanceId;
	}

	/**
	 * Method 'getDatasourceInstanceName'
	 * 
	 * @return String
	 */
	public String getDatasourceInstanceName()
	{
		return datasourceInstanceName;
	}

	/**
	 * Method 'setDatasourceInstanceName'
	 * 
	 * @param datasourceInstanceName
	 */
	public void setDatasourceInstanceName(String datasourceInstanceName)
	{
		this.datasourceInstanceName = datasourceInstanceName;
	}

	/**
	 * Method 'getDatasourceTypeId'
	 * 
	 * @return int
	 */
	public int getDatasourceTypeId()
	{
		return datasourceTypeId;
	}

	/**
	 * Method 'setDatasourceTypeId'
	 * 
	 * @param datasourceTypeId
	 */
	public void setDatasourceTypeId(int datasourceTypeId)
	{
		this.datasourceTypeId = datasourceTypeId;
	}

	/**
	 * Method 'getDatasourceTypeName'
	 * 
	 * @return String
	 */
	public String getDatasourceTypeName()
	{
		return datasourceTypeName;
	}

	/**
	 * Method 'setDatasourceTypeName'
	 * 
	 * @param datasourceTypeName
	 */
	public void setDatasourceTypeName(String datasourceTypeName)
	{
		this.datasourceTypeName = datasourceTypeName;
	}

	/**
	 * Method 'getDatabaseId'
	 * 
	 * @return int
	 */
	public int getDatabaseId()
	{
		return databaseId;
	}

	/**
	 * Method 'setDatabaseId'
	 * 
	 * @param databaseId
	 */
	public void setDatabaseId(int databaseId)
	{
		this.databaseId = databaseId;
	}

	/**
	 * Method 'getDatabaseName'
	 * 
	 * @return String
	 */
	public String getDatabaseName()
	{
		return databaseName;
	}

	/**
	 * Method 'setDatabaseName'
	 * 
	 * @param databaseName
	 */
	public void setDatabaseName(String databaseName)
	{
		this.databaseName = databaseName;
	}

	/**
	 * Method 'getDatabaseDefaultPort'
	 * 
	 * @return String
	 */
	public String getDatabaseDefaultPort()
	{
		return databaseDefaultPort;
	}

	/**
	 * Method 'setDatabaseDefaultPort'
	 * 
	 * @param databaseDefaultPort
	 */
	public void setDatabaseDefaultPort(String databaseDefaultPort)
	{
		this.databaseDefaultPort = databaseDefaultPort;
	}

	/**
	 * Method 'getDatabaseIsDeleted'
	 * 
	 * @return short
	 */
	public short getDatabaseIsDeleted()
	{
		return databaseIsDeleted;
	}

	/**
	 * Method 'setDatabaseIsDeleted'
	 * 
	 * @param databaseIsDeleted
	 */
	public void setDatabaseIsDeleted(short databaseIsDeleted)
	{
		this.databaseIsDeleted = databaseIsDeleted;
	}

	/**
	 * Method 'getDatabaseDesc'
	 * 
	 * @return String
	 */
	public String getDatabaseDesc()
	{
		return databaseDesc;
	}

	/**
	 * Method 'setDatabaseDesc'
	 * 
	 * @param databaseDesc
	 */
	public void setDatabaseDesc(String databaseDesc)
	{
		this.databaseDesc = databaseDesc;
	}

	/**
	 * Method 'getDatabaseCdatetime'
	 * 
	 * @return String
	 */
	public String getDatabaseCdatetime()
	{
		return databaseCdatetime;
	}

	/**
	 * Method 'setDatabaseCdatetime'
	 * 
	 * @param databaseCdatetime
	 */
	public void setDatabaseCdatetime(String databaseCdatetime)
	{
		this.databaseCdatetime = databaseCdatetime;
	}

	/**
	 * Method 'getDatabaseMdatetime'
	 * 
	 * @return String
	 */
	public String getDatabaseMdatetime()
	{
		return databaseMdatetime;
	}

	/**
	 * Method 'setDatabaseMdatetime'
	 * 
	 * @param databaseMdatetime
	 */
	public void setDatabaseMdatetime(String databaseMdatetime)
	{
		this.databaseMdatetime = databaseMdatetime;
	}

	/**
	 * Method 'getJdbcDriverId'
	 * 
	 * @return int
	 */
	public int getJdbcDriverId()
	{
		return jdbcDriverId;
	}

	/**
	 * Method 'setJdbcDriverId'
	 * 
	 * @param jdbcDriverId
	 */
	public void setJdbcDriverId(int jdbcDriverId)
	{
		this.jdbcDriverId = jdbcDriverId;
		this.jdbcDriverIdNull = false;
	}

	/**
	 * Method 'setJdbcDriverIdNull'
	 * 
	 * @param value
	 */
	public void setJdbcDriverIdNull(boolean value)
	{
		this.jdbcDriverIdNull = value;
	}

	/**
	 * Method 'isJdbcDriverIdNull'
	 * 
	 * @return boolean
	 */
	public boolean isJdbcDriverIdNull()
	{
		return jdbcDriverIdNull;
	}

	/**
	 * Method 'getJdbcDriverUrl'
	 * 
	 * @return String
	 */
	public String getJdbcDriverUrl()
	{
		return jdbcDriverUrl;
	}

	/**
	 * Method 'setJdbcDriverUrl'
	 * 
	 * @param jdbcDriverUrl
	 */
	public void setJdbcDriverUrl(String jdbcDriverUrl)
	{
		this.jdbcDriverUrl = jdbcDriverUrl;
	}

	/**
	 * Method 'getJdbcDriverName'
	 * 
	 * @return String
	 */
	public String getJdbcDriverName()
	{
		return jdbcDriverName;
	}

	/**
	 * Method 'setJdbcDriverName'
	 * 
	 * @param jdbcDriverName
	 */
	public void setJdbcDriverName(String jdbcDriverName)
	{
		this.jdbcDriverName = jdbcDriverName;
	}

	/**
	 * Method 'getJdbcDriverClassname'
	 * 
	 * @return String
	 */
	public String getJdbcDriverClassname()
	{
		return jdbcDriverClassname;
	}

	/**
	 * Method 'setJdbcDriverClassname'
	 * 
	 * @param jdbcDriverClassname
	 */
	public void setJdbcDriverClassname(String jdbcDriverClassname)
	{
		this.jdbcDriverClassname = jdbcDriverClassname;
	}

	/**
	 * Method 'getJdbcDriverDesc'
	 * 
	 * @return String
	 */
	public String getJdbcDriverDesc()
	{
		return jdbcDriverDesc;
	}

	/**
	 * Method 'setJdbcDriverDesc'
	 * 
	 * @param jdbcDriverDesc
	 */
	public void setJdbcDriverDesc(String jdbcDriverDesc)
	{
		this.jdbcDriverDesc = jdbcDriverDesc;
	}

	/**
	 * Method 'getJdbcDriverCdatetime'
	 * 
	 * @return String
	 */
	public String getJdbcDriverCdatetime()
	{
		return jdbcDriverCdatetime;
	}

	/**
	 * Method 'setJdbcDriverCdatetime'
	 * 
	 * @param jdbcDriverCdatetime
	 */
	public void setJdbcDriverCdatetime(String jdbcDriverCdatetime)
	{
		this.jdbcDriverCdatetime = jdbcDriverCdatetime;
	}

	/**
	 * Method 'getJdbcDriverMdatetime'
	 * 
	 * @return String
	 */
	public String getJdbcDriverMdatetime()
	{
		return jdbcDriverMdatetime;
	}

	/**
	 * Method 'setJdbcDriverMdatetime'
	 * 
	 * @param jdbcDriverMdatetime
	 */
	public void setJdbcDriverMdatetime(String jdbcDriverMdatetime)
	{
		this.jdbcDriverMdatetime = jdbcDriverMdatetime;
	}

	/**
	 * Method 'getDatabaseInstanceName'
	 * 
	 * @return String
	 */
	public String getDatabaseInstanceName()
	{
		return databaseInstanceName;
	}

	/**
	 * Method 'setDatabaseInstanceName'
	 * 
	 * @param databaseInstanceName
	 */
	public void setDatabaseInstanceName(String databaseInstanceName)
	{
		this.databaseInstanceName = databaseInstanceName;
	}

	/**
	 * Method 'getDatabaseServerUsername'
	 * 
	 * @return String
	 */
	public String getDatabaseServerUsername()
	{
		return databaseServerUsername;
	}

	/**
	 * Method 'setDatabaseServerUsername'
	 * 
	 * @param databaseServerUsername
	 */
	public void setDatabaseServerUsername(String databaseServerUsername)
	{
		this.databaseServerUsername = databaseServerUsername;
	}

	/**
	 * Method 'getDatabaseServerPassword'
	 * 
	 * @return String
	 */
	public String getDatabaseServerPassword()
	{
		return databaseServerPassword;
	}

	/**
	 * Method 'setDatabaseServerPassword'
	 * 
	 * @param databaseServerPassword
	 */
	public void setDatabaseServerPassword(String databaseServerPassword)
	{
		this.databaseServerPassword = databaseServerPassword;
	}

	/**
	 * Method 'getDatabaseServerName'
	 * 
	 * @return String
	 */
	public String getDatabaseServerName()
	{
		return databaseServerName;
	}

	/**
	 * Method 'setDatabaseServerName'
	 * 
	 * @param databaseServerName
	 */
	public void setDatabaseServerName(String databaseServerName)
	{
		this.databaseServerName = databaseServerName;
	}

	/**
	 * Method 'getDatabaseServerPort'
	 * 
	 * @return String
	 */
	public String getDatabaseServerPort()
	{
		return databaseServerPort;
	}

	/**
	 * Method 'setDatabaseServerPort'
	 * 
	 * @param databaseServerPort
	 */
	public void setDatabaseServerPort(String databaseServerPort)
	{
		this.databaseServerPort = databaseServerPort;
	}

	/**
	 * Method 'getDatasourceInstanceCdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceInstanceCdatetime()
	{
		return datasourceInstanceCdatetime;
	}

	/**
	 * Method 'setDatasourceInstanceCdatetime'
	 * 
	 * @param datasourceInstanceCdatetime
	 */
	public void setDatasourceInstanceCdatetime(String datasourceInstanceCdatetime)
	{
		this.datasourceInstanceCdatetime = datasourceInstanceCdatetime;
	}

	/**
	 * Method 'getDatasourceInstanceMdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceInstanceMdatetime()
	{
		return datasourceInstanceMdatetime;
	}

	/**
	 * Method 'setDatasourceInstanceMdatetime'
	 * 
	 * @param datasourceInstanceMdatetime
	 */
	public void setDatasourceInstanceMdatetime(String datasourceInstanceMdatetime)
	{
		this.datasourceInstanceMdatetime = datasourceInstanceMdatetime;
	}

	/**
	 * Method 'getIsValidated'
	 * 
	 * @return short
	 */
	public short getIsValidated()
	{
		return isValidated;
	}

	/**
	 * Method 'setIsValidated'
	 * 
	 * @param isValidated
	 */
	public void setIsValidated(short isValidated)
	{
		this.isValidated = isValidated;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
	}
	
	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodReportsViewReportInstanceDetails: " );
		ret.append( "reportId=" + reportId );
		ret.append( ", userId=" + userId );
		ret.append( ", userName=" + userName );
		ret.append( ", reportQuery=" + reportQuery );
		ret.append( ", reportExcelLocation=" + reportExcelLocation );
		ret.append( ", reportPdfLocation=" + reportPdfLocation );
		ret.append( ", reportCdatetime=" + reportCdatetime );
		ret.append( ", reportMdatetime=" + reportMdatetime );
		ret.append( ", reportIsScheduled=" + reportIsScheduled );
		ret.append( ", reportIsDeleted=" + reportIsDeleted );
		ret.append( ", rtemplateId=" + rtemplateId );
		ret.append( ", rtemplateName=" + rtemplateName );
		ret.append( ", rtemplateTitle=" + rtemplateTitle );
		ret.append( ", rtemplateSubTitle=" + rtemplateSubTitle );
		ret.append( ", rtemplateSqlquery=" + rtemplateSqlquery );
		ret.append( ", rtemplateJrxml=" + rtemplateJrxml );
		ret.append( ", rtemplateCdatetime=" + rtemplateCdatetime );
		ret.append( ", rtemplateMdatetime=" + rtemplateMdatetime );
		ret.append( ", rtemplateIsDeleted=" + rtemplateIsDeleted );
		ret.append( ", datasourceInstanceId=" + datasourceInstanceId );
		ret.append( ", datasourceInstanceName=" + datasourceInstanceName );
		ret.append( ", datasourceTypeId=" + datasourceTypeId );
		ret.append( ", datasourceTypeName=" + datasourceTypeName );
		ret.append( ", databaseId=" + databaseId );
		ret.append( ", databaseName=" + databaseName );
		ret.append( ", databaseDefaultPort=" + databaseDefaultPort );
		ret.append( ", databaseIsDeleted=" + databaseIsDeleted );
		ret.append( ", databaseDesc=" + databaseDesc );
		ret.append( ", databaseCdatetime=" + databaseCdatetime );
		ret.append( ", databaseMdatetime=" + databaseMdatetime );
		ret.append( ", jdbcDriverId=" + jdbcDriverId );
		ret.append( ", jdbcDriverUrl=" + jdbcDriverUrl );
		ret.append( ", jdbcDriverName=" + jdbcDriverName );
		ret.append( ", jdbcDriverClassname=" + jdbcDriverClassname );
		ret.append( ", jdbcDriverDesc=" + jdbcDriverDesc );
		ret.append( ", jdbcDriverCdatetime=" + jdbcDriverCdatetime );
		ret.append( ", jdbcDriverMdatetime=" + jdbcDriverMdatetime );
		ret.append( ", databaseInstanceName=" + databaseInstanceName );
		ret.append( ", databaseServerUsername=" + databaseServerUsername );
		ret.append( ", databaseServerPassword=" + databaseServerPassword );
		ret.append( ", databaseServerName=" + databaseServerName );
		ret.append( ", databaseServerPort=" + databaseServerPort );
		ret.append( ", datasourceInstanceCdatetime=" + datasourceInstanceCdatetime );
		ret.append( ", datasourceInstanceMdatetime=" + datasourceInstanceMdatetime );
		ret.append( ", isValidated=" + isValidated );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

	 public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
			return hashMapCheckRoleModuleOperationListG;
		}

		public void setHashMapCheckRoleModuleOperationListG(
				HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
			this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
		}


		public String getReportExcelLocation() {
			return reportExcelLocation;
		}


		public void setReportExcelLocation(String reportExcelLocation) {
			this.reportExcelLocation = reportExcelLocation;
		}


}
