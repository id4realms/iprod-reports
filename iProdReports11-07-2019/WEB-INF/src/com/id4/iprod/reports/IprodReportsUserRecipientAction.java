package com.id4.iprod.reports;

import java.io.Serializable;

public class IprodReportsUserRecipientAction implements Serializable
{
	/** 
	 * This attribute maps to the column USER_ID in the IprodReports_user_recipient table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column RECIPIENT_ID in the IprodReports_user_recipient table.
	 */
	protected int recipientId;

	/**
	 * Method 'IprodReportsUserRecipient'
	 * 
	 */
	public IprodReportsUserRecipientAction()
	{
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getRecipientId'
	 * 
	 * @return int
	 */
	public int getRecipientId()
	{
		return recipientId;
	}

	/**
	 * Method 'setRecipientId'
	 * 
	 * @param recipientId
	 */
	public void setRecipientId(int recipientId)
	{
		this.recipientId = recipientId;
	}
}
