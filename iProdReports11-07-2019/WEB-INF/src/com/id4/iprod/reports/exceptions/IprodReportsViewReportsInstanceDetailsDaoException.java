package com.id4.iprod.reports.exceptions;

public class IprodReportsViewReportsInstanceDetailsDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsViewReportsInstanceDetailsDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsViewReportsInstanceDetailsDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsViewReportsInstanceDetailsDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsViewReportsInstanceDetailsDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
