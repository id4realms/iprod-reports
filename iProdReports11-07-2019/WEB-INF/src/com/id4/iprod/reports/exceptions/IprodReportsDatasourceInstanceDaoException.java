package com.id4.iprod.reports.exceptions;

public class IprodReportsDatasourceInstanceDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsDatasourceInstanceDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsDatasourceInstanceDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsDatasourceInstanceDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsDatasourceInstanceDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
