package com.id4.iprod.reports.exceptions;

public class IprodReportsInstanceDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsInstanceDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsInstanceDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsInstanceDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsInstanceDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
