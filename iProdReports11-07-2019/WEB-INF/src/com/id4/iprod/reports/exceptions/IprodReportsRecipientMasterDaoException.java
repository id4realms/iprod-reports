package com.id4.iprod.reports.exceptions;

public class IprodReportsRecipientMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsRecipientMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsRecipientMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsRecipientMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsRecipientMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
