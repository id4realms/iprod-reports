package com.id4.iprod.reports.exceptions;

public class IprodReportsJobDataDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsJobDataDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsJobDataDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsJobDataDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsJobDataDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
