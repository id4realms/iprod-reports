package com.id4.iprod.reports.exceptions;

public class IprodReportsTemplateMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsTemplateMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsTemplateMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsTemplateMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsTemplateMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
