package com.id4.iprod.reports.exceptions;

public class IprodReportsRdbmsMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsRdbmsMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsRdbmsMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsRdbmsMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsRdbmsMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
