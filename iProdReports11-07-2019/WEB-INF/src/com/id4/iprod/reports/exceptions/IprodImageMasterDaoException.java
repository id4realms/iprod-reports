package com.id4.iprod.reports.exceptions;

public class IprodImageMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodImageMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodImageMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodImageMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodImageMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
