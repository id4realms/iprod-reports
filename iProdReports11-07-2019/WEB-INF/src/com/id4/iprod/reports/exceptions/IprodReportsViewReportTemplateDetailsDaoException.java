package com.id4.iprod.reports.exceptions;

public class IprodReportsViewReportTemplateDetailsDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsViewReportTemplateDetailsDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsViewReportTemplateDetailsDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsViewReportTemplateDetailsDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsViewReportTemplateDetailsDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
