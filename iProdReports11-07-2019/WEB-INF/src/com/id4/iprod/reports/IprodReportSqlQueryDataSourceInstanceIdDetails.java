package com.id4.iprod.reports;

public class IprodReportSqlQueryDataSourceInstanceIdDetails {

	
	protected String rtemplateSqlquery;
    protected Integer datasourceInstanceId;
	protected String dataFieldName;
    protected Integer rtemplateId;
	public String getRtemplateSqlquery()
	{
		return rtemplateSqlquery;
	}

		public void setRtemplateSqlquery(String rtemplateSqlquery)
	{
		this.rtemplateSqlquery = rtemplateSqlquery;
	}

	public Integer getDatasourceInstanceId() {
		return datasourceInstanceId;
	}

	public void setDatasourceInstanceId(Integer datasourceInstanceId) {
		this.datasourceInstanceId = datasourceInstanceId;
	}

	public String getDataFieldName() {
		return dataFieldName;
	}

	public void setDataFieldName(String dataFieldName) {
		this.dataFieldName = dataFieldName;
	}

	public Integer getRtemplateId() {
		return rtemplateId;
	}

	public void setRtemplateId(Integer rtemplateId) {
		this.rtemplateId = rtemplateId;
	}

	
	

	
	
}
