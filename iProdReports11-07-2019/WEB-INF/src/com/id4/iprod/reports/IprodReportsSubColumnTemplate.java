package com.id4.iprod.reports;


import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("unused")
public class IprodReportsSubColumnTemplate extends ActionSupport{
	
protected String subColumnLabel; 

protected String subColumnDataField;

public String subColumnDataFieldType;

protected String subColumnPageSum;

protected String subColumnGrandSum;

public String getSubColumnLabel() {
	return subColumnLabel;
}
public void setSubColumnLabel(String subColumnLabel) {
	this.subColumnLabel = subColumnLabel;
}
public String getSubColumnDataField() {
	return subColumnDataField;
}
public void setSubColumnDataField(String subColumnDataField) {
	this.subColumnDataField = subColumnDataField;
}
public String getSubColumnDataFieldType() {
	return subColumnDataFieldType;
}
public void setSubColumnDataFieldType(String subColumnDataFieldType) {
	this.subColumnDataFieldType = subColumnDataFieldType;
}
public String getSubColumnPageSum() {
	return subColumnPageSum;
}
public void setSubColumnPageSum(String subColumnPageSum) {
	this.subColumnPageSum = subColumnPageSum;
}
public String getSubColumnGrandSum() {
	return subColumnGrandSum;
}
public void setSubColumnGrandSum(String subColumnGrandSum) {
	this.subColumnGrandSum = subColumnGrandSum;
}

}