package com.id4.iprod.reports;

import java.io.Serializable;

public class IprodReportsViewUserRecipientAction implements Serializable
{
	/** 
	 * This attribute maps to the column USER_ID in the IprodReports_view_user_recipient table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column USER_NAME in the IprodReports_view_user_recipient table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column FIRST_NAME in the IprodReports_view_user_recipient table.
	 */
	protected String firstName;

	/** 
	 * This attribute maps to the column LAST_NAME in the IprodReports_view_user_recipient table.
	 */
	protected String lastName;

	/** 
	 * This attribute maps to the column GENDER in the IprodReports_view_user_recipient table.
	 */
	protected String gender;

	/** 
	 * This attribute maps to the column EMAIL_ID in the IprodReports_view_user_recipient table.
	 */
	protected String emailId;

	/** 
	 * This attribute maps to the column CONTACT_NO in the IprodReports_view_user_recipient table.
	 */
	protected String contactNo;

	/** 
	 * This attribute maps to the column USER_PASSWORD in the IprodReports_view_user_recipient table.
	 */
	protected String userPassword;

	/** 
	 * This attribute maps to the column IS_DELETED in the IprodReports_view_user_recipient table.
	 */
	protected short isDeleted;

	/** 
	 * This attribute maps to the column RECIPIENT_ID in the IprodReports_view_user_recipient table.
	 */
	protected int recipientId;

	/** 
	 * This attribute maps to the column R_FIRST_NAME in the IprodReports_view_user_recipient table.
	 */
	protected String rFirstName;

	/** 
	 * This attribute maps to the column R_LAST_NAME in the IprodReports_view_user_recipient table.
	 */
	protected String rLastName;

	/** 
	 * This attribute maps to the column R_EMAIL_ID in the IprodReports_view_user_recipient table.
	 */
	protected String rEmailId;

	/** 
	 * This attribute maps to the column R_CONTACT_NO in the IprodReports_view_user_recipient table.
	 */
	protected String rContactNo;

	/** 
	 * This attribute maps to the column R_IS_DELETED in the IprodReports_view_user_recipient table.
	 */
	protected short rIsDeleted;

	/**
	 * Method 'IprodReportsViewUserRecipient'
	 * 
	 */
	public IprodReportsViewUserRecipientAction()
	{
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getUserName'
	 * 
	 * @return String
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * Method 'setUserName'
	 * 
	 * @param userName
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * Method 'getFirstName'
	 * 
	 * @return String
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Method 'setFirstName'
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Method 'getLastName'
	 * 
	 * @return String
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Method 'setLastName'
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Method 'getGender'
	 * 
	 * @return String
	 */
	public String getGender()
	{
		return gender;
	}

	/**
	 * Method 'setGender'
	 * 
	 * @param gender
	 */
	public void setGender(String gender)
	{
		this.gender = gender;
	}

	/**
	 * Method 'getEmailId'
	 * 
	 * @return String
	 */
	public String getEmailId()
	{
		return emailId;
	}

	/**
	 * Method 'setEmailId'
	 * 
	 * @param emailId
	 */
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	/**
	 * Method 'getContactNo'
	 * 
	 * @return String
	 */
	public String getContactNo()
	{
		return contactNo;
	}

	/**
	 * Method 'setContactNo'
	 * 
	 * @param contactNo
	 */
	public void setContactNo(String contactNo)
	{
		this.contactNo = contactNo;
	}

	/**
	 * Method 'getUserPassword'
	 * 
	 * @return String
	 */
	public String getUserPassword()
	{
		return userPassword;
	}

	/**
	 * Method 'setUserPassword'
	 * 
	 * @param userPassword
	 */
	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Method 'getRecipientId'
	 * 
	 * @return int
	 */
	public int getRecipientId()
	{
		return recipientId;
	}

	/**
	 * Method 'setRecipientId'
	 * 
	 * @param recipientId
	 */
	public void setRecipientId(int recipientId)
	{
		this.recipientId = recipientId;
	}

	/**
	 * Method 'getRFirstName'
	 * 
	 * @return String
	 */
	public String getRFirstName()
	{
		return rFirstName;
	}

	/**
	 * Method 'setRFirstName'
	 * 
	 * @param rFirstName
	 */
	public void setRFirstName(String rFirstName)
	{
		this.rFirstName = rFirstName;
	}

	/**
	 * Method 'getRLastName'
	 * 
	 * @return String
	 */
	public String getRLastName()
	{
		return rLastName;
	}

	/**
	 * Method 'setRLastName'
	 * 
	 * @param rLastName
	 */
	public void setRLastName(String rLastName)
	{
		this.rLastName = rLastName;
	}

	/**
	 * Method 'getREmailId'
	 * 
	 * @return String
	 */
	public String getREmailId()
	{
		return rEmailId;
	}

	/**
	 * Method 'setREmailId'
	 * 
	 * @param rEmailId
	 */
	public void setREmailId(String rEmailId)
	{
		this.rEmailId = rEmailId;
	}

	/**
	 * Method 'getRContactNo'
	 * 
	 * @return String
	 */
	public String getRContactNo()
	{
		return rContactNo;
	}

	/**
	 * Method 'setRContactNo'
	 * 
	 * @param rContactNo
	 */
	public void setRContactNo(String rContactNo)
	{
		this.rContactNo = rContactNo;
	}

	/**
	 * Method 'getRIsDeleted'
	 * 
	 * @return short
	 */
	public short getRIsDeleted()
	{
		return rIsDeleted;
	}

	/**
	 * Method 'setRIsDeleted'
	 * 
	 * @param rIsDeleted
	 */
	public void setRIsDeleted(short rIsDeleted)
	{
		this.rIsDeleted = rIsDeleted;
	}
}
