package com.id4.iprod.reports;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.Session;

import org.apache.log4j.Logger;
import org.eclipse.birt.chart.model.data.Trigger;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.reports.dao.IprodReportsInstanceDao;
import com.id4.iprod.reports.dto.IprodReportsInstance;
import com.id4.iprod.reports.dto.IprodReportsInstancePk;
import com.id4.iprod.reports.exceptions.IprodReportsInstanceDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodReportsSchedularAction  extends ActionSupport{
	
	static Logger log = Logger.getLogger(IprodReportsSchedularAction.class);


	protected String databaseInstanceName;
	protected String databaseServerUsername;
	protected String databaseServerPassword;
	protected String jdbcDriverUrl;
	protected int jdbcDriverId;
	protected String jdbcDriverClassName;

	/** 
	 * This attribute maps to the column EMAIL_ID in the iprodreports_recipient_master table.
	 */
	protected String selectedRecipientEmailId;


	/** 
	 * This attribute maps to the column REPORT_ID in the iprodreports_instance table.
	 */
	protected int reportId;

	/** 
	 * This attribute maps to the column RTEMPLATE_ID in the iprodreports_instance table.
	 */
	protected int rtemplateId;

	/** 
	 * This attribute maps to the column USER_ID in the iprodreports_instance table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column USER_NAME in the iprodreports_instance table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column REPORT_QUERY in the iprodreports_instance table.
	 */
	protected String reportQueryFrozen;
	
	protected String reportQueryWithFilters;

	protected String justTheQueryFilter;

	/** 
	 * This attribute maps to the column REPORT_PDF_LOCATION in the iprodreports_instance table.
	 */
	protected String reportPdfLocation;

	/** 
	 * This attribute maps to the column REPORT_CDATETIME in the iprodreports_instance table.
	 */
	protected String reportCdatetime;

	/** 
	 * This attribute maps to the column REPORT_MDATETIME in the iprodreports_instance table.
	 */
	protected String reportMdatetime;

	/** 
	 * This attribute maps to the column IS_SCHEDULED in the iprod_reports_instance table.
	 */
	protected int isScheduled;

	/** 
	 * This attribute represents whether the primitive attribute isScheduled is null.
	 */
	protected boolean isScheduledNull = true;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_reports_instance table.
	 */
	protected int isDeleted;

	/** 
	 * This attribute maps to the column RTEMPLATE_IS_SPLIT in the iprodReports_templates table.
	 */
	protected int rtemplateIsSplit;

	/** 
	 * This attribute represents whether the primitive attribute rtemplateIsSplit is null.
	 */
	protected boolean rtemplateIsSplitNull = true;


	protected String cron = null;

	protected String[] chooseRecipientEmails; 

	protected String[] exportOption;
	
	protected String reportScheduleSelection;
	
	/** 
	 * This attribute maps to the column REPORT_EXCEL_LOCATION in the iprod_reports_instance table.
	 */
	protected String reportExcelLocation;
	
	protected String dateTimeFilter;
	



	public String scheduleReport(){
		log.info("IprodReportsSchedularAction::scheduleReport" +dateTimeFilter);
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat ("HH:mm:ss");
		String currentTime = ft.format(dNow);
		IprodReportsInstancePk instancePk = new IprodReportsInstancePk();
		IprodReportsInstanceDao instanceDao = DaoFactory.createIprodReportsInstanceDao();
		IprodReportsInstance instanceDto= new IprodReportsInstance();
		Map<String, Object> session = ActionContext.getContext().getSession();
		this.userId=(Integer)session.get("IprodUserId");
		instanceDto.setRtemplateId(this.rtemplateId);
		instanceDto.setUserId(this.userId);
		instanceDto.setUserName("iprodReports");
		instanceDto.setReportQuery(this.reportQueryWithFilters);
		instanceDto.setReportExcelLocation("");
		instanceDto.setReportPdfLocation("");
		this.reportCdatetime = new Date().toString();
		instanceDto.setReportCdatetime(this.reportCdatetime);
		instanceDto.setReportMdatetime(new Date().toString());
		instanceDto.setIsDeleted((short)0);
		instanceDto.setIsScheduled((short)0);
		instancePk=instanceDao.insert(instanceDto);
		log.info("Report Instance Added Successfully" +instancePk.getReportId());
		
		String reportInstanceTrigger="reportInstanceTrigger"+(instancePk.getReportId()+"-"+currentTime);
		String reportInstanceJob="reportInstanceJob"+(instancePk.getReportId()+"-"+currentTime);
		String reportInstanceGroup = "reportInstanceGroup"+(instancePk.getReportId()+"-"+currentTime);
		try {
			SchedulerFactory schedulerFactory = new StdSchedulerFactory();
			Scheduler scheduler = schedulerFactory.getScheduler();
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put("RtemplateId", this.rtemplateId);
			jobDataMap.put("ReportId", instancePk.getReportId());
			jobDataMap.put("ReportQueryWithFilters", this.reportQueryWithFilters);
			jobDataMap.put("ExportOption", exportOption);
			jobDataMap.put("RecipientEmails", getChooseRecipientEmails());
			jobDataMap.put("DateTimeFilter", getDateTimeFilter());
			
			JobDetail jobdetail = JobBuilder.newJob(IprodGenerateReportAction.class).withIdentity(reportInstanceJob, reportInstanceGroup).usingJobData(jobDataMap).build();
			if(this.getReportScheduleSelection().equals("scheduleParticularTime")){
				log.info("CRON");
				try{
				CronTrigger crontrigger = (CronTrigger) TriggerBuilder.newTrigger().withIdentity(reportInstanceTrigger, reportInstanceGroup).withSchedule(createSchedule(this.cron)).build();
				scheduler.start();
				scheduler.scheduleJob(jobdetail, crontrigger);
				} catch(SchedulerException e) {
					log.error("Exception while scheduling job" +e.getMessage());
				}
				//UPDATE IS_SCHEDULE = 1 IN REPORT INSTANCE

				try {
					instanceDao.updateIsScheduledWhereReportIdEquals(1,instancePk.getReportId());
				} catch (IprodReportsInstanceDaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(this.getReportScheduleSelection().equals("scheduleNow")){
			   log.info("NOW");
				try {
					SimpleTrigger simpleTrigger = (SimpleTrigger) TriggerBuilder.newTrigger().withIdentity(reportInstanceTrigger, reportInstanceGroup).startNow().build();
					scheduler.start();
					scheduler.scheduleJob(jobdetail, simpleTrigger);
				} catch (SchedulerException e) {
					log.error("Exception while scheduling job" +e.getMessage());	
				}
			}
		} catch (SchedulerException e) {
			log.error("Exception while scheduling job" +e.getMessage());			
			e.printStackTrace();
		}
	
		return SUCCESS;
	}

	private static ScheduleBuilder createSchedule(String cronExpression){
		log.info("IprodReportsSchedularAction::createSchedule");
		CronScheduleBuilder builder = CronScheduleBuilder.cronSchedule(cronExpression);
		return builder;
	}
	
	public String stopSchedularForSpecificReport(){
		log.info("IprodReportsSchedularAction::stopSchedularForSpecificReport"+this.reportId);
		//UPDATE IS_SCHEDULE = 0 IN REPORT INSTANCE
		try {
			IprodReportsInstanceDao instanceDao = DaoFactory.createIprodReportsInstanceDao();
			instanceDao.updateIsScheduledWhereReportIdEquals(0,this.reportId);
		} catch (IprodReportsInstanceDaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//DELETE JOB AND UNSCHEDULE ALL OF ITS TRIGGER
		SchedulerFactory schedulerFactory = new StdSchedulerFactory();
		try {
			Scheduler scheduler = schedulerFactory.getScheduler();
			  for (TriggerKey triggerKey : scheduler.getTriggerKeys(GroupMatcher.triggerGroupContains("reportInstanceGroup"+this.reportId)))
			  {
				String jobName = triggerKey.getName();
				String jobGroup = triggerKey.getGroup();
			    scheduler.unscheduleJob(triggerKey);
			  }
			
			
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return SUCCESS;
	}


	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReportPdfLocation() {
		return reportPdfLocation;
	}

	public void setReportPdfLocation(String reportPdfLocation) {
		this.reportPdfLocation = reportPdfLocation;
	}

	public int getRtemplateId() {
		return rtemplateId;
	}

	public void setRtemplateId(int rtemplateId) {
		this.rtemplateId = rtemplateId;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getSelectedRecipientEmailId() {
		return selectedRecipientEmailId;
	}

	public void setSelectedRecipientEmailId(String selectedRecipientEmailId) {
		this.selectedRecipientEmailId = selectedRecipientEmailId;
	}



	public String getReportQueryFrozen() {
		return reportQueryFrozen;
	}

	public void setReportQueryFrozen(String reportQueryFrozen) {
		this.reportQueryFrozen = reportQueryFrozen;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}	

	public String getReportCdatetime() {
		return reportCdatetime;
	}

	public void setReportCdatetime(String reportCdatetime) {
		this.reportCdatetime = reportCdatetime;
	}
	public String getReportMdatetime() {
		return reportMdatetime;
	}

	public void setReportMdatetime(String reportMdatetime) {
		this.reportMdatetime = reportMdatetime;
	}

	public int getIsScheduled() {
		return isScheduled;
	}

	public void setIsScheduled(int isScheduled) {
		this.isScheduled = isScheduled;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String[] getExportOption() {
		return exportOption;
	}

	public void setExportOption(String[] exportOption) {
		this.exportOption = exportOption;
	}

	public String[] getChooseRecipientEmails() {
		return chooseRecipientEmails;
	}

	public void setChooseRecipientEmails(String[] chooseRecipientEmails) {
		this.chooseRecipientEmails = chooseRecipientEmails;
	}

	public String getReportQueryWithFilters() {
		return reportQueryWithFilters;
	}

	public void setReportQueryWithFilters(String reportQueryWithFilters) {
		this.reportQueryWithFilters = reportQueryWithFilters;
	}

	public String getReportScheduleSelection() {
		return reportScheduleSelection;
	}

	public void setReportScheduleSelection(String reportScheduleSelection) {
		this.reportScheduleSelection = reportScheduleSelection;
	}

	public String getReportExcelLocation() {
		return reportExcelLocation;
	}

	public void setReportExcelLocation(String reportExcelLocation) {
		this.reportExcelLocation = reportExcelLocation;
	}



	public String getDateTimeFilter() {
		return dateTimeFilter;
	}

	public void setDateTimeFilter(String dateTimeFilter) {
		this.dateTimeFilter = dateTimeFilter;
	}

	

}
