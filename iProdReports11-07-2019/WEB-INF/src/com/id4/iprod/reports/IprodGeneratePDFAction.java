package com.id4.iprod.reports;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.TableHandle;

import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.reports.dao.IprodReportsInstanceDao;

public class IprodGeneratePDFAction {	
	static Logger log = Logger.getLogger(IprodGeneratePDFAction.class);

	public void generatePDF(IReportEngine engine2, IReportRunnable design2, int reportId, String reportTemplateName) {
		log.info("IprodGeneratePDFAction:: generatePDF :: " + reportTemplateName);
		// this for set run time style to PDF
		try {
			ReportDesignHandle reportDesignHandle = (ReportDesignHandle) design2.getDesignHandle();
			TableHandle table = (TableHandle) reportDesignHandle.findElement("JdbcQueryResultDataTable");
			RowHandle detailRow = (RowHandle) table.getDetail().get(0);
			log.info("IprodGeneratePDFAction :: DetailRow.getCells().getCount() : " + detailRow.getCells().getCount());
			int detailRowCounter = 0;
			while (detailRowCounter < detailRow.getCells().getCount()) {
				detailRow.getCells().get(detailRowCounter).setStyleName("reportTemplateDTDetailStyleForPDF");
				detailRowCounter++;
			}
		} catch (Exception ex) {
			log.error("Exception  for PDF Styling :: " + ex.getMessage());
		}
		
		File dir = new File("D:\\REPORTS");
        if (!dir.exists()) {
            if (dir.mkdir()) {
            	log.info("Directory is created!");
            } else {
            	log.info("Failed to create directory!");
            }
        }
        
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("dd-MMM-yyyy-HH-mm-ss");
		String currentTime = ft.format(dNow);
		try {
			IRunAndRenderTask task = engine2.createRunAndRenderTask(design2);
			PDFRenderOption options = new PDFRenderOption();
			options.setOutputFileName("D:/REPORTS/" + reportTemplateName + "-" + currentTime + ".PDF");
			options.setOutputFormat("pdf");
			task.setRenderOption(options);
			task.run();
			task.close();
			log.info("PDF FILE GENERATED SUCCESSFULLY");

			
			// UPDATE THE PATH IN REPORT_INSTANCE TABLE
			IprodReportsInstanceDao instanceDao = DaoFactory.createIprodReportsInstanceDao();
			try {
				instanceDao.updateReportPDFLocationWhereReportIdIs(reportId, "D:/REPORTS/" + reportTemplateName + "-" + currentTime + ".PDF");
			} catch (Exception e) {
				log.error("Exception while updating Report PDF location" + e.getMessage());
				e.printStackTrace();
			}

		} catch (EngineException e) {
			log.info("REPORT GENERATION IN PDF FORMAT FAILED DUE TO EngineException : " + e.toString());
		} catch (Exception e1) {
			log.info("REPORT GENERATION IN PDF FORMAT FAILED DUE TO General Exception : " + e1.toString());
		}
	}

}
