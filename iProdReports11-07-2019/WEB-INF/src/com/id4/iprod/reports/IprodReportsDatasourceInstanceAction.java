package com.id4.iprod.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import com.id4.iprod.Consts;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.id4.iprod.reports.dao.IprodReportsDatasourceInstanceDao;
import com.id4.iprod.reports.dao.IprodReportsRdbmsMasterDao;
import com.id4.iprod.reports.dto.IprodReportsDatasourceInstance;
import com.id4.iprod.reports.dto.IprodReportsDatasourceInstancePk;
import com.id4.iprod.reports.dto.IprodReportsRdbmsMaster;
import com.id4.iprod.reports.exceptions.IprodReportsDatasourceInstanceDaoException;
import com.id4.iprod.reports.exceptions.IprodReportsRdbmsMasterDaoException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodReportsDatasourceInstanceAction extends ActionSupport implements IprodModuleAuthorization{
	static Logger log = Logger.getLogger(IprodReportsDatasourceInstanceAction.class);
	/** 
	 * This attribute maps to the column DATASOURCE_ID in the iprod_reports_datasource_instance table.
	 */
	protected int datasourceId;

	/** 
	 * This attribute maps to the column DATASOURCE_NAME in the iprod_reports_datasource_instance table.
	 */
	protected String datasourceName;

	/** 
	 * This attribute maps to the column RDBMS_ID in the iprod_reports_datasource_instance table.
	 */
	protected int rdbmsId;

	/** 
	 * This attribute maps to the column RDBMS_JDBC_DRIVER_URL in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsJdbcDriverUrl;

	/** 
	 * This attribute maps to the column RDBMS_IS_WINDOWS_AUTH in the iprod_reports_datasource_instance table.
	 */
	protected int rdbmsIsWindowsAuth;

	/** 
	 * This attribute represents whether the primitive attribute rdbmsIsWindowsAuth is null.
	 */
	protected boolean rdbmsIsWindowsAuthNull = true;

	/** 
	 * This attribute maps to the column RDBMS_DATABASE_NAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsDatabaseName;

	/** 
	 * This attribute maps to the column RDBMS_INSTANCE_NAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsInstanceName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_USERNAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerUsername;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PASSWORD in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerPassword;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_MACHINE_NAME in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerMachineName;

	/** 
	 * This attribute maps to the column RDBMS_SERVER_PORT in the iprod_reports_datasource_instance table.
	 */
	protected String rdbmsServerPort;

	/** 
	 * This attribute maps to the column DATASOURCE_CDATETIME in the iprod_reports_datasource_instance table.
	 */
	protected String datasourceCdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_MDATETIME in the iprod_reports_datasource_instance table.
	 */
	protected String datasourceMdatetime;

	/** 
	 * This attribute maps to the column DATASOURCE_IS_DELETED in the iprod_reports_datasource_instance table.
	 */
	protected short datasourceIsDeleted; 
	
	private String jdbcDriverClassname;
	protected short isDeleted;
    private String data;
    protected String response;
    
    public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	public String escapeSemicolon(String jdbcDriverUrl){
	    return jdbcDriverUrl.replace(";", "\\;");
}
	
	public List<IprodReportsDatasourceInstance> allDatasourceInstanceListG = new ArrayList<IprodReportsDatasourceInstance>();
	public List<IprodReportsRdbmsMaster> allRdbmsListG = new ArrayList<IprodReportsRdbmsMaster>();
	public List<IprodReportsDatasourceInstance> datasourceListG = new ArrayList<IprodReportsDatasourceInstance>();

	
	public String findAllDataSourceInstance()
	{
		try{
			log.info("IprodReportsDatasourceInstanceAction :: findAllDataSourceInstance()");
			Map<String, Object> session = ActionContext.getContext().getSession();
			if(session.get(Consts.IPRODUSERID)!= null)
			{
				IprodReportsDatasourceInstanceDao iprodReportsDatasourceInstanceDaoObj = DaoFactory.createIprodReportsDatasourceInstanceDao();
				List<IprodReportsDatasourceInstance> allDataSourceListL = new ArrayList<IprodReportsDatasourceInstance>();
				
	            try{      	
	            	allDataSourceListL = iprodReportsDatasourceInstanceDaoObj.findWhereDatasourceIsDeletedEquals((short)0);
	                if(allDataSourceListL.size()>0){
	                	allDatasourceInstanceListG = allDataSourceListL;  	
	                }
	                else{
	                    addActionError("Sorry!! Currently there is no datasource to be displayed ");
	                }                                                                           
	                return SUCCESS;
	            }catch(IprodReportsDatasourceInstanceDaoException e){
	                addActionError("Cannot Find datasource list");
	                return ERROR;
	            }
		    }else{
		        addActionError("Sorry!! You have to Login first");
		        return "login";
		    }
		 }catch (Exception e){
		log.error("findAllDataSourceInstance :: Exception :: " + e.getMessage() +" occured in findAllDataSourceInstance.");
	  }	
	  return SUCCESS;	                
	}
	
	public String findAllRdbms()
	{
		try{
			log.info("IprodReportsDatasourceInstanceAction :: findAllRdbms");
			Map<String, Object> session = ActionContext.getContext().getSession();
			int roleId = (Integer) session.get(Consts.IPRODUSERROLEID);
			if(session.get(Consts.IPRODUSERID)!= null)
		    {
			  IprodReportsRdbmsMasterDao rdbmsDao = DaoFactory.createIprodReportsRdbmsMasterDao();
			  List<IprodReportsRdbmsMaster> allRdbmsListL = new ArrayList<IprodReportsRdbmsMaster>();
			  try {		
				  allRdbmsListL = rdbmsDao.findWhereRdbmsIsDeletedEquals((short)0);
				  	  if(allRdbmsListL.size() > 0){				  		  				  		  				  			  
				  		allRdbmsListG = allRdbmsListL;
					  }else{			  	
						  addActionError("Currently there are no datasource to be displayed");
				  	  }
			  } catch (IprodReportsRdbmsMasterDaoException e) {
				addActionError("IprodReportsRdbmsMasterDaoException"+e.getMessage());
				return ERROR;
			  }
			  IprodReportsDatasourceInstanceDao iprodReportDatasourceInstanceDao = DaoFactory.createIprodReportsDatasourceInstanceDao();
			  List<IprodReportsDatasourceInstance> datasourceListL = new ArrayList<IprodReportsDatasourceInstance>();
			  try {		
				  datasourceListL = iprodReportDatasourceInstanceDao.findWhereDatasourceIdEquals(this.datasourceId);
				  
				  	  if(datasourceListL.size() > 0){				  		  				  		  				  			  
				  		datasourceListG = datasourceListL;
					  }else{			  	
						  addActionError("Currently there are no datasource to be displayed");
				  	  }
			  } catch (IprodReportsDatasourceInstanceDaoException e) {
				addActionError("IprodReportsDatasourceInstanceDaoException"+e.getMessage());
				return ERROR;
			  }
			  
		    }else{
		    	addActionError("Sorry!! You have to Login first");
		    	return "login";
		    }
		}catch (Exception e){
			log.error("findAllRdbms :: Exception :: " + e.getMessage() +" occured in getAllRdbms.");
		}
		return SUCCESS;	
	}
	
	
	public String addNewDatasourceInstanceDetails()
	{
		log.info("IprodReportsDatasourceInstanceAction :: addNewDatasourceInstanceDetails");
		Gson gs=new Gson();
		IprodReportsDatasourceInstanceAction IprodReportsDatasourceInstanceActionObject = gs.fromJson(getData(), IprodReportsDatasourceInstanceAction.class);
	    //log.info("Data received:"+ getData());
	 
	    datasourceName=IprodReportsDatasourceInstanceActionObject.getDatasourceName();
	    rdbmsId=IprodReportsDatasourceInstanceActionObject.getRdbmsId();
	    rdbmsIsWindowsAuth=IprodReportsDatasourceInstanceActionObject.getRdbmsIsWindowsAuth();
	    rdbmsDatabaseName=IprodReportsDatasourceInstanceActionObject.getRdbmsDatabaseName();
	    rdbmsInstanceName=IprodReportsDatasourceInstanceActionObject.getRdbmsInstanceName();
	    rdbmsJdbcDriverUrl=IprodReportsDatasourceInstanceActionObject.getRdbmsJdbcDriverUrl();
	    rdbmsServerUsername=IprodReportsDatasourceInstanceActionObject.getRdbmsServerUsername();
	    rdbmsServerPassword=IprodReportsDatasourceInstanceActionObject.getRdbmsServerPassword();
	    rdbmsServerMachineName=IprodReportsDatasourceInstanceActionObject.getRdbmsServerMachineName();
	    rdbmsServerPort=IprodReportsDatasourceInstanceActionObject.getRdbmsServerPort();
	    
		List listQueryColName= new ArrayList();
		Map<String, Object> session = ActionContext.getContext().getSession();
		if(session.get(Consts.IPRODUSERID)!= null){
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat ("dd MMM yyyy HH:mm:ss:mm");
			String currentDateTime = ft.format(dNow);
		 	IprodReportsDatasourceInstanceDao dataSourceInstanceDao = DaoFactory.createIprodReportsDatasourceInstanceDao();
		 	IprodReportsDatasourceInstance dataSourceInstance = new IprodReportsDatasourceInstance();
		 	dataSourceInstance.setDatasourceName(datasourceName);
		 	dataSourceInstance.setRdbmsId(rdbmsId);
		 	dataSourceInstance.setDatasourceId(datasourceId);
		 	dataSourceInstance.setRdbmsIsWindowsAuth(rdbmsIsWindowsAuth);
		 	dataSourceInstance.setRdbmsDatabaseName(rdbmsDatabaseName);
		 	dataSourceInstance.setRdbmsInstanceName(rdbmsInstanceName);
		 	dataSourceInstance.setRdbmsJdbcDriverUrl(rdbmsJdbcDriverUrl);
		 	dataSourceInstance.setRdbmsServerUsername(rdbmsServerUsername);
		 	dataSourceInstance.setRdbmsServerPassword(rdbmsServerPassword);
		 	dataSourceInstance.setRdbmsServerMachineName(rdbmsServerMachineName);
		 	dataSourceInstance.setRdbmsServerPort(rdbmsServerPort);
		 	dataSourceInstance.setDatasourceCdatetime(currentDateTime);
		 	dataSourceInstance.setDatasourceMdatetime(currentDateTime);
		 	
			IprodReportsDatasourceInstancePk dataSourceInstancePk = new IprodReportsDatasourceInstancePk(); 
			dataSourceInstancePk = dataSourceInstanceDao.insert(dataSourceInstance);	
			setResponse("Datasource Connection Profile "+this.datasourceName+" has been successfully added");
			log.info("Datasource Connection Profile "+this.datasourceName+" has been successfully added");	
		 	//addActionMessage("Datasource Connection Profile "+this.datasourceInstanceName+" has been successfully added");
		return SUCCESS;
		}else {
		 	setResponse("Sorry!! You have to Login first");
	    	//addActionError("Sorry!! You have to Login first");
	    	return "login";
		}
	}
	
	public String updateDatasourceInstanceDetails()
	{
		log.info("IprodReportsDatasourceInstanceAction :: updateDatasourceInstanceDetails");
		Gson gs=new Gson();
		IprodReportsDatasourceInstanceAction IprodReportsDatasourceInstanceActionObject = gs.fromJson(getData(), IprodReportsDatasourceInstanceAction.class);
	   // log.info("Data received:"+ getData());
	    datasourceId = IprodReportsDatasourceInstanceActionObject.getDatasourceId();
	    datasourceName=IprodReportsDatasourceInstanceActionObject.getDatasourceName();
	    rdbmsId=IprodReportsDatasourceInstanceActionObject.getRdbmsId();
	    rdbmsIsWindowsAuth=IprodReportsDatasourceInstanceActionObject.getRdbmsIsWindowsAuth();
	    rdbmsDatabaseName=IprodReportsDatasourceInstanceActionObject.getRdbmsDatabaseName();
	    rdbmsInstanceName=IprodReportsDatasourceInstanceActionObject.getRdbmsInstanceName();
	    rdbmsJdbcDriverUrl=IprodReportsDatasourceInstanceActionObject.getRdbmsJdbcDriverUrl();
	    rdbmsServerUsername=IprodReportsDatasourceInstanceActionObject.getRdbmsServerUsername();
	    rdbmsServerPassword=IprodReportsDatasourceInstanceActionObject.getRdbmsServerPassword();
	    rdbmsServerMachineName=IprodReportsDatasourceInstanceActionObject.getRdbmsServerMachineName();
	    rdbmsServerPort=IprodReportsDatasourceInstanceActionObject.getRdbmsServerPort();
	    
		List listQueryColName= new ArrayList();
		Map<String, Object> session = ActionContext.getContext().getSession();
		if(session.get(Consts.IPRODUSERID)!= null){
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat ("dd MMM yyyy HH:mm:ss:mm");
			String currentDateTime = ft.format(dNow);
		 	IprodReportsDatasourceInstanceDao dataSourceInstanceDao = DaoFactory.createIprodReportsDatasourceInstanceDao();
		 	try {
				dataSourceInstanceDao.updateDatasourceDetailsWhereDatasourceIdEquals(datasourceName,rdbmsId,rdbmsDatabaseName,rdbmsJdbcDriverUrl,datasourceId);
			} catch (IprodReportsDatasourceInstanceDaoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			setResponse("Datasource Connection Profile "+this.datasourceName+" has been successfully updated");
			log.info("Datasource Connection Profile "+this.datasourceName+" has been successfully updated");	
		 	//addActionMessage("Datasource Connection Profile "+this.datasourceInstanceName+" has been successfully added");
		return SUCCESS;
		}else {
		 	setResponse("Sorry!! You have to Login first");
	    	//addActionError("Sorry!! You have to Login first");
	    	return "login";
		}
	}
	
	public String deleteDatasource(){
		log.info("IprodReportsDatasourceInstanceAction: deleteDatasource()"); 
		Map<String, Object> session = ActionContext.getContext().getSession();
		if (session.get(Consts.IPRODUSERID) != null) {
			IprodReportsDatasourceInstanceDao dataSourceInstanceDao = DaoFactory.createIprodReportsDatasourceInstanceDao();
				try {
					dataSourceInstanceDao.updateIsDeleted((short)1,this.datasourceId);
				} catch (IprodReportsDatasourceInstanceDaoException e) {
					e.printStackTrace();
				}			
				addActionMessage("Datasource deleted Successfully !");
		} else {
			addActionError("Sorry!! You have to Login first");
			return ERROR;
		}		
		return SUCCESS;
	 }
	
	
	
	

	
	
	public int getDatasourceId()
	{
		return datasourceId;
	}

	/**
	 * Method 'setDatasourceId'
	 * 
	 * @param datasourceId
	 */
	public void setDatasourceId(int datasourceId)
	{
		this.datasourceId = datasourceId;
	}

	/**
	 * Method 'getDatasourceName'
	 * 
	 * @return String
	 */
	public String getDatasourceName()
	{
		return datasourceName;
	}

	/**
	 * Method 'setDatasourceName'
	 * 
	 * @param datasourceName
	 */
	public void setDatasourceName(String datasourceName)
	{
		this.datasourceName = datasourceName;
	}

	/**
	 * Method 'getRdbmsId'
	 * 
	 * @return int
	 */
	public int getRdbmsId()
	{
		return rdbmsId;
	}

	/**
	 * Method 'setRdbmsId'
	 * 
	 * @param rdbmsId
	 */
	public void setRdbmsId(int rdbmsId)
	{
		this.rdbmsId = rdbmsId;
	}

	/**
	 * Method 'getRdbmsJdbcDriverUrl'
	 * 
	 * @return String
	 */
	public String getRdbmsJdbcDriverUrl()
	{
		return rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'setRdbmsJdbcDriverUrl'
	 * 
	 * @param rdbmsJdbcDriverUrl
	 */
	public void setRdbmsJdbcDriverUrl(String rdbmsJdbcDriverUrl)
	{
		this.rdbmsJdbcDriverUrl = rdbmsJdbcDriverUrl;
	}

	/**
	 * Method 'getRdbmsIsWindowsAuth'
	 * 
	 * @return int
	 */
	public int getRdbmsIsWindowsAuth()
	{
		return rdbmsIsWindowsAuth;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuth'
	 * 
	 * @param rdbmsIsWindowsAuth
	 */
	public void setRdbmsIsWindowsAuth(int rdbmsIsWindowsAuth)
	{
		this.rdbmsIsWindowsAuth = rdbmsIsWindowsAuth;
		this.rdbmsIsWindowsAuthNull = false;
	}

	/**
	 * Method 'setRdbmsIsWindowsAuthNull'
	 * 
	 * @param value
	 */
	public void setRdbmsIsWindowsAuthNull(boolean value)
	{
		this.rdbmsIsWindowsAuthNull = value;
	}

	/**
	 * Method 'isRdbmsIsWindowsAuthNull'
	 * 
	 * @return boolean
	 */
	public boolean isRdbmsIsWindowsAuthNull()
	{
		return rdbmsIsWindowsAuthNull;
	}

	/**
	 * Method 'getRdbmsDatabaseName'
	 * 
	 * @return String
	 */
	public String getRdbmsDatabaseName()
	{
		return rdbmsDatabaseName;
	}

	/**
	 * Method 'setRdbmsDatabaseName'
	 * 
	 * @param rdbmsDatabaseName
	 */
	public void setRdbmsDatabaseName(String rdbmsDatabaseName)
	{
		this.rdbmsDatabaseName = rdbmsDatabaseName;
	}

	/**
	 * Method 'getRdbmsInstanceName'
	 * 
	 * @return String
	 */
	public String getRdbmsInstanceName()
	{
		return rdbmsInstanceName;
	}

	/**
	 * Method 'setRdbmsInstanceName'
	 * 
	 * @param rdbmsInstanceName
	 */
	public void setRdbmsInstanceName(String rdbmsInstanceName)
	{
		this.rdbmsInstanceName = rdbmsInstanceName;
	}

	/**
	 * Method 'getRdbmsServerUsername'
	 * 
	 * @return String
	 */
	public String getRdbmsServerUsername()
	{
		return rdbmsServerUsername;
	}

	/**
	 * Method 'setRdbmsServerUsername'
	 * 
	 * @param rdbmsServerUsername
	 */
	public void setRdbmsServerUsername(String rdbmsServerUsername)
	{
		this.rdbmsServerUsername = rdbmsServerUsername;
	}

	/**
	 * Method 'getRdbmsServerPassword'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPassword()
	{
		return rdbmsServerPassword;
	}

	/**
	 * Method 'setRdbmsServerPassword'
	 * 
	 * @param rdbmsServerPassword
	 */
	public void setRdbmsServerPassword(String rdbmsServerPassword)
	{
		this.rdbmsServerPassword = rdbmsServerPassword;
	}

	/**
	 * Method 'getRdbmsServerMachineName'
	 * 
	 * @return String
	 */
	public String getRdbmsServerMachineName()
	{
		return rdbmsServerMachineName;
	}

	/**
	 * Method 'setRdbmsServerMachineName'
	 * 
	 * @param rdbmsServerMachineName
	 */
	public void setRdbmsServerMachineName(String rdbmsServerMachineName)
	{
		this.rdbmsServerMachineName = rdbmsServerMachineName;
	}

	/**
	 * Method 'getRdbmsServerPort'
	 * 
	 * @return String
	 */
	public String getRdbmsServerPort()
	{
		return rdbmsServerPort;
	}

	/**
	 * Method 'setRdbmsServerPort'
	 * 
	 * @param rdbmsServerPort
	 */
	public void setRdbmsServerPort(String rdbmsServerPort)
	{
		this.rdbmsServerPort = rdbmsServerPort;
	}

	/**
	 * Method 'getDatasourceCdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceCdatetime()
	{
		return datasourceCdatetime;
	}

	/**
	 * Method 'setDatasourceCdatetime'
	 * 
	 * @param datasourceCdatetime
	 */
	public void setDatasourceCdatetime(String datasourceCdatetime)
	{
		this.datasourceCdatetime = datasourceCdatetime;
	}

	/**
	 * Method 'getDatasourceMdatetime'
	 * 
	 * @return String
	 */
	public String getDatasourceMdatetime()
	{
		return datasourceMdatetime;
	}

	/**
	 * Method 'setDatasourceMdatetime'
	 * 
	 * @param datasourceMdatetime
	 */
	public void setDatasourceMdatetime(String datasourceMdatetime)
	{
		this.datasourceMdatetime = datasourceMdatetime;
	}

	/**
	 * Method 'getDatasourceIsDeleted'
	 * 
	 * @return short
	 */
	public short getDatasourceIsDeleted()
	{
		return datasourceIsDeleted;
	}

	/**
	 * Method 'setDatasourceIsDeleted'
	 * 
	 * @param datasourceIsDeleted
	 */
	public void setDatasourceIsDeleted(short datasourceIsDeleted)
	{
		this.datasourceIsDeleted = datasourceIsDeleted;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}



	public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();
	 
	public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() 
	{
			return hashMapCheckRoleModuleOperationListG;
    }
	
	public void setHashMapCheckRoleModuleOperationListG(HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
		this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
	}
}
