package com.id4.iprod.factory;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

import com.id4.iprod.dao.IprodModuleMasterDao;
import com.id4.iprod.dao.IprodModuleOperationMasterDao;
import com.id4.iprod.dao.IprodOperationMasterDao;
import com.id4.iprod.dao.IprodReportsLicenseDao;
import com.id4.iprod.dao.IprodRoleMasterDao;
import com.id4.iprod.dao.IprodRoleModuleOperationsDao;
import com.id4.iprod.dao.IprodSchedulerMasterDao;
import com.id4.iprod.dao.IprodUserMasterDao;
import com.id4.iprod.dao.IprodUserRoleDao;
import com.id4.iprod.dao.IprodViewModuleOperationMasterDao;
import com.id4.iprod.dao.IprodViewRoleModuleOperationsDao;
import com.id4.iprod.dao.IprodViewUserDetailsDao;
import com.id4.iprod.reports.dao.IprodImageMasterDao;
import com.id4.iprod.reports.dao.IprodReportsDatasourceInstanceDao;
import com.id4.iprod.reports.dao.IprodReportsInstanceDao;
import com.id4.iprod.reports.dao.IprodReportsJobDataDao;
import com.id4.iprod.reports.dao.IprodReportsRdbmsMasterDao;
import com.id4.iprod.reports.dao.IprodReportsRecipientMasterDao;
import com.id4.iprod.reports.dao.IprodReportsTemplateMasterDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportDatasourceDetailsDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportTemplateDetailsDao;
import com.id4.iprod.reports.dao.IprodReportsViewReportsInstanceDetailsDao;
import com.id4.iprod.reports.dto.IprodReportsViewReportsInstanceDetails;

@SuppressWarnings("deprecation")
public class DaoFactory
{

	/**
	 * Method 'createIprodRoleMasterDao'
	 * 
	 * @return IprodRoleMasterDao
	 */
	public static IprodRoleMasterDao createIprodRoleMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodRoleMasterDao) bf.getBean( "IprodRoleMasterDao" );
	}

	/**
	 * Method 'createIprodSchedulerMasterDao'
	 * 
	 * @return IprodSchedulerMasterDao
	 */
	public static IprodSchedulerMasterDao createIprodSchedulerMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodSchedulerMasterDao) bf.getBean( "IprodSchedulerMasterDao" );
	}

	/**
	 * Method 'createIprodUserMasterDao'
	 * 
	 * @return IprodUserMasterDao
	 */
	public static IprodUserMasterDao createIprodUserMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodUserMasterDao) bf.getBean( "IprodUserMasterDao" );
	}

	/**
	 * Method 'createIprodUserRoleDao'
	 * 
	 * @return IprodUserRoleDao
	 */
	public static IprodUserRoleDao createIprodUserRoleDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodUserRoleDao) bf.getBean( "IprodUserRoleDao" );
	}
		
	/**
	 * Method 'createIprodViewUserDetailsDao'
	 * 
	 * @return IprodViewUserDetailsDao
	 */
	public static IprodViewUserDetailsDao createIprodViewUserDetailsDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodViewUserDetailsDao) bf.getBean( "IprodViewUserDetailsDao" );
	}
	
	public static IprodViewRoleModuleOperationsDao  createIprodViewRoleModuleOperationsDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodViewRoleModuleOperationsDao) bf.getBean( "IprodViewRoleModuleOperationsDao" );
	}
	
	/**
	 * Method 'createIprodRoleModuleOperationsDao'
	 * 
	 * @return IprodRoleModuleOperationsDao
	 */
	public static IprodRoleModuleOperationsDao createIprodRoleModuleOperationsDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodRoleModuleOperationsDao) bf.getBean( "IprodRoleModuleOperationsDao" );
	}
	
	/**
	 * Method 'createIprodModuleOperationMasterDao'
	 * 
	 * @return IprodModuleOperationMasterDao
	 */
	public static IprodModuleOperationMasterDao createIprodModuleOperationMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodModuleOperationMasterDao) bf.getBean( "IprodModuleOperationMasterDao" );
	}
	
	/**
	 * Method 'createIprodModuleMasterDao'
	 * 
	 * @return IprodModuleMasterDao
	 */
	public static IprodModuleMasterDao createIprodModuleMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodModuleMasterDao) bf.getBean( "IprodModuleMasterDao" );
	}
	
	/**
	 * Method 'createIprodOperationMasterDao'
	 * 
	 * @return IprodOperationMasterDao
	 */
	public static IprodOperationMasterDao createIprodOperationMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodOperationMasterDao) bf.getBean( "IprodOperationMasterDao" );
	}
	
	public static IprodViewModuleOperationMasterDao  createIprodViewModuleOperationMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodViewModuleOperationMasterDao) bf.getBean( "IprodViewModuleOperationMasterDao" );
	}
	
	/**
	 * Method 'createIprodImageMasterDao'
	 * 
	 * @return IprodImageMasterDao
	 */
	public static IprodImageMasterDao createIprodImageMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodImageMasterDao) bf.getBean( "IprodImageMasterDao" );
	}
	
	/**
	 * Method 'createIprodReportsInstanceDao'
	 * 
	 * @return IprodReportsInstanceDao
	 */
	public static IprodReportsInstanceDao createIprodReportsInstanceDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsInstanceDao) bf.getBean( "IprodReportsInstanceDao" );
	}

	/**
	 * Method 'createIprodReportsJobDataDao'
	 * 
	 * @return IprodReportsJobDataDao
	 */
	public static IprodReportsJobDataDao createIprodReportsJobDataDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsJobDataDao) bf.getBean( "IprodReportsJobDataDao" );
	}

	/**
	 * Method 'createIprodReportsViewReportDatasourceDetailsDao'
	 * 
	 * @return IprodReportsViewReportDatasourceDetailsDao
	 */
	public static IprodReportsViewReportDatasourceDetailsDao createIprodReportsViewReportDatasourceDetailsDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsViewReportDatasourceDetailsDao) bf.getBean( "IprodReportsViewReportDatasourceDetailsDao" );
	}
	
	
	/**
	 * Method 'createIprodReportsViewReportInstanceDetailsDao'
	 * 
	 * @return IprodReportsViewReportTemplateDetailsDao
	 */
	public static IprodReportsViewReportTemplateDetailsDao createIprodReportsViewReportTemplateDetailsDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsViewReportTemplateDetailsDao) bf.getBean( "IprodReportsViewReportTemplateDetailsDao" );
	}
		

	/**
	 * Method 'createIprodReportsRecipientMasterDao'
	 * 
	 * @return IprodReportsRecipientMasterDao
	 */
	public static IprodReportsRecipientMasterDao createIprodReportsRecipientMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsRecipientMasterDao) bf.getBean( "IprodReportsRecipientMasterDao" );
	}

	/**
	 * Method 'createIprodReportsTemplateMasterDao'
	 * 
	 * @return IprodReportsTemplateMasterDao
	 */
	public static IprodReportsTemplateMasterDao createIprodReportsTemplateMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsTemplateMasterDao) bf.getBean( "IprodReportsTemplateMasterDao" );
	}
	
	public static IprodReportsDatasourceInstanceDao createIprodReportsDatasourceInstanceDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsDatasourceInstanceDao) bf.getBean( "IprodReportsDatasourceInstanceDao" );
	}
	
	public static IprodReportsRdbmsMasterDao createIprodReportsRdbmsMasterDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsRdbmsMasterDao) bf.getBean( "IprodReportsRdbmsMasterDao" );
	}
	public static IprodReportsLicenseDao createIprodReportsLicenseDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprod-dao-beans.xml") );
		return (IprodReportsLicenseDao) bf.getBean( "IprodReportsLicenseDao" );
	}
	
	public static IprodReportsViewReportsInstanceDetailsDao createIprodReportsViewReportsInstanceDetailsDao()
	{
		BeanFactory bf = new XmlBeanFactory( new ClassPathResource("iprodreports-dao-beans.xml") );
		return (IprodReportsViewReportsInstanceDetailsDao) bf.getBean( "IprodReportsViewReportsInstanceDetailsDao" );
	}
	
	}