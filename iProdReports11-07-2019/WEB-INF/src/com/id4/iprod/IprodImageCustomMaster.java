package com.id4.iprod;

import com.id4.iprod.reports.dao.*;
import com.id4.iprod.reports.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodImageCustomMaster implements Serializable
{
	/** 
	 * This attribute maps to the column ImageID in the iprod_image_master table.
	 */
	protected int imageId;

	/** 
	 * This attribute maps to the column ImageName in the iprod_image_master table.
	 */
	protected String imageName;

	/** 
	 * This attribute maps to the column ImageContent in the iprod_image_master table.
	 */
	protected String imageContent;

	/**
	 * Method 'IprodImageCustomMaster'
	 * 
	 */
	public IprodImageCustomMaster()
	{
	}

	/**
	 * Method 'IprodImageCustomMaster'
	 * Parameterised Constructor
	 */
	public IprodImageCustomMaster(int imageID, String imageName, String imageContent)
	{
		this.imageId = imageID;
		this.imageName = imageName;
		this.imageContent = imageContent;
	}
	
	/**
	 * Method 'getImageID'
	 * 
	 * @return int
	 */
	public int getImageId()
	{
		return imageId;
	}

	/**
	 * Method 'setImageID'
	 * 
	 * @param imageID
	 */
	public void setImageID(int imageID)
	{
		this.imageId = imageID;
	}

	/**
	 * Method 'getImageName'
	 * 
	 * @return String
	 */
	public String getImageName()
	{
		return imageName;
	}

	/**
	 * Method 'setImageName'
	 * 
	 * @param imageName
	 */
	public void setImageName(String imageName)
	{
		this.imageName = imageName;
	}

	/**
	 * Method 'getImageContent'
	 * 
	 * @return byte[]
	 */
	public String getImageContent()
	{
		return imageContent;
	}

	/**
	 * Method 'setImageContent'
	 * 
	 * @param imageContent
	 */
	public void setImageContent(String imageContent)
	{
		this.imageContent = imageContent;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodImageCustomMaster)) {
			return false;
		}
		
		final IprodImageCustomMaster _cast = (IprodImageCustomMaster) _other;
		if (imageId != _cast.imageId) {
			return false;
		}
		
		if (imageName == null ? _cast.imageName != imageName : !imageName.equals( _cast.imageName )) {
			return false;
		}
		
		if (imageContent == null ? _cast.imageContent != imageContent : !imageContent.equals( _cast.imageContent )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + imageId;
		if (imageName != null) {
			_hashCode = 29 * _hashCode + imageName.hashCode();
		}
		
		if (imageContent != null) {
			_hashCode = 29 * _hashCode + imageContent.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.reports.dto.IprodImageCustomMaster: " );
		ret.append( "imageID=" + imageId );
		ret.append( ", imageName=" + imageName );
		ret.append( ", imageContent=" + imageContent );
		return ret.toString();
	}

}
