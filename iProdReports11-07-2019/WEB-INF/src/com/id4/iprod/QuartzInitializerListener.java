package com.id4.iprod;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

//import com.id4.iProdReports.schedule.listeners.MyJobListener;  

public class QuartzInitializerListener implements ServletContextListener {
	private boolean performShutdown = true;
	private Scheduler scheduler = null;
	public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";
	
	public void contextDestroyed(ServletContextEvent sce) {
		 System.out.println("sce."+sce);
		 System.out.println("Scheduler."+scheduler);
		// TODO Auto-generated method stub
		if (!performShutdown) {
		              return;
		     }
		try {
		    if (scheduler != null) {
		        scheduler.shutdown();
		        Thread.sleep(1000);
		        System.out.println("Quartz Scheduler successful shutdown.");
		     }
	    } catch (Exception e) {
			 System.out.println("Quartz Scheduler failed to shutdown cleanly: " + e.toString());
			 e.printStackTrace();
			 }
	  	 System.out.println("performShutdown."+performShutdown);
      }
 
	public void contextInitialized(ServletContextEvent sce) {
		 System.out.println("2st."+performShutdown);
		// TODO Auto-generated method stub
		System.out.println("Quartz Initializer Servlet loaded, initializing Scheduler...");
		ServletContext servletContext = sce.getServletContext();
		StdSchedulerFactory factory;
	    try {
          String shutdownPref = servletContext.getInitParameter("shutdown-on-unload");
          if (shutdownPref != null) {
              performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
           }
          System.out.println("shutdownPref."+shutdownPref);
          // get Properties
          factory = new StdSchedulerFactory("quartz.properties");
          // Always want to get the scheduler, even if it isn't starting, 
          // to make sure it is both initialized and registered.
          scheduler = factory.getScheduler();
          servletContext.setAttribute(QUARTZ_FACTORY_KEY, factory);
          
           // Should the Scheduler being started now or later
          String startOnLoad = servletContext.getInitParameter("start-scheduler-on-load");
          System.out.println("startOnLoad."+startOnLoad);
           /*
            * If the "start-scheduler-on-load" init-parameter is not specified,
             * the scheduler will be started. This is to maintain backwards
             * compatability.
             */
            if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
                // Start now
                scheduler.start();
                System.out.println("Scheduler has been started...");
                System.out.println("Now check whether iPROD essential auto triggers are set. If not create them.");
                createIprodAutoTriggers(servletContext);
            } else {
                System.out.println("Scheduler has not been started. Use scheduler.start()");
            }
        } catch (Exception e) {
            System.out.println("Quartz Scheduler failed to initialize: " + e.toString());
             e.printStackTrace();
       }
        System.out.println("performShutdown."+performShutdown);
        System.out.println("Scheduler."+scheduler);
	}

	/**
	 * @throws SchedulerException
	 * 
	 */
	private void createIprodAutoTriggers(ServletContext servletContext)
			throws SchedulerException {
		
		
	}

}
 



