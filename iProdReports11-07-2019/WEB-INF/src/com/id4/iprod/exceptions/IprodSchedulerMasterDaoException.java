package com.id4.iprod.exceptions;

public class IprodSchedulerMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodSchedulerMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodSchedulerMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodSchedulerMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodSchedulerMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
