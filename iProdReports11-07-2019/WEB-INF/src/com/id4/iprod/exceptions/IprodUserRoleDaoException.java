package com.id4.iprod.exceptions;

public class IprodUserRoleDaoException extends DaoException
{
	/**
	 * Method 'IprodUserRoleDaoException'
	 * 
	 * @param message
	 */
	public IprodUserRoleDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodUserRoleDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodUserRoleDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
