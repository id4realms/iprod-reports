package com.id4.iprod.exceptions;

public class IprodModuleMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodModuleMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodModuleMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodModuleMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodModuleMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
