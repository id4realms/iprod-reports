package com.id4.iprod.exceptions;

public class IprodViewRoleModuleOperationsDaoException extends DaoException
{
	/**
	 * Method 'IprodViewRoleModuleOperationsDaoException'
	 * 
	 * @param message
	 */
	public IprodViewRoleModuleOperationsDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodViewRoleModuleOperationsDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodViewRoleModuleOperationsDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
