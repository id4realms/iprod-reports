package com.id4.iprod.exceptions;

public class IprodOperationLoggerHistoryDaoException extends DaoException
{
	/**
	 * Method 'IprodOperationLoggerHistoryDaoException'
	 * 
	 * @param message
	 */
	public IprodOperationLoggerHistoryDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodOperationLoggerHistoryDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodOperationLoggerHistoryDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
