package com.id4.iprod.exceptions;

public class IprodUserMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodUserMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodUserMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodUserMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodUserMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
