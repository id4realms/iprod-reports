package com.id4.iprod.exceptions;

public class IprodOperationMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodOperationMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodOperationMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodOperationMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodOperationMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
