package com.id4.iprod.exceptions;

public class IprodViewModuleOperationMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodViewModuleOperationMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodViewModuleOperationMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodViewModuleOperationMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodViewModuleOperationMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
