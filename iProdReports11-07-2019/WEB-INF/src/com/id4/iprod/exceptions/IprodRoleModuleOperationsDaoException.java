package com.id4.iprod.exceptions;

public class IprodRoleModuleOperationsDaoException extends DaoException
{
	/**
	 * Method 'IprodRoleModuleOperationsDaoException'
	 * 
	 * @param message
	 */
	public IprodRoleModuleOperationsDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodRoleModuleOperationsDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodRoleModuleOperationsDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
