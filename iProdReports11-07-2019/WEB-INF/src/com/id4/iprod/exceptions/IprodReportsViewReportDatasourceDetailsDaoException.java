package com.id4.iprod.exceptions;

public class IprodReportsViewReportDatasourceDetailsDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsViewReportDatasourceDetailsDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsViewReportDatasourceDetailsDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsViewReportDatasourceDetailsDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsViewReportDatasourceDetailsDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
