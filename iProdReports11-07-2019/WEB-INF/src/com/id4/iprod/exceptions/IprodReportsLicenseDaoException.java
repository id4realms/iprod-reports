package com.id4.iprod.exceptions;

public class IprodReportsLicenseDaoException extends DaoException
{
	/**
	 * Method 'IprodReportsLicenseDaoException'
	 * 
	 * @param message
	 */
	public IprodReportsLicenseDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodReportsLicenseDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodReportsLicenseDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
