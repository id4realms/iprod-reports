package com.id4.iprod.exceptions;

public class IprodViewUserDetailsDaoException extends DaoException
{
	/**
	 * Method 'IprodViewUserDetailsDaoException'
	 * 
	 * @param message
	 */
	public IprodViewUserDetailsDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodViewUserDetailsDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodViewUserDetailsDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
