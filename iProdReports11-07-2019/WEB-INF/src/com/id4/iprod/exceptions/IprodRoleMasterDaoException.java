package com.id4.iprod.exceptions;

public class IprodRoleMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodRoleMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodRoleMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodRoleMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodRoleMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
