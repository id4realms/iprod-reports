package com.id4.iprod.exceptions;

public class IprodModuleOperationMasterDaoException extends DaoException
{
	/**
	 * Method 'IprodModuleOperationMasterDaoException'
	 * 
	 * @param message
	 */
	public IprodModuleOperationMasterDaoException(String message)
	{
		super(message);
	}

	/**
	 * Method 'IprodModuleOperationMasterDaoException'
	 * 
	 * @param message
	 * @param cause
	 */
	public IprodModuleOperationMasterDaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
