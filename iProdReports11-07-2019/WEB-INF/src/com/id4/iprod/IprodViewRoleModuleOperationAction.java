package com.id4.iprod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.dto.IprodModuleMaster;
import com.id4.iprod.dto.IprodOperationMaster;
import com.id4.iprod.dto.IprodViewModuleOperationMaster;
import com.id4.iprod.dto.IprodViewRoleModuleOperations;
import com.id4.iprod.exceptions.IprodModuleMasterDaoException;
import com.id4.iprod.exceptions.IprodOperationMasterDaoException;
import com.id4.iprod.exceptions.IprodViewRoleModuleOperationsDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodViewRoleModuleOperationAction extends ActionSupport implements IprodModuleAuthorization{
	
	static Logger log = Logger.getLogger(IprodViewRoleModuleOperationAction.class);
	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_view_role_module_operations table.
	 */
	public int roleId;

	/** 
	 * This attribute maps to the column ROLE_NAME in the iprod_view_role_module_operations table.
	 */
	public String roleName;

	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_view_role_module_operations table.
	 */
	public int moduleId;

	/** 
	 * This attribute maps to the column MODULE_NAME in the iprod_view_role_module_operations table.
	 */
	public String moduleName;

	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_view_role_module_operations table.
	 */
	public int operationId;

	/** 
	 * This attribute maps to the column OPERATION_NAME in the iprod_view_role_module_operations table.
	 */
	public String operationName;

	/** 
	 * This attribute maps to the column OPERATION_DESCRIPTION in the iprod_view_role_module_operations table.
	 */
	public String operationDescription;

	/** 
	 * This attribute maps to the column IS_ALLOWED in the iprod_view_role_module_operations table.
	 */
	public short isAllowed;


	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	
	public List<IprodModuleMaster> moduleListG = new ArrayList<IprodModuleMaster>();
	public List<IprodOperationMaster> operationListG = new ArrayList<IprodOperationMaster>();
	public List<IprodViewModuleOperationMaster> moduleWiseOperationListG = new ArrayList<IprodViewModuleOperationMaster>();
	public List<IprodViewRoleModuleOperations> moduleWiseOperationPermissionsListG = new ArrayList<IprodViewRoleModuleOperations>();
	public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();

	public String getAllRolePermissionsDetails(){
		log.info("IprodViewRoleModuleOperationAction :: getAllRolePermissionsDetails");
		try{
		Map<String, Object> session = ActionContext.getContext().getSession();
		if(session.get(Consts.IPRODUSERID)!= null)
	    {
			List<IprodModuleMaster> moduleListL = new ArrayList<IprodModuleMaster>();
			List<IprodOperationMaster> operationListL = new ArrayList<IprodOperationMaster>();
			List<IprodViewRoleModuleOperations> moduleWiseOperationPermissionsListL = new ArrayList<IprodViewRoleModuleOperations>();
			//TO GET ALL MODULES
			try
			{
				moduleListL = DaoFactory.createIprodModuleMasterDao().findAll();
				if(moduleListL.size()>0)
				{
					moduleListG = moduleListL;
					log.info("moduleListG" +moduleListG);

				}
			}
			catch(IprodModuleMasterDaoException e)
			{
				log.info("Exception" +e.getMessage());
			}
			//TO GET ALL OPERATIONS
			try
			{
				operationListL = DaoFactory.createIprodOperationMasterDao().findAll();
				if(operationListL.size()>0)
				{
					operationListG = operationListL;
					log.info("operationListG" + operationListG);

				}
			}
			catch(IprodOperationMasterDaoException e)
			{
				log.error("Exception" +e.getMessage());
			}
			
			try
			{
				moduleWiseOperationPermissionsListL = DaoFactory.createIprodViewRoleModuleOperationsDao().findWhereRoleIdEquals(this.roleId);
				if(moduleWiseOperationPermissionsListL.size() > 0)
				{
					moduleWiseOperationPermissionsListG = moduleWiseOperationPermissionsListL;
					log.info("moduleWiseOperationPermissionsListG ::" +moduleWiseOperationPermissionsListG);

				}
			}
			catch(IprodViewRoleModuleOperationsDaoException e)
			{
				log.error("Exception" +e.getMessage());
			}

	    }
	 }
		catch (Exception e){
			log.error("getAllRoles :: Exception :: " + e.getMessage() +" occured in getAllRoles.");
		}
		
		return SUCCESS;
	}
	

	
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/**
	 * Method 'getRoleName'
	 * 
	 * @return String
	 */
	public String getRoleName()
	{
		return roleName;
	}

	/**
	 * Method 'setRoleName'
	 * 
	 * @param roleName
	 */
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}

	/**
	 * Method 'getModuleId'
	 * 
	 * @return int
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'setModuleId'
	 * 
	 * @param moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getModuleName'
	 * 
	 * @return String
	 */
	public String getModuleName()
	{
		return moduleName;
	}

	/**
	 * Method 'setModuleName'
	 * 
	 * @param moduleName
	 */
	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	/**
	 * Method 'getOperationId'
	 * 
	 * @return int
	 */
	public int getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'setOperationId'
	 * 
	 * @param operationId
	 */
	public void setOperationId(int operationId)
	{
		this.operationId = operationId;
	}

	/**
	 * Method 'getOperationName'
	 * 
	 * @return String
	 */
	public String getOperationName()
	{
		return operationName;
	}

	/**
	 * Method 'setOperationName'
	 * 
	 * @param operationName
	 */
	public void setOperationName(String operationName)
	{
		this.operationName = operationName;
	}

	/**
	 * Method 'getOperationDescription'
	 * 
	 * @return String
	 */
	public String getOperationDescription()
	{
		return operationDescription;
	}

	/**
	 * Method 'setOperationDescription'
	 * 
	 * @param operationDescription
	 */
	public void setOperationDescription(String operationDescription)
	{
		this.operationDescription = operationDescription;
	}

	/**
	 * Method 'getIsAllowed'
	 * 
	 * @return short
	 */
	public short getIsAllowed()
	{
		return isAllowed;
	}

	/**
	 * Method 'setIsAllowed'
	 * 
	 * @param isAllowed
	 */
	public void setIsAllowed(short isAllowed)
	{
		this.isAllowed = isAllowed;
	}
	
	 public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
			return hashMapCheckRoleModuleOperationListG;
		}

		public void setHashMapCheckRoleModuleOperationListG(
				HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
			this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
		}


	
}
