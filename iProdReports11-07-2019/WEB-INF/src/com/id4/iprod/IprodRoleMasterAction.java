package com.id4.iprod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.id4.iprod.dao.IprodModuleMasterDao;
import com.id4.iprod.dao.IprodModuleOperationMasterDao;
import com.id4.iprod.dao.IprodRoleMasterDao;
import com.id4.iprod.dao.IprodRoleModuleOperationsDao;
import com.id4.iprod.dao.IprodViewRoleModuleOperationsDao;
import com.id4.iprod.dto.IprodModuleMaster;
import com.id4.iprod.dto.IprodRoleMaster;
import com.id4.iprod.dto.IprodRoleMasterPk;
import com.id4.iprod.dto.IprodRoleModuleOperations;
import com.id4.iprod.dto.IprodRoleModuleOperationsPk;
import com.id4.iprod.dto.IprodViewRoleModuleOperations;
import com.id4.iprod.exceptions.IprodModuleMasterDaoException;
import com.id4.iprod.exceptions.IprodRoleMasterDaoException;
import com.id4.iprod.exceptions.IprodRoleModuleOperationsDaoException;
import com.id4.iprod.exceptions.IprodViewRoleModuleOperationsDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.interceptors.IprodModuleAuthorization;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class IprodRoleMasterAction extends ActionSupport implements IprodModuleAuthorization{
	
static Logger log = Logger.getLogger(IprodRoleMasterAction.class);
	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_role_master table.
	 */
	protected int roleId;

	/** 
	 * This attribute maps to the column ROLE_NAME in the iprod_role_master table.
	 */
	protected String roleName;
	/** 
	 * This attribute maps to the column ROLE_DESC in the iprod_role_master table.
	 */
	protected String roleDesc;


	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_view_role_module_permissions_details table.
	 */
	protected int moduleId;
	
	protected int userId;
	/** 
	 * This attribute maps to the column MODULE_NAME in the iprod_view_role_module_permissions_details table.
	 */
	protected String moduleName;
	
	protected ArrayList<String> permissions = new ArrayList<String>();
	
	protected ArrayList<String> line = new ArrayList<String>();
	public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();
	
	String[] selectedIndex;

	public List<IprodRoleMaster> allRoleListG = new ArrayList<IprodRoleMaster>();
	public List<IprodViewRoleModuleOperations> iprodViewRoleModuleOperationsListG = new ArrayList<IprodViewRoleModuleOperations>();

	public List<IprodModuleMaster> allRolePermissionsListG = new ArrayList<IprodModuleMaster>();
	
	public List<IprodRoleWisePermissions> roleModulePerms = new ArrayList<IprodRoleWisePermissions>();
	
	public String execute() {
			return SUCCESS;
	}	

	
	public String getAllRoles(){
		try{
			log.info("IprodRoleMasterAction :: getAllRoles");
			Map<String, Object> session = ActionContext.getContext().getSession();
			int roleId = (Integer) session.get(Consts.IPRODUSERROLEID);
			if(session.get(Consts.IPRODUSERID)!= null)
		    {
			  IprodRoleMasterDao roleDao = DaoFactory.createIprodRoleMasterDao();
			  List<IprodRoleMaster> allRoleListL = new ArrayList<IprodRoleMaster>();
			  try {	
					  //allRoleListL = roleDao.findWhereRoleIdNotEquals(1);		
				  allRoleListL = roleDao.findWhereRoleIsDeletedEquals((short)0);
				  	  if(allRoleListL.size() > 0){				  		  				  		  				  			  
						  allRoleListG = allRoleListL;
						  log.info("allRoleListG ::" +allRoleListG);
					  }else{			  	
						  addActionError("Currently there are no roles to be displayed");
					  	  return ERROR;
				  	  }
			  } catch (IprodRoleMasterDaoException e) {
				addActionError("IprodRoleMasterActionException"+e.getMessage());
				return ERROR;
			  }
		    }else{
		    	addActionError("Sorry!! You have to Login first");
		    	return "login";
		    }
		}catch (Exception e){
			log.error("getAllRoles :: Exception :: " + e.getMessage() +" occured in getAllRoles.");
		}
		return SUCCESS;	
	}
	
	
	/*public String addRolePermission(){
		try{
			log.info("IprodRoleMasterAction :: addRolePermission");
			log.info("roleModulePerms are::"+roleModulePerms);
			Map<String, Object> session = ActionContext.getContext().getSession();
			if(session.get(Consts.IPRODUSERID)!= null)
		    {
			//	log.info("NewRoleId" + (Integer)session.get("NewRoleId")) ;
	
			for(int i = 0 ; i < roleModulePerms.size(); i++){				
				if(roleModulePerms.get(i) != null)
				{
				
					if((roleModulePerms.get(i).getRetrievePer()!=1) && ((roleModulePerms.get(i).getCreatePer()==1) || (roleModulePerms.get(i).getCreatePer()==1))) {
						IprodRoleModulePermissionsDao IprodRoleModulePermissionsDao = DaoFactory.createIprodRoleModulePermissionsDao();
						IprodRoleModulePermissions IprodRoleModulePermissionsDto = new IprodRoleModulePermissions();
						IprodRoleModulePermissionsDto.setRoleId((Integer)session.get("NewRoleId"));
						IprodRoleModulePermissionsDto.setModuleId(roleModulePerms.get(i).getModuleId());
						IprodRoleModulePermissionsDto.setCreatePer(roleModulePerms.get(i).getCreatePer());
						IprodRoleModulePermissionsDto.setDeletePer(roleModulePerms.get(i).getDeletePer());
						IprodRoleModulePermissionsDto.setUpdatePer(roleModulePerms.get(i).getUpdatePer());
						IprodRoleModulePermissionsDto.setRetrievePer((short)1);
						IprodRoleModulePermissionsDto.setAllPer(roleModulePerms.get(i).getAllPer());
						IprodRoleModulePermissionsDao.insert(IprodRoleModulePermissionsDto);
					}
					else{
						IprodRoleModulePermissionsDao IprodRoleModulePermissionsDao = DaoFactory.createIprodRoleModulePermissionsDao();
						IprodRoleModulePermissions IprodRoleModulePermissionsDto = new IprodRoleModulePermissions();
						IprodRoleModulePermissionsDto.setRoleId((Integer)session.get("NewRoleId"));
						IprodRoleModulePermissionsDto.setModuleId(roleModulePerms.get(i).getModuleId());
						IprodRoleModulePermissionsDto.setCreatePer(roleModulePerms.get(i).getCreatePer());
						IprodRoleModulePermissionsDto.setDeletePer(roleModulePerms.get(i).getDeletePer());
						IprodRoleModulePermissionsDto.setUpdatePer(roleModulePerms.get(i).getUpdatePer());
						IprodRoleModulePermissionsDto.setRetrievePer(roleModulePerms.get(i).getRetrievePer());
						IprodRoleModulePermissionsDto.setAllPer(roleModulePerms.get(i).getAllPer());
						IprodRoleModulePermissionsDao.insert(IprodRoleModulePermissionsDto);
					}
				}
				
				
			}
		   }
		}catch (Exception e){
			log.info("addRolePermission :: Exception :: " + e.getMessage() +" occured in addRolePermission.");
		}
		return SUCCESS;	
	}
	*/
/*	public String editRolePermissionsDetails(){
		try{
			log.info("IprodRoleMasterAction :: editRolePermissionsDetails");
			Map<String, Object> session = ActionContext.getContext().getSession();
			for(int i = 0 ; i < roleModulePerms.size(); i++){				
				if(roleModulePerms.get(i) != null)
				{
					IprodRoleModulePermissionsDao IprodRoleModulePermissionsDao = DaoFactory.createIprodRoleModulePermissionsDao();
					IprodRoleModulePermissionsPk IprodRoleModulePermissionsPk = new IprodRoleModulePermissionsPk();
					try {
						IprodRoleModulePermissionsDao.updateWhereRoleIdIsAndModuleIdIs(roleModulePerms.get(i).getCreatePer(),roleModulePerms.get(i).getDeletePer(),roleModulePerms.get(i).getUpdatePer(),roleModulePerms.get(i).getRetrievePer(),roleModulePerms.get(i).getAllPer(),roleModulePerms.get(i).getModuleId(),this.roleId);
					} catch (IprodRoleModulePermissionsDaoException e) {
						log.info("editRolePermissionsDetails :: Exception occurs while updating permissions" +e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}catch (Exception e){
			log.info("editRolePermissionsDetails :: Exception :: " + e.getMessage() +" occured in editRolePermissionsDetails.");
		}
		return SUCCESS;	
	}*/
	
	
	public String getAllModules(){
		try{
			log.info("IprodRoleMasterAction :: getAllModules");
			IprodModuleMasterDao iprodModuleMasterDao = DaoFactory.createIprodModuleMasterDao();
			  List<IprodModuleMaster> allRolePermissionsListL = new ArrayList<IprodModuleMaster>();
			  try {	
				  allRolePermissionsListL = iprodModuleMasterDao.findAll();
				  	  if(allRolePermissionsListL.size() > 0){				  		  				  		  				  			  
				  		allRolePermissionsListG = allRolePermissionsListL;
					  }else{			  	
						  addActionError("");
					  	  return ERROR;
				  	  }
				
			  } catch (IprodModuleMasterDaoException e) {
				addActionError("IprodModuleMasterActionException"+e.getMessage());
				return ERROR;
			
			  }
		}catch (Exception e){
			log.error("getAllModules :: Exception :: " + e.getMessage() +" occured in getAllModules.");
		}
		return SUCCESS;	
	}
	
	
	
	/* previous working function
	 
	public String getAllRolePermissionsDetails(){
		try{
			log.info("IprodRoleMasterAction :: getAllRolePermissionsDetails");
			Map<String, Object> session = ActionContext.getContext().getSession();
			IprodViewRoleModulePermissionsDetailsDao iprodViewRoleModulePermissionsDetailsDao = DaoFactory.createIprodViewRoleModulePermissionsDetailsDao();
			List<IprodViewRoleModulePermissionsDetails> iprodViewRoleModulePermissionsDetailsListL = new ArrayList<IprodViewRoleModulePermissionsDetails>();
			if(this.roleId != 0)
			{
			  try {	
				  iprodViewRoleModulePermissionsDetailsListL = iprodViewRoleModulePermissionsDetailsDao.findWhereRoleIdEquals(this.roleId);
			  	  if(iprodViewRoleModulePermissionsDetailsListL.size() > 0){				  		  				  		  				  			  
			  		iprodViewRoleModulePermissionsDetailsListG = iprodViewRoleModulePermissionsDetailsListL;
				  }else{			  	
					  addActionError("");
				  	  return ERROR;
			  	  }
			  } catch (IprodViewRoleModulePermissionsDetailsDaoException e) {
				addActionError("IprodViewRoleModulePermissionsDetailsDaoException"+e.getMessage());
				return ERROR;
			  }
		}
		else if(this.roleId == 0)
		{
			int newRoleId = (Integer)session.get("NewRoleId");

			try {	
				  iprodViewRoleModulePermissionsDetailsListL = iprodViewRoleModulePermissionsDetailsDao.findWhereRoleIdEquals(newRoleId);
			  	  if(iprodViewRoleModulePermissionsDetailsListL.size() > 0){				  		  				  		  				  			  
			  		iprodViewRoleModulePermissionsDetailsListG = iprodViewRoleModulePermissionsDetailsListL;
				  }else{			  	
					  addActionError("");
				  	  return ERROR;
			  	  }
			  } catch (IprodViewRoleModulePermissionsDetailsDaoException e) {
				addActionError("IprodViewRoleModulePermissionsDetailsDaoException"+e.getMessage());
				return ERROR;
			  }
		}
				
		}catch (Exception e){
			log.info("getAllRolePermissionsDetails :: Exception :: " + e.getMessage() +" occured in getAllRolePermissionsDetails.");
		}
		return SUCCESS;	
	}

*/
	
public String roleWisePermissionDetails(){
	try{
		log.info("IprodRoleMasterAction :: roleWisePermissionDetails");
		Map<String, Object> session = ActionContext.getContext().getSession();
		IprodViewRoleModuleOperationsDao iprodViewRoleModuleOperationsDao = DaoFactory.createIprodViewRoleModuleOperationsDao();
		List<IprodViewRoleModuleOperations> iprodViewRoleModuleOperationsListL = new ArrayList<IprodViewRoleModuleOperations>();

		  try {	
			  iprodViewRoleModuleOperationsListL = iprodViewRoleModuleOperationsDao.findWhereRoleIdEquals(this.roleId);
		  	  if(iprodViewRoleModuleOperationsListL.size() > 0){				  		  				  		  				  			  
		  		iprodViewRoleModuleOperationsListG = iprodViewRoleModuleOperationsListL;
		  		log.info("iprodViewRoleModuleOperationsListG :: " +iprodViewRoleModuleOperationsListG);
			  }else{			  	
				  addActionError("");
			  	  return ERROR;
		  	  }
		  	  } catch (IprodViewRoleModuleOperationsDaoException e) {
			    addActionError("IprodViewRoleModuleOperationsDaoException"+e.getMessage());
				return ERROR;
			  }
		}

		catch (Exception e){
			log.error("roleWisePermissionDetails :: Exception :: " + e.getMessage() +" occured in roleWisePermissionDetails.");
			}
			return SUCCESS;	
		}

	
	
	// FUNCTION TO ADD THE ROLE TO TE DATABASE//
	public String addRole() {
		try{
			log.info("IprodRoleMasterAction :: addRole");
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat ("dd MMM yyyy HH:mm:ss:mm");
			String currentDateTime = ft.format(dNow);
			Map<String, Object> session = ActionContext.getContext().getSession();
			if (session.get(Consts.IPRODUSERID) != null) {
				IprodRoleMasterDao roleDetailsDao = DaoFactory.createIprodRoleMasterDao();
				IprodRoleMaster roleDetailsDto = new IprodRoleMaster();
				roleDetailsDto.setRoleName(this.roleName);
				roleDetailsDto.setRoleDesc(this.roleDesc);
				roleDetailsDto.setRoleCdatettime(currentDateTime);
				roleDetailsDto.setRoleMdatettime(currentDateTime);
				roleDetailsDto.setRoleIsDeleted((short) 0);
				IprodRoleMasterPk rolePk = new IprodRoleMasterPk();
				rolePk = roleDetailsDao.insert(roleDetailsDto);
				 session.put("NewRoleId", rolePk.getRoleId());
				//log.info("NewRoleId" + (Integer)session.get("NewRoleId")) ;
				addActionMessage("Role "+this.roleName+" added successfully !");
				
				IprodModuleOperationMasterDao 	iprodModuleOperationMasterDao =  DaoFactory.createIprodModuleOperationMasterDao();
				if(roleModulePerms != null)
				{
					for(int i = 0 ; i < roleModulePerms.size(); i++){
						log.info("roleModulePerms.size()::" +roleModulePerms.size());
						IprodRoleModuleOperationsDao iprodRoleModuleOperationsDao  = DaoFactory.createIprodRoleModuleOperationsDao();
						IprodRoleModuleOperations iprodRoleModuleOperations = new IprodRoleModuleOperations();
						iprodRoleModuleOperations.setRoleId(rolePk.getRoleId());
						iprodRoleModuleOperations.setModuleId(roleModulePerms.get(i).moduleId);
						for(int j=0; j < roleModulePerms.get(i).operation.size(); j++){
							iprodRoleModuleOperations.setOperationId(roleModulePerms.get(i).operation.get(j).operationId);
							iprodRoleModuleOperations.setIsAllowed(roleModulePerms.get(i).operation.get(j).isAllowed);
							
							try {
								iprodRoleModuleOperationsDao.insert(iprodRoleModuleOperations);
							} catch (Exception e) {
								log.error("Exception while inserting permissions" +e.getMessage());
								//e.printStackTrace();
							}
						}
						
					}
				}
				return SUCCESS;
			} else {
				log.error("Error while adding new role : "+this.roleName);
				addActionError("Sorry!! You have to Login first");
				return "login";
			}
		}catch (Exception e){
			log.error("addRole :: Exception :: " + e.getMessage() +" occured in addRole.");
		}
		return SUCCESS;	
	}
	
	public String editRoleModuleOperationsDetails(){
		try{
			log.info("IprodRoleMasterAction :: editRoleModuleOperationsDetails");
			Map<String, Object> session = ActionContext.getContext().getSession();
			log.info("roleModulePerms:: "+roleModulePerms);
			log.info("Role Id Is :: "+this.roleId);
			log.info("roleModulePerms..1:: "+roleModulePerms);

			for(int i = 0 ; i < roleModulePerms.size(); i++){				
				
					IprodRoleModuleOperationsDao IprodRoleModuleOperationsDao = DaoFactory.createIprodRoleModuleOperationsDao();
					IprodRoleModuleOperationsPk IprodRoleModuleOperationsPk = new IprodRoleModuleOperationsPk();
					for(int j=0; j < roleModulePerms.get(i).operation.size(); j++){
						log.info("roleModulePerms..1:: "+roleModulePerms.get(i).operation.get(j).isAllowed);

					try {
						IprodRoleModuleOperationsDao.updateWhereRoleIdIsAndModuleIdAndOperationIdIs(roleModulePerms.get(i).operation.get(j).isAllowed,roleModulePerms.get(i).getModuleId(),roleModulePerms.get(i).operation.get(j).operationId,this.roleId);
						log.info("Updated Successfully");
					} catch (IprodRoleModuleOperationsDaoException e) {
						log.error("editRoleModuleOperationsDetails :: Exception occurs while updating permissions" +e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					}
				
			}
		}catch (Exception e){
			log.error("editRoleModuleOperationsDetails :: Exception :: " + e.getMessage() +" occured in editRoleModuleOperationsDetails.");
		}
		return SUCCESS;	
	}

	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/**
	 * Method 'getRoleName'
	 * 
	 * @return String
	 */
	public String getRoleName()
	{
		return roleName;
	}

	/**
	 * Method 'setRoleName'
	 * 
	 * @param roleName
	 */
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}

	/**
	 * Method 'getRoleDesc'
	 * 
	 * @return String
	 */
	public String getRoleDesc()
	{
		return roleDesc;
	}

	/**
	 * Method 'setRoleDesc'
	 * 
	 * @param roleDesc
	 */
	public void setRoleDesc(String roleDesc)
	{
		this.roleDesc = roleDesc;
	}

	

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getModuleName'
	 * 
	 * @return String
	 */
	public String getModuleName()
	{
		return moduleName;
	}

	/**
	 * Method 'setModuleName'
	 * 
	 * @param moduleName
	 */
	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}
	
	public ArrayList<String> getPermissions() {
		return permissions;
	}

		public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
			return hashMapCheckRoleModuleOperationListG;
		}


		public void setHashMapCheckRoleModuleOperationListG(HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
			this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
		}
}
