package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_scheduler_master table.
 */
public class IprodSchedulerMasterPk implements Serializable
{
	protected int schedularId;

	/** 
	 * This attribute represents whether the primitive attribute schedularId is null.
	 */
	protected boolean schedularIdNull;

	/** 
	 * Sets the value of schedularId
	 */
	public void setSchedularId(int schedularId)
	{
		this.schedularId = schedularId;
	}

	/** 
	 * Gets the value of schedularId
	 */
	public int getSchedularId()
	{
		return schedularId;
	}

	/**
	 * Method 'IprodSchedulerMasterPk'
	 * 
	 */
	public IprodSchedulerMasterPk()
	{
	}

	/**
	 * Method 'IprodSchedulerMasterPk'
	 * 
	 * @param schedularId
	 */
	public IprodSchedulerMasterPk(final int schedularId)
	{
		this.schedularId = schedularId;
	}

	/** 
	 * Sets the value of schedularIdNull
	 */
	public void setSchedularIdNull(boolean schedularIdNull)
	{
		this.schedularIdNull = schedularIdNull;
	}

	/** 
	 * Gets the value of schedularIdNull
	 */
	public boolean isSchedularIdNull()
	{
		return schedularIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodSchedulerMasterPk)) {
			return false;
		}
		
		final IprodSchedulerMasterPk _cast = (IprodSchedulerMasterPk) _other;
		if (schedularId != _cast.schedularId) {
			return false;
		}
		
		if (schedularIdNull != _cast.schedularIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + schedularId;
		_hashCode = 29 * _hashCode + (schedularIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodSchedulerMasterPk: " );
		ret.append( "schedularId=" + schedularId );
		return ret.toString();
	}

}
