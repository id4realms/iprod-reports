package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_operation_master table.
 */
public class IprodOperationMasterPk implements Serializable
{
	protected int operationId;

	/** 
	 * This attribute represents whether the primitive attribute operationId is null.
	 */
	protected boolean operationIdNull;

	/** 
	 * Sets the value of operationId
	 */
	public void setOperationId(int operationId)
	{
		this.operationId = operationId;
	}

	/** 
	 * Gets the value of operationId
	 */
	public int getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'IprodOperationMasterPk'
	 * 
	 */
	public IprodOperationMasterPk()
	{
	}

	/**
	 * Method 'IprodOperationMasterPk'
	 * 
	 * @param operationId
	 */
	public IprodOperationMasterPk(final int operationId)
	{
		this.operationId = operationId;
	}

	/** 
	 * Sets the value of operationIdNull
	 */
	public void setOperationIdNull(boolean operationIdNull)
	{
		this.operationIdNull = operationIdNull;
	}

	/** 
	 * Gets the value of operationIdNull
	 */
	public boolean isOperationIdNull()
	{
		return operationIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodOperationMasterPk)) {
			return false;
		}
		
		final IprodOperationMasterPk _cast = (IprodOperationMasterPk) _other;
		if (operationId != _cast.operationId) {
			return false;
		}
		
		if (operationIdNull != _cast.operationIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + operationId;
		_hashCode = 29 * _hashCode + (operationIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodOperationMasterPk: " );
		ret.append( "operationId=" + operationId );
		return ret.toString();
	}

}
