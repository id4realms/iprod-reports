package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodUserMaster implements Serializable
{
	/** 
	 * This attribute maps to the column USER_ID in the iprod_user_master table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column USER_NAME in the iprod_user_master table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column USER_TITLE in the iprod_user_master table.
	 */
	protected String userTitle;

	/** 
	 * This attribute maps to the column FIRST_NAME in the iprod_user_master table.
	 */
	protected String firstName;

	/** 
	 * This attribute maps to the column LAST_NAME in the iprod_user_master table.
	 */
	protected String lastName;

	/** 
	 * This attribute maps to the column GENDER in the iprod_user_master table.
	 */
	protected String gender;

	/** 
	 * This attribute maps to the column EMAIL_ID in the iprod_user_master table.
	 */
	protected String emailId;

	/** 
	 * This attribute maps to the column CONTACT_NO in the iprod_user_master table.
	 */
	protected String contactNo;

	/** 
	 * This attribute maps to the column USER_PASSWORD in the iprod_user_master table.
	 */
	protected String userPassword;

	/** 
	 * This attribute maps to the column LAST_LOGIN_DETAILS in the iprod_user_master table.
	 */
	protected String lastLoginDetails;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_user_master table.
	 */
	protected short isDeleted;

	/**
	 * Method 'IprodUserMaster'
	 * 
	 */
	public IprodUserMaster()
	{
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getUserName'
	 * 
	 * @return String
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * Method 'setUserName'
	 * 
	 * @param userName
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * Method 'getUserTitle'
	 * 
	 * @return String
	 */
	public String getUserTitle()
	{
		return userTitle;
	}

	/**
	 * Method 'setUserTitle'
	 * 
	 * @param userTitle
	 */
	public void setUserTitle(String userTitle)
	{
		this.userTitle = userTitle;
	}

	/**
	 * Method 'getFirstName'
	 * 
	 * @return String
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Method 'setFirstName'
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Method 'getLastName'
	 * 
	 * @return String
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Method 'setLastName'
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Method 'getGender'
	 * 
	 * @return String
	 */
	public String getGender()
	{
		return gender;
	}

	/**
	 * Method 'setGender'
	 * 
	 * @param gender
	 */
	public void setGender(String gender)
	{
		this.gender = gender;
	}

	/**
	 * Method 'getEmailId'
	 * 
	 * @return String
	 */
	public String getEmailId()
	{
		return emailId;
	}

	/**
	 * Method 'setEmailId'
	 * 
	 * @param emailId
	 */
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	/**
	 * Method 'getContactNo'
	 * 
	 * @return String
	 */
	public String getContactNo()
	{
		return contactNo;
	}

	/**
	 * Method 'setContactNo'
	 * 
	 * @param contactNo
	 */
	public void setContactNo(String contactNo)
	{
		this.contactNo = contactNo;
	}

	/**
	 * Method 'getUserPassword'
	 * 
	 * @return String
	 */
	public String getUserPassword()
	{
		return userPassword;
	}

	/**
	 * Method 'setUserPassword'
	 * 
	 * @param userPassword
	 */
	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}

	/**
	 * Method 'getLastLoginDetails'
	 * 
	 * @return String
	 */
	public String getLastLoginDetails()
	{
		return lastLoginDetails;
	}

	/**
	 * Method 'setLastLoginDetails'
	 * 
	 * @param lastLoginDetails
	 */
	public void setLastLoginDetails(String lastLoginDetails)
	{
		this.lastLoginDetails = lastLoginDetails;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodUserMaster)) {
			return false;
		}
		
		final IprodUserMaster _cast = (IprodUserMaster) _other;
		if (userId != _cast.userId) {
			return false;
		}
		
		if (userName == null ? _cast.userName != userName : !userName.equals( _cast.userName )) {
			return false;
		}
		
		if (userTitle == null ? _cast.userTitle != userTitle : !userTitle.equals( _cast.userTitle )) {
			return false;
		}
		
		if (firstName == null ? _cast.firstName != firstName : !firstName.equals( _cast.firstName )) {
			return false;
		}
		
		if (lastName == null ? _cast.lastName != lastName : !lastName.equals( _cast.lastName )) {
			return false;
		}
		
		if (gender == null ? _cast.gender != gender : !gender.equals( _cast.gender )) {
			return false;
		}
		
		if (emailId == null ? _cast.emailId != emailId : !emailId.equals( _cast.emailId )) {
			return false;
		}
		
		if (contactNo == null ? _cast.contactNo != contactNo : !contactNo.equals( _cast.contactNo )) {
			return false;
		}
		
		if (userPassword == null ? _cast.userPassword != userPassword : !userPassword.equals( _cast.userPassword )) {
			return false;
		}
		
		if (lastLoginDetails == null ? _cast.lastLoginDetails != lastLoginDetails : !lastLoginDetails.equals( _cast.lastLoginDetails )) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + userId;
		if (userName != null) {
			_hashCode = 29 * _hashCode + userName.hashCode();
		}
		
		if (userTitle != null) {
			_hashCode = 29 * _hashCode + userTitle.hashCode();
		}
		
		if (firstName != null) {
			_hashCode = 29 * _hashCode + firstName.hashCode();
		}
		
		if (lastName != null) {
			_hashCode = 29 * _hashCode + lastName.hashCode();
		}
		
		if (gender != null) {
			_hashCode = 29 * _hashCode + gender.hashCode();
		}
		
		if (emailId != null) {
			_hashCode = 29 * _hashCode + emailId.hashCode();
		}
		
		if (contactNo != null) {
			_hashCode = 29 * _hashCode + contactNo.hashCode();
		}
		
		if (userPassword != null) {
			_hashCode = 29 * _hashCode + userPassword.hashCode();
		}
		
		if (lastLoginDetails != null) {
			_hashCode = 29 * _hashCode + lastLoginDetails.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) isDeleted;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodUserMasterPk
	 */
	public IprodUserMasterPk createPk()
	{
		return new IprodUserMasterPk(userId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodUserMaster: " );
		ret.append( "userId=" + userId );
		ret.append( ", userName=" + userName );
		ret.append( ", userTitle=" + userTitle );
		ret.append( ", firstName=" + firstName );
		ret.append( ", lastName=" + lastName );
		ret.append( ", gender=" + gender );
		ret.append( ", emailId=" + emailId );
		ret.append( ", contactNo=" + contactNo );
		ret.append( ", userPassword=" + userPassword );
		ret.append( ", lastLoginDetails=" + lastLoginDetails );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

}
