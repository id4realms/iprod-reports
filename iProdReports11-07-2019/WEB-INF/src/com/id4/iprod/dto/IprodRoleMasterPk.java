package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_role_master table.
 */
public class IprodRoleMasterPk implements Serializable
{
	protected int roleId;

	/** 
	 * This attribute represents whether the primitive attribute roleId is null.
	 */
	protected boolean roleIdNull;

	/** 
	 * Sets the value of roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/** 
	 * Gets the value of roleId
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'IprodRoleMasterPk'
	 * 
	 */
	public IprodRoleMasterPk()
	{
	}

	/**
	 * Method 'IprodRoleMasterPk'
	 * 
	 * @param roleId
	 */
	public IprodRoleMasterPk(final int roleId)
	{
		this.roleId = roleId;
	}

	/** 
	 * Sets the value of roleIdNull
	 */
	public void setRoleIdNull(boolean roleIdNull)
	{
		this.roleIdNull = roleIdNull;
	}

	/** 
	 * Gets the value of roleIdNull
	 */
	public boolean isRoleIdNull()
	{
		return roleIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodRoleMasterPk)) {
			return false;
		}
		
		final IprodRoleMasterPk _cast = (IprodRoleMasterPk) _other;
		if (roleId != _cast.roleId) {
			return false;
		}
		
		if (roleIdNull != _cast.roleIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + roleId;
		_hashCode = 29 * _hashCode + (roleIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodRoleMasterPk: " );
		ret.append( "roleId=" + roleId );
		return ret.toString();
	}

}
