package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_sequence_master table.
 */
public class IprodSequenceMasterPk implements Serializable
{
	protected int sequenceId;

	/** 
	 * This attribute represents whether the primitive attribute sequenceId is null.
	 */
	protected boolean sequenceIdNull;

	/** 
	 * Sets the value of sequenceId
	 */
	public void setSequenceId(int sequenceId)
	{
		this.sequenceId = sequenceId;
	}

	/** 
	 * Gets the value of sequenceId
	 */
	public int getSequenceId()
	{
		return sequenceId;
	}

	/**
	 * Method 'IprodSequenceMasterPk'
	 * 
	 */
	public IprodSequenceMasterPk()
	{
	}

	/**
	 * Method 'IprodSequenceMasterPk'
	 * 
	 * @param sequenceId
	 */
	public IprodSequenceMasterPk(final int sequenceId)
	{
		this.sequenceId = sequenceId;
	}

	/** 
	 * Sets the value of sequenceIdNull
	 */
	public void setSequenceIdNull(boolean sequenceIdNull)
	{
		this.sequenceIdNull = sequenceIdNull;
	}

	/** 
	 * Gets the value of sequenceIdNull
	 */
	public boolean isSequenceIdNull()
	{
		return sequenceIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodSequenceMasterPk)) {
			return false;
		}
		
		final IprodSequenceMasterPk _cast = (IprodSequenceMasterPk) _other;
		if (sequenceId != _cast.sequenceId) {
			return false;
		}
		
		if (sequenceIdNull != _cast.sequenceIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + sequenceId;
		_hashCode = 29 * _hashCode + (sequenceIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodSequenceMasterPk: " );
		ret.append( "sequenceId=" + sequenceId );
		return ret.toString();
	}

}
