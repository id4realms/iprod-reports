package com.id4.iprod.dto;

import java.io.Serializable;

import com.id4.iprod.dto.IprodOperationLoggerHistoryPk;

public class IprodOperationLoggerHistory implements Serializable
{
	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_operation_logger_history table.
	 */
	protected int operationId;

	/** 
	 * This attribute maps to the column OPERATION_PAGE_SOURCE in the iprod_operation_logger_history table.
	 */
	protected String operationPageSource;

	/** 
	 * This attribute maps to the column COMMENTS in the iprod_operation_logger_history table.
	 */
	protected String comments;

	/** 
	 * This attribute maps to the column USER_ID in the iprod_operation_logger_history table.
	 */
	protected int userId;

	/** 
	 * This attribute represents whether the primitive attribute userId is null.
	 */
	protected boolean userIdNull = true;

	/** 
	 * This attribute maps to the column USERNAME in the iprod_operation_logger_history table.
	 */
	protected String username;

	/** 
	 * This attribute maps to the column OPERATION_LOGGER_CDATETIME in the iprod_operation_logger_history table.
	 */
	protected String operationLoggerCdatetime;

	/** 
	 * This attribute maps to the column OPERATION_LOGGER_MDATETIME in the iprod_operation_logger_history table.
	 */
	protected String operationLoggerMdatetime;

	/**
	 * Method 'IprodOperationLoggerHistory'
	 * 
	 */
	public IprodOperationLoggerHistory()
	{
	}

	/**
	 * Method 'getOperationId'
	 * 
	 * @return int
	 */
	public int getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'setOperationId'
	 * 
	 * @param operationId
	 */
	public void setOperationId(int operationId)
	{
		this.operationId = operationId;
	}

	/**
	 * Method 'getOperationPageSource'
	 * 
	 * @return String
	 */
	public String getOperationPageSource()
	{
		return operationPageSource;
	}

	/**
	 * Method 'setOperationPageSource'
	 * 
	 * @param operationPageSource
	 */
	public void setOperationPageSource(String operationPageSource)
	{
		this.operationPageSource = operationPageSource;
	}

	/**
	 * Method 'getComments'
	 * 
	 * @return String
	 */
	public String getComments()
	{
		return comments;
	}

	/**
	 * Method 'setComments'
	 * 
	 * @param comments
	 */
	public void setComments(String comments)
	{
		this.comments = comments;
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
		this.userIdNull = false;
	}

	/**
	 * Method 'setUserIdNull'
	 * 
	 * @param value
	 */
	public void setUserIdNull(boolean value)
	{
		this.userIdNull = value;
	}

	/**
	 * Method 'isUserIdNull'
	 * 
	 * @return boolean
	 */
	public boolean isUserIdNull()
	{
		return userIdNull;
	}

	/**
	 * Method 'getUsername'
	 * 
	 * @return String
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * Method 'setUsername'
	 * 
	 * @param username
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * Method 'getOperationLoggerCdatetime'
	 * 
	 * @return String
	 */
	public String getOperationLoggerCdatetime()
	{
		return operationLoggerCdatetime;
	}

	/**
	 * Method 'setOperationLoggerCdatetime'
	 * 
	 * @param operationLoggerCdatetime
	 */
	public void setOperationLoggerCdatetime(String operationLoggerCdatetime)
	{
		this.operationLoggerCdatetime = operationLoggerCdatetime;
	}

	/**
	 * Method 'getOperationLoggerMdatetime'
	 * 
	 * @return String
	 */
	public String getOperationLoggerMdatetime()
	{
		return operationLoggerMdatetime;
	}

	/**
	 * Method 'setOperationLoggerMdatetime'
	 * 
	 * @param operationLoggerMdatetime
	 */
	public void setOperationLoggerMdatetime(String operationLoggerMdatetime)
	{
		this.operationLoggerMdatetime = operationLoggerMdatetime;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodOperationLoggerHistory)) {
			return false;
		}
		
		final IprodOperationLoggerHistory _cast = (IprodOperationLoggerHistory) _other;
		if (operationId != _cast.operationId) {
			return false;
		}
		
		if (operationPageSource == null ? _cast.operationPageSource != operationPageSource : !operationPageSource.equals( _cast.operationPageSource )) {
			return false;
		}
		
		if (comments == null ? _cast.comments != comments : !comments.equals( _cast.comments )) {
			return false;
		}
		
		if (userId != _cast.userId) {
			return false;
		}
		
		if (userIdNull != _cast.userIdNull) {
			return false;
		}
		
		if (username == null ? _cast.username != username : !username.equals( _cast.username )) {
			return false;
		}
		
		if (operationLoggerCdatetime == null ? _cast.operationLoggerCdatetime != operationLoggerCdatetime : !operationLoggerCdatetime.equals( _cast.operationLoggerCdatetime )) {
			return false;
		}
		
		if (operationLoggerMdatetime == null ? _cast.operationLoggerMdatetime != operationLoggerMdatetime : !operationLoggerMdatetime.equals( _cast.operationLoggerMdatetime )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + operationId;
		if (operationPageSource != null) {
			_hashCode = 29 * _hashCode + operationPageSource.hashCode();
		}
		
		if (comments != null) {
			_hashCode = 29 * _hashCode + comments.hashCode();
		}
		
		_hashCode = 29 * _hashCode + userId;
		_hashCode = 29 * _hashCode + (userIdNull ? 1 : 0);
		if (username != null) {
			_hashCode = 29 * _hashCode + username.hashCode();
		}
		
		if (operationLoggerCdatetime != null) {
			_hashCode = 29 * _hashCode + operationLoggerCdatetime.hashCode();
		}
		
		if (operationLoggerMdatetime != null) {
			_hashCode = 29 * _hashCode + operationLoggerMdatetime.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodOperationLoggerHistoryPk
	 */
	public IprodOperationLoggerHistoryPk createPk()
	{
		return new IprodOperationLoggerHistoryPk(operationId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.d4.iprod.dto.IprodOperationLoggerHistory: " );
		ret.append( "operationId=" + operationId );
		ret.append( ", operationPageSource=" + operationPageSource );
		ret.append( ", comments=" + comments );
		ret.append( ", userId=" + userId );
		ret.append( ", username=" + username );
		ret.append( ", operationLoggerCdatetime=" + operationLoggerCdatetime );
		ret.append( ", operationLoggerMdatetime=" + operationLoggerMdatetime );
		return ret.toString();
	}

}
