package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_user_role table.
 */
public class IprodUserRolePk implements Serializable
{
	protected int userId;

	protected int roleId;

	/** 
	 * This attribute represents whether the primitive attribute userId is null.
	 */
	protected boolean userIdNull;

	/** 
	 * This attribute represents whether the primitive attribute roleId is null.
	 */
	protected boolean roleIdNull;

	/** 
	 * Sets the value of userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/** 
	 * Gets the value of userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/** 
	 * Sets the value of roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/** 
	 * Gets the value of roleId
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'IprodUserRolePk'
	 * 
	 */
	public IprodUserRolePk()
	{
	}

	/**
	 * Method 'IprodUserRolePk'
	 * 
	 * @param userId
	 * @param roleId
	 */
	public IprodUserRolePk(final int userId, final int roleId)
	{
		this.userId = userId;
		this.roleId = roleId;
	}

	/** 
	 * Sets the value of userIdNull
	 */
	public void setUserIdNull(boolean userIdNull)
	{
		this.userIdNull = userIdNull;
	}

	/** 
	 * Gets the value of userIdNull
	 */
	public boolean isUserIdNull()
	{
		return userIdNull;
	}

	/** 
	 * Sets the value of roleIdNull
	 */
	public void setRoleIdNull(boolean roleIdNull)
	{
		this.roleIdNull = roleIdNull;
	}

	/** 
	 * Gets the value of roleIdNull
	 */
	public boolean isRoleIdNull()
	{
		return roleIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodUserRolePk)) {
			return false;
		}
		
		final IprodUserRolePk _cast = (IprodUserRolePk) _other;
		if (userId != _cast.userId) {
			return false;
		}
		
		if (roleId != _cast.roleId) {
			return false;
		}
		
		if (userIdNull != _cast.userIdNull) {
			return false;
		}
		
		if (roleIdNull != _cast.roleIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + userId;
		_hashCode = 29 * _hashCode + roleId;
		_hashCode = 29 * _hashCode + (userIdNull ? 1 : 0);
		_hashCode = 29 * _hashCode + (roleIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodUserRolePk: " );
		ret.append( "userId=" + userId );
		ret.append( ", roleId=" + roleId );
		return ret.toString();
	}

}
