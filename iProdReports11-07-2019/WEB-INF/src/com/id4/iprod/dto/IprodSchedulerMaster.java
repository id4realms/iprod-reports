package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodSchedulerMaster implements Serializable
{
	/** 
	 * This attribute maps to the column SCHEDULAR_ID in the iprod_scheduler_master table.
	 */
	protected int schedularId;

	/** 
	 * This attribute maps to the column SCHEDULAR_NAME in the iprod_scheduler_master table.
	 */
	protected String schedularName;

	/** 
	 * This attribute maps to the column TRIGGER_NAME in the iprod_scheduler_master table.
	 */
	protected String triggerName;

	/** 
	 * This attribute maps to the column SCHEDULAR_STATUS in the iprod_scheduler_master table.
	 */
	protected String schedularStatus;

	/**
	 * Method 'IprodSchedulerMaster'
	 * 
	 */
	public IprodSchedulerMaster()
	{
	}

	/**
	 * Method 'getSchedularId'
	 * 
	 * @return int
	 */
	public int getSchedularId()
	{
		return schedularId;
	}

	/**
	 * Method 'setSchedularId'
	 * 
	 * @param schedularId
	 */
	public void setSchedularId(int schedularId)
	{
		this.schedularId = schedularId;
	}

	/**
	 * Method 'getSchedularName'
	 * 
	 * @return String
	 */
	public String getSchedularName()
	{
		return schedularName;
	}

	/**
	 * Method 'setSchedularName'
	 * 
	 * @param schedularName
	 */
	public void setSchedularName(String schedularName)
	{
		this.schedularName = schedularName;
	}

	/**
	 * Method 'getTriggerName'
	 * 
	 * @return String
	 */
	public String getTriggerName()
	{
		return triggerName;
	}

	/**
	 * Method 'setTriggerName'
	 * 
	 * @param triggerName
	 */
	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	/**
	 * Method 'getSchedularStatus'
	 * 
	 * @return String
	 */
	public String getSchedularStatus()
	{
		return schedularStatus;
	}

	/**
	 * Method 'setSchedularStatus'
	 * 
	 * @param schedularStatus
	 */
	public void setSchedularStatus(String schedularStatus)
	{
		this.schedularStatus = schedularStatus;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodSchedulerMaster)) {
			return false;
		}
		
		final IprodSchedulerMaster _cast = (IprodSchedulerMaster) _other;
		if (schedularId != _cast.schedularId) {
			return false;
		}
		
		if (schedularName == null ? _cast.schedularName != schedularName : !schedularName.equals( _cast.schedularName )) {
			return false;
		}
		
		if (triggerName == null ? _cast.triggerName != triggerName : !triggerName.equals( _cast.triggerName )) {
			return false;
		}
		
		if (schedularStatus == null ? _cast.schedularStatus != schedularStatus : !schedularStatus.equals( _cast.schedularStatus )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + schedularId;
		if (schedularName != null) {
			_hashCode = 29 * _hashCode + schedularName.hashCode();
		}
		
		if (triggerName != null) {
			_hashCode = 29 * _hashCode + triggerName.hashCode();
		}
		
		if (schedularStatus != null) {
			_hashCode = 29 * _hashCode + schedularStatus.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodSchedulerMasterPk
	 */
	public IprodSchedulerMasterPk createPk()
	{
		return new IprodSchedulerMasterPk(schedularId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodSchedulerMaster: " );
		ret.append( "schedularId=" + schedularId );
		ret.append( ", schedularName=" + schedularName );
		ret.append( ", triggerName=" + triggerName );
		ret.append( ", schedularStatus=" + schedularStatus );
		return ret.toString();
	}

}
