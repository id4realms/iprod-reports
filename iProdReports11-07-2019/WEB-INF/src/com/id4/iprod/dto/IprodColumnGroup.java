package com.id4.iprod.dto;

public class IprodColumnGroup {
	private String columnName = null;
	private String columnType = null;
	private String columnAlias = null;
	private Boolean columnSummation = false;
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public String getColumnAlias() {
		return columnAlias;
	}
	public void setColumnAlias(String columnAlias) {
		this.columnAlias = columnAlias;
	}
	public Boolean getColumnSummation() {
		return columnSummation;
	}
	public void setColumnSummation(Boolean columnSummation) {
		this.columnSummation = columnSummation;
	}
	

}
