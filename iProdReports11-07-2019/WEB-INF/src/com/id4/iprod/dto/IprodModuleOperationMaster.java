package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodModuleOperationMaster implements Serializable
{
	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_module_operation_master table.
	 */
	protected Integer moduleId;

	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_module_operation_master table.
	 */
	protected Integer operationId;

	/** 
	 * This attribute maps to the column IS_APPLICABLE in the iprod_module_operation_master table.
	 */
	protected Short isApplicable;

	/**
	 * Method 'IprodModuleOperationMaster'
	 * 
	 */
	public IprodModuleOperationMaster()
	{
	}

	/**
	 * Method 'getModuleId'
	 * 
	 * @return Integer
	 */
	public Integer getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'setModuleId'
	 * 
	 * @param moduleId
	 */
	public void setModuleId(Integer moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getOperationId'
	 * 
	 * @return Integer
	 */
	public Integer getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'setOperationId'
	 * 
	 * @param operationId
	 */
	public void setOperationId(Integer operationId)
	{
		this.operationId = operationId;
	}

	/**
	 * Method 'getIsApplicable'
	 * 
	 * @return Short
	 */
	public Short getIsApplicable()
	{
		return isApplicable;
	}

	/**
	 * Method 'setIsApplicable'
	 * 
	 * @param isApplicable
	 */
	public void setIsApplicable(Short isApplicable)
	{
		this.isApplicable = isApplicable;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodModuleOperationMaster)) {
			return false;
		}
		
		final IprodModuleOperationMaster _cast = (IprodModuleOperationMaster) _other;
		if (moduleId == null ? _cast.moduleId != moduleId : !moduleId.equals( _cast.moduleId )) {
			return false;
		}
		
		if (operationId == null ? _cast.operationId != operationId : !operationId.equals( _cast.operationId )) {
			return false;
		}
		
		if (isApplicable == null ? _cast.isApplicable != isApplicable : !isApplicable.equals( _cast.isApplicable )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (moduleId != null) {
			_hashCode = 29 * _hashCode + moduleId.hashCode();
		}
		
		if (operationId != null) {
			_hashCode = 29 * _hashCode + operationId.hashCode();
		}
		
		if (isApplicable != null) {
			_hashCode = 29 * _hashCode + isApplicable.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodModuleOperationMasterPk
	 */
	public IprodModuleOperationMasterPk createPk()
	{
		return new IprodModuleOperationMasterPk(moduleId, operationId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodModuleOperationMaster: " );
		ret.append( "moduleId=" + moduleId );
		ret.append( ", operationId=" + operationId );
		ret.append( ", isApplicable=" + isApplicable );
		return ret.toString();
	}

}
