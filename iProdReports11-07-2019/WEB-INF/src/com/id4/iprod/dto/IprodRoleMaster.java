package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodRoleMaster implements Serializable
{
	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_role_master table.
	 */
	protected int roleId;

	/** 
	 * This attribute maps to the column ROLE_NAME in the iprod_role_master table.
	 */
	protected String roleName;

	/** 
	 * This attribute maps to the column ROLE_DESC in the iprod_role_master table.
	 */
	protected String roleDesc;

	/** 
	 * This attribute maps to the column ROLE_IS_DELETED in the iprod_role_master table.
	 */
	protected short roleIsDeleted;

	/** 
	 * This attribute represents whether the primitive attribute roleIsDeleted is null.
	 */
	protected boolean roleIsDeletedNull = true;

	/** 
	 * This attribute maps to the column ROLE_CDATETTIME in the iprod_role_master table.
	 */
	protected String roleCdatettime;

	/** 
	 * This attribute maps to the column ROLE_MDATETTIME in the iprod_role_master table.
	 */
	protected String roleMdatettime;

	/**
	 * Method 'IprodRoleMaster'
	 * 
	 */
	public IprodRoleMaster()
	{
	}

	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/**
	 * Method 'getRoleName'
	 * 
	 * @return String
	 */
	public String getRoleName()
	{
		return roleName;
	}

	/**
	 * Method 'setRoleName'
	 * 
	 * @param roleName
	 */
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}

	/**
	 * Method 'getRoleDesc'
	 * 
	 * @return String
	 */
	public String getRoleDesc()
	{
		return roleDesc;
	}

	/**
	 * Method 'setRoleDesc'
	 * 
	 * @param roleDesc
	 */
	public void setRoleDesc(String roleDesc)
	{
		this.roleDesc = roleDesc;
	}

	/**
	 * Method 'getRoleIsDeleted'
	 * 
	 * @return short
	 */
	public short getRoleIsDeleted()
	{
		return roleIsDeleted;
	}

	/**
	 * Method 'setRoleIsDeleted'
	 * 
	 * @param roleIsDeleted
	 */
	public void setRoleIsDeleted(short roleIsDeleted)
	{
		this.roleIsDeleted = roleIsDeleted;
		this.roleIsDeletedNull = false;
	}

	/**
	 * Method 'setRoleIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setRoleIsDeletedNull(boolean value)
	{
		this.roleIsDeletedNull = value;
	}

	/**
	 * Method 'isRoleIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isRoleIsDeletedNull()
	{
		return roleIsDeletedNull;
	}

	/**
	 * Method 'getRoleCdatettime'
	 * 
	 * @return String
	 */
	public String getRoleCdatettime()
	{
		return roleCdatettime;
	}

	/**
	 * Method 'setRoleCdatettime'
	 * 
	 * @param roleCdatettime
	 */
	public void setRoleCdatettime(String roleCdatettime)
	{
		this.roleCdatettime = roleCdatettime;
	}

	/**
	 * Method 'getRoleMdatettime'
	 * 
	 * @return String
	 */
	public String getRoleMdatettime()
	{
		return roleMdatettime;
	}

	/**
	 * Method 'setRoleMdatettime'
	 * 
	 * @param roleMdatettime
	 */
	public void setRoleMdatettime(String roleMdatettime)
	{
		this.roleMdatettime = roleMdatettime;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodRoleMaster)) {
			return false;
		}
		
		final IprodRoleMaster _cast = (IprodRoleMaster) _other;
		if (roleId != _cast.roleId) {
			return false;
		}
		
		if (roleName == null ? _cast.roleName != roleName : !roleName.equals( _cast.roleName )) {
			return false;
		}
		
		if (roleDesc == null ? _cast.roleDesc != roleDesc : !roleDesc.equals( _cast.roleDesc )) {
			return false;
		}
		
		if (roleIsDeleted != _cast.roleIsDeleted) {
			return false;
		}
		
		if (roleIsDeletedNull != _cast.roleIsDeletedNull) {
			return false;
		}
		
		if (roleCdatettime == null ? _cast.roleCdatettime != roleCdatettime : !roleCdatettime.equals( _cast.roleCdatettime )) {
			return false;
		}
		
		if (roleMdatettime == null ? _cast.roleMdatettime != roleMdatettime : !roleMdatettime.equals( _cast.roleMdatettime )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + roleId;
		if (roleName != null) {
			_hashCode = 29 * _hashCode + roleName.hashCode();
		}
		
		if (roleDesc != null) {
			_hashCode = 29 * _hashCode + roleDesc.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) roleIsDeleted;
		_hashCode = 29 * _hashCode + (roleIsDeletedNull ? 1 : 0);
		if (roleCdatettime != null) {
			_hashCode = 29 * _hashCode + roleCdatettime.hashCode();
		}
		
		if (roleMdatettime != null) {
			_hashCode = 29 * _hashCode + roleMdatettime.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodRoleMasterPk
	 */
	public IprodRoleMasterPk createPk()
	{
		return new IprodRoleMasterPk(roleId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodRoleMaster: " );
		ret.append( "roleId=" + roleId );
		ret.append( ", roleName=" + roleName );
		ret.append( ", roleDesc=" + roleDesc );
		ret.append( ", roleIsDeleted=" + roleIsDeleted );
		ret.append( ", roleCdatettime=" + roleCdatettime );
		ret.append( ", roleMdatettime=" + roleMdatettime );
		return ret.toString();
	}

}
