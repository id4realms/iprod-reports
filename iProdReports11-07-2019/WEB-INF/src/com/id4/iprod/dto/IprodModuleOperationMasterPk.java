package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_module_operation_master table.
 */
public class IprodModuleOperationMasterPk implements Serializable
{
	protected Integer moduleId;

	protected Integer operationId;

	/** 
	 * Sets the value of moduleId
	 */
	public void setModuleId(Integer moduleId)
	{
		this.moduleId = moduleId;
	}

	/** 
	 * Gets the value of moduleId
	 */
	public Integer getModuleId()
	{
		return moduleId;
	}

	/** 
	 * Sets the value of operationId
	 */
	public void setOperationId(Integer operationId)
	{
		this.operationId = operationId;
	}

	/** 
	 * Gets the value of operationId
	 */
	public Integer getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'IprodModuleOperationMasterPk'
	 * 
	 */
	public IprodModuleOperationMasterPk()
	{
	}

	/**
	 * Method 'IprodModuleOperationMasterPk'
	 * 
	 * @param moduleId
	 * @param operationId
	 */
	public IprodModuleOperationMasterPk(final Integer moduleId, final Integer operationId)
	{
		this.moduleId = moduleId;
		this.operationId = operationId;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodModuleOperationMasterPk)) {
			return false;
		}
		
		final IprodModuleOperationMasterPk _cast = (IprodModuleOperationMasterPk) _other;
		if (moduleId == null ? _cast.moduleId != moduleId : !moduleId.equals( _cast.moduleId )) {
			return false;
		}
		
		if (operationId == null ? _cast.operationId != operationId : !operationId.equals( _cast.operationId )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (moduleId != null) {
			_hashCode = 29 * _hashCode + moduleId.hashCode();
		}
		
		if (operationId != null) {
			_hashCode = 29 * _hashCode + operationId.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodModuleOperationMasterPk: " );
		ret.append( "moduleId=" + moduleId );
		ret.append( ", operationId=" + operationId );
		return ret.toString();
	}

}
