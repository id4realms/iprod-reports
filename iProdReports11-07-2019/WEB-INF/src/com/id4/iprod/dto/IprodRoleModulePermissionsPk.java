package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_role_module_permissions table.
 */
public class IprodRoleModulePermissionsPk implements Serializable
{
	protected int roleId;

	protected int moduleId;

	/** 
	 * This attribute represents whether the primitive attribute roleId is null.
	 */
	protected boolean roleIdNull;

	/** 
	 * This attribute represents whether the primitive attribute moduleId is null.
	 */
	protected boolean moduleIdNull;

	/** 
	 * Sets the value of roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/** 
	 * Gets the value of roleId
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/** 
	 * Sets the value of moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/** 
	 * Gets the value of moduleId
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'IprodRoleModulePermissionsPk'
	 * 
	 */
	public IprodRoleModulePermissionsPk()
	{
	}

	/**
	 * Method 'IprodRoleModulePermissionsPk'
	 * 
	 * @param roleId
	 * @param moduleId
	 */
	public IprodRoleModulePermissionsPk(final int roleId, final int moduleId)
	{
		this.roleId = roleId;
		this.moduleId = moduleId;
	}

	/** 
	 * Sets the value of roleIdNull
	 */
	public void setRoleIdNull(boolean roleIdNull)
	{
		this.roleIdNull = roleIdNull;
	}

	/** 
	 * Gets the value of roleIdNull
	 */
	public boolean isRoleIdNull()
	{
		return roleIdNull;
	}

	/** 
	 * Sets the value of moduleIdNull
	 */
	public void setModuleIdNull(boolean moduleIdNull)
	{
		this.moduleIdNull = moduleIdNull;
	}

	/** 
	 * Gets the value of moduleIdNull
	 */
	public boolean isModuleIdNull()
	{
		return moduleIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodRoleModulePermissionsPk)) {
			return false;
		}
		
		final IprodRoleModulePermissionsPk _cast = (IprodRoleModulePermissionsPk) _other;
		if (roleId != _cast.roleId) {
			return false;
		}
		
		if (moduleId != _cast.moduleId) {
			return false;
		}
		
		if (roleIdNull != _cast.roleIdNull) {
			return false;
		}
		
		if (moduleIdNull != _cast.moduleIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + roleId;
		_hashCode = 29 * _hashCode + moduleId;
		_hashCode = 29 * _hashCode + (roleIdNull ? 1 : 0);
		_hashCode = 29 * _hashCode + (moduleIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodRoleModulePermissionsPk: " );
		ret.append( "roleId=" + roleId );
		ret.append( ", moduleId=" + moduleId );
		return ret.toString();
	}

}
