package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodRoleModulePermissions implements Serializable
{
	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_role_module_permissions table.
	 */
	protected int roleId;

	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_role_module_permissions table.
	 */
	protected int moduleId;

	/** 
	 * This attribute maps to the column CREATE_PER in the iprod_role_module_permissions table.
	 */
	protected short createPer;

	/** 
	 * This attribute maps to the column RETRIEVE_PER in the iprod_role_module_permissions table.
	 */
	protected short retrievePer;

	/** 
	 * This attribute maps to the column UPDATE_PER in the iprod_role_module_permissions table.
	 */
	protected short updatePer;

	/** 
	 * This attribute maps to the column DELETE_PER in the iprod_role_module_permissions table.
	 */
	protected short deletePer;

	/** 
	 * This attribute maps to the column ALL_PER in the iprod_role_module_permissions table.
	 */
	protected short allPer;

	/** 
	 * This attribute maps to the column PERMISSIONS_IS_DELETED in the iprod_role_module_permissions table.
	 */
	protected short permissionsIsDeleted;

	/** 
	 * This attribute represents whether the primitive attribute permissionsIsDeleted is null.
	 */
	protected boolean permissionsIsDeletedNull = true;

	/**
	 * Method 'IprodRoleModulePermissions'
	 * 
	 */
	public IprodRoleModulePermissions()
	{
	}

	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/**
	 * Method 'getModuleId'
	 * 
	 * @return int
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'setModuleId'
	 * 
	 * @param moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getCreatePer'
	 * 
	 * @return short
	 */
	public short getCreatePer()
	{
		return createPer;
	}

	/**
	 * Method 'setCreatePer'
	 * 
	 * @param createPer
	 */
	public void setCreatePer(short createPer)
	{
		this.createPer = createPer;
	}

	/**
	 * Method 'getRetrievePer'
	 * 
	 * @return short
	 */
	public short getRetrievePer()
	{
		return retrievePer;
	}

	/**
	 * Method 'setRetrievePer'
	 * 
	 * @param retrievePer
	 */
	public void setRetrievePer(short retrievePer)
	{
		this.retrievePer = retrievePer;
	}

	/**
	 * Method 'getUpdatePer'
	 * 
	 * @return short
	 */
	public short getUpdatePer()
	{
		return updatePer;
	}

	/**
	 * Method 'setUpdatePer'
	 * 
	 * @param updatePer
	 */
	public void setUpdatePer(short updatePer)
	{
		this.updatePer = updatePer;
	}

	/**
	 * Method 'getDeletePer'
	 * 
	 * @return short
	 */
	public short getDeletePer()
	{
		return deletePer;
	}

	/**
	 * Method 'setDeletePer'
	 * 
	 * @param deletePer
	 */
	public void setDeletePer(short deletePer)
	{
		this.deletePer = deletePer;
	}

	/**
	 * Method 'getAllPer'
	 * 
	 * @return short
	 */
	public short getAllPer()
	{
		return allPer;
	}

	/**
	 * Method 'setAllPer'
	 * 
	 * @param allPer
	 */
	public void setAllPer(short allPer)
	{
		this.allPer = allPer;
	}

	/**
	 * Method 'getPermissionsIsDeleted'
	 * 
	 * @return short
	 */
	public short getPermissionsIsDeleted()
	{
		return permissionsIsDeleted;
	}

	/**
	 * Method 'setPermissionsIsDeleted'
	 * 
	 * @param permissionsIsDeleted
	 */
	public void setPermissionsIsDeleted(short permissionsIsDeleted)
	{
		this.permissionsIsDeleted = permissionsIsDeleted;
		this.permissionsIsDeletedNull = false;
	}

	/**
	 * Method 'setPermissionsIsDeletedNull'
	 * 
	 * @param value
	 */
	public void setPermissionsIsDeletedNull(boolean value)
	{
		this.permissionsIsDeletedNull = value;
	}

	/**
	 * Method 'isPermissionsIsDeletedNull'
	 * 
	 * @return boolean
	 */
	public boolean isPermissionsIsDeletedNull()
	{
		return permissionsIsDeletedNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodRoleModulePermissions)) {
			return false;
		}
		
		final IprodRoleModulePermissions _cast = (IprodRoleModulePermissions) _other;
		if (roleId != _cast.roleId) {
			return false;
		}
		
		if (moduleId != _cast.moduleId) {
			return false;
		}
		
		if (createPer != _cast.createPer) {
			return false;
		}
		
		if (retrievePer != _cast.retrievePer) {
			return false;
		}
		
		if (updatePer != _cast.updatePer) {
			return false;
		}
		
		if (deletePer != _cast.deletePer) {
			return false;
		}
		
		if (allPer != _cast.allPer) {
			return false;
		}
		
		if (permissionsIsDeleted != _cast.permissionsIsDeleted) {
			return false;
		}
		
		if (permissionsIsDeletedNull != _cast.permissionsIsDeletedNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + roleId;
		_hashCode = 29 * _hashCode + moduleId;
		_hashCode = 29 * _hashCode + (int) createPer;
		_hashCode = 29 * _hashCode + (int) retrievePer;
		_hashCode = 29 * _hashCode + (int) updatePer;
		_hashCode = 29 * _hashCode + (int) deletePer;
		_hashCode = 29 * _hashCode + (int) allPer;
		_hashCode = 29 * _hashCode + (int) permissionsIsDeleted;
		_hashCode = 29 * _hashCode + (permissionsIsDeletedNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodRoleModulePermissionsPk
	 */
	public IprodRoleModulePermissionsPk createPk()
	{
		return new IprodRoleModulePermissionsPk(roleId, moduleId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodRoleModulePermissions: " );
		ret.append( "roleId=" + roleId );
		ret.append( ", moduleId=" + moduleId );
		ret.append( ", createPer=" + createPer );
		ret.append( ", retrievePer=" + retrievePer );
		ret.append( ", updatePer=" + updatePer );
		ret.append( ", deletePer=" + deletePer );
		ret.append( ", allPer=" + allPer );
		ret.append( ", permissionsIsDeleted=" + permissionsIsDeleted );
		return ret.toString();
	}

}
