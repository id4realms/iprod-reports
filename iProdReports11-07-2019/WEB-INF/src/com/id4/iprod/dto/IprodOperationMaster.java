package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodOperationMaster implements Serializable
{
	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_operation_master table.
	 */
	protected int operationId;

	/** 
	 * This attribute maps to the column OPERATION_NAME in the iprod_operation_master table.
	 */
	protected String operationName;

	/** 
	 * This attribute maps to the column OPERATION_DESCRIPTION in the iprod_operation_master table.
	 */
	protected String operationDescription;

	/** 
	 * This attribute maps to the column OPERATION_CDATETTIME in the iprod_operation_master table.
	 */
	protected String operationCdatettime;

	/** 
	 * This attribute maps to the column OPERATION_MDATETIME in the iprod_operation_master table.
	 */
	protected String operationMdatetime;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_operation_master table.
	 */
	protected short isDeleted;

	/**
	 * Method 'IprodOperationMaster'
	 * 
	 */
	public IprodOperationMaster()
	{
	}

	/**
	 * Method 'getOperationId'
	 * 
	 * @return int
	 */
	public int getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'setOperationId'
	 * 
	 * @param operationId
	 */
	public void setOperationId(int operationId)
	{
		this.operationId = operationId;
	}

	/**
	 * Method 'getOperationName'
	 * 
	 * @return String
	 */
	public String getOperationName()
	{
		return operationName;
	}

	/**
	 * Method 'setOperationName'
	 * 
	 * @param operationName
	 */
	public void setOperationName(String operationName)
	{
		this.operationName = operationName;
	}

	/**
	 * Method 'getOperationDescription'
	 * 
	 * @return String
	 */
	public String getOperationDescription()
	{
		return operationDescription;
	}

	/**
	 * Method 'setOperationDescription'
	 * 
	 * @param operationDescription
	 */
	public void setOperationDescription(String operationDescription)
	{
		this.operationDescription = operationDescription;
	}

	/**
	 * Method 'getOperationCdatettime'
	 * 
	 * @return String
	 */
	public String getOperationCdatettime()
	{
		return operationCdatettime;
	}

	/**
	 * Method 'setOperationCdatettime'
	 * 
	 * @param operationCdatettime
	 */
	public void setOperationCdatettime(String operationCdatettime)
	{
		this.operationCdatettime = operationCdatettime;
	}

	/**
	 * Method 'getOperationMdatetime'
	 * 
	 * @return String
	 */
	public String getOperationMdatetime()
	{
		return operationMdatetime;
	}

	/**
	 * Method 'setOperationMdatetime'
	 * 
	 * @param operationMdatetime
	 */
	public void setOperationMdatetime(String operationMdatetime)
	{
		this.operationMdatetime = operationMdatetime;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodOperationMaster)) {
			return false;
		}
		
		final IprodOperationMaster _cast = (IprodOperationMaster) _other;
		if (operationId != _cast.operationId) {
			return false;
		}
		
		if (operationName == null ? _cast.operationName != operationName : !operationName.equals( _cast.operationName )) {
			return false;
		}
		
		if (operationDescription == null ? _cast.operationDescription != operationDescription : !operationDescription.equals( _cast.operationDescription )) {
			return false;
		}
		
		if (operationCdatettime == null ? _cast.operationCdatettime != operationCdatettime : !operationCdatettime.equals( _cast.operationCdatettime )) {
			return false;
		}
		
		if (operationMdatetime == null ? _cast.operationMdatetime != operationMdatetime : !operationMdatetime.equals( _cast.operationMdatetime )) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + operationId;
		if (operationName != null) {
			_hashCode = 29 * _hashCode + operationName.hashCode();
		}
		
		if (operationDescription != null) {
			_hashCode = 29 * _hashCode + operationDescription.hashCode();
		}
		
		if (operationCdatettime != null) {
			_hashCode = 29 * _hashCode + operationCdatettime.hashCode();
		}
		
		if (operationMdatetime != null) {
			_hashCode = 29 * _hashCode + operationMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) isDeleted;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodOperationMasterPk
	 */
	public IprodOperationMasterPk createPk()
	{
		return new IprodOperationMasterPk(operationId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodOperationMaster: " );
		ret.append( "operationId=" + operationId );
		ret.append( ", operationName=" + operationName );
		ret.append( ", operationDescription=" + operationDescription );
		ret.append( ", operationCdatettime=" + operationCdatettime );
		ret.append( ", operationMdatetime=" + operationMdatetime );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

}
