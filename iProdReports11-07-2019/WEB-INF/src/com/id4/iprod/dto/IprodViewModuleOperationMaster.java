package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodViewModuleOperationMaster implements Serializable
{
	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_view_module_operation_master table.
	 */
	protected int moduleId;

	/** 
	 * This attribute maps to the column MODULE_NAME in the iprod_view_module_operation_master table.
	 */
	protected String moduleName;

	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_view_module_operation_master table.
	 */
	protected int operationId;

	/** 
	 * This attribute maps to the column OPERATION_NAME in the iprod_view_module_operation_master table.
	 */
	protected String operationName;

	/** 
	 * This attribute maps to the column OPERATION_DESCRIPTION in the iprod_view_module_operation_master table.
	 */
	protected String operationDescription;

	/** 
	 * This attribute maps to the column IS_APPLICABLE in the iprod_view_module_operation_master table.
	 */
	protected short isApplicable;

	/**
	 * Method 'IprodViewModuleOperationMaster'
	 * 
	 */
	public IprodViewModuleOperationMaster()
	{
	}

	/**
	 * Method 'getModuleId'
	 * 
	 * @return int
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'setModuleId'
	 * 
	 * @param moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getModuleName'
	 * 
	 * @return String
	 */
	public String getModuleName()
	{
		return moduleName;
	}

	/**
	 * Method 'setModuleName'
	 * 
	 * @param moduleName
	 */
	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	/**
	 * Method 'getOperationId'
	 * 
	 * @return int
	 */
	public int getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'setOperationId'
	 * 
	 * @param operationId
	 */
	public void setOperationId(int operationId)
	{
		this.operationId = operationId;
	}

	/**
	 * Method 'getOperationName'
	 * 
	 * @return String
	 */
	public String getOperationName()
	{
		return operationName;
	}

	/**
	 * Method 'setOperationName'
	 * 
	 * @param operationName
	 */
	public void setOperationName(String operationName)
	{
		this.operationName = operationName;
	}

	/**
	 * Method 'getOperationDescription'
	 * 
	 * @return String
	 */
	public String getOperationDescription()
	{
		return operationDescription;
	}

	/**
	 * Method 'setOperationDescription'
	 * 
	 * @param operationDescription
	 */
	public void setOperationDescription(String operationDescription)
	{
		this.operationDescription = operationDescription;
	}

	/**
	 * Method 'getIsApplicable'
	 * 
	 * @return short
	 */
	public short getIsApplicable()
	{
		return isApplicable;
	}

	/**
	 * Method 'setIsApplicable'
	 * 
	 * @param isApplicable
	 */
	public void setIsApplicable(short isApplicable)
	{
		this.isApplicable = isApplicable;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodViewModuleOperationMaster)) {
			return false;
		}
		
		final IprodViewModuleOperationMaster _cast = (IprodViewModuleOperationMaster) _other;
		if (moduleId != _cast.moduleId) {
			return false;
		}
		
		if (moduleName == null ? _cast.moduleName != moduleName : !moduleName.equals( _cast.moduleName )) {
			return false;
		}
		
		if (operationId != _cast.operationId) {
			return false;
		}
		
		if (operationName == null ? _cast.operationName != operationName : !operationName.equals( _cast.operationName )) {
			return false;
		}
		
		if (operationDescription == null ? _cast.operationDescription != operationDescription : !operationDescription.equals( _cast.operationDescription )) {
			return false;
		}
		
		if (isApplicable != _cast.isApplicable) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + moduleId;
		if (moduleName != null) {
			_hashCode = 29 * _hashCode + moduleName.hashCode();
		}
		
		_hashCode = 29 * _hashCode + operationId;
		if (operationName != null) {
			_hashCode = 29 * _hashCode + operationName.hashCode();
		}
		
		if (operationDescription != null) {
			_hashCode = 29 * _hashCode + operationDescription.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) isApplicable;
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodViewModuleOperationMaster: " );
		ret.append( "moduleId=" + moduleId );
		ret.append( ", moduleName=" + moduleName );
		ret.append( ", operationId=" + operationId );
		ret.append( ", operationName=" + operationName );
		ret.append( ", operationDescription=" + operationDescription );
		ret.append( ", isApplicable=" + isApplicable );
		return ret.toString();
	}

}
