package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodSequenceMaster implements Serializable
{
	/** 
	 * This attribute maps to the column SEQUENCE_ID in the iprod_sequence_master table.
	 */
	protected int sequenceId;

	/** 
	 * This attribute maps to the column SEQUENCE_NAME in the iprod_sequence_master table.
	 */
	protected String sequenceName;

	/** 
	 * This attribute maps to the column SEQUENCE_XML_PATH in the iprod_sequence_master table.
	 */
	protected String sequenceXmlPath;

	/** 
	 * This attribute maps to the column PROCESS_RESOURCE_NAME in the iprod_sequence_master table.
	 */
	protected String processResourceName;

	/** 
	 * This attribute maps to the column PROCESS_INSTANCE_KEY in the iprod_sequence_master table.
	 */
	protected String processInstanceKey;

	/** 
	 * This attribute maps to the column SEQUENCE_DESC in the iprod_sequence_master table.
	 */
	protected String sequenceDesc;

	/** 
	 * This attribute maps to the column SEQUENCE_CDATETTIME in the iprod_sequence_master table.
	 */
	protected String sequenceCdatettime;

	/** 
	 * This attribute maps to the column SEQUENCE_MDATETIME in the iprod_sequence_master table.
	 */
	protected String sequenceMdatetime;

	/** 
	 * This attribute maps to the column IS_DELETED in the iprod_sequence_master table.
	 */
	protected short isDeleted;

	/**
	 * Method 'IprodSequenceMaster'
	 * 
	 */
	public IprodSequenceMaster()
	{
	}

	/**
	 * Method 'getSequenceId'
	 * 
	 * @return int
	 */
	public int getSequenceId()
	{
		return sequenceId;
	}

	/**
	 * Method 'setSequenceId'
	 * 
	 * @param sequenceId
	 */
	public void setSequenceId(int sequenceId)
	{
		this.sequenceId = sequenceId;
	}

	/**
	 * Method 'getSequenceName'
	 * 
	 * @return String
	 */
	public String getSequenceName()
	{
		return sequenceName;
	}

	/**
	 * Method 'setSequenceName'
	 * 
	 * @param sequenceName
	 */
	public void setSequenceName(String sequenceName)
	{
		this.sequenceName = sequenceName;
	}

	/**
	 * Method 'getSequenceXmlPath'
	 * 
	 * @return String
	 */
	public String getSequenceXmlPath()
	{
		return sequenceXmlPath;
	}

	/**
	 * Method 'setSequenceXmlPath'
	 * 
	 * @param sequenceXmlPath
	 */
	public void setSequenceXmlPath(String sequenceXmlPath)
	{
		this.sequenceXmlPath = sequenceXmlPath;
	}

	/**
	 * Method 'getProcessResourceName'
	 * 
	 * @return String
	 */
	public String getProcessResourceName()
	{
		return processResourceName;
	}

	/**
	 * Method 'setProcessResourceName'
	 * 
	 * @param processResourceName
	 */
	public void setProcessResourceName(String processResourceName)
	{
		this.processResourceName = processResourceName;
	}

	/**
	 * Method 'getProcessInstanceKey'
	 * 
	 * @return String
	 */
	public String getProcessInstanceKey()
	{
		return processInstanceKey;
	}

	/**
	 * Method 'setProcessInstanceKey'
	 * 
	 * @param processInstanceKey
	 */
	public void setProcessInstanceKey(String processInstanceKey)
	{
		this.processInstanceKey = processInstanceKey;
	}

	/**
	 * Method 'getSequenceDesc'
	 * 
	 * @return String
	 */
	public String getSequenceDesc()
	{
		return sequenceDesc;
	}

	/**
	 * Method 'setSequenceDesc'
	 * 
	 * @param sequenceDesc
	 */
	public void setSequenceDesc(String sequenceDesc)
	{
		this.sequenceDesc = sequenceDesc;
	}

	/**
	 * Method 'getSequenceCdatettime'
	 * 
	 * @return String
	 */
	public String getSequenceCdatettime()
	{
		return sequenceCdatettime;
	}

	/**
	 * Method 'setSequenceCdatettime'
	 * 
	 * @param sequenceCdatettime
	 */
	public void setSequenceCdatettime(String sequenceCdatettime)
	{
		this.sequenceCdatettime = sequenceCdatettime;
	}

	/**
	 * Method 'getSequenceMdatetime'
	 * 
	 * @return String
	 */
	public String getSequenceMdatetime()
	{
		return sequenceMdatetime;
	}

	/**
	 * Method 'setSequenceMdatetime'
	 * 
	 * @param sequenceMdatetime
	 */
	public void setSequenceMdatetime(String sequenceMdatetime)
	{
		this.sequenceMdatetime = sequenceMdatetime;
	}

	/**
	 * Method 'getIsDeleted'
	 * 
	 * @return short
	 */
	public short getIsDeleted()
	{
		return isDeleted;
	}

	/**
	 * Method 'setIsDeleted'
	 * 
	 * @param isDeleted
	 */
	public void setIsDeleted(short isDeleted)
	{
		this.isDeleted = isDeleted;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodSequenceMaster)) {
			return false;
		}
		
		final IprodSequenceMaster _cast = (IprodSequenceMaster) _other;
		if (sequenceId != _cast.sequenceId) {
			return false;
		}
		
		if (sequenceName == null ? _cast.sequenceName != sequenceName : !sequenceName.equals( _cast.sequenceName )) {
			return false;
		}
		
		if (sequenceXmlPath == null ? _cast.sequenceXmlPath != sequenceXmlPath : !sequenceXmlPath.equals( _cast.sequenceXmlPath )) {
			return false;
		}
		
		if (processResourceName == null ? _cast.processResourceName != processResourceName : !processResourceName.equals( _cast.processResourceName )) {
			return false;
		}
		
		if (processInstanceKey == null ? _cast.processInstanceKey != processInstanceKey : !processInstanceKey.equals( _cast.processInstanceKey )) {
			return false;
		}
		
		if (sequenceDesc == null ? _cast.sequenceDesc != sequenceDesc : !sequenceDesc.equals( _cast.sequenceDesc )) {
			return false;
		}
		
		if (sequenceCdatettime == null ? _cast.sequenceCdatettime != sequenceCdatettime : !sequenceCdatettime.equals( _cast.sequenceCdatettime )) {
			return false;
		}
		
		if (sequenceMdatetime == null ? _cast.sequenceMdatetime != sequenceMdatetime : !sequenceMdatetime.equals( _cast.sequenceMdatetime )) {
			return false;
		}
		
		if (isDeleted != _cast.isDeleted) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + sequenceId;
		if (sequenceName != null) {
			_hashCode = 29 * _hashCode + sequenceName.hashCode();
		}
		
		if (sequenceXmlPath != null) {
			_hashCode = 29 * _hashCode + sequenceXmlPath.hashCode();
		}
		
		if (processResourceName != null) {
			_hashCode = 29 * _hashCode + processResourceName.hashCode();
		}
		
		if (processInstanceKey != null) {
			_hashCode = 29 * _hashCode + processInstanceKey.hashCode();
		}
		
		if (sequenceDesc != null) {
			_hashCode = 29 * _hashCode + sequenceDesc.hashCode();
		}
		
		if (sequenceCdatettime != null) {
			_hashCode = 29 * _hashCode + sequenceCdatettime.hashCode();
		}
		
		if (sequenceMdatetime != null) {
			_hashCode = 29 * _hashCode + sequenceMdatetime.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) isDeleted;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodSequenceMasterPk
	 */
	public IprodSequenceMasterPk createPk()
	{
		return new IprodSequenceMasterPk(sequenceId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodSequenceMaster: " );
		ret.append( "sequenceId=" + sequenceId );
		ret.append( ", sequenceName=" + sequenceName );
		ret.append( ", sequenceXmlPath=" + sequenceXmlPath );
		ret.append( ", processResourceName=" + processResourceName );
		ret.append( ", processInstanceKey=" + processInstanceKey );
		ret.append( ", sequenceDesc=" + sequenceDesc );
		ret.append( ", sequenceCdatettime=" + sequenceCdatettime );
		ret.append( ", sequenceMdatetime=" + sequenceMdatetime );
		ret.append( ", isDeleted=" + isDeleted );
		return ret.toString();
	}

}
