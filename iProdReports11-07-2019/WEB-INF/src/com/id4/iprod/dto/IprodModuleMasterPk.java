package com.id4.iprod.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the iprod_module_master table.
 */
public class IprodModuleMasterPk implements Serializable
{
	protected int moduleId;

	/** 
	 * This attribute represents whether the primitive attribute moduleId is null.
	 */
	protected boolean moduleIdNull;

	/** 
	 * Sets the value of moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/** 
	 * Gets the value of moduleId
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'IprodModuleMasterPk'
	 * 
	 */
	public IprodModuleMasterPk()
	{
	}

	/**
	 * Method 'IprodModuleMasterPk'
	 * 
	 * @param moduleId
	 */
	public IprodModuleMasterPk(final int moduleId)
	{
		this.moduleId = moduleId;
	}

	/** 
	 * Sets the value of moduleIdNull
	 */
	public void setModuleIdNull(boolean moduleIdNull)
	{
		this.moduleIdNull = moduleIdNull;
	}

	/** 
	 * Gets the value of moduleIdNull
	 */
	public boolean isModuleIdNull()
	{
		return moduleIdNull;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodModuleMasterPk)) {
			return false;
		}
		
		final IprodModuleMasterPk _cast = (IprodModuleMasterPk) _other;
		if (moduleId != _cast.moduleId) {
			return false;
		}
		
		if (moduleIdNull != _cast.moduleIdNull) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + moduleId;
		_hashCode = 29 * _hashCode + (moduleIdNull ? 1 : 0);
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodModuleMasterPk: " );
		ret.append( "moduleId=" + moduleId );
		return ret.toString();
	}

}
