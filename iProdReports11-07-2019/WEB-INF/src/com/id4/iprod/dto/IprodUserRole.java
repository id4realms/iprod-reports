package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodUserRole implements Serializable
{
	/** 
	 * This attribute maps to the column USER_ID in the iprod_user_role table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_user_role table.
	 */
	protected int roleId;

	/**
	 * Method 'IprodUserRole'
	 * 
	 */
	public IprodUserRole()
	{
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodUserRole)) {
			return false;
		}
		
		final IprodUserRole _cast = (IprodUserRole) _other;
		if (userId != _cast.userId) {
			return false;
		}
		
		if (roleId != _cast.roleId) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + userId;
		_hashCode = 29 * _hashCode + roleId;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodUserRolePk
	 */
	public IprodUserRolePk createPk()
	{
		return new IprodUserRolePk(userId, roleId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodUserRole: " );
		ret.append( "userId=" + userId );
		ret.append( ", roleId=" + roleId );
		return ret.toString();
	}

}
