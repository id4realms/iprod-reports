package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodRoleModuleOperations implements Serializable
{
	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_role_module_operations table.
	 */
	protected int roleId;

	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_role_module_operations table.
	 */
	protected int moduleId;

	/** 
	 * This attribute maps to the column OPERATION_ID in the iprod_role_module_operations table.
	 */
	protected int operationId;

	/** 
	 * This attribute maps to the column IS_ALLOWED in the iprod_role_module_operations table.
	 */
	protected short isAllowed;

	/**
	 * Method 'IprodRoleModuleOperations'
	 * 
	 */
	public IprodRoleModuleOperations()
	{
	}

	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	/**
	 * Method 'getModuleId'
	 * 
	 * @return int
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'setModuleId'
	 * 
	 * @param moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getOperationId'
	 * 
	 * @return int
	 */
	public int getOperationId()
	{
		return operationId;
	}

	/**
	 * Method 'setOperationId'
	 * 
	 * @param operationId
	 */
	public void setOperationId(int operationId)
	{
		this.operationId = operationId;
	}

	/**
	 * Method 'getIsAllowed'
	 * 
	 * @return short
	 */
	public short getIsAllowed()
	{
		return isAllowed;
	}

	/**
	 * Method 'setIsAllowed'
	 * 
	 * @param isAllowed
	 */
	public void setIsAllowed(short isAllowed)
	{
		this.isAllowed = isAllowed;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodRoleModuleOperations)) {
			return false;
		}
		
		final IprodRoleModuleOperations _cast = (IprodRoleModuleOperations) _other;
		if (roleId != _cast.roleId) {
			return false;
		}
		
		if (moduleId != _cast.moduleId) {
			return false;
		}
		
		if (operationId != _cast.operationId) {
			return false;
		}
		
		if (isAllowed != _cast.isAllowed) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + roleId;
		_hashCode = 29 * _hashCode + moduleId;
		_hashCode = 29 * _hashCode + operationId;
		_hashCode = 29 * _hashCode + (int) isAllowed;
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodRoleModuleOperationsPk
	 */
	public IprodRoleModuleOperationsPk createPk()
	{
		return new IprodRoleModuleOperationsPk(roleId, moduleId, operationId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodRoleModuleOperations: " );
		ret.append( "roleId=" + roleId );
		ret.append( ", moduleId=" + moduleId );
		ret.append( ", operationId=" + operationId );
		ret.append( ", isAllowed=" + isAllowed );
		return ret.toString();
	}

}
