package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodViewUserDetails implements Serializable
{
	/** 
	 * This attribute maps to the column USER_ID in the iprod_view_user_details table.
	 */
	protected int userId;

	/** 
	 * This attribute maps to the column USER_NAME in the iprod_view_user_details table.
	 */
	protected String userName;

	/** 
	 * This attribute maps to the column USER_TITLE in the iprod_view_user_details table.
	 */
	protected String userTitle;

	/** 
	 * This attribute maps to the column FIRST_NAME in the iprod_view_user_details table.
	 */
	protected String firstName;

	/** 
	 * This attribute maps to the column LAST_NAME in the iprod_view_user_details table.
	 */
	protected String lastName;

	/** 
	 * This attribute maps to the column GENDER in the iprod_view_user_details table.
	 */
	protected String gender;

	/** 
	 * This attribute maps to the column EMAIL_ID in the iprod_view_user_details table.
	 */
	protected String emailId;

	/** 
	 * This attribute maps to the column CONTACT_NO in the iprod_view_user_details table.
	 */
	protected String contactNo;

	/** 
	 * This attribute maps to the column USER_PASSWORD in the iprod_view_user_details table.
	 */
	protected String userPassword;

	/** 
	 * This attribute maps to the column LAST_LOGIN_DETAILS in the iprod_view_user_details table.
	 */
	protected String lastLoginDetails;

	/** 
	 * This attribute maps to the column USER_IS_DELETED in the iprod_view_user_details table.
	 */
	protected short userIsDeleted;

	/** 
	 * This attribute maps to the column ROLE_ID in the iprod_view_user_details table.
	 */
	protected int roleId;

	/** 
	 * This attribute represents whether the primitive attribute roleId is null.
	 */
	protected boolean roleIdNull = true;

	/** 
	 * This attribute maps to the column ROLE_NAME in the iprod_view_user_details table.
	 */
	protected String roleName;

	/** 
	 * This attribute maps to the column ROLE_DESC in the iprod_view_user_details table.
	 */
	protected String roleDesc;

	/**
	 * Method 'IprodViewUserDetails'
	 * 
	 */
	public IprodViewUserDetails()
	{
	}

	/**
	 * Method 'getUserId'
	 * 
	 * @return int
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * Method 'setUserId'
	 * 
	 * @param userId
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * Method 'getUserName'
	 * 
	 * @return String
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * Method 'setUserName'
	 * 
	 * @param userName
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * Method 'getUserTitle'
	 * 
	 * @return String
	 */
	public String getUserTitle()
	{
		return userTitle;
	}

	/**
	 * Method 'setUserTitle'
	 * 
	 * @param userTitle
	 */
	public void setUserTitle(String userTitle)
	{
		this.userTitle = userTitle;
	}

	/**
	 * Method 'getFirstName'
	 * 
	 * @return String
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Method 'setFirstName'
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Method 'getLastName'
	 * 
	 * @return String
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Method 'setLastName'
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Method 'getGender'
	 * 
	 * @return String
	 */
	public String getGender()
	{
		return gender;
	}

	/**
	 * Method 'setGender'
	 * 
	 * @param gender
	 */
	public void setGender(String gender)
	{
		this.gender = gender;
	}

	/**
	 * Method 'getEmailId'
	 * 
	 * @return String
	 */
	public String getEmailId()
	{
		return emailId;
	}

	/**
	 * Method 'setEmailId'
	 * 
	 * @param emailId
	 */
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	/**
	 * Method 'getContactNo'
	 * 
	 * @return String
	 */
	public String getContactNo()
	{
		return contactNo;
	}

	/**
	 * Method 'setContactNo'
	 * 
	 * @param contactNo
	 */
	public void setContactNo(String contactNo)
	{
		this.contactNo = contactNo;
	}

	/**
	 * Method 'getUserPassword'
	 * 
	 * @return String
	 */
	public String getUserPassword()
	{
		return userPassword;
	}

	/**
	 * Method 'setUserPassword'
	 * 
	 * @param userPassword
	 */
	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}

	/**
	 * Method 'getLastLoginDetails'
	 * 
	 * @return String
	 */
	public String getLastLoginDetails()
	{
		return lastLoginDetails;
	}

	/**
	 * Method 'setLastLoginDetails'
	 * 
	 * @param lastLoginDetails
	 */
	public void setLastLoginDetails(String lastLoginDetails)
	{
		this.lastLoginDetails = lastLoginDetails;
	}

	/**
	 * Method 'getUserIsDeleted'
	 * 
	 * @return short
	 */
	public short getUserIsDeleted()
	{
		return userIsDeleted;
	}

	/**
	 * Method 'setUserIsDeleted'
	 * 
	 * @param userIsDeleted
	 */
	public void setUserIsDeleted(short userIsDeleted)
	{
		this.userIsDeleted = userIsDeleted;
	}

	/**
	 * Method 'getRoleId'
	 * 
	 * @return int
	 */
	public int getRoleId()
	{
		return roleId;
	}

	/**
	 * Method 'setRoleId'
	 * 
	 * @param roleId
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
		this.roleIdNull = false;
	}

	/**
	 * Method 'setRoleIdNull'
	 * 
	 * @param value
	 */
	public void setRoleIdNull(boolean value)
	{
		this.roleIdNull = value;
	}

	/**
	 * Method 'isRoleIdNull'
	 * 
	 * @return boolean
	 */
	public boolean isRoleIdNull()
	{
		return roleIdNull;
	}

	/**
	 * Method 'getRoleName'
	 * 
	 * @return String
	 */
	public String getRoleName()
	{
		return roleName;
	}

	/**
	 * Method 'setRoleName'
	 * 
	 * @param roleName
	 */
	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}

	/**
	 * Method 'getRoleDesc'
	 * 
	 * @return String
	 */
	public String getRoleDesc()
	{
		return roleDesc;
	}

	/**
	 * Method 'setRoleDesc'
	 * 
	 * @param roleDesc
	 */
	public void setRoleDesc(String roleDesc)
	{
		this.roleDesc = roleDesc;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodViewUserDetails)) {
			return false;
		}
		
		final IprodViewUserDetails _cast = (IprodViewUserDetails) _other;
		if (userId != _cast.userId) {
			return false;
		}
		
		if (userName == null ? _cast.userName != userName : !userName.equals( _cast.userName )) {
			return false;
		}
		
		if (userTitle == null ? _cast.userTitle != userTitle : !userTitle.equals( _cast.userTitle )) {
			return false;
		}
		
		if (firstName == null ? _cast.firstName != firstName : !firstName.equals( _cast.firstName )) {
			return false;
		}
		
		if (lastName == null ? _cast.lastName != lastName : !lastName.equals( _cast.lastName )) {
			return false;
		}
		
		if (gender == null ? _cast.gender != gender : !gender.equals( _cast.gender )) {
			return false;
		}
		
		if (emailId == null ? _cast.emailId != emailId : !emailId.equals( _cast.emailId )) {
			return false;
		}
		
		if (contactNo == null ? _cast.contactNo != contactNo : !contactNo.equals( _cast.contactNo )) {
			return false;
		}
		
		if (userPassword == null ? _cast.userPassword != userPassword : !userPassword.equals( _cast.userPassword )) {
			return false;
		}
		
		if (lastLoginDetails == null ? _cast.lastLoginDetails != lastLoginDetails : !lastLoginDetails.equals( _cast.lastLoginDetails )) {
			return false;
		}
		
		if (userIsDeleted != _cast.userIsDeleted) {
			return false;
		}
		
		if (roleId != _cast.roleId) {
			return false;
		}
		
		if (roleIdNull != _cast.roleIdNull) {
			return false;
		}
		
		if (roleName == null ? _cast.roleName != roleName : !roleName.equals( _cast.roleName )) {
			return false;
		}
		
		if (roleDesc == null ? _cast.roleDesc != roleDesc : !roleDesc.equals( _cast.roleDesc )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + userId;
		if (userName != null) {
			_hashCode = 29 * _hashCode + userName.hashCode();
		}
		
		if (userTitle != null) {
			_hashCode = 29 * _hashCode + userTitle.hashCode();
		}
		
		if (firstName != null) {
			_hashCode = 29 * _hashCode + firstName.hashCode();
		}
		
		if (lastName != null) {
			_hashCode = 29 * _hashCode + lastName.hashCode();
		}
		
		if (gender != null) {
			_hashCode = 29 * _hashCode + gender.hashCode();
		}
		
		if (emailId != null) {
			_hashCode = 29 * _hashCode + emailId.hashCode();
		}
		
		if (contactNo != null) {
			_hashCode = 29 * _hashCode + contactNo.hashCode();
		}
		
		if (userPassword != null) {
			_hashCode = 29 * _hashCode + userPassword.hashCode();
		}
		
		if (lastLoginDetails != null) {
			_hashCode = 29 * _hashCode + lastLoginDetails.hashCode();
		}
		
		_hashCode = 29 * _hashCode + (int) userIsDeleted;
		_hashCode = 29 * _hashCode + roleId;
		_hashCode = 29 * _hashCode + (roleIdNull ? 1 : 0);
		if (roleName != null) {
			_hashCode = 29 * _hashCode + roleName.hashCode();
		}
		
		if (roleDesc != null) {
			_hashCode = 29 * _hashCode + roleDesc.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodViewUserDetails: " );
		ret.append( "userId=" + userId );
		ret.append( ", userName=" + userName );
		ret.append( ", userTitle=" + userTitle );
		ret.append( ", firstName=" + firstName );
		ret.append( ", lastName=" + lastName );
		ret.append( ", gender=" + gender );
		ret.append( ", emailId=" + emailId );
		ret.append( ", contactNo=" + contactNo );
		ret.append( ", userPassword=" + userPassword );
		ret.append( ", lastLoginDetails=" + lastLoginDetails );
		ret.append( ", userIsDeleted=" + userIsDeleted );
		ret.append( ", roleId=" + roleId );
		ret.append( ", roleName=" + roleName );
		ret.append( ", roleDesc=" + roleDesc );
		return ret.toString();
	}

}
