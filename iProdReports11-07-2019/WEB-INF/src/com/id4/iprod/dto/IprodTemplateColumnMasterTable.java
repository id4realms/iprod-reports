package com.id4.iprod.dto;

import java.util.ArrayList;

public class IprodTemplateColumnMasterTable 
{
	public String columnGroupAlias = null;
	public ArrayList<IprodColumnGroup> templateColumnSubTable = new ArrayList<IprodColumnGroup>();
	
	public String getColumnGroupAlias() {
		//System.out.println("IprodTemplateColumnMasterTable::getColumnGroupAlias" +columnGroupAlias);
		return columnGroupAlias;
	}
	public void setColumnGroupAlias(String columnGroupAlias) {
		this.columnGroupAlias = columnGroupAlias;
	}
	public ArrayList<IprodColumnGroup> getTemplateColumnSubTable() {
		return templateColumnSubTable;
	}
	public void setTemplateColumnSubTable(ArrayList<IprodColumnGroup> templateColumnSubTable) {
		this.templateColumnSubTable = templateColumnSubTable;
	}

	
}
