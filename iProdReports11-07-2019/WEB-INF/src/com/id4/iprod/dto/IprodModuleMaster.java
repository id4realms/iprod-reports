package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodModuleMaster implements Serializable
{
	/** 
	 * This attribute maps to the column MODULE_ID in the iprod_module_master table.
	 */
	protected int moduleId;

	/** 
	 * This attribute maps to the column MODULE_NAME in the iprod_module_master table.
	 */
	protected String moduleName;

	/**
	 * Method 'IprodModuleMaster'
	 * 
	 */
	public IprodModuleMaster()
	{
	}

	/**
	 * Method 'getModuleId'
	 * 
	 * @return int
	 */
	public int getModuleId()
	{
		return moduleId;
	}

	/**
	 * Method 'setModuleId'
	 * 
	 * @param moduleId
	 */
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * Method 'getModuleName'
	 * 
	 * @return String
	 */
	public String getModuleName()
	{
		return moduleName;
	}

	/**
	 * Method 'setModuleName'
	 * 
	 * @param moduleName
	 */
	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodModuleMaster)) {
			return false;
		}
		
		final IprodModuleMaster _cast = (IprodModuleMaster) _other;
		if (moduleId != _cast.moduleId) {
			return false;
		}
		
		if (moduleName == null ? _cast.moduleName != moduleName : !moduleName.equals( _cast.moduleName )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + moduleId;
		if (moduleName != null) {
			_hashCode = 29 * _hashCode + moduleName.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodModuleMasterPk
	 */
	public IprodModuleMasterPk createPk()
	{
		return new IprodModuleMasterPk(moduleId);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodModuleMaster: " );
		ret.append( "moduleId=" + moduleId );
		ret.append( ", moduleName=" + moduleName );
		return ret.toString();
	}

}
