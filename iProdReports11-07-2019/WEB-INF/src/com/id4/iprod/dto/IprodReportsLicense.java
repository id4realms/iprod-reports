package com.id4.iprod.dto;

import com.id4.iprod.dao.*;
import com.id4.iprod.factory.*;
import com.id4.iprod.exceptions.*;
import java.io.Serializable;
import java.util.*;

public class IprodReportsLicense implements Serializable
{
	/** 
	 * This attribute maps to the column ID in the iprod_reports_license table.
	 */
	protected int id;

	/** 
	 * This attribute maps to the column PRODUCT_KEY in the iprod_reports_license table.
	 */
	protected String productKey;

	/** 
	 * This attribute maps to the column E_MAIL in the iprod_reports_license table.
	 */
	protected String eMail;

	/** 
	 * This attribute maps to the column COMPANY_NAME in the iprod_reports_license table.
	 */
	protected String companyName;

	/** 
	 * This attribute maps to the column KEY_VALUE in the iprod_reports_license table.
	 */
	protected String keyValue;

	/**
	 * Method 'IprodReportsLicense'
	 * 
	 */
	public IprodReportsLicense()
	{
	}

	/**
	 * Method 'getId'
	 * 
	 * @return int
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Method 'setId'
	 * 
	 * @param id
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * Method 'getProductKey'
	 * 
	 * @return String
	 */
	public String getProductKey()
	{
		return productKey;
	}

	/**
	 * Method 'setProductKey'
	 * 
	 * @param productKey
	 */
	public void setProductKey(String productKey)
	{
		this.productKey = productKey;
	}

	/**
	 * Method 'getEMail'
	 * 
	 * @return String
	 */
	public String getEMail()
	{
		return eMail;
	}

	/**
	 * Method 'setEMail'
	 * 
	 * @param eMail
	 */
	public void setEMail(String eMail)
	{
		this.eMail = eMail;
	}

	/**
	 * Method 'getCompanyName'
	 * 
	 * @return String
	 */
	public String getCompanyName()
	{
		return companyName;
	}

	/**
	 * Method 'setCompanyName'
	 * 
	 * @param companyName
	 */
	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	/**
	 * Method 'getKeyValue'
	 * 
	 * @return String
	 */
	public String getKeyValue()
	{
		return keyValue;
	}

	/**
	 * Method 'setKeyValue'
	 * 
	 * @param keyValue
	 */
	public void setKeyValue(String keyValue)
	{
		this.keyValue = keyValue;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof IprodReportsLicense)) {
			return false;
		}
		
		final IprodReportsLicense _cast = (IprodReportsLicense) _other;
		if (id != _cast.id) {
			return false;
		}
		
		if (productKey == null ? _cast.productKey != productKey : !productKey.equals( _cast.productKey )) {
			return false;
		}
		
		if (eMail == null ? _cast.eMail != eMail : !eMail.equals( _cast.eMail )) {
			return false;
		}
		
		if (companyName == null ? _cast.companyName != companyName : !companyName.equals( _cast.companyName )) {
			return false;
		}
		
		if (keyValue == null ? _cast.keyValue != keyValue : !keyValue.equals( _cast.keyValue )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		_hashCode = 29 * _hashCode + id;
		if (productKey != null) {
			_hashCode = 29 * _hashCode + productKey.hashCode();
		}
		
		if (eMail != null) {
			_hashCode = 29 * _hashCode + eMail.hashCode();
		}
		
		if (companyName != null) {
			_hashCode = 29 * _hashCode + companyName.hashCode();
		}
		
		if (keyValue != null) {
			_hashCode = 29 * _hashCode + keyValue.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'createPk'
	 * 
	 * @return IprodReportsLicensePk
	 */
	public IprodReportsLicensePk createPk()
	{
		return new IprodReportsLicensePk(id);
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "com.id4.iprod.dto.IprodReportsLicense: " );
		ret.append( "id=" + id );
		ret.append( ", productKey=" + productKey );
		ret.append( ", eMail=" + eMail );
		ret.append( ", companyName=" + companyName );
		ret.append( ", keyValue=" + keyValue );
		return ret.toString();
	}

}
