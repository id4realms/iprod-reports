package com.id4.iprod.interceptors;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.id4.iprod.dao.IprodViewRoleModuleOperationsDao;
import com.id4.iprod.dto.IprodViewRoleModuleOperations;
import com.id4.iprod.exceptions.IprodViewRoleModuleOperationsDaoException;
import com.id4.iprod.factory.DaoFactory;
import com.id4.iprod.Consts;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.Interceptor;




public class IprodModuleAuthorizationInterceptor extends ActionSupport implements Interceptor{
	
	static Logger log = Logger.getLogger(IprodModuleAuthorizationInterceptor.class);
	
	private static final long serialVersionUID = 1L;
	
	/** 
	 * This attribute maps to the column MODULE_NAME in the iprod_view_role_module_operations table.
	 */
	protected String moduleName;
	
	/** 
	 * This attribute maps to the column OPERATION_NAME in the iprod_view_role_module_operations table.
	 */
	protected String operationName;
	
	/** 
	 * This attribute maps to the column IS_ALLOWED in the iprod_view_role_module_operations table.
	 */
	protected short isAllowed;
	
	
	
    public void init() {   
    }   
  
    public List<IprodViewRoleModuleOperations> checkRoleModuleOperationListG = new ArrayList<IprodViewRoleModuleOperations>();
    public HashMap<String, Short> hashMapCheckRoleModuleOperationListG = new HashMap<String, Short>();

   


	public boolean checkAuthorizationForRole(Integer roleId, String moduleName, String operationName){
    	//log.info("IprodModuleAuthorizationInterceptor :: checkAuthorizationForRole");
     	List<IprodViewRoleModuleOperations> roleModuleOperationsListL = new ArrayList<IprodViewRoleModuleOperations>();
		List<IprodViewRoleModuleOperations> checkRoleModuleOperationListL = new ArrayList<IprodViewRoleModuleOperations>();
     	IprodViewRoleModuleOperationsDao iprodViewRoleModuleOperationsDao = DaoFactory.createIprodViewRoleModuleOperationsDao();
    	try {
    		roleModuleOperationsListL = iprodViewRoleModuleOperationsDao.findWhereRoleIdModuleNameAndOperationNameEquals(roleId,moduleName,operationName);
			//log.info("roleModuleOperationsListL::" +roleModuleOperationsListL.size());

 		} catch (IprodViewRoleModuleOperationsDaoException e) {
 			// TODO Auto-generated catch block
 		}
 		if(roleModuleOperationsListL.size()>0){
			if(roleModuleOperationsListL.get(0).getIsAllowed() == (short)1)
			{
				try 
				{
				checkRoleModuleOperationListL = iprodViewRoleModuleOperationsDao.findWhereRoleIdModuleNameEquals(roleId,moduleName);
					if(checkRoleModuleOperationListL.size() > 0){
						checkRoleModuleOperationListG = checkRoleModuleOperationListL;
						for(int i=0;i<checkRoleModuleOperationListG.size();i++)
						{
							hashMapCheckRoleModuleOperationListG.put(checkRoleModuleOperationListG.get(i).getOperationName(),checkRoleModuleOperationListG.get(i).getIsAllowed());
						}
						//log.info("hashMapCheckRoleModuleOperationListG::" +hashMapCheckRoleModuleOperationListG);

					}
				}
				catch (IprodViewRoleModuleOperationsDaoException e) {
		 			// TODO Auto-generated catch block
		 		}
				return true;
			}
			else{	
				return false;	
	     	}
 		}
 		return true;
    }
    
    
    
    public String intercept(ActionInvocation invocation) throws Exception { 
    	try{
	    	log.info("IprodModuleAuthorizationInterceptor :: intercept");
	 	   String result = null;
	 	   HttpServletResponse response = ServletActionContext.getResponse();
	     	Map<String, Object> session = ActionContext.getContext().getSession();
	     	IprodViewRoleModuleOperationsDao iprodViewRoleModuleOperationsDao = DaoFactory.createIprodViewRoleModuleOperationsDao();
	     	if (session.get(Consts.IPRODUSERID) != null) {
	     	//FIRST CHECK PAGE ACCESS LEVEL
	 		if(checkAuthorizationForRole((Integer)session.get(Consts.IPRODUSERROLEID),this.moduleName,"VIEW_PAGE_PERM") == true)
	 		{
	 			Action action = (Action) invocation.getAction();
	            if(action instanceof IprodModuleAuthorization){
	                ((IprodModuleAuthorization) action).setHashMapCheckRoleModuleOperationListG(hashMapCheckRoleModuleOperationListG);
	            }
	 			//NOW CHECK FOR SPECIFIC OPERATION
	 			if(checkAuthorizationForRole((Integer)session.get(Consts.IPRODUSERROLEID), this.moduleName,this.operationName) == true) {
	 				return "success";
	 			}else{
	 				return ERROR;
	 			}
	 		
	 		}else{
	 			return ERROR;
	 		}
	 		
	 	}
	     	
    }
	    catch(Exception e){
	    	log.info("intercept :: Exception :: " + e.getMessage() +" occured in intercept.");
	    }
     	return invocation.invoke();
     }
    
  
    /**
     * Handles a rejection by sending a 403 HTTP error
     *
     * @param invocation The invocation
     * @return The result code
     * @throws Exception
     */
    protected String handleRejection(ActionInvocation invocation, HttpServletResponse response){
    	try{
	    	log.info("IprodModuleAuthorizationInterceptor :: handleRejection");
		      try {
				response.sendError(HttpServletResponse.SC_FORBIDDEN);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
	    catch(Exception e){
	    	log.info("handleRejection :: Exception :: " + e.getMessage() +" occured in handleRejection.");
	    }
      return null;
	}
   
    public void destroy() {   
         
    }
    
    /**
	 * Method 'getModuleName'
	 * 
	 * @return String
	 */
	public String getModuleName()
	{
		return moduleName;
	}

	/**
	 * Method 'setModuleName'
	 * 
	 * @param moduleName
	 */
	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}
	
	/**
	 * Method 'getOperationName'
	 * 
	 * @return String
	 */
	public String getOperationName()
	{
		return operationName;
	}

	/**
	 * Method 'setOperationName'
	 * 
	 * @param operationName
	 */
	public void setOperationName(String operationName)
	{
		this.operationName = operationName;
	}
	/**
	 * Method 'getIsAllowed'
	 * 
	 * @return short
	 */
	public short getIsAllowed()
	{
		return isAllowed;
	}

	/**
	 * Method 'setIsAllowed'
	 * 
	 * @param isAllowed
	 */
	public void setIsAllowed(short isAllowed)
	{
		this.isAllowed = isAllowed;
	}
	
	public List<IprodViewRoleModuleOperations> getCheckRoleModuleOperationListG(){
		return checkRoleModuleOperationListG;
	}
	public void setCheckRoleModuleOperationListG(
			List<IprodViewRoleModuleOperations> checkRoleModuleOperationListG) {
		this.checkRoleModuleOperationListG = checkRoleModuleOperationListG;
	}
	
	 public HashMap<String, Short> getHashMapCheckRoleModuleOperationListG() {
			return hashMapCheckRoleModuleOperationListG;
		}

		public void setHashMapCheckRoleModuleOperationListG(
				HashMap<String, Short> hashMapCheckRoleModuleOperationListG) {
			this.hashMapCheckRoleModuleOperationListG = hashMapCheckRoleModuleOperationListG;
		}




  }
