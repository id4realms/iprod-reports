package com.id4.iprod;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import com.opensymphony.xwork2.ActionSupport;

public class Config extends ActionSupport
{
    public String dbCofig()
    {
    	Properties prop = new Properties();
 
    	try {
    		//set the properties value
    		prop.setProperty("jdbc.driverClassName","com.microsoft.sqlserver.jdbc.SQLServerDriver");
    		prop.setProperty("jdbc.url", "jdbc:sqlserver://localhost:1433;");
    		prop.setProperty("jdbc.username", "iprod_reports");
    		prop.setProperty("jdbc.password", "Object123");
 
    		//save properties to project root folder
    		prop.store(new FileOutputStream("jdbc.properties"), null);
 
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
    	return SUCCESS;
    }
}