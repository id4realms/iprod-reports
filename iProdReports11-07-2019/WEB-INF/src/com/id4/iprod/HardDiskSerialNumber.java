package com.id4.iprod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class HardDiskSerialNumber {
  public HardDiskSerialNumber() {  }

  public String getHardDiskSerialNumber(String drive) {
  String result = "";
    try {
      File file = new File("temp\\hsn_"+System.currentTimeMillis()+".vbs");

      FileWriter fw = new java.io.FileWriter(file);

      String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                  +"Set colDrives = objFSO.Drives\n"
                  +"Set objDrive = colDrives.item(\"" + drive + "\")\n"
                  +"Wscript.Echo objDrive.SerialNumber";  // see note
      
      fw.write(vbs);
      fw.close();
      
      List<String> commandList = new ArrayList<String>();
      commandList.add("cscript");
      commandList.add("/NoLogo");
      commandList.add(file.getCanonicalPath());
           
      //System.out.println("Command list contains : "+commandList);
      ProcessBuilder processBuilder = new ProcessBuilder(commandList);
      Process p = processBuilder.start();
      BufferedReader input = new BufferedReader (new InputStreamReader(p.getInputStream()));
      String line;
      while ((line = input.readLine()) != null) {
         result += line;
      }
      
      int exitStatus = p.waitFor();
      if (exitStatus != 0) {
    	    result = "";
    	    System.out.println("The disk licensor did not run succesfully.");
      }else{
    	    System.out.println("The disk licensor ran succesfully with exit status : "+exitStatus);
      }
      
      if(file.exists())
    	  file.delete();
      input.close();
    }
    catch(Exception e){
        e.printStackTrace();
    }
    return result.trim();
  }

}
