<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Manage Profile";</script>
<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
<title>iProd</title>
</head>
<body>		
<div class="container-fluid" id="pcont">
<div class="row-fluid">
<div class="span12">
<h3 class="page-title">Profile Details</h3>
<ul class="breadcrumb">
	<li>
		<i class="fa fa-home"></i>
		<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
	</li>
	<li><a href="#">Profile Details</a></li>

</ul>
  <!--<div class="page-head">-->
<div class="eventmessages">
				<s:if test="hasActionMessages()">
				<div class="success-message">
				<img src="images/success.png" />
				<s:iterator value="actionMessages">
				<s:property />
				</s:iterator>
				</div>
				</s:if>
				<s:if test="hasActionErrors()">
				<div class="error-message">
				<img src="images/error.png" />
				<s:iterator value="actionErrors">
				<s:property />
				</s:iterator>
				</div>
				</s:if>
			</div>
<div class="cl-mcont">
    <div class="content">
		<form class="form-horizontal group-border-dashed" name="view_my_profile_form" id="view_my_profile_form" data-validate="parsley" autocomplete='off' action="ChangePasswordAction">
		  <s:iterator value="selectedUserListDetails" status="rowstatus">
            <s:hidden name="userId" value="%{userId}"/>
            
             <div class="form-group">
              <label class="col-sm-3 control-label">User Name</label>
			   <div class="col-sm-6">
               <input readonly  name="userName" id="userName" data-required="true"  data-trigger="change" class="form-control" value="<s:property value="%{userName}" />"></input>
			   </div>
			   <span id="display"></span>
			  </div>
			  
			  
			  <div class="form-group"> 
			  <label class="col-sm-3 control-label">First Name</label>
			  <div class="col-sm-6">
              <input type="text" readonly name="firstName" id="firstName" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{firstName}" />">
              </div>
              </div>
              
              <div class="form-group"> 
			  <label class="col-sm-3 control-label">Last Name</label>
			  <div class="col-sm-6">
              <input type="text" readonly name="lastName" id="lastName" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{lastName}" />">
              </div>
              </div>
               <div class="form-group"> 
			  <label class="col-sm-3 control-label">Gender</label>
			  <div class="col-sm-6">
              <input type="text" readonly name="gender" id="gender" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{gender}" />">
              </div>
              </div>
           
              <div class="form-group"> 
			  <label class="col-sm-3 control-label">Email Id</label>
			  <div class="col-sm-6">
              <input type="email" readonly required parsley-type="email" name="emailId" id="emailId" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{emailId}" />">
              </div>
              </div>
              
              <div class="form-group"> 
			  <label class="col-sm-3 control-label">Conact Number</label>
			  <div class="col-sm-6">
              <input type="text" readonly name="contactNo" id="contactNo" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{contactNo}" />">
              </div>
              </div>       
	 </s:iterator>
	 			 <div class="form-group"> 
				         <div class="col-sm-offset-2 col-sm-10">
							 
				         <button class="btn btn-primary" type="submit" onclick="javascript:if($('#view_my_profile_form').parsley('validate')==true){var f =document.forms['view_my_profile_form'];f.action='ChangePasswordAction';f.submit()}">Change Password</button>
				         <button class="btn btn-default" onclick="javascript:var f =document.forms['view_my_profile_form'];f.action='ProfileAction';f.submit()">Cancel</button>
							  </div>
							  </div>
							       
			</form>
            </div><!-- End of content -->
			</div>
			</div>
			</div>
		</div>
	</div>
	
	<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
		
<script type="text/javascript">
	$(document).ready(function(){
	$('#datatable').DataTable();
	$('.dataTables_filter input').addClass('form-control').attr('placeholder','Search');
	$('.dataTables_length select').addClass('form-control'); 
	});

</script>		





</body>
</html>
