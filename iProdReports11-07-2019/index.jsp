<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="java.net.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="com.id4.iprod.dao.*"%>
<%@ page import="com.id4.iprod.dto.*"%>
<%@ page import="com.id4.iprod.factory.*"%>
<%@ page import="com.id4.iprod.exceptions.*"%>

<%
	String remoteHost = request.getRemoteAddr();
	String hostName = InetAddress.getByName(remoteHost).getHostName();
%>
<% String hasErrors = "false"; %>
<s:if test="hasActionErrors()">
	<% hasErrors = "true"; %>
</s:if>

<%
if((session.getAttribute("IPRODUSERID") != null) && (hasErrors.equals("false"))){
%>
<s:action name="VerifyLoginAction" executeResult="true" />
<%
} else{
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Welcome to iProd Reports</title>
<!--Old CSS ends-->
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<meta charset="utf-8"/>
	<link rel="shortcut icon" href="images/favicon.png"/>
    <link href="js/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
	<link href="css/style.css" rel="stylesheet" />

	<title>iProd Reports</title>
	
	<script>
	$(function() {
		$(function() {
			$("input:submit").button();
			$('#userName').focus();
		});
	});

	function FocusOnInput() {
		document.getElementById("#userName").focus();
	}
</script>
</head>
<body>
<div class="index-container">
<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
		<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button> 
		<ul class="nav navbar-nav navbar-left user-nav">
			<li style="height: 50px;"><img
				src="images/plusconlogo.png" /></li>
		</ul>
        </div>
		<div class="navbar-right navbar-collapse collapse">		
		<div class="nav navbar-nav user-nav">

		</div>
		</div>
		<div class="site-header">
			<span class="header-brand">
			iProd Reports
			</span>
		</div>

		
</div>
<div id="cl-wrapper" class="notify-container">
	<div class="eventmessages">
				<s:if test="hasActionMessages()">
					<div class="success-message">
						<img src="images/success.png" />
						<s:iterator value="actionMessages">
							<s:property />
						</s:iterator>
					</div>
				</s:if>
				<s:if test="hasActionErrors()">
					<div class="error-message">
						<img src="images/error.png" />
						<s:iterator value="actionErrors">
							<s:property />
						</s:iterator>
					</div>
				</s:if>
	</div>
</div>
<div id="cl-wrapper" class="login-container">
		<!--<div class="indexLeftSide pull-center">-->
		<div class="span12"  align="center">
			
	<!--   <img src="images/IPROD_PDMS_ICON.png"/> 
			<h1>iProd Reports</h1> -->
		<div class="middle-login">
		<div class="block-flat">
				<div class="header">							
				<h3 class="text-center">Gateway to iProd Reports </h3>
				</div>
					<s:form  name="login_form" id="login_form" action="LoginAction" style="margin-bottom: 0px !important;" class="form-horizontal" onload="FocusOnInput()">
					<div class="content">
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="text" placeholder="Username" name="userName" id="userName" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" placeholder="Password" name="userPassword" id="userPassword" class="form-control"/>
										 <input type="hidden" name="ipaddress" id="ipaddress" value="<%=hostName.toLowerCase().trim() %>" />
									</div>
								</div>
							</div>
					</div>
					<div class="foot">
					 <button class="btn" value="Log me in">Log me in</button>
					</div>
				
					<%-- <s:hidden id="iprodLicense" name="iprodLicense"
						value="26b8cefcf720a083f1cb657538c4ca41692f2b79" /> --%>
					</s:form>
		</div>
		<div class="text-center out-links"><a href="#">&copy;iD4 Realms Infotech Pvt. Ltd</a></div>
		</div>
		</div>

</div>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
	<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
	<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
	<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/fuelux/js/fuelux.js"></script>
	<script type="text/javascript" src="js/wColorPicker.min.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
</div>
</body>
</html>
<%
}
%>
