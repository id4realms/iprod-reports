<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Manage Report";</script>
<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
<title>iProd</title>
</head>
<body>		
<div class="container-fluid" id="pcont">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Manage Image <small>users details and more</small></h3>
			<ul class="breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
				</li>
				<li><a href="#">Manage Image</a></li>
			</ul>
		<div class="eventmessages">
		<s:if test="hasActionMessages()">
		<div class="success-message">
		<img src="images/success.png" />
		<s:iterator value="actionMessages">
		<s:property />
		</s:iterator>
		</div>
		</s:if>
		<s:if test="hasActionErrors()">
		<div class="error-message">
		<img src="images/error.png" />
		<s:iterator value="actionErrors">
		<s:property />
		</s:iterator>
		</div>
		</s:if>
		</div>
			<div class="cl-mcont">

			<div class="button-panel">		
			<div class="button"><a href="<s:url action="AddImageMenuAction" encode="true"/>"><input id="add" type="button" class="inputButton" value="+ ADD IMAGE"/></a></div>
			</div>
			</br>
				<div class="content">
					<div class="table-responsive">
						<table class="table table-bordered" id="datatable" >
							<thead>
								<tr>
									<th class="span4-1">SR NO</th>
									<th class="span4-1">IMAGE NAME</th>
									<th class="span4-1">IMAGE</th>
									<th class="span4-1" id="cpnl">CONTROL PANEL</th>
								</tr>
							</thead>
							<tbody>
								<s:iterator value="allImageCustomListG" status="allImageCustomListGStatus">
								<tr class="odd gradeX">
								<td class="span4"><s:property value="#allImageCustomListGStatus.count"/></td>
								<td class="span4"><s:property value="imageName"/></td>
								<td class="span4">
								<img id = "newImage" height ="50" width = "100" src="data:image/jpeg;base64,<s:property value="imageContent" />" />
								</td>	
								<td>
								<!--Delete button-->	
								<s:url id="deleteUrl" action="DeleteImageDetailsAction" encode="true">
								<s:param name="imageId" value="%{imageId}" />
								</s:url>
								<s:a href="%{deleteUrl}" onclick="return confirmDelete()" title="Delete" style="padding-left: 15px; padding-right: 15px;"><i class="fa fa-times-circle-o" width="16" height="16"></i></s:a>	
								</td>
								</tr>
								</s:iterator>
							</tbody>
						</table>	
					</div><!-- End of table-responsive -->
				</div><!-- End of content -->
			</div><!-- End of cl-mcont -->
		</div>
	</div>
</div>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
		
<script type="text/javascript">
	$(document).ready(function(){
	$('#datatable').DataTable();
	$('.dataTables_filter input').addClass('form-control').attr('placeholder','Search');
	$('.dataTables_length select').addClass('form-control'); 
	});

</script>		
<script>  
	function confirmDelete() {  
	var answer = window.confirm("Are you sure you want to delete this record");  
	if(answer)
	return true;  
	else 
	return false;  
	}  
</script>

</body>
</html>
