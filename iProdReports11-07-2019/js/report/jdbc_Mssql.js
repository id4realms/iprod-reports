$(function() {
	// for MSSQL Datasource
	var datasourceId = '';
	var datasourceName = '';
	var rdbmsId = '';
	var jdbcDriverId = '';
	var rdbmsDatabaseName = '';
	var rdbmsServerMachineName = '';
	var rdbmsServerPort = '';
	var rdbmsIsWindowsAuth = '';
	var rdbmsServerUsername = '';
	var rdbmsServerPassword = '';
	var rdbmsInstanceName = '';
	var rdbmsJdbcDriverUrl = '';
	var jdbcDriverClassname;
	var datasourceTypeId = $("#datasourceId").val();
$("#validatePopUpForm").on("click",function() 
{
//alert("inside validatePopUpForm");
		datasourceName = $("#datasourceName").val();
		//alert("datasourceName:: "+datasourceName);
		rdbmsId = $('#rdbmsId').val();
		//alert("rdbmsId ::"+rdbmsId);
		jdbcrdbmsDatabaseNameUrl = $('#databaseId :selected').text();
		//alert("jdbcrdbmsDatabaseNameUrl :: "+jdbcrdbmsDatabaseNameUrl);
		rdbmsIsWindowsAuth = $('input[name=rdbmsIsWindowsAuth]:radio:checked').val();
		//alert("rdbmsIsWindowsAuth:: "+rdbmsIsWindowsAuth);
		jdbcDriverId = $('#jdbcDriverId :selected').val();
		//alert("jdbcDriverId:: "+jdbcDriverId);
		rdbmsDatabaseName = $('#rdbmsDatabaseName').val();
		//alert("rdbmsDatabaseName:: "+rdbmsDatabaseName);
		rdbmsServerMachineName = $("#rdbmsServerMachineName").val();
		//alert("rdbmsServerMachineName:: "+rdbmsServerMachineName);
		rdbmsServerPort = $("#rdbmsServerPort").val();
		//alert("rdbmsServerPort:: "+rdbmsServerPort);
		rdbmsServerUsername = $("#rdbmsServerUsername").val();
		//alert("rdbmsServerUsername::"+rdbmsServerUsername);
		rdbmsServerPassword = $("#rdbmsServerPassword").val();
		//alert("rdbmsServerPassword::"+rdbmsServerPassword);
		jdbcDriverClassname = jdbcDriverId;	
		rdbmsInstanceName = 'SQLEXPRESS';
		//alert("rdbmsInstanceName::"+rdbmsInstanceName);
		
		if (rdbmsIsWindowsAuth == 0) 
				{
				  if (rdbmsServerPort == "") 
				  {
				    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
				    + rdbmsServerMachineName
				    + ";databaseName="
				    + rdbmsDatabaseName + ";";
				  } 
				  else 
				  {
				    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
				    + rdbmsServerMachineName + ":"
				    + rdbmsServerPort
				    + ";databaseName="
				    + rdbmsDatabaseName + ";";
				  }
				}

				if (rdbmsIsWindowsAuth == 1) 
				{
				//alert("rdbmsIsWindowsAuth 1")
				  if (rdbmsServerPort == "") 
				  {
				  //alert("rdbmsServerPort null")
				    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
				    + rdbmsServerMachineName
				    + ";databaseName="
				    + rdbmsDatabaseName
				    + ";integratedSecurity=true";
				  } 
				  else 
				  {
					//alert("in else ")
				    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
				    + rdbmsServerMachineName + ":"
				    + rdbmsServerPort
				    + ";instanceName="
				    + rdbmsInstanceName
				    + ";databaseName="
				    + rdbmsDatabaseName
				    + ";integratedSecurity=true";
					//alert("rdbmsJdbcDriverUrl ::"+rdbmsJdbcDriverUrl)
				  }
				}
			
		
		var responseHTML = "";
			var url = 'AddNewDatasourceInstanceSaveAction.action';
			var IprodReportsDatasourceInstanceAction={
			rdbmsIsWindowsAuth : rdbmsIsWindowsAuth,
			//datasourceTypeId : datasourceTypeId,
			rdbmsId : rdbmsId,
			datasourceName : datasourceName,
			//databaseId : jdbcrdbmsDatabaseName,
			//jdbcDriverId : jdbcDriverId,
			rdbmsDatabaseName : rdbmsDatabaseName,
			rdbmsJdbcDriverUrl : rdbmsJdbcDriverUrl,
			rdbmsInstanceName : rdbmsInstanceName,
			rdbmsServerMachineName : rdbmsServerMachineName,
			rdbmsServerPort : rdbmsServerPort,
			rdbmsServerUsername : rdbmsServerUsername,
			rdbmsServerPassword : rdbmsServerPassword
			};
			$.ajax({
			type : "GET",
			url : url,
			cache : false,
			async : false,
			dataType : 'html',
			data:"data="+JSON.stringify(IprodReportsDatasourceInstanceAction),
			contentType:'json',
			success : function(data) {
			//alert("data:: "+data);
			alert("Datasource Instance Added Successfully!!!!");
			
			var sUrl ='../iProdReports/ManageDataSourceMenuAction.action';
			window.location.replace(sUrl);
			
			$('#viewDataSourcePopUp').fadeOut("normal");
			$('#backgroundPopup').fadeOut("normal");


			},
			error : function(e) {

			alert("Error: " + e);
			}
			});

		
		$('#datasourceValidateResultDiv')
		.html(responseHTML);
	//} 
	//else {
	//alert("in else.......................");
	//}
});// function for getting values of Datasource


//UPDATE DATASOURCE

$("#updateDatasource").on("click",function() {
		datasourceId = $("#datasourceId").val();
		datasourceName = $("#datasourceName").val();
		rdbmsId = $('#rdbmsId').val();
		rdbmsIsWindowsAuth = $("#rdbmsIsWindowsAuth").val();
		jdbcDriverId = 'com.microsoft.sqlserver.jdbc.SQLServerDriver';
		rdbmsDatabaseName = $('#rdbmsDatabaseName').val();
		rdbmsServerMachineName = $("#rdbmsServerMachineName").val();
		rdbmsServerPort = $("#rdbmsServerPort").val();
		rdbmsServerUsername = $("#rdbmsServerUsername").val();
		rdbmsServerPassword = $("#rdbmsServerPassword").val();
		jdbcDriverClassname = jdbcDriverId;	
		rdbmsInstanceName = 'SQLEXPRESS';		
		if (rdbmsIsWindowsAuth == 0) {
			  if (rdbmsServerPort == "") 
			  {
			    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
			    + rdbmsServerMachineName
			    + ";databaseName="
			    + rdbmsDatabaseName + ";";
			  } 
			  else 
			  {
			    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
			    + rdbmsServerMachineName + ":"
			    + rdbmsServerPort
			    + ";databaseName="
			    + rdbmsDatabaseName + ";";
			  }
		}

		if (rdbmsIsWindowsAuth == 1) {
				//alert("rdbmsIsWindowsAuth 1")
		  if (rdbmsServerPort == "") {
		  //alert("rdbmsServerPort null")
		    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
		    + rdbmsServerMachineName
		    + ";databaseName="
		    + rdbmsDatabaseName
		    + ";integratedSecurity=true";
		  } 
		  else {
			//alert("in else ")
		    rdbmsJdbcDriverUrl = "jdbc:sqlserver://"
		    + rdbmsServerMachineName + ":"
		    + rdbmsServerPort
		    + ";instanceName="
		    + rdbmsInstanceName
		    + ";databaseName="
		    + rdbmsDatabaseName
		    + ";integratedSecurity=true";
			//alert("rdbmsJdbcDriverUrl ::"+rdbmsJdbcDriverUrl)
		  }
		}
			
		
		var responseHTML = "";
			var url = 'UpdateDatasourceInstanceSaveAction.action';
			var IprodReportsDatasourceInstanceAction={
			datasourceId : datasourceId,
			rdbmsIsWindowsAuth : rdbmsIsWindowsAuth,
			//datasourceTypeId : datasourceTypeId,
			rdbmsId : rdbmsId,
			datasourceName : datasourceName,
			//databaseId : jdbcrdbmsDatabaseName,
			//jdbcDriverId : jdbcDriverId,
			rdbmsDatabaseName : rdbmsDatabaseName,
			rdbmsJdbcDriverUrl : rdbmsJdbcDriverUrl,
			rdbmsInstanceName : rdbmsInstanceName,
			rdbmsServerMachineName : rdbmsServerMachineName,
			rdbmsServerPort : rdbmsServerPort,
			rdbmsServerUsername : rdbmsServerUsername,
			rdbmsServerPassword : rdbmsServerPassword
			};
			$.ajax({
			type : "GET",
			url : url,
			cache : false,
			async : false,
			dataType : 'html',
			data:"data="+JSON.stringify(IprodReportsDatasourceInstanceAction),
			contentType:'json',
			success : function(data) {
			alert("Datasource Updated Successfully!!!!");			
			var sUrl ='../iProdReports/ManageDataSourceMenuAction.action';
		//	alert(sUrl);
			window.location.replace(sUrl);
			},
			error : function(e) {
			}
			});

});

});
