$(function(){
	var emailConfiguredValue;	
	$('#templateNameEntryDivError').hide();		
	$('#enterSqlQueryResult').hide();
	$('#rtemplateId').on("change",function(){
		var datasource = $('#rtemplateId :selected').text();
		if(datasource == 'Select Template'){
		}
		else{
		 $('#templateNameEntryDivError').hide();	
		}
	});

/**
Validation on sql query - that is executed against the database on the click of the NEXT button on the TAB
*/


// $('#validateTabFields').on('click',function(evt,data){
$('#generateReportTemplateWizard').on('actionclicked.fu.wizard', function (evt, data) {
var active = $('#generateReportTemplateWizard').wizard('selectedItem');
	if(active.step == 1){
		var datasource = $('#rtemplateId :selected').text();
		if (datasource == 'Select Template'){
			 $('#templateNameEntryDivError').show();	
			 $('#templateNameEntryDivError').html('<i class="fa fa-times-circle sign"></i><strong>ERROR!!!</strong>You need to select a REPORT TEMPLATE to proceed with report creation!!!');
			 evt.preventDefault();
		} 				
		else{
			var rtemplateId=$('#rtemplateId').val();
			//alert(rtemplateId);
			jQuery.GenerateReportTemplateTab.getSelectReportTemplateDetails(rtemplateId);
			var datasourceId = $('#datasourceId').val();	
			var reportQueryWithFilters = $('#reportQueryWithFilters').val();
			jQuery.GenerateReportTemplateTab.getSQLQueryColumnsAndDataType(reportQueryWithFilters,datasourceId,rtemplateId);
			$('#templateNameEntryDivError').hide();	
		}
	}	

	if(active.step == 2){    
			//alert($('#builder').queryBuilder('getSQL', false));
			// plan SQL with linebreaks			
			var sqlQueryReport = $('#reportQueryWithFilters').val();
			//alert("queryValue==="+sqlQueryReport);
			if (sqlQueryReport == ""){
				$('#enterSqlQueryResult').show();
				$('#enterSqlQueryResult').html('<i class="fa fa-times-circle sign"></i><strong>ERROR!!!</strong> Enter SQL Query!!!');
				preventNextTab();
			} 	 
	}

	if(active.step == 3){
		var sqlQuery = $('#reportQueryWithFilters').val();
		sqlQuery = sqlQuery.replace(/(\r\n|\n|\r)/gm,' ');
		sqlQuery = sqlQuery.replace(/\s{2,}/g,' ');
		if (sqlQuery == ""){
			$('#sqlQueryValidateDivError').show();
			$('#sqlQueryValidateDivError').html('<i class="fa fa-times-circle sign"></i><strong>ERROR :: </strong> You need to enter a valid SQL Query to create a DATASET!!!');
			evt.preventDefault();
		}
		if (sqlQuery != ""){ 
			if(($('#generateReportSqlQueryValidated').text()) != "VALIDATED"){
			  $('#sqlQueryValidateDivWarning').show();
			  $('#sqlQueryValidateDivWarning').html('<i class="fa fa-warning sign"></i><strong>WARNING : </strong>Entered SQL Query needs to be validated to construct the DATASET!!!');
			  evt.preventDefault();
			}else{
			  $('#sqlQueryValidateDivSuccess').show();
			  $('#sqlQueryValidateDivSuccess').html('<i class="fa fa-check sign"></i><strong>SUCCESS : </strong>Entered SQL Query has been successfully validated!!!');
			}
		}
	}		   
});


function preventNextTab(){
	alert('preventing next tab');
    evt.preventDefault();
    $(this).tab('show');
}

}); 