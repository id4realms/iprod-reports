jQuery.CreateReportTemplateTab = {


displayColumnDataTypeAndAlias : function(selectedOptionDataField,masterTableRowIndex,subTableRowIndex){
	 //alert("selectedOptionDataField:"+selectedOptionDataField);
	 var datasourceId=$('#datasourceId').val();
	 //alert("DATASOURSE INSTANCE ID"+datasourceInstanceId);
	
		sqlQuery = $("#rtemplateSqlquery").val();
		sqlQuery = sqlQuery.replace(/(\r\n|\n|\r)/gm,' ');
		sqlQuery = sqlQuery.replace(/\s{2,}/g,' ');
		$("#sqlQueryExecuteResultDiv").html('');
	

		var iprodReportSqlQueryDetails= {
			rtemplateSqlquery : sqlQuery,
			dataFieldName : selectedOptionDataField,
			datasourceId : datasourceId
		};

		var result;
		$.ajax({
		    url : 'GetSelectedSQLColumnDataTypeAction',
			cache : false,
			async : false,
			data :  "inputJSONData="+JSON.stringify(iprodReportSqlQueryDetails),
			dataType : "json",
			contentType : "application/json",
			 
			success: function(data) {
				
			jsonSQLColumnDataTypeResult=jQuery.parseJSON(data.jsonSQLColumnDataType);
				//alert("result:"+result);			
			}
		});
		
		//alert(result.toString().toUpperCase());
		$('input[name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnType"]').val(jsonSQLColumnDataTypeResult.toString().toUpperCase());
		$('input[name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnAlias"]').val(selectedOptionDataField.toString().toUpperCase());
},


createDataSourceColumns : function(masterTableRowIndex,subTableRowIndex){
	//alert("building column select");
	var columnValues=[];
    columnValues.push("SELECT DB COLUMN");
	$('#mainTableSqlQueryResult thead tr:first th').each(function(){   
	columnValues.push($(this).html());     
	}); 

    var selectElement ='<select id="reportGridColumnName" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable['+subTableRowIndex+'].columnName" class="form-control">';
	for(var i=0; i<columnValues.length; i++){	 
		if(columnValues[i]=="-- SELECT DB COLUMN --"){
			selectElement += '<option value="'+0+ '">' + columnValues[i] + '</option>';
		}
		else{
			selectElement += '<option value="'+ columnValues[i] + '">' + columnValues[i] + '</option>';
		}	
	} 
	
	selectElement +='</select>';

	//alert(selectElement);
	return selectElement;
},

createTemplateColumnSingletonFirstRow : function(masterTableRowIndex,subTableRowIndex){
			var datasourceColNameDropdownElement = jQuery.CreateReportTemplateTab.createDataSourceColumns(masterTableRowIndex,subTableRowIndex);
            var reportTemplateColumnGroupElem = '<tr class="reportTemplateGroupClass">' +
				'<td colspan="5">' +
				'<div class="table-responsive">' +
				'<table class="table no-border" name="templateColumnMasterTable[' + masterTableRowIndex + ']">' + 
				'<tr class="reportColumnSelectionGrid">' +  
				'<td style="width:5%"><input type="hidden" name="templateColumnMasterTable[' + masterTableRowIndex + '].columnGroupAlias" value="GRPALIASNONE"/><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRow" value="1" class="form-control" disabled/>' +
				'<input type="hidden" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRowHidden" value="1" class="form-control" /></td>' +
                '<td style="width:25%">' + datasourceColNameDropdownElement +'</td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnType" class="form-control" /></td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnAlias" class="form-control" /></td>' +  
                '<td style="width:20%">&nbsp;</td>' +    
                '</tr>' +
				'</table>' +
				'</div>' +
				'</td>' +
				'</tr>';
			
           /* $('#reportTemplateParmMasterTable').append(reportTemplateColumnGroupElem);*/ // Adding these controls to Main table class

			var selectElementName = "templateColumnMasterTable[" + masterTableRowIndex + "].templateColumnSubTable[" + subTableRowIndex +"].columnName";

			$('select[name="' + selectElementName + '"]').change(function(){
				//alert($(this).val()); 
				//alert($('select[name="' + selectElementName + '"] option:selected').val());
				jQuery.CreateReportTemplateTab.displayColumnDataTypeAndAlias($('select[name="' + selectElementName + '"] option:selected').val(),masterTableRowIndex,subTableRowIndex);
			});
			
},

createTemplateColumnGroupFirstRow : function(masterTableRowIndex,subTableRowIndex){
			var datasourceColNameDropdownElement = jQuery.CreateReportTemplateTab.createDataSourceColumns(masterTableRowIndex,subTableRowIndex);
            var reportTemplateColumnGroupElem = '<tr id="reportTemplateMasterGroupRow" class="reportTemplateGroupClass">' +
				'<td><table name="templateColumnMasterTable[' + masterTableRowIndex + ']">' +
				'<tr class="reportColumnSelectionGrid">' +  
				'<td><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRow" value="1" class="form-control" disabled/>' +
				'<input type="hidden" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRowHidden" value="1" class="form-control" /></td>' +
                '<td>' + datasourceColNameDropdownElement +'</td>' +  
                '<td><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnType" class="form-control" /></td>' +  
                '<td><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnAlias" class="form-control" /></td>' +  
                '<td>&nbsp;</td>' +    
                '</tr>' +
				'</table>' +
				'</td>' +
				'</tr>';
			
            $('#reportTemplateParmMasterTable').append(reportTemplateColumnGroupElem); // Adding these controls to Main table class

			var selectElementName = "templateColumnMasterTable[" + masterTableRowIndex + "].templateColumnSubTable[" + subTableRowIndex +"].columnName";

			$('select[name="' + selectElementName + '"]').change(function(){
				//alert($(this).val()); 
				//alert($('select[name="' + selectElementName + '"] option:selected').val());
				jQuery.CreateReportTemplateTab.displayColumnDataTypeAndAlias($('select[name="' + selectElementName + '"] option:selected').val(), masterTableRowIndex, subTableRowIndex);
			});
			
},

checkTemplateNameTabFirst : function(reportTemplateName){
	var flag=0;
	var rtemplateNameDetails={rtemplateName: reportTemplateName};
    $.ajax({
			url : "CheckExistingReportTemplateNameAction",
			cache : false,
			async : false,
			data : "inputJSONData="+JSON.stringify(rtemplateNameDetails),
			dataType : "json",
			contentType : "application/json",
			success : function(data){	
				//alert("templateNameResponse"+data.templateNameResponse);
				if(data.templateNameResponse == "Template Name is Available"){
				$('#templateNameCheckResult').html("Template Name is Available");
				flag=1;
				}
				if(data.templateNameResponse == "Template Name Exists"){
				$('#templateNameCheckResult').html("Template Name Exists");
				flag=0;
				}
			},
			failure : function(e){
				alert("error");
			}
	});
    return flag;
},


checkDbInstanceCheckboxValue : function(){
	var checkTablebody= $('#datatable tbody').children().length;
	var dbInstanceId;
	if(checkTablebody == 0){     	
		 dbInstance=1;
		 $('#selectDatasourceResult').show();
		 $('#selectDatasourceResult').html('<i class="fa fa-times-circle sign"></i><strong>ERROR : </strong> No Datasource Instance Available!!!');
		 e.preventDefault();
		 $(this).tab('show');
	}
	else{		
		var checkboxes = $("input[type='checkbox']");
		doc=(checkboxes.is(":checked"));
		if((checkboxes.is(":checked")) == false)
		{
		 $('#selectDatasourceResult').show();
		 $('#selectDatasourceResult').html('<i class="fa fa-times-circle sign"></i><strong>ERROR : </strong>Please select a datasource to proceed!!!');
		 e.preventDefault();
		 $(this).tab('show');
		}	
		if((checkboxes.is(":checked")) == true)
		{
			 $('#selectDatasourceResult').hide();
			 var checked = $("div#datasourceCBArray").find("input:checked").closest("tr").find("td:eq(0)");
			 $.each(checked,function() {
			 datasourceId=$(this).text();
		});
		}
	}			
	return datasourceId;
},	
		
/** Function to validate sql query with database and throw back sql exception if validation fails
*/
checkSqlQueryValidation :function(sqlQuery,datasourceId){
 var iprodReportsViewReportSqlQueryDetails= {
			rtemplateSqlquery: sqlQuery,
			datasourceId:datasourceId
 };
				
 $.ajax({																
	url : 'ValidateBuiltSqlQueryAction',
	cache : false,
	async : false,
	data :  "inputJSONData="+JSON.stringify(iprodReportsViewReportSqlQueryDetails),
	dataType : "json",
	contentType : "application/json",
	success: function(resp){
		$('#reportTemplateSqlQueryValidated').html(resp.isSQLQueryValidCheckMessage);
		var response = jQuery.parseJSON(resp.outputJSONData);
		var tableHeaderValues=[];
		var tableRowValuesOuter=[];
			$.each(response, function(i, e)
			{
				var tableRowValuesInner=[];
				$.each(e, function(key, val)
				{
					checkIndex=$.inArray(key, tableHeaderValues) > -1;
					if(checkIndex == false)
					{
						tableHeaderValues.push(key);
					}
					tableRowValuesInner.push(val);
				
				});
				tableRowValuesOuter.push(tableRowValuesInner);
			});
			$tableHeaderValues=addTableHeaders(tableHeaderValues);
			$tableRowValues=addRowValues(tableRowValuesOuter);
			$("#reportExecuteSqlQueryOutputDiv").append('<table id="mainTableSqlQueryResult" class="table table-bordered"><thead>'+$tableHeaderValues+'</thead><tbody id="sqlQueryResult">'+$tableRowValues+'</tbody></table>');
			// $('#sqlQueryResult').easyPaginate({step : 5});
	},
	error: function(response){
			console.log("FAILED");
			console.log(response);
	}
}); 

/** Add table header values in output 
*/
function addTableHeaders(tableHeaderValues){
	var $headerValues='<tr>';
	for(var i=0;i<tableHeaderValues.length;i++)
	{
	 $headerValues+='<th>'+tableHeaderValues[i]+'</th>';
	}
	$headerValues+='</tr>';
	return $headerValues;
}


/** Add table details values in output 
*/
function addRowValues(tableRowValuesOuter){
	var $tableRowList="";
	for(var i=0;i < tableRowValuesOuter.length;i++)
	{
	var tableRowValuesInner = tableRowValuesOuter[i];
	var $tableRowValues='<tr>';
	for(var j=0;j < tableRowValuesInner.length;j++)
	{
	 $tableRowValues+='<td>'+tableRowValuesInner[j]+'</td>';
	}

	$tableRowValues+='</tr>';
	$tableRowList+=$tableRowValues;
	}
	return $tableRowList; 
		
}

},
};