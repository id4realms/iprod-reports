jQuery.GenerateReportTemplateTab = {
	createGenerateQueryFilter : function(jsonObjFilters){
	var templateName = ($("#rtemplateId option:selected" ).text());
		jsonObjFilters = JSON.parse(jsonObjFilters);
		 var arr=[];
		 var cnt=0;
		 $.each(jsonObjFilters, function (key, value) {
		  var opt = document.createElement('option');	  
			
			opt.textContent = value.label;
			var opnValue = opt.textContent;
			opt.value = value.id;
			if(opnValue.includes("DATE") || opnValue.includes("TIME"))
			{
				arr.push(jsonObjFilters[cnt]);
				$('#orderByFilters').append(opt);
			}
			cnt++;
		  });
			jsonObjFilters=arr;
			$('#orderByFilters').multiselect();

			
			var whereBuilderOptions = {
		  allow_empty: true,
		  display_errors : false,

		  plugins: {
			'bt-tooltip-errors': { delay: 100},
			'sortable': null,
			'unique-filter': null,
			'bt-checkbox': { color: 'primary' },
			'invert': null
			
		  },
	
		  filters: [{
				  id: 'CAST(CONVERT(DATETIME,DATE_COL1,120) AS DATETIME)',
				  label: 'DATE_COL1',
				  type: 'string',
				  operators: ['between'],
				  plugin: 'datepicker',
				  plugin_config: {
				  format: 'mm/dd/yyyy',
				  todayBtn: 'linked',
				  todayHighlight: true,
				  autoclose: true
				}
			},
		
			{
				  id: 'CAST(CONVERT(DATETIME,TIME_COL1,120) AS DATETIME)',
				  label: 'TIME_COL1',
				  type: 'string',
				  operators: ['between'],
				  plugin: 'timepicker',
				  plugin_config: {
				  format: 'hh:mm:ss',
				  defaultTime:false
				}
			},
			{
				  id: 'Model',
				  label: 'Model',
				  type: 'string',
				  operators: ['equal']
			},
			{
				  id: 'Batch_No',
				  label: 'Batch_No',
				  type: 'string',
				  operators: ['equal']
			}
			
			]
			};

		// init
				$('#whereBuilder').queryBuilder(whereBuilderOptions);
				//$('#whereBuilder').queryBuilder('addFilter',jsonObjFilters,'name');		  
				//$('#whereBuilder').queryBuilder('removeFilter', 'name');
				if(templateName.includes("HOURLY") || templateName.includes("hourly") || templateName.includes("Hourly")){
				$('#whereBuilder').queryBuilder('setRules',{
				condition:'AND',
				"rules" :[{		
					id: 'CAST(CONVERT(DATETIME,DATE_COL1,120) AS DATETIME)',
					//field: 'DATE_COL1',
					operator: 'between',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}					
					
				},
				{		
					id: 'CAST(CONVERT(DATETIME,TIME_COL1,120) AS DATETIME)',
					//field: 'TIME_COL1',
					operator: 'between',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}				
				}
			]
		});
		}
		
		else if((templateName.includes("BATCH") || templateName.includes("batch") || templateName.includes("Batch"))  && (templateName.includes("DAILY") || templateName.includes("daily") || templateName.includes("Daily")) && (templateName.includes("report") || templateName.includes("REPORT") || templateName.includes("Report"))){
			$('#whereBuilder').queryBuilder('setRules',{
				condition:'AND',
				"rules" :[{		
					id: 'CAST(CONVERT(DATETIME,DATE_COL1,120) AS DATETIME)',
					//field: 'DATE_COL1',
					operator: 'between',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}					
					
				},
				{		
					id: 'CAST(CONVERT(DATETIME,TIME_COL1,120) AS DATETIME)',
					//field: 'TIME_COL1',
					operator: 'between',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}				
				},
				{		
					id: 'Model',
					//field: 'Model',
					operator: 'equal',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}				
				},
				{		
					id: 'Batch_No',
					//field: 'Batch_No',
					operator: 'equal',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}				
				}
			]
		});
			
			
		}else if( (templateName.includes("BATCH") || templateName.includes("batch") || templateName.includes("Batch"))&& (templateName.includes("report") || templateName.includes("REPORT") || templateName.includes("Report")) && !((templateName.includes("DAILY") || templateName.includes("daily") || templateName.includes("Daily"))) ){
			$('#whereBuilder').queryBuilder('setRules',{
				condition:'AND',
				"rules" :[{		
					id: 'CAST(CONVERT(DATETIME,DATE_COL1,120) AS DATETIME)',
					//field: 'DATE_COL1',
					operator: 'between',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}					
					
				},
				{		
					id: 'CAST(CONVERT(DATETIME,TIME_COL1,120) AS DATETIME)',
					//field: 'TIME_COL1',
					operator: 'between',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}				
				},
				{		
					id: 'Model',
					//field: 'Model',
					operator: 'equal',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}				
				}
				
			]
		});
			
			
		}
		else if(templateName.includes("DAILY") || templateName.includes("daily") || templateName.includes("Daily")){
			$('#whereBuilder').queryBuilder('setRules',{
				condition:'AND',
				"rules" :[{		
					id: 'CAST(CONVERT(DATETIME,DATE_COL1,120) AS DATETIME)',
					//field: 'DATE_COL1',
					operator: 'between',
					value: [, ],
					flags:{					
					  condition_readonly: true,
					  no_add_rule: false,
					  no_add_group: false,
					  no_delete: true					
					}					
				}
			]
		});
		}else{
			
		}

		// add a filter at position 2 to n

		$('#showFilter').on('click', function() {
		// first validate the WHERE/HAVING clause builder and append to base query
		console.log("showFilter");
		var filtersToAppend = "";
		if($('#whereBuilder').queryBuilder('validate') == true){
			var sql_raw = $('#whereBuilder').queryBuilder('getSQL', false);
			if (($('#reportQueryOriginal').val().toLowerCase().indexOf("order by") < 0) && ($('#reportQueryOriginal').val().toLowerCase().indexOf("where") < 0) && ($('#reportQueryOriginal').val().toLowerCase().indexOf("having") < 0)){
				if ($('#reportQueryOriginal').val().toLowerCase().indexOf("group by") >= 0)
					filtersToAppend = ' HAVING ' +sql_raw.sql;
				else
					filtersToAppend = ' WHERE ' +sql_raw.sql;
			}else{
				alert("Sorry! The base query already having one of WHERE, HAVING, or ORDER BY which is not allowed!!");
                $('#whereBuilder').queryBuilder('reset');
			}

		}

		if($('#orderByFilters').val() != null){
			if ($('#reportQueryOriginal').val().toLowerCase().indexOf("order by") < 0){
                 filtersToAppend += ' ORDER BY '+ $('#orderByFilters').val();
				 
			}else{
				alert("Sorry! The base query already having SORTING clause ORDER BY which is not allowed!!");
				$("#orderByFilters :selected").prop("selected", false);
				$('#orderByFilters').multiselect('refresh');
			}
		}
		
		if(filtersToAppend!=""){
        $('#reportQueryFrozen').val($('#reportQueryOriginal').val()+filtersToAppend);
        $('#reportQueryWithFilters').val($('#reportQueryOriginal').val()+filtersToAppend);
		}
		
	       
	$('#generateReportSqlQueryValidated').html("");
	$('#reportExecuteSqlQueryOutputDiv').html("");
	$('#sqlQueryValidateDivSuccess').hide();
	$('#sqlQueryValidateDivError').hide();
	$('#sqlQueryValidateDivWarning').hide();
  
	var rtemplateId=$('#rtemplateId').val();
	var datasourceId=$('#datasourceId').val();
	var sqlQuery = $('#reportQueryWithFilters').val();

	
	//Get the SQL Query from the input form and remove all special characters
	sqlQuery = sqlQuery.replace(/(\r\n|\n|\r)/gm,' ');
	sqlQuery = sqlQuery.replace(/\s{2,}/g,' ');

	if (sqlQuery == ""){
			$('#sqlQueryValidateDivError').show();
			$('#sqlQueryValidateDivError').html('<i class="fa fa-times-circle sign"></i><strong>ERROR :: </strong> You need to enter a valid SQL Query to create a DATASET!!!');
			evt.preventDefault();
	}
	
	if (sqlQuery != ""){ 
		var sqlQueryExecutionResult = jQuery.GenerateReportTemplateTab.checkSqlQueryValidation(sqlQuery,datasourceId,rtemplateId);
		//$('#enterSqlQueryResult').hide();

		var resultDatasourceInstanceId=$('#datasourceId').val();
			if(($('#generateReportSqlQueryValidated').text()) != "VALIDATED"){
			  $('#sqlQueryValidateDivWarning').show();
			  $('#sqlQueryValidateDivWarning').html('<i class="fa fa-warning sign"></i><strong>WARNING : </strong>Entered SQL Query needs to be validated to construct the DATASET!!!');
			  evt.preventDefault();
			}else{
			  $('#sqlQueryValidateDivSuccess').show();
			  $('#sqlQueryValidateDivSuccess').html('<i class="fa fa-check sign"></i><strong>SUCCESS : </strong>Entered SQL Query has been successfully validated!!!');
			}
	}	

		
		});


        $('#resetFilter').on('click', function() {
			$('#whereBuilder').queryBuilder('reset');
			$("#orderByFilters :selected").prop("selected", false);
			$('#orderByFilters').multiselect('refresh');
			$('#reportQueryFrozen').val($('#reportQueryOriginal').val());
			$('#reportQueryWithFilters').val($('#reportQueryOriginal').val());
		});

		return true;
	},

	

	getSelectReportTemplateDetails : function(rtemplateId){
		 var rtemplateIdDetails={rtemplateId: rtemplateId};			 			
		 $.ajax({							
			url : "ValidateSelectedReportTemplateAction",
			cache : false,
			async : false,
			data : "inputJSONData="+JSON.stringify(rtemplateIdDetails),
			dataType : "json",
			contentType : "application/json",
			success : function(resp){
					//alert("SUCCESS:");
					var response=jQuery.parseJSON(resp.outputJSONData);
					//alert(resp.outputJSONData);
					$.each(response, function(key, value){
					//$('#rtemplateIsSplit').val(value.RTEMPLATE_IS_SPLIT);
					//$('#reportBirtCount').val(value.REPORT_BIRT_COUNT);
					$('#dbServerUsernameHidden').val(value.DATABASE_SERVER_USERNAME);
					$('#dbServerPasswordHidden').val(value.DATABASE_SERVER_PASSWORD);
					$('#dbInstanceNameHidden').val(value.DATABASE_INSTANCE_NAME);
					$('#datasourceId').val(value.DATASOURCE_ID);	
					$('#jdbcDriverUrlHidden').val(value.JDBC_DRIVER_URL);
					$('#jdbcDriverClassnameHidden').val(value.JDBC_DRIVER_CLASSNAME);
					$('#dsTypeNameHidden').val(value.DATASOURCE_TYPE_NAME);	
					$('#reportQueryFrozen').val(value.RTEMPLATE_SQLQUERY);
					$('#reportQueryWithFilters').val(value.RTEMPLATE_SQLQUERY);
					$('#reportQueryOriginal').val(value.RTEMPLATE_SQLQUERY);
					//alert("queryResult=="+$('#reportQuery').val());
					});
			},
			failure : function(e){
				alert("error");
			}
			
		});
    return true;
	},
	
	getSQLQueryColumnsAndDataType : function(sqlQuery,datasourceId,rtemplateId){
		var inputDataSourceCredentials = {
				rtemplateSqlquery: sqlQuery,
				datasourceId:datasourceId,
				rtemplateId:rtemplateId
		};		 			
		 $.ajax({							
			url : "GetListOfSQLColumnsAndDataTypeAction",
			cache : false,
			async : false,
			data : "inputJSONData="+JSON.stringify(inputDataSourceCredentials),
			dataType : "json",
			contentType : "application/json",
			success : function(resp){
					jQuery.GenerateReportTemplateTab.createGenerateQueryFilter(resp.outputJSONData);
			},
			failure : function(e){
				alert("error");
			}
			
		});
   return true;
	},
	
	 /** Function to validate sql query with database and throw back sql exception if validation fails
	*/
	checkSqlQueryValidation :function(sqlQuery,datasourceId,rtemplateId){

	 var iprodReportsViewReportSqlQueryDetails= {
				rtemplateSqlquery: sqlQuery,
				datasourceId:datasourceId,
				rtemplateId:rtemplateId
	 };
					
	 $.ajax({																
		url : 'ValidateReportSqlQueryAction',
		cache : false,
		async : false,
		data :  "inputJSONData="+JSON.stringify(iprodReportsViewReportSqlQueryDetails),
		dataType : "json",
		contentType : "application/json",
		success: function(resp){
		$('#generateReportSqlQueryValidated').html(resp.isSQLQueryValidCheckMessage);
			var response = jQuery.parseJSON(resp.outputJSONData);
			var tableHeaderValues=[];
			var tableRowValuesOuter=[];
				$.each(response, function(i, e){
					var tableRowValuesInner=[];
					$.each(e, function(key, val){
						checkIndex=$.inArray(key, tableHeaderValues) > -1;
						if(checkIndex == false){
							tableHeaderValues.push(key);
						}
						tableRowValuesInner.push(val);
					});
					tableRowValuesOuter.push(tableRowValuesInner);
				});
			$tableHeaderValues=addTableHeaders(tableHeaderValues);
			$tableRowValues=addRowValues(tableRowValuesOuter);
			$("#reportExecuteSqlQueryOutputDiv").append('<table id="mainTableSqlQueryResult" class="table table-bordered"><thead>'+$tableHeaderValues+'</thead><tbody id="sqlQueryResult">'+$tableRowValues+'</tbody></table>');
		},
		error: function(response){
				console.log("FAILED");
				console.log(response);
		}
	}); 

	/** Add table header values in output 
	*/
	function addTableHeaders(tableHeaderValues){
		var $headerValues='<tr>';
		for(var i=0;i<tableHeaderValues.length;i++){
		 $headerValues+='<th>'+tableHeaderValues[i]+'</th>';
		}
		$headerValues+='</tr>';
		return $headerValues;
	}


	/** Add table details values in output 
	*/
	function addRowValues(tableRowValuesOuter){
		var $tableRowList="";
		for(var i=0;i < tableRowValuesOuter.length;i++){
		var tableRowValuesInner = tableRowValuesOuter[i];
		var $tableRowValues='<tr>';
		for(var j=0;j < tableRowValuesInner.length;j++)
		{
		 $tableRowValues+='<td>'+tableRowValuesInner[j]+'</td>';
		}
		$tableRowValues+='</tr>';
		$tableRowList+=$tableRowValues;
		}
		return $tableRowList; 	
	}

	},
};