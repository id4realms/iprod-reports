/*jQuery('tbody.assemblyqueueTbody').on({
    'mouseover':  function () {*/
		jQuery(function($) {		
		var stationIdForWhichPopupisOpened = 0;

			/* event for close the popup */
			$("div.popupclose").hover(
							function() {
								//$('span.ecs_tooltip').show();
							},
							function () {
		    					//$('span.ecs_tooltip').hide();
		  					}
						);
		
			$(this).keyup(function(event) {
				if (event.which == 27) { // 27 is 'Ecs' in the keyboard
					disablePopup($( "#stationIdForWhichPopupisOpened" ).val());  // function close pop up
				}
			});
		
		        $("div#backgroundPopup").click(function() {
				disablePopup($( "#stationIdForWhichPopupisOpened" ).val());  // function close pop up
			});
		
			$('a.livebox').click(function() {
				//alert('Hello World!');
			return false;
			});		
			
		}); // jQuery End


		$('.topopup').click(function(){	
		loadStationHistoryPopup();
		});

		$('.popupclose').click(function() {
			disableStationHistoryPopup();  // function close pop up
		});	

		function loading() {
		$("div.loader").show();
		}
		function closeloading() {
		$("div.loader").fadeOut('normal');
		}


		function loadDataSourcePopup(){ 
			closeloading(); // fadeout loading
			$("div#viewDataSourcePopUp").fadeIn(0500); // fadein popup div
			$("div#viewDataSourcePopUp").css({
				"overflow":"scroll",
				"height":"600px"
			});
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001);
		}

		/*ABOUT US*/
		function disableAboutUsPopup() {
		$("div#aboutUsContentDiv").fadeOut("normal");
		$("#backgroundPopup").fadeOut("normal");
		}

		function loadAboutUsPopup(){	
		closeloading(); // fadeout loading
		$("div#aboutUsContentDiv").fadeIn(0500); // fadein popup div
		$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
		$("#backgroundPopup").fadeIn(0001);
		}

		
		
		/*FOR LICENSE*/
		function disableEulaPopup() {
		$("div#eulaContentDiv").fadeOut("normal");
		$("#backgroundPopup").fadeOut("normal");
		}

		function loadEulaPopup(){	
		closeloading(); // fadeout loading
		$("div#eulaContentDiv").fadeIn(0500); // fadein popup div
		$("#backgroundPopup").fadeIn(0001);
		}
				
		function disableloadDataSourcePopup() {
			$("div#viewDataSourcePopUp").fadeOut("normal");
				$("#backgroundPopup").fadeOut("normal");
		}