$(function()
{
$('#sqlQueryValidateDivSuccess').hide();
$('#sqlQueryValidateDivError').hide();
$('#sqlQueryValidateDivWarning').hide();
$('#selectDatasourceResult').hide();
$('#templateNameEntryDivError').hide();
$('#templateNameEntryDivWarning').hide();
$('#chooseStyleResult').hide();
var datasourceId;

/**
Validation on template name - it must be greater than 4 chars in size and must not be duplicated
*/
$("#rtemplateName").on('keyup',function(){
	var reportTemplateName=$("#rtemplateName").val();
	if(reportTemplateName.length<=4){
	    $('#templateNameCheckResult').html("Template Name Should be more than 4 characters");
	} 
	else{   
		$('#templateNameEntryDivError').hide();
		$('#templateNameEntryDivWarning').hide();
		$('#templateNameCheckResult').html("");
		var templateNameResult=jQuery.CreateReportTemplateTab.checkTemplateNameTabFirst(reportTemplateName);
	}		
});

/**
Validation on sql query - that is executed against the database on the click of the NEXT button on the TAB
*/
$("#validateSqlQuery").on("click",function(){       
	$('#reportTemplateSqlQueryValidated').html("");
	$('#reportExecuteSqlQueryOutputDiv').html("");
	$('#sqlQueryValidateDivSuccess').hide();
	$('#sqlQueryValidateDivError').hide();
	$('#sqlQueryValidateDivWarning').hide();
  
	//Get the SQL Query from the input form and remove all special characters
	sqlQuery = $("#rtemplateSqlquery").val();
	sqlQuery = sqlQuery.replace(/(\r\n|\n|\r)/gm,' ');
	sqlQuery = sqlQuery.replace(/\s{2,}/g,' ');

	if (sqlQuery == ""){
			$('#sqlQueryValidateDivError').show();
			$('#sqlQueryValidateDivError').html('<i class="fa fa-times-circle sign"></i><strong>ERROR :: </strong> You need to enter a valid SQL Query to create a DATASET!!!');
			evt.preventDefault();
	}
	
	if (sqlQuery != ""){ 
		var sqlQueryExecutionResult = jQuery.CreateReportTemplateTab.checkSqlQueryValidation(sqlQuery,datasourceId);
		//$('#enterSqlQueryResult').hide();

		var resultDatasourceInstanceId=$('#datasourceId').val();
			if(($('#reportTemplateSqlQueryValidated').text()) != "VALIDATED"){
			  $('#sqlQueryValidateDivWarning').show();
			  $('#sqlQueryValidateDivWarning').html('<i class="fa fa-warning sign"></i><strong>WARNING : </strong>Entered SQL Query needs to be validated to construct the DATASET!!!');
			  evt.preventDefault();
			}else{
			  $('#sqlQueryValidateDivSuccess').show();
			  $('#sqlQueryValidateDivSuccess').html('<i class="fa fa-check sign"></i><strong>SUCCESS : </strong>Entered SQL Query has been successfully validated!!!');
			}
	}	
});

$('#clearSqlQuery').on('click',function(){
		       $('#rtemplateSqlquery').val("");
			   $('#sqlQueryValidateDivSuccess').hide();
			   $('#sqlQueryValidateDivError').hide();
			   $('#sqlQueryValidateDivWarning').hide();
			   $('#reportTemplateSqlQueryValidated').html("");
			   $('#reportExecuteSqlQueryOutputDiv').html("");			
});

// $('#validateTabFields').on('click',function(evt,data){
$('#createReportTemplateWizard').on('actionclicked.fu.wizard', function (evt, data) {
var active = $('#createReportTemplateWizard').wizard('selectedItem');
	if(active.step == 1)
	{
			var templateFormResult = $('#rtemplateName').parsley('options');
			
			if(templateFormResult.isValid() == false){        
					   $('#templateNameEntryDivError').show();
					   $('#templateNameEntryDivError').html('<i class="fa fa-times-circle sign"></i><strong>ERROR : </strong>You need to enter a template name to proceed!!!');
					   evt.preventDefault();
					   //$(this).tab('show');
			}else{
			    
				 var reportTemplateName=$("#rtemplateName").val();
				 if(reportTemplateName.length<=4){      
						$('#templateNameEntryDivWarning').show();
						$('#templateNameEntryDivWarning').html('<i class="fa fa-warning sign"></i><strong>WARNING : </strong>Selected Report Template Name must exceed 4 characters!!!');
						evt.preventDefault();
				 } 
				if(($('#templateNameCheckResult').text()) == "Template Name Already Exists"){     
						 $('#templateNameEntryDivError').show();
						 $('#templateNameEntryDivError').html('<i class="fa fa-times-circle sign"></i><strong>ERROR : </strong>Selected Report Template Name Already Exists!!!');
						 evt.preventDefault();
				}
				
			 }
	}	

	if(active.step == 2){    
		datasourceId=jQuery.CreateReportTemplateTab.checkDbInstanceCheckboxValue();
		$('#datasourceId').val(datasourceId); 	 
	}

	if(active.step == 3){
		var sqlQuery = $('#rtemplateSqlquery').val();
		sqlQuery = sqlQuery.replace(/(\r\n|\n|\r)/gm,' ');
		sqlQuery = sqlQuery.replace(/\s{2,}/g,' ');
		if (sqlQuery == ""){
			$('#sqlQueryValidateDivError').show();
			$('#sqlQueryValidateDivError').html('<i class="fa fa-times-circle sign"></i><strong>ERROR :: </strong> You need to enter a valid SQL Query to create a DATASET!!!');
			evt.preventDefault();
		}
		if (sqlQuery != ""){ 
			if(($('#reportTemplateSqlQueryValidated').text()) != "VALIDATED"){
			  $('#sqlQueryValidateDivWarning').show();
			  $('#sqlQueryValidateDivWarning').html('<i class="fa fa-warning sign"></i><strong>WARNING : </strong>Entered SQL Query needs to be validated to construct the DATASET!!!');
			  evt.preventDefault();
			}else{
			  $('#sqlQueryValidateDivSuccess').show();
			  $('#sqlQueryValidateDivSuccess').html('<i class="fa fa-check sign"></i><strong>SUCCESS : </strong>Entered SQL Query has been successfully validated!!!');
				
			  
			  jQuery.CreateReportTemplateTab.createTemplateColumnSingletonFirstRow(0,0);
			}
		}
	}		   

	if(active.step=='10'){
		 var reportTitle=$('#rtemplateTitle').val();
		 if(reportTitle==""){
				$('#chooseStyleResult').show();
				$('#chooseStyleResult').html('<i class="fa fa-times-circle sign"></i><strong>ERROR!!!</strong> Enter Report Title!!!');
				evt.preventDefault();
		 }
		 else{
				 $('#chooseStyleResult').hide();		 
		 }
	}
});


function preventNextTab(){
	alert('preventing next tab');
    evt.preventDefault();
    $(this).tab('show');
}

}); 