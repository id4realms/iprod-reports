// for Duplicate check
var selectElementNameList = [100];
for (var i = 0; i < 100; i++) {
  selectElementNameList[i] = [30];
}

var masterTableRowIndexG = 0;
var subTableRowIndexG = 0;

$(function(){
	
    function createTemplateSingletonRow(subTableRowIndex){
		//var rowCount = $('.reportColumnSelectionGrid').length + 1;
		var masterTableRowIndex =  $("#reportTemplateParmMasterTable  >tbody >tr").length;
		var datasourceColNameDropdownElement = jQuery.CreateReportTemplateTab.createDataSourceColumns(masterTableRowIndex,subTableRowIndex);
        var reportTemplateColumnGroupElem = '<tr class="reportTemplateGroupClass">' +
				'<td colspan="5">' +
				'<div class="table-responsive">' +
				'<table class="table no-border" id="reportTemplateParmSubTable" name="templateColumnMasterTable[' + masterTableRowIndex + ']">' + 
				'<tbody>'+
				'<tr class="reportColumnSelectionGrid">' +  
				'<td style="width:5%"><input type="hidden" name="templateColumnMasterTable[' + masterTableRowIndex + '].columnGroupAlias" value="GRPALIASNONE"/><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRow" value="1" class="form-control" disabled/>' +
				'<input type="hidden" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRowHidden" value="1" class="form-control" /></td>' +
                '<td style="width:25%">' + datasourceColNameDropdownElement +'</td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnType" class="form-control" /></td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnAlias" class="form-control" /></td>' +  
                '</tr>' +
				'</tbody>'+
				'</table>' +
				'</div>' +
				'</td>' +
				'</tr>';
			
            $('#reportTemplateParmMasterTable > tbody').append(reportTemplateColumnGroupElem); // Adding these controls to Main table class

			var selectElementName = "templateColumnMasterTable[" + masterTableRowIndex + "].templateColumnSubTable[" + subTableRowIndex +"].columnName";

			$('select[name="' + selectElementName + '"]').change(function(){
				//alert($(this).val()); 
				//alert($('select[name="' + selectElementName + '"] option:selected').val());
				findDuplicate(selectElementNameList, $(this).val());
				selectElementNameList[masterTableRowIndex][subTableRowIndex] = $(this).val();
				masterTableRowIndexG = masterTableRowIndex;
				subTableRowIndexG = subTableRowIndex;
				jQuery.CreateReportTemplateTab.displayColumnDataTypeAndAlias($('select[name="' + selectElementName + '"] option:selected').val(),masterTableRowIndex,subTableRowIndex);
			});
	}

    $(document).on("click", ".addColumnSingleton", function () {
	 createTemplateSingletonRow(0);          
    });  


	$(document).on("click", ".deleteColumnSingleton", function () {  
            $(this).closest(".reportTemplateGroupClass").remove(); // closest used to remove the respective 'tr' in which I have my controls   
	});
	
	 $(document).on("click", ".removeSingletonAndColumnGroup", function () 
	{
			selectElementNameList[masterTableRowIndexG] [subTableRowIndexG]= "";
			var rowCount = $('#reportTemplateParmMasterTable tr').length;
			if(rowCount > 4)
				$('#reportTemplateParmMasterTable tr:last').remove();
			else
				alert("There should be atleast one row in report");
	  });    

	
	function createTemplateColumnGroupRow(subTableRowIndex){
			var masterTableRowIndex =  $("#reportTemplateParmMasterTable >tbody >tr").length;
			var datasourceColNameDropdownElement = jQuery.CreateReportTemplateTab.createDataSourceColumns(masterTableRowIndex,subTableRowIndex);

			var reportTemplateColumnGroupElem = '<tr id="reportTemplateMasterGroupRow" class="reportTemplateGroupClass">' +
				'<td colspan="5">' +
				'<div class="table-responsive">' +
				'<table class="table no-border" id="reportTemplateParmSubTable" name="templateColumnMasterTable[' + masterTableRowIndex + ']">' +
				'<tbody>' +
				'<tr class="reportColumnSelectionGrid">' +  
				'<td align="right"><strong>COLUMN GROUP ALIAS : </strong></td>' + 
				'<td colspan="3"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex +'].columnGroupAlias" class="form-control"/>' +  
                '<td><button type="button" id="btnDelete" class="deleteColumnGroup btn btn btn-danger btn-xs">Remove Group</button><button type="button" id="btnAdd" class="addColumnSingletonInGroup btn btn btn-danger btn-xs">Add Sub</button></td>' +  
                '</tr>' +
				'<tr class="reportColumnSelectionGrid">' +  
				'<td style="width:5%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRow" value="1" class="form-control" disabled/>' +
				'<input type="hidden" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRowHidden" value="1" class="form-control" /></td>' +
                '<td style="width:25%">' + datasourceColNameDropdownElement +'</td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnType" class="form-control" /></td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnAlias" class="form-control" /></td>' +  
                '</tr>' +
				'</tbody>' +
				'</table>' +
				'</div>' +
				'</td>' +
				'</tr>';

            
            $('#reportTemplateParmMasterTable  > tbody').append(reportTemplateColumnGroupElem); // Adding these controls to Main table class

			var selectElementName = "templateColumnMasterTable[" + masterTableRowIndex + "].templateColumnSubTable[" + subTableRowIndex +"].columnName";

			$('select[name="' + selectElementName + '"]').change(function()
			{
				findDuplicate(selectElementNameList, $(this).val());
				selectElementNameList[masterTableRowIndex][subTableRowIndex] = $(this).val();
				masterTableRowIndexG = masterTableRowIndex;
				subTableRowIndexG = subTableRowIndex;
				// alert($(this).val()); 
				//alert($('select[name="' + selectElementName + '"] option:selected').val());
				jQuery.CreateReportTemplateTab.displayColumnDataTypeAndAlias($('select[name="' + selectElementName + '"] option:selected').val(),masterTableRowIndex,subTableRowIndex);
			});
	}

	$(document).on("click", ".deleteColumnGroup", function () 
	{  
		selectElementNameList[masterTableRowIndexG] = [30];
		$(this).closest(".reportTemplateGroupClass").remove(); // closest used to remove the respective 'tr' in which I have my controls
	});

    $(document).on("click", ".addColumnGroup", function () {
	 createTemplateColumnGroupRow(0);          
    });

	function createTemplateColumnSingletonInGroupRow(masterTableRowIndex,subTable){
			//alert(masterTableRowIndex);
			var subTableRowIndex = $(subTable).find('tr').length-1;
			var datasourceColNameDropdownElement = jQuery.CreateReportTemplateTab.createDataSourceColumns(masterTableRowIndex,subTableRowIndex);

			var reportTemplateColumnGroupElem = '<tr class="reportColumnSelectionGrid">' +  
				'<td style="width:5%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRow" value="1" class="form-control" disabled/>' +
				'<input type="hidden" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].seqRowHidden" value="1" class="form-control" /></td>' +
                '<td style="width:25%">' + datasourceColNameDropdownElement +'</td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnType" class="form-control" /></td>' +  
                '<td style="width:25%"><input type="text" name="templateColumnMasterTable[' + masterTableRowIndex + '].templateColumnSubTable[' + subTableRowIndex +'].columnAlias" class="form-control" /></td>' +  
                '</tr>';

            
            $(subTable).append(reportTemplateColumnGroupElem); // Adding these controls to Main table class

			var selectElementName = "templateColumnMasterTable[" + masterTableRowIndex + "].templateColumnSubTable[" + subTableRowIndex +"].columnName";

			$('select[name="' + selectElementName + '"]').change(function()
			{
				// alert($(this).val()); 
				//alert($('select[name="' + selectElementName + '"] option:selected').val());
				findDuplicate(selectElementNameList, $(this).val());
				selectElementNameList[masterTableRowIndex][subTableRowIndex] = $(this).val();
				masterTableRowIndexG = masterTableRowIndex;
				subTableRowIndexG = subTableRowIndex;
				jQuery.CreateReportTemplateTab.displayColumnDataTypeAndAlias($('select[name="' + selectElementName + '"] option:selected').val(),masterTableRowIndex,subTableRowIndex);
			});
	}

	$(document).on("click", ".deleteColumnSingletonFromGroup", function () {
		selectElementNameList[masterTableRowIndexG] [subTableRowIndexG]= "";
		$(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
	});

    $(document).on("click", ".addColumnSingletonInGroup", function () {
	 createTemplateColumnSingletonInGroupRow($('#reportTemplateParmMasterTable >tbody >tr').index($(this).closest('#reportTemplateMasterGroupRow')),$(this).closest("table"));          
    });

	
	$('#myCheckbox').on("click",function(){
		 $('#selectDatasourceResult').hide();
	});

	$('#rtemplateTitle').on("click",function(){
         $('#chooseStyleResult').hide();
	});

    
	$("#datasourceId").on("change", function() {
		var datasource = $('#datasourceId :selected').text();
		if (datasource == 'JDBC') {
			$('#topopup').show();
		} else {
			$('#topopup').hide();

		}
	});

	
	$(function() {
		$("[data-toggle='tooltip']").tooltip();

	});//for tooltip

	$('input#myCheckbox').on('change', function() {
		$('input#myCheckbox').not(this).prop('checked', false);
	});

	$(".pick-a-color").pickAColor();

	$("#rtemplateFooterLeft").change(function(){
	     readURL(this);
	});
	 
	$("#rtemplateFooterRight").change(function(){
         readURLFr(this);
	});
	 
	$("#rtemplateHeaderRight").change(function(){
	        readURLHr(this);
	});
	 
	 
	$("#rtemplateHeaderLeft").change(function(){
	        readURLHl(this);
	});

	
	function readURL(input) {
		if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#FooterLeftLogo').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
  }
       	
  function readURLFr(input){
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#FooterRightLogo').attr('src', e.target.result);
			}
			
			reader.readAsDataURL(input.files[0]);
		}
  }
   

  function readURLHr(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#HeaderRightLogo').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
  }
    
 function readURLHl(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#HeadderLeftLogo').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
       }
 }
 
 
 function findDuplicate(selectElementNameList, elementName){
	//console.log(selectElementNameList);
	var count = 0;
	while(count < selectElementNameList.length){
		var insideArray = selectElementNameList[count];
		var insideArrayCounter = 0;
		
		while(insideArrayCounter < insideArray.length){
			if(insideArray[insideArrayCounter] == elementName){
				alert(elementName + " is duplicate");
			}
			insideArrayCounter++;
		}
		
		count ++;
	}
 }
 
});