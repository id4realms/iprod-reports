
$("#rtemplateHeaderLeft").change(function (e) {
	var fileExt = document.getElementById("rtemplateHeaderLeft").value;
	var _URL = window.URL || window.webkitURL;
    var file, img,imageWidth,imageHeight,control;
    if ((file = this.files[0])) {
    	img = new Image();
        img.onload = function () {
			imageWidth=this.width;
			imageHeight=this.height;
			if(imageWidth>140 && imageHeight>140)
			{
				document.getElementById("maxSizeForHeaderLeft").textContent="Image Size cannot excede dimension 140*140";
				imageDupCheck();
				//document.getElementById("addImage").disabled = true;
				control = $("#rtemplateHeaderLeft");
				control.replaceWith( control = control.clone( true ) );
			}
			if(imageWidth>140 || imageHeight>140)
			{
				document.getElementById("maxSizeForHeaderLeft").textContent="Image Size cannot excede dimension 140*140";
				imageDupCheck();
				document.getElementById("addImage").disabled = true;
				control = $("#rtemplateHeaderLeft");
				control.replaceWith( control = control.clone( true ) );
			}
			else if(imageWidth<140 || imageHeight<140)
			{
				imageDupCheck();
				//document.getElementById("addImage").disabled = false;
			}
			
			else if(imageWidth<140 && imageHeight<140)
			{
				imageDupCheck();
				//document.getElementById("addImage").disabled = false;
			}

        };
        img.src = _URL.createObjectURL(file);	
		document.getElementById("maxSizeForHeaderLeft").textContent="";
		readURL(this);
		validate(fileExt);

    }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewLogo').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function validate(file) {
    var ext = file.split(".");
    ext = ext[ext.length-1].toLowerCase();      
    var arrayExtensions = ["jpg" , "jpeg", "png"];

    if (arrayExtensions.lastIndexOf(ext) == -1) {
		document.getElementById("maxSizeForHeaderLeft").textContent="Wrong extension type.";
        $("#rtemplateHeaderLeft").val("");
    }
}

