<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Add Datasource";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
<script type="text/javascript">
function userDupCheck(){
	$("#displayuserName").load('ajax_check_existing_datasource_instance_name.jsp',{datasourceName:document.getElementById('datasourceName').value});
  }
</script>
</head>
<body>
<div class="container-fluid" id="pcont">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Add Datasource<small></small></h3>
			<ul class="breadcrumb">
				<li>
					<i class="fa fa-link"></i>
					<a href="#">Quick links : </a>
				</li>
				<li>
					<i class="fa fa-home"></i>
					<a href="<s:url action="#"/>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				 <li>
					<a href="<s:url action="ManageDataSourceMenuAction"/>">Manage Datasource</a>
				</li>
			</ul>
 
			<div class="cl-mcont">
				<div id="popupbodycontent">
					<div class="popupclose"></div>
						<div class="row"><!---------START OF CLASS row------------------->
							<div class="col-sm-6 col-md-6">
								<div class="block-flat">	
									<div class="header">
										<h3>JDBC Datasource Details</h3>
									</div>
									<div class="content">
									  <form action="#" id="popDetailForm" name="popDetailFormName" data-validate="parsley"  >
									  <div class="form-group">
										  <label class="control-label">Database Profile Name</label>
										  <input type="text" id="datasourceName" required parsley-type="alphanum" maxlength="30" parsley-trigger="change" data-required="true"  placeholder="Enter Profile name" onkeyup="userDupCheck();" class="form-control" />
										  <span id="displayuserName"></span>
									  </div>
						
								<div class="form-group">
								<label class="col-sm-3 control-label">Select Database</label>
								<p>Choose one of various RBDMS supported</p>
								<s:select id="rdbmsId" name="rdbmsId" required="true" parsley-error-message="Please select RDBMS." parsley-required="true" list="allRdbmsListG" listValue="rdbmsName + '-' +rdbmsSubVersionName" listKey="rdbmsId" headerKey="" headerValue="Select RDBMS" parsley-trigger="change" cssClass="form-control"/>
								</div> 
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="block-flat">
									<div class="header">
										<h3>Build JDBC URL</h3>
									</div>
									
									<div class="content">
										<div class="form-group">
											<label class="control-label">Select JDBC Driver</label>
											<p>Choose one of JDBC Drivers supported</p>
											<select class="select2" id="jdbcDriverId" parsley-trigger="change" required>
											<option value="1">com.microsoft.sqlserver.jdbc.SQLServerDriver</option>
											</select>
										</div>
										
										
										<div class="form-group">
											<label>Database Name</label> 
											<input type="text" id="rdbmsDatabaseName" parsley-trigger="change" required placeholder="Enter database name" class="form-control"/>
										</div>
										
										<div class="form-group">
											<label>Server Name</label> 
											<input type="text" id="rdbmsServerMachineName" parsley-trigger="change" required placeholder="Enter server name" class="form-control"/>
										</div>
										<div class="form-group">
											<label>Port No</label> <input type="text" id="rdbmsServerPort" parsley-trigger="change" required	placeholder="Enter port number" class="form-control"/>
										</div>
										 <div class="form-group">
										  <div id="enableWindowsAuthLabelDiv">
										  <font color="red">*</font>Enable Windows Authentication
										  </div>
										  <div id="rdbmsIsWindowsAuth">
										  
										  
										<input type='radio' name='rdbmsIsWindowsAuth' checked="checked"  value='1' id="yes"/>Yes
										<input type='radio' name='rdbmsIsWindowsAuth' value='0' id="no"/>No
										  </div>
									  </div>
										<div class="form-group">
											<label>Username</label> <input type="text" id="rdbmsServerUsername" name="rdbmsServerUsername" parsley-trigger="change" required placeholder="Enter DB user name" disabled class="form-control"/>
										</div>
										<div class="form-group">
											<label>Password</label> <input type="password" id="rdbmsServerPassword" name="rdbmsServerPassword" parsley-trigger="change" required placeholder="Enter DB password" disabled class="form-control"/>
										</div>
										</form>
									</div>
								</div>
							</div><center>
							<div>
							<button id="validatePopUpForm" class="btn btn-primary" onclick="javascript:if($('#popDetailFormName').parsley('validate')==true) type="submit">Validate</button>
							 <!-- <button class="btn btn-default" id="cancelbutton">Cancel</button>   -->
							 <button class="btn btn-default" onclick="javascript:var f = document.forms['popDetailFormName'];f.action='ManageDataSourceMenuAction';f.submit();">Cancel</button>
							</div>
						</div>
				</div><!-- End of popupbodycontent -->
			</div><!-- End of cl-mcont -->
		</div>
	</div>			
</div>

<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/report/jdbc_Mssql.js"/>
<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript">
	document.getElementById('yes').onchange = displayTextBox;
	document.getElementById('no').onchange = displayTextBox;

	var textBox1 = document.getElementById('rdbmsServerUsername');
	var textBox2 = document.getElementById('rdbmsServerPassword');
	
	function displayTextBox(evt){
	if(evt.target.value=="1"){
	textBox1.disabled = true;
	textBox2.disabled = true;
	}else{
	textBox1.disabled = false;
	textBox2.disabled = false;
	}
	}
</script>
<script>
	$("#validatePopUpForm").on("click",function() 
	{
	});
</script>

</body>
</html>