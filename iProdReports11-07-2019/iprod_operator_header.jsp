<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="images/favicon.png">

<title>iProd</title>
<link href="js/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="css/popup_style.css" type="text/css" />
<link href="css/style.css" rel="stylesheet" />
</head>
<body>
	<div id="head-nav" class="navbar navbar-default navbar-fixed-top"
		style="height: 50px; min-height: 50px;">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="fa fa-gear"></span>
			</button>
			<a class="navbar-brand" href="#"><span>iPROD Reports</span></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="<s:url action="ManageReportsOperatorDashboardAction"/>">HOME</a></li>

				<ul class="nav navbar-nav">
					<li><a href="<s:url action="GenerateReportOperatorMenuAction"/>">CREATE REPORTS</a></li>
				</ul>
			</ul>

			<ul class="nav navbar-nav navbar-right user-nav">
				<li class="dropdown profile_menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"><%=(String) session.getAttribute("IprodFname")%>
						<%=(String) session.getAttribute("IprodLname")%> (<%=(String) session.getAttribute("IPRODUSERROLENAME")%>)<b
						class="caret"></b> </a>
					<ul class="dropdown-menu">
						<li><a href="ProfileAction">My Profile</a></li>
						<li class="about"><a href="#" class="topopup"
							id="aboutUsContentDiv">About iProd</a></li>
						<li class="about"><a href="#" class="topopup"
							id="eulaContentDiv">EULA</a></li>
						<li class="divider"></li>
						<li><a href="LogoutAction">Sign Out</a></li>
					</ul></li>
			</ul>


		</div>
	</div>

	<div class="aboutUsContent" id="aboutUsContentDiv">
		<div class="popupclose"></div>
		<span class="ecs_tooltip"><span class="arrow"></span></span> <br>
		<div id="aboutusheaderinfo">
			<img src="images/product-icon.png" />
			<h1 class="leelawade-font">
				iProd<sup><sup>&#0153 </sup> </sup>
			</h1>
		</div>
		<div id="versioninfo">
			iProd Reports<sup>&#0153</sup> Version 2.1
		</div>
		<br>
		<div id="copyrightinfo">&copy 2009-16 iD4 Realms Infotech Pvt
			Ltd.</div>
		<p id="aboutusinfo">The iProd name, associated trademarks and
			iprod logos and the product logo are trademarks of iD4 Realms or
			related entities.</p>
		<p align="center" id="warninginfo">Warning: This program is
			protected by copyright law and international treaties. Unauthorized
			reproduction or distribution of this program, or any portions of it,
			may result in severe civil and criminal penalties, and will be
			prosecuted to the maximum extent possible under the law.</p>
	</div>

	<div class="eulaContent" id="eulaContentDiv">
		<div class="popupclose"></div>
		<span class="ecs_tooltip"><span class="arrow"></span></span>
		<div class="popUpContentDiv" style="background-color: white;">
			<br>
			<table>

				<tr>
					<td class="span4-1"><span style="font-size: 26px;"
						align="center">LICENSE AGREEMENT</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">GRANT OF LICENSE</span>: You, the end
							user, are granted a non-exclusive SINGLE SERVER node-locked
							license to use IPROD PLANT DATA MANAGEMENT SYSTEM (“IPROD”) or
							(“IPROD SOFTWARE”) or (“IPRO SERVER PLATFORM”) collective terms
							which represents one of the sub-packages either of (“IPROD
							ANDON”) or (“IPROD WORKBENCH”) or (“IPROD REPORTS”) or (“IPROD
							WEB”) or CLIENT ACCESS LICENSE [“CAL”] under the terms stated in
							this agreement. This license allows you to run the IRPOD SERVER
							PLATFORM on one node [“COMPUTER”] and CLIENT ACCESS LICENSE
							[“CAL”] over Local Area Network [“LAN”] restricted to the
							sub-system packages subscribed and number of CLIENT ACCESS
							LICENSE specified in the project proposal. You may not rent or
							lease the software. IPROD PLATFORM grants you the right to use
							the Software as a Quality, Performance management, Production
							Optimization tool and for making copies of it for backup and
							archival purposes only.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"
						class="pull-right"></span><span style="font-size: 18px;"
						class="pull-right"><span style="font-size: 20px;">COPYRIGHT</span>:
							All title, intellectual property and proprietary rights,
							including trade secrets and presentation, in the software and any
							copies thereof and the accompanying electronic and written
							materials, are owned by ID4 REALMS INFOTECH PVT. LTD. [“ID4
							REALMS”] and are protected by copyright laws and international
							treaty provisions. ID4 REALMS retain all rights with respect to
							the software not expressly granted herein. ID4 REALMS also retain
							all rights to terminate the license without stating any reason.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">RESTRICTIONS</span>: You may not use,
							copy, merge, alter, adapt, modify, rent, lease or sublicense the
							software or the written materials, or any copy thereof, in whole
							or in part, except as expressly provided in this Agreement or
							under applicable statutes. You may not reverse-engineer,
							de-compile or disassemble the software. You may not modify, or
							create derivative works based upon the software, in whole or in
							part. You may not copy the software, or any portion thereof,
							except as specifically permitted by the terms of this agreement.
							The Software and its component parts may not be separated for
							use.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"
						class="pull-right"></span><span style="font-size: 18px;"
						class="pull-right"><span style="font-size: 20px;">TERMINATION</span>:
							The license is effective until terminated. Without prejudice to
							other rights, ID4 REALMS may terminate this license automatically
							if you fail to comply with any of the terms of this Agreement.
							Upon any termination of this Agreement, you agree to either
							return the Software to ID4 REALMS or destroy all copies of the
							Software, including, without limitation, any associated written
							materials.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">DISCLAIMER</span>: The software is not
							fault-tolerant. All decisions and actions made by you are your
							own sole responsibility. we make no guarantees or representations
							regarding the accuracy or significance. We cannot be held
							responsible for any interpretation, action or use that may be
							made. In no event shall we be liable for damages of any kind,
							including without limitation any compensatory, incidental,
							direct, indirect, special, punitive or consequential damages,
							loss of income or profit, damage to property, claims of third
							parties, or any other kind of losses. We cannot be held
							responsible for any claims regarding negative and adverse results
							as well. ID4 REALMS and its suppliers specifically disclaim any
							express or implied warranty of fitness.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">LIABILITY</span>: NO LIABILITY FOR
							CONSEQUENTIAL DAMAGES. TO THE MAXIMUM EXTENT PERMITTED BY
							APPLICABLE LAW, IN NO EVENT SHALL ID4 REALMS OR ITS SUPPLIERS BE
							LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL
							DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR
							LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS
							INFORMATION OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE
							OF OR INABILITY TO USE THE SOFTWARE PRODUCTS, EVEN IF ID4 REALMS
							HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. NO LIABILITY
							WHATSOEVER.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">LIMITATION OF LIABILITY</span>: BECAUSE
							SOME STATES AND JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR
							LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES,
							THE ABOVE RESTRICTION MAY NOT APPLY TO YOU. IN ANY CASE, ID4
							REALMS ENTIRE LIABILITY AND YOUR EXCLUSIVE REMEDY UNDER THIS EULA
							SHALL NOT EXCEED ONE INDIAN RUPEE (INR 1.00).</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">LIMITATION OF REMEDIES</span>: EXCEPT TO
							THE EXTENT EXPRESSLY REQUIRED BY APPLICABLE STATUTES, THE ENTIRE
							LIABILITY OF ID4 REALMS AND YOUR REMEDY SHALL BE LIMITED TO
							PRODUCT REPLACEMENT ONLY.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">INTERPRETATION</span>:If one or more
							provisions of this Agreement are held to be illegal or
							unenforceable under applicable law, such illegal or unenforceable
							portion(s) shall be limited or excluded from this Agreement to
							the minimum extent required so that this Agreement shall
							otherwise remain in full force and effect and enforceable in
							accordance with its terms.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right"><span
							style="font-size: 20px;">GOVERNING LAW</span>: This EULA will be
							governed by laws and rules of India, without regard to its choice
							of law principles. The United Nations Convention for the
							International Sale of Goods shall not apply. You agree to accept
							competent courts of Pune and India to have jurisdiction over any
							dispute arising out of your use of Software and this EULA. You
							and your end-users are solely responsible for use of the Software
							in your or their region or country. ID4 REALMS shall not be
							responsible for any violation of any statutory provisions
							applicable in your or your end-user’s region or country. You
							agree that this Agreement is the complete and exclusive statement
							of the mutual understanding between us and supersedes and cancels
							all previous written and oral agreements and communications
							relating to the subject matter of this Agreement. If any
							provision of this Agreement is found void, invalid or
							unenforceable, it will not affect the validity of the balance of
							this Agreement, which shall remain valid and enforceable
							according to its terms.</span></td>
				</tr>

				<tr>
					<td class="span4-1"><span style="font-size: 24px;"></span><span
						style="font-size: 18px;" class="pull-right">ID4 ID4 REALMS
							RESERVES ALL RIGHTS NOT SPECIFICALLY GRANTED IN THIS STATEMENT.
							YOU ACKNOWLEDGE THAT YOU HAVE READ THE TERMS OF THIS AGREEMENT
							AND AGREE TO BE BOUND BY ITS TERMS. SHOULD YOU HAVE ANY QUESTIONS
							CONCERNING THIS AGREEMENT, OR IF YOU DESIRE TO CONTACT ID4 REALMS
							FOR ANY REASON, CONTACT US AT info@id4-realms.com <br>
							<p align="center" style="color: blue;">Copyright© 2008-2016
								iD4 REALMS Infotech Pvt. Ltd. All Rights Reserved.</p>
					</span></td>
				</tr>

				</div>
			</table>
			<div></div>
		</div>

		<!-- End -->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/fuelux/loader.min.js"></script>
		<script type="text/javascript" src="js/report/popup_script.js"></script>
		<script type="text/javascript" src="js/active.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				$("a#aboutUsContentDiv").on('click', function(e) {
					e.preventDefault();
					loading(); // loading
					setTimeout(function() { // then show print summary popup, deley in .5 second
						loadAboutUsPopup();
					}, 500); // .5 second
					return false;
				});
				$("div.popupclose").on('click', function(e) {
					disableAboutUsPopup();
					// function close pop up
				});

				$("a#eulaContentDiv").on('click', function(e) {
					e.preventDefault();
					loading(); // loading
					setTimeout(function() { // then show print summary popup, deley in .5 second
						loadEulaPopup();
					}, 500); // .5 second
					return false;
				});
				$("div.popupclose").on('click', function(e) {
					disableEulaPopup();
					// function close pop up
				});
			});
		</script>
</body>
</html>