<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="/struts-tags" prefix="s" %>
	 <%@ page import ="java.util.*" %>
   <%@ page import ="java.text.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%if(session.getAttribute("IprodUserId") != null){%>
<html>
<head>
<script>document.title = "MANAGE SCHEDULERS";</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!--<meta HTTP-EQUIV="Refresh" CONTENT="10">-->
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection" />
<!--[if lt IE 8]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen, projection"><![endif]-->
<link rel="stylesheet" href="css/template-style.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="css/tabular-template-style.css" type="text/css" media="screen, projection" />
<link href="css/blitzer/jquery-ui-1.10.3.custom.css" rel="stylesheet">
<link rel="stylesheet" href="css/parsley-validations.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="css/dimming.css" type="text/css" media="screen, projection"/>
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="js/dimmingdiv.js"></script>

<script>
$(function() {
       	$( "input:button" ).button();
		$( "input:submit" ).button();
		$( "a.submit" ).button();
});

</script>

</head>
<body>
<div class="container">
  <div class="eventmesssages">
				<s:if test="hasActionMessages()">
					<div class="success-message">
						<img src="images/success.png" />
						<s:iterator value="actionMessages">
							<s:property />
						</s:iterator>
					</div>
				</s:if>
				<s:if test="hasActionErrors()">
					<div class="error-message">
						<img src="images/error.png" />
						<s:iterator value="actionErrors">
							<s:property />
						</s:iterator>
					</div>
				</s:if>
	</div>
<div class="container-subpage">
<div class="page-title"><div class="firstWord">Manage</div><div class="restWords">Schedulers</div>
<table class="pull-right">
<tr>
<td><img src="images/on.png"></td><td><strong>Enable Scheduler</strong></td>
<td>&nbsp;</td>
<td><img src="images/off.png"></td><td><strong>Disable Scheduler</strong></td>
</tr>
</table>
</div>
<table border="0" cellspacing="0" cellpadding="0" class="listingHead">
<thead>
<tr>
		<th class="span4">Scheduler Id</th>
		<th class="span4">Scheduler Name</th>
		<th class="span4">Trigger Name</th>
		<th class="span4">Scheduler State</th>
	</tr>
</thead>
  <tbody>
	  <s:iterator value="iprodSchedulerMasterListG" var="schedulerList">
		<tr class="even">
			<td class="span4"><s:property value="schedularId" /></td>
			<td class="span4"><s:property value="schedularName" /></td>
			<td class="span4"><s:property value="triggerName" /></td>
			<td  class="span2">
				<s:if test="#schedulerList.schedularStatus == 'ON'">
					<s:url id="stopIprodAutoTriggerUrl" action="StopIprodAutoTriggerAction" encode="true">
					<s:param name="triggerName" value="%{triggerName}" />
					</s:url>
					<s:a href="%{stopIprodAutoTriggerUrl}" title="Press to Stop" style="padding-left: 15px; padding-right: 15px;">
					<img src="images/off.png">
					</s:a>
				</s:if>

				<s:else>
					<s:url id="startIprodAutoTriggerUrl" action="StartIprodAutoTriggerAction" encode="true">
					<s:param name="triggerName" value="%{triggerName}" />
					</s:url>
					<s:a href="%{startIprodAutoTriggerUrl}" title="Press to Start" style="padding-left: 15px; padding-right: 15px;">
					<img src="images/on.png">
					</s:a>
				</s:else>
			</td>
		</tr>
	  </s:iterator>
  </tbody>
  </table>
  <div id="paging" style="float: right;"></div>
</div>
<div id="footerpush" class="footerpush"></div>
</div>
<%}else{%>
<jsp:forward page="index.jsp"></jsp:forward>
<%}%>
</body>
</html>