<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<body >
<div class="container-fluid" >
<div class="row-fluid" >
<div class="cl-mcont" >
	<div class="error-container">
		<div class="page-error">
		<h1 class="number text-center" style="color: black;"><img  src="images/IPROD_PDMS_ICON.png"/>401</h1>
		<h2 class="description text-center" style="color: black;">Sorry, but you don't have access to this page!</h2>
		<h3 class="text-center" style="color: black;">Would you like to go <a href="<s:url action="ManageReportsDashboardAction"/>">HOME</a>?</h3>
		</div>
		<div class="text-center copy" style="color: black;">&copy;<a href="#" style="color: black;">iD4-Realms 2009-2016 </a></div>	
		<h1 class="number text-center" style="color: black;"><img  src="images/iprod_logo_216_50.png"/></h1>
		</div>		
	</div>
</div>
</div>
	<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
</body>
</html>
