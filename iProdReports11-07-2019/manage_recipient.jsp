<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Manage Recipient";</script>
<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
</head>
<body>		
	<div class="container-fluid" id="pcont">
		<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">Manage Recipient <small>recipient details and more</small></h3>
				<ul class="breadcrumb">
				<li>
				<i class="fa fa-home"></i>
				<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
				</li>
				<li><a href="#">Manage Recipient</a></li>

				</ul>
				<div class="eventmessages">
					<s:if test="hasActionMessages()">
					<div class="success-message">
					<img src="images/success.png" />
					<s:iterator value="actionMessages">
					<s:property />
					</s:iterator>
					</div>
					</s:if>
					<s:if test="hasActionErrors()">
					<div class="error-message">
					<img src="images/error.png" />
					<s:iterator value="actionErrors">
					<s:property />
					</s:iterator>
					</div>
					</s:if>
				</div>
				<div class="cl-mcont">
					<s:set var="roleModuleOperationPermHashMap" value="hashMapCheckRoleModuleOperationListG" />
					<s:if test="#roleModuleOperationPermHashMap['CREATE_PERM'] == 1">
					<div class="button-panel">		
					<div class="button"><a href="<s:url action="AddNewRecipientMenuAction" encode="true"/>"><input id="add" type="button" class="inputButton" value="+ ADD RECIPIENT"/></a></div>
					</div>
					</s:if>
					<s:else>
					<div class="button-panel">	
					<div class="button"><input id="add" type="button" class="inputButton" value="+ ADD RECIPIENT" onclick="alert('You dont have permission to add recipient');"/></div>
					</div>	
					</s:else>
					</br>
					<div class="content">
						<div class="table-responsive">
							<table class="table table-bordered" id="datatable" >
							<thead>
							<tr>
								<th class="span4-1">FIRST NAME</th>
								<th class="span4-1">LAST NAME </th>
								<th class="span4-1">CONTACT NUMBER</th>
								<th class="span4-1">EMAIL ID</th>
								<th class="span4-1" id="cpnl">CONTROL PANEL</th>
							</tr>
							</thead>
							<tbody>
							<s:iterator value="AllRecipientDetailsListG" status="rowstatus">
								<tr class="odd gradeX">
									<td class="span4"><s:property value="firstName"/></td>
									<td class="span4"><s:property value="lastName"/></td>
									<td class="span4"><s:property value="contactNo"/></td>
									<td class="span4"><s:property value="emailId"/></td>
									<td class="span4">
									<s:if test="#roleModuleOperationPermHashMap['EDIT_PERM'] == 1">
									<s:url id="editUrl" action="EditRecipientMenuAction" encode="true">
									<s:param name="recipientId" value="%{recipientId}" />
									</s:url>
									<s:a href="%{editUrl}" title="Edit" style="padding-left: 15px; padding-right: 15px;"><i class="fa fa-pencil" width="16" height="16"></i></s:a>
									</s:if>
									<s:else>
										<s:a href="" title="Edit" style="padding-left: 15px; padding-right: 15px;" onclick="alert('You dont have permission to edit Recipient');"><i class="fa fa-pencil" width="16" height="16"></i></s:a>
									</s:else>
									<!--Delete button-->	
									<s:if test="#roleModuleOperationPermHashMap['DELETE_PERM'] == 1">
									<s:url id="deleteUrl" action="DeleteRecipientDetailsAction" encode="true">
									<s:param name="recipientId" value="%{recipientId}" />
									</s:url>
									<s:a href="%{deleteUrl}" onclick="return confirmDelete()" title="Delete" style="padding-left: 15px; padding-right: 15px;"><i class="fa fa-times-circle-o" width="16" height="16"></i></s:a>	
									</s:if>
									<s:else>
										<s:a href="" onclick="alert('You dont have permission to delete recipient')" title="Delete" style="padding-left: 15px; padding-right: 15px;"><i class="fa fa-times-circle-o" width="16" height="16"></i></s:a>	
									</s:else>
									</td>
								</tr>
							</s:iterator>
							</tbody>
							</table>				
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript">
	$(document).ready(function(){
	$('#datatable').DataTable();
	$('.dataTables_filter input').addClass('form-control').attr('placeholder','Search');
	$('.dataTables_length select').addClass('form-control'); 
	});

</script>

<script>  
	function confirmDelete() {  
	var answer = window.confirm("Are you sure you want to delete this record");  
	if(answer)
	return true;  
	else 
	return false;  
	}  
</script>



</body>
</html>
