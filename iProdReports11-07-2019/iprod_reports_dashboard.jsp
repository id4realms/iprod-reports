<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "iProd Reports Dashboard";</script>
<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
<title>iProd</title>
</head>
<body>		
	<div class="container-fluid" id="pcont">
		<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">Scheduled Report <small>scheduled report details</small></h3>
				<ul class="breadcrumb">
				<li>
				<i class="fa fa-home"></i>
				<a href="<s:url action="#"/>">Home</a>
				</li>
				<li><a href="#">Scheduled Report</a></li>

				</ul>
				<div class="eventmessages">
					<s:if test="hasActionMessages()">
					<div class="success-message">
					<img src="images/success.png" />
					<s:iterator value="actionMessages">
					<s:property />
					</s:iterator>
					</div>
					</s:if>
					<s:if test="hasActionErrors()">
					<div class="error-message">
					<img src="images/error.png" />
					<s:iterator value="actionErrors">
					<s:property />
					</s:iterator>
					</div>
					</s:if>
				</div>

				<div class="cl-mcont">
				<s:set var="roleModuleOperationPermHashMap" value="hashMapCheckRoleModuleOperationListG" />
					<s:if test="#roleModuleOperationPermHashMap['CREATE_PERM'] == 1">
					<!-- <div class="button-panel">	
					<div class="button"><a href="<s:url action="GenerateReportOperatorMenuAction" encode="true"/>"><input id="add" type="button" class="inputButton" value="+ Create Report"/></a></div>
					</div> -->
					</s:if>
					<s:else>
					<div class="button-panel">	
					<div class="button"><input id="add" type="button" class="inputButton" value="+ Create Report" onclick="alert('You dont have permission to create report');"/></div>
					</div>
					</s:else>
					</br>
					<div class="content">
						<div class="table-responsive">
							<table class="table table-bordered" id="datatable" >
								<thead>
									<tr>
									<th class="span4-1">REPORT ID</th>
									<th class="span4-1">REPORT TITLE</th> 
									<th class="span4-1">TEMPLATE</th>
									<th class="span4-1">SCHEDULED</th>
									<th class="span4-1">CREATION TIME</th>
									<th class="span4-1">REPORT LOCATION</th>
									<th class="span4-1" id="cpnl">CONTROL PANEL</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="allReportInstanceListG" var="allReportInstanceListGVar" status="allReportInstanceListGStatus">
										<tr>
											<td class="span4"><s:property value="#allReportInstanceListGStatus.count"/></td>
											<td class="span4"><s:property value="rtemplateTitle"/>
											<td class="span4"><s:property value="rtemplateName"/></td>
											<s:if test="#allReportInstanceListGVar.isScheduled == 1">
											<td class="span4-1">Yes</td>
											</s:if>
											<s:else>
											<td class="span4-1">No</td>
											</s:else>	
											<td class="span4"><s:property value="reportCdatetime"/></td>
											<td class="span4">D:\REPORTS\<s:property value="rtemplateName"/></td>
											<td class="span4">
											<!--Delete button-->	
											<s:if test="#allReportInstanceListGVar.isScheduled == 0">
											</s:if>
											<s:else>
											<s:url id="stopScheduleURL" action="StopOperatorSchedularAction" encode="true">
											<s:param name="reportId" value="%{reportId}" />
											</s:url>
											<s:a href="%{stopScheduleURL}" title="Stop Schedular For This Report">
											<img src="images/unprovisioned_icon.png" width="14" height="14" border="0" alt="Stop Schedule For this report" name="unscheduleReport" />
											</s:a>	
											</s:else>
											
											
											
											
											</td>
											
										</tr>
									</s:iterator>
								</tbody>
							</table>							
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->
		
<script type="text/javascript">
	$(document).ready(function(){
	$('#datatable').DataTable();
	$('.dataTables_filter input').addClass('form-control').attr('placeholder','Search');
	$('.dataTables_length select').addClass('form-control'); 
	});

</script>
<script>
	function confirmDelete() {  
	var answer = window.confirm("Are you sure you want to delete this record");  
	if(answer)
	return true;  
	else 
	return false;  
}  
</script>

	
</body>
</html>
