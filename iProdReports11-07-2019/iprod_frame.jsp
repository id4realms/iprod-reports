<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%
	response.setHeader("Cache-control","no-store"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires", -1);
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><tiles:importAttribute name="title" scope="request"/></title>
<!-- Framework CSS -->

</head>
<body>
  <div id="headerTile" class="headerTile">
  <tiles:insertAttribute name="header" />
  </div>  

  <div id="cl-wrapper">
  </div>
  <div id="bodyTile" class="bodyTile">
  <tiles:insertAttribute name="body"/>
  </div>
  </div>
  <div id="footerTile" class="footerTile">
  <tiles:insertAttribute name="footer"/>
  </div>
</body>
</html>