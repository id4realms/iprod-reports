<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 <%@taglib uri="/struts-tags" prefix="s" %>
 <%@ page import ="java.util.*" %>
 <%@ page import ="java.text.*" %>
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
 <%
	response.setHeader("Cache-control","no-store"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires", -1);
%>
 <!-- Framework CSS -->
<link rel="stylesheet" href="css/footer.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" type="text/css" href="js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
</head>
<body>
<div id="footer">
 <div class="footer_btm_block">
        <div class="footer_btm">
            <div class="pull-left">
              <!--<a href="http:\\www.id4-realms.com" target="_blank"> --> <span class="cl-iprod-brand"></span> <!--</a> --> 
            </div>
            <div class="pull-left">
                All rights reserved. &copy; iD4 Realms 2009-18
            </div>
		<!--	<div>
                <img
				src="images/plusconlogo.png" /> 
            </div> -->
			
				<div id="dashboard-report-range" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
					<span>
						<div id="clockboxForLineSummary" style="font-size: 20px; float:right;">&nbsp; February 20, 2015&nbsp;|&nbsp;4:30:26 PM </div>
					</span>
				</div>

			
        </div>
</div>
</div>


<script type="text/javascript">
	tday  =new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");

	function GetClockForLineSummary(){
	d = new Date();
	nday   = d.getDay();
	nmonth = d.getMonth();
	ndate  = d.getDate();
	nyear = d.getYear();
	nhour  = d.getHours();
	nmin   = d.getMinutes();
	msec=d.getSeconds();
	if(nyear<1000) nyear=nyear+1900;
	if(nhour ==  0) {ap = " AM";nhour = 12;} 
	else if(nhour <= 11) {ap = " AM";} 
	else if(nhour == 12) {ap = " PM";} 
	else if(nhour >= 13) {ap = " PM";nhour -= 12;}

	if(nmin <= 9) {nmin = "0" +nmin;}
	if(msec <= 9) {msec = "0" +msec;}

	document.getElementById('clockboxForLineSummary').innerHTML=" "+"&nbsp;&nbsp;"+" "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+"&nbsp;|&nbsp;"+" "+"&nbsp;"+nhour+":"+nmin+":"+msec+ap+" ";
	setTimeout("GetClockForLineSummary()", 1000);
	}

	GetClockForLineSummary();
</script>


</body>
</html>