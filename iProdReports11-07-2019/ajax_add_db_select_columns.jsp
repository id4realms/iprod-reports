<%@ page import="java.sql.*" %>
<%
	response.setHeader("Cache-control","no-store"); 
	response.setHeader("Pragma","no-cache");
	response.setDateHeader("Expires", -1);
%>
<%
    int numberOfColumns = 0;
	String sQuery = request.getParameter("sqlQuery");
	String jdbcDriverUrlG = request.getParameter("jdbcDriverUrl");
	String jdbcDriverClassnameG = request.getParameter("jdbcDriverClassname");
	String databaseNameG = request.getParameter("databaseInstanceName");
	String databaseUsernameG = request.getParameter("databaseServerUsername");
	String databasePasswordG = request.getParameter("databaseServerPassword");
	String mid = request.getParameter("mColIndex");
	ResultSet rs = null;		
	Class.forName(jdbcDriverClassnameG);		
	Connection con=DriverManager.getConnection(jdbcDriverUrlG,databaseUsernameG,databasePasswordG);	
	String sql = sQuery;		
	Statement st=con.createStatement();

try{
	rs = st.executeQuery(sql);
	ResultSetMetaData rsMetaData = rs.getMetaData();
	numberOfColumns = rsMetaData.getColumnCount();
%>
<select name='mainCols[<%=mid %>].mainColumnDataField' id='columnDataField' onchange="javascript:checkDbFieldDuplication(this,'<%=mid %>','singleton')" class='select'>
<option value='0'>Select Field to Map</option>
<%
for(int i = 1; i <= numberOfColumns; i++){
%>
<option value="<%=rsMetaData.getColumnLabel(i)%>"><%=rsMetaData.getColumnLabel(i)%></option>
<% } %>
</select>
<%
}
catch(SQLException e1){ 
    log.info("ajax_add_db_select_columns : DB connection closed due to error.");
	 do {
%>
<span style="color: red;"><strong>SQL STATE: <%=e1.getSQLState() %></strong></span> 
<span style="color: red;"><strong>ERROR CODE: <%=e1.getErrorCode() %></strong></span>
<span style="color: red;"><strong>MESSAGE: <%=e1.getMessage() %></strong></span>
<%
         e1= e1.getNextException();
      } while (e1 != null);

 }
finally {
                  if(con != null) {
                        try {
                              con.close();
                              log.info("ajax_add_db_select_columns - DB connection closed graciously.");
                        } catch(Exception e3){
 	                             con.close();
								 log.info("ajax_add_db_select_columns - DB connection closed due to error.");
%>                 
				   <span style="color: white;"><strong>Error: <%=e3 %></strong></span> 
<%                       
                        }
                  }
} 
%>

