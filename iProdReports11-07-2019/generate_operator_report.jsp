


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Generate Report";</script>
<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
<link href="js/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
<link href="js/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"/> 
<link href="js/bootstrap-datepicker-1.5.1/css/bootstrap-datepicker3.min.css" rel="stylesheet"/> 
<link href="js/bootstrap-datetimepicker-0.0.11/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

<link href="css/query-builder.default.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="js/fuelux/css/fuelux.css"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link href="css/override_defaults.css" rel="stylesheet" />
<!-- Custom template-style for this template -->
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css" />

<title>iProd</title>
</head>
<body class="fuelux">
<div class="container-fluid" id="pcont">
	<div class="row">
	<div class="col-md-12">
	<div class="col-md-6">
	<h3 class="page-title">Generate Report <small>datasets, styles and more</small></h3>
	</div>
	<div class="col-md-6 alert-title">
		<div class="eventmessages">
			<s:if test="hasActionMessages()">
			<div class="alert alert-success alert-white rounded">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<div class="icon"><i class="fa fa-check"></i></div>
				<strong>Success!</strong> 
				<s:iterator value="actionMessages">
				<s:property /><br>
				</s:iterator>
			</div>					
			</s:if>
			<s:if test="hasActionErrors()">
				<div class="alert alert-danger alert-white rounded" >
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="fa fa-times-circle"></i><strong>Error!</strong>
					<s:iterator value="actionErrors">
					<s:property />
					</s:iterator>
				</div>
			</s:if>
		</div>
	</div>
	<div class="col-md-12">
	<ol class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="<s:url action='LoginAction'/>">iPROD Reports Home</a>
		</li>
		<li class="active">Generate Report</li>

	</ol>
	</div>
	</div>
	</div>

 	<div class="cl-mcont">
	<div class="row">
	<div class="col-md-12">
	<div class="wizard" data-initialize="wizard" id="generateReportTemplateWizard">
		<div class="steps-container">
			<ul class="steps">
				<li data-step="1" class="active"><span class="badge">1</span>Init Report<span class="chevron"></span></li>
				<li data-step="2"><span class="badge">2</span>Select Date & Time<span class="chevron"></span></li>
				<li data-step="3"><span class="badge">3</span>Schedule Report<span class="chevron"></span></li>
				<!--<li data-step="4"><span class="badge">4</span>Email Audience<span class="chevron"></span></li>-->
				<li data-step="5"><span class="badge">4</span>Commit<span class="chevron"></span></li>
			</ul>
		</div>

		<div class="actions">
		<button type="button" class="btn btn-default btn-prev">
			<span class="glyphicon glyphicon-arrow-left"></span>Prev</button>
		<button type="button" class="btn btn-primary btn-next" id="validateTabFields" data-last="Generate Report">Next
			<span class="glyphicon glyphicon-arrow-right"></span>
		</button>
		</div>
					
		<form class="form-horizontal" role="form" name="generate_report_form" id="generate_report_form" data-validate="parsley" action="SaveNewOperatorReportAction" method="get"  theme="simple" >
		<div class="step-content">
			<div class="step-pane sample-pane active" id="step1" data-step="1">
			<div class="row">
				<div class="col-sm-6 col-md-6">
				<div class="block-flat">
					<div class="header">							
					<h3>Select Template To Create Report</h3>
					</div>
				</div>
				</div>
				<div class="col-md-6 col-sm-6">
						<div class="alert alert-danger" id="templateNameEntryDivError" style="display:none">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						</div>
						<div class="alert alert-warning" id="templateNameEntryDivWarning" style="display:none">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>	
						</div>
				</div>
			</div>
			<div class="col-md-12">		
				<div class="block-flat">
						<p>The process of generating a report is your first key step
						to generating dynamic reports. You kick-start the template creation with a unique name
						following which the process includes selection of data from a dataset using SQL, design & styling, column selection & computation as per your needs</p>
						<label>Select Template</label> 
						<s:select id="rtemplateId" name="rtemplateId" list="iprodReportsViewReportTemplateDetailsListG" listValue="rtemplateName" listKey="rtemplateId" headerValue="Select Template" headerKey="0" emptyOption="false" cssClass="form-control" theme="simple"/>
						<input type="hidden" name="dsTypeNameHidden" id="dsTypeNameHidden"/>
						<input type="hidden" name="dbServerUsernameHidden" id="dbServerUsernameHidden"/>
						<input type="hidden" name="dbServerPasswordHidden" id="dbServerPasswordHidden"/>
						<input type="hidden" name="dbInstanceNameHidden" id="dbInstanceNameHidden" />
						<input type="hidden" name="jdbcDriverUrlHidden" id="jdbcDriverUrlHidden"/>
						<input type="hidden" name="jdbcDriverClassnameHidden" id="jdbcDriverClassnameHidden"/>
						<input type="hidden" name="datasourceId" id="datasourceId"/>
				</div>
			</div>
		</div>
		<div class="step-pane" id="step2" data-step="2">
		<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success" id="sqlQueryValidateDivSuccess" style="display:none;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			</div>
			<div class="alert alert-danger" id="sqlQueryValidateDivError" style="display:none;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			</div>
			<div class="alert alert-warning" id="sqlQueryValidateDivWarning" style="display:none;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			</div>
			<h3><strong><label class="jsonresult" id="generateReportSqlQueryValidated" style="display:none"></label></strong></h3>
		</div>
		</div>

		<div class="row">
		<div class="col-md-12">		
					 
			  <input type="hidden" id="reportQueryOriginal" name="reportQueryOriginal"/>
			  <input type="hidden" id="reportQueryWithFilters" name="reportQueryWithFilters"/>
			 <textarea style="display:none;" name="reportQueryFrozen" id="reportQueryFrozen" class="form-control col-sm-12 col-md-12" readonly="true" style="height:400px; margin-top: 10px; font-size: 14px;"></textarea>
					 
				 <div class="col-sm-7 col-md-7">
				 <div id="generateReportSqlQueryResultDiv"></div>
				 <div class="block-flat">
				 <div class="header">							
						<h3>Select Date And Time</h3>
				 </div>			
				 <div id="whereBuilder" style="margin-top:10px;"></div>
				 <h4><span style="color: red;"><strong>*NOTE:HOURLY REPORT-ORDER BY->>DATETIME</strong></span></h4>
				  <h4><span style="color: red;"><strong>*NOTE:DAILY REPORT-ORDER BY->>DATE</strong></span></h4>
				 <div class="query-builder form-inline">
                 <dl class="rules-group-container" style="border-radius: 5px;">
				 <dt class="rules-group-header">
				 <div class="btn-group group-conditions">                
				 <label class="btn btn-xs btn-primary disabled">           
				 <input type="radio" value="WHERE" disabled>ORDER BY</label>           
				 </div>
				 </dt>
				 <dd class="rules-group-body">
				 <ul class="rules-list">
				 <li class="rule-container">
				 <select multiple="true" size="10" id="orderByFilters" name="orderByFilters">
				 </select>
				 </li>
				 </ul>
				 </dd>
				 </dl>
				 </div>
				 <input type="button" class="btn btn-primary" id="showFilter" onclick="filterValues();" value="VERIFY"/>
			<!-- 	 <input type="button" class="btn btn-primary" id="resetFilter" value="Reset Filters"/>
 -->				  </div>
				  </div>
		</div>
		</div>

		<div class="row">
		<div class="col-md-12">	
			<div id="reportExecuteSqlQueryOutputDiv"></div>
		</div>
		</div>

		</div><!--DIV WITH CLASS step-pane-active AND ID step2 ENDS-->
		<div class="step-pane" id="step3" data-step="3">
		  <div>
			<label for="now"><input type="radio" name="reportScheduleSelection" id="scheduleNow" checked="checked" value="scheduleNow"/>Now</label>
		  </div>
		    
		    <div>
			<input type="radio" name="reportScheduleSelection" id="scheduleParticularTime" value="scheduleParticularTime"/>Select Particular Time
			<input id="cron" name="cornExpression" class="form-control"/>
		    </div>
		</div>

		<div class="step-pane" id="step4" data-step="4">

					<h4>Select Recipient :</h4>
					<s:select multiple="true" size="10" id="chooseRecipientEmails" name="chooseRecipientEmails"
					list="AllRecipientDetailsListG" listValue="emailId" listKey="emailId" />
						  
		</div>
		
		<div class="step-pane" id="step5" data-step="5">
		   <div class="form-group no-padding">
				<div class="col-sm-7">
					<h3 class="hthin">Generate Report</h3>
				</div>
			</div>				
			<div class="form-group">														
				<label class="col-sm-3 control-label">
				Choose export	Option(s) :</label>
				<input type="checkbox" checked	name="exportOption" id="PDF" value="PDF" /> Export To PDF
				<!--<input type="checkbox" 	name="exportOption" id="HTML" value="HTML" /> Export  To HTML -->
				<input type="checkbox" 	checked name="exportOption" id="EXCEL" value="EXCEL"/> Export To EXCEL
			</div>
			<input type="hidden" id="filterData" name="dateTimeFilter"/>

			<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-primary">Submit </button>
			</div>
			</div>	
		</div>

		</div><!---div with step-content ends-->
		</form>
		<div id="loading" style="display:none; text-align: center"><img src="images/load.gif" alt="" /></div>
	</div><!--DIV WITH BLOCK WIZARD END--->
	</div><!----DIV WITH CLASS "col-md-12"------>
	</div><!----DIV WITH CLASS class="row"------->
	</div><!-----DIV WITH CLASS class="cl-mcont"-->
	</div><!-----DIV WITH CLASS class="container-fluid"-->	

	
	<!-------------------CLEAN ZONE JS FILES ------------------------------->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/moment-2.15.1.min.js"></script>	
	<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
	<script type="text/javascript" src="js/query-builder.standalone.min.js"></script>
	<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
	<script type="text/javascript" src="js/fuelux/js/fuelux.js"></script>
	<!-------------------CLEAN ZONE JS FILES ------------------------------->
	<!-------------------VALIDATION JS FILES ------------------------------->
	<!--<script type="text/javascript" src="js/jdbc_Mysql.js"/>-->
	<script type="text/javascript" src="js/report/generateReportFormElements.js"></script>
	<script type="text/javascript" src="js/report/generateReportCustomValidation.js"></script>
	<script type="text/javascript" src="js/report/generateReportFormTabValidation.js"></script>
	<script type="text/javascript" src="js/report/popup_script.js"></script>
	<!-------------------VALIDATION JS FILES ------------------------------->
	<script type="text/javascript" src="js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="js/bootstrap-datepicker-1.5.1/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.min.js"></script>
	
	

		<script>
		function filterValues()
		{
			var str ="";
			var rulValueCount = 0;
			var fromControl = 0;
			var index = 0;
			
			$('.rules-list .rule-value-container').each(function() {
				fromControl = $(this).find(".form-control").length;
				index = 0;
				var list = document.getElementsByClassName("rule-value-container")[rulValueCount];
				while(index < fromControl){
					str = str + "-" + list.getElementsByClassName("form-control")[index].value;
					index ++;
				}
				
				rulValueCount++;
			});
			document.getElementById("filterData").value = str;
		}
		
	</script>

	<script src="js/cronGen.js"></script>
	<script>
		$(function () {
		$("#cron").cronGen();                
		});
	</script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#chooseRecipientEmails').multiselect();
		});
	</script>

	<script type="text/javascript">
		$('#validateTabFields').on("click",function(){
			$('div.step-content').hide();
			$( '#loading' ).fadeIn( 500 );			
			$( '#loading' ).delay( 1000 ).fadeOut( 500 );	
			setTimeout(function(){$('div.step-content').show();}, 2000);	
		});
	</script>

</body>
</html>