<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>document.title = "Edit Datasource";</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="shortcut icon" href="images/favicon.png"/>
<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.css" />
<title>iProd</title>
</head>
<body>	
<div class="container-fluid" >
	<div class="row-head">
		<div class="span12">
			<h3 class="page-title">Edit Datasource <small>Edit Datasource details</small></h3>
			<ul class="breadcrumb">
				<li>
					<i class="fa fa-link"></i>
					<a href="#">Quick links : </a>
				</li>
				<li>
					<i class="fa fa-home"></i>
					<a href="<s:url action="ManageReportsDashboardAction"/>">Home</a>
				</li>
				<li><a href="ManageDataSourceMenuAction.action">Manage Datasource</a></li>

			</ul>
			<div class="cl-mcont">
				<div class="content">
					
					<form class="form-horizontal group-border-dashed" action="#" id="update_datasource" name="update_datasource_form" data-validate="parsley"  >
					   <s:iterator value="datasourceListG" status="rowstatus">
						<s:hidden name="datasourceId" value="%{datasourceId}"/>
						<s:hidden name="rdbmsServerMachineName" value="%{rdbmsServerMachineName}"/>
						<s:hidden name="rdbmsServerUsername" value="%{rdbmsServerUsername}"/>
						<s:hidden name="rdbmsServerPassword" value="%{rdbmsServerPassword}"/>
						<s:hidden name="rdbmsIsWindowsAuth" value="%{rdbmsIsWindowsAuth}"/>
						
						
						<div class="form-group">
						<label class="col-sm-3 control-label">Datasource Name</label>
						<div class="col-sm-6">
						<input type="text" name="datasourceName" id="datasourceName" data-required="true"  disabled data-trigger="change" class="form-control" value="<s:property value="%{datasourceName}" />">
						</div>
						<span id="display"></span>
						</div>
					
						<div class="form-group"> 
						<label class="col-sm-3 control-label">Database Name</label>
						<div class="col-sm-6">
						<input type="text" name="rdbmsDatabaseName" id="rdbmsDatabaseName" data-required="true"  data-trigger="change" class="form-control"  value="<s:property value="%{rdbmsDatabaseName}" />">
						</div>
						</div>
						
						
						<div class="form-group"> 
						<label class="col-sm-3 control-label"> Connection URL</label>
						<div class="col-sm-6">
						<input type="text" name="rdbmsJdbcDriverUrl"  id="rdbmsJdbcDriverUrl" maxlength="100" parsley-trigger="change" data-required="true" required placeholder="Enter Database Connection URL" parsley-error-message="Please Enter Database Connection URL" class="form-control" value="<s:property value='%{rdbmsJdbcDriverUrl}'/>">
						</div>
						</div>
						
						<div class="form-group"> 
						<label class="col-sm-3 control-label">Port Number</label>
						<div class="col-sm-6">
						<input type="text" name="rdbmsServerPort"  id="rdbmsServerPort" maxlength="100" parsley-trigger="change" data-required="true" required placeholder="Enter Port Number" parsley-error-message="Please Enter Port Number" class="form-control" value="<s:property value='%{rdbmsServerPort}'/>">
						</div>
						</div>
						
						<div class="form-group"> 
						<label class="col-sm-3 control-label">Instance Name</label>
						<div class="col-sm-6">
						<input type="text" name="rdbmsInstanceName"  id="rdbmsInstanceName" maxlength="100" parsley-trigger="change" data-required="true" required placeholder="Enter Instance Name" parsley-error-message="Please Enter Instance Name" class="form-control" value="<s:property value='%{rdbmsInstanceName}'/>">
						</div>
						</div>
						
						<div class="form-group"> 
						<label class="col-sm-3 control-label">Server Name</label>
						<div class="col-sm-6">
						<input type="text" name="rdbmsServerMachineName"  id="rdbmsServerMachineName" maxlength="100" parsley-trigger="change" data-required="true" required placeholder="Enter Server Name" parsley-error-message="Please Enter Server Name" class="form-control" value="<s:property value='%{rdbmsServerMachineName}'/>">
						</div>
						</div>
						
						<div class="form-group">
						<label class="col-sm-3 control-label">Select Database</label>
						<div class="col-sm-6">
						<s:select id="rdbmsId" name="rdbmsId" required="true" parsley-error-message="Please select RDBMS." parsley-required="true" list="allRdbmsListG" listValue="rdbmsName + '-' +rdbmsSubVersionName" listKey="rdbmsId" headerKey="" headerValue="Select RDBMS" parsley-trigger="change" cssClass="form-control"/>
						</div>
						</div>
				
				 </s:iterator>
				 
						<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
						<button id="updateDatasource" class="btn btn-primary" onclick="javascript:if($('#update_datasource_form').parsley('validate')==true) type= "submit">Update</button>
						 <button class="btn btn-default" onclick="javascript:var f =document.forms['update_datasource_form'];f.action='ManageDataSourceMenuAction';f.submit()">Cancel</button>
						</div>
						</div>			   

					</form>
				
				</div><!-- End of content -->
			</div><!-- End of cl-mcont -->	
		</div>
	</div>
</div>
	
	
	
	
<!-------------------CLEAN ZONE JS FILES ------------------------------->
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script type="text/javascript" src="js/report/jdbc_Mssql.js"/>
<script type="text/javascript" src="js/jquery.datatables/jquery.datatables.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<!-------------------CLEAN ZONE JS FILES ------------------------------->			
			
<script>
	$("#updateDatasource").on("click",function() 
	{
	});
</script>



</body>
</html>
