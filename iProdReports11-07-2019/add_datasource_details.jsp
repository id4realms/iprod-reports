
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.net.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String remoteHost =  request.getRemoteAddr();
	String hostName = InetAddress.getByName(remoteHost).getHostAddress();
%>
<html xmlns="http://www.w3.org/1999/xhtml">    
<head>
	<script>document.title = "";</script>
	<meta http-equiv="Content-Type" content="text/html; charset= iso-8859-1" />
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="css/popup_style.css" type="text/css" />

	<title>iProd</title>
</head>
<body>

<div id="popupbodycontent">
	<div class="popupclose"></div>
		<div class="row"><!---------START OF CLASS row------------------->
		<div class="header">										
			<div class="button-panel">						
			<div class="button"><h4 style="color:white;" class="text-center">ADD NEW DATASOURCE </h4></div>	 
             </div>											
			</div>
			<div class="col-sm-6 col-md-6">
				<div class="block-flat">	
					<div class="header">
						<h3>JDBC Datasource Details</h3>
					</div>
					<div class="content">
					  <form action="#" id="popDetailForm" name="popDetailFormName" data-validate="parsley"  >
					  <div class="form-group">
						  <label class="control-label">Database Profile Name</label>
						  <input type="text" id="datasourceInstanceName" parsley-trigger="change" data-required="true" required placeholder="Enter Profile name" class="form-control" />
						  <div id="existDatasourceInstanceNameDiv"></div>
					  </div>
					  <div class="form-group">
						  <label class="control-label">Select Database</label>
						  <p>Choose one of various RBDMS supported</p>
						  <select class="select2" id="databaseId"
						  parsley-trigger="change" required>
						  <option value="1">MYSQL</option>
						  <option value="2">MSSQL</option>
						  </select>
					  </div>
					  <div class="form-group">
						  <div id="dialectId" style="display: none; text-align: left;">Database Dialect</div>
						  <select class="select" id="sqlDialectId" parsley-trigger="change" required style="display: none;">
						  <option value="p1">MySQL 5.0</option>
						  <option value="p2">MS SQLServer 2005</option>
						  <option value="p3">MS SQLServer 2008</option>
						  <option value="p4">MS SQLServer 2008 R2</option>
						  </select>
					  </div>
					  <div class="form-group">
						  <div id="enabledbMSSQLInstanceLabelDiv" style="display: none;">SelectMSSQL Instance</div>
						  <div id="enabledbMSSQLInstanceDiv" style="display: none;">
						  <div  id="selectInastanceid" style="display: none;">Select MSSQL Instance</div>
						  <select class="select2" id="mssqlInstanceId"
						  parsley-trigger="change" required>
						  <option value="SQLEXPRESS">SQLEXPRESS</option>
						  <option value="SQLEXPRESS1">SQLEXPRESS1</option>
						  </select>
						  </div>
					  </div>
					  <div class="form-group">
						  <div id="enableWindowsAuthLabelDiv" style="display: none;">
						  <font color="red">*</font>Enable Windows Authentication
						  </div>
						  <div id="enableWindowsAuthRadioDiv" style="display: none;">
						  <s:radio name="isWindowsAuth" id="isWindowsAuth"
						  list="#{1:'Yes',0:'No'}" value="1" theme="simple"
						  cssClass="radioInput" />
						  </div>
					  </div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<div class="block-flat">
					<div class="header">
						<h3>Build JDBC URL</h3>
					</div>
					
					<div class="content">
						<div class="form-group">
							<label class="control-label">Select JDBC Driver</label>
							<p>Choose one of JDBC Drivers supported</p>
							<select class="select2" id="jdbcDriverId" parsley-trigger="change" required>
							<option value="1">com.microsoft.sqlserver.jdbc.SQLServerDriver</option>
							</select>
						</div>
						<div class="form-group">
							<label class="control-label">Select Database</label>
							<p>Choose one of database schemas</p>
							<select class="select2" id="databaseNameid" parsley-trigger="change" required>
							<option value="p1">iprod_fiat_db</option>
							<option value="p2">iprod_reports_db</option>
							</select>
						</div>
						<div class="form-group">
							<label>Server Name</label> 
							<input type="text" id="servername" parsley-trigger="change" required placeholder="Enter server name" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Port No</label> <input type="text" id="portnumber" parsley-trigger="change" required	placeholder="Enter port number" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Username</label> <input type="text" id="username" parsley-trigger="change" required placeholder="Enter DB user name" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Password</label> <input type="password" id="password" parsley-trigger="change" required placeholder="Enter DB password" class="form-control"/>
						</div>
						</form>
					</div>
				</div>
			</div><center>
			<div>
			 <button id="validatePopUpForm" class="btn btn-primary" type="submit">Validate</button>
			 <button class="btn btn-default" id="cancelbutton">Cancel</button>
			
			</div>
		</div>
</div>

<script type="text/javascript">
	$("div.popupclose").on('click', function(e) {
	disableloadDataSourcePopup();
	// function close pop up
	});
</script>
<script type="text/javascript" src="js/jquery.core/jquery-3.1.0.js"></script>
	<script type="text/javascript" src="js/parsley-2.4.4/js/parsley.js"></script>
    <script type="text/javascript" src="js/report/jdbc_Mssql.js"/>
</body>
</html>