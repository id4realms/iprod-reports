<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	response.setHeader("Cache-control","no-store"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires", -1);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Opps! Invalid iPROD Reports License</title>
</head>
<body>
<center>
<h2>Looks like you do not have a valid iPROD Reports License installed</h2>
<p align="left" style="font-family: Arial, Trebuchet MS;">"Software license is invalid" error message appears in two instances: <br>

1. When there is no license key installed in the system.<br>
2. When the license is generated for the wrong installation url (SITE_URL)<br><br>

To fix issue 1, you need to:<br>
a) Copy licenses procured into the root of the installation folder.<br><br>

To fix issue 2, you need to:<br>
a) Generate a proper license for your installation directory. Please be advised that http://yoursite.com and http://www.yoursite.com are two different URL's. If your software is installed onto the yoursite.com host, you need to generate the license to http://yoursite.com, and vice versa.<br><br>

If you need further assistance or to purchase a valid license please contact support@id4-realms.com or sales@id4-realms.com</p>
</center>
</body>
</html>